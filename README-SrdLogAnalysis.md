# Log Analysis for the RHEDcloud Security Risk Detection Service

Log analysis consumes a lot of memory, so I've started using a large EC2 instance.

Create a m5ad.4xlarge EC2 instance and install Java.

ssh -i emory-aws-account-165-1.pem ec2-user@$IP
sudo yum install java-1.8.0-openjdk

Back on your laptop, copy the compiled SRD code and dependent libraries to the EC2 instance:

scp -i emory-aws-account-165-1.pem -r rhedcloud-srd-service/target/classes ec2-user@$IP:/home/ec2-user/classes
scp -i emory-aws-account-165-1.pem rhedcloud-srd-service/lib/aws-java-sdk-core-1.11.772.jar ec2-user@$IP:/home/ec2-user/classes

Generate full logs from the SRD beanstalk console and download them.
My other script unzips them into directories and then uncompresses the rotated logs.
Zip just the SrdService.log files and put them up on the EC2 instance.

cd ~/Downloads
zip logfiles.zip vari-00f007c39ccb255dd/log/tomcat8/SrdService.log vari-0245119b2a80e546e/log/tomcat8/SrdService.log vari-0adf73180587a920a/log/tomcat8/SrdService.log vari-00f007c39ccb255dd/log/tomcat8/rotated/SrdService.log1598875261 ...
scp -i emory-aws-account-165-1.pem logfiles.zip ec2-user@$IP:/home/ec2-user

SSH to host and run the log analysis.  Notice the Xmx setting is giving a ton of memory to the JVM.

ssh -i emory-aws-account-165-1.pem ec2-user@$IP
unzip logfiles.zip
java -Xmx50g -classpath /home/ec2-user/classes:/home/ec2-user/classes/aws-java-sdk-core-1.11.772.jar \
   edu.emory.it.services.srd.SrdLogAnalysis vari-00f007c39ccb255dd/log/tomcat8/SrdService.log ...

Copy the results back to your laptop

scp -i emory-aws-account-165-1.pem ec2-user@$IP:/home/ec2-user/SrdLogAnalysis-20200831-085501.csv .

