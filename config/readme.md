# Config folder files

Name | Description
----------------- | -------------
`AwsSrdApplication.properties`  | Example properties file that can be used to run the appliction
`AwsSrdApplication.xml`  | Example configuration file that can be used to run the appliction
`AwsSrdService.properties`  | Example properties file that can be used to run the service
`AwsSrdSrdService.xml`  | Example configuration file that can be used to run the service
`AwsSrdApplication.properties`  | Example properties file that can be used to run the appliction
`AwsSrdApplication.xml`  | Example configuration file that can be used to run the appliction
`AwsSrdServiceSync.properties`  | Example properties file that can be used to run the sync consumer command
`AwsSrdSrdServiceSync.xml`  | Example configuration file that can be used to run the sync consumer command
`srd.properties` | Properties file used by the ServiceGen app to produce the hibernate mapping and WSDL files.


