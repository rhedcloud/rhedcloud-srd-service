package edu.emory.it.services.srd.provider;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetectionMetric;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionMetricQuerySpecification;


public interface SecurityRiskDetectionMetricQueryProvider {
    /**
     * Initialize the provider.
     * @throws InstantiationException on error
     */
    void init() throws InstantiationException;

    /**
     * Query for metrics.
     * @param spec query spec
     * @param metric query results
     * @throws ProviderException on error
     */
    void query(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) throws ProviderException;
}
