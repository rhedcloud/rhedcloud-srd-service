package edu.emory.it.services.srd;

import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;

public enum ComplianceClass {
    Standard(StandardComplianceClass.class),
    HIPAA(HIPAAComplianceClass.class),
    EnhancedSecurity(EnhancedSecurityComplianceClass.class);

    public final Class clazz;

    ComplianceClass(Class clazz) {
        this.clazz = clazz;
    }
}
