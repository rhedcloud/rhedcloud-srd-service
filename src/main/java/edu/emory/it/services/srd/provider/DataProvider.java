package edu.emory.it.services.srd.provider;

import edu.emory.it.services.srd.util.SpringIntegration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.TransactionManagementConfigurer;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
public class DataProvider implements TransactionManagementConfigurer {
    @Bean
    public DataSource dataSource() {
        SpringIntegration springIntegration = SpringIntegration.getInstance();

        DriverManagerDataSource driver = new DriverManagerDataSource();
        driver.setDriverClassName(springIntegration.getDriverClassName());
        driver.setUrl(springIntegration.getJdbcUrl());
        driver.setUsername(springIntegration.getUsername());
        driver.setPassword(springIntegration.getPassword());
        return driver;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        SpringIntegration springIntegration = SpringIntegration.getInstance();

        Database database;
        if (springIntegration.getJdbcUrl().startsWith("jdbc:postgresql")) {
            database = Database.POSTGRESQL;
        }
        else if (springIntegration.getJdbcUrl().startsWith("jdbc:oracle")) {
            database = Database.ORACLE;
        }
        else if (springIntegration.getJdbcUrl().startsWith("jdbc:mysql")) {
            database = Database.MYSQL;
        }
        else {
            database = Database.DEFAULT;
        }

        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(database);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JpaVendorAdapter jpaVendorAdapter) {
        SpringIntegration springIntegration = SpringIntegration.getInstance();

        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setDataSource(dataSource);
        lef.setJpaVendorAdapter(jpaVendorAdapter);
        lef.setPackagesToScan(springIntegration.getBasePackages());
        return lef;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

    @Override  // must not be a @Bean
    public TransactionManager annotationDrivenTransactionManager() {
        return transactionManager();
    }
}
