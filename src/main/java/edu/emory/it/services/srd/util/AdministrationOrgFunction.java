package edu.emory.it.services.srd.util;

import com.amazonaws.services.organizations.AWSOrganizations;
import org.apache.logging.log4j.Logger;

public abstract class AdministrationOrgFunction {
    private String accountId;
    private Logger logger;
    private AWSOrganizations orgClient;
    private SecurityRiskContext srContext;

    public AdministrationOrgFunction(String accountId, Logger logger, SecurityRiskContext srContext, AWSOrganizations orgClient) {
        this.accountId = accountId;
        this.logger = logger;
        this.orgClient = orgClient;
        this.srContext = srContext;
    }

    public abstract void apply() throws Exception;

    public String getAccountId() { return accountId; }
    public Logger getLogger() { return logger; }
    public String getLogtag() { return srContext.LOGTAG; }
    public AWSOrganizations getOrgClient() { return orgClient; }
    public SecurityRiskContext getSecurityRiskContext() { return srContext; }
}
