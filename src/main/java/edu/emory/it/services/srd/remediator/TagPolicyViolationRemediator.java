package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPI;
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPIClient;
import com.amazonaws.services.resourcegroupstaggingapi.model.FailureInfo;
import com.amazonaws.services.resourcegroupstaggingapi.model.TagResourcesRequest;
import com.amazonaws.services.resourcegroupstaggingapi.model.TagResourcesResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

/**
 * Remediates tag policy violations and missing required tags.
 * @see edu.emory.it.services.srd.detector.TagPolicyViolationDetector the detector
 */
public class TagPolicyViolationRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator  {
	
	private long writeDelayInMillis = 0;
	
    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
        String LOGTAG="[TagPolicyViolationRemediator.init] ";
        try {
            Properties properties = aConfig.getProperties("TagPolicyViolation");
            String writeDelayInMillisString = properties.getProperty("WriteDelayInMillis");
            if (writeDelayInMillisString!=null) {
            	try {
            		writeDelayInMillis = Long.parseLong(writeDelayInMillisString);
            		if (writeDelayInMillis < 0) {
            			logger.fatal(LOGTAG+"WriteDelayInMillis must be >= 0");
            			throw new SecurityRiskRemediationException("WriteDelayInMillis must be >= 0", null);
            		}
            		logger.info(LOGTAG+"TagPolicyViolation writeDelayInMillisString="+writeDelayInMillis+" milliseconds");
            	} catch (NumberFormatException e) {
            		logger.fatal(LOGTAG+"Bad number in WriteDelayInMillis");
            		throw new SecurityRiskRemediationException(e);
            	}
            } else {
            	logger.info(LOGTAG+"TagPolicyViolation is using the default writeDelayInMillis of "+writeDelayInMillis+" milliseconds");
            }
        } catch (EnterpriseConfigurationObjectException e) {
            logger.warn(LOGTAG+"Can't get PropertyConfig named TagPolicyViolation");
        	logger.info(LOGTAG+"TagPolicyViolation is using the default writeDelayInMillis of "+writeDelayInMillis+" milliseconds");
       }
    }
	
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
    	String LOGTAG="[TagPolicyViolationRemediator.remediate] ";
        if (detected.getType().equals(DetectionType.TagPolicyViolationNoTagsOnAccount.name())
                || detected.getType().equals(DetectionType.TagPolicyViolationNoProfileTag.name())
                || detected.getType().equals(DetectionType.TagPolicyViolationNoRTPSEntry.name())) {
            setNotificationOnly(detected, srContext.LOGTAG);
            return;
        }

        // All we can validate is the basic AWS ARN and detection type.
        if (remediatorPreConditionFailed(detected, "arn:aws:", srContext.LOGTAG,
                DetectionType.TagPolicyViolationBadValue, DetectionType.TagPolicyViolationNoTag))
            return;

        // ARN is for any taggable resource but generally the format is
        // arn:partition:service:region:account:resource/resourceId
        // but see https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html for complete details
        // because this remediator deals with a huge selection of services the ARN validation has to allow for all
        // the variations which means we can't validate account (S3 has no account or region in the ARN)
        Arn arn = remediatorCheckResourceName(detected, null, srContext.LOGTAG);
        if (arn == null)
            return;

        // for some resources, there may not be a region but getClient handles that
        String region = (arn.getRegion().isEmpty() ? null : arn.getRegion());

        AWSResourceGroupsTaggingAPI client;
        try {
            client = getClient(accountId, region, srContext.getMetricCollector(), AWSResourceGroupsTaggingAPIClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Taggable resource ARN '" + detected.getAmazonResourceName() + "'", e);
            return;
        }

        handleDetectedRisk(detected, srContext, client);
    }

    void handleDetectedRisk(DetectedSecurityRisk detected, SecurityRiskContext srContext, AWSResourceGroupsTaggingAPI client) {
    	String LOGTAG="[TagPolicyViolationRemediator.handleDetectedRisk] ";    	
        Properties violationProperties = detected.getProperties();
        if (null == violationProperties || violationProperties.isEmpty()) {
            setError(detected, srContext.LOGTAG, "Security risk input for " + detected.getType() + " is missing violation properties");
            return;
        }

        Map<String, String> tags = new HashMap<>();
        for (Map.Entry<Object, Object> entry : violationProperties.entrySet()) {
            String tagName = (String) entry.getKey();
            String tagValue = (String) entry.getValue();

            if (null == tagName || tagName.isEmpty()) {
                setError(detected, srContext.LOGTAG, "Security risk input for " + detected.getType() + " has empty tag name");
                return;
            }
            if (null == tagValue || tagValue.isEmpty()) {
                setError(detected, srContext.LOGTAG, "Security risk input for " + detected.getType() + " has empty tag value");
                return;
            }
            tags.put(tagName, tagValue);
        }

        if (isSimulation()) {
            setSuccess(detected, srContext.LOGTAG, "Simulation Only - Would have added required tags to resource (" + tags.toString() + ")");
        }
        else {
            TagResourcesRequest tagResourcesRequest = new TagResourcesRequest()
                    .withResourceARNList(detected.getAmazonResourceName())
                    .withTags(tags);

            try {
            	logger.info(LOGTAG+"TagPolicyViolationRemediator is delaying tag update for "+writeDelayInMillis+" ms");
            	Thread.sleep(writeDelayInMillis);
                TagResourcesResult tagResourcesResult = client.tagResources(tagResourcesRequest);
                Map<String, FailureInfo> failedResourcesMap = tagResourcesResult.getFailedResourcesMap();
                if (failedResourcesMap.isEmpty()) {
                    setSuccess(detected, srContext.LOGTAG, "Added required tags to resource (" + tags.toString() + ")");
                }
                else {
                    // because we only ever operate on a single resource, there can only ever be one failure
                    setError(detected, srContext.LOGTAG, "Failed to add required tags to resource (" + tags.toString() + "). "
                            + failedResourcesMap.values().iterator().next().getErrorMessage());
                }
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Failed to add required tags to resource (" + tags.toString() + ")", e);
            }
        }
    }
    

    boolean isSimulation() {
        return false;
    }
}
