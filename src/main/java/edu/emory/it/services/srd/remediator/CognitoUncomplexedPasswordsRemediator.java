package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolRequest;
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolResult;
import com.amazonaws.services.cognitoidp.model.PasswordPolicyType;
import com.amazonaws.services.cognitoidp.model.ResourceNotFoundException;
import com.amazonaws.services.cognitoidp.model.UpdateUserPoolRequest;
import com.amazonaws.services.cognitoidp.model.UserPoolType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates Cognito user pools with a policy that does not have the required password complexity<br>
 *
 * <p>Remediates the policy by setting the policy to:</p>
 * <ul>
 *     <li>Minimum length: 9</li>
 *     <li>Require numbers: Yes</li>
 *     <li>Require special character: Yes to all</li>
 *     <li>Require uppercase: Yes</li>
 *     <li>Require lowercase: Yes</li>
 * </ul>
 *
 * <p>This remediator is currently not running due to a security policy that does not allow user pools to be listed.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: TBD<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.CognitoUncomplexedPasswordsDetector the detector
 */
public class CognitoUncomplexedPasswordsRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cognito-idp:", srContext.LOGTAG))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String userPoolId = arn[5].substring(arn[5].lastIndexOf("/") + 1);

        final AWSCognitoIdentityProvider cognito;
        try {
            cognito = getClient(accountId, srContext.getMetricCollector(), AWSCognitoIdentityProviderClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "User Pool with id '" + userPoolId + "'", e);
            return;
        }


        DescribeUserPoolRequest describeUserPoolRequest = new DescribeUserPoolRequest().withUserPoolId(userPoolId);
        UserPoolType userPool;
        try {
            DescribeUserPoolResult describeUserPoolResult = cognito.describeUserPool(describeUserPoolRequest);
            userPool = describeUserPoolResult.getUserPool();
        } catch (ResourceNotFoundException e) {
            setError(detected, srContext.LOGTAG,
                    "User Pool with id '" + userPoolId + "' not found", e);
            return;
        }

        PasswordPolicyType passwordPolicy = userPool.getPolicies().getPasswordPolicy();

        boolean simplePassword =  passwordPolicy.getMinimumLength() == null
                                || passwordPolicy.getMinimumLength() < 9
                                || !Boolean.TRUE.equals(passwordPolicy.getRequireLowercase())
                                || !Boolean.TRUE.equals(passwordPolicy.getRequireUppercase())
                                || !Boolean.TRUE.equals(passwordPolicy.getRequireNumbers())
                                || !Boolean.TRUE.equals(passwordPolicy.getRequireSymbols());

        if (!simplePassword) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "User Pool with id '" + userPoolId + "' already has a complex password");
            return;
        }

        if (passwordPolicy.getMinimumLength() == null || passwordPolicy.getMinimumLength() < 9) {
            passwordPolicy.setMinimumLength(9);
        }
        passwordPolicy.setRequireLowercase(true);
        passwordPolicy.setRequireUppercase(true);
        passwordPolicy.setRequireNumbers(true);
        passwordPolicy.setRequireSymbols(true);

        UpdateUserPoolRequest updateUserPoolRequest = createUpdateUserPoolRequest(userPoolId, userPool);
        cognito.updateUserPool(updateUserPoolRequest);

        setSuccess(detected, srContext.LOGTAG,
                "User Pool with id '" + userPoolId + "' has been updated with required password complexity.");
    }

    private UpdateUserPoolRequest createUpdateUserPoolRequest(String userPoolId, UserPoolType userPool) {
        UpdateUserPoolRequest updateUserPoolRequest = new UpdateUserPoolRequest()
                .withUserPoolId(userPoolId)
                .withPolicies(userPool.getPolicies())
                .withLambdaConfig(userPool.getLambdaConfig())
                .withAutoVerifiedAttributes(userPool.getAutoVerifiedAttributes())
                .withSmsVerificationMessage(userPool.getSmsVerificationMessage())
                .withEmailVerificationMessage(userPool.getEmailVerificationMessage())
                .withEmailVerificationSubject(userPool.getEmailVerificationSubject())
                .withVerificationMessageTemplate(userPool.getVerificationMessageTemplate())
                .withSmsAuthenticationMessage(userPool.getSmsAuthenticationMessage())
                .withMfaConfiguration(userPool.getMfaConfiguration())
                .withDeviceConfiguration(userPool.getDeviceConfiguration())
                .withEmailConfiguration(userPool.getEmailConfiguration())
                .withSmsConfiguration(userPool.getSmsConfiguration())
                .withUserPoolTags(userPool.getUserPoolTags())
                .withAdminCreateUserConfig(userPool.getAdminCreateUserConfig())
                .withUserPoolAddOns(userPool.getUserPoolAddOns());

        if (updateUserPoolRequest.getUserPoolAddOns() != null &&
                updateUserPoolRequest.getUserPoolAddOns().getAdvancedSecurityMode() == null) {
            updateUserPoolRequest.getUserPoolAddOns().setAdvancedSecurityMode("OFF");
        }

        return updateUserPoolRequest;
    }
}
