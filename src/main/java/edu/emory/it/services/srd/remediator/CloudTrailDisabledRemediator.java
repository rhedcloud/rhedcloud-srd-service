package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudtrail.AWSCloudTrail;
import com.amazonaws.services.cloudtrail.AWSCloudTrailClient;
import com.amazonaws.services.cloudtrail.model.CreateTrailRequest;
import com.amazonaws.services.cloudtrail.model.DescribeTrailsRequest;
import com.amazonaws.services.cloudtrail.model.DescribeTrailsResult;
import com.amazonaws.services.cloudtrail.model.GetTrailStatusRequest;
import com.amazonaws.services.cloudtrail.model.GetTrailStatusResult;
import com.amazonaws.services.cloudtrail.model.InsufficientS3BucketPolicyException;
import com.amazonaws.services.cloudtrail.model.S3BucketDoesNotExistException;
import com.amazonaws.services.cloudtrail.model.StartLoggingRequest;
import com.amazonaws.services.cloudtrail.model.Trail;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.Properties;

/**
 * Enables CloudTrail logging if it is disabled.<br>
 *
 * <p>The remediator creates a new CloudTrail if none exists. The S3 bucket from configuration parameter
 * CloudTrailDisabled : s3BucketToUse   is used to store new logs.</p>
 *
 * <p>For HIPAA class accounts.</p>
 *
 * <p>Security: Enforce, Alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.CloudTrailDisabledDetector the detector
 */
public class CloudTrailDisabledRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private String s3BucketToUse;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
        try {
            Properties properties = appConfig.getProperties("CloudTrailDisabled");
            s3BucketToUse = properties.getProperty("s3BucketToUse");
        } catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudtrail:", srContext.LOGTAG, DetectionType.CloudTrailDisabled))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];

        final AWSCloudTrail cloudTrail;
        try {
            cloudTrail = getClient(accountId, region, srContext.getMetricCollector(), AWSCloudTrailClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "CloudTrail disabled", e);
            return;
        }

        DescribeTrailsRequest request = new DescribeTrailsRequest().withIncludeShadowTrails(true);
        DescribeTrailsResult result = cloudTrail.describeTrails(request);

        String s3BucketName = s3BucketToUse.replace("{accountId}", accountId);

        boolean hasActiveTrail = false;
        String targetTrail = null;
        AWSCloudTrail targetRegionClient = null;
        for (Trail trail : result.getTrailList()) {
            AWSCloudTrail regionClient;
            try {
                regionClient = getClient(accountId, trail.getHomeRegion(), srContext.getMetricCollector(), AWSCloudTrailClient.class);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "CloudTrail Region '" + region + "'", e);
                return;
            }
            if (targetTrail == null && s3BucketName.equals(trail.getS3BucketName())) {
                targetTrail = trail.getName();
                targetRegionClient = regionClient;
            }
            GetTrailStatusRequest getTrailStatusRequest = new GetTrailStatusRequest().withName(trail.getName());
            GetTrailStatusResult getTrailStatusResult = regionClient.getTrailStatus(getTrailStatusRequest);
            if (getTrailStatusResult.isLogging()) {
                hasActiveTrail = true;
                break;
            }
        }

        if (hasActiveTrail) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Region '" + region + "' already has CloudTrail enabled.");
            return;
        }

        boolean createTrail = (targetTrail == null);
        if (createTrail) {
            targetTrail = "emory-aws-cloudtrail";
            CreateTrailRequest createTrailRequest = new CreateTrailRequest()
                    .withIsMultiRegionTrail(true)
                    .withS3BucketName(s3BucketName)
                    .withName(targetTrail);
            try {
                try {
                    targetRegionClient = getClient(accountId, CLOUDTRAIL_CREATE_REGION, srContext.getMetricCollector(), AWSCloudTrailClient.class);
                }
                catch (Exception e) {
                    setError(detected, srContext.LOGTAG, "CloudTrail disabled", e);
                    return;
                }
                targetRegionClient.createTrail(createTrailRequest);
            } catch (S3BucketDoesNotExistException e) {
                logger.error(srContext.LOGTAG + "Please check property config CloudTrailDisabled > s3BucketToUse." +
                        "  Trying to create a CloudTrail with logging to s3 bucket '" + s3BucketName + "'." +
                        "  S3 bucket does not exist.");
                setError(detected, srContext.LOGTAG,
                        "Remediator cannot create a new CloudTrail because of a configuration issue in the Security Risk Detection Service (1)", e);
                return;
            } catch (InsufficientS3BucketPolicyException e) {
                logger.error(srContext.LOGTAG + "Please check property config CloudTrailDisabled > s3BucketToUse." +
                        "  Trying to create a CloudTrail with logging to s3 bucket '" + s3BucketName + "'." +
                        "  S3 bucket does not have proper bucket policy.");
                setError(detected, srContext.LOGTAG,
                        "Remediator cannot create a new CloudTrail because of a configuration issue in the Security Risk Detection Service (2)", e);
                return;
            }
        }

        StartLoggingRequest startLoggingRequest = new StartLoggingRequest().withName(targetTrail);
        targetRegionClient.startLogging(startLoggingRequest);

        if (createTrail) {
            setSuccess(detected, srContext.LOGTAG, "CloudTrail created for all regions in the account.");
        } else {
            setSuccess(detected, srContext.LOGTAG, "CloudTrail set to active.");
        }
    }
}
