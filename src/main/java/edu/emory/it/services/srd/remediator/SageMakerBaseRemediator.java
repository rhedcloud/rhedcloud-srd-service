package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.sagemaker.AmazonSageMaker;
import com.amazonaws.services.sagemaker.AmazonSageMakerClient;
import com.amazonaws.services.sagemaker.model.AmazonSageMakerException;
import com.amazonaws.services.sagemaker.model.DeleteModelRequest;
import com.amazonaws.services.sagemaker.model.DeleteNotebookInstanceRequest;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceRequest;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceResult;
import com.amazonaws.services.sagemaker.model.StopNotebookInstanceRequest;
import com.amazonaws.services.sagemaker.model.StopTrainingJobRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Random;

/**
 * Remediate by deleting or stopping the SageMaker resources.
 *
 * <p>SageMaker notebook instances and models are deleted.</p>
 * <p>SageMaker training jobs can not be deleted so they are stopped.</p>
 */
public class SageMakerBaseRemediator extends AbstractSecurityRiskRemediator {
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext, DetectionType detectionType) {
        if (remediatorPreConditionFailed(detected, "arn:aws:sagemaker:", srContext.LOGTAG, detectionType))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        // like arn:aws:sagemaker:us-east-1:123456789012:notebook-instance/name
        //   or arn:aws:sagemaker:us-east-1:123456789012:training-job/name
        //   or arn:aws:sagemaker:us-east-1:123456789012:model/name

        String region = arn[3];
        String resource = arn[5];

        arn = detected.getAmazonResourceName().split("/");
        if (arn.length != 2) {
            setError(detected, srContext.LOGTAG,
                    "Security risk input to remediator has incorrectly formatted ARN: " + detected.getAmazonResourceName());
            return;
        }


        final AmazonSageMaker sageMakerClient;
        try {
            sageMakerClient = getClient(accountId, region, srContext.getMetricCollector(), AmazonSageMakerClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "SageMaker resource '" + resource + "'", e);
            return;
        }

        if (resource.startsWith("notebook-instance/")) {
            String notebookInstanceName = arn[1];
            remediateNotebookInstance(sageMakerClient, notebookInstanceName, detected, srContext.LOGTAG);
        }
        else if (resource.startsWith("training-job/")) {
            String trainingJobName = arn[1];
            remediateTrainingJob(sageMakerClient, trainingJobName, detected, srContext.LOGTAG);
        }
        else if (resource.startsWith("model/")) {
            String modelName = arn[1];
            remediateModel(sageMakerClient, modelName, detected, srContext.LOGTAG);
        }
        else {
            setError(detected, srContext.LOGTAG,
                    "Do not know how to remediate resources like: " + detected.getAmazonResourceName());
        }
    }

    private void remediateNotebookInstance(AmazonSageMaker sageMakerClient, String notebookInstanceName,
                                           DetectedSecurityRisk detected, String LOGTAG) {
        DescribeNotebookInstanceRequest describeNotebookInstanceRequest = new DescribeNotebookInstanceRequest()
                .withNotebookInstanceName(notebookInstanceName);
        DescribeNotebookInstanceResult describeNotebookInstanceResult;

        Random random = new Random();
        boolean readyToDelete = false;
        long startedAt = System.currentTimeMillis();

        while (!readyToDelete) {
            // don't loop forever - no longer than 10 minutes
            if ((System.currentTimeMillis() - startedAt) > (1000 * 60 * 10)) {
                setError(detected, LOGTAG, "Waited too long for notebook instance (" + notebookInstanceName + ") to reach the proper state");
                return;
            }

            /*
             * in order to delete a notebook instance it has to be in the correct state.
             * manage those states and transitions here.
             *
             * it turns out that more than one instance of the detector can be tracking the same risk.
             * then, each remediator ends up being in lock-step with each other with regard to seeing the state changes.
             *
             * this ends up with all remediators breaking out of the loop on readyToDelete and each of them trying
             * to delete the notebook instance which results in all but one giving the error:
             *   "Unexpected error while deleting SageMaker notebook instance: Status (Deleting) not in ([Stopped, Failed])."
             *
             * it is also the case that they can all try to stop the notebook instance at the same time. giving the error:
             *   "Status (Stopping) not in ([InService]). Unable to transition to (Stopping) for Notebook Instance"
             *
             * to fix the problem, wait a random amount of time before getting the status.
             */
            try {
                Thread.sleep((random.nextInt(10) + 1) * 1000);
                describeNotebookInstanceResult = sageMakerClient.describeNotebookInstance(describeNotebookInstanceRequest);
            }
            catch (AmazonSageMakerException e) {
                if (e.getErrorCode().equals("ValidationException") && e.getErrorMessage().equals("RecordNotFound")) {
                    // odd situation.  the detector found a notebook instance at risk,
                    // but it's gone by the time we reach the remediation.  could flag it as an error
                    // or just say our job is done since there isn't a notebook instance at risk anymore.

                    setIgnoreRisk(detected, "SageMaker notebook instance " + notebookInstanceName + " is gone.");
                } else {
                    setError(detected, LOGTAG,
                            "Unexpected error while getting status of SageMaker notebook instance (" + notebookInstanceName + "): " + e.getMessage());
                }
                return;
            }
            catch (InterruptedException e) {
                setError(detected, LOGTAG,
                        "Interrupted while waiting for SageMaker notebook instance (" + notebookInstanceName + ") to transition to a new state", e);
                return;
            }

            String status = describeNotebookInstanceResult.getNotebookInstanceStatus();

            switch (status) {
                case "InService":
                    // notebook instances have to be stopped before deleting them
                    StopNotebookInstanceRequest stopNotebookInstanceRequest = new StopNotebookInstanceRequest()
                            .withNotebookInstanceName(notebookInstanceName);
                    try {
                        sageMakerClient.stopNotebookInstance(stopNotebookInstanceRequest);
                    } catch (AmazonSageMakerException e) {
                        // even though the randomized sleeps tried to prevent more than one remediator from stopping the
                        // notebook instance at the same time, it can still happen.  based on the exception message,
                        // if the notebook instance state appears to have changed, loop again to get an updated status.
                        if (e.getErrorCode().equals("ValidationException") && e.getErrorMessage().contains("Status (Stopping) not in")) {
                            break;
                        }
                        else {
                            setError(detected, LOGTAG,
                                    "Unexpected error while stopping SageMaker notebook instance (" + notebookInstanceName + ")", e);
                            return;
                        }
                    }
                    break;
                case "Failed":
                case "Stopped":
                    // failed and stopped notebook instances can be deleted
                    readyToDelete = true;
                    break;
                case "Pending":
                case "Deleting":
                case "Updating":
                case "Stopping":
                    // intermediate states so loop again to get the new status
                    break;
                default:
                    setError(detected, LOGTAG, "Unexpected SageMaker notebook instance (" + notebookInstanceName + ") status: " + status);
                    return;
            }
        }

        DeleteNotebookInstanceRequest deleteNotebookInstanceRequest = new DeleteNotebookInstanceRequest()
                .withNotebookInstanceName(notebookInstanceName);
        try {
            sageMakerClient.deleteNotebookInstance(deleteNotebookInstanceRequest);
            setSuccess(detected, LOGTAG, "SageMaker notebook instance " + notebookInstanceName + " deleted.");
        }
        catch (AmazonSageMakerException e) {
            // even though the randomized sleeps tried to prevent more than one remediator from deleting the
            // notebook instance at the same time, it can still happen.  don't raise an error since the goal
            // was to get rid of it and it no longer exists (or won't very soon) so consider the job done.
            if (e.getErrorCode().equals("ValidationException") && e.getErrorMessage().contains("does not exist")) {
                setIgnoreRisk(detected, "SageMaker notebook instance " + notebookInstanceName + " has already been deleted.");
            } else if (e.getErrorCode().equals("ValidationException") && e.getErrorMessage().contains("Status (Deleting) not in")) {
                setIgnoreRisk(detected, "SageMaker notebook instance " + notebookInstanceName + " is already being deleted.");
            } else {
                setError(detected, LOGTAG, "Unexpected error while deleting SageMaker notebook instance (" + notebookInstanceName + ")", e);
            }
        }
    }

    private void remediateTrainingJob(AmazonSageMaker sageMakerClient, String trainingJobName,
                                      DetectedSecurityRisk detected, String LOGTAG) {
        StopTrainingJobRequest stopTrainingJobRequest = new StopTrainingJobRequest()
                .withTrainingJobName(trainingJobName);
        try {
            sageMakerClient.stopTrainingJob(stopTrainingJobRequest);
            setSuccess(detected, LOGTAG, "SageMaker training job " + trainingJobName + " stopped.");
        }
        catch (AmazonSageMakerException e) {
            // the training job could have finished since the risk was detected.
            // Completed is an end state so there's no need to make it an error.
            if (e.getErrorCode().equals("ValidationException")
                    && e.getErrorMessage().equals("The request was rejected because the training job is in status Completed.")) {
                setIgnoreRisk(detected, "SageMaker training job " + trainingJobName + " already completed.");
            } else {
                setError(detected, LOGTAG, "Unexpected error while stopping SageMaker training job (" + trainingJobName + ")", e);
            }
        }
    }

    private void remediateModel(AmazonSageMaker sageMakerClient, String modelName,
                                DetectedSecurityRisk detected, String LOGTAG) {
        DeleteModelRequest deleteModelRequest = new DeleteModelRequest()
                .withModelName(modelName);
        try {
            sageMakerClient.deleteModel(deleteModelRequest);
            setSuccess(detected, LOGTAG, "SageMaker model " + modelName + " deleted.");
        }
        catch (AmazonSageMakerException e) {
            // more than one detector can be tracking the same risk so each remediator will try to delete the model.
            // if the model is gone then just ignore the risk since the remediation was going to be deleting it anyway.
            if (e.getErrorCode().equals("ValidationException") && e.getErrorMessage().contains("Could not find model")) {
                setIgnoreRisk(detected, "SageMaker model " + modelName + " has already been deleted.");
            } else {
                setError(detected, LOGTAG, "Unexpected error while deleting SageMaker model (" + modelName + ")", e);
            }
        }
    }
}
