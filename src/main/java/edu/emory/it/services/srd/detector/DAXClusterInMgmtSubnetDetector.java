package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.dax.AmazonDax;
import com.amazonaws.services.dax.AmazonDaxClient;
import com.amazonaws.services.dax.model.Cluster;
import com.amazonaws.services.dax.model.DescribeClustersRequest;
import com.amazonaws.services.dax.model.DescribeClustersResult;
import com.amazonaws.services.dax.model.DescribeSubnetGroupsRequest;
import com.amazonaws.services.dax.model.DescribeSubnetGroupsResult;
import com.amazonaws.services.dax.model.SubnetGroup;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * Detects DAX clusters configured to launch in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.DAXClusterInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class DAXClusterInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonDax dax;
            final AmazonEC2 ec2;
            try {
                dax = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonDaxClient.class);
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.DAXClusterInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> groupsInDisallowedSubnet = getSubnetGroupsInDisallowedSubnet(ec2, dax);
            detectClustersInDisallowedSubnet(dax, groupsInDisallowedSubnet, detection, srContext.LOGTAG);
        }
    }

    private Set<String> getSubnetGroupsInDisallowedSubnet(AmazonEC2 ec2, AmazonDax dax) {
        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

        DescribeSubnetGroupsRequest describeSubnetGroupsRequest = new DescribeSubnetGroupsRequest();
        boolean done = false;

        Set<String> groupsWithDisallowedSubnet = new HashSet<>();
        while (!done) {
            DescribeSubnetGroupsResult describeSubnetGroupsResult = dax.describeSubnetGroups(describeSubnetGroupsRequest);

            for (SubnetGroup subnetGroup : describeSubnetGroupsResult.getSubnetGroups()) {
                boolean hasDisallowedSubnet = subnetGroup.getSubnets().stream()
                        .anyMatch( item -> disallowedSubnets.contains(item.getSubnetIdentifier()) );
                if (hasDisallowedSubnet) {
                    groupsWithDisallowedSubnet.add(subnetGroup.getSubnetGroupName());
                }
            }

            describeSubnetGroupsRequest.setNextToken(describeSubnetGroupsResult.getNextToken());

            if (describeSubnetGroupsResult.getNextToken() == null) {
                done = true;
            }
        }

        return groupsWithDisallowedSubnet;
    }

    private void detectClustersInDisallowedSubnet(AmazonDax dax, Set<String> groupsInDisallowedSubnet,
                                                  SecurityRiskDetection detection, String LOGTAG) {
        DescribeClustersRequest describeClustersRequest = new DescribeClustersRequest();
        boolean done = false;

        while (!done) {
            DescribeClustersResult describeClustersResult = dax.describeClusters(describeClustersRequest);

            for (Cluster cluster : describeClustersResult.getClusters()) {
                if (cluster.getStatus().equals("deleting")) {
                    continue;
                }
                if (groupsInDisallowedSubnet.contains(cluster.getSubnetGroup())) {
                    addDetectedSecurityRisk(detection, LOGTAG, DetectionType.DAXClusterInMgmtSubnet, cluster.getClusterArn());
                }
            }

            describeClustersRequest.setNextToken(describeClustersResult.getNextToken());

            if (describeClustersResult.getNextToken() == null) {
                done = true;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "DAXClusterInMgmtSubnet";
    }
}
