package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentResourcesRequest;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentResourcesResult;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentsResult;
import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription;
import com.amazonaws.services.elasticbeanstalk.model.EnvironmentResourceDescription;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Detect Elastic Beanstalk environments in a disallowed subnet.<br>
 * If the environment is Terminated or Terminating then it is skipped since no actions can be performed on them.
 *
 * @see edu.emory.it.services.srd.remediator.ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AWSElasticBeanstalk ebsClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSElasticBeanstalkClient.class);
                final AmazonEC2 ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                final AmazonAutoScaling asClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonAutoScalingClient.class);

                // collect the IDs of the disallowed subnets
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);

                // for each of the Elastic Beanstalk environments
                // we'll get the environment resources which tell us about the Auto Scaling Groups.
                // those give us details about the subnets that the environment is allowed to run in.
                // the MGMT and Attachment subnets must not be used.

                DescribeEnvironmentsResult describeEnvironmentsResult = ebsClient.describeEnvironments();
                for (EnvironmentDescription environmentDescription : describeEnvironmentsResult.getEnvironments()) {
                    if (environmentDescription.getStatus().equals("Terminated")) {
                        // can't describe environment resources for terminated environments
                        continue;
                    }
                    if (environmentDescription.getStatus().equals("Terminating")) {
                        // can't remediate an environment that is terminating
                        continue;
                    }

                    DescribeEnvironmentResourcesRequest describeEnvironmentResourcesRequest
                            = new DescribeEnvironmentResourcesRequest()
                            .withEnvironmentName(environmentDescription.getEnvironmentName())
                            .withEnvironmentId(environmentDescription.getEnvironmentId());
                    DescribeEnvironmentResourcesResult describeEnvironmentResourcesResult
                            = ebsClient.describeEnvironmentResources(describeEnvironmentResourcesRequest);
                    EnvironmentResourceDescription environmentResourceDescription
                            = describeEnvironmentResourcesResult.getEnvironmentResources();

                    getAutoScalingGroupsLabel:
                    for (com.amazonaws.services.elasticbeanstalk.model.AutoScalingGroup autoScalingGroup
                            : environmentResourceDescription.getAutoScalingGroups()) {

                        String describeAutoScalingGroupsRequestNextToken = null;
                        do {
                            // results could be paginated so look for a token
                            DescribeAutoScalingGroupsRequest describeAutoScalingGroupsRequest
                                    = new DescribeAutoScalingGroupsRequest()
                                    .withAutoScalingGroupNames(autoScalingGroup.getName())
                                    .withMaxRecords(100);
                            if (describeAutoScalingGroupsRequestNextToken != null)
                                describeAutoScalingGroupsRequest.setNextToken(describeAutoScalingGroupsRequestNextToken);
                            DescribeAutoScalingGroupsResult describeAutoScalingGroupsResult
                                    = asClient.describeAutoScalingGroups(describeAutoScalingGroupsRequest);
                            describeAutoScalingGroupsRequestNextToken = describeAutoScalingGroupsResult.getNextToken();

                            for (com.amazonaws.services.autoscaling.model.AutoScalingGroup autoScalingGroup2
                                    : describeAutoScalingGroupsResult.getAutoScalingGroups()) {

                                // VPCZoneIdentifier
                                //
                                // The subnet identifier for the Amazon VPC connection, if applicable.
                                // You can specify several subnets in a comma-separated list.
                                // When you specify VPCZoneIdentifier with AvailabilityZones, ensure that the subnets'
                                //  Availability Zones match the values you specify for AvailabilityZones.

                                if (autoScalingGroup2.getVPCZoneIdentifier() != null
                                        && !autoScalingGroup2.getVPCZoneIdentifier().isEmpty()) {
                                    String[] VpcZoneIds = autoScalingGroup2.getVPCZoneIdentifier().split(",");
                                    for (String VpcZoneId : VpcZoneIds) {
                                        if (disallowedSubnets.contains(VpcZoneId)) {
                                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                                    DetectionType.ElasticBeanstalkEnvironmentWithinManagementSubnets,
                                                    environmentDescription.getEnvironmentArn(),
                                                    new SrdNameValuePair("environmentId", environmentDescription.getEnvironmentId()));
                                            break getAutoScalingGroupsLabel;
                                        }
                                    }
                                }
                            }
                        } while (describeAutoScalingGroupsRequestNextToken != null);
                    }
                }
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ElasticBeanstalkEnvironmentWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "ElasticBeanstalkEnvironmentWithinManagementSubnets";
    }
}
