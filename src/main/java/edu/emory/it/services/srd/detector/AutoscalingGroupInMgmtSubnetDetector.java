package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.List;
import java.util.Set;

/**
 * Detects Auto Scaling groups that are in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.AutoscalingGroupInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class AutoscalingGroupInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonAutoScaling asClient;
            final AmazonEC2 ec2;
            try {
                asClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonAutoScalingClient.class);
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.AutoscalingGroupInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

            DescribeAutoScalingGroupsRequest describeAutoScalingGroupsRequest = new DescribeAutoScalingGroupsRequest();

            boolean done = false;

            while (!done) {
                DescribeAutoScalingGroupsResult describeAutoScalingGroupsResult
                        = asClient.describeAutoScalingGroups(describeAutoScalingGroupsRequest);

                List<AutoScalingGroup> groups = describeAutoScalingGroupsResult.getAutoScalingGroups();

                for (AutoScalingGroup group : groups) {

                    if ("Delete in progress".equals(group.getStatus())) {
                        continue;
                    }
                    // VPCZoneIdentifier
                    //
                    // The subnet identifier for the Amazon VPC connection, if applicable.
                    // You can specify several subnets in a comma-separated list.
                    // When you specify VPCZoneIdentifier with AvailabilityZones, ensure that the subnets'
                    //  Availability Zones match the values you specify for AvailabilityZones.

                    if (group.getVPCZoneIdentifier() != null
                            && !group.getVPCZoneIdentifier().isEmpty()) {
                        String[] VpcZoneIds = group.getVPCZoneIdentifier().split(",");
                        for (String VpcZoneId : VpcZoneIds) {
                            if (disallowedSubnets.contains(VpcZoneId)) {
                                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                        DetectionType.AutoscalingGroupInMgmtSubnet, group.getAutoScalingGroupARN());
                                break;
                            }
                        }
                    }
                }

                describeAutoScalingGroupsRequest.setNextToken(describeAutoScalingGroupsResult.getNextToken());

                if (describeAutoScalingGroupsResult.getNextToken() == null) {
                    done = true;
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "AutoscalingGroupInMgmtSubnet";
    }
}
