package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient;
import com.amazonaws.services.elasticbeanstalk.model.TerminateEnvironmentRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by terminating the environment.
 *
 * <p>Security: Enforce, delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector the detector
 */
public class ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:elasticbeanstalk:", srContext.LOGTAG, DetectionType.ElasticBeanstalkEnvironmentWithinManagementSubnets))
            return;
        // like arn:aws:elasticbeanstalk:us-east-1:123456789012:environment/EB_Application_Name/EB_Environment_Name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];
        String environmentId = detected.getProperties().getProperty("environmentId");
        String environmentName = arn[5].split("/")[2];

        try {
            AWSElasticBeanstalk client = getClient(accountId, region, srContext.getMetricCollector(), AWSElasticBeanstalkClient.class);
            TerminateEnvironmentRequest terminateEnvironmentRequest = new TerminateEnvironmentRequest()
                    .withEnvironmentName(environmentName)
                    .withEnvironmentId(environmentId)
                    .withForceTerminate(true);
            client.terminateEnvironment(terminateEnvironmentRequest);

            setSuccess(detected, srContext.LOGTAG, "Environment " + environmentName + " terminated");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to terminate environment " + environmentName, e);
        }
    }
}
