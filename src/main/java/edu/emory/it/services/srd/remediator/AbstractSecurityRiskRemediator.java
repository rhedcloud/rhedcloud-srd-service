package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult;
import com.amazonaws.arn.Arn;
import com.amazonaws.auth.AWSCredentialsProvider;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.RemediationStatus;
import edu.emory.it.services.srd.exceptions.ConnectTimedOutException;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskAwsRegionsAndClientBuilder;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseFieldException;

/**
 * Super class for all remediators.
 */
public abstract class AbstractSecurityRiskRemediator extends SecurityRiskAwsRegionsAndClientBuilder {
    protected static final Logger logger = OpenEaiObject.logger;
    protected AppConfig appConfig;

    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        this.appConfig = aConfig;
        super.init(credentialsProvider, securityRole);
    }

    protected void setSuccess(DetectedSecurityRisk detected, String LOGTAG, String message) {
        newRemediationResult(LOGTAG, detected, RemediationStatus.REMEDIATION_SUCCESS, message, null);
    }

    protected void setError(DetectedSecurityRisk detected, String LOGTAG, String errDesc) {
        newRemediationResult(LOGTAG, detected, RemediationStatus.REMEDIATION_FAILED, "Remediation Error", errDesc);
    }

    protected void setError(DetectedSecurityRisk detected, String LOGTAG, String errDesc, Exception exception) {
        RemediationStatus remediationStatus = ConnectTimedOutException.isConnectTimedOutException(exception)
                ? RemediationStatus.REMEDIATION_TIMEOUT : RemediationStatus.REMEDIATION_FAILED;
        newRemediationResult(LOGTAG, detected, remediationStatus, "Remediation Error",
                errDesc + (exception==null ? "" : ". The exception is: " + exception.getMessage()));
    }

    protected void setNotificationOnly(DetectedSecurityRisk detected, String LOGTAG) {
        newRemediationResult(LOGTAG, detected, RemediationStatus.REMEDIATION_NOTIFICATION_ONLY, "Passive Remediation", null);
    }

    protected void setNoLongerRequired(DetectedSecurityRisk detected, String LOGTAG, String desc) {
        newRemediationResult(LOGTAG, detected, RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED, desc, null);
    }

    void setPostponed(DetectedSecurityRisk detected, String LOGTAG, String desc) {
        newRemediationResult(LOGTAG, detected, RemediationStatus.REMEDIATION_POSTPONED, desc, null);
    }

    public static void newRemediationResult(String LOGTAG, DetectedSecurityRisk detected,
                                            RemediationStatus remediationStatus, String description, String errDesc) {
        try {
            logger.info(LOGTAG + "Remediation status " + remediationStatus + (errDesc==null ? "" : (" with errDesc " + errDesc)) + " for desc " + description);

            RemediationResult remediationResult = detected.newRemediationResult();
            detected.setRemediationResult(remediationResult);

            remediationResult.setStatus(remediationStatus.getStatus());
            remediationResult.setDescription(description);
            if (errDesc != null)
                remediationResult.addError(errDesc);
        }
        catch (EnterpriseFieldException e) {
            logger.fatal(LOGTAG + "Error setting RemediationResult field for " + remediationStatus + " with errDesc " + errDesc
                    + ". The exception is: " + e.getMessage());
        }
    }

    void setIgnoreRisk(DetectedSecurityRisk detected, String desc) {
        detected.getProperties().setProperty("__ignored__", desc);
    }
    public static boolean isRiskIgnored(DetectedSecurityRisk detected) {
        return detected.getProperties().getProperty("__ignored__") != null;
    }
    public static String getRiskIgnoredReason(DetectedSecurityRisk detected) {
        return detected.getProperties().getProperty("__ignored__");
    }

    protected boolean remediatorPreConditionFailed(DetectedSecurityRisk detected, String arnPrefix, String LOGTAG,
                                                   DetectionType... detectionTypes) {
        String preconditionError;
        if (detected == null) {
            // programming error
            logger.error(LOGTAG + "Security risk input to the remediator is invalid: Detected risk is null");
            return true; // precondition check failed
        } else if (detected.getType() == null) {
            // detector error
            preconditionError = "Detected risk type is null";
        } else if (detected.getAmazonResourceName() == null) {
            // detector error
            preconditionError = "Detected ARN is null";
        } else if (!detected.getAmazonResourceName().startsWith(arnPrefix)) {
            // remediator not prepared to handle this kind of resource
            preconditionError = "Unexpected ARN at risk";
        } else if (detectionTypes == null || detectionTypes.length == 0) {
            // detection types are mandatory
            preconditionError = "At least one detection type is required";
        } else {
            boolean remediatorCompatible = false;
            for (DetectionType detectionType : detectionTypes) {
                if (detected.getType().equals(detectionType.name())) {
                    remediatorCompatible = true;
                    break;
                }
            }
            if (!remediatorCompatible) {
                // wrong remediator paired with detector
                preconditionError = "Remediator not compatible with risk";
            } else {
                // preconditions met
                preconditionError = null;
            }
        }

        if (preconditionError != null) {
            setError(detected, LOGTAG,
                    "Security risk input to the " + detected.getType() + " remediator is invalid: " + preconditionError);
            return true; // precondition check failed
        }

        return false;

    }

    protected String[] remediatorCheckResourceName(DetectedSecurityRisk detected, int nbrComponents,
                                                   String accountId, int accountIdIdx, String LOGTAG) {
    	logger.info(LOGTAG+"detected.getAmazonResourceName()="+detected.getAmazonResourceName());
        String[] arn = detected.getAmazonResourceName().split(":");

        if (arn.length != nbrComponents) {
            setError(detected, LOGTAG, "Security risk input to the " + detected.getType() + " remediator has incorrectly formatted ARN: "
                    + detected.getAmazonResourceName());
            return null;
        }
        if (accountId != null && !accountId.equals(arn[accountIdIdx])) {
            setError(detected, LOGTAG, "Security risk input to the " + detected.getType() + " remediator has mis-matched accounts: "
                    + "expected: " + accountId + " found: " + arn[accountIdIdx]);
            return null;
        }

        return arn;
    }

    protected Arn remediatorCheckResourceName(DetectedSecurityRisk detected, String accountId, String LOGTAG) {
        Arn arn;
        try {
            arn = Arn.fromString(detected.getAmazonResourceName());
        }
        catch (IllegalArgumentException e) {
            setError(detected, LOGTAG, "Security risk input to the " + detected.getType() + " remediator has "
                    + e.getMessage() + " for " + detected.getAmazonResourceName());
            return null;
        }

        if (accountId != null && !accountId.equals(arn.getAccountId())) {
            setError(detected, LOGTAG, "Security risk input to the " + detected.getType() + " remediator has mis-matched accounts: "
                    + "expected: " + accountId + " found: " + arn.getAccountId());
            return null;
        }
        
        if ("role".equals(arn.getResource().getResourceType()) && arn.getResourceAsString().contains("/")) {
        	
        }

        return arn;
    }
}
