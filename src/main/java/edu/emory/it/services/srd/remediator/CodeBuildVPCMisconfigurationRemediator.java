package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates CodeBuild project that is missing a vpc<br>
 * <p>The remediator deletes the project</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.CodeBuildVPCMisconfigurationDetector the detector
 */
public class CodeBuildVPCMisconfigurationRemediator extends CodeBuildBaseRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        remediate(accountId, detected, srContext, DetectionType.CodeBuildVPCMisconfigurationMissingVPC);
    }
}
