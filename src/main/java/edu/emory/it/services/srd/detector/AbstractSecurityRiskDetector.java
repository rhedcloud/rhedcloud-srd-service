package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult;
import com.amazonaws.auth.AWSCredentialsProvider;
import edu.emory.it.services.srd.ComplianceClass;
import edu.emory.it.services.srd.DetectionStatus;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.ConnectTimedOutException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.SecurityRiskAwsRegionsAndClientBuilder;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import org.apache.logging.log4j.Logger;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;

import java.lang.annotation.Annotation;

/**
 * Super class for all detectors.
 */
public abstract class AbstractSecurityRiskDetector extends SecurityRiskAwsRegionsAndClientBuilder {
    protected static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(AbstractSecurityRiskDetector.class);

    private static final long FREQUENCY_BACKOFF_DEFAULT = 0L;

    protected AppConfig appConfig;
    protected String additionalNotificationText;
    protected long frequencyBackoff;

    /**
     * Initialize the detector.
     *
     * @param aConfig, an AppConfig object with all this detector needs.
     * @param credentialsProvider AWS credentials
     * @param securityRole AWS role to assume
     * @throws SecurityRiskDetectionException with details of the initialization error.
     */
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
        this.appConfig = aConfig;
        super.init(credentialsProvider, securityRole);
        setupCommonProperties();
    }

    abstract public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext);
    abstract public String getBaseName();

    public long getFrequencyBackoff() {
        return frequencyBackoff;
    }

    private void setupCommonProperties() {
        String LOGTAG = "[" + getBaseName() + "] ";

        Object o = appConfig.getObjects().get(getBaseName().toLowerCase());
        if (o == null)
            return;
        if (!(o instanceof PropertyConfig))
            return;

        PropertyConfig propertyConfig = (PropertyConfig) o;
        setupAdditionalNotificationText(propertyConfig, LOGTAG);
        setupFrequencyBackoff(propertyConfig, LOGTAG);
    }

    /**
     * Notification messages can provide additional information that is configured in the app config.
     * It is optional for any detector so don't throw exceptions accessing it.
     *
     * @param propertyConfig property config
     * @param LOGTAG logtag
     */
    private void setupAdditionalNotificationText(PropertyConfig propertyConfig, String LOGTAG) {
        String rawText = propertyConfig.getProperties().getProperty("AdditionalNotificationText");
        if (rawText == null) {
            logger.info(LOGTAG + " AdditionalNotificationText=" + this.additionalNotificationText + " (no property, use default)");
            return;
        }

        // the text will be wrapped across lines so strip all the extra whitespace out
        this.additionalNotificationText = stripExtraWhitespace(rawText);

        int len = Math.min(20, this.additionalNotificationText.length());
        logger.info(LOGTAG + " AdditionalNotificationText=" + additionalNotificationText.substring(0, len) + " ...");
    }

    /**
     * Frequency backoff provides a way to delay the start of the next detector execution.
     * It is optional for any detector so don't throw exceptions accessing it.
     *
     * @param propertyConfig property config
     * @param LOGTAG logtag
     */
    private void setupFrequencyBackoff(PropertyConfig propertyConfig, String LOGTAG) {
        String propertyValue = propertyConfig.getProperties().getProperty("FrequencyBackoff");
        if (propertyValue == null) {
            this.frequencyBackoff = FREQUENCY_BACKOFF_DEFAULT;
            logger.info(LOGTAG + "FrequencyBackoff=" + this.frequencyBackoff + " seconds (no property, use default)");
            return;
        }

        // the text can be wrapped across lines so strip all the extra whitespace out
        propertyValue = stripExtraWhitespace(propertyValue);

        if (propertyValue.isEmpty()) {
            this.frequencyBackoff = FREQUENCY_BACKOFF_DEFAULT;
            logger.info(LOGTAG + " FrequencyBackoff=" + this.frequencyBackoff + " seconds (empty property, use default)");
        }
        else {
            try {
                this.frequencyBackoff = Long.parseLong(propertyValue);
                if (this.frequencyBackoff < 0) {
                    this.frequencyBackoff = FREQUENCY_BACKOFF_DEFAULT;
                    logger.info(LOGTAG + " FrequencyBackoff=" + this.frequencyBackoff + " seconds (range error, use default)");
                }
                else {
                    logger.info(LOGTAG + " FrequencyBackoff=" + this.frequencyBackoff + " seconds");
                }
            }
            catch (NumberFormatException e) {
                this.frequencyBackoff = FREQUENCY_BACKOFF_DEFAULT;
                logger.info(LOGTAG + " FrequencyBackoff=" + this.frequencyBackoff + " seconds (format error, use default)");
            }
        }
    }

    private String stripExtraWhitespace(String rawText) {
        rawText = rawText.trim();
        rawText = rawText.replaceAll("\\t", " ");
        rawText = rawText.replaceAll(" *\\n *", " ");
        rawText = rawText.replaceAll(" *\\r *", " ");
        return rawText;
    }

    void setDetectionSuccess(SecurityRiskDetection detection, DetectionType detectionType, String LOGTAG) {
        newDetectionResult(LOGTAG, detection, detectionType.name(), DetectionStatus.DETECTION_SUCCESS, null, null);
    }

    void setDetectionError(SecurityRiskDetection detection, DetectionType detectionType, String LOGTAG, String errDesc) {
        newDetectionResult(LOGTAG, detection, detectionType.name(), DetectionStatus.DETECTION_FAILED, errDesc, null);
    }

    void setDetectionError(SecurityRiskDetection detection, DetectionType detectionType, String LOGTAG, String errDesc, Exception exception) {
        DetectionStatus detectionStatus = ConnectTimedOutException.isConnectTimedOutException(exception)
                ? DetectionStatus.DETECTION_TIMEOUT : DetectionStatus.DETECTION_FAILED;
        newDetectionResult(LOGTAG, detection, detectionType.name(), detectionStatus,
                errDesc + (exception==null ? "" : ". The exception is: " + exception.getMessage()), null);
    }

    protected void addDetectedSecurityRisk(SecurityRiskDetection detection, String LOGTAG,
                                           DetectionType detectionType, String arn,
                                           SrdNameValuePair... properties) {
    	
        DetectionStatus detectionStatus = DetectionStatus.DETECTION_SUCCESS;
        String msg = "arn " + arn;

        try {
            DetectedSecurityRisk risk = detection.newDetectedSecurityRisk();
            detection.addDetectedSecurityRisk(risk);

            risk.setType(detectionType.name());
            risk.setAmazonResourceName(arn);

            for (SrdNameValuePair nvp : properties) {
                risk.getProperties().setProperty(nvp.getName(), nvp.getValue());
                msg += " (" + nvp.getName() + "=" + nvp.getValue() + ")";
            }
            
            //TODO: add AdditionalNotificationText to risk.setAdditionalNotificationText

            newDetectionResult(LOGTAG, detection, detectionType.name(), detectionStatus, null, msg);
        }
        catch (EnterpriseFieldException e) {
            // overwrite the DETECTION_SUCCESS status that may have been set above
        	e.printStackTrace();
            String errDesc = "Error setting DetectedSecurityRisk field for " + detectionStatus + " with arn " + arn
                    + ". The exception is: " + e.getMessage();
            newDetectionResult(LOGTAG, detection, detectionType.name(), DetectionStatus.DETECTION_FAILED, errDesc, null);
        }
    }

    public void newDetectionResult(String LOGTAG, SecurityRiskDetection detection, String detectionType,
                                   DetectionStatus detectionStatus, String errDesc, String msg) {

        try {
            logger.info(LOGTAG + "Detection status " + detectionStatus + " for type " + detectionType
                    + (errDesc==null ? "" : (" with errDesc " + errDesc + "!")) 
                    + ((msg==null ? "" : (" with detected risk " + msg))));

            DetectionResult detectionResult = detection.newDetectionResult();
            detection.setDetectionResult(detectionResult);

            detectionResult.setType(detectionType);
            detectionResult.setStatus(detectionStatus.getStatus());
            if (errDesc != null)
                detectionResult.addError(errDesc);
            //TODO: delete legacy code
            //legacy code
            if (additionalNotificationText != null)
                detectionResult.addError("ANT:" + additionalNotificationText); // find ANT: in SrdAccountNotificationCommand
        }
        catch (EnterpriseFieldException e) {
            logger.fatal(LOGTAG + "Error setting DetectionResult field for " + detectionStatus + " with errDesc " + errDesc
                    + ". The exception is: " + e.getMessage());
        }
    }

    public boolean isStandardComplianceClass() {
        return isDetectorInComplianceClass(StandardComplianceClass.class);
    }

    public boolean isHipaaComplianceClass() {
        return isDetectorInComplianceClass(HIPAAComplianceClass.class);
    }

    public boolean isEnhancedSecurityComplianceClass() {
        return isDetectorInComplianceClass(EnhancedSecurityComplianceClass.class);
    }

    public boolean isDetectorRelevantToAccount(ComplianceClass accountComplianceClass) {
        return isDetectorInComplianceClass(accountComplianceClass.clazz);
    }

    private boolean isDetectorInComplianceClass(Class<?> complianceClass) {
        Annotation[] annotations = this.getClass().getAnnotations();
        for (Annotation annotation : annotations) {
            if (complianceClass.equals(annotation.annotationType())) {
                return true;
            }
        }
        return false;
    }
}
