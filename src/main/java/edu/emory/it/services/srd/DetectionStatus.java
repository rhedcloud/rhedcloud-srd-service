package edu.emory.it.services.srd;

public enum DetectionStatus {
    DETECTION_SUCCESS("Detection Success"),
    DETECTION_FAILED("Detection Failed"),
    DETECTION_TIMEOUT("Detection Timeout");

    private final String status;

    DetectionStatus(String status) {
        this.status = status;
    }

    public String getStatus() { return status; }
}
