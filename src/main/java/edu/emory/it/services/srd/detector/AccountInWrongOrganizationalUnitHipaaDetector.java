package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import edu.emory.it.services.srd.ComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detect if the account is not in the HIPAA AWS Organization when it should be.<br>
 *
 * @see edu.emory.it.services.srd.remediator.AccountInWrongOrganizationalUnitHipaaRemediator the remediator
 */
@HIPAAComplianceClass
public class AccountInWrongOrganizationalUnitHipaaDetector extends AccountInWrongOrganizationalUnitBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, ComplianceClass.HIPAA);
    }

    @Override
    public String getBaseName() {
        return "AccountInWrongOrganizationalUnitHipaa";
    }
}
