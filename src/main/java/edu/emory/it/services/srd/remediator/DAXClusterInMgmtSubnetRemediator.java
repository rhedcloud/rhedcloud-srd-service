package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.dax.AmazonDax;
import com.amazonaws.services.dax.AmazonDaxClient;
import com.amazonaws.services.dax.model.Cluster;
import com.amazonaws.services.dax.model.ClusterNotFoundException;
import com.amazonaws.services.dax.model.DeleteClusterRequest;
import com.amazonaws.services.dax.model.DescribeClustersRequest;
import com.amazonaws.services.dax.model.DescribeClustersResult;
import com.amazonaws.services.dax.model.DescribeSubnetGroupsRequest;
import com.amazonaws.services.dax.model.DescribeSubnetGroupsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Remediates DAX clusters that are in a disallowed subnet<br>
 *
 * <p>The remediator deletes the cluster that is launched into a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.DAXClusterInMgmtSubnetDetector the detector
 */
public class DAXClusterInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:dax:", srContext.LOGTAG, DetectionType.DAXClusterInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String clusterName = arn[5].replace("cache/", "");
        String region = arn[3];

        final AmazonDax dax;
        final AmazonEC2 ec2;
        try {
            dax = getClient(accountId, region, srContext.getMetricCollector(), AmazonDaxClient.class);
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Cluster '" + clusterName + "'", e);
            return;
        }

        DescribeClustersRequest clustersRequest = new DescribeClustersRequest().withClusterNames(clusterName);
        DescribeClustersResult clustersResult = null;
        try {
            clustersResult = dax.describeClusters(clustersRequest);
        } catch (ClusterNotFoundException e) {
            //ignore
        }

        if (clustersResult == null || clustersResult.getClusters() == null || clustersResult.getClusters().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Cluster '" + clusterName + "' cannot be found. It may have already been deleted.");
            return;
        }

        Cluster cluster =  clustersResult.getClusters().get(0);

        if (cluster.getStatus().equals("deleting")) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Cluster '" + clusterName + "' is already deleting. It does not need to be remediate.");
            return;
        }

        if (cluster.getSubnetGroup() == null) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Cluster '" + clusterName + "' does not belong to a subnet group. It does not need to be remediate.");
            return;
        }

        DescribeSubnetGroupsRequest describeSubnetGroupsRequest = new DescribeSubnetGroupsRequest().withSubnetGroupNames(cluster.getSubnetGroup());
        DescribeSubnetGroupsResult describeSubnetGroupsResult = dax.describeSubnetGroups(describeSubnetGroupsRequest);

        if (describeSubnetGroupsResult.getSubnetGroups() == null || describeSubnetGroupsResult.getSubnetGroups().isEmpty()) {
            setError(detected, srContext.LOGTAG,
                    "Subnet group '" + cluster.getSubnetGroup() + "' is empty. Cluster '" + clusterName + "' cannot be remediated.");
            return;
        }

        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);
        boolean found = describeSubnetGroupsResult.getSubnetGroups().get(0).getSubnets().stream().anyMatch(item -> disallowedSubnets.contains(item.getSubnetIdentifier()));

        if (!found) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Subnet group '" + cluster.getSubnetGroup() + "' is not in a disallowed subnet. Cluster '" + clusterName + "' does not need to be remediated.");
            return;
        }


        //remediate:
        DeleteClusterRequest deleteClusterRequest = new DeleteClusterRequest().withClusterName(clusterName);
        dax.deleteCluster(deleteClusterRequest);

        setSuccess(detected, srContext.LOGTAG, "The cluster '" + clusterName + "' has been deleted.");
    }
}
