package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Detects EC2 instances that are in disallowed subnet and does not have a tag of RHEDcloud = true<br>
 *
 * @see edu.emory.it.services.srd.remediator.EC2CustomerInstancesInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EC2CustomerInstancesInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonEC2 ec2;
            try {
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.EC2CustomerInstancesInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);
            DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withFilters(new Filter("subnet-id", new ArrayList<>(disallowedSubnets)));
            String arnPrefix = "arn:aws:ec2:" + region.getName() + ":" + accountId + ":" + "instance/";
            boolean done = false;

            while (!done) {
                DescribeInstancesResult describeInstancesResult = ec2.describeInstances(describeInstancesRequest);

                List<Reservation> reservations = describeInstancesResult.getReservations();

                for (Reservation reservation : reservations) {

                    for (Instance instance : reservation.getInstances()) {

                        String instanceState = instance.getState().getName();
                        if (instanceState.equals("shutting-down") || instanceState.equals("terminated")) {
                            continue;
                        }

                        boolean isRhedCloud = instance.getTags()
                                .stream()
                                .anyMatch(tag -> tag.getKey().equalsIgnoreCase("RHEDcloud") && tag.getValue().equalsIgnoreCase("true"));
                        if (!isRhedCloud) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.EC2CustomerInstancesInMgmtSubnet, arnPrefix + instance.getInstanceId());
                        }
                    }
                }

                describeInstancesRequest.setNextToken(describeInstancesResult.getNextToken());

                if (describeInstancesResult.getNextToken() == null) {
                    done = true;
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "EC2CustomerInstancesInMgmtSubnet";
    }
}
