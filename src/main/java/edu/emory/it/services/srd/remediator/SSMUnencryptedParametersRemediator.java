package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClient;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParameterResult;
import com.amazonaws.services.simplesystemsmanagement.model.Parameter;
import com.amazonaws.services.simplesystemsmanagement.model.ParameterNotFoundException;
import com.amazonaws.services.simplesystemsmanagement.model.PutParameterRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates unencrypted Systems Manager Parameter Store.<br>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, encryption. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.SSMUnencryptedParametersDetector the detector
 */
public class SSMUnencryptedParametersRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ssm:", srContext.LOGTAG, DetectionType.SSMUnencryptedParameters))
            return;
        // like arn:aws:ssm:us-east-1:123456789012:parameter/parameter_name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String parameterName = arn[5].substring(arn[5].indexOf("/") + 1);
        String region = arn[3];

        final AWSSimpleSystemsManagement ssm;
        try {
            ssm = getClient(accountId, region, srContext.getMetricCollector(), AWSSimpleSystemsManagementClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Systems Manager parameter " + parameterName, e);
            return;
        }

        // get parameter details
        GetParameterRequest getParameterRequest = new GetParameterRequest().withName(parameterName);
        GetParameterResult getParameterResult = ssm.getParameter(getParameterRequest);
        Parameter parameter;
        try {
            parameter = getParameterResult.getParameter();
        }
        catch (ParameterNotFoundException e) {
            // strange, but the parameter no longer exists - nothing left to do
            setIgnoreRisk(detected, "Systems Manager Parameter " + parameterName + " no longer exists.");
            return;
        }
        if (parameter.getType().equals("SecureString")) {
            // strange, but the parameter is already encrypted - nothing left to do
            setIgnoreRisk(detected, "Systems Manager parameter " + parameterName + " is already encrypted.");
            return;
        }

        // encrypt the parameter
        PutParameterRequest putParameterRequest = new PutParameterRequest()
                .withOverwrite(true)
                .withType("SecureString")
                .withName(parameter.getName())
                .withValue(parameter.getValue());
        ssm.putParameter(putParameterRequest);

        setSuccess(detected, srContext.LOGTAG, "Successfully encrypted the Systems Manager parameter.");
    }
}
