package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentManagedActionsRequest;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentManagedActionsResult;
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentsResult;
import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription;
import com.amazonaws.services.elasticbeanstalk.model.ManagedAction;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import org.openeai.config.AppConfig;
import org.openeai.config.PropertyConfig;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Detect Elastic Beanstalk instances that do not have "Managed Updates" enabled.<br>
 * If the environment is in certain states (Terminated, Terminating or health status Suspended)
 * then it is skipped since no actions can be performed on them.
 * <br>
 * The detector can be setup to ignore specific accounts by setting
 * the ElasticBeanstalkDisabledManagedUpdates/whitelistedAccounts property to a comma-separated list of account ids.
 *
 * @see edu.emory.it.services.srd.remediator.ElasticBeanstalkDisabledManagedUpdatesRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ElasticBeanstalkDisabledManagedUpdatesDetector extends AbstractSecurityRiskDetector {
    private final Set<String> whitelistedAccounts = new HashSet<>();

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
        super.init(aConfig, credentialsProvider, securityRole);

        // whitelist is optional
        Object o = appConfig.getObjects().get(getBaseName().toLowerCase());
        if (o instanceof PropertyConfig) {
            String property = ((PropertyConfig) o).getProperties().getProperty("whitelistedAccounts");
            if (property != null && !property.isEmpty()) {
                String[] accountIds = property.replaceAll("[\n\t\r ]", "").split(",");
                for (String accountId : accountIds)
                    if (!accountId.isEmpty())
                        this.whitelistedAccounts.add(accountId);
            }
        }
    }

    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        // do the whitelist check up front instead of only when a risk is found (like other detectors)
        // because ElasticBeanstalk has a lowish throttle limit on API calls so best to avoid them if possible
        if (whitelistedAccounts.contains(accountId)) {
            logger.info(srContext.LOGTAG + "Ignoring whitelisted account " + accountId + ".");
            setDetectionSuccess(detection, DetectionType.ElasticBeanstalkDisabledManagedUpdates, srContext.LOGTAG);
            return;
        }

        for (Region region : getRegions()) {
            final AWSElasticBeanstalk client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSElasticBeanstalkClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ElasticBeanstalkDisabledManagedUpdates,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            DescribeEnvironmentsResult describeEnvironmentsResult;
            try {
                describeEnvironmentsResult = client.describeEnvironments();
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ElasticBeanstalkDisabledManagedUpdates,
                        srContext.LOGTAG, "Error describing environments in region " + region.getName(), e);
                return;
            }

            for (EnvironmentDescription environmentDescription : describeEnvironmentsResult.getEnvironments()) {
                if (environmentDescription.getStatus().equals("Terminated")) {
                    // can't describe managed actions for terminated environments
                    continue;
                }
                if (environmentDescription.getStatus().equals("Terminating")) {
                    // can't remediate an environment that is terminating
                    continue;
                }
                if (environmentDescription.getHealthStatus().equals("Suspended")) {
                    // can't remediate an environment that has Suspended health status
                    continue;
                }

                // https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environment-platform-update-managed.html
                DescribeEnvironmentManagedActionsRequest describeEnvironmentManagedActionsRequest = new DescribeEnvironmentManagedActionsRequest()
                        .withEnvironmentName(environmentDescription.getEnvironmentName())
                        .withEnvironmentId(environmentDescription.getEnvironmentId());
                DescribeEnvironmentManagedActionsResult describeEnvironmentManagedActionsResult;
                try {
                    describeEnvironmentManagedActionsResult = client.describeEnvironmentManagedActions(describeEnvironmentManagedActionsRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.ElasticBeanstalkDisabledManagedUpdates,
                            srContext.LOGTAG, "Error describing environment managed actions for " + environmentDescription.getEnvironmentId(), e);
                    return;
                }
                List<ManagedAction> managedActions = describeEnvironmentManagedActionsResult.getManagedActions();

                if (managedActions == null || managedActions.size() == 0) {
                    addDetectedSecurityRisk(detection, srContext.LOGTAG,
                            DetectionType.ElasticBeanstalkDisabledManagedUpdates, environmentDescription.getEnvironmentArn(),
                            new SrdNameValuePair("environmentId", environmentDescription.getEnvironmentId()));
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "ElasticBeanstalkDisabledManagedUpdates";
    }
}
