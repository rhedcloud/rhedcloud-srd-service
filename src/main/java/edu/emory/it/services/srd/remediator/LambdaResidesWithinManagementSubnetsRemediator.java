package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.DeleteFunctionRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by deleting the Lambda function.
 *
 * <p>Security: Enforce, delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.LambdaResidesWithinManagementSubnetsDetector the detector
 */
public class LambdaResidesWithinManagementSubnetsRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:lambda:", srContext.LOGTAG, DetectionType.LambdaResidesWithinManagementSubnets))
            return;
        String[] arn = remediatorCheckResourceName(detected, 7, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];


        try {
            AWSLambda lambdaClient = getClient(accountId, region, srContext.getMetricCollector(), AWSLambdaClient.class);
            DeleteFunctionRequest deleteFunctionRequest = new DeleteFunctionRequest()
                    .withFunctionName(detected.getAmazonResourceName());
            lambdaClient.deleteFunction(deleteFunctionRequest);

            setSuccess(detected, srContext.LOGTAG, "Lambda function deleted");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to delete Lambda function", e);
        }
    }
}
