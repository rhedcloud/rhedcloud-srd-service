package edu.emory.it.services.srd.exceptions;

public class SecurityRiskRemediationException extends Exception {
    public SecurityRiskRemediationException(String message) {
        super(message);
    }

    public SecurityRiskRemediationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityRiskRemediationException(Throwable cause) {
        super(cause);
    }
}
