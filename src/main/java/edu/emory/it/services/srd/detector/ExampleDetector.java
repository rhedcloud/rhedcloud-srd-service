package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Example detector for the test suite.
 *
 * @see edu.emory.it.services.srd.remediator.ExampleRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ExampleDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                DetectionType.Example, "arn:aws:fake:region:" + accountId + ":example");
    }

    @Override
    public String getBaseName() {
        return "Example";
    }
}
