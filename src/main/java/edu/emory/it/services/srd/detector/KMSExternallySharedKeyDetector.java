package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.kms.model.DescribeKeyRequest;
import com.amazonaws.services.kms.model.DescribeKeyResult;
import com.amazonaws.services.kms.model.GetKeyPolicyRequest;
import com.amazonaws.services.kms.model.GetKeyPolicyResult;
import com.amazonaws.services.kms.model.KeyListEntry;
import com.amazonaws.services.kms.model.ListKeysRequest;
import com.amazonaws.services.kms.model.ListKeysResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Collections;
import java.util.Set;

/**
 * Detects externally shared KMS keys<br>
 *
 * <p>The logic for check if a principal is external goes as follows, if any of the follow is true, the principal is external</p>
 * <ul>
 *     <li>principal is federated</li>
 *     <li>not principal is used</li>
 *     <li>principal = *</li>
 *     <li>principal arn starts with arn:aws:iam: and the account id is an external account</li>
 *     <li>principal arn starts with arn:aws:sts: and the account id is an external account</li>
 * </ul>
 *
 * @see edu.emory.it.services.srd.remediator.KMSExternallySharedKeyRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class KMSExternallySharedKeyDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            AWSKMS kms;
            try {
                kms = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSKMSClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.KMSExternallySharedKey,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> whitelistedAccounts = Collections.singleton(accountId);

            ListKeysRequest listKeysRequest = new ListKeysRequest();
            ListKeysResult listKeysResult;
            do {
                try {
                    listKeysResult = kms.listKeys(listKeysRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.KMSExternallySharedKey, srContext.LOGTAG, "Error listing keys", e);
                    return;
                }

                for (KeyListEntry keyListEntry : listKeysResult.getKeys()) {
    				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
    						.isExemptResourceArnForAccountAndDetector(keyListEntry.getKeyArn(),accountId,getBaseName());
    				boolean exceptionStatusUnderminable = resourceIsExempt == null;
    				if (exceptionStatusUnderminable) {
    					logger.error(srContext.LOGTAG + "Exemption status of key " + keyListEntry.getKeyArn() + " can not be determined on account " + accountId);
    					continue;					
    				}
					if (resourceIsExempt) {
						logger.info(srContext.LOGTAG + "Ignoring exempted key " + keyListEntry.getKeyArn() + " on account " + accountId);
						continue;
					}
                	
                    DescribeKeyRequest describeKeyRequest = new DescribeKeyRequest()
                            .withKeyId(keyListEntry.getKeyId());
                    DescribeKeyResult describeKeyResult;
                    try {
                        describeKeyResult = kms.describeKey(describeKeyRequest);
                    }
                    catch (Exception e) {
                        setDetectionError(detection, DetectionType.KMSExternallySharedKey, srContext.LOGTAG, "Error describing key", e);
                        return;
                    }

                    // only customer managed keys are considered
                    if (describeKeyResult.getKeyMetadata().getKeyManager().equals("CUSTOMER")) {
                        GetKeyPolicyRequest getKeyPolicyRequest = new GetKeyPolicyRequest()
                                .withKeyId(keyListEntry.getKeyId())
                                .withPolicyName("default");
                        GetKeyPolicyResult getKeyPolicyResult;
                        try {
                            getKeyPolicyResult = kms.getKeyPolicy(getKeyPolicyRequest);
                        }
                        catch (Exception e) {
                            setDetectionError(detection, DetectionType.KMSExternallySharedKey, srContext.LOGTAG, "Error getting key policy", e);
                            return;
                        }

                        String policy = getKeyPolicyResult.getPolicy();
                        if (AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy)) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.KMSExternallySharedKey, keyListEntry.getKeyArn());
                        }
                    }
                }

                listKeysRequest.setMarker(listKeysResult.getNextMarker());
            } while (listKeysResult.getNextMarker() != null);
        }
    }

    @Override
    public String getBaseName() {
        return "KMSExternallySharedKey";
    }
}
