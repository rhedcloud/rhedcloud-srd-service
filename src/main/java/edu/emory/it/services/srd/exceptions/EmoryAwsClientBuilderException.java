package edu.emory.it.services.srd.exceptions;

public class EmoryAwsClientBuilderException extends Exception {
    public EmoryAwsClientBuilderException(String message) {
        super(message);
    }

    public EmoryAwsClientBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmoryAwsClientBuilderException(Throwable cause) {
        super(cause);
    }
}
