package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.redshift.AmazonRedshift;
import com.amazonaws.services.redshift.AmazonRedshiftClient;
import com.amazonaws.services.redshift.model.Cluster;
import com.amazonaws.services.redshift.model.CreateClusterParameterGroupRequest;
import com.amazonaws.services.redshift.model.DescribeClustersRequest;
import com.amazonaws.services.redshift.model.DescribeClustersResult;
import com.amazonaws.services.redshift.model.ModifyClusterParameterGroupRequest;
import com.amazonaws.services.redshift.model.ModifyClusterRequest;
import com.amazonaws.services.redshift.model.Parameter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Locale;

/**
 * Enable encryption in transit for the Redshift cluster.<br>
 * See <a href="https://docs.aws.amazon.com/redshift/latest/mgmt/security-encryption-in-transit.html">Encryption in transit</a>.<br>
 *
 * A new Redshift parameter group will be created with the require_ssl parameter set to "true".<br>
 * It will be named "in-transit-encryption-[YMDhms]-[random]" and the cluster will be modified to use it.
 * The cluster will not be rebooted so the new parameter group will not take effect until then.
 *
 * @see edu.emory.it.services.srd.detector.RedshiftSSLDisabledDetector the detector
 */
public class RedshiftSSLDisabledRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    /** see https://docs.aws.amazon.com/redshift/latest/mgmt/working-with-parameter-groups.html */
    public static final String REDSHIFT_PARAMETER_GROUP_FAMILY = "redshift-1.0";
    public static final String PARAMETER_GROUP_NAME_PREFIX = "in-transit-encryption";

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:redshift:", srContext.LOGTAG,
                DetectionType.RedshiftSSLDisabled, DetectionType.RedshiftPendingReboot))
            return;
        // like arn:aws:redshift:us-east-1:123456789012:cluster:cluster-name
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null)
            return;

        /*
         * see comments in the detector, but basically, the information about in-transit encryption
         * isn't available until the cluster has been rebooted
         */
        if (detected.getType().equals(DetectionType.RedshiftPendingReboot.name())) {
            setNotificationOnly(detected, srContext.LOGTAG);
            return;
        }

        // enable in transit encryption
        AmazonRedshift client;
        try {
            client = getClient(accountId, arn.getRegion(), srContext.getMetricCollector(), AmazonRedshiftClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error getting Redshift client");
            return;
        }

        DescribeClustersRequest describeClustersRequest = new DescribeClustersRequest()
                .withClusterIdentifier(arn.getResource().getResource());
        DescribeClustersResult describeClustersResult;
        Cluster theCluster = null;

        findTheCluster:
        do {
            try {
                describeClustersResult = client.describeClusters(describeClustersRequest);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Error describing the Redshift cluster", e);
                return;
            }
            for (Cluster cluster : describeClustersResult.getClusters()) {
                if (cluster.getClusterIdentifier().equals(arn.getResource().getResource())) {
                    theCluster = cluster;
                    break findTheCluster;
                }
            }
            describeClustersRequest.setMarker(describeClustersResult.getMarker());
        } while (describeClustersResult.getMarker() != null);

        if (theCluster == null) {
            setNoLongerRequired(detected, srContext.LOGTAG, "Redshift cluster not found");
            return;
        }

        /*
         * From AWS Docs:
         * By default, cluster databases accept a connection whether it uses SSL or not. To configure your
         * cluster to require an SSL connection, set the require_SSL parameter to true in the parameter
         * group that is associated with the cluster.
         *
         * also, if the cluster is using the default parameter group "default.redshift-1.0", it can not be modified
         * so a new parameter group has to be created.  also, it doesn't seem right to update the customers
         * parameter group.  so, create a new one for the purpose of remediating the cluster.
         */
        String parameterGroupName = DateTimeFormatter
                .ofPattern("'" + PARAMETER_GROUP_NAME_PREFIX + "'" + "-yyyyMMddkkmmss-n")
                .withLocale(Locale.UK)
                .withZone(ZoneOffset.UTC)
                .format(Instant.now());
        try {
            CreateClusterParameterGroupRequest createClusterParameterGroupRequest = new CreateClusterParameterGroupRequest()
                    .withParameterGroupName(parameterGroupName)
                    .withDescription(theCluster.getClusterIdentifier() + " in transit encryption")
                    .withParameterGroupFamily(REDSHIFT_PARAMETER_GROUP_FAMILY);
            client.createClusterParameterGroup(createClusterParameterGroupRequest);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error creating the new Redshift cluster parameter group", e);
            return;
        }

        try {
            ModifyClusterParameterGroupRequest modifyClusterParameterGroupRequest = new ModifyClusterParameterGroupRequest()
                    .withParameterGroupName(parameterGroupName)
                    .withParameters(Collections.singletonList(new Parameter().withParameterName("require_ssl").withParameterValue("true")));
            client.modifyClusterParameterGroup(modifyClusterParameterGroupRequest);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error modifying the new Redshift cluster parameter group", e);
            return;
        }

        try {
            ModifyClusterRequest modifyClusterRequest = new ModifyClusterRequest()
                    .withClusterIdentifier(theCluster.getClusterIdentifier())
                    .withClusterParameterGroupName(parameterGroupName);
            client.modifyCluster(modifyClusterRequest);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error setting the parameter group for the Redshift cluster", e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG, "Redshift cluster modified to enable encryption in transit.  Reboot required.");
    }
}
