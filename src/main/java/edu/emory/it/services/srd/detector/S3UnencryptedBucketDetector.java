package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetBucketEncryptionRequest;
import com.amazonaws.services.s3.model.GetBucketEncryptionResult;
import com.amazonaws.services.s3.model.ServerSideEncryptionConfiguration;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;

/**
 * Detects S3 buckets that are unencrypted<br>
 *
 * <p>Check each bucket's configuration to see if encryption is enable</p>
 *
 * @see edu.emory.it.services.srd.remediator.S3UnencryptedBucketRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class S3UnencryptedBucketDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonS3 s3;
        try {
            s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3UnencryptedBucket,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        List<Bucket> buckets;
        try {
            buckets = s3.listBuckets();
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3UnencryptedBucket,
                    srContext.LOGTAG, "Error listing buckets", e);
            return;
        }

        for (Bucket b : buckets) {
 
        	final String bucketArn = "arn:aws:s3:::" + b.getName();
			Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
					.isExemptResourceArnForAccountAndDetector(bucketArn,accountId,getBaseName());
			boolean exceptionStatusUnderminable = resourceIsExempt == null;
			if (exceptionStatusUnderminable) {
				logger.error(srContext.LOGTAG + "Exemption status of bucket " + bucketArn + " can not be determined on account " + accountId);
				continue;					
			}

            if (resourceIsExempt) {
            logger.info(srContext.LOGTAG + "Ignoring exempted bucket \"" + bucketArn + "\" on account " + accountId);
                continue;
            }
        	
        	GetBucketEncryptionRequest request = new GetBucketEncryptionRequest().withBucketName(b.getName());
            GetBucketEncryptionResult result = null;
            ServerSideEncryptionConfiguration encryptionConfiguration = null;
            try {
                result = s3.getBucketEncryption(request);
            }
            catch (Exception e) {
                if (e instanceof AmazonS3Exception
                        && ((AmazonS3Exception) e).getStatusCode() == 404) {
                    // ignore not found exceptions
                }
                else {
                    setDetectionError(detection, DetectionType.S3UnencryptedBucket,
                            srContext.LOGTAG, "Error getting info about bucket encryption for bucket " + b.getName(), e);
                    return;
                }
            }
            if (result != null) {
                encryptionConfiguration = result.getServerSideEncryptionConfiguration();
            }
            if (result ==  null || encryptionConfiguration == null || encryptionConfiguration.getRules() == null || encryptionConfiguration.getRules().isEmpty()) {
                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                        DetectionType.S3UnencryptedBucket, "arn:aws:s3:::" + b.getName());
            }
        }
    }

    @Override
    public String getBaseName() {
        return "S3UnencryptedBucket";
    }
}
