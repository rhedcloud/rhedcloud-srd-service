package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.mediastore.AWSMediaStore;
import com.amazonaws.services.mediastore.AWSMediaStoreClient;
import com.amazonaws.services.mediastore.model.DeleteContainerRequest;
import com.amazonaws.services.mediastore.model.DescribeContainerRequest;
import com.amazonaws.services.mediastore.model.DescribeContainerResult;
import com.amazonaws.services.mediastoredata.AWSMediaStoreData;
import com.amazonaws.services.mediastoredata.AWSMediaStoreDataClient;
import com.amazonaws.services.mediastoredata.model.DeleteObjectRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.MediaStoreUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;

/**
 * Remediate by deleting the Elemental MediaStore container.<br>
 * First, all of the objects/files stored in the container are deleted.
 *
 * <p>Security: HIPAA workloads: Enforce, delete. (this remediator)<br>
 * Security: Standard workloads: revisit. (see MediaStorePublicContainerStandardRemediator)<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.MediaStorePublicContainerHipaaDetector the detector
 */
public class MediaStorePublicContainerHipaaRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:mediastore:", srContext.LOGTAG, DetectionType.MediaStorePublicContainer))
            return;
        // like arn:aws:mediastore:us-east-1:123456789012:container/container_name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];
        String containerName = arn[5].split("/")[1];

        try {
            AWSMediaStore msClient = getClient(accountId, region, srContext.getMetricCollector(), AWSMediaStoreClient.class);

            // describe the container to get the endpoint information
            DescribeContainerRequest describeContainerRequest = new DescribeContainerRequest()
                    .withContainerName(containerName);
            DescribeContainerResult describeContainerResult = msClient.describeContainer(describeContainerRequest);

            AWSMediaStoreData msdClient = getClient(accountId, region, describeContainerResult.getContainer().getEndpoint(),
                    srContext.getMetricCollector(), AWSMediaStoreDataClient.class);

            // delete all the objects in the container
            List<String> containerObjects = new MediaStoreUtil(msdClient).getContainerObjects();
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest();

            // then we can delete the container itself
            DeleteContainerRequest deleteContainerRequest = new DeleteContainerRequest()
                    .withContainerName(containerName);

            for (String path : containerObjects) {
                msdClient.deleteObject(deleteObjectRequest.withPath(path));
            }

            // wait for the object deletion to finish
            try {
                Thread.sleep(1000 * 10);
            } catch (InterruptedException e) {
                // ignore
            }

            msClient.deleteContainer(deleteContainerRequest);

            setSuccess(detected, srContext.LOGTAG, "Elemental MediaStore container deleted");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to delete Elemental MediaStore container", e);
        }
    }
}
