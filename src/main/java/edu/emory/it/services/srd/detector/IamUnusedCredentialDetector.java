package edu.emory.it.services.srd.detector;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.AccessKeyLastUsed;
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedRequest;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedResult;
import com.amazonaws.services.identitymanagement.model.GetLoginProfileRequest;
import com.amazonaws.services.identitymanagement.model.GetLoginProfileResult;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult;
import com.amazonaws.services.identitymanagement.model.ListUsersRequest;
import com.amazonaws.services.identitymanagement.model.ListUsersResult;
import com.amazonaws.services.identitymanagement.model.LoginProfile;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.User;

import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

///**
// * Look at users in the account and their access keys and checks the last time a user login has happened or an API access key 
// * was used. Call that number X. 
// * 
// * if X is >= the DeleteCredentialAfterInactiveForDays config parameter then
// * 		setDetectionError DetectionType.IamUnusedCredentialAwsConsole || DetectionType.IamUnusedCredentialApiKey
// * 
// * else if X is >= DisableCredentialAfterInactiveForDays 
// * 		setDetectionError DetectionType.IamUnusedCredentialDisableApiKey
// * 
// * else if X is => WarningCredentialAfterInactiveForDays  
// * 		setDetectionError DetectionType.IamUnusedCredentialWarningApiKey
// * 
// * else //all users with console access and all API access keys have been recently used
// * 		setDetectionSuccess(detection, DetectionType.IamUnusedCredentialAwsConsole, srContext.LOGTAG);
// *
// * @see edu.emory.it.services.srd.remediator.IamUnusedCredentialRemediator the remediator
// */

@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class IamUnusedCredentialDetector extends AbstractSecurityRiskDetector {
	private int 
	deleteAfterInactiveForDays,
	disableAfterInactiveForDays,
	warningAfterInactiveForDays;

	private long deleteAfterInactiveForDaysInMillis,
	disableAfterInactiveForDaysInMillis,
	warningAfterInactiveForDaysInMillis;

	private String additionalWarningNotificationText,
	additionalDisableNotificationText,
	additionalDeleteNotificationText;



	@Override
	public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
		super.init(aConfig, credentialsProvider, securityRole);

		try {
			Properties properties = appConfig.getProperties(getBaseName());
			String days = properties.getProperty("DeleteCredentialAfterInactiveForDays");
			deleteAfterInactiveForDays = Integer.parseInt(days);
			days = properties.getProperty("DisableCredentialAfterInactiveForDays");
			disableAfterInactiveForDays = Integer.parseInt(days);
			days = properties.getProperty("WarningCredentialAfterInactiveForDays");
			warningAfterInactiveForDays = Integer.parseInt(days);

			if (!((warningAfterInactiveForDays < disableAfterInactiveForDays) 
					&& (disableAfterInactiveForDays < deleteAfterInactiveForDays))
					|| deleteAfterInactiveForDays <= 0) {
				throw new SecurityRiskDetectionException(
						"Bad values for config parameters. They must be > 0 and "
								+ "WarningCredentialAfterInactiveForDays must be < DisableCredentialAfterInactiveForDays "
								+ " musr be < DeleteCredentialAfterInactiveForDays");
			}

			deleteAfterInactiveForDaysInMillis = ((long) deleteAfterInactiveForDays) * 24L * 60L * 60L * 1000L;
			disableAfterInactiveForDaysInMillis = ((long) disableAfterInactiveForDays) * 24L * 60L * 60L * 1000L;
			warningAfterInactiveForDaysInMillis = ((long) warningAfterInactiveForDays) * 24L * 60L * 60L * 1000L;

			additionalWarningNotificationText = properties.getProperty("AdditionalWarningNotificationText");
			additionalDisableNotificationText = properties.getProperty("AdditionalDisableNotificationText");
			additionalDeleteNotificationText = properties.getProperty("AdditionalDeleteNotificationText");

		}
		catch (NumberFormatException | EnterpriseConfigurationObjectException e) {
			throw new SecurityRiskDetectionException(e);
		}
	}

	
	HashMap<String,String> ants = new HashMap<>();

	@Override
	public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
		ants.clear();
		final AmazonIdentityManagement iam;
		try {
			iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);
		}
		catch (EmoryAwsClientBuilderException e) {
			// The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
			logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
			// detector name does not match a DetectionType so ensure DetectionResult is set
			setDetectionSuccess(detection, DetectionType.IamUnusedCredentialAwsConsole, srContext.LOGTAG);
			return;
		}
		catch (Exception e) {
			setDetectionError(detection, DetectionType.IamUnusedCredentialAwsConsole,
					srContext.LOGTAG, "On region: global", e);
			return;
		}


		boolean done = false;
		ListUsersRequest request = new ListUsersRequest();

		
		while (!done) {
			ListUsersResult response;
			try {
				response = iam.listUsers(request);
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.IamUnusedCredentialAwsConsole,
						srContext.LOGTAG, "Error listing users ", e);
				return;
			}


			for (User user : response.getUsers()) {
				checkUserLogin(iam, accountId, user, detection, srContext.LOGTAG);
				checkAccessKeys(iam, accountId, user, detection, srContext.LOGTAG);
			}

			request.setMarker(response.getMarker());

			if (!response.getIsTruncated()) {
				done = true;
			}
		}
		// detector name does not match a DetectionType so ensure DetectionResult is set
		if (detection.getDetectionResult() == null) {
			setDetectionSuccess(detection, DetectionType.IamUnusedCredentialAwsConsole, srContext.LOGTAG);

		} else {
			for(DetectedSecurityRisk risk: ((List<DetectedSecurityRisk>) detection.getDetectedSecurityRisk())) {
				try {
					risk.setAdditionalNotificationText(ants.get(risk.getAmazonResourceName()));
				} catch (EnterpriseFieldException e) {
					logger.error("IamUnusedCredentiaDetector: Error setting additional notification text\nANTs are:\n"+ants);
					setDetectionError(detection, DetectionType.valueOf(risk.getType()), 
							srContext.LOGTAG, "Error setting additional notification text ", e);
					return;
				}
			}
  		}
	}

	private void checkUserLogin(AmazonIdentityManagement iam, String accountId, User user,
			SecurityRiskDetection detection, String LOGTAG) {
		Date passwordLastUsed = user.getCreateDate();

		if (user.getPasswordLastUsed() != null) {
			passwordLastUsed = user.getPasswordLastUsed();
		}
		GetLoginProfileRequest getLoginProfileRequest = new GetLoginProfileRequest().withUserName(user.getUserName());
		LoginProfile loginProfile = null;
		try {
			GetLoginProfileResult getLoginProfileResult = iam.getLoginProfile(getLoginProfileRequest);
			loginProfile = getLoginProfileResult.getLoginProfile();
		}
		catch (NoSuchEntityException e) {
			//ignore
		}
		catch (Exception e) {
			setDetectionError(detection, DetectionType.IamUnusedCredentialAwsConsole,
					LOGTAG, "Error getting login profile for user " + user.getUserName(), e);
			return;
		}

		if (loginProfile != null 
				&& System.currentTimeMillis() - passwordLastUsed.getTime() 
				> ((long) deleteAfterInactiveForDays) * 24L * 60L * 60L * 1000L) {
			addDetectedSecurityRisk(detection, LOGTAG,
					DetectionType.IamUnusedCredentialAwsConsole, user.getArn());
		}
	}

	private void checkAccessKeys(AmazonIdentityManagement iam, String accountId, User user,
			SecurityRiskDetection detection, String LOGTAG) {

		boolean done = false;
		ListAccessKeysRequest accessKeysRequest = new ListAccessKeysRequest()
				.withUserName(user.getUserName());
		
		while (!done) {
			ListAccessKeysResult accessKeys;
			try {
				accessKeys = iam.listAccessKeys(accessKeysRequest);
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.IamUnusedCredentialDeleteApiKey,
						LOGTAG, "Error getting access keys for user " + user.getUserName(), e);
				return;
			}
			
			for (AccessKeyMetadata metadata : accessKeys.getAccessKeyMetadata()) {

				GetAccessKeyLastUsedRequest lastUsedRequest = new GetAccessKeyLastUsedRequest()
						.withAccessKeyId(metadata.getAccessKeyId());

				GetAccessKeyLastUsedResult lastUsedResponse;
				try {
					lastUsedResponse = iam.getAccessKeyLastUsed(lastUsedRequest);
				}
				catch (Exception e) {
					setDetectionError(detection, DetectionType.IamUnusedCredentialDeleteApiKey,
							LOGTAG, "Error getting access key last used for user " + user.getUserName() + " and id " + metadata.getAccessKeyId(), e);
					return;
				}

				AccessKeyLastUsed lastUsed = lastUsedResponse.getAccessKeyLastUsed();
				Date lastTimeUsed = metadata.getCreateDate();
				if (lastUsed.getLastUsedDate() != null) {
					lastTimeUsed = lastUsed.getLastUsedDate();
				}

				long inactiveForMillis = System.currentTimeMillis() - lastTimeUsed.getTime();
				String keyId = user.getArn() + "/access-key/" + metadata.getAccessKeyId();

				if (inactiveForMillis >= deleteAfterInactiveForDaysInMillis) {
					String ant = cookedAdditionalNotificationText(
							additionalDeleteNotificationText, user.getUserName(), 
							keyId, inactiveForMillis, 0);
					ants.put(keyId, ant);
					addDetectedSecurityRisk(detection, LOGTAG,
							DetectionType.IamUnusedCredentialDeleteApiKey, keyId);  
				} else if (inactiveForMillis >= disableAfterInactiveForDaysInMillis) {
					String ant = cookedAdditionalNotificationText(
							additionalDisableNotificationText, user.getUserName(), 
							keyId, inactiveForMillis, deleteAfterInactiveForDays);
					ants.put(keyId, ant);
					addDetectedSecurityRisk(detection, LOGTAG,
							DetectionType.IamUnusedCredentialDisableApiKey, keyId);
				} else if (inactiveForMillis >= warningAfterInactiveForDaysInMillis) {
					String ant = cookedAdditionalNotificationText(
							additionalWarningNotificationText, user.getUserName(), 
							keyId, inactiveForMillis, disableAfterInactiveForDays);
					ants.put(keyId, ant);
					addDetectedSecurityRisk(detection, LOGTAG,
							DetectionType.IamUnusedCredentialWarningApiKey, keyId);
				} 			

			}

			accessKeysRequest.setMarker(accessKeys.getMarker());

			if (!accessKeys.getIsTruncated()) {
				done = true;
			}
		}
				
	}

	private static String cookedAdditionalNotificationText(String rawText, String user, String credential, 
			long inactiveForMillis, int period) {
		return rawText.replaceAll("\\{iamUser}", user)
				.replaceAll("VWXYZ", credential.substring(credential.length()-5))
				.replaceAll("\\^\\^", Math.floorDiv(inactiveForMillis, 1000 * 60 * 60 * 24)+"")
				.replaceAll("%%", period - Math.floorDiv(inactiveForMillis, 1000 * 60 * 60 * 24)+"")
				.trim().replaceAll("\\t", " ")
				.replaceAll(" *\\n *", " ")
				.replaceAll(" *\\r *", " ");
	}


	@Override
	public String getBaseName() {
		return "IamUnusedCredential";
	}
}
