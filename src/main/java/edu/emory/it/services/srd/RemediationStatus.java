package edu.emory.it.services.srd;

public enum RemediationStatus {
    REMEDIATION_SUCCESS("Remediated"),
    REMEDIATION_FAILED("Remediation Failed"),
    REMEDIATION_POSTPONED("Remediation Postponed"),
    REMEDIATION_NO_LONGER_REQUIRED("Remediation No Longer Required"),
    REMEDIATION_NOTIFICATION_ONLY("Remediation Notification Only"),
    REMEDIATION_TIMEOUT("Remediation Timeout");

    private final String status;

    RemediationStatus(String status) {
        this.status = status;
    }

    public String getStatus() { return status; }
}
