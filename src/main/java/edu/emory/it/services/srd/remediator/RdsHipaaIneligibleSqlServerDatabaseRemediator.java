package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by stopping the DB instance.<br>
 * The snapshot identifier prefix is "rds-unencrypted-transport-snapshot-".
 *
 * <p>See the
 * {@linkplain edu.emory.it.services.srd.remediator.AbstractRdsRemediator#remediateDBInstance(DetectedSecurityRisk, String, DetectionType, String, RemediationAction, SecurityRiskContext) remediateDBInstance}
 * method for addition information.</p>
 *
 * @see edu.emory.it.services.srd.detector.RdsHipaaIneligibleSqlServerDatabaseDetector the detector
 */
public class RdsHipaaIneligibleSqlServerDatabaseRemediator extends AbstractRdsRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediateDBInstance(detected, accountId,
                DetectionType.RdsHipaaIneligibleSqlServerDatabase,
                "rds-unencrypted-transport-snapshot-",
                AbstractRdsRemediator.RemediationAction.STOP,
                srContext);
    }
}
