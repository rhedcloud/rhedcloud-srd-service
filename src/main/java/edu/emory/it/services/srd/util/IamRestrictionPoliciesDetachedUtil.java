package edu.emory.it.services.srd.util;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.model.ListPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListPoliciesResult;
import com.amazonaws.services.identitymanagement.model.Policy;

import java.util.Arrays;

/**
 * Define important IAM resource names.
 */
public class IamRestrictionPoliciesDetachedUtil {
    public static final String ADMINISTRATOR_ROLE_NAME = "RHEDcloudAdministratorRole";

    public static final String CUSTOM_ROLE_IAM_PATH = "/customroles/";
    public static final String RHEDCLOUD_IAM_PATH = "/rhedcloud/";
    public static final String AWS_RESERVED_IAM_PATH = "/aws-reserved/sso.amazonaws.com/";

    private static final String ADMINISTRATOR_ROLE_POLICY_NAME = "RHEDcloudAdministratorRolePolicy";
    private static final String ADMINISTRATOR_ROLE_HIPAA_POLICY_NAME = "RHEDcloudAdministratorRoleHipaaPolicy";
    private static final String MANAGEMENT_SUBNET_POLICY_NAME_SUFFIX = "-RHEDcloudManagementSubnetPolicy";

    /**
     * Get the important RHEDcloud restriction policies.
     *
     * @param iam client
     * @return policies
     */
    public static RestrictionPolicies getRestrictionPolicies(AmazonIdentityManagement iam) {
        RestrictionPolicies restrictionPolicies = new RestrictionPolicies();

        ListPoliciesRequest listPoliciesRequest = new ListPoliciesRequest();
        ListPoliciesResult listPoliciesResult;

        do {
            listPoliciesResult = iam.listPolicies(listPoliciesRequest);

            for (Policy policy : listPoliciesResult.getPolicies()) {
                if (ADMINISTRATOR_ROLE_POLICY_NAME.equals(policy.getPolicyName())) {
                    restrictionPolicies.RHEDcloudAdministratorRolePolicy = policy;
                } else if (ADMINISTRATOR_ROLE_HIPAA_POLICY_NAME.equals(policy.getPolicyName())) {
                    restrictionPolicies.RHEDcloudAdministratorRoleHipaaPolicy = policy;
                } else if (policy.getPolicyName().contains(MANAGEMENT_SUBNET_POLICY_NAME_SUFFIX)) {
                    if (restrictionPolicies.RHEDcloudManagementSubnetPolicies == null) {
                        restrictionPolicies.RHEDcloudManagementSubnetPolicies = new Policy[] {policy};
                    } else {
                        restrictionPolicies.RHEDcloudManagementSubnetPolicies
                                = Arrays.copyOf(restrictionPolicies.RHEDcloudManagementSubnetPolicies,
                                restrictionPolicies.RHEDcloudManagementSubnetPolicies.length + 1);
                        restrictionPolicies.RHEDcloudManagementSubnetPolicies[restrictionPolicies.RHEDcloudManagementSubnetPolicies.length - 1] = policy;
                    }
                }
            }

            listPoliciesRequest.setMarker(listPoliciesResult.getMarker());
        } while (listPoliciesResult.getIsTruncated());

        return restrictionPolicies;
    }
    public static class RestrictionPolicies {
        public Policy RHEDcloudAdministratorRolePolicy;
        public Policy RHEDcloudAdministratorRoleHipaaPolicy;
        public Policy[] RHEDcloudManagementSubnetPolicies;
    }
}
