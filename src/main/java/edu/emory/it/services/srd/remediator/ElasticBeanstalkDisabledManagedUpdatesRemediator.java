package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk;
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient;
import com.amazonaws.services.elasticbeanstalk.model.ConfigurationOptionSetting;
import com.amazonaws.services.elasticbeanstalk.model.UpdateEnvironmentRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by enabling managed updates.
 *
 * <p>Several options are updated to enable managed updates:
 * <ul>
 * <li>Managed platform updates depend on enhanced health reporting so it will be enabled.</li>
 * <li>The scheduled maintenance window is set to Sunday at 3:00 AM.</li>
 * <li>The environment is configured to automatically apply both patch and minor version updates.</li>
 * <li>Finally, instance replacement is enabled (the <code>InstanceRefreshEnabled</code> option).</li>
 * </ul>
 *
 * <p>See <a href="https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/environment-platform-update-managed.html">Managed Platform Updates</a>.</p>
 *
 * <p>Security: Enforce, enabled or stop or delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.ElasticBeanstalkDisabledManagedUpdatesDetector the detector
 */
public class ElasticBeanstalkDisabledManagedUpdatesRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:elasticbeanstalk:", srContext.LOGTAG, DetectionType.ElasticBeanstalkDisabledManagedUpdates))
            return;
        // like arn:aws:elasticbeanstalk:us-east-1:123456789012:environment/EB_Application_Name/EB_Environment_Name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];
        String environmentId = detected.getProperties().getProperty("environmentId");
        String environmentName = arn[5].split("/")[2];

        UpdateEnvironmentRequest updateEnvironmentRequest = new UpdateEnvironmentRequest()
                .withEnvironmentId(environmentId)
                .withEnvironmentName(environmentName)
                .withOptionSettings(
                        new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:healthreporting:system")
                                .withOptionName("SystemType")
                                .withValue("enhanced")  // needed in order to enable managed actions
                        ,new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:managedactions")
                                .withOptionName("ManagedActionsEnabled")
                                .withValue("true")
                        ,new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:managedactions")
                                .withOptionName("PreferredStartTime")
                                .withValue("Sun:03:00")  // early morning on Sunday
                        ,new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:managedactions:platformupdate")
                                .withOptionName("UpdateLevel")
                                .withValue("minor")   // both minor and patch version updates
                        ,new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:managedactions:platformupdate")
                                .withOptionName("InstanceRefreshEnabled")
                                .withValue("true")
                );

        try {
            AWSElasticBeanstalk client = getClient(accountId, region, srContext.getMetricCollector(), AWSElasticBeanstalkClient.class);
            client.updateEnvironment(updateEnvironmentRequest);
            setSuccess(detected, srContext.LOGTAG, "Managed actions enabled for environment " + environmentName);
        }
        catch (Exception e) {
            // TODO - requirement is to stop or delete the environment if the update failed
            setError(detected, srContext.LOGTAG, "Failed to enable managed actions for environment " + environmentName, e);
        }
    }
}
