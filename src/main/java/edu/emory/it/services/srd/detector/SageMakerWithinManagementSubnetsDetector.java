package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.sagemaker.AmazonSageMaker;
import com.amazonaws.services.sagemaker.AmazonSageMakerClient;
import com.amazonaws.services.sagemaker.model.DescribeModelRequest;
import com.amazonaws.services.sagemaker.model.DescribeModelResult;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceRequest;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceResult;
import com.amazonaws.services.sagemaker.model.DescribeTrainingJobRequest;
import com.amazonaws.services.sagemaker.model.DescribeTrainingJobResult;
import com.amazonaws.services.sagemaker.model.ListModelsRequest;
import com.amazonaws.services.sagemaker.model.ListModelsResult;
import com.amazonaws.services.sagemaker.model.ListNotebookInstancesRequest;
import com.amazonaws.services.sagemaker.model.ListNotebookInstancesResult;
import com.amazonaws.services.sagemaker.model.ListTrainingJobsRequest;
import com.amazonaws.services.sagemaker.model.ListTrainingJobsResult;
import com.amazonaws.services.sagemaker.model.ModelSummary;
import com.amazonaws.services.sagemaker.model.NotebookInstanceSummary;
import com.amazonaws.services.sagemaker.model.TrainingJobSummary;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * Checks for SageMaker resources in a disallowed subnet.
 *
 * @see edu.emory.it.services.srd.remediator.SageMakerWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class SageMakerWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                AmazonEC2 ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                AmazonSageMaker sageMakerClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonSageMakerClient.class);

                // collect the IDs of the disallowed subnets
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);

                inspectNotebookInstances(detection, srContext, sageMakerClient, disallowedSubnets);
                inspectTrainingJobs(detection, srContext, sageMakerClient, disallowedSubnets);
                inspectModels(detection, srContext, sageMakerClient, disallowedSubnets);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.SageMakerWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    private void inspectNotebookInstances(SecurityRiskDetection detection, SecurityRiskContext srContext,
                                          AmazonSageMaker sageMakerClient, Set<String> disallowedSubnets) {
        ListNotebookInstancesRequest listNotebookInstancesRequest = new ListNotebookInstancesRequest();
        ListNotebookInstancesResult listNotebookInstancesResult;
        do {
            listNotebookInstancesResult = sageMakerClient.listNotebookInstances(listNotebookInstancesRequest);

            for (NotebookInstanceSummary notebookInstanceSummary : listNotebookInstancesResult.getNotebookInstances()) {
                DescribeNotebookInstanceRequest describeNotebookInstanceRequest = new DescribeNotebookInstanceRequest()
                        .withNotebookInstanceName(notebookInstanceSummary.getNotebookInstanceName());
                DescribeNotebookInstanceResult describeNotebookInstanceResult
                        = sageMakerClient.describeNotebookInstance(describeNotebookInstanceRequest);

                if (describeNotebookInstanceResult.getSubnetId() != null
                        && disallowedSubnets.contains(describeNotebookInstanceResult.getSubnetId())) {
                    addDetectedSecurityRisk(detection, srContext.LOGTAG,
                            DetectionType.SageMakerWithinManagementSubnets, describeNotebookInstanceResult.getNotebookInstanceArn());
                }
            }

            listNotebookInstancesRequest.setNextToken(listNotebookInstancesResult.getNextToken());
        } while (listNotebookInstancesResult.getNextToken() != null);
    }

    private void inspectTrainingJobs(SecurityRiskDetection detection, SecurityRiskContext srContext,
                                     AmazonSageMaker sageMakerClient, Set<String> disallowedSubnets) {
        ListTrainingJobsRequest listTrainingJobsRequest = new ListTrainingJobsRequest();
        ListTrainingJobsResult listTrainingJobsResult;
        do {
            listTrainingJobsResult = sageMakerClient.listTrainingJobs(listTrainingJobsRequest);

            for (TrainingJobSummary trainingJobSummary : listTrainingJobsResult.getTrainingJobSummaries()) {
                DescribeTrainingJobRequest describeTrainingJobRequest = new DescribeTrainingJobRequest()
                        .withTrainingJobName(trainingJobSummary.getTrainingJobName());
                DescribeTrainingJobResult describeTrainingJobResult
                        = sageMakerClient.describeTrainingJob(describeTrainingJobRequest);

                // most job statuses are ignored because it means the training job is doing no work
                // and training jobs can't be deleted - only stopped
                // in fact, the only status that is actionable is 'InProgress'
                switch (describeTrainingJobResult.getTrainingJobStatus()) {
                    case "Completed":
                    case "Failed":
                    case "Stopping":
                    case "Stopped":
                        continue;
                }

                if (describeTrainingJobResult.getVpcConfig() != null) {
                    Set<String> trainingJobSubnets = new HashSet<>(describeTrainingJobResult.getVpcConfig().getSubnets());
                    if (disallowedSubnets.stream().anyMatch(trainingJobSubnets::contains)) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.SageMakerWithinManagementSubnets, describeTrainingJobResult.getTrainingJobArn());
                    }
                }
            }

            listTrainingJobsRequest.setNextToken(listTrainingJobsResult.getNextToken());
        } while (listTrainingJobsResult.getNextToken() != null);
    }

    private void inspectModels(SecurityRiskDetection detection, SecurityRiskContext srContext,
                               AmazonSageMaker sageMakerClient, Set<String> disallowedSubnets) {
        ListModelsRequest listModelsRequest = new ListModelsRequest();
        ListModelsResult listModelsResult;
        do {
            listModelsResult = sageMakerClient.listModels(listModelsRequest);

            for (ModelSummary modelSummary : listModelsResult.getModels()) {
                DescribeModelRequest describeModelRequest = new DescribeModelRequest()
                        .withModelName(modelSummary.getModelName());
                DescribeModelResult describeModelResult = sageMakerClient.describeModel(describeModelRequest);

                if (describeModelResult.getVpcConfig() != null) {
                    Set<String> modelSubnets = new HashSet<>(describeModelResult.getVpcConfig().getSubnets());
                    if (disallowedSubnets.stream().anyMatch(modelSubnets::contains)) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.SageMakerWithinManagementSubnets, describeModelResult.getModelArn());
                    }
                }
            }

            listModelsRequest.setNextToken(listModelsResult.getNextToken());
        } while (listModelsResult.getNextToken() != null);
    }

    @Override
    public String getBaseName() {
        return "SageMakerWithinManagementSubnets";
    }
}
