package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CustomOriginConfig;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigRequest;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigResult;
import com.amazonaws.services.cloudfront.model.Origin;
import com.amazonaws.services.cloudfront.model.UpdateDistributionRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates cloud distributions where OriginProtocolPolicy is not equal to "https-only"<br>
 * <p>The remediator updates the configuration and sets OriginProtocolPolicy to "https-only"</p>
 *
 * @see edu.emory.it.services.srd.detector.CloudFrontNonHTTPSOriginEnabledDetector the detector
 */
public class CloudFrontNonHTTPSOriginEnabledRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudfront:", srContext.LOGTAG, DetectionType.CloudFrontNonHTTPSOriginEnabled))
            return;
        // like
        // arn:${Partition}:cloudfront::${Account}:distribution/${DistributionId}
        // arn:${Partition}:cloudfront::${Account}:streaming-distribution/${DistributionId}
        // arn:${Partition}:cloudfront::${Account}:origin-access-identity/${Id}
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String distributionPath = arn[5];
        String distributionId = distributionPath.substring(distributionPath.lastIndexOf("/") + 1);

        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Distribution with id '" + distributionId + "'", e);
            return;
        }

        GetDistributionConfigRequest request = new GetDistributionConfigRequest().withId(distributionId);
        GetDistributionConfigResult result = cloudFront.getDistributionConfig(request);

        boolean hasChange = false;
        for (Origin origin : result.getDistributionConfig().getOrigins().getItems() ) {
            CustomOriginConfig config = origin.getCustomOriginConfig();
            if (config != null) {  //config == null for S3 buckets, we want to skip those
                if (!config.getOriginProtocolPolicy().equals("https-only")) {  //found something to change
                    config.setOriginProtocolPolicy("https-only");
                    hasChange = true;
                }
            }
        }

        if (!hasChange) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Distribution with id '" + distributionId + "' only contains origins with origin protocol policy of https-only");
            return;
        }


        UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest()
                .withId(distributionId)
                .withIfMatch(result.getETag())
                .withDistributionConfig(result.getDistributionConfig());
        cloudFront.updateDistribution(updateDistributionRequest);

        setSuccess(detected, srContext.LOGTAG,
                "Distribution with id '" + distributionId + "' has been updated with origin protocol policy of https-only");
    }
}
