package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountNotification;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.Annotation;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult;
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult;
import com.openii.openeai.commands.OpeniiSyncCommand;
import edu.emory.it.services.srd.util.SrdMetricUtil;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.SyncCommand;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.EnterpriseObjectCreateException;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.List;

public class SrdAccountNotificationCommand extends OpeniiSyncCommand implements SyncCommand {
    private static final String LOGTAG = "[SrdAccountNotificationCommand] ";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SrdAccountNotificationCommand.class);

    private static final String PRIORITY_HIGH = "High";
    private static final String PRIORITY_MEDIUM = "Medium";
    private static final String PRIORITY_LOW = "Low";

    private final ProducerPool awsAccountProducerPool;

    public SrdAccountNotificationCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        try {
            awsAccountProducerPool = (ProducerPool) getAppConfig().getObject("AwsAccountP2pProducer");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the AwsAccountP2pProducer producer pool from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public void execute(int messageNumber, Message aMessage) throws CommandException {
        // get the message body from the JMS message
        Document inDoc;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        }
        catch (Exception e) {
            String errMsg = "Exception occurred initializing inputs. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the ControlArea element.
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgObject = eControlArea.getAttribute("messageObject").getValue();

        // Verify that the message is a SecurityRiskDetection-Create-Sync
        // since both SecurityRiskDetection sync and Account sync messages are routed to the
        // SrdServiceTopic we just ignore the messages we don't handle
        if (!msgObject.equalsIgnoreCase("SecurityRiskDetection")) {
            return;
        }
        if (!msgAction.equalsIgnoreCase("Create")) {
            return;
        }

        // Verify that SecurityRiskDetection element is not null; if it is, reply with an error.
        Element newData = inDoc.getRootElement().getChild("DataArea").getChild("NewData").getChild("SecurityRiskDetection");
        if (newData == null) {
            String errDesc = "Invalid NewData element found in the Create-Sync message. This command expects a SecurityRiskDetection.";
            logger.fatal(LOGTAG + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            throw new CommandException(errDesc);
        }

        SecurityRiskDetection detection;
        try {
            detection = (SecurityRiskDetection) getAppConfig().getObjectByType(SecurityRiskDetection.class.getName());
            detection.buildObjectFromInput(newData);
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error retrieving a SecurityRiskDetection object from AppConfig: The exception is: " + e.getMessage());
            String errDesc = "Error retrieving EO.";
            throw new CommandException(errDesc);
        }
        catch (EnterpriseLayoutException ele) {
            String errDesc = "An error occurred building object from the " +
                    "SecurityRiskDetection element in the Create-Sync message. The exception is: " + ele.getMessage();
            logger.fatal(LOGTAG + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            throw new CommandException(errDesc);
        }

        // gather metrics as we process the sync messages
        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();
        srdMetricUtil.cacheLastSecurityRiskDetectionIdSyncAccountNotification(detection.getSecurityRiskDetectionId());

        // evaluate to determine if a notification should be sent
        if (!DetectionStatus.DETECTION_SUCCESS.getStatus().equals(detection.getDetectionResult().getStatus())) {
            evaluateDetectionError(detection);
        }
        else if (detection.getDetectedSecurityRisk().size() > 0) {
            @SuppressWarnings("unchecked")
            List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk();
            for (DetectedSecurityRisk risk : risks) {
                evaluateRisk(detection, risk);
            }
        }
        else {
            logger.info(LOGTAG + "Skipping account notification for "
                    + detection.getDetectionResult().getType() + "-" + detection.getAccountId()
                    + " for sync message number " + messageNumber
                    + " and detection status '" + detection.getDetectionResult().getStatus() + "'"
                    + " and number of risks " + detection.getDetectedSecurityRisk().size()
                    + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                    + " with instanceId " + detection.getInstanceId());
        }
    }

    /**
     * Should an account notification be created?
     *
     * Exempt Error failures should not create account notifications since they were a last second
     *   verification that the account is eligible for SRDs to run.
     * Successful detections will not notify since remediation will be evaluated.
     *
     * Detection failed and timeout error should notify but at a LOW priority.
     *   While it's true that we failed while identifying risks, an end user generally can't do
     *   anything about it.  Making it LOW allows a user to opt in or out of the notification.
     */
    private void evaluateDetectionError(SecurityRiskDetection detection) {

        long startTime = System.currentTimeMillis();

        DetectionResult detectionResult = detection.getDetectionResult();
        DetectionStatus detectionStatus = null;
        String detectionResultStatus = detectionResult.getStatus();
        String priority;

        // it should never happen that detectionResultStatus is null but be defensive
        if (detectionResultStatus != null) {
            for (DetectionStatus ds : DetectionStatus.values()) {
                if (ds.getStatus().equals(detectionResultStatus)) {
                    detectionStatus = ds;
                    break;
                }
            }
        }

        if (detectionStatus == DetectionStatus.DETECTION_FAILED) {
            priority = PRIORITY_LOW;
        }
        else if (detectionStatus == DetectionStatus.DETECTION_TIMEOUT) {
            priority = PRIORITY_LOW;
        }
        else {
            priority = null;

            logger.info(LOGTAG + "Skipping account notification for "
                    + detectionResult.getType() + "-" + detection.getAccountId()
                    + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                    + " with instanceId " + detection.getInstanceId()
                    + " and SRR " + detection.getSecurityRiskRemediator()
                    + " and detection result status '" + detectionResultStatus + "'"
                    + " and detection status '" + detectionStatus + "'"
                    + " in elapsed time " + (System.currentTimeMillis() - startTime) + " ms");
        }

        if (priority != null) {
            DetectionType detectionType = DetectionType.valueOf(detectionResult.getType());
            String subject = getSubjectForDetectionError(detectionType, detectionStatus);
            String messageBody = getNotificationMessageBodyForDetectionError(detectionResult, detectionStatus);
            // there is no ARN for detection error notifications so make one up
            String arn = "arn:partition:service:region:" + detection.getAccountId();

            boolean sent = sendAccountNotification(detection, priority, subject, messageBody, detectionType, arn,
                    detectionStatus, null);

            logger.info(LOGTAG + priority + " priority account notification " + (sent ? "sent" : "not sent") +  " for "
                    + detectionType + "-" + detection.getAccountId()
                    + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                    + " with instanceId " + detection.getInstanceId()
                    + " and arn " + arn
                    + " and SRR " + detection.getSecurityRiskRemediator()
                    + " and detection result status '" + detectionResultStatus + "'"
                    + " and detection status '" + detectionStatus + "'"
                    + " and message subject '" + subject + "'"
                    + " and message body '" + messageBody + "'"
                    + " in elapsed time " + (System.currentTimeMillis() - startTime) + " ms");
        }
    }

    /**
     * Should an account notification be created?
     *
     * If there was no remediator specified then it is considered to be notification-only,
     *   though there are explicit notification-only detectors too.
     * Sometimes a risk is found but by the time the remediator runs the risk is no longer present.
     *   For example, an unencrypted database might be deleted by the time the remediator runs.
     *   In this case, remediation is no longer required.
     * Otherwise there was a remediation which has a status like successful or failed or timed out or etc.
     *
     * Many of the detectors will create a HIGH notification for all of the remediation statuses except
     *   when remediation is no longer required (in which case the priority is LOW).
     * Still other remediators completely fix the risk (or will soon enough) so the the notification is
     *   considered a MEDIUM priority.
     * There are still a few detectors/remediators that don't work yet so they are considered LOW priority.
     */
    private void evaluateRisk(SecurityRiskDetection detection, DetectedSecurityRisk risk) {

        long startTime = System.currentTimeMillis();

        RemediationStatus remediationStatus = null;
        String remediationResultStatus = null;
        String remediationResultDesc = null;

        RemediationResult remediationResult = risk.getRemediationResult();

        if (remediationResult != null) {
            remediationResultStatus = remediationResult.getStatus();
            remediationResultDesc = remediationResult.getDescription();

            // it should never happen that remediationResultStatus is null but be defensive
            if (remediationResultStatus != null) {
                for (RemediationStatus rs : RemediationStatus.values()) {
                    if (rs.getStatus().equals(remediationResultStatus)) {
                        remediationStatus = rs;
                        break;
                    }
                }
            }
        }

        // in the Generate command handling when the response is setup,
        // if a remediator was not given in the request, the response will get "none" for the SecurityRiskRemediator field
        // treat this case as a notification-only SRD
        if (detection.getSecurityRiskRemediator() == null
                || detection.getSecurityRiskRemediator().equals("none")
                || remediationStatus == null) {
            remediationStatus = RemediationStatus.REMEDIATION_NOTIFICATION_ONLY;
        }

        DetectionType detectionType = DetectionType.valueOf(risk.getType());
        String priority = priorityMappingForRemediation(detectionType, remediationStatus);
        String arn = risk.getAmazonResourceName();

        String baseLogMsg = " for "
                + detectionType + "-" + detection.getAccountId()
                + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                + " with instanceId " + detection.getInstanceId()
                + " and arn " + risk.getAmazonResourceName()
                + " and SRR " + detection.getSecurityRiskRemediator()
                + " and remediation result status '" + remediationResultStatus + "' and description '" + remediationResultDesc + "'"
                + " and remediation status '" + remediationStatus + "'";

        if (priority != null) {
            String subject = getSubjectForRemediation(detectionType, remediationStatus);
            String messageBody = getNotificationMessageBodyForRemediation(detection, risk, remediationStatus);

            boolean sent = sendAccountNotification(detection, priority, subject, messageBody, detectionType, arn,
                    DetectionStatus.DETECTION_SUCCESS, remediationStatus);

            logger.info(LOGTAG + priority + " priority account notification " + (sent ? "sent" : "not sent")
                    + baseLogMsg
                    + " and message subject '" + subject + "'"
                    + " and message body '" + messageBody + "'"
                    + " in elapsed time " + (System.currentTimeMillis() - startTime) + " ms");
        }
        else {
            logger.info(LOGTAG + "Skipping account notification"
                    + baseLogMsg
                    + " in elapsed time " + (System.currentTimeMillis() - startTime) + " ms");
        }
    }

    /**
     * Send an Account Notification via AWS Account Service.
     * The formulation of the annotation is specific to SRD and incorporates various elements to ensure that
     * notifications aren't suppressed when they shouldn't be.  For example, RDS operations can take many minutes
     * which means there can be quite a few POSTPONED remediations until the RDS instance/cluster reaches a state
     * where they can be stopped or deleted.  All but the first one will be suppressed.  But, once the remediation
     * is successful, the notification can not be suppressed which is why the remediation status is included
     * in the annotation.  Similar would be a steady stream of failed detections followed by a successful detection
     * which is why the detection status is included in the annotation.
     *
     * @return true if the notification was sent
     */
    private boolean sendAccountNotification(SecurityRiskDetection detection,
                                            String priority, String subject, String messageBody,
                                            DetectionType detectionType, String arn,
                                            DetectionStatus detectionStatus, RemediationStatus remediationStatus) {
        PointToPointProducer awsAccountProducer = null;
        try {
            awsAccountProducer = (PointToPointProducer) awsAccountProducerPool.getExclusiveProducer();

            try {
                AccountNotification accountNotification = (AccountNotification) getAppConfig().getObject("AccountNotification.v1_0");
                accountNotification.setAccountId(detection.getAccountId());
                accountNotification.setType("SRD");
                accountNotification.setPriority(priority);
                accountNotification.setCreateUser("SRD," + detection.getCreateUser());
                accountNotification.setCreateDatetime(detection.getCreateDatetime());
                accountNotification.setSubject(subject);
                accountNotification.setText(messageBody);
                accountNotification.setReferenceId(detection.getSecurityRiskDetectionId());
                Annotation annotation = accountNotification.newAnnotation();
                annotation.setText("SRDOBJECT," + detectionType + "," + detectionStatus + "," + remediationStatus + "," + arn);
                annotation.setCreateUser("SRD," + detection.getCreateUser());
                annotation.setCreateDatetime(detection.getCreateDatetime());
                accountNotification.addAnnotation(annotation);

                accountNotification.create(awsAccountProducer);

                return true;
            }
            catch (EnterpriseObjectCreateException e) {
                String errMsg = "An error occurred while sending AccountNotification.Create-Request"
                        + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
            }
            catch (EnterpriseConfigurationObjectException e) {
                String errMsg = "An error occurred while getting AccountNotification object from AppConfig"
                        + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "An error occurred while setting a field in AccountNotification object"
                        + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
            }
        }
        catch (JMSException e) {
            String errMsg = "An error occurred while getting AWS Account Service Producer"
                    + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                    + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
        }
        finally {
            if (awsAccountProducer != null)
                awsAccountProducerPool.releaseProducer(awsAccountProducer);
        }

        return false;
    }

    private String getSubjectForDetectionError(DetectionType detectionType, DetectionStatus detectionStatus) {
        switch (detectionStatus) {
            case DETECTION_FAILED:
                return "Detection failed for " + detectionType.name();
            case DETECTION_TIMEOUT:
                return "Detection timed out for " + detectionType.name();
            // currently, the other statuses will not pass evaluation but fallback to a sensible message if it does
            case DETECTION_SUCCESS: // fallthrough
            default:
                return "Detection for " + detectionStatus.name();
        }
    }

    private String getSubjectForRemediation(DetectionType detectionType, RemediationStatus remediationStatus) {
        switch (detectionType) {
            case AccountInWrongOrganizationalUnit:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated the account in the wrong organizational unit";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for the account in the wrong organizational unit";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for the account in the wrong organizational unit";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for the account in the wrong organizational unit";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for the account in the wrong organizational unit";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found the account in the wrong organizational unit";
                }

            case ApiGatewayCaching:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an API Gateway with caching enabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an API Gateway with caching enabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an API Gateway with caching enabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an API Gateway with caching enabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an API Gateway with caching enabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an API Gateway with caching enabled";
                }

            case AutoscalingGroupInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an Auto Scaling group that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an Auto Scaling group that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an Auto Scaling group that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an Auto Scaling group that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an Auto Scaling group that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an Auto Scaling group that was launched within a disallowed subnet";
                }

            case CloudFrontNonHTTPSOriginEnabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a CloudFront with non HTTPS origin protocol policy";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a CloudFront with non HTTPS origin protocol policy";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a CloudFront with non HTTPS origin protocol policy";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a CloudFront with non HTTPS origin protocol policy";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a CloudFront with non HTTPS origin protocol policy";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a CloudFront with non HTTPS origin protocol policy";
                }

            case CloudFrontStaticSiteACL:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a static CloudFront with no web ACL";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a static CloudFront with no web ACL";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a static CloudFront with no web ACL";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a static CloudFront with no web ACL";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a static CloudFront with no web ACL";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a static CloudFront with no web ACL";
                }

            case CloudFrontUnsignedURLUnfoundCookies:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unsigned CloudFront with cookie forwarding enabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unsigned CloudFront with cookie forwarding enabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unsigned CloudFront with cookie forwarding enabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unsigned CloudFront with cookie forwarding enabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unsigned CloudFront with cookie forwarding enabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unsigned CloudFront with cookie forwarding enabled";
                }

            case CloudTrailDisabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an account where CloudTrail was disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an account where CloudTrail was disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an account where CloudTrail was disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an account where CloudTrail was disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an account where CloudTrail was disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an account where CloudTrail was disabled";
                }

            case CodeBuildVPCMisconfigurationInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a CodeBuild project that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a CodeBuild project that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a CodeBuild project that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a CodeBuild project that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a CodeBuild project that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a CodeBuild project that was launched within a disallowed subnet";
                }

            case CodeBuildVPCMisconfigurationMissingVPC:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a CodeBuild project that is missing a VPC";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a CodeBuild project that is missing a VPC";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a CodeBuild project that is missing a VPC";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a CodeBuild project that is missing a VPC";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a CodeBuild project that is missing a VPC";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a CodeBuild project that is missing a VPC";
                }

            case CognitoUncomplexedPasswords:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Cognito user pool with an password not meeting complexity policy";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Cognito user pool with an password not meeting complexity policy";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Cognito user pool with an password not meeting complexity policy";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Cognito user pool with an password not meeting complexity policy";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Cognito user pool with an password not meeting complexity policy";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Cognito user pool with an password not meeting complexity policy";
                }

            case CWEventsCustomerAlarmsOnEmoryInstances:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a CloudWatch alarm that is created on a RHEDcloud managed instance";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a CloudWatch alarm that is created on a RHEDcloud managed instance";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a CloudWatch alarm that is created on a RHEDcloud managed instance";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a CloudWatch alarm that is created on a RHEDcloud managed instance";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a CloudWatch alarm that is created on a RHEDcloud managed instance";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a CloudWatch alarm that is created on a RHEDcloud managed instance";
                }

            case DAXClusterInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a DAX cluster that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a DAX cluster that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a DAX cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a DAX cluster that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a DAX cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a DAX cluster that was launched within a disallowed subnet";
                }

            case RdsDBClusterPendingReboot:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a DB cluster pending reboot";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a DB cluster pending reboot";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a DB cluster pending reboot";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a DB cluster pending reboot";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a DB cluster pending reboot";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a DB cluster pending reboot";
                }

            case RdsDBClusterUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a DB cluster where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a DB cluster where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a DB cluster where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a DB cluster where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a DB cluster where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a DB cluster where transport encryption is disabled";
                }

            case DSIneligibleDirectory:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Directory Service not using the Enterprise Edition";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Directory Service not using the Enterprise Edition";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Directory Service not using the Enterprise Edition";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Directory Service not using the Enterprise Edition";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Directory Service not using the Enterprise Edition";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Directory Service not using the Enterprise Edition";
                }

            case DynamoDbUnencryptedDatabase:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unencrypted DynamoDB";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unencrypted DynamoDB";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unencrypted DynamoDB";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unencrypted DynamoDB";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unencrypted DynamoDB";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unencrypted DynamoDB";
                }

            case EbsUnencryptedSecondaryVolume:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unencrypted EBS secondary volume";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unencrypted EBS secondary volume";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unencrypted EBS secondary volume";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unencrypted EBS secondary volume";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unencrypted EBS secondary volume";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unencrypted EBS secondary volume";
                }

            case EC2CustomerInstancesInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EC2 instance that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EC2 instance that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EC2 instance that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EC2 instance that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EC2 instance that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EC2 instance that was launched within a disallowed subnet";
                }

            case EC2FleetInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EC2 fleet that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EC2 fleet that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EC2 fleet that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EC2 fleet that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EC2 fleet that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EC2 fleet that was launched within a disallowed subnet";
                }

            case EC2SpotFleetScheduledInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EC2 Spot Fleet request that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EC2 Spot Fleet request that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EC2 Spot Fleet request that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EC2 Spot Fleet request that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EC2 Spot Fleet request that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EC2 Spot Fleet request that was launched within a disallowed subnet";
                }

            case EC2SpotScheduledInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EC2 Spot Instance request that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EC2 Spot Instance request that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EC2 Spot Instance request that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EC2 Spot Instance request that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EC2 Spot Instance request that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EC2 Spot Instance request that was launched within a disallowed subnet";
                }

            case ECSLaunchedWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an ECS cluster that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an ECS cluster that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an ECS cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an ECS cluster that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an ECS cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an ECS cluster that was launched within a disallowed subnet";
                }

            case ElasticacheRedisDisabledRedisCommands:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redis ElastiCache cluster where transit encryption is disabled or auth token is disabled";
                }

            case ElasticacheRedisUnencryptedRest:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redis ElastiCache cluster where REST encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redis ElastiCache cluster where REST encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redis ElastiCache cluster where REST encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redis ElastiCache cluster where REST encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redis ElastiCache cluster where REST encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redis ElastiCache cluster where REST encryption is disabled";
                }

            case ElasticacheRedisUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redis ElastiCache cluster where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redis ElastiCache cluster where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redis ElastiCache cluster where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redis ElastiCache cluster where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redis ElastiCache cluster where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redis ElastiCache cluster where transport encryption is disabled";
                }

            case ElasticBeanstalkDisabledManagedUpdates:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an Elastic Beanstalk environment where managed updates are disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an Elastic Beanstalk environment where managed updates are disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an Elastic Beanstalk environment where managed updates are disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an Elastic Beanstalk environment where managed updates are disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an Elastic Beanstalk environment where managed updates are disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an Elastic Beanstalk environment where managed updates are disabled";
                }

            case ElasticBeanstalkEnvironmentWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an Elastic Beanstalk environment that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an Elastic Beanstalk environment that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an Elastic Beanstalk environment that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an Elastic Beanstalk environment that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an Elastic Beanstalk environment that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an Elastic Beanstalk environment that was launched within a disallowed subnet";
                }

            case ElasticSearchInMgmtSubnet:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an Elasticsearch Service domain that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an Elasticsearch Service domain that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an Elasticsearch Service domain that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an Elasticsearch Service domain that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an Elasticsearch Service domain that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an Elasticsearch Service domain that was launched within a disallowed subnet";
                }

            case ELBLoggingDisabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an Elastic Load Balancing setup where logging is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an Elastic Load Balancing setup where logging is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an Elastic Load Balancing setup where logging is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an Elastic Load Balancing setup where logging is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an Elastic Load Balancing setup where logging is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an Elastic Load Balancing setup where logging is disabled";
                }

            case EMRClusterUnencryptedRest:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EMR Cluster where REST encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EMR Cluster where REST encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EMR Cluster where REST encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EMR Cluster where REST encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EMR Cluster where REST encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EMR Cluster where REST encryption is disabled";
                }

            case EMRClusterUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EMR cluster where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EMR cluster where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EMR cluster where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EMR cluster where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EMR cluster where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EMR cluster where transport encryption is disabled";
                }

            case EMRLaunchedWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an EMR cluster that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an EMR cluster that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an EMR cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an EMR cluster that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an EMR cluster that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an EMR cluster that was launched within a disallowed subnet";
                }

            case GuardDutyMisconfiguration:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an account that has misconfigured GuardDuty detectors";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an account that has misconfigured GuardDuty detectors";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an account that has misconfigured GuardDuty detectors";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an account that has misconfigured GuardDuty detectors";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an account that has misconfigured GuardDuty detectors";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an account that has misconfigured GuardDuty detectors";
                }

            case IamExternalTrustRelationshipPolicy:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an IAM role that allows external trust";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an IAM role that allows external trust";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an IAM role that allows external trust";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an IAM role that allows external trust";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an IAM role that allows external trust";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an IAM role that allows external trust";
                }

            case IamRestrictionPoliciesDetached:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an IAM user / group / role where required policies are not attached";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an IAM user / group / role where required policies are not attached";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an IAM user / group / role where required policies are not attached";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an IAM user / group / role where required policies are not attached";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an IAM user / group / role where required policies are not attached";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an IAM user / group / role where required policies are not attached";
                }

            case IamUnusedCredentialDisableApiKey:               
            case IamUnusedCredentialWarningApiKey:
            case IamUnusedCredentialDeleteApiKey:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an access key that has not been used recently";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an access key that has not been used recently";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an access key that has not been used recently";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an access key that has not been used recently";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an access key that has not been used recently";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an access key that has not been used recently";
                }
                

            case IamUnusedCredentialAwsConsole:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an IAM user that has not logged in recently";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an IAM user that has not logged in recently";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an IAM user that has not logged in recently";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an IAM user that has not logged in recently";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an IAM user that has not logged in recently";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an IAM user that has not logged in recently";
                }

            case KMSExternallySharedKey:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a KMS key that allows external trust";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a KMS key that allows external trust";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a KMS key that allows external trust";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a KMS key that allows external trust";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a KMS key that allows external trust";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a KMS key that allows external trust";
                }

            case LambdaResidesWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Lambda function that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Lambda function that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Lambda function that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Lambda function that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Lambda function that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Lambda function that was launched within a disallowed subnet";
                }

            case MediaStorePublicContainer:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a public Elemental MediaStore container";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a public Elemental MediaStore container";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a public Elemental MediaStore container";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a public Elemental MediaStore container";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a public Elemental MediaStore container";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a public Elemental MediaStore container";
                }

            case RdsHipaaIneligibleMemcached:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Memcached ElastiCache cluster";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Memcached ElastiCache cluster";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Memcached ElastiCache cluster";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Memcached ElastiCache cluster";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Memcached ElastiCache cluster";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Memcached ElastiCache cluster";
                }

            case RdsHipaaIneligibleSqlServerDatabase:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a RDS SQL Server Express Server";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a RDS SQL Server Express Server";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a RDS SQL Server Express Server";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a RDS SQL Server Express Server";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a RDS SQL Server Express Server";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a RDS SQL Server Express Server";
                }

            case RdsLaunchedWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a RDS instance that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a RDS instance that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a RDS instance that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a RDS instance that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a RDS instance that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a RDS instance that was launched within a disallowed subnet";
                }

            case RdsOracleUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a RDS Oracle instance where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a RDS Oracle instance where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a RDS Oracle instance where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a RDS Oracle instance where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a RDS Oracle instance where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a RDS Oracle instance where transport encryption is disabled";
                }

            case RdsPostgreSQLUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a RDS Postgres instance where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a RDS Postgres instance where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a RDS Postgres instance where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a RDS Postgres instance where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a RDS Postgres instance where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a RDS Postgres instance where transport encryption is disabled";
                }

            case RdsSqlServerUnencryptedTransport:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a RDS SQL Server instance where transport encryption is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a RDS SQL Server instance where transport encryption is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a RDS SQL Server instance where transport encryption is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a RDS SQL Server instance where transport encryption is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a RDS SQL Server instance where transport encryption is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a RDS SQL Server instance where transport encryption is disabled";
                }

            case RdsUnencryptedDatabase:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unencrypted RDS instance";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unencrypted RDS instance";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unencrypted RDS instance";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unencrypted RDS instance";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unencrypted RDS instance";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unencrypted RDS instance";
                }

            case RedshiftPendingReboot:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redshift cluster pending reboot";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redshift cluster pending reboot";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redshift cluster pending reboot";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redshift cluster pending reboot";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redshift cluster pending reboot";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redshift cluster pending reboot";
                }

            case RedshiftSSLDisabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redshift cluster where encryption in transit is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redshift cluster where encryption in transit is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redshift cluster where encryption in transit is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redshift cluster where encryption in transit is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redshift cluster where encryption in transit is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redshift cluster with SSL disabled";
                }

            case RedshiftUnencryptedRest:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Redshift cluster where encryption at rest is disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Redshift cluster where encryption at rest is disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Redshift cluster where encryption at rest is disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Redshift cluster encryption at rest is disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Redshift cluster where encryption at rest is disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Redshift cluster where encryption at rest is disabled";
                }

            case RootUserPasswordChange:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an account where the root password was changed";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an account where the root password was changed";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an account where the root password was changed";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an account where the root password was changed";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an account where the root password was changed";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an account where the root password was changed";
                }

            case S3BucketsResideOutsideOfUSRegions:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a S3 bucket with a region outside the US";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a S3 bucket with a region outside the US";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a S3 bucket with a region outside the US";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a S3 bucket with a region outside the US";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a S3 bucket with a region outside the US";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a S3 bucket with a region outside the US";
                }

            case S3PublicAccessBlockMisconfiguration:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a public S3 bucket with a Public Access Block misconfiguration";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a public S3 bucket with a Public Access Block misconfiguration";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a public S3 bucket with a Public Access Block misconfiguration";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a public S3 bucket with a Public Access Block misconfiguration";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a public S3 bucket with a Public Access Block misconfiguration";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a public S3 bucket with a Public Access Block misconfiguration";
                }

            case S3PublicBucketAcl:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a public S3 bucket ACL";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a public S3 bucket ACL";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a public S3 bucket ACL";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a public S3 bucket ACL";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a public S3 bucket ACL";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a public S3 bucket ACL";
                }

            case S3PublicBucketObject:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a public S3 bucket object";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a public S3 bucket object";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a public S3 bucket object";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a public S3 bucket object";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a public S3 bucket object";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a public S3 bucket object";
                }

            case S3UnencryptedBucket:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unencrypted S3 bucket";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unencrypted S3 bucket";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unencrypted S3 bucket";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unencrypted S3 bucket";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unencrypted S3 bucket";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unencrypted S3 bucket";
                }

            case SageMakerVPCMisconfiguration:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a SageMaker instance not located within provisioned VPC";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a SageMaker instance not located within provisioned VPC";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a SageMaker instance not located within provisioned VPC";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a SageMaker instance not located within provisioned VPC";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a SageMaker instance not located within provisioned VPC";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a SageMaker instance not located within provisioned VPC";
                }

            case SageMakerWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a SageMaker instance that was launched within a disallowed subnet";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a SageMaker instance that was launched within a disallowed subnet";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a SageMaker instance that was launched within a disallowed subnet";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a SageMaker instance that was launched within a disallowed subnet";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a SageMaker instance that was launched within a disallowed subnet";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a SageMaker instance that was launched within a disallowed subnet";
                }

            case SQSExternallySharedQueue:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a SQS queue that allows external trust";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a SQS queue that allows external trust";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a SQS queue that allows external trust";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a SQS queue that allows external trust";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a SQS queue that allows external trust";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a SQS queue that allows external trust";
                }

            case SSMUnencryptedParameters:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unencrypted Systems Manager Parameter Store";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unencrypted Systems Manager Parameter Store";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unencrypted Systems Manager Parameter Store";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unencrypted Systems Manager Parameter Store";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unencrypted Systems Manager Parameter Store";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unencrypted Systems Manager Parameter Store";
                }

            case TagPolicyViolationNoTag:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a tag policy violation for a resource with no tag";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a tag policy violation for a resource with no tag";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a tag policy violation for a resource with no tag";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a tag policy violation for a resource with no tag";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a tag policy violation for a resource with no tag";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a tag policy violation for a resource with no tag";
                }

            case TagPolicyViolationBadValue:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a tag policy violation for a resource with a bad tag value";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a tag policy violation for a resource with a bad tag value";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a tag policy violation for a resource with a bad tag value";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a tag policy violation for a resource with a bad tag value";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a tag policy violation for a resource with a bad tag value";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a tag policy violation for a resource with a bad tag value";
                }

            case TagPolicyViolationNoProfileTag:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a tag policy violation for a resource with no profile tag";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a tag policy violation for a resource with no profile tag";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a tag policy violation for a resource with no profile tag";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a tag policy violation for a resource with no profile tag";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a tag policy violation for a resource with no profile tag";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a tag policy violation for a resource with no profile tag";
                }

            case TagPolicyViolationNoTagsOnAccount:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a tag policy violation for no tags on account";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a tag policy violation for no tags on account";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a tag policy violation for no tags on account";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a tag policy violation for no tags on account";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a tag policy violation for no tags on account";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a tag policy violation for no tags on account";
                }

            case TagPolicyViolationNoRTPSEntry:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a tag policy violation for a resource with no Resource Tag Profile Service entry";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a tag policy violation for a resource with no Resource Tag Profile Service entry";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a tag policy violation for a resource with no Resource Tag Profile Service entry";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a tag policy violation for a resource with no Resource Tag Profile Service entry";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a tag policy violation for a resource with no Resource Tag Profile Service entry";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a tag policy violation for a resource with no Resource Tag Profile Service entry";
                }

            case UnregisteredVpc:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an unregistered VPC";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an unregistered VPC";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an unregistered VPC";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an unregistered VPC";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an unregistered VPC";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an unregistered VPC";
                }

            case VPCFlowLogsDisabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a VPC with Flow Logs disabled";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a VPC with Flow Logs disabled";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a VPC with Flow Logs disabled";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a VPC with Flow Logs disabled";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a VPC with Flow Logs disabled";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a VPC with Flow Logs disabled";
                }

            case WorkDocsNonEmoryDomain:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a WorkDocs user with an email outside the Emory (Healthcare) domain";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a WorkDocs user with an email outside the Emory (Healthcare) domain";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a WorkDocs user with an email outside the Emory (Healthcare) domain";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a WorkDocs user with an email outside the Emory (Healthcare) domain";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a WorkDocs user with an email outside the Emory (Healthcare) domain";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a WorkDocs user with an email outside the Emory (Healthcare) domain";
                }

            case WorkSpacesUnencryptedSpace:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a Workspace with an unencrypted volume";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a Workspace with an unencrypted volume";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a Workspace with an unencrypted volume";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a Workspace with an unencrypted volume";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a Workspace with an unencrypted volume";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a Workspace with an unencrypted volume";
                }

            case Example: // should never happen
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated an example";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for an example";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for an example";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for an example";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for an example";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found an example";
                }

            case ServiceMonitoring: // should never happen
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a service monitoring";
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a service monitoring";
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for a service monitoring";
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for a service monitoring";
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for a service monitoring";
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a service monitoring";
                }

            default: // should never happen
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                        return "Remediated a risk for " + detectionType.name();
                    case REMEDIATION_FAILED:
                        return "Remediation failed for a risk for " + detectionType.name();
                    case REMEDIATION_POSTPONED:
                        return "Remediation postponed for " + detectionType.name();
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return "Remediation no longer required for " + detectionType.name();
                    case REMEDIATION_TIMEOUT:
                        return "Remediation timed out for " + detectionType.name();
                    case REMEDIATION_NOTIFICATION_ONLY: // fallthrough
                    default:
                        return "Found a risk for " + detectionType.name();
                }
        }
    }

    private String getNotificationMessageBodyForDetectionError(DetectionResult detectionResult, DetectionStatus detectionStatus) {
        String detectionMessage;

        switch (detectionStatus) {
            case DETECTION_FAILED:
                detectionMessage = "The detection failed.  " + detectionResult.getError(0) + ".";
                break;
            case DETECTION_TIMEOUT:
                detectionMessage = "The detection timed out.  " + detectionResult.getError(0) + ".";
                break;
            // currently, the other statuses will not pass evaluation,
            // but just in case they do, fallback to a sensible message
            case DETECTION_SUCCESS:
                detectionMessage = "The detection was successful.";
                break;
            default:
                detectionMessage = "The detection was " + detectionStatus.name() + ".";
                break;
        }

        // additional notification text is not used for detection error notifications
        return detectionMessage;
    }

    private String getNotificationMessageBodyForRemediation(SecurityRiskDetection detection, DetectedSecurityRisk risk, RemediationStatus remediationStatus) {
        String remediationAction;

        switch (remediationStatus) {
            case REMEDIATION_SUCCESS:
                remediationAction = "The remediation was successful.  " + risk.getRemediationResult().getDescription();
                break;
            case REMEDIATION_FAILED:
                remediationAction = "The remediation failed.  " + risk.getRemediationResult().getError(0) + ".";
                break;
            case REMEDIATION_NOTIFICATION_ONLY:
                remediationAction = "The remediation action was to send a notification.";
                break;
            case REMEDIATION_POSTPONED:
                remediationAction = "The remediation action was postponed.  " + risk.getRemediationResult().getDescription();
                break;
            case REMEDIATION_NO_LONGER_REQUIRED:
                remediationAction = "The remediation action was no longer required.  " + risk.getRemediationResult().getDescription();
                break;
            case REMEDIATION_TIMEOUT:
                remediationAction = "The remediation timed out.  " + risk.getRemediationResult().getError(0) + ".";
                break;
            default:
                remediationAction = "The remediation action was taken.";
                break;
        }

        String additionalNotificationText = getAdditionalNotificationText(detection, risk);

        // message body is made up of the ARN and the action taken to remediate the risk
        // plus any additional notification text taken from the app config
        return ((additionalNotificationText == null) ? "" : (additionalNotificationText + "\n\n"))
                + "The Amazon Resource Name (ARN) is " + risk.getAmazonResourceName() + ".  " + remediationAction;
    }

    private String getAdditionalNotificationText(SecurityRiskDetection detection, DetectedSecurityRisk risk) {
    	
    	if ( risk.getAdditionalNotificationText() != null ) {
    		return risk.getAdditionalNotificationText();
    	}
    	
    	//TODO: replace legacy code. See AbstractSecurityRiskDetector.newDetectionResult
    	//legacy code below

        for (Object error : detection.getDetectionResult().getError()) {
            String err = (String) error;
            if (err.startsWith("ANT:")) {  // find ANT: in AbstractSecurityRiskDetector
                return err.substring(4);
            }
        }
        return null;
    }

    private String priorityMappingForRemediation(DetectionType detectionType, RemediationStatus remediationStatus) {
        switch (detectionType) {
            case AccountInWrongOrganizationalUnit:
            case AutoscalingGroupInMgmtSubnet:
            case CodeBuildVPCMisconfigurationMissingVPC:
            case CodeBuildVPCMisconfigurationInMgmtSubnet:
            case DAXClusterInMgmtSubnet:
            case RdsDBClusterPendingReboot:
            case RdsDBClusterUnencryptedTransport:
            case EbsUnencryptedSecondaryVolume:
            case EC2CustomerInstancesInMgmtSubnet:
            case EC2FleetInMgmtSubnet:
            case EC2SpotScheduledInMgmtSubnet:
            case EC2SpotFleetScheduledInMgmtSubnet:
            case ECSLaunchedWithinManagementSubnets:
            case ElasticacheRedisDisabledRedisCommands:
            case ElasticacheRedisUnencryptedRest:
            case ElasticacheRedisUnencryptedTransport:
            case ElasticBeanstalkEnvironmentWithinManagementSubnets:
            case ElasticSearchInMgmtSubnet:
            case EMRClusterUnencryptedRest:
            case EMRClusterUnencryptedTransport:
            case EMRLaunchedWithinManagementSubnets:
            case IamUnusedCredentialAwsConsole:
            case IamUnusedCredentialDeleteApiKey:
            case IamUnusedCredentialDisableApiKey:
            case IamUnusedCredentialWarningApiKey:
            case LambdaResidesWithinManagementSubnets:
            case MediaStorePublicContainer:
            case RdsHipaaIneligibleMemcached:
            case RdsHipaaIneligibleSqlServerDatabase:
            case RdsLaunchedWithinManagementSubnets:
            case RdsOracleUnencryptedTransport:
            case RdsPostgreSQLUnencryptedTransport:
            case RdsSqlServerUnencryptedTransport:
            case RdsUnencryptedDatabase:
            case RedshiftPendingReboot:
            case RedshiftSSLDisabled:
            case RedshiftUnencryptedRest:
            case RootUserPasswordChange:
            case S3BucketsResideOutsideOfUSRegions:
            case SageMakerVPCMisconfiguration:
            case SageMakerWithinManagementSubnets:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                    case REMEDIATION_FAILED:
                    case REMEDIATION_TIMEOUT:
                    case REMEDIATION_POSTPONED:
                    case REMEDIATION_NOTIFICATION_ONLY:
                        return PRIORITY_HIGH;
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return PRIORITY_LOW;
                }
                break;
            case CloudFrontNonHTTPSOriginEnabled:
            case CloudFrontStaticSiteACL:
            case CloudFrontUnsignedURLUnfoundCookies:
            case CognitoUncomplexedPasswords:
            case CWEventsCustomerAlarmsOnEmoryInstances:
            case ElasticBeanstalkDisabledManagedUpdates:
            case GuardDutyMisconfiguration:
            case IamExternalTrustRelationshipPolicy:
            case IamRestrictionPoliciesDetached:
            case KMSExternallySharedKey:
            case S3PublicBucketAcl:
            case S3PublicBucketObject:
            case S3PublicAccessBlockMisconfiguration:
            case S3UnencryptedBucket:
            case SQSExternallySharedQueue:
            case SSMUnencryptedParameters:
            case UnregisteredVpc:
            case VPCFlowLogsDisabled:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                    case REMEDIATION_FAILED:
                    case REMEDIATION_TIMEOUT:
                    case REMEDIATION_POSTPONED:
                    case REMEDIATION_NOTIFICATION_ONLY:
                        return PRIORITY_MEDIUM;
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return PRIORITY_LOW;
                }
                break;
            case ApiGatewayCaching:
            case CloudTrailDisabled:
            case DSIneligibleDirectory:
            case DynamoDbUnencryptedDatabase:
            case ELBLoggingDisabled:
            case TagPolicyViolationNoTag:
            case TagPolicyViolationBadValue:
            case TagPolicyViolationNoProfileTag:
            case TagPolicyViolationNoTagsOnAccount:
            case TagPolicyViolationNoRTPSEntry:
            case WorkDocsNonEmoryDomain:
            case WorkSpacesUnencryptedSpace:
            case Example:
            case ServiceMonitoring:
                switch (remediationStatus) {
                    case REMEDIATION_SUCCESS:
                    case REMEDIATION_FAILED:
                    case REMEDIATION_TIMEOUT:
                    case REMEDIATION_POSTPONED:
                    case REMEDIATION_NOTIFICATION_ONLY:
                    case REMEDIATION_NO_LONGER_REQUIRED:
                        return PRIORITY_LOW;
                }
                break;
        }
        logger.error(LOGTAG + "Unhandled priority mapping for " + detectionType);
        return null;
    }
}
