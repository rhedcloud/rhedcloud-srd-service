package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudformation.AmazonCloudFormation;
import com.amazonaws.services.cloudformation.AmazonCloudFormationClient;
import com.amazonaws.services.cloudformation.model.Capability;
import com.amazonaws.services.cloudformation.model.CreateStackRequest;
import com.amazonaws.services.cloudformation.model.Parameter;
import com.amazonaws.services.guardduty.AmazonGuardDuty;
import com.amazonaws.services.guardduty.AmazonGuardDutyClient;
import com.amazonaws.services.guardduty.model.AcceptInvitationRequest;
import com.amazonaws.services.guardduty.model.AccountDetail;
import com.amazonaws.services.guardduty.model.AmazonGuardDutyException;
import com.amazonaws.services.guardduty.model.CreateDetectorRequest;
import com.amazonaws.services.guardduty.model.CreateDetectorResult;
import com.amazonaws.services.guardduty.model.CreateMembersRequest;
import com.amazonaws.services.guardduty.model.DisassociateFromMasterAccountRequest;
import com.amazonaws.services.guardduty.model.Invitation;
import com.amazonaws.services.guardduty.model.InviteMembersRequest;
import com.amazonaws.services.guardduty.model.ListDetectorsRequest;
import com.amazonaws.services.guardduty.model.ListDetectorsResult;
import com.amazonaws.services.guardduty.model.ListInvitationsRequest;
import com.amazonaws.services.guardduty.model.ListInvitationsResult;
import com.amazonaws.services.guardduty.model.UpdateDetectorRequest;
import com.amazonaws.services.organizations.AWSOrganizations;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.AWSOrgUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Remediate by enabling the detector, creating a detector, and/or re-associating to the master account.
 *
 * <p>Security: Enforce, correct and alert<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.GuardDutyMisconfigurationDetector the detector
 */
public class GuardDutyMisconfigurationRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private String guardDutyMasterAccountId;
    private String guardDutySplunkCfnTemplate;
    private String hecToken;
    private String hecEndpoint;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);

        try {
            Properties properties = appConfig.getProperties("GuardDutyMisconfiguration");
            guardDutyMasterAccountId = properties.getProperty("GuardDutyMasterAccountID");
            guardDutySplunkCfnTemplate = properties.getProperty("GuardDutySplunkCfnTemplate");
            hecToken = properties.getProperty("HECToken");
            hecEndpoint = properties.getProperty("HECEndpoint");
        } catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:guardduty:", srContext.LOGTAG, DetectionType.GuardDutyMisconfiguration))
            return;
        // like arn:aws:guardduty:us-east-1:123456789012:detector/detectorId
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];

        AWSOrganizations orgClient = getOrganizationsClient(srContext);

        // these properties may not be set depending on the detected risk
        String detectorId = detected.getProperties().getProperty("detectorId");
        String status = detected.getProperties().getProperty("status");
        String masterAccountId = detected.getProperties().getProperty("masterAccountId");

        String remediationDescription = "";

        try {
            AmazonGuardDuty memberClient = getClient(accountId, region, srContext.getMetricCollector(), AmazonGuardDutyClient.class);

            /*
             * if there is no detector then create it
             * otherwise, if the detector is suspended then enable it
             */
            if (detectorId == null) {
                try {
                    CreateDetectorRequest createDetectorRequest = new CreateDetectorRequest()
                            .withEnable(true);  // create in the enabled state
                    CreateDetectorResult createDetectorResult = memberClient.createDetector(createDetectorRequest);
                    detectorId = createDetectorResult.getDetectorId();
                    remediationDescription += "Created detector with ID " + detectorId + ". ";
                }
                catch (AmazonGuardDutyException e) {
                    setError(detected, srContext.LOGTAG, "Failed to create GuardDuty detector", e);
                    return;
                }
            }
            else if (!status.equals("ENABLED")) {
                try {
                    UpdateDetectorRequest updateDetectorRequest = new UpdateDetectorRequest()
                            .withDetectorId(detectorId)
                            .withEnable(true);  // re-enable the detector
                    memberClient.updateDetector(updateDetectorRequest);
                    remediationDescription += "Enabled detector with ID " + detectorId + ". ";
                }
                catch (AmazonGuardDutyException e) {
                    setError(detected, srContext.LOGTAG, "Failed to enable GuardDuty detector", e);
                    return;
                }
            }

            /*
             * if there's already a master account association, verify that it's the configured GuardDuty Master.
             * if needed, make the association between the GuardDuty Master and the current (member) account
             */
            if (masterAccountId != null && !masterAccountId.equals(guardDutyMasterAccountId)) {
                try {
                    // master is NOT the configured GuardDuty Master so disassociate it

                    memberClient.disassociateFromMasterAccount(new DisassociateFromMasterAccountRequest().withDetectorId(detectorId));
                    remediationDescription += "Member disassociated from incorrect GuardDuty Master " + masterAccountId + ". ";

                    // null this out so that the correct association is made
                    masterAccountId = null;
                }
                catch (AmazonGuardDutyException e) {
                    setError(detected, srContext.LOGTAG, "Failed to disassociated from incorrect GuardDuty Master", e);
                    return;
                }
            }

            if (masterAccountId == null) {
                try {
                    associateToMaster(detected, srContext, memberClient, accountId, detectorId, region, orgClient);
                    remediationDescription += "Member associated to GuardDuty master. ";
                }
                catch (AmazonGuardDutyException e) {
                    // error already set
                    return;
                }
            }

            setSuccess(detected, srContext.LOGTAG, remediationDescription);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to remediate GuardDuty detector", e);
        }
    }

    private void associateToMaster(DetectedSecurityRisk detected, SecurityRiskContext srContext,
                                   AmazonGuardDuty memberGdClient, String memberAccountId, String memberDetectorId,
                                   String region, AWSOrganizations orgClient) throws Exception {
        AmazonGuardDuty masterGdClient;
        List<String> masterDetectorsIds;
        try {
            masterGdClient = getClient(guardDutyMasterAccountId, region, srContext.getMetricCollector(), AmazonGuardDutyClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to build GuardDuty Master API client", e);
            throw new AmazonGuardDutyException(e.getMessage());
        }
        try {
            /*
             * A GuardDuty Master may not have a detector configured in all regions since new regions can be added
             * at any time by AWS.  if the detector is not found we will create one and run the Cloud Formation
             * template that sets up the infrastructure to send GuardDuty events to Splunk.
             */
            masterDetectorsIds = getMasterDetectorsIds(masterGdClient);
            if (masterDetectorsIds.size() == 0) {
                AmazonCloudFormation masterCfnClient = getClient(guardDutyMasterAccountId, region, srContext.getMetricCollector(), AmazonCloudFormationClient.class);
                createMasterDetector(masterGdClient, masterCfnClient);
                masterDetectorsIds = getMasterDetectorsIds(masterGdClient);
            }
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to get GuardDuty Master detector IDs", e);
            throw new AmazonGuardDutyException(e.getMessage());
        }

        // member account id and email address are needed to create a GaurdDuty member
        String memberAccountEmail = AWSOrgUtil.getInstance().getAccountEmailAddress(orgClient, memberAccountId);

        for (String masterDetectorId : masterDetectorsIds) {
            /*
             * from the Master, create the member and send the invitation
             */
            try {
                CreateMembersRequest masterCreateMembersRequest = new CreateMembersRequest()
                        .withDetectorId(masterDetectorId)
                        .withAccountDetails(new AccountDetail()
                                .withAccountId(memberAccountId)
                                .withEmail(memberAccountEmail));
                masterGdClient.createMembers(masterCreateMembersRequest);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Failed to create GuardDuty Member", e);
                throw new AmazonGuardDutyException(e.getMessage());
            }
            try {
                InviteMembersRequest masterInviteMembersRequest = new InviteMembersRequest()
                        .withDetectorId(masterDetectorId)
                        .withAccountIds(memberAccountId)
                        .withDisableEmailNotification(true);
                masterGdClient.inviteMembers(masterInviteMembersRequest);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Failed to invite GuardDuty Member", e);
                throw new AmazonGuardDutyException(e.getMessage());
            }


            /*
             * the results of the invitation request sent from the Master doesn't contain any identifying info,
             * so, as the member, we have to search through all invitations looking for the correct invitation.
             * there can only be one from the Master account.
             */
            List<Invitation> invitations = new ArrayList<>();
            long startedAt = System.currentTimeMillis();

            do {
                // don't loop forever - no longer than 5 minutes
                if ((System.currentTimeMillis() - startedAt) > (1000 * 60 * 5))
                    throw new AmazonGuardDutyException("Waited too long for invitation from " + guardDutyMasterAccountId + " to " + memberAccountId);
                // it takes a few seconds for the invitation to be available to the member so sleep before retrying
                try { Thread.sleep(1000); } catch (InterruptedException e) {/* ignore and check for the invitation */}

                ListInvitationsRequest memberListInvitationsRequest = new ListInvitationsRequest()
                        .withMaxResults(50);  // get as many as is allowed
                ListInvitationsResult memberListInvitationsResult;

                do {
                    try {
                        memberListInvitationsResult = memberGdClient.listInvitations(memberListInvitationsRequest);
                    }
                    catch (Exception e) {
                        setError(detected, srContext.LOGTAG, "Failed to list GuardDuty invitations", e);
                        throw new AmazonGuardDutyException(e.getMessage());
                    }

                    memberListInvitationsResult.getInvitations().stream()
                            .filter(invitation -> invitation.getAccountId().equals(guardDutyMasterAccountId)
                                    && invitation.getRelationshipStatus().equals("Invited"))
                            .collect(Collectors.toCollection(() -> invitations));

                    memberListInvitationsRequest.setNextToken(memberListInvitationsResult.getNextToken());
                } while (memberListInvitationsResult.getNextToken() != null);
            } while (invitations.size() == 0);

            // found the invitation so accept it
            Invitation invitation = invitations.get(0);

            try {
                AcceptInvitationRequest acceptInvitationRequest = new AcceptInvitationRequest()
                        .withInvitationId(invitation.getInvitationId())
                        .withMasterId(invitation.getAccountId())
                        .withDetectorId(memberDetectorId);
                memberGdClient.acceptInvitation(acceptInvitationRequest);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Failed to accept GuardDuty invitations", e);
                throw new AmazonGuardDutyException(e.getMessage());
            }
        }
    }

    /**
     * Get GuardDuty Master detector IDs.
     *
     * @param masterGdClient GuardDuty client for GuardDuty Master account
     * @return detector IDs
     */
    private List<String> getMasterDetectorsIds(AmazonGuardDuty masterGdClient) {
        ListDetectorsRequest masterListDetectorsRequest = new ListDetectorsRequest();
        ListDetectorsResult masterListDetectorsResult;

        List<String> masterDetectorsIds = new ArrayList<>();

        do {
            masterListDetectorsResult = masterGdClient.listDetectors(masterListDetectorsRequest);
            masterDetectorsIds.addAll(masterListDetectorsResult.getDetectorIds());
            masterListDetectorsRequest.setNextToken(masterListDetectorsResult.getNextToken());
        } while (masterListDetectorsResult.getNextToken() != null);

        return masterDetectorsIds;
    }

    /**
     * Create a GuardDuty Master detector and hook it up to Splunk.
     *
     * @param masterGdClient GuardDuty client for GuardDuty Master account
     * @param masterCfnClient CloudFormation client for GuardDuty Master account
     */
    private void createMasterDetector(AmazonGuardDuty masterGdClient, AmazonCloudFormation masterCfnClient) {
        CreateDetectorRequest createDetectorRequest = new CreateDetectorRequest()
                .withEnable(true);
        masterGdClient.createDetector(createDetectorRequest);

        // run the template for creating and configuring infrastructure to send GuardDuty events to Splunk via HEC
        CreateStackRequest createStackRequest = new CreateStackRequest()
                .withStackName("guardduty-splunk")
                .withTemplateURL(guardDutySplunkCfnTemplate)
                .withParameters(new Parameter().withParameterKey("stsExternalId").withParameterValue(guardDutyMasterAccountId))
                .withParameters(new Parameter().withParameterKey("HECToken").withParameterValue(hecToken))
                .withParameters(new Parameter().withParameterKey("HECEndpoint").withParameterValue(hecEndpoint))
                .withCapabilities(Capability.CAPABILITY_NAMED_IAM);
        masterCfnClient.createStack(createStackRequest);
    }
}
