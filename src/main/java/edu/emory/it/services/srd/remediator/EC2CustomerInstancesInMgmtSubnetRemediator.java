package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Remediates EC2 instances that are in a disallowed subnet and does not have a tag of RHEDcloud = true<br>
 *
 * <p>The remediator deletes the instance that is launched into a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, correct, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.EC2CustomerInstancesInMgmtSubnetDetector the detector
 */
public class EC2CustomerInstancesInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG, DetectionType.EC2CustomerInstancesInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String instanceId = arn[5].replace("instance/", "");
        String region = arn[3];

        final AmazonEC2 ec2;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Instance '" + instanceId + "'", e);
            return;
        }

        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(instanceId);
        DescribeInstancesResult describeInstancesResult = ec2.describeInstances(describeInstancesRequest);

        if (describeInstancesResult.getReservations() == null
                || describeInstancesResult.getReservations().isEmpty()
                || describeInstancesResult.getReservations().get(0).getInstances().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Instance '" + instanceId + "' cannot be found. It may have already been deleted.");
            return;
        }

        Instance instance = describeInstancesResult.getReservations().get(0).getInstances().get(0);

        boolean hasTag = instance.getTags().stream()
                .anyMatch(tag -> tag.getKey().equalsIgnoreCase("RHEDcloud") && tag.getValue().equalsIgnoreCase("true"));
        if (hasTag) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Instance '" + instanceId + "' has tag 'RHEDcloud'=true and should not be remediated.");
            return;
        }

        String instanceState = instance.getState().getName();

        if (instanceState.equals("shutting-down") || instanceState.equals("terminated")) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Instance '" + instanceId + "' already remediated, it is already terminated.");
            return;
        }

        //remediate
        TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest().withInstanceIds(instanceId);
        TerminateInstancesResult terminateInstancesResult = ec2.terminateInstances(terminateInstancesRequest);

        List<InstanceStateChange> terminatedInstances = terminateInstancesResult.getTerminatingInstances();

        setSuccess(detected, srContext.LOGTAG,
                "The following instances '"
                        + terminatedInstances.stream().map(InstanceStateChange::getInstanceId).collect(Collectors.joining(","))
                        + "' were terminated because they were launched within a disallowed subnet.");
    }
}
