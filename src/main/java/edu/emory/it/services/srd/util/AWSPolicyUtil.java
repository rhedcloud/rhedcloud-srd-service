package edu.emory.it.services.srd.util;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Statement;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import edu.emory.it.services.srd.exceptions.AWSPolicyUtilNoChangeException;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AWS Policy utils.
 */
public class AWSPolicyUtil {
    private static final String LOGTAG = "[AWSPolicyUtil] ";
    private static final Logger logger = OpenEaiObject.logger;

    private static final Pattern IAM_STS_ARN_PATTERN = Pattern.compile("^arn:aws:(iam|sts)::(\\d+):.*$");
    private static final ObjectMapper mapper = new ObjectMapper();

    public static String removeAllowToNonWhitelistedAccounts(Set<String> whitelistedAccounts, String policy)
            throws AWSPolicyUtilNoChangeException {
        boolean hasChanged = false;
        try {
            ObjectNode node = (ObjectNode) mapper.readTree(policy);
            ArrayNode statement = (ArrayNode) node.get("Statement");
            Iterator<JsonNode> it = statement.iterator();
            while (it.hasNext()) {
                ObjectNode current = (ObjectNode) it.next();
                String effect = null;
                if (current.get("Effect") != null) {
                    effect = current.get("Effect").asText();
                }
                JsonNode principal = current.get("Principal");


                if (effect != null && effect.equals("Allow") && principal != null) {
                    JsonNode newPrincipal;
                    try {
                        newPrincipal = AWSPolicyUtil.removePrincipalContainingOtherAccounts(whitelistedAccounts, principal);
                        hasChanged = true;
                        if (newPrincipal == null) {
                            it.remove();
                        } else {
                            current.replace("Principal", newPrincipal);
                        }
                    } catch (AWSPolicyUtilNoChangeException e) {

                    }
                }
            }

            if (!hasChanged) {
                throw new AWSPolicyUtilNoChangeException();
            }

            if (statement.size() == 0) {
                return "";
            }

            return node.toString();

        } catch (IOException e) {
            logger.fatal(LOGTAG + "Jackson exception: " + e.getMessage());
        }
        return null;
    }

    public static boolean policyIsAllowAndHasPrincipalNotEqualToAccount(Set<String> whitelistedAccounts, String policy) {
        if (policy == null || policy.isEmpty()) {
            return false;
        }
        try {
            JsonNode node = mapper.readTree(policy);
            ArrayNode statement = (ArrayNode) node.get("Statement");
            for (JsonNode current : statement) {
                String effect = null;
                if (current.get("Effect") != null) {
                    effect = current.get("Effect").asText();
                }
                JsonNode principal = current.get("Principal");

                JsonNode notPrincipal = current.get("NotPrincipal");

                if (effect != null && effect.equals("Allow")) {
                    if (notPrincipal != null) {
                        return true;
                    }
                    if (AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)) {
                        return true;
                    }
                }
            }
        }
        catch (IOException e) {
            logger.fatal(LOGTAG + "Jackson exception: " + e.getMessage());
        }
        return false;
    }


    public static boolean isPolicyPrincipalContainingOtherAccounts(Set<String> allowedAccounts, JsonNode principalNode) {
        //uses the pattern from the following document to detect accountIds not matching (https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_principal.html)

        if (principalNode == null) {
            return false;
        }

        // when it is value is *
        if (principalNode instanceof TextNode && "*".equals(principalNode.asText())) {
            return true;
        }

        if (principalNode.get("Federated") != null) {
        	String federatedArn = principalNode.get("Federated").asText();

        	for (String allowed: allowedAccounts) {
        		String allowedString = "arn:aws:iam::" + allowed + ":oidc-provider/oidc.eks"; //"arn:aws:iam::012345678902:oidc-provider/oidc.eks
            	if (federatedArn.startsWith(allowedString) ) {
            		logger.info(LOGTAG+allowed+" is exempt from the restriction");
            		return false;
            	}
        	}
            return true;
        }

        JsonNode awsNode = principalNode.get("AWS");

        if (awsNode == null) {
            return false;
        }


        if (awsNode instanceof TextNode) {
            ArrayNode aaData = new ArrayNode(JsonNodeFactory.instance);
            aaData.add(awsNode);
            awsNode = aaData;
        }

        if (awsNode instanceof ArrayNode) {
            Iterator<JsonNode> it = awsNode.iterator();
            while (it.hasNext()) {
                String item = it.next().asText();
                if ("*".equals(item)) {
                    return true;
                }
                Matcher matcher = IAM_STS_ARN_PATTERN.matcher(item);
                if (matcher.find() && !allowedAccounts.contains(matcher.group(2))) {
                    return true;
                }
            }
        }

        return false;
    }

    public static JsonNode removePrincipalContainingOtherAccounts(Set<String> accountsToIgnore, JsonNode principalNode) throws AWSPolicyUtilNoChangeException {

        // uses the pattern from the following document to detect accountIds not matching
        // https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_elements_principal.html

        if (principalNode == null) {
           throw new AWSPolicyUtilNoChangeException();
        }

        // when it is value is *
        if (principalNode instanceof TextNode && "*".equals(principalNode.asText())) {
            return null;
        }

        boolean hasChanged = false;
        ObjectNode objectNode = principalNode.deepCopy();
        if (principalNode.get("Federated") != null) {
            objectNode.remove("Federated");
            hasChanged = true;
        }

        JsonNode awsNode = principalNode.get("AWS");

        if (awsNode != null) {
            ArrayNode arrayNode = JsonNodeFactory.instance.arrayNode();
            if (awsNode instanceof TextNode) {
                ArrayNode aaData = new ArrayNode(JsonNodeFactory.instance);
                aaData.add(awsNode);
                awsNode = aaData;
            }

            if (awsNode instanceof ArrayNode) {
                Iterator<JsonNode> it = awsNode.iterator();
                while (it.hasNext()) {
                    String item = it.next().asText();
                    if ("*".equals(item)) {
                        hasChanged = true;
                        continue;
                    }
                    Matcher matcher = IAM_STS_ARN_PATTERN.matcher(item);
                    if (matcher.find() && !accountsToIgnore.contains(matcher.group(2))) {
                        hasChanged = true;
                        continue;
                    }

                    arrayNode.add(item);
                }
            }

            if (arrayNode.size() > 0) {
                objectNode.replace("AWS", arrayNode);
            } else {
                objectNode.remove("AWS");
            }
        }

        if (!hasChanged) {
            throw new AWSPolicyUtilNoChangeException();
        }

        if (objectNode.size() == 0) {
           return null;
        }

        return objectNode;
    }

    /*
     * This method is a Work In Progress and is not used in any detector.
     *
     * It was written in an attempt to fix a bug in the removeAllowToNonWhitelistedAccounts/removePrincipalContainingOtherAccounts
     * methods.  In a policy, if there is a statement that has an externally shared principal, the statement can
     * be removed entirely.  It can reach a point where the policy becomes invalid and AWS throws the exception:
     *     "Error putting key policy on key 'blah'.
     *      The exception is: The new key policy will not allow you to update the key policy in the future."
     * For example, this policy would cause this kind of error:
     *     {
     *         "Version": "2012-10-17",
     *         "Id": "policy-test",
     *         "Statement": [
     *             {
     *                 "Sid": "Enable IAM User Permissions",
     *                 "Effect": "Allow",
     *                 "Principal": { "AWS": "*" },
     *                 "Action": "kms:*",
     *                 "Resource": "*"
     *             },
     *             {
     *                 "Sid": "Allow access for Key Administrators",
     *                 "Effect": "Allow",
     *                 "Principal": { "AWS": "*" },
     *                 "Action": [ "kms:Create*", "kms:Describe*", "kms:Enable*", "kms:List*", "kms:Put*" ],
     *                 "Resource": "*"
     *             }
     *         ]
     *     }
     *
     * The idea of this new method is to leave statements intact but just substitute a valid principal
     * if all of the original principals are externally shared.
     * It mostly works but the com.amazonaws.auth.policy.Policy class doesn't handle NotPrincipal yet
     * and I didn't have time to figure out how to incorporate that part.
     */
    public static boolean WIP_removeExternallyShared(Policy policy, String substituteArn, String... whitelistedAccounts) {
        Set<String> uniqueWhitelistedAccounts = new HashSet<>(Arrays.asList(whitelistedAccounts));
        boolean policyUpdated = false;

        for (Statement statement : policy.getStatements()) {
            if (statement.getEffect() == Statement.Effect.Allow && !statement.getPrincipals().isEmpty()) {
                /*
                 * The statement effect is allow and there are principals.
                 * If any of the principals are ALL or if a principal is not one of the whitelisted accounts,
                 *  then it is granting external access to the resource.  They will be removed from the list
                 *  of principals in the policy statement.
                 * But, we have to be careful not to remove every principal.  If that's happened then we
                 *  just include the substituteArn since it is a non-external principal.
                 */
                boolean policyStatementUpdated = false;
                List<Principal> principals = new ArrayList<>();
                for (Principal principal : statement.getPrincipals()) {
                    // never ok for Principal to allow all
                    boolean all = principal.equals(Principal.All)
                            || principal.equals(Principal.AllUsers)
                            || principal.equals(Principal.AllServices)
                            || principal.equals(Principal.AllWebProviders);

                    if (!all && uniqueWhitelistedAccounts.contains(principal.getId())) {
                        principals.add(principal);
                    }
                    else {
                        policyStatementUpdated = true;
                    }
                }

                /*
                 * But, we have to be careful not to remove every principal.  If that's happened then we
                 *  just include the substituteArn since it is a non-external principal.
                 */
                if (principals.size() == 0) {
                    principals.add(new Principal(substituteArn));
                }

                statement.setPrincipals(principals);
                if (policyStatementUpdated) {
                    statement.setId(statement.getId() + " Updated by SRD");
                    policyUpdated = true;
                }
            }
        }
        return policyUpdated;
    }
}
