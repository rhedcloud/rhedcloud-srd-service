package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClient;
import com.amazonaws.services.ecs.model.DescribeServicesRequest;
import com.amazonaws.services.ecs.model.DescribeServicesResult;
import com.amazonaws.services.ecs.model.ListClustersRequest;
import com.amazonaws.services.ecs.model.ListClustersResult;
import com.amazonaws.services.ecs.model.ListServicesRequest;
import com.amazonaws.services.ecs.model.ListServicesResult;
import com.amazonaws.services.ecs.model.Service;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.List;
import java.util.Set;

/**
 * Detect Elastic Container Service (ECS) cluster services in a disallowed subnet.
 *
 * @see edu.emory.it.services.srd.remediator.ECSLaunchedWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ECSLaunchedWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonEC2 ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                final AmazonECS ecsClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonECSClient.class);

                // collect the IDs of the disallowed subnets
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);

                // we'll get the details about the services running in each Elastic Container Service cluster.
                // service details tell us about the subnets that it is allowed to be launched in.
                // the MGMT and Attachment subnets must not be used.

                String listClustersRequestToken = null;
                do {
                    ListClustersRequest listClustersRequest = new ListClustersRequest()
                            .withMaxResults(100);  // get as many as is allowed
                    // deal with pagination
                    if (listClustersRequestToken != null)
                        listClustersRequest.setNextToken(listClustersRequestToken);
                    ListClustersResult listClustersResult = ecsClient.listClusters(listClustersRequest);
                    listClustersRequestToken = listClustersResult.getNextToken();

                    // inspect the services in each of the clusters
                    for (String clusterArn : listClustersResult.getClusterArns()) {
                        String listServicesRequestToken = null;
                        do {
                            ListServicesRequest listServicesRequest = new ListServicesRequest()
                                    .withCluster(clusterArn)
                                    .withMaxResults(10);  // get as many as is allowed
                            // deal with pagination
                            if (listServicesRequestToken != null)
                                listServicesRequest.setNextToken(listServicesRequestToken);
                            ListServicesResult listServicesResult = ecsClient.listServices(listServicesRequest);
                            listServicesRequestToken = listServicesResult.getNextToken();

                            // the describeServices() call can only operate on a maximum of 10 services at a time
                            // so we have to chunk the service ARNs to fit in those limits
                            List<String> serviceArns = listServicesResult.getServiceArns();
                            int fromIndex, toIndex;
                            final int CHUNK_SIZE = 10;

                            for (fromIndex = 0; fromIndex < serviceArns.size(); fromIndex += CHUNK_SIZE) {
                                toIndex = Math.min(serviceArns.size(), fromIndex + CHUNK_SIZE);

                                DescribeServicesRequest describeServicesRequest = new DescribeServicesRequest()
                                        .withCluster(clusterArn)
                                        .withServices(serviceArns.subList(fromIndex, toIndex));
                                DescribeServicesResult describeServicesResult = ecsClient.describeServices(describeServicesRequest);

                                describeServicesLabel:
                                for (Service service : describeServicesResult.getServices()) {
                                    List<String> subnets = service.getNetworkConfiguration().getAwsvpcConfiguration().getSubnets();

                                    for (String subnet : subnets) {
                                        if (disallowedSubnets.contains(subnet)) {
                                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                                    DetectionType.ECSLaunchedWithinManagementSubnets, service.getServiceArn(),
                                                    new SrdNameValuePair("clusterArn", clusterArn));
                                            break describeServicesLabel;
                                        }
                                    }
                                }
                            }
                        } while (listServicesRequestToken != null);
                    }
                } while (listClustersRequestToken != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ECSLaunchedWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "ECSLaunchedWithinManagementSubnets";
    }
}
