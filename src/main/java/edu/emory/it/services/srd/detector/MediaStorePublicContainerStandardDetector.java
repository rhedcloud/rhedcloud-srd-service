package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detect Elemental MediaStore containers that are exposed publicly.<br>
 * Only containers with an <code>Active</code> status are processed.
 *
 * <p>The Standard and HIPAA detectors are identical but the remediators are are different.</p>
 *
 * @see edu.emory.it.services.srd.remediator.MediaStorePublicContainerStandardRemediator the remediator
 */
@StandardComplianceClass
public class MediaStorePublicContainerStandardDetector extends MediaStorePublicContainerBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext);
    }

    @Override
    public String getBaseName() {
        return "MediaStorePublicContainerStandard";
    }
}
