package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeAttachment;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Remediates EBS Secondary Volumes that are unencrypted<br>
 *
 * <p>The remediator stops the instance that is unencrypted</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.EbsUnencryptedSecondaryVolumeDetector the detector
 */
public class EbsUnencryptedSecondaryVolumeRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG, DetectionType.EbsUnencryptedSecondaryVolume))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String volumeId = arn[5].replace("volume/", "");
        String region = arn[3];

        final AmazonEC2 ec2;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Volume '" + volumeId + "'", e);
            return;
        }


        Map<String, String> instanceRootVolume = retrieveRootDeviceToIgnore(ec2);

        DescribeVolumesRequest request = new DescribeVolumesRequest().withVolumeIds(volumeId);
        DescribeVolumesResult response = ec2.describeVolumes(request);
        if (response.getVolumes() == null || response.getVolumes().isEmpty()) {
            setError(detected, srContext.LOGTAG,
                    "Volume '" + volumeId + "' cannot be found.");
            return;
        }

        Volume volume = response.getVolumes().get(0);

        if (volume.getEncrypted()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Volume '" + volumeId + "' already fixed, it is already encrypted.");
            return;
        }


        if (volume.getState() == null || !volume.getState().equals("in-use")) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Volume '" + volumeId + "' already stopped.");
            return;
        }


        if (volume.getAttachments() == null || volume.getAttachments().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Volume '" + volumeId + "' is detached from any instances. No change necessary.");
            return;
        }

        boolean hasAttachment = false;
        for (VolumeAttachment attachment : volume.getAttachments()) {
            if (attachment.getState().equals("attached")) {
                hasAttachment = true;
                break;
            }
        }

        if (!hasAttachment) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Volume '" + volumeId + "' is detached from any instances. No change necessary.");
            return;
        }

        boolean isSecondaryVolume = false;
        List<String> instanceIdsToStop = new ArrayList<>();
        for (VolumeAttachment attachment : volume.getAttachments()) {
            String rootDeviceName = instanceRootVolume.get(attachment.getInstanceId());
            if (rootDeviceName == null || !rootDeviceName.equals(attachment.getDevice())) {
                isSecondaryVolume = true;
                instanceIdsToStop.add(attachment.getInstanceId());
            }
        }

        if (!isSecondaryVolume) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Specified volume '" + volumeId + "' is a primary volume and does not need to be remediated.");
            return;
        }

        //remediate
        StopInstancesRequest stopInstancesRequest = new StopInstancesRequest()
                .withInstanceIds(instanceIdsToStop);
        StopInstancesResult result = ec2.stopInstances(stopInstancesRequest);

        List<InstanceStateChange> stoppedInstances = result.getStoppingInstances();

        setSuccess(detected, srContext.LOGTAG,
                "The following instances '"
                        + stoppedInstances.stream().map(InstanceStateChange::getInstanceId).collect(Collectors.joining(","))
                        + "' were stopped because volume '" + detected.getAmazonResourceName()+ "' is unencrypted.");
    }

    private Map<String, String> retrieveRootDeviceToIgnore(AmazonEC2 ec2) {
        boolean done = false;
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        Map<String, String> instanceRootVolume = new HashMap<>();
        while(!done) {

            DescribeInstancesResult response = ec2.describeInstances(request);
            for(Reservation reservation : response.getReservations()) {
                for(Instance instance : reservation.getInstances()) {
                    if (instance.getRootDeviceType().equalsIgnoreCase("ebs")) {
                        instanceRootVolume.put(instance.getInstanceId(), instance.getRootDeviceName());
                    }
                }
            }

            request.setNextToken(response.getNextToken());

            if(response.getNextToken() == null) {
                done = true;
            }
        }

        return instanceRootVolume;
    }
}
