package edu.emory.it.services.srd.provider;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "t_lock")
public class TLock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "lock_name", nullable = false, unique = true, updatable = false)
	private String lockName;

	@Column(name = "key_value", updatable = false)
	private String keyValue;

	@Column(name = "create_user", updatable = false)
	private String createUser;

	@Column(name = "create_date", updatable = false)
	@CreationTimestamp
	private Date createDate;

	public TLock() {
	}

	public String getLockName() {
		return lockName;
	}

	public void setLockName(String lockName) {
		this.lockName = lockName;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("LockName: ").append(getLockName()).append(",");
		sb.append("KeyValue: ").append(getKeyValue()).append(",");
		sb.append("CreateUser: ").append(getCreateUser()).append(",");
		sb.append("CreateDate: ").append(getCreateDate());
		sb.append("}");
		return sb.toString();
	}
}