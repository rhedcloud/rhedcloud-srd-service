package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.kms.model.AWSKMSException;
import com.amazonaws.services.kms.model.GetKeyPolicyRequest;
import com.amazonaws.services.kms.model.GetKeyPolicyResult;
import com.amazonaws.services.kms.model.NotFoundException;
import com.amazonaws.services.kms.model.PutKeyPolicyRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.AWSPolicyUtilNoChangeException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Collections;
import java.util.Set;

/**
 * Remediates KMS Externally Shared Key<br>
 * <p>Rewrites the target key's policy by removing any policy statement with Effect = "Allow" and an external
 * principal to to the owner account instead.  The rest of the policy will be kept as before.</p>
 *
 * @see edu.emory.it.services.srd.detector.KMSExternallySharedKeyDetector the detector
 */
public class KMSExternallySharedKeyRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:kms:", srContext.LOGTAG, DetectionType.KMSExternallySharedKey))
            return;
        // like arn:aws:kms:us-east-1:123456789012:key/c714db97-f89f-4aed-8f1c-40b2dd864b58
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn.getRegion();
        String keyId = arn.getResource().getResource();

        final AWSKMS kms;
        try {
            kms = getClient(accountId, region, srContext.getMetricCollector(), AWSKMSClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Key '" + keyId + "'", e);
            return;
        }

        // arn:aws:iam::accountid:role/rhedcloud/RHEDcloudAdministratorRole

        String updatedPolicy;
        try {
            GetKeyPolicyRequest getKeyPolicyRequest = new GetKeyPolicyRequest()
                    .withKeyId(keyId)
                    .withPolicyName("default");
            GetKeyPolicyResult getKeyPolicyResult = kms.getKeyPolicy(getKeyPolicyRequest);
            Set<String> whitelistedAccounts = Collections.singleton(accountId);
            String policy = getKeyPolicyResult.getPolicy();
            updatedPolicy = AWSPolicyUtil.removeAllowToNonWhitelistedAccounts(whitelistedAccounts, policy);
        }
        catch (NotFoundException e) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Key '" + keyId + "' cannot be found.");
            return;
        }
        catch (AWSPolicyUtilNoChangeException e) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Policy does not have externally shared account.  No changes have been made.");
            return;
        }

        try {
            PutKeyPolicyRequest putKeyPolicyRequest = new PutKeyPolicyRequest()
                    .withKeyId(keyId)
                    .withPolicy(updatedPolicy)
                    .withPolicyName("default");
            kms.putKeyPolicy(putKeyPolicyRequest);

            setSuccess(detected, srContext.LOGTAG,
                    "Key policy successfully modified to remove any statements that permits sharing the key with other accounts.");
        }
        catch (AWSKMSException e) {
            if ("AccessDeniedException".equals(e.getErrorCode())) {
                setError(detected, srContext.LOGTAG,
                        "Permission for Security Risk Detection Service Role is required on key '" + keyId
                                + "' to be able to remediate the key", e);
            }
            else {
                setError(detected, srContext.LOGTAG, "Error putting key policy on key '" + keyId, e);
            }
        }
    }
}
