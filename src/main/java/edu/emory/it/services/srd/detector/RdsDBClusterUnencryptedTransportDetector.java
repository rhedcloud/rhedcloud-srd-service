package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBCluster;
import com.amazonaws.services.rds.model.DescribeDBClusterParametersRequest;
import com.amazonaws.services.rds.model.DescribeDBClusterParametersResult;
import com.amazonaws.services.rds.model.DescribeDBClustersRequest;
import com.amazonaws.services.rds.model.DescribeDBClustersResult;
import com.amazonaws.services.rds.model.Parameter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Detect DB clusters (Aurora, Neptune, and DocumentDB) where encryption in-transit is not enabled.
 *
 * @see edu.emory.it.services.srd.remediator.RdsDBClusterUnencryptedTransportRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsDBClusterUnencryptedTransportDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            AmazonRDS rdsClient;
            try {
                rdsClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsDBClusterUnencryptedTransport,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            // cluster databases - things like aurora, document db, neptune, etc
            DescribeDBClustersRequest describeDBClustersRequest = new DescribeDBClustersRequest();
            DescribeDBClustersResult describeDBClustersResult;
            do {
                try {
                    describeDBClustersResult = rdsClient.describeDBClusters(describeDBClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsDBClusterUnencryptedTransport,
                            srContext.LOGTAG, "Error describing DB clusters", e);
                    return;
                }

                for (DBCluster dbCluster : describeDBClustersResult.getDBClusters()) {
                    String engine = dbCluster.getEngine();
                    String parameterName;
                    String parameterValue;

                    if (engine.equals("aurora-mysql")) {
                        // encryption in transit for MySQL is a user setting and would need Administrator level
                        // access to fix, like - ALTER USER 'the_user'@'the_host' REQUIRE SSL;
                        // so skip this detection for regular MySQL instances plus cluster configurations
                        logger.debug(srContext.LOGTAG + "Skipping DB Cluster engine " + engine);
                        continue;
                    }
                    else if (engine.equals("aurora-postgresql")) {
                        parameterName = "rds.force_ssl";
                        parameterValue = "1";
                    }
                    else if (engine.equals("docdb")) {
                        parameterName = "tls";
                        parameterValue = "enabled";
                    }
                    else if (engine.equals("neptune")) {
                        parameterName = "neptune_enforce_ssl";
                        parameterValue = "1";
                    }
                    else {
                        logger.info(srContext.LOGTAG + "DB Cluster engine '" + engine + "' not supported" +
                                " for unencrypted transport detection for cluster " + dbCluster.getDBClusterArn());
                        // this includes RDS global clusters (Aurora MySQL), Serverless, etc.
                        continue;
                    }

                    try {
                        DetectionType clusterAtRisk = isClusterAtRisk(rdsClient, dbCluster, parameterName, parameterValue);
                        if (clusterAtRisk != null) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    clusterAtRisk, dbCluster.getDBClusterArn(),
                                    new SrdNameValuePair("DBClusterIdentifier", dbCluster.getDBClusterIdentifier()),
                                    new SrdNameValuePair("DBClusterStatus", dbCluster.getStatus()));
                        }
                    }
                    catch (Exception e) {
                        setDetectionError(detection, DetectionType.RdsDBClusterUnencryptedTransport,
                                srContext.LOGTAG, "Error inspecting DB cluster " + dbCluster.getDBClusterArn(), e);
                        return;
                    }
                }

                describeDBClustersRequest.setMarker(describeDBClustersResult.getMarker());
            } while (describeDBClustersResult.getMarker() != null);
        }
    }

    /*
     * See https://docs.aws.amazon.com/neptune/latest/userguide/security-ssl.html
     */
    private DetectionType isClusterAtRisk(AmazonRDS rdsClient, DBCluster cluster, String parameterName, String parameterValue) {
        /*
         * Parameter groups can be changed for a cluster but may not take effect until a reboot.
         * There are several parameter group statuses that indicates a reboot is required but
         *  the most important is when the cluster member is not in sync.
         * Until the cluster is rebooted we can't tell what the actual value of the "neptune_enforce_ssl" parameter is.
         * So, we mark the cluster at-risk though it'll probably just be a notification.
         */
        boolean allClusterMemberInSync = cluster.getDBClusterMembers().stream()
                .allMatch(c -> c.getDBClusterParameterGroupStatus().equals("in-sync"));
        if (!allClusterMemberInSync) {
            return DetectionType.RdsDBClusterPendingReboot;
        }

        /*
         * collect all of the DB parameters and check the specified one that indicates encryption in transit
         */
        DescribeDBClusterParametersRequest describeDBClusterParametersRequest = new DescribeDBClusterParametersRequest()
                .withDBClusterParameterGroupName(cluster.getDBClusterParameterGroup());
        DescribeDBClusterParametersResult describeDBClusterParametersResult;
        List<Parameter> parameters = new ArrayList<>();

        do {
            describeDBClusterParametersResult = rdsClient.describeDBClusterParameters(describeDBClusterParametersRequest);
            parameters.addAll(describeDBClusterParametersResult.getParameters());
            describeDBClusterParametersRequest.setMarker(describeDBClusterParametersResult.getMarker());
        } while (describeDBClusterParametersResult.getMarker() != null);

        Optional<Parameter> enforceSslParam = parameters.stream()
                .filter(p -> p.getParameterName().equals(parameterName))
                .findAny();
        if (enforceSslParam.isPresent()) {
            if (enforceSslParam.get().getParameterValue().equals(parameterValue)) {
                // parameter found and indicates that the cluster has encryption in transit enabled
                return null;
            }
            else {
                return DetectionType.RdsDBClusterUnencryptedTransport;
            }
        }
        else {
            // very odd that the parameter can't be found but without it we can't say anything about the cluster
            return null;
        }
    }

    @Override
    public String getBaseName() {
        return "RdsDBClusterUnencryptedTransport";
    }
}
