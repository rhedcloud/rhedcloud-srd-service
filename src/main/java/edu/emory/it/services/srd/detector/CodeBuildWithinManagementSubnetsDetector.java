package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.codebuild.AWSCodeBuild;
import com.amazonaws.services.codebuild.AWSCodeBuildClient;
import com.amazonaws.services.codebuild.model.BatchGetProjectsRequest;
import com.amazonaws.services.codebuild.model.BatchGetProjectsResult;
import com.amazonaws.services.codebuild.model.ListProjectsRequest;
import com.amazonaws.services.codebuild.model.ListProjectsResult;
import com.amazonaws.services.codebuild.model.Project;
import com.amazonaws.services.codebuild.model.VpcConfig;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Detects CodeBuild configuration that are in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.CodeBuildWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CodeBuildWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                AWSCodeBuild codeBuild = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSCodeBuildClient.class);
                AmazonEC2 ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);
                inspectProjects(codeBuild, disallowedSubnets, detection, srContext.LOGTAG);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
        // detector name does not match a DetectionType so ensure DetectionResult is set
        if (detection.getDetectionResult() == null) {
            setDetectionSuccess(detection, DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet, srContext.LOGTAG);
        }
    }

    private void inspectProjects(AWSCodeBuild codeBuild, Set<String> disallowedSubnets, SecurityRiskDetection detection, String LOGTAG) {
        ListProjectsRequest listProjectsRequest = new ListProjectsRequest();
        ListProjectsResult listProjectsResult;

        do {
            try {
                listProjectsResult = codeBuild.listProjects(listProjectsRequest);
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet,
                        LOGTAG, "Error listing projects", e);
                return;
            }

            if (listProjectsResult.getProjects() != null && !listProjectsResult.getProjects().isEmpty()) {
                BatchGetProjectsRequest batchGetProjectsRequest = new BatchGetProjectsRequest().withNames(listProjectsResult.getProjects());
                BatchGetProjectsResult batchGetProjectsResult;
                try {
                    batchGetProjectsResult = codeBuild.batchGetProjects(batchGetProjectsRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet,
                            LOGTAG, "Error getting projects", e);
                    return;
                }
                for (Project project : batchGetProjectsResult.getProjects()) {
                    VpcConfig vpcConfig = project.getVpcConfig();
                    if (vpcConfig != null && vpcConfig.getVpcId() != null
                            && vpcConfig.getSubnets().stream().anyMatch(disallowedSubnets::contains)) {
                        addDetectedSecurityRisk(detection, LOGTAG,
                                DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet, project.getArn());
                    }
                }
            }

            listProjectsRequest.setNextToken(listProjectsResult.getNextToken());
        } while (listProjectsResult.getNextToken() != null);
    }

    @Override
    public String getBaseName() {
        return "CodeBuildWithinManagementSubnets";
    }
}
