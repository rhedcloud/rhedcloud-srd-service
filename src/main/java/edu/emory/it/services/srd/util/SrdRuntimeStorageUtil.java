package edu.emory.it.services.srd.util;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.KeyAttribute;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class SrdRuntimeStorageUtil {
    private static final Object lock = new Object();

    private static SrdRuntimeStorageUtil instance;

    /**
     * see SrdRuntimeStorage/storageTableName deployment descriptor property
     */
    private Table storageTable;

    public static SrdRuntimeStorageUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new SrdRuntimeStorageUtil();
                }
            }
        }
        return instance;
    }

    /**
     * Initialize the singleton.
     *
     * @param storageTable storage table
     */
    public void initialize(Table storageTable) {
        this.storageTable = storageTable;
    }

    public Table getStorageTable() {
        return storageTable;
    }

    public String getNextSecurityRiskDetectionId(String sequenceName) {
        PrimaryKey primaryKey = makePrimaryKey(sequenceName, "sequence");

        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKey)
                .withUpdateExpression("set #seqval = if_not_exists(#seqval, :zero) + :increment")
                .withValueMap(new ValueMap().withNumber(":increment", 1).withNumber(":zero", 0))
                .withNameMap(new NameMap().with("#seqval", "value"))
                .withReturnValues(ReturnValue.UPDATED_NEW);

        UpdateItemOutcome outcome = storageTable.updateItem(updateItemSpec);  // caller handles all exceptions
        return outcome.getItem().getString("value");
    }

    // DetectorExecutionFinishTime supports frequency backoff
    public long retrieveDetectorExecutionFinishTime(String detectorBaseName, String accountId, Logger logger, String LOGTAG) {
        PrimaryKey primaryKey = makePrimaryKey(detectorBaseName, accountId);

        try {
            Item item = storageTable.getItem(primaryKey);
            if (item != null) {
                BigDecimal number = item.getNumber("executionFinishTime");
                if (number != null)
                    return number.longValue();
            }
        }
        catch (ProvisionedThroughputExceededException e) {
            // handle specifically with the future goal of tracking these failures somewhere like Datadog
            String msg = LOGTAG + "ProvisionedThroughputExceeded on DynamoDB table " + storageTable.getTableName()
                    + " while getting DetectorExecutionFinishTime"
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
        catch (Exception e) {
            String msg = LOGTAG + "Error getting DetectorExecutionFinishTime from the DynamoDB table " + storageTable.getTableName()
                    + " with primary key " + primaryKey
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }

        return 0;
    }

    public void updateDetectorExecutionFinishTime(String detectorBaseName, String accountId, Logger logger, String LOGTAG) {
        PrimaryKey primaryKey = makePrimaryKey(detectorBaseName, accountId);
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKey)
                .withUpdateExpression("set executionFinishTime = :v")
                .withValueMap(new ValueMap().withNumber(":v", System.currentTimeMillis()));

        try {
            storageTable.updateItem(updateItemSpec);
        }
        catch (ProvisionedThroughputExceededException e) {
            // handle specifically with the future goal of tracking these failures somewhere like Datadog
            String msg = LOGTAG + "ProvisionedThroughputExceeded on DynamoDB table " + storageTable.getTableName()
                    + " while updating DetectorExecutionFinishTime"
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
        catch (Exception e) {
            String msg = LOGTAG + "Error updating DetectorExecutionFinishTime in the DynamoDB table " + storageTable.getTableName()
                    + " with primary key " + primaryKey
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
    }

    /**
     * Retrieve already known Root user password change events.
     *
     * @param detectorBaseName primary key
     * @param accountId primary key
     * @param logger logger
     * @param LOGTAG logtag
     * @return null on error, otherwise a (possibly empty) set
     */
    public Set<String> retrieveRootUserPasswordChangeEvents(String detectorBaseName, String accountId, Logger logger, String LOGTAG) {
        PrimaryKey primaryKey = makePrimaryKey(detectorBaseName, accountId);

        try {
            Item eventItems = storageTable.getItem(primaryKey);
            if (eventItems == null)
                return new HashSet<>();
            if (eventItems.getStringSet("eventIds") == null)
                return new HashSet<>();
            return eventItems.getStringSet("eventIds");
        }
        catch (ProvisionedThroughputExceededException e) {
            // handle specifically with the future goal of tracking these failures somewhere like Datadog
            String msg = LOGTAG + "ProvisionedThroughputExceeded on DynamoDB table " + storageTable.getTableName()
                    + " while getting RootUserPasswordChangeEvents"
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
        catch (Exception e) {
            String msg = LOGTAG + "Error getting RootUserPasswordChangeEvents from the DynamoDB table " + storageTable.getTableName()
                    + " with primary key " + primaryKey
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }

        return null;
    }

    public void removeRootUserPasswordChangeEvents(String detectorBaseName, String accountId, Logger logger, String LOGTAG) {
        PrimaryKey primaryKey = makePrimaryKey(detectorBaseName, accountId);
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKey)
                .withUpdateExpression("REMOVE eventIds");

        try {
            storageTable.updateItem(updateItemSpec);
        }
        catch (ProvisionedThroughputExceededException e) {
            // handle specifically with the future goal of tracking these failures somewhere like Datadog
            String msg = LOGTAG + "ProvisionedThroughputExceeded on DynamoDB table " + storageTable.getTableName()
                    + " while removing RootUserPasswordChangeEvents"
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
        catch (Exception e) {
            String msg = LOGTAG + "Error removing RootUserPasswordChangeEvents from the DynamoDB table " + storageTable.getTableName()
                    + " with primary key " + primaryKey
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
    }

    public void updateRootUserPasswordChangeEvents(String detectorBaseName, String accountId, Set<String> eventIds, Logger logger, String LOGTAG) {
        PrimaryKey primaryKey = makePrimaryKey(detectorBaseName, accountId);
        UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                .withPrimaryKey(primaryKey)
                .withUpdateExpression("SET eventIds = :v")
                .withValueMap(new ValueMap().withStringSet(":v", eventIds));

        try {
            storageTable.updateItem(updateItemSpec);
        }
        catch (ProvisionedThroughputExceededException e) {
            // handle specifically with the future goal of tracking these failures somewhere like Datadog
            String msg = LOGTAG + "ProvisionedThroughputExceeded on DynamoDB table " + storageTable.getTableName()
                    + " while updating RootUserPasswordChangeEvents"
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
        catch (Exception e) {
            String msg = LOGTAG + "Error updating RootUserPasswordChangeEvents in the DynamoDB table " + storageTable.getTableName()
                    + " with primary key " + primaryKey
                    + ". The exception is: " + e.getMessage();
            logger.error(msg);
        }
    }

    private PrimaryKey makePrimaryKey(String detectorBaseName, String accountId) {
        return new PrimaryKey(
                new KeyAttribute("detectorName", detectorBaseName),
                new KeyAttribute("property", accountId));
    }
}
