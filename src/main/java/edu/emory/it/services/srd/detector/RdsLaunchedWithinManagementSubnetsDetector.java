package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBCluster;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DBSubnetGroup;
import com.amazonaws.services.rds.model.DescribeDBClustersRequest;
import com.amazonaws.services.rds.model.DescribeDBClustersResult;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.amazonaws.services.rds.model.DescribeDBSubnetGroupsRequest;
import com.amazonaws.services.rds.model.DescribeDBSubnetGroupsResult;
import com.amazonaws.services.rds.model.DescribeGlobalClustersRequest;
import com.amazonaws.services.rds.model.DescribeGlobalClustersResult;
import com.amazonaws.services.rds.model.GlobalCluster;
import com.amazonaws.services.rds.model.GlobalClusterMember;
import com.amazonaws.services.rds.model.Subnet;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Detect RDS instances and DB clusters (Aurora, Neptune, and DocumentDB) in a disallowed subnet.
 *
 * @see edu.emory.it.services.srd.remediator.RdsLaunchedWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsLaunchedWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        // global clusters sit outside regions so keep track of them and their members
        List<GlobalCluster> globalClusters = new ArrayList<>();
        Set<String> globalClusterArns = new HashSet<>();

        for (Region region : getRegions()) {
            AmazonRDS client;
            AmazonEC2 ec2Client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);
                ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets;
            try {
                disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);
                // no point checking further if there aren't any disallowed subnets
                if (disallowedSubnets.isEmpty())
                    continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                        srContext.LOGTAG, "Error getting disallowed subnets", e);
                return;
            }


            // collect the names of subnet groups that contain disallowed subnets
            Set<String> disallowedSubnetGroupNames = new HashSet<>();

            DescribeDBSubnetGroupsRequest describeDBSubnetGroupsRequest = new DescribeDBSubnetGroupsRequest();
            DescribeDBSubnetGroupsResult describeDBSubnetGroupsResult;
            do {
                try {
                    describeDBSubnetGroupsResult = client.describeDBSubnetGroups(describeDBSubnetGroupsRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                            srContext.LOGTAG, "Error describing RDS subnet groups", e);
                    return;
                }
                for (DBSubnetGroup subnetGroup : describeDBSubnetGroupsResult.getDBSubnetGroups()) {
                    boolean hasDisallowed = subnetGroup.getSubnets().stream()
                            .anyMatch(subnet -> disallowedSubnets.contains(subnet.getSubnetIdentifier()));
                    if (hasDisallowed)
                        disallowedSubnetGroupNames.add(subnetGroup.getDBSubnetGroupName());
                }
                describeDBSubnetGroupsRequest.setMarker(describeDBSubnetGroupsResult.getMarker());
            } while (describeDBSubnetGroupsResult.getMarker() != null);


            // global databases - things like Aurora (MySQL)
            // just keep track of them so we can know if an at-risk cluster is global or not
            DescribeGlobalClustersRequest describeGlobalClustersRequest = new DescribeGlobalClustersRequest();
            DescribeGlobalClustersResult describeGlobalClustersResult;
            do {
                try {
                    describeGlobalClustersResult = client.describeGlobalClusters(describeGlobalClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                            srContext.LOGTAG, "Error describing DB global clusters", e);
                    return;
                }

                for (GlobalCluster globalCluster : describeGlobalClustersResult.getGlobalClusters()) {
                    // because global clusters are outside of any region, we'll get the same cluster
                    // as we iterate over the regions.  keep track so we don't process them more than once.
                    if (globalClusterArns.contains(globalCluster.getGlobalClusterArn()))
                        continue;
                    globalClusterArns.add(globalCluster.getGlobalClusterArn());
                    globalClusters.add(globalCluster);
                }

                describeGlobalClustersRequest.setMarker(describeGlobalClustersResult.getMarker());
            } while (describeGlobalClustersResult.getMarker() != null);


            // cluster databases - things like aurora, document db, neptune, serverless, etc
            // some clusters are made up of instances while others (serverless) don't have any instances.
            DescribeDBClustersRequest describeDBClustersRequest = new DescribeDBClustersRequest();
            DescribeDBClustersResult describeDBClustersResult;
            do {
                try {
                    describeDBClustersResult = client.describeDBClusters(describeDBClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                            srContext.LOGTAG, "Error describing DB clusters", e);
                    return;
                }

                for (DBCluster dbCluster : describeDBClustersResult.getDBClusters()) {
                    if (disallowedSubnetGroupNames.contains(dbCluster.getDBSubnetGroup())) {
                        // at risk - either a global or regular cluster
                        GlobalCluster globalDbCluster = null;
                        for (GlobalCluster globalCluster : globalClusters) {
                            boolean match = globalCluster.getGlobalClusterMembers().stream()
                                    .map(GlobalClusterMember::getDBClusterArn)
                                    .anyMatch(memberArn -> memberArn.equals(dbCluster.getDBClusterArn()));
                            if (match) {
                                globalDbCluster = globalCluster;
                                break;
                            }
                        }
                        if (globalDbCluster == null) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsLaunchedWithinManagementSubnets, dbCluster.getDBClusterArn(),
                                    new SrdNameValuePair("DBClusterIdentifier", dbCluster.getDBClusterIdentifier()),
                                    new SrdNameValuePair("DBClusterStatus", dbCluster.getStatus()));
                        }
                        else {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsLaunchedWithinManagementSubnets, globalDbCluster.getGlobalClusterArn(),
                                    new SrdNameValuePair("DBGlobalClusterIdentifier", globalDbCluster.getGlobalClusterIdentifier()),
                                    new SrdNameValuePair("DBGlobalClusterStatus", globalDbCluster.getStatus()));
                        }
                    }
                }

                describeDBClustersRequest.setMarker(describeDBClustersResult.getMarker());
            } while (describeDBClustersResult.getMarker() != null);


            // database instances
            DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest();
            DescribeDBInstancesResult describeDBInstancesResult;
            do {
                try {
                    describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsLaunchedWithinManagementSubnets,
                            srContext.LOGTAG, "Error describing DB instances", e);
                    return;
                }

                for (DBInstance dbInstance : describeDBInstancesResult.getDBInstances()) {
                    /*
                     * the cluster databases (aurora, document db, neptune, etc) may have multiple instances
                     * that generally can't be operated on individually and were handled in the cluster section
                     */
                    if (dbInstance.getDBClusterIdentifier() != null)
                        continue;

                    for (Subnet subnet : dbInstance.getDBSubnetGroup().getSubnets()) {
                        if (disallowedSubnets.contains(subnet.getSubnetIdentifier())) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsLaunchedWithinManagementSubnets, dbInstance.getDBInstanceArn(),
                                    new SrdNameValuePair("DBInstanceIdentifier", dbInstance.getDBInstanceIdentifier()),
                                    new SrdNameValuePair("DBInstanceStatus", dbInstance.getDBInstanceStatus()));
                            break;
                        }
                    }
                }

                describeDBInstancesRequest.setMarker(describeDBInstancesResult.getMarker());
            } while (describeDBInstancesResult.getMarker() != null);
        }
    }

    @Override
    public String getBaseName() {
        return "RdsLaunchedWithinManagementSubnets";
    }
}
