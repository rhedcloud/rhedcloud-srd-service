package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.SetTerminationProtectionRequest;
import com.amazonaws.services.elasticmapreduce.model.TerminateJobFlowsRequest;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Elastic Map Reduce remediator utilities.
 */
public abstract class AbstractEMRRemediator extends AbstractSecurityRiskRemediator {
    public void terminateCluster(DetectedSecurityRisk detected, String accountId, SecurityRiskContext srContext) {
        String region = detected.getProperties().getProperty("region");
        String clusterId = detected.getProperties().getProperty("clusterId");
        String clusterName = detected.getProperties().getProperty("clusterName");
        String clusterState = detected.getProperties().getProperty("clusterState");
        // cluster names are not unique so qualify with the ID when including in user messages
        String clusterFq = "'" + clusterName + "' (" + clusterId + ")";

        switch (clusterState) {
            case "TERMINATING":
                // fall through

            case "TERMINATED":
                // fall through

            case "TERMINATED_WITH_ERRORS":
                /*
                 * The cluster is either terminating or already terminated which is the state we
                 * were about to change it to so just ignore the risk.  If the end state does not end
                 * up being terminated (with errors) then the next run of the detector will find it.
                 */
                setIgnoreRisk(detected, "EMR cluster " + clusterFq + " already " + clusterState.toLowerCase());
                break;

            default:

                try {
                    final AmazonElasticMapReduce emrClient = getClient(accountId, region, srContext.getMetricCollector(), AmazonElasticMapReduceClient.class);

                    // termination protection must first be disabled
                    SetTerminationProtectionRequest setTerminationProtectionRequest = new SetTerminationProtectionRequest()
                            .withJobFlowIds(clusterId)
                            .withTerminationProtected(false);

                    emrClient.setTerminationProtection(setTerminationProtectionRequest);

                    TerminateJobFlowsRequest terminateJobFlowsRequest = new TerminateJobFlowsRequest()
                            .withJobFlowIds(clusterId);

                    emrClient.terminateJobFlows(terminateJobFlowsRequest);

                    setSuccess(detected, srContext.LOGTAG, "EMR cluster " + clusterFq + " terminated");
                }
                catch (Exception e) {
                    // TODO - requirement is to delete the cluster if the terminate failed
                    setError(detected, srContext.LOGTAG, "Failed to terminate EMR cluster " + clusterFq, e);
                }
                break;
        }
    }
}
