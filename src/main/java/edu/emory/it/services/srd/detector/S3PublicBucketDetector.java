package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetPublicAccessBlockRequest;
import com.amazonaws.services.s3.model.GetPublicAccessBlockResult;
import com.amazonaws.services.s3.model.Grant;
import com.amazonaws.services.s3.model.Grantee;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PublicAccessBlockConfiguration;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;

/**
 * Detects S3 buckets and/or objects that are public<br>
 *
 * <p>Checks the ACL on buckets (to find AllUsers and AllAuthenticatedUsers)</p>
 * <p>Checks the Public Access Block configuration on buckets:</p>
 * <ul>
 *     <li>BlockPublicAcls</li>
 *     <li><ul>
 *         <li>When this is set to TRUE, it will prevent any bucket or objects from having its ACL updated,
 *             or from being upload files, if they contain public ACLs.</li>
 *         <li>Setting this will require the customer to change the way they upload files to make sure they
 *             include a non-public ACL.</li>
 *     </ul></li>
 *     <li>IgnorePublicAcls</li>
 *     <li><ul>
 *         <li>When this is set to TRUE, the public ACLs on objects will be ignored.</li>
 *         <li>The customer can upload files with a non-public ACL and it won't be stopped.</li>
 *         <li>But, the object will still not be public since the public ACL will be ignored.</li>
 *     </ul></li>
 *     <li>BlockPublicPolicy</li>
 *     <li><ul>
 *         <li>When this is set to TRUE, it will prevent any bucket policy changes if the new policy allows public access.</li>
 *     </ul></li>
 *     <li>RestrictPublicBuckets</li>
 *     <li><ul>
 *         <li>When this is set to TRUE, it restricts access to a bucket with a public policy to only AWS services
 *             and authorized users within the bucket owner's account.</li>
 *         <li>The rhedcloud-aws-rs-account CFN template creates a bucket (blah-ct1) for CloudTrail and already
 *             sets explicit service policies so this bucket won't be effected.</li>
 *     </ul></li>
 * </ul>
 *
 * @see edu.emory.it.services.srd.remediator.S3PublicBucketRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class S3PublicBucketDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonS3 s3;
        try {
            s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            // detector name does not match a DetectionType so ensure DetectionResult is set
            setDetectionSuccess(detection, DetectionType.S3PublicBucketAcl, srContext.LOGTAG);
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3PublicBucketAcl,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        List<Bucket> buckets;
        try {
            buckets = s3.listBuckets();
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3PublicBucketAcl,
                    srContext.LOGTAG, "Error listing buckets", e);
            return;
        }

        for (Bucket bucket : buckets) {
        	
        	final String bucketArn = "arn:aws:s3:::" + bucket.getName();
			Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
					.isExemptResourceArnForAccountAndDetector(bucketArn,accountId,getBaseName());
			boolean exceptionStatusUnderminable = resourceIsExempt == null;
			if (exceptionStatusUnderminable) {
				logger.error(srContext.LOGTAG + "Exemption status of bucket " + bucketArn + " can not be determined on account " + accountId);
				continue;					
			}

            if (resourceIsExempt) {
                 logger.info(srContext.LOGTAG + "Ignoring exempted bucket \"" + bucketArn + "\" on account " + accountId);
                continue;
            }
            checkBucketAcl(s3, bucket, detection, srContext.LOGTAG);
            checkBucketBlockPublicAccess(s3, bucket, detection, srContext.LOGTAG);
        }
        // detector name does not match a DetectionType so ensure DetectionResult is set
        if (detection.getDetectionResult() == null) {
            setDetectionSuccess(detection, DetectionType.S3PublicBucketAcl, srContext.LOGTAG);
        }
    }

    private void checkBucketAcl(AmazonS3 s3, Bucket bucket, SecurityRiskDetection detection, String LOGTAG) {
        AccessControlList acl;
        try {
            acl = s3.getBucketAcl(bucket.getName());
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3PublicBucketAcl,
                    LOGTAG, "Error getting ACL for bucket " + bucket.getName(), e);
            return;
        }
        List<Grant> grants = acl.getGrantsAsList();

        for (Grant grant : grants) {
            Grantee grantee = grant.getGrantee();
            if (grantee instanceof GroupGrantee) {
                if (grantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.S3PublicBucketAcl, "arn:aws:s3:::" + bucket.getName());
                    break;
                }
            }
        }
    }

    /*
     * here are a couple of references for this feature:
     * https://docs.aws.amazon.com/cli/latest/reference/s3api/put-public-access-block.html
     * https://docs.aws.amazon.com/AmazonS3/latest/dev/access-policy-alternatives-guidelines.html
     */
    private void checkBucketBlockPublicAccess(AmazonS3 s3, Bucket bucket, SecurityRiskDetection detection, String LOGTAG) {
        GetPublicAccessBlockRequest getPublicAccessBlockRequest = new GetPublicAccessBlockRequest()
                .withBucketName(bucket.getName());
        GetPublicAccessBlockResult getPublicAccessBlockResult;
        boolean publicAccessBlockMisconfiguration = false;
        try {
            getPublicAccessBlockResult = s3.getPublicAccessBlock(getPublicAccessBlockRequest);
            PublicAccessBlockConfiguration publicAccessBlockConfiguration = getPublicAccessBlockResult.getPublicAccessBlockConfiguration();

            if (!publicAccessBlockConfiguration.getIgnorePublicAcls()) {
                publicAccessBlockMisconfiguration = true;
            }
            if (!publicAccessBlockConfiguration.getBlockPublicPolicy()) {
                publicAccessBlockMisconfiguration = true;
            }
            if (!publicAccessBlockConfiguration.getRestrictPublicBuckets()) {
                publicAccessBlockMisconfiguration = true;
            }
            /*
             * as mentioned in the class level javadoc, BlockPublicAcls is not checked by the detector or set
             * by the remediator.  the reason being: Setting this will require the customer to change the way they
             * upload files to make sure they include a non-public ACL.  but because IgnorePublicAcls is considered,
             * any object will still not be public since the public ACL will be ignored.
             */
        }
        catch (Exception e) {
            // com.amazonaws.services.s3.model.AmazonS3Exception: The public access block configuration was not found
            //   (Service: Amazon S3; Status Code: 404; Error Code: NoSuchPublicAccessBlockConfiguration)
            if (e instanceof AmazonS3Exception
                    && ((AmazonS3Exception) e).getErrorCode().equals("NoSuchPublicAccessBlockConfiguration")) {
                publicAccessBlockMisconfiguration = true;
            }
            else {
                setDetectionError(detection, DetectionType.S3PublicAccessBlockMisconfiguration,
                        LOGTAG, "Error getting bucket public access block for bucket " + bucket.getName(), e);
                return;
            }
        }
        if (publicAccessBlockMisconfiguration) {
            addDetectedSecurityRisk(detection, LOGTAG,
                    DetectionType.S3PublicAccessBlockMisconfiguration, "arn:aws:s3:::" + bucket.getName());
        }
    }

    /**
     * Check whether objects in a bucket are public.  This remains unused - see the comment below.
     * @param s3 client
     * @param bucket bucket
     * @param detection Security Risk Detection
     */
    private void checkBucketObjectsAcl(AmazonS3 s3, Bucket bucket, SecurityRiskDetection detection, String LOGTAG) {
        /*
         * I uncommented the call to checkBucketObjectsAcl() in my local environment and ran it against
         * the 'Emory Dev 300' (731363757793) account.
         * The only bucket for that user is aws-dev-300-ct1 which gets created automatically by the RS CFN template.
         * The last bit of monitoring I did I saw over 20,000 objects had been checked.
         * That took over 1 hour at which point our STS assumed role expired and we get:
         *
         * com.amazonaws.services.s3.model.AmazonS3Exception: The provided token has expired.
         * (Service: Amazon S3; Status Code: 400; Error Code: ExpiredToken; Request ID: 79DE509FF55EA522)
         *
         * So, I suspect we're going to need some kind of whitelist for this detector before we can
         * enable the object detection part. Or, some other strategy entirely.
         */
        long start = System.currentTimeMillis();
        ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request().withBucketName(bucket.getName());
        ListObjectsV2Result listObjectsV2Result;
        int nbrObjectsVisited = 0;

        do {
            try {
                listObjectsV2Result = s3.listObjectsV2(listObjectsV2Request);
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.S3PublicBucketObject,
                        LOGTAG, "Error listing bucket objects for bucket " + bucket.getName(), e);
                return;
            }

            for (S3ObjectSummary summary : listObjectsV2Result.getObjectSummaries()) {
                nbrObjectsVisited++;

                AccessControlList aclObject;
                try {
                    aclObject = s3.getObjectAcl(bucket.getName(), summary.getKey());
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.S3PublicBucketObject,
                            LOGTAG, "Error getting object ACL for object " + bucket.getName() + "/" + summary.getKey(), e);
                    return;
                }
                List<Grant> grantsObject = aclObject.getGrantsAsList();

                for (Grant grant : grantsObject) {
                    Grantee grantee = grant.getGrantee();
                    if (grantee instanceof GroupGrantee) {
                        if (grantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                            addDetectedSecurityRisk(detection, LOGTAG,
                                    DetectionType.S3PublicBucketObject,
                                    "arn:aws:s3:::" + bucket.getName() + "/" + summary.getKey());
                            break;
                        }
                    }
                }
            }

            listObjectsV2Request.setContinuationToken(listObjectsV2Result.getNextContinuationToken());
        } while (listObjectsV2Result.isTruncated());

        logger.info(LOGTAG + "Trawled through " + nbrObjectsVisited + " S3 objects in elapsed time "
                + (System.currentTimeMillis() - start) + " ms");
    }

    @Override
    public String getBaseName() {
        return "S3PublicBucket";
    }
}
