package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition;
import com.openii.openeai.commands.OpeniiScheduledCommand;
import org.apache.logging.log4j.Logger;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectGenerateException;

import javax.jms.JMSException;

public class SecurityRiskDetectionBatchCommand extends OpeniiScheduledCommand implements ScheduledCommand {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionBatchCommand.class);

    private ProducerPool awsSrdProducerPool;


    /**
     * Standard and HIPAA detectors are defined in:
     * https://serviceforge.atlassian.net/wiki/spaces/ESSRDS/pages/137953292/Security+Risk+Detector+Technical+Design+Document
     *
     * The (root) name of the detectors for Standard and HIPAA accounts are configured in the AppConfig.
     */
    private String activeDetectors;
    private boolean remediationRequested;


    public SecurityRiskDetectionBatchCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        String LOGTAG = "[SrdBatchCommand] ";

        // Set the properties for this command.
        try {
            PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("AwsSrdBatchCommandProperties");
            setProperties(pConfig.getProperties());
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving a property config from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }


        // setup our properties
        remediationRequested = getProperties().getProperty("remediate", "false").equalsIgnoreCase("true");

        logger.info(LOGTAG + "The value of detector-compliance is: " + activeDetectors);
        logger.info(LOGTAG + "The value of remediate is: " + remediationRequested);

        // Get the producer pools we specified in the deployment descriptor from AppConfig
        try {
            awsSrdProducerPool = (ProducerPool) getAppConfig().getObject("AwsSrdP2pProducer");
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the AwsSrdP2pProducer producer pool from AppConfig"
                    + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public int execute() throws ScheduledCommandException {
        long start = System.currentTimeMillis();

        String LOGTAG = "[SrdBatchCommand] ";
        logger.info(LOGTAG + "Execution started");

        PointToPointProducer pointToPointProducer = null;
        try {
            pointToPointProducer = (PointToPointProducer) awsSrdProducerPool.getExclusiveProducer();
            pointToPointProducer.setRequestTimeoutInterval(600000);

            SecurityRiskDetectionRequisition detectionRequisition
                    = (SecurityRiskDetectionRequisition) getAppConfig().getObject("SecurityRiskDetectionRequisition");
            SecurityRiskDetection detection = (SecurityRiskDetection) getAppConfig().getObject("SecurityRiskDetection.v1_0");
            detection.setCommandName("SecurityRiskDetectionGenerateCommand");

            // sends a batch oriented Generate which is denoted by "..." for the accountId.
            // unlike the regular Generate, the detector is the entire list of detectors,
            // and the remediator is true|false instead of an individual remediator name.
            detectionRequisition.setAccountId("...");
            // Set the properties for this command.
            try {
                PropertyConfig pConfig = (PropertyConfig) getAppConfig().getMainAppConfig().getObject("ActiveSrdsQueryCommandProperties");
                activeDetectors = pConfig.getProperties().getProperty("active-detectors").trim().replaceAll("[\n\t\r ]", "");
            } catch (EnterpriseConfigurationObjectException e) {
                String errMsg = "An error occurred retrieving 'ActiveSrdsQueryCommandProperties' property config from Main AppConfig. The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
            }
            logger.info(LOGTAG + "Active detectors = "+activeDetectors);
            detectionRequisition.setSecurityRiskDetector(activeDetectors);
            detectionRequisition.setSecurityRiskRemediator(Boolean.toString(remediationRequested));

            detection.generate(detectionRequisition, pointToPointProducer);
        } catch (JMSException e) {
            logger.fatal(LOGTAG + "An error occurred getting a producer from the producer pool"
                    + ". The exception is: " + e.getMessage());
        } catch (EnterpriseObjectGenerateException e) {
            logger.fatal(LOGTAG + "An error occurred in SecurityRiskDetectionRequisition.Generate-Request"
                    + ". The exception is: " + e.getMessage());
        } catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "An error occurred retrieving an object from AppConfig"
                    + ". The exception is: " + e.getMessage());
        } catch (EnterpriseFieldException e) {
            logger.fatal(LOGTAG + "An error occurred setting up SecurityRiskDetectionRequisition"
                    + ". The exception is: " + e.getMessage());
        } finally {
            if (pointToPointProducer != null)
                awsSrdProducerPool.releaseProducer(pointToPointProducer);
        }

        long timeElapsed = System.currentTimeMillis() - start;
        logger.info(LOGTAG + "Execution done in elapsed time " + timeElapsed + " ms");

        return 0;
    }
}
