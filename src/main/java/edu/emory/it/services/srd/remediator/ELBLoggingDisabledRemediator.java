package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.metrics.RequestMetricCollector;
import com.amazonaws.regions.Region;
import com.amazonaws.services.elasticloadbalancing.model.AccessLog;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerAttributes;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancingv2.model.AmazonElasticLoadBalancingException;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancerAttributesRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancerAttributesResult;
import com.amazonaws.services.elasticloadbalancingv2.model.InvalidConfigurationRequestException;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancerAttribute;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancerNotFoundException;
import com.amazonaws.services.elasticloadbalancingv2.model.ModifyLoadBalancerAttributesRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Remediates ELBs where logging is disabled<br>
 *
 * <p>Remediates ELB by enabling logging to the S3 bucket configured in the configuration parameter
 * ELBLoggingDisabled: S3LoggingBucketPath-{region name} </p>
 *
 * <p>For HIPAA compliance class accounts</p>
 *
 * <p>Security: Skip. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.ELBLoggingDisabledDetector the detector
 */
public class ELBLoggingDisabledRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private Map<String,String> regionToBucketName, regionToBucketPrefix;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);

        /*
         * For much of the life of this SRR, it has been disabled and going forward the SRD and SRR will be retired.
         * See https://serviceforge.atlassian.net/wiki/spaces/EAIT/pages/395903005/Change+Request+SRD+Updates#RETIRE---ELBLoggingDisabledRemediator
         * Until then, ignore the missing configuration.
         */
        if (!appConfig.getObjects().contains("ELBLoggingDisabled".toLowerCase())) {
            logger.info("ELBLoggingDisabled remediator configuration is missing - ignoring");
            return;
        }

        try {
            Properties properties = appConfig.getProperties("ELBLoggingDisabled");

            regionToBucketName = getRegions().stream().collect(Collectors.toMap(Region::getName, region -> {
                String property = properties.getProperty("S3LoggingBucketPath-" + region.getName());
                if (property != null && !property.isEmpty()) {
                    String[] bucketAndPrefix =  property.split("/", 2);
                    return bucketAndPrefix[0];
                }
                return "";
            }));

            regionToBucketPrefix = getRegions().stream().collect(Collectors.toMap(Region::getName, region -> {
                String property = properties.getProperty("S3LoggingBucketPath-" + region.getName());
                if (property != null) {
                    String[] bucketAndPrefix =  property.split("/", 2);
                    return bucketAndPrefix.length > 1 ? bucketAndPrefix[1] : "";
                }
                return "";
            }));

        } catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:elasticloadbalancing:", srContext.LOGTAG, DetectionType.ELBLoggingDisabled))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        // if the configuration is missing turn this into a notification-only remediation
        if (regionToBucketName == null || regionToBucketPrefix == null) {
            setNotificationOnly(detected, srContext.LOGTAG);
            return;
        }

        String region = arn[3];
        String s3loggingBucket = regionToBucketName.get(region);
        String s3loggingPrefix = regionToBucketPrefix.get(region);

        if (s3loggingBucket == null || s3loggingBucket.isEmpty()) {
            setError(detected, srContext.LOGTAG,
                    "S3 logging bucket is not configured for region: '" + region
                            + "'.  The property 'S3LoggingBucketPath-" + region +
                            "' needs to be set in the deployment descriptor.");
            return;
        }

        String loadBalancerPath = arn[5];
        boolean isClassicLoadBalancer = loadBalancerPath.split("/").length == 2;

        if (isClassicLoadBalancer) {
            String loadBalancerName = loadBalancerPath.split("/")[1];
            remediateV1LoadBalancer(accountId, region, detected, srContext.getMetricCollector(),
                    s3loggingBucket, s3loggingPrefix, loadBalancerName, srContext.LOGTAG);
        } else {
            remediateV2LoadBalancer(accountId, region, detected, srContext.getMetricCollector(),
                    s3loggingBucket, s3loggingPrefix, srContext.LOGTAG);
        }
    }

    private void remediateV2LoadBalancer(String accountId, String region, DetectedSecurityRisk detected,
                                         RequestMetricCollector requestMetricCollector,
                                         String s3loggingBucket, String s3loggingPrefix, String LOGTAG) {
        final AmazonElasticLoadBalancingClient elb;
        try {
            elb = getClient(accountId, region, requestMetricCollector, AmazonElasticLoadBalancingClient.class);
        }
        catch (Exception e) {
            setError(detected, LOGTAG, "Elastic Load Balancer V2", e);
            return;
        }

        DescribeLoadBalancerAttributesRequest describeLoadBalancerAttributesRequest = new DescribeLoadBalancerAttributesRequest().withLoadBalancerArn(detected.getAmazonResourceName());
        DescribeLoadBalancerAttributesResult describeLoadBalancerAttributesResult;
        try {
            describeLoadBalancerAttributesResult = elb.describeLoadBalancerAttributes(describeLoadBalancerAttributesRequest);
        } catch (LoadBalancerNotFoundException e) {
            setError(detected, LOGTAG,
                    "Elastic Load Balancer '" + detected.getAmazonResourceName() + "' not found", e);
            return;
        }

        Map<String, String> attributes = describeLoadBalancerAttributesResult.getAttributes().stream().collect(Collectors.toMap(LoadBalancerAttribute::getKey, LoadBalancerAttribute::getValue));
        boolean loggingIsCorrect =
                s3loggingBucket.equals(attributes.get("access_logs.s3.bucket")) &&
                        s3loggingPrefix.equals(attributes.get("access_logs.s3.prefix")) &&
                        "true".equals(attributes.get("access_logs.s3.enabled"));

        if (loggingIsCorrect) {
            setNoLongerRequired(detected, LOGTAG,
                    "Elastic Load Balancer '" + detected.getAmazonResourceName() + "' is already log enabled");
            return;
        }


        ModifyLoadBalancerAttributesRequest modifyLoadBalancerAttributesRequest = new ModifyLoadBalancerAttributesRequest()
                .withLoadBalancerArn(detected.getAmazonResourceName())
                .withAttributes(
                        new LoadBalancerAttribute().withKey("access_logs.s3.bucket").withValue(s3loggingBucket),
                        new LoadBalancerAttribute().withKey("access_logs.s3.prefix").withValue(s3loggingPrefix),
                        new LoadBalancerAttribute().withKey("access_logs.s3.enabled").withValue("true")
                );
        try {
            elb.modifyLoadBalancerAttributes(modifyLoadBalancerAttributesRequest);
        } catch (InvalidConfigurationRequestException e) {
            logger.error(LOGTAG + s3loggingBucket +" has the wrong permissions access permission.  putObject request permission needs to be granted to the Elastic Load Balancing. Refer to https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html to set permissions correctly.");
            setError(detected, LOGTAG,
                    "Modifying Elastic Load Balancing attributes failed because of a misconfiguration on s3 bucket " + s3loggingBucket, e);
            return;
        } catch (AmazonElasticLoadBalancingException e) {
            logger.error(LOGTAG + e.getMessage());
            setError(detected, LOGTAG,
                    "Modifying Elastic Load Balancing attributes failed because of missing s3 bucket " + s3loggingBucket, e);
            return;
        }

        setSuccess(detected, LOGTAG, "Elastic Load Balancer successfully updated to have logging enabled.");
    }

    private void remediateV1LoadBalancer(String accountId, String region, DetectedSecurityRisk detected,
                                         RequestMetricCollector requestMetricCollector,
                                         String s3loggingBucket, String s3loggingPrefix, String loadBalancerName, String LOGTAG) {
        final com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient elb;
        try {
            elb = getClient(accountId, region, requestMetricCollector, com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient.class);
        }
        catch (Exception e) {
            setError(detected, LOGTAG, "Elastic Load Balancer V1", e);
            return;
        }

        com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesRequest describeLoadBalancerAttributesRequest = new com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesRequest().withLoadBalancerName(loadBalancerName);
        com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesResult describeLoadBalancerAttributesResult;
        try {
            describeLoadBalancerAttributesResult = elb.describeLoadBalancerAttributes(describeLoadBalancerAttributesRequest);
        } catch (LoadBalancerNotFoundException e) {
            setError(detected, LOGTAG,
                    "Elastic Load Balancing '" + detected.getAmazonResourceName() + "' not found", e);
            return;
        }

        LoadBalancerAttributes loadBalancerAttributes = describeLoadBalancerAttributesResult.getLoadBalancerAttributes();
        AccessLog accessLog = describeLoadBalancerAttributesResult.getLoadBalancerAttributes().getAccessLog();
        boolean loggingIsCorrect =
                accessLog != null &&
                        s3loggingBucket.equals(accessLog.getS3BucketName()) &&
                        s3loggingPrefix.equals(accessLog.getS3BucketPrefix()) &&
                        accessLog.getEnabled();

        if (loggingIsCorrect) {
            setNoLongerRequired(detected, LOGTAG,
                    "Elastic Load Balancing '" + detected.getAmazonResourceName() + "' is already log enabled");
            return;
        }

        loadBalancerAttributes.setAccessLog(new AccessLog()
                .withS3BucketName(s3loggingBucket)
                .withS3BucketPrefix(s3loggingPrefix)
                .withEnabled(true)
                .withEmitInterval(60));

        com.amazonaws.services.elasticloadbalancing.model.ModifyLoadBalancerAttributesRequest modifyLoadBalancerAttributesRequest = new com.amazonaws.services.elasticloadbalancing.model.ModifyLoadBalancerAttributesRequest()
                .withLoadBalancerName(loadBalancerName)
                .withLoadBalancerAttributes(loadBalancerAttributes);

        try {
            elb.modifyLoadBalancerAttributes(modifyLoadBalancerAttributesRequest);
        } catch (com.amazonaws.services.elasticloadbalancing.model.InvalidConfigurationRequestException e) {
            logger.error(LOGTAG + s3loggingBucket + " has the wrong permissions access permission." +
                    "  putObject request permission needs to be granted to the Elastic Load Balancing." +
                    "  Refer to https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html to set permissions correctly.");
            setError(detected, LOGTAG,
                    "Modifying Elastic Load Balancing attributes failed because of a misconfiguration on s3 bucket " + s3loggingBucket, e);
            return;
        } catch (com.amazonaws.services.elasticloadbalancing.model.AmazonElasticLoadBalancingException e) {
            setError(detected, LOGTAG,
                    "Modifying Elastic Load Balancing attributes failed because of missing s3 bucket " + s3loggingBucket, e);
            return;
        }

        setSuccess(detected, LOGTAG, "Elastic Load Balancing successfully updated to have logging enabled.");
    }
}
