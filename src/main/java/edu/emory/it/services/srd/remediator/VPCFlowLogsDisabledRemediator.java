package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.CreateFlowLogsRequest;
import com.amazonaws.services.ec2.model.DescribeFlowLogsRequest;
import com.amazonaws.services.ec2.model.DescribeFlowLogsResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.FlowLogsResourceType;
import com.amazonaws.services.ec2.model.TrafficType;
import com.amazonaws.services.logs.AWSLogs;
import com.amazonaws.services.logs.AWSLogsClient;
import com.amazonaws.services.logs.model.AWSLogsException;
import com.amazonaws.services.logs.model.CreateLogGroupRequest;
import com.amazonaws.services.logs.model.DescribeLogGroupsRequest;
import com.amazonaws.services.logs.model.DescribeLogGroupsResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.Properties;

/**
 * Remediates VPCs where Flow Logs are disabled.<br>
 * <p>The remediator creates a Flow Log and attaches to the VPC.  The CloudWatch Log Group is created if needed.</p>
 * <p>The name of the Flog Log is consistent with the named used in the CloudFormation templates: vpcId-FlowLogs.</p>
 * <p>The <code>LogDestinationType</code> is <code>CloudWatchLogs</code> (destination cloud-watch-logs).</p>
 * <p>The default <code>LogFormat</code> is used.</p>
 *
 * @see edu.emory.it.services.srd.detector.VPCFlowLogsDisabledDetector the detector
 */
public class VPCFlowLogsDisabledRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private String flowLogRole;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
        try {
            Properties properties = appConfig.getProperties("VPCFlowLogsDisabled");
            flowLogRole = properties.getProperty("flowLogRole");
        } catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG, DetectionType.VPCFlowLogsDisabled))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String vpcId = arn[5].substring(arn[5].lastIndexOf("/")+1);
        String region = arn[3];

        AmazonEC2 ec2;
        AWSLogs logs;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
            logs = getClient(accountId, region, srContext.getMetricCollector(), AWSLogsClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to remediate " + detected.getAmazonResourceName(), e);
            return;
        }

        DescribeFlowLogsRequest describeFlowLogsRequest = new DescribeFlowLogsRequest()
                    .withFilter(new Filter().withName("resource-id").withValues(vpcId));
        DescribeFlowLogsResult describeFlowLogsResult;
        try {
            describeFlowLogsResult = ec2.describeFlowLogs(describeFlowLogsRequest);
        } catch (AmazonEC2Exception e) {
            setError(detected, srContext.LOGTAG, "Flow Logs describe error", e);
            return;
        }

        if (!describeFlowLogsResult.getFlowLogs().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "VPC with vpcId '" + vpcId + "' already has Flow Logs enabled.");
            return;
        }

        String groupName = vpcId + "-FlowLogs";  // naming consistent with rhedcloud-aws-vpc-type1-cfn.json


        try {
            // make sure the log group exists
            DescribeLogGroupsRequest describeLogGroupsRequest = new DescribeLogGroupsRequest()
                    .withLogGroupNamePrefix(groupName);
            DescribeLogGroupsResult describeLogGroupsResult = logs.describeLogGroups(describeLogGroupsRequest);
            boolean nameExists = describeLogGroupsResult.getLogGroups().stream()
                    .anyMatch(logGroup -> groupName.equals(logGroup.getLogGroupName()));
            if (!nameExists) {
                CreateLogGroupRequest createLogGroupRequest = new CreateLogGroupRequest()
                        .withLogGroupName(groupName);
                logs.createLogGroup(createLogGroupRequest);
            }
        } catch (AWSLogsException e) {
            setError(detected, srContext.LOGTAG, "Flow Logs log group error", e);
            return;
        }

        try {
            // create the Flow Logs using some defaults:
            //     LogDestinationType of CloudWatchLogs (destination cloud-watch-logs)
            //     LogFormat of default
            CreateFlowLogsRequest createFlowLogsRequest = new CreateFlowLogsRequest()
                    .withResourceIds(vpcId)
                    .withDeliverLogsPermissionArn("arn:aws:iam::" + accountId + ":role/" + flowLogRole)
                    .withLogGroupName(groupName)
                    .withTrafficType(TrafficType.ALL)
                    .withResourceType(FlowLogsResourceType.VPC);

            ec2.createFlowLogs(createFlowLogsRequest);
        } catch (AmazonEC2Exception e) {
            setError(detected, srContext.LOGTAG, "Flow Logs create error", e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG, "VPC with vpcId '" + vpcId + "' has been updated to enable Flow Logs.");
    }
}
