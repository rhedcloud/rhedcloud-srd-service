package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;

/**
 * Interface for all remediators.
 */
public interface SecurityRiskRemediator {
    /**
     * Initialize the remediator.
     *
     * @param aConfig, an AppConfig object with all this remediator needs.
     * @param credentialsProvider AWS credentials
     * @param securityRole AWS Role to assume
     * @throws SecurityRiskRemediationException with details of the initialization error.
     */
    void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException;

    void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext);
}
