package edu.emory.it.services.srd.util;

import java.util.ArrayList;
import java.util.List;

public class SrdMetricDetectorMeasure {
    private final String accountId;
    private final String detectorName;
    private final long millis;

    private final List<Measures> measures = new ArrayList<>();

    public SrdMetricDetectorMeasure(String accountId, String detectorName) {
        this.accountId = accountId;
        this.detectorName = detectorName;
        this.millis = System.currentTimeMillis();
    }

    public void addMeasure(String nameAndRequestType, Double value, String unit, String dimensionValue) {
        measures.add(new Measures(nameAndRequestType, value, unit, dimensionValue));
    }
    public String toJson() {
        StringBuilder sb = new StringBuilder(1024);
        boolean first = true;
        sb.append("{");
        sb.append("\"detectorName\":").append("\"").append(detectorName).append("\"");
        sb.append(",\"accountId\":").append("\"").append(accountId).append("\"");
        sb.append(",\"millis\":").append(millis);
        sb.append(",\"measures\":[");
        for (Measures m : measures) {
            if (!first) {
                sb.append(",");
            }
            first = false;
            sb.append("{");
            sb.append("\"nameAndRequestType\":").append("\"").append(m.nameAndRequestType).append("\"");
            sb.append(",\"value\":").append("\"").append(m.value).append("\"");
            sb.append(",\"unit\":").append("\"").append(m.unit).append("\"");
            sb.append(",\"dimensionValue\":").append("\"").append(m.dimensionValue).append("\"");
            sb.append("}");
        }
        sb.append("]}");
        return sb.toString();
    }

    public String getAccountId() {
        return accountId;
    }

    public String getDetectorName() {
        return detectorName;
    }

    @Override
    public String toString() {
        return toJson();
    }

    private static final class Measures {
        String nameAndRequestType;
        Double value;
        String unit;
        String dimensionValue;

        public Measures(String nameAndRequestType, Double value, String unit, String dimensionValue) {
            this.nameAndRequestType = nameAndRequestType;
            this.value = value;
            this.unit = unit;
            this.dimensionValue = dimensionValue;
        }
    }
}
