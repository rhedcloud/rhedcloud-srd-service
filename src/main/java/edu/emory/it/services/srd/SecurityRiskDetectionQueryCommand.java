package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionQuerySpecification;
import com.openii.openeai.commands.OpeniiRequestCommand;
import edu.emory.it.services.srd.provider.DynamoDBSecurityRiskDetectionQueryProvider;
import edu.emory.it.services.srd.provider.ProviderException;
import edu.emory.it.services.srd.provider.SecurityRiskDetectionQueryProvider;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.objects.resources.Error;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SecurityRiskDetectionQueryCommand extends OpeniiRequestCommand implements RequestCommand {
    private static final String LOGTAG = "[SrdQueryCommand] ";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionQueryCommand.class);



    private Document m_responseDoc;  // the primed XML response document

    private SecurityRiskDetectionQueryProvider securityRiskDetectionQueryProvider;

    public SecurityRiskDetectionQueryCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        } catch (EnterpriseConfigurationObjectException e) {
            e.printStackTrace();
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Initialize response document.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            logger.debug(LOGTAG + " responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
            m_responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"), getOutboundXmlValidation());
            if (m_responseDoc == null) {
                String errMsg = "Missing 'responseDocumentUri' property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + " " + errMsg);
                throw new InstantiationException(errMsg);
            }
        }
        catch (XmlDocumentReaderException e) {
            logger.fatal(LOGTAG + " Error initializing the primed documents.");
            e.printStackTrace();
            throw new InstantiationException(e.getMessage());
        }

        securityRiskDetectionQueryProvider = new DynamoDBSecurityRiskDetectionQueryProvider();
        securityRiskDetectionQueryProvider.init(getAppConfig());

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        long executionStart = System.currentTimeMillis();
        logger.info(LOGTAG + "[execute] - Start message number " + messageNumber);

        // Make a local copy of the response document to use in the replies.
        Document localResponseDoc = (Document) m_responseDoc.clone();
        String replyContents;

        // Convert the JMS Message to an XML Document
        Document inDoc;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        }
        catch (Exception e) {
            String errMsg = "Exception occurred processing input message in " +
                    "org.openeai.jms.consumer.commands.Command.  Exception: " +
                    e.getMessage();
            throw new CommandException(errMsg);
        }

        // Retrieve text portion of message.
        TextMessage msg = (TextMessage)aMessage;
        try {
            // Clear the message body for the reply, so we do not have to do it later.
            msg.clearBody();
        }
        catch (Exception e) {
            String errMsg = "Error clearing the message body.";
            throw new CommandException(errMsg + ". The exception is: " + e.getMessage());
        }
        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the ControlArea element.
        String msgObject = eControlArea.getAttribute("messageObject").getValue();
        String msgAction = eControlArea.getAttribute("messageAction").getValue();


        // Verify that the message object we are dealing with is a SecurityRiskDetection; if not, reply with an error.
        if (!msgObject.equalsIgnoreCase("SecurityRiskDetection")) {
            String errType = "application";
            String errCode = "OpenEAI-1001";
            String errDesc = "Unsupported message object: " + msgObject + ". This command expects 'SecurityRiskDetection'.";
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal(LOGTAG + " Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Get a configured querySpecification from AppConfig.
        SecurityRiskDetectionQuerySpecification querySpecification;
        try {
            querySpecification = (SecurityRiskDetectionQuerySpecification) getAppConfig().getObjectByType(
                    SecurityRiskDetectionQuerySpecification.class.getName());
        } catch (EnterpriseConfigurationObjectException eoce) {
            logger.fatal(LOGTAG + " Error retrieving a SecurityRiskDetection or SecurityRiskDetectionQuerySpecification " +
                    "object from AppConfig: The exception is: " + eoce.getMessage());
            String errType = "application";
            String errCode = "OpenEAI-1002";
            String errDesc = "Error retrieving EO.";
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        if (!msgAction.equalsIgnoreCase("Query")) {
            // The messageAction is invalid; it is not a query.
            String errType = "application";
            String errCode = "OpenEAI-1003";
            String errDesc = "Unsupported message action: " + msgAction + ". This command only supports 'query'.";
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Handle a Query-Request.
        logger.info(LOGTAG + "Handling an SecurityRiskDetection.Query-Request message.");
        Element eRequisition = inDoc.getRootElement().getChild("DataArea").getChild("SecurityRiskDetectionQuerySpecification");

        // Verify that SecurityRiskDetectionQuerySpecification element is not null; if it is, reply with an error.
        if (eRequisition == null) {
            String errType = "application";
            String errCode = "OpenEAI-1015";
            String errDesc = "Invalid generate element found in the Query-" +
                    "Request message. This command expects a SecurityRiskDetectionQuerySpecification.";
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Now build a SecurityRiskDetectionQuerySpecification object from the SecurityRiskDetectionRequisition element in the message.
        try {
            querySpecification.buildObjectFromInput(eRequisition);
        } catch (EnterpriseLayoutException ele) {
            // There was an error building the requisition object from the request.
            String errType = "application";
            String errCode = "OpenEAI-1018";
            String errDesc = "An error occurred building  object from the " +
                    "SecurityRiskDetectionQuerySpecification element in the Query-Request message. The exception " +
                    "is: " + ele.getMessage();
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Query the response.
        Collection<SecurityRiskDetection> securityRiskDetections;
        try {
            securityRiskDetections = securityRiskDetectionQueryProvider.query(querySpecification);
        } catch (ProviderException e) {
            String errType = "application";
            String errCode = "AwsSrdService-1001";
            String errDesc = "An error occurred running the detector. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errDesc);
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }


        // Serialize the response object and place it into the reply.
        try {
            localResponseDoc.getRootElement().getChild("DataArea").removeContent();
            for (SecurityRiskDetection securityRiskDetection : securityRiskDetections) {
                localResponseDoc.getRootElement().getChild("DataArea").addContent((Element)securityRiskDetection.buildOutputFromObject());
            }
            replyContents = buildReplyDocument(eControlArea, localResponseDoc);
        } catch (EnterpriseLayoutException ele) {
            // There was an error building the SecurityDetection element from the securityRiskDetection object.
            String errType = "application";
            String errCode = "OpenEAI-1020";
            String errDesc = "Error building SecurityRiskDetection element.  The exception is: " + ele.getMessage();
            logger.fatal(LOGTAG + " " + errDesc);
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Return the response with status success.
        long timeElapsed = System.currentTimeMillis() - executionStart;
        logger.info(LOGTAG + "[execute] - Finished in elapsed time " + timeElapsed + " ms");
        return getMessage(msg, replyContents);
    }
}
