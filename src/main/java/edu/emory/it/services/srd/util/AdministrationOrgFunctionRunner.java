package edu.emory.it.services.srd.util;

import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.organizations.model.OrganizationalUnit;
import com.amazonaws.services.organizations.model.Root;
import org.apache.logging.log4j.Logger;
import org.openeai.utils.lock.Key;

public class AdministrationOrgFunctionRunner {
    /**
     * Run the function in an Administration OU context.
     * SRDs are turned off (srdExempt=true) when the account is in the Administration OU.
     *
     * @param func function to run
     * @throws Exception on error
     */
    public static void run(AdministrationOrgFunction func)
            throws Exception {

        AWSOrgUtil awsOrgUtil = AWSOrgUtil.getInstance();
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();

        String accountId = func.getAccountId();
        Logger logger = func.getLogger();
        String LOGTAG = func.getLogtag();
        AWSOrganizations orgClient = func.getOrgClient();
        SecurityRiskContext srContext = func.getSecurityRiskContext();

        Key ouShuffleLockKey = null;
        String accountParentId = null;
        String accountParentName = null;

        // outer most try-finally is for OU shuffle locking
        try {
            srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorOuShuffleLockSet);
            try {
                ouShuffleLockKey = srContext.acquireOuShuffleLock(accountId);
            } finally {
                srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorOuShuffleLockSet);
            }
            if (ouShuffleLockKey == null) {
                throw new Exception(LOGTAG + "Could not acquire OU Shuffle lock");
            }

            // next level of try-finally is for SRD exempt management
            boolean accountWasExempted = false;
            long startTimeAccountWasExempted = 0;  // for timing how long the account was srdExempt
            try {
                // we are running in the context of either a detector or a remediator,
                // and the only way we can be doing that is if srdExempt was false.
                // so, if that pre-condition isn't met then something has changed and we
                // should abort the running of the function in the Administration Org.
                boolean unexpectedState = awsAccountServiceUtil.setAccountSrdExempt(accountId, "true", LOGTAG);
                if (unexpectedState) {
                    throw new Exception(LOGTAG + "Unexpected state setting srdExempt status");
                }
                accountWasExempted = true;
                startTimeAccountWasExempted = System.currentTimeMillis();

                // inner level of try-finally is for shuffling the account across OUs
                boolean accountWasMoved = false;
                long startTimeAccountWasMoved = 0;  // for timing how long the account is in Administration org
                try {
                    Object accountParent = awsOrgUtil.findAccountParent(orgClient, accountId, LOGTAG);
                    if (accountParent instanceof OrganizationalUnit) {
                        accountParentId = ((OrganizationalUnit) accountParent).getId();
                        accountParentName = ((OrganizationalUnit) accountParent).getName();
                    }
                    else if (accountParent instanceof Root) {
                        accountParentId = ((Root) accountParent).getId();
                        accountParentName = ((Root) accountParent).getName();
                    }
                    else {
                        // the reason was logged by findAccountParent()
                        throw new Exception(LOGTAG + "Cannot find the parent of account " + accountId + " with " + accountParent);
                    }

                    if (!accountParentId.equals(awsOrgUtil.getStandardOrganizationalUnitId())
                            && !accountParentId.equals(awsOrgUtil.getHipaaOrganizationalUnitId())) {
                        throw new Exception(LOGTAG + "Unexpected parent for account " + accountParentName + " (" + accountParentId + ")");
                    }

                    // try a few times to do the move
                    final String moveFromToMsg = " moved from " + accountParentName + " (" + accountParentId + ")"
                            + " to " + awsOrgUtil.getAdministrationOrganizationalUnitName();
                    final String moveMsg = "Account " + accountId + moveFromToMsg;
                    final String moveNotMsg = "Account " + accountId + " not" + moveFromToMsg;

                    for (int retry = 0, maxRetry = 5; retry < maxRetry; ++retry) {
                        try {
                            awsOrgUtil.moveAccountToAdministrationOrg(orgClient, accountId, accountParentId);
                            accountWasMoved = true;
                            break;
                        }
                        catch (Exception e) {
                            logger.info(LOGTAG + moveNotMsg + " on retry " + retry + " of " + maxRetry
                                    + ". The exception is: " + e.getMessage());
                        }
                    }
                    if (!accountWasMoved) {
                        throw new Exception(LOGTAG + moveNotMsg + " on exhausted retry");
                    }
                    // keep track of the amount of time in the Administration OU
                    startTimeAccountWasMoved = System.currentTimeMillis();
                    logger.info(LOGTAG + moveMsg);



                    logger.info(LOGTAG + "Running function in Administration Org for account " + accountId);
                    func.apply();
                }
                finally {
                    if (accountWasMoved) {
                        accountWasMoved = false;

                        // try harder to do the move back
                        final String moveFromToMsg = " moved from " + awsOrgUtil.getAdministrationOrganizationalUnitName()
                                + " to " + accountParentName + " (" + accountParentId + ")";
                        final String moveMsg = "Account " + accountId + moveFromToMsg;
                        final String moveNotMsg = "Account " + accountId + " not" + moveFromToMsg;

                        for (int retry = 0, maxRetry = 10; retry < maxRetry; ++retry) {
                            try {
                                awsOrgUtil.moveAccountFromAdministrationOrg(orgClient, accountId, accountParentId);
                                accountWasMoved = true;
                                break;
                            } catch (Exception e) {
                                logger.info(LOGTAG + moveNotMsg + " on retry " + retry + " of " + maxRetry
                                        + ". The exception is: " + e.getMessage());
                            }
                        }
                        if (accountWasMoved) {
                            long totalTime = System.currentTimeMillis() - startTimeAccountWasMoved;
                            logger.info(LOGTAG + moveMsg + ". Time spent in AdministrationOrg for account " + accountId
                                    + " was " + totalTime + " ms");
                        }
                        else {
                            logger.info(LOGTAG + moveNotMsg + " on exhausted retry");
                        }
                    }
                }
            }
            finally {
                if (accountWasExempted) {
                    // if we fail to toggle the srdExempt flag a message will be logged
                    // but there is nothing else we can do at this point
                    boolean unexpectedState = awsAccountServiceUtil.setAccountSrdExempt(accountId, "false", LOGTAG);
                    if (unexpectedState) {
                        logger.error(LOGTAG + "Unable to restore previous setting of SRD exempt status for account " + accountId);
                    }
                    else {
                        long totalTime = System.currentTimeMillis() - startTimeAccountWasExempted;
                        logger.info(LOGTAG + "Time spent in SRD exempt status for account "
                                + accountId + " was " + totalTime + " ms");
                    }
                }
            }
        }
        finally {
            srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorOuShuffleLockRelease);
            try {
                srContext.releaseOuShuffleLock(accountId, ouShuffleLockKey);
            } finally {
                srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorOuShuffleLockRelease);
            }
        }
    }
}
