package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.workdocs.AmazonWorkDocs;
import com.amazonaws.services.workdocs.AmazonWorkDocsClient;
import com.amazonaws.services.workdocs.model.DescribeUsersRequest;
import com.amazonaws.services.workdocs.model.DescribeUsersResult;
import com.amazonaws.services.workdocs.model.User;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.Properties;

/**
 * Check all users in WorkDocs for non-emory domains in the email<br>
 *
 * @see edu.emory.it.services.srd.remediator.WorkDocsNonEmoryDomainRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class WorkDocsNonEmoryDomainDetector extends AbstractSecurityRiskDetector {
    private String organizationId;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
        super.init(aConfig, credentialsProvider, securityRole);

        String LOGTAG = "[" + DetectionType.WorkDocsNonEmoryDomain + "] ";
        try {
            Properties properties = appConfig.getProperties(getBaseName());
            organizationId = properties.getProperty("organizationId");
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.error(LOGTAG + e.getMessage());
            throw new SecurityRiskDetectionException(e);
        }
    }

    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonWorkDocs workDocs;
            try {
                workDocs = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonWorkDocsClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.WorkDocsNonEmoryDomain,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:workdocs:" + region.getName() + ":" + accountId + ":user/";
            checkWorkDocs(workDocs, detection, arnPrefix, srContext.LOGTAG);
        }
    }

    private void checkWorkDocs(AmazonWorkDocs workDocs, SecurityRiskDetection detection, String arnPrefix, String LOGTAG) {
        DescribeUsersRequest request = new DescribeUsersRequest().withOrganizationId(organizationId);
        boolean done = false;
        try {
            while (!done) {
                DescribeUsersResult result = workDocs.describeUsers(request);

                for (User user : result.getUsers()) {
                    if (user.getEmailAddress() == null ||
                            !(user.getEmailAddress().endsWith("emory.edu") || user.getEmailAddress().endsWith("emoryhealthcare.org")) ) {
                        addDetectedSecurityRisk(detection, LOGTAG,
                                DetectionType.WorkDocsNonEmoryDomain, arnPrefix + user.getId());
                    }
                }

                request.setMarker(result.getMarker());

                if (request.getMarker() == null) {
                    done = true;
                }
            }
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.WorkDocsNonEmoryDomain,
                    LOGTAG, "Error describing WorkDocs users", e);
        }
    }

    @Override
    public String getBaseName() {
        return "WorkDocsNonEmoryDomain";
    }
}
