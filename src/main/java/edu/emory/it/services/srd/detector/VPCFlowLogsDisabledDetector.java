package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeFlowLogsRequest;
import com.amazonaws.services.ec2.model.DescribeFlowLogsResult;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Vpc;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;
import java.util.Optional;

/**
 * Check all registered VPCs to ensure Flow Logs are enabled.<br>
 *
 * @see edu.emory.it.services.srd.remediator.VPCFlowLogsDisabledRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class VPCFlowLogsDisabledDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
        List<VirtualPrivateCloud> registeredVpcs;
        try {
            registeredVpcs = awsAccountServiceUtil.getRegisteredVpcs(accountId, srContext.LOGTAG);
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.VPCFlowLogsDisabled,
                    srContext.LOGTAG, "Error getting registered VPCs from AWS Account Service", e);
            return;
        }

        for (VirtualPrivateCloud registeredVpc : registeredVpcs) {
            for (Region region : getRegions()) {
                final AmazonEC2 ec2;
                try {
                    ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                }
                catch (EmoryAwsClientBuilderException e) {
                    // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                    logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                    continue;
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.VPCFlowLogsDisabled,
                            srContext.LOGTAG, "On region: " + region.getName(), e);
                    return;
                }

                DescribeVpcsResult describeVpcsResult = ec2.describeVpcs();

                Optional<Vpc> awsVpcO = describeVpcsResult.getVpcs().stream()
                    .filter(vpc -> vpc.getVpcId().equals(registeredVpc.getVpcId()))
                    .findFirst();  // findFirst because there can only be one

                if (awsVpcO.isPresent()) {
                    Vpc awsVpc = awsVpcO.get();

                    DescribeFlowLogsRequest describeFlowLogsRequest = new DescribeFlowLogsRequest()
                            .withFilter(new Filter().withName("resource-id").withValues(awsVpc.getVpcId()));
                    DescribeFlowLogsResult describeFlowLogsResult;
                    do {
                        describeFlowLogsResult = ec2.describeFlowLogs(describeFlowLogsRequest);

                        if (describeFlowLogsResult.getFlowLogs().isEmpty()) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.VPCFlowLogsDisabled,
                                    "arn:aws:ec2:" + region.getName() + ":" + accountId + ":vpc/" + awsVpc.getVpcId());
                        }

                        describeFlowLogsRequest.setNextToken(describeFlowLogsResult.getNextToken());
                    } while (describeFlowLogsResult.getNextToken() != null);

                    // found the AWS VPC so no need to search other regions
                    break;
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "VPCFlowLogsDisabled";
    }
}
