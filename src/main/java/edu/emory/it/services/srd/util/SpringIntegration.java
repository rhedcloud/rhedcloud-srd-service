package edu.emory.it.services.srd.util;

import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringIntegration {
    private static final Object lock = new Object();
    private static SpringIntegration instance;

    private String driverClassName;
    private String jdbcUrl;
    private String username;
    private String password;
    private String[] basePackages;

    private AbstractApplicationContext context;

    public static SpringIntegration getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new SpringIntegration();
                }
            }
        }
        return instance;
    }

    /**
     * Initialize the singleton.
     *
     * @param driverClassName JDBC driver class name
     * @param jdbcUrl JDBC connection URL
     * @param username username
     * @param password password
     * @param basePackages EntityManager looks for classes starting here
     */
    public void initialize(String driverClassName, String jdbcUrl, String username, String password, String... basePackages) {
        // these must be set before the application context is initialized
        this.driverClassName = driverClassName;
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
        this.basePackages = basePackages;

        context = new AnnotationConfigApplicationContext(basePackages);
    }

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String[] getBasePackages() {
        return basePackages;
    }

    public <T> T getBean(Class<T> requiredType) throws BeansException {
        return context.getBean(requiredType);
    }
}
