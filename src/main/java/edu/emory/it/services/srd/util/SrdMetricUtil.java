package edu.emory.it.services.srd.util;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.PriorityBlockingQueue;

public class SrdMetricUtil {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SrdMetricUtil.class);
    private static final Object lock = new Object();

    private static SrdMetricUtil instance;

    //
    private int nbrActiveStandardAccounts = 0;
    private int nbrActiveHipaaAccounts = 0;
    private int nbrActiveEnhancedSecurityAccounts = 0;
    private int nbrExemptAccounts = 0;
    private int nbrStandardDetectors = 0;
    private int nbrHipaaDetectors = 0;
    private int nbrEnhancedSecurityDetectors = 0;
    private int nbrRelevantDetectors = 0;

    private String lastSecurityRiskDetectionIdGenerated;
    private String lastSecurityRiskDetectionIdSyncPersister;
    private String lastSecurityRiskDetectionIdSyncAccountNotification;

    private final Map<String, CircularFifoQueue<SrdMetricDetectorMeasure>> detectorAnalysis = new HashMap<>();

    public static SrdMetricUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new SrdMetricUtil();
                }
            }
        }
        return instance;
    }

    public void cacheAccountMetrics(int nbrActiveStandardAccounts, int nbrActiveHipaaAccounts, int nbrActiveEnhancedSecurityAccounts,
                                    int nbrExemptAccounts,
                                    int nbrStandardDetectors, int nbrHipaaDetectors, int nbrEnhancedSecurityDetectors,
                                    int nbrRelevantDetectors) {
        this.nbrActiveStandardAccounts = nbrActiveStandardAccounts;
        this.nbrActiveHipaaAccounts = nbrActiveHipaaAccounts;
        this.nbrActiveEnhancedSecurityAccounts = nbrActiveEnhancedSecurityAccounts;
        this.nbrExemptAccounts = nbrExemptAccounts;
        this.nbrStandardDetectors = nbrStandardDetectors;
        this.nbrHipaaDetectors = nbrHipaaDetectors;
        this.nbrEnhancedSecurityDetectors = nbrEnhancedSecurityDetectors;
        this.nbrRelevantDetectors = nbrRelevantDetectors;
    }

    public int getNbrActiveStandardAccounts() {
        return nbrActiveStandardAccounts;
    }

    public int getNbrActiveHipaaAccounts() {
        return nbrActiveHipaaAccounts;
    }

    public int getNbrActiveEnhancedSecurityAccounts() {
        return nbrActiveEnhancedSecurityAccounts;
    }

    public int getNbrExemptAccounts() {
        return nbrExemptAccounts;
    }

    public int getNbrStandardDetectors() {
        return nbrStandardDetectors;
    }

    public int getNbrHipaaDetectors() {
        return nbrHipaaDetectors;
    }

    public int getNbrEnhancedSecurityDetectors() {
        return nbrEnhancedSecurityDetectors;
    }

    public int getNbrRelevantDetectors() {
        return nbrRelevantDetectors;
    }

    public void cacheLastSecurityRiskDetectionIdGenerated(String securityRiskDetectionId) {
        this.lastSecurityRiskDetectionIdGenerated = securityRiskDetectionId;
    }
    public void cacheLastSecurityRiskDetectionIdSyncPersister(String securityRiskDetectionId) {
        this.lastSecurityRiskDetectionIdSyncPersister = securityRiskDetectionId;
    }
    public void cacheLastSecurityRiskDetectionIdSyncAccountNotification(String securityRiskDetectionId) {
        this.lastSecurityRiskDetectionIdSyncAccountNotification = securityRiskDetectionId;
    }
    public String getLastSecurityRiskDetectionIdGenerated() {
        return lastSecurityRiskDetectionIdGenerated;
    }
    public String getLastSecurityRiskDetectionIdSyncPersister() {
        return lastSecurityRiskDetectionIdSyncPersister;
    }
    public String getLastSecurityRiskDetectionIdSyncAccountNotification() {
        return lastSecurityRiskDetectionIdSyncAccountNotification;
    }

    public void cacheDetectorAnalysis(SrdMetricDetectorMeasure da) {
        String key = da.getDetectorName() + "-" + da.getAccountId();
        this.detectorAnalysis.computeIfAbsent(key, k -> new CircularFifoQueue<>(1)).add(da);
    }
    public Map<String, CircularFifoQueue<SrdMetricDetectorMeasure>> getDetectorAnalysis() {
        return detectorAnalysis;
    }

    /*
     * Track security risk detection ids for the purpose of reporting the rate of detections.
     * This is mostly per-instance tracking but that should be representative of the entire cluster.
     */
    private static class DetectionIdTracking {
        String detectionId;
        long millis;

        public DetectionIdTracking(String detectionId) {
            this.detectionId = detectionId;
            this.millis = System.currentTimeMillis();
        }

        public String getDetectionId() {
            return detectionId;
        }
    }

    private final PriorityBlockingQueue<DetectionIdTracking> securityRiskDetectionIdTracking
            = new PriorityBlockingQueue<>(2000, Comparator.comparing(DetectionIdTracking::getDetectionId));
    private static final DetectionIdTracking[] DETECTION_ID_TRACKING_ARRAY_TYPE = new DetectionIdTracking[0];
    private static final long DETECTION_ID_TRACKING_TTL = 60 * 1000;

    /*
     * see comments in detectionRatePerSecond() about fallback handling
     */
    public void trackSecurityRiskDetectionIdFallback(String securityRiskDetectionId) {
        trackSecurityRiskDetectionId("-" + securityRiskDetectionId);
    }
    private boolean isDetectionIdFallback(DetectionIdTracking detectionIdTracking) {
        return detectionIdTracking.detectionId.charAt(0) == '-';
    }
    public void trackSecurityRiskDetectionId(String securityRiskDetectionId) {
        DetectionIdTracking detectionIdTracking = new DetectionIdTracking(securityRiskDetectionId);
        securityRiskDetectionIdTracking.offer(detectionIdTracking);

        // org.apache.log4j.LogManager.getLogger(SrdMetricUtil.class).setLevel(org.apache.log4j.Level.DEBUG)
        if (logger.isInfoEnabled()) {
            detectionRatePerSecond();
        }

        // time after which tracking entries are expired
        final long expireTime = detectionIdTracking.millis - DETECTION_ID_TRACKING_TTL;

        while (true) {
            DetectionIdTracking peek = securityRiskDetectionIdTracking.peek();
            if (peek == null)
                return;  // shouldn't happen but peek is allowed to return null
            String reap = null;
            if (isDetectionIdFallback(peek)) {
                reap = "fallback";
            }
            else if (peek.millis < expireTime) {
                reap = "expired";
            }
            if (reap == null)
                return;

            // oldest element has expired so remove it and loop around looking for more expired elements
            DetectionIdTracking poll = securityRiskDetectionIdTracking.poll();

            if (logger.isInfoEnabled()) {
                // yikes can happen when another thread removes a tracking between the peek and poll
                // not a big deal to loose the occasional tracking
                String yikes = (peek.millis != poll.millis) ? " yikes" : "";
                logger.info("DetectionRatePerSecond reap " + reap
                        + " expireTime " + expireTime
                        + " now " + detectionIdTracking.millis + "/" + detectionIdTracking.detectionId
                        + " peek " + peek.millis + "/" + peek.detectionId
                        + " poll " + poll.millis + "/" + poll.detectionId
                        + yikes);
            }
        }
    }

    public double detectionRatePerSecond() {
        DetectionIdTracking[] ary = securityRiskDetectionIdTracking.toArray(DETECTION_ID_TRACKING_ARRAY_TYPE);
        Arrays.sort(ary, Comparator.comparing(DetectionIdTracking::getDetectionId));
        if (ary.length < 2)
            return 0;

        DetectionIdTracking endingTracking = ary[ary.length - 1];
        DetectionIdTracking startingTracking = ary[0];

        /*
         * Generally, the security risk detection id comes from incrementing a DynamoDB Number data type
         * which can have up to 38 digits of precision, and has an upper range
         * of 9.9999999999999999999999999999999999999E+125 (yeah that's big)
         * See https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Limits.html#limits-data-types
         *
         * This means we have to use double (or BigInteger) to represent them.
         *
         * When calculating how many sequence values were consumed there is a special case to consider.
         * If a fallback sequence generator was used, the detection id will be prefixed with a '-'
         * in which case we just count the number of tracking slots as an estimation.
         */
        double samplingIntervalInSeconds = ((double) endingTracking.millis - startingTracking.millis) / 1000;
        double sequencesConsumed;
        if (isDetectionIdFallback(endingTracking) || isDetectionIdFallback(startingTracking)) {
            sequencesConsumed = ary.length;
        }
        else {
            sequencesConsumed = Double.parseDouble(endingTracking.detectionId) - Double.parseDouble(startingTracking.detectionId);
        }

        // calculate the per-second rate (being defensive of 0 denominator)
        double detectionRatePerSecond = (samplingIntervalInSeconds == 0) ? 0 : sequencesConsumed / samplingIntervalInSeconds;

        logger.info("DetectionRatePerSecond is " + detectionRatePerSecond
                + " with ary len " + ary.length
                + " sequence E/S " + endingTracking.detectionId + "/" + startingTracking.detectionId
                + " millis E/S " + endingTracking.millis + "/" + startingTracking.millis
                + " rate C/I " + sequencesConsumed + "/" + samplingIntervalInSeconds);

        return detectionRatePerSecond;
    }
}
