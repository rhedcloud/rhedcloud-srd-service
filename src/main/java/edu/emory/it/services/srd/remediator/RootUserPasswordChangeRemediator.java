package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.organizations.model.AWSOrganizationsException;
import com.amazonaws.services.organizations.model.OrganizationalUnit;
import com.amazonaws.services.organizations.model.Root;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.AWSOrgUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Move account to a quarantine Organizational Unit.
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.RootUserPasswordChangeDetector the detector
 */
public class RootUserPasswordChangeRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudtrail:", srContext.LOGTAG, DetectionType.RootUserPasswordChange))
            return;
        // like arn:aws:cloudtrail::123456789012:event/abc-def-ghi-jkl/PasswordRecoveryRequested
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        AWSOrganizations orgClient = getOrganizationsClient(srContext);
        AWSOrgUtil awsOrgUtil = AWSOrgUtil.getInstance();

        Object accountParent = awsOrgUtil.findAccountParent(orgClient, accountId, srContext.LOGTAG);
        String accountParentId;
        if (accountParent instanceof OrganizationalUnit) {
            accountParentId = ((OrganizationalUnit) accountParent).getId();
        }
        else if (accountParent instanceof Root) {
            accountParentId = ((Root) accountParent).getId();
        }
        else {
            // the reason was logged by findAccountParent()
            setError(detected, srContext.LOGTAG, "Cannot find the parent of account " + accountId + " with " + accountParent);
            return;
        }

        /*
         * as detailed in the class level javadoc for the detector,
         * account root user password change is a two step process - click 'Forgot password?' link then follow link in email.
         * each step generates a separate event - PasswordRecoveryRequested and PasswordRecoveryCompleted respectively.
         *
         * each one generates a risk so after the first risk is remediated the account will have been
         * quarantined and no longer needs to be remediated
         */
        if (awsOrgUtil.getQuarantineOrganizationalUnitId().equals(accountParentId)) {
            setNoLongerRequired(detected, srContext.LOGTAG, "Account " + accountId + " has already been quarantined");
            return;
        }

        try {
            awsOrgUtil.moveAccountToQuarantineOrg(orgClient, accountId, accountParentId);
        } catch (AWSOrganizationsException e) {
            setError(detected, srContext.LOGTAG, "Error while trying to quarantine account " + accountId, e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG, "Account " + accountId + " has been quarantined to "
                + awsOrgUtil.getQuarantineOrganizationalUnitName());
    }
}
