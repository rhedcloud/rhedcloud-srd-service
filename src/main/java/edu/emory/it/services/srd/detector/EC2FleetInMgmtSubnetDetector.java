package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeFleetsRequest;
import com.amazonaws.services.ec2.model.DescribeFleetsResult;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsRequest;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsResult;
import com.amazonaws.services.ec2.model.FleetData;
import com.amazonaws.services.ec2.model.FleetLaunchTemplateConfig;
import com.amazonaws.services.ec2.model.FleetLaunchTemplateOverrides;
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateVersion;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Detects EC2 Fleets configured to launch in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.EC2FleetInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EC2FleetInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonEC2 ec2;
            try {
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.EC2FleetInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

            String arnPrefix = "arn:aws:ec2:" + region.getName() + ":" + accountId + ":fleet/";

            DescribeFleetsRequest describeFleetsRequest = new DescribeFleetsRequest();
            boolean done = false;
            while (!done) {
                DescribeFleetsResult describeFleetsResult = ec2.describeFleets(describeFleetsRequest);

                for (FleetData fleet : describeFleetsResult.getFleets()) {
                    if (fleet.getFleetState().equals("deleted") || fleet.getFleetState().equals("deleted_terminating")) {
                        continue;
                    }
                    if ( fleet.getLaunchTemplateConfigs() == null) {
                        continue;
                    }
                    boolean found = false;
                    for (FleetLaunchTemplateConfig template : fleet.getLaunchTemplateConfigs()) {

                        String subnetId = null;
                        for (FleetLaunchTemplateOverrides overrides : template.getOverrides()) {
                            if (overrides.getSubnetId() != null) {
                                subnetId = overrides.getSubnetId();
                            }
                            if (disallowedSubnets.contains(overrides.getSubnetId())) {
                                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                        DetectionType.EC2FleetInMgmtSubnet, arnPrefix + fleet.getFleetId());
                                found = true;
                                break;
                            }
                        }
                        if (found) {
                            break;
                        }
                        if (subnetId == null) {
                            //check launch template
                            String templateId = template.getLaunchTemplateSpecification().getLaunchTemplateId();
                            String templateVersion = template.getLaunchTemplateSpecification().getVersion();
                            DescribeLaunchTemplateVersionsRequest describeLaunchTemplateVersionsRequest = new DescribeLaunchTemplateVersionsRequest()
                                    .withLaunchTemplateId(templateId)
                                    .withVersions(templateVersion);

                            DescribeLaunchTemplateVersionsResult launchTemplatesResult = ec2.describeLaunchTemplateVersions(describeLaunchTemplateVersionsRequest);

                            LaunchTemplateVersion launchTemplate = launchTemplatesResult.getLaunchTemplateVersions().get(0);

                            for (LaunchTemplateInstanceNetworkInterfaceSpecification networkInterface : launchTemplate.getLaunchTemplateData().getNetworkInterfaces()) {
                                String subnet = networkInterface.getSubnetId();
                                if (disallowedSubnets.contains(subnet)) {
                                    found = true;
                                    addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                            DetectionType.EC2FleetInMgmtSubnet, arnPrefix + fleet.getFleetId());
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                    }

                }

                describeFleetsRequest.setNextToken(describeFleetsResult.getNextToken());

                if (describeFleetsResult.getNextToken() == null) {
                    done = true;
                }

            }
        }
    }

    @Override
    public String getBaseName() {
        return "EC2FleetInMgmtSubnet";
    }
}
