package edu.emory.it.services.srd.util;

import com.amazonaws.metrics.MetricType;

public enum SrdMetricTypeField implements MetricType {
    SRD_ThreadDelay,
    SRD_DetectorExecution,

    SRD_Detect,
    SRD_Remediate,
    SRD_DetectedIssueCount,

    SRD_DetectorSetup,
    SRD_DetectorFinalizing,
    SRD_SequenceGeneration,
    SRD_SequenceGenerationWaitingCount,
    SRD_BuildResults,
    SRD_DetectorLockSet,
    SRD_DetectorLockRelease,
    SRD_DetectorLockSetWaitingCount,
    SRD_DetectorLockReleaseWaitingCount,
    SRD_DetectorOuShuffleLockSet,
    SRD_DetectorOuShuffleLockRelease,
    SRD_PublishSync,
    SRD_PublishSyncWaitingCount,

    SRD_DetectorCommandException
}
