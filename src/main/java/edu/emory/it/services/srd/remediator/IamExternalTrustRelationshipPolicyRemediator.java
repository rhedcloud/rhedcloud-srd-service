package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.GetRoleRequest;
import com.amazonaws.services.identitymanagement.model.GetRoleResult;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.UpdateAssumeRolePolicyRequest;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.AWSPolicyUtilNoChangeException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

/**
 * Remediates IAM External Trust roles<br>
 *
 * <p>Rewrites the target role's policy by setting any policy statement with Effect = "Allow" and an external principal to
 * to the owner account instead.   The rest of the policy will be kept as before.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete<br> Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.IamExternalTrustRelationshipPolicyDetector the detector
 */
public class IamExternalTrustRelationshipPolicyRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private static final String LOGTAG = "[IamExternalTrustRelationshipPolicyRemediator] ";

	@Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:iam:", srContext.LOGTAG, DetectionType.IamExternalTrustRelationshipPolicy))
            return;
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null) return;

        String roleName = (arn.getResource().getQualifier() != null)
                ? arn.getResource().getQualifier() : arn.getResource().getResource();
        
        logger.info(LOGTAG+"roleName="+roleName);
        
        if (roleName.contains("/")) {
        	String[] s = roleName.split("/");
        	roleName = s[s.length-1];
        	logger.info(LOGTAG+"roleName contains paths, naked roleName="+roleName);
        }

        AmazonIdentityManagement iam;
        try {
            iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Role Policy", e);
            return;
        }

        GetCallerIdentityResult identityResult;
        try {
            AWSSecurityTokenService stsClient = getSecurityTokenServiceClient(srContext);
            identityResult = stsClient.getCallerIdentity(new GetCallerIdentityRequest());
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error getting caller identity", e);
            return;
        }

        String updatedPolicy;
        try {
            GetRoleRequest getRoleRequest = new GetRoleRequest()
                    .withRoleName(roleName);
            logger.info(LOGTAG+"Getting role with getRoleRequest: "+getRoleRequest);
            GetRoleResult getRoleResult = iam.getRole(getRoleRequest);
            logger.info(LOGTAG+"Got role with getRoleResult: "+getRoleResult);
            Set<String> whitelistedAccounts = new HashSet<>();
            whitelistedAccounts.add(accountId);
            whitelistedAccounts.add(identityResult.getAccount());
            // policy documents have to be decoded
            // see https://docs.aws.amazon.com/IAM/latest/APIReference/API_PolicyVersion.html
            String policy = URLDecoder.decode(getRoleResult.getRole().getAssumeRolePolicyDocument(), StandardCharsets.UTF_8.name());
            updatedPolicy = AWSPolicyUtil.removeAllowToNonWhitelistedAccounts(whitelistedAccounts, policy);
        }
        catch (AWSPolicyUtilNoChangeException e) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Role Policy does not have externally shared account.  No changes have been made.");
            return;
        }
        catch (NoSuchEntityException e) {
            setNoLongerRequired(detected, srContext.LOGTAG, "Role cannot be found.");
            return;
        }
        catch (UnsupportedEncodingException e) {
            setError(detected, srContext.LOGTAG, "Policy document cannot be decoded", e);
            return;
        } catch (RuntimeException e) {
        	logger.error(LOGTAG+"Runtime exception:",e);
        	e.printStackTrace();
            setError(detected, srContext.LOGTAG, "Runtime exception", e);
            return;
        }

        if (updatedPolicy == null || updatedPolicy.isEmpty()) {
            updatedPolicy = "{" +
                    "\"Version\":\"2012-10-17\"," +
                    "\"Statement\":[" +
                    "{" +
                    "\"Effect\":\"Allow\"," +
                    "\"Principal\":{" +
                    "\"AWS\":\"arn:aws:iam::"+accountId+":root\"" +
                    "}," +
                    "\"Action\":\"sts:AssumeRole\"," +
                    "\"Condition\":{}" +
                    "}" +
                    "]" +
                    "}";
        }

        try {
            UpdateAssumeRolePolicyRequest updateAssumeRolePolicyRequest = new UpdateAssumeRolePolicyRequest()
                    .withRoleName(roleName)
                    .withPolicyDocument(updatedPolicy);
            iam.updateAssumeRolePolicy(updateAssumeRolePolicyRequest);

            setSuccess(detected, srContext.LOGTAG,
                    "Role policy successfully modified to remove any statements that permits sharing the key with other accounts.");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error updating update assumed role policy", e);
        }
    }
}
