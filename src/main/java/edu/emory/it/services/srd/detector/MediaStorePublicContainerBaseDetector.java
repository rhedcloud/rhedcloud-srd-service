package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.services.mediastore.AWSMediaStore;
import com.amazonaws.services.mediastore.AWSMediaStoreClient;
import com.amazonaws.services.mediastore.model.Container;
import com.amazonaws.services.mediastore.model.ListContainersRequest;
import com.amazonaws.services.mediastore.model.ListContainersResult;
import com.amazonaws.services.mediastoredata.AWSMediaStoreData;
import com.amazonaws.services.mediastoredata.AWSMediaStoreDataClient;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.MediaStoreUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.List;

/**
 * Base class for the Standard and HIPAA detectors.
 *
 * @see edu.emory.it.services.srd.detector.MediaStorePublicContainerStandardDetector the detector
 * @see edu.emory.it.services.srd.detector.MediaStorePublicContainerHipaaDetector the detector
 */
public abstract class MediaStorePublicContainerBaseDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            for (Region region : getRegions()) {
                try {
                    AWSMediaStore msClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSMediaStoreClient.class);

                    String listContainersRequestToken = null;
                    do {
                        ListContainersRequest listContainersRequest = new ListContainersRequest()
                                .withMaxResults(100);  // get as many as is allowed
                        // deal with pagination
                        if (listContainersRequestToken != null)
                            listContainersRequest.setNextToken(listContainersRequestToken);
                        ListContainersResult listContainersResult = msClient.listContainers(listContainersRequest);
                        listContainersRequestToken = listContainersResult.getNextToken();

                        for (Container container : listContainersResult.getContainers()) {
                            if (!container.getStatus().equals("ACTIVE")) {
                                // can only do container operations if the status is ACTIVE.
                                // if they're in the middle of being created then we'll get them next time,
                                // or if they're being deleted then we don't need to bother this time.
                                continue;
                            }

                            final AWSMediaStoreData msdClient = getClient(accountId, region.getName(), container.getEndpoint(),
                                    srContext.getMetricCollector(), AWSMediaStoreDataClient.class);

                            // collect all the objects in the container
                            List<String> containerObjects = new MediaStoreUtil(msdClient).getContainerObjects();

                            for (String path : containerObjects) {
                                String uri = container.getEndpoint() + "/" + path;

                                // if a range GET request is made, the response is like this - HTTP/1.1 206 Partial Content
                                // HttpGet request = new HttpGet(uri);
                                // request.setHeader("Range", "bytes=0-1");

                                HttpHead request = new HttpHead(uri);

                                try (CloseableHttpResponse response = httpclient.execute(request)) {
                                    if (response.getStatusLine().getStatusCode() >= 200
                                            && response.getStatusLine().getStatusCode() < 300) {
                                        // bad - was able to GET the object over the public internet
                                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                                DetectionType.MediaStorePublicContainer, container.getARN());
                                        break;
                                    }
                                }
                            }
                        }
                    } while (listContainersRequestToken != null);
                }
                catch (EmoryAwsClientBuilderException e) {
                    // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                    logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                }
                catch (Exception e) {
                    // Elemental MediaStore doesn't have endpoints in all regions so certain exceptions here are not unexpected
                    if (e instanceof SdkClientException && e.getCause() instanceof java.net.UnknownHostException) {
                        logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                    }
                    else {
                        setDetectionError(detection, DetectionType.MediaStorePublicContainer,
                                srContext.LOGTAG, "On region: " + region.getName(), e);
                        return;
                    }
                }
            }
        }
        catch (IOException e) {
            setDetectionError(detection, DetectionType.MediaStorePublicContainer,
                    srContext.LOGTAG, "Error with HTTP client", e);
        }
        // detector name does not match a DetectionType so ensure DetectionResult is set
        if (detection.getDetectionResult() == null) {
            setDetectionSuccess(detection, DetectionType.MediaStorePublicContainer, srContext.LOGTAG);
        }
    }
}
