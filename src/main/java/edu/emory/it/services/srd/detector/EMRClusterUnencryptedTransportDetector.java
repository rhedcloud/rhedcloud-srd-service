package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.model.Cluster;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.function.BiPredicate;

/**
 * Detect Elastic Map Reduce clusters where encryption in-transit is not enabled.
 *
 * @see edu.emory.it.services.srd.remediator.EMRClusterUnencryptedTransportRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EMRClusterUnencryptedTransportDetector extends EMRClusterBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, DetectionType.EMRClusterUnencryptedTransport);
    }

    @Override
    protected BiPredicate<AmazonElasticMapReduce, Cluster> getPredicate() {
        return (emrClient, cluster) ->
                ! super.hasSecurityConfigurationSetting(emrClient, cluster, "EnableInTransitEncryption");
    }

    @Override
    public String getBaseName() {
        return "EMRClusterUnencryptedTransport";
    }
}
