package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.CancelSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.CancelSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsRequest;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsResult;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsResult;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateConfig;
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateVersion;
import com.amazonaws.services.ec2.model.SpotFleetLaunchSpecification;
import com.amazonaws.services.ec2.model.SpotFleetRequestConfig;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.List;
import java.util.Set;

/**
 * Remediates Spot Instance / Block / Fleet configured to launch in a disallowed subnet<br>
 *
 * <p>The remediator deletes the spot request configured to launched in a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.EC2SpotScheduledInMgmtSubnetDetector the detector
 */
public class EC2SpotScheduledInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG,
                DetectionType.EC2SpotScheduledInMgmtSubnet, DetectionType.EC2SpotFleetScheduledInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String spotRequestId = arn[5].replace("spot-request/", "");
        String region = arn[3];

        final AmazonEC2 ec2;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Spot Request '" + spotRequestId + "'", e);
            return;
        }

        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

        if (detected.getType().equals(DetectionType.EC2SpotScheduledInMgmtSubnet.name())) {
            remediateSpotInstances(ec2, spotRequestId, detected, disallowedSubnets, srContext.LOGTAG);
        } else {
            remediateSpotFleets(ec2, spotRequestId, detected, disallowedSubnets, srContext.LOGTAG);
        }
    }


    private void remediateSpotInstances(AmazonEC2 ec2, String spotRequestId,
                                        DetectedSecurityRisk detected, Set<String> disallowedSubnets, String LOGTAG) {
        DescribeSpotInstanceRequestsRequest spotInstanceRequestsRequest = new DescribeSpotInstanceRequestsRequest().withSpotInstanceRequestIds(spotRequestId);
        DescribeSpotInstanceRequestsResult describeSpotInstanceRequestsResult = ec2.describeSpotInstanceRequests(spotInstanceRequestsRequest);

        List<SpotInstanceRequest> spotInstanceRequests = describeSpotInstanceRequestsResult.getSpotInstanceRequests();

        if (spotInstanceRequests == null
                || spotInstanceRequests.isEmpty()) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' cannot be found. It may have already been deleted.");
            return;
        }


        SpotInstanceRequest spotInstanceRequest = spotInstanceRequests.get(0);

        if (spotInstanceRequest.getState().equals("cancelled")) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' is already cancelled.");
            return;
        }

        boolean found = false;
        if (disallowedSubnets.contains(spotInstanceRequest.getLaunchSpecification().getSubnetId())) {
            found = true;
        }
        if (!found) {
            for (InstanceNetworkInterfaceSpecification networkInterface : spotInstanceRequest.getLaunchSpecification().getNetworkInterfaces()) {
                String subnet = networkInterface.getSubnetId();
                if (disallowedSubnets.contains(subnet)) {
                    found = true;
                    break;
                }
            }
        }

        if (!found) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' is not in a disallowed subnet.  Remediation not required.");
            return;
        }

        //remediate
        CancelSpotInstanceRequestsRequest cancelRequest = new CancelSpotInstanceRequestsRequest().withSpotInstanceRequestIds(spotRequestId);
        ec2.cancelSpotInstanceRequests(cancelRequest);

        setSuccess(detected, LOGTAG, "Spot Request '" + spotRequestId + "' has been cancelled.");

    }


    private void remediateSpotFleets(AmazonEC2 ec2, String spotRequestId,
                                     DetectedSecurityRisk detected, Set<String> disallowedSubnets, String LOGTAG) {
        DescribeSpotFleetRequestsRequest describeSpotFleetRequestsRequest = new DescribeSpotFleetRequestsRequest().withSpotFleetRequestIds(spotRequestId);
        DescribeSpotFleetRequestsResult describeSpotFleetRequestsResult = ec2.describeSpotFleetRequests(describeSpotFleetRequestsRequest);
        List<SpotFleetRequestConfig> spotFleetRequestConfigs = describeSpotFleetRequestsResult.getSpotFleetRequestConfigs();

        if (spotFleetRequestConfigs == null
                || spotFleetRequestConfigs.isEmpty()) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' cannot be found. It may have already been deleted.");
            return;
        }

        SpotFleetRequestConfig spotRequest = spotFleetRequestConfigs.get(0);

        if (spotRequest.getSpotFleetRequestState().equals("cancelled")
                || spotRequest.getSpotFleetRequestState().equals("cancelled_terminating")) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' is already cancelled.");
            return;
        }

        boolean found = false;
        if (spotRequest.getSpotFleetRequestConfig().getLaunchSpecifications() != null) {
            for (SpotFleetLaunchSpecification launchSpecification : spotRequest.getSpotFleetRequestConfig().getLaunchSpecifications()) {
                if (disallowedSubnets.contains(launchSpecification.getSubnetId())) {
                    found = true;
                    break;
                }
            }
        }
        if (!found && spotRequest.getSpotFleetRequestConfig().getLaunchTemplateConfigs() != null) {
            for (LaunchTemplateConfig launchTemplateConfig : spotRequest.getSpotFleetRequestConfig().getLaunchTemplateConfigs()) {

                String templateId = launchTemplateConfig.getLaunchTemplateSpecification().getLaunchTemplateId();
                String templateVersion = launchTemplateConfig.getLaunchTemplateSpecification().getVersion();
                DescribeLaunchTemplateVersionsRequest describeLaunchTemplateVersionsRequest = new DescribeLaunchTemplateVersionsRequest()
                        .withLaunchTemplateId(templateId)
                        .withVersions(templateVersion);

                DescribeLaunchTemplateVersionsResult launchTemplatesResult = ec2.describeLaunchTemplateVersions(describeLaunchTemplateVersionsRequest);

                LaunchTemplateVersion launchTemplate = launchTemplatesResult.getLaunchTemplateVersions().get(0);

                for (LaunchTemplateInstanceNetworkInterfaceSpecification networkInterface : launchTemplate.getLaunchTemplateData().getNetworkInterfaces()) {
                    String subnet = networkInterface.getSubnetId();
                    if (disallowedSubnets.contains(subnet)) {
                        found = true;
                        break;
                    }
                }

                if (found) {
                    break;
                }
            }
        }

        if (!found) {
            setNoLongerRequired(detected, LOGTAG,
                    "Spot Request '" + spotRequestId + "' is not in a disallowed subnet.  Remediation not required.");
            return;
        }

        //remediate
        CancelSpotFleetRequestsRequest cancelSpotFleetRequestsRequest = new CancelSpotFleetRequestsRequest().withSpotFleetRequestIds(spotRequestId).withTerminateInstances(true);
        ec2.cancelSpotFleetRequests(cancelSpotFleetRequestsRequest);

        setSuccess(detected, LOGTAG, "Spot Request '" + spotRequestId + "' has been cancelled.");
    }
}
