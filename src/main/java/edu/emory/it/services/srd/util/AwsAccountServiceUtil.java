package edu.emory.it.services.srd.util;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.objects.resources.v1_0.AccountQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.VirtualPrivateCloudQuerySpecification;
import org.apache.logging.log4j.Logger;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.XmlEnterpriseObjectException;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

public class AwsAccountServiceUtil {
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(AwsAccountServiceUtil.class);

	private static final Object lock = new Object();
	private static final String SRD_EXEMPT_PROPERTY_NAME = "srdExempt";
	private static final String SRD_EXEMPT_BECAUSE_PROPERTY_NAME = "srdExemptBecause";
	
	private static final ConcurrentHashMap<String, Account> accountCache = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<String, AccountAliasCacheEntry> accountAliasCache = new ConcurrentHashMap<>();

	private static AwsAccountServiceUtil instance;
	private AppConfig appConfig;
	private ProducerPool awsAccountServiceProducerPool;

	public static AwsAccountServiceUtil getInstance() {
		if (instance == null) {
			synchronized (lock) {
				if (instance == null) {
					instance = new AwsAccountServiceUtil();
				}
			}
		}
		return instance;
	}

	/**
	 * Initialize the singleton.
	 *
	 * @param appConfig AppConfig
	 * @param LOGTAG    LOGTAG
	 * @throws EnterpriseConfigurationObjectException on error
	 */
	public void initialize(AppConfig appConfig, String LOGTAG) throws EnterpriseConfigurationObjectException {
		this.appConfig = appConfig;
		this.awsAccountServiceProducerPool = (ProducerPool) appConfig.getObject("AwsAccountP2pProducer");

		try {
			// prime account cache
			getAllAccounts(LOGTAG);
		} catch (Exception e) {
			logger.error(LOGTAG + "An error occurred while getting accounts from the AWS Account Producer"
					+ ". The exception is: " + e.getMessage());
		}
	}

	public List<Account> getAllAccounts(String LOGTAG) throws Exception {
		// sometimes this gets called before full initialization is done
		if (awsAccountServiceProducerPool == null) {
			return Collections.emptyList();
		}

		PointToPointProducer awsAccountProducer = null;
		try {
			awsAccountProducer = (PointToPointProducer) awsAccountServiceProducerPool.getExclusiveProducer();

			Account accountQuery = (Account) appConfig.getObject("Account");
			AccountQuerySpecification accountQuerySpecification = (AccountQuerySpecification) appConfig
					.getObject("AccountQuerySpecification");

			@SuppressWarnings("unchecked")
			List<Account> awsAccounts = accountQuery.query(accountQuerySpecification, awsAccountProducer);

			// synchronized with accountCacheIsSrdExempt() and
			// accountCacheAccountComplianceClass()
			// since we are clearing and repopulating the entire cache.
			// the cache is cleared first to handle the case where an account disappears.
			// it will not happen very frequently but accounts can, and will, be
			// de-provisioned.
			synchronized (accountCache) {
				accountCache.clear();
				for (Account act : awsAccounts) {
					accountCachePut(act, LOGTAG, "fresh while getting all accounts");
				}
			}

			return awsAccounts;
		} finally {
			if (awsAccountProducer != null)
				awsAccountServiceProducerPool.releaseProducer(awsAccountProducer);
		}
	}

	public void accountCachePut(Account account, String LOGTAG, String why) {
		accountCache.put(account.getAccountId(), account); // synchronization not required
		accountCacheLogChange(account, LOGTAG, "Caching account", why);
	}

	public void accountCacheRemove(Account account, String LOGTAG, String why) {
		accountCache.remove(account.getAccountId()); // synchronization not required
		accountCacheLogChange(account, LOGTAG, "Remove account from cache", why);
	}

	private void accountCacheLogChange(Account account, String LOGTAG, String what, String why) {
		String accountIdAnnotation;
		switch (account.getComplianceClass()) {
		case "Standard":
			accountIdAnnotation = account.getAccountId() + "~S";
			break;
		case "HIPAA":
			accountIdAnnotation = account.getAccountId() + "~H";
			break;
		case "Enhanced Security":
			accountIdAnnotation = account.getAccountId() + "~ES";
			break;
		default:
			accountIdAnnotation = account.getAccountId() + "~" + account.getComplianceClass();
			break;
		}
		StringBuilder propsAnnotation = new StringBuilder();
		@SuppressWarnings("unchecked")
		List<Property> accountProperties = account.getProperty();
		if (accountProperties != null) {
			accountProperties
					.forEach(p -> propsAnnotation.append(p.getKey()).append("=").append(p.getValue()).append(";"));
		}
		logger.info(LOGTAG + what + " " + accountIdAnnotation + " {" + propsAnnotation + "} " + why);
	}

	public boolean accountCacheIsSrdExempt(String accountId, String LOGTAG) {
		Account account;

		// synchronized with getAllAccounts() in case we are clearing and repopulating
		// the entire cache
		synchronized (accountCache) {
			account = accountCache.get(accountId);
		}

		if (account == null) {
			// could have been removed due to a Account.Delete-Sync or some other reason.
			// but since there are several mechanisms in place to cache knowledge about all
			// accounts,
			// something is odd about this account not being in the cache so indicate it is
			// exempt.
			logger.warn(LOGTAG + "Account " + accountId + " is missing from cache.");
			return true;
		}

		@SuppressWarnings("unchecked")
		List<Property> accountProperties = account.getProperty();
		if (accountProperties == null) {
			// it is very odd that there are no properties since they are integral to
			// provisioning
			// but it is not as dire as the oddity of the account missing from the cache
			logger.info(LOGTAG + "Account " + accountId + " is missing properties.");
			return false;
		}

		return accountProperties.stream().anyMatch(p -> p.getKey() != null
				&& p.getKey().equals(SRD_EXEMPT_PROPERTY_NAME) 
				&& p.getValue() != null 
				&& p.getValue().equals("true"));
	}

	public String accountCacheAccountComplianceClass(String accountId, String LOGTAG) {
		Account account;

		// synchronized with getAllAccounts() in case we are clearing and repopulating
		// the entire cache
		synchronized (accountCache) {
			account = accountCache.get(accountId);
		}

		if (account == null) {
			// could have been removed due to a Account.Delete-Sync or some other reason.
			// but since there are several mechanisms in place to cache knowledge about all
			// accounts,
			// something is odd about this account not being in the cache so indicate it is
			// exempt.
			logger.warn(LOGTAG + "Account " + accountId + " is missing from cache.");
			return "";
		}

		return account.getComplianceClass();
	}

	public List<Property> getAccountProperties(String accountId, String LOGTAG) {
		Account account;

		// synchronized with getAllAccounts() in case we are clearing and repopulating
		// the entire cache
		synchronized (accountCache) {
			account = accountCache.get(accountId);
		}

		if (account == null) {
			// could have been removed due to a Account.Delete-Sync or some other reason.
			// but since there are several mechanisms in place to cache knowledge about all
			// accounts,
			// something is odd about this account not being in the cache.
			logger.warn(LOGTAG + "Account " + accountId + " is missing from cache.");
			return Collections.emptyList();
		}

		@SuppressWarnings("unchecked")
		List<Property> property = account.getProperty();
		return property;
	}

	public boolean setAccountSrdExempt(String accountId, String exemptSetting, String LOGTAG) throws Exception {
		PointToPointProducer awsAccountProducer = null;

		try {
			awsAccountProducer = (PointToPointProducer) awsAccountServiceProducerPool.getExclusiveProducer();

			Account accountQuery = (Account) appConfig.getObject("Account");
			AccountQuerySpecification accountQuerySpecification = (AccountQuerySpecification) appConfig
					.getObject("AccountQuerySpecification");

			accountQuerySpecification.setAccountId(accountId);

			@SuppressWarnings("unchecked")
			List<Account> awsAccounts = accountQuery.query(accountQuerySpecification, awsAccountProducer);

			if (awsAccounts.size() != 1) {
				throw new Exception(LOGTAG + "Expected to retrieve 1 account for ID " + accountId + " but got "
						+ awsAccounts.size());
			}

			Account account = awsAccounts.get(0);
			accountCachePut(account, LOGTAG, "fresh while setting srdExempt");

			/*
			 * the request is to set the property to a particular value but if it already
			 * has that value then one of the expected pre-conditions is wrong. do not do
			 * the update and let the caller know by returning true.
			 *
			 * sometimes there are multiple srdExempt properties on the account so they all
			 * need to agree. since the property is used in provisioning it must exist or
			 * else something unexpected is going on.
			 */
			boolean propertyFound = false;

			@SuppressWarnings("unchecked")
			List<Property> accountProperties = account.getProperty();
			Property becauseProperty = null;
			for (Property p : accountProperties) {
				if (SRD_EXEMPT_PROPERTY_NAME.equals(p.getKey())) {
					propertyFound = true;
					if (!exemptSetting.equals(p.getValue())) {
						p.setValue(exemptSetting);
					} else {
						logger.error(LOGTAG + "Unexpected state for account " + accountId + " where the "
								+ SRD_EXEMPT_PROPERTY_NAME + " property is already set to " + exemptSetting);
						return true;
					}
				}
				if (SRD_EXEMPT_BECAUSE_PROPERTY_NAME.equals(p.getKey())) {
					p.setValue(LOGTAG + " set to " + exemptSetting + ((becauseProperty != null) ? " ++++" : ""));
					becauseProperty = p;
				}
			}

			if (!propertyFound) {
				logger.error(LOGTAG + "Unexpected state for account " + accountId + " where the "
						+ SRD_EXEMPT_PROPERTY_NAME + " property is missing");
				return true;
			}
			if (becauseProperty == null) {
				becauseProperty = account.newProperty();
				becauseProperty.setKey(SRD_EXEMPT_BECAUSE_PROPERTY_NAME);
				becauseProperty.setValue(LOGTAG + " set to " + exemptSetting);
				account.addProperty(becauseProperty);
			}

			// expected update of the property done - persist it
			account.update(awsAccountProducer);
			accountCachePut(account, LOGTAG, "updated while setting srdExempt");

			return false;
		} finally {
			if (awsAccountProducer != null)
				awsAccountServiceProducerPool.releaseProducer(awsAccountProducer);
		}
	}

	public List<VirtualPrivateCloud> getRegisteredVpcs(String accountId, String LOGTAG) throws Exception {
		PointToPointProducer awsAccountProducer = null;
		try {
			awsAccountProducer = (PointToPointProducer) awsAccountServiceProducerPool.getExclusiveProducer();

			VirtualPrivateCloud virtualPrivateCloudQuery = (VirtualPrivateCloud) appConfig
					.getObject("VirtualPrivateCloud");
			VirtualPrivateCloudQuerySpecification virtualPrivateCloudQuerySpecification = (VirtualPrivateCloudQuerySpecification) appConfig
					.getObject("VirtualPrivateCloudQuerySpecification");

			virtualPrivateCloudQuerySpecification.setAccountId(accountId);

			@SuppressWarnings("unchecked")
			List<VirtualPrivateCloud> v = virtualPrivateCloudQuery.query(virtualPrivateCloudQuerySpecification,
					awsAccountProducer);

			StringBuilder buf = new StringBuilder(256);
			buf.append("AWS Account Service reports ").append(v.size()).append(" registered VPCs for account ")
					.append(accountId);
			for (VirtualPrivateCloud regVpc : v) {
				buf.append(" (").append(regVpc.getAccountId()).append(" : ").append(regVpc.getVpcId()).append(")");
			}
			logger.info(LOGTAG + buf);

			// recently there was a case where a VPC for a different account was returned by
			// this query
			// it was caused by using a non-local VirtualPrivateCloudQuerySpecification
			// object which is now fixed
			// but since this is a inexpensive safe guard and bugs here are not nice, keep
			// this verification code
			for (VirtualPrivateCloud regVpc : v) {
				if (!regVpc.getAccountId().equals(accountId)) {
					String errMsg = "AWS Account Service reported wrong account " + regVpc.getAccountId()
							+ " vs expected account " + accountId + " with VPC ID " + regVpc.getVpcId();
					logger.error(LOGTAG + errMsg);
					throw new Exception(errMsg);
				}
			}

			return v;
		} finally {
			if (awsAccountProducer != null)
				awsAccountServiceProducerPool.releaseProducer(awsAccountProducer);
		}
	}

	public String getCachedAccountAlias(String accountId) {
		AccountAliasCacheEntry ce = accountAliasCache.get(accountId);
		if (ce == null)
			return null;
		return ce.getAccountAlias();
	}

	public boolean needCachedAccountAliasRefresh(String accountId) {
		AccountAliasCacheEntry ce = accountAliasCache.get(accountId);
		if (ce == null)
			return true;
		return ce.needsRefresh();
	}

	public void putCachedAccountAlias(String accountId, String accountAlias) {
		accountAliasCache.put(accountId, new AccountAliasCacheEntry(accountAlias));
	}

	private static class AccountAliasCacheEntry {
		private final String accountAlias;
		private final long millis;

		AccountAliasCacheEntry(String accountAlias) {
			this.accountAlias = accountAlias;
			// cache entry expires this many hours from now
			this.millis = System.currentTimeMillis() + (1 * 60 * 60 * 1000);
		}

		String getAccountAlias() {
			return accountAlias;
		}

		boolean needsRefresh() {
			return accountAlias == null || accountAlias.isEmpty() || millis < System.currentTimeMillis();
		}
	}
	
	public Account accountCacheGet(String accountId, boolean refresh) {
		Account acct = accountCacheGet(accountId);
		if( refresh == false ) {
			return acct;
		}
		// refresh all accounts then retry
		if (acct == null) {
			final String LOGTAG2 = "->>>> ";
			try {
				logger.info(LOGTAG2 + "accountID " + accountId
						+ " not found in cache, attempting to refresh cache");
				getAllAccounts(LOGTAG2 + "accountCacheGet for accountId " + accountId);
				logger.info(LOGTAG2 + "cache refreshed");
				acct = accountCacheGet(accountId);
				if (acct == null) {
					logger.error(LOGTAG2 + "accountID " + accountId
							+ " not found in cache after refresh");
					return null;
				} else {
					return acct;
				}
			} catch (Exception e) {
				logger.error(LOGTAG2 + "accountID " + accountId
						+ " not found in cache, getAllAccounts throws exception");
				logger.error(LOGTAG2 + e);
				return null;
			}
		} else {
			return acct;
		}	
	}

	public Account accountCacheGet(String accountId) {
		return accountCache.get(accountId);
	}

	HashMap<String, Pattern> arnPatterns = new HashMap<>();

	/**
	 * isExemptResourceArnForDetector will search for a property in the account
	 * whose key starts with "SRD:". This key is either an SRD (Only) or SRD:ARN
	 * exemption. If it is an SRD exemption then it will not contain the string
	 * ":arn:". Conversely, if it contains the ":arn:" string then it is an SRD:ARN
	 * exemption.
	 * 
	 * @param detectorName - the name of the detector in question
	 * @param accountId    - the key to the account whose list of properties may
	 *                     contain SRD or SRD:ARN exemptions
	 * @return a Boolean true if the detectorName + candidateArn matches an SRD/ARN
	 *         exemption in the account properties and a false otherwise
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Boolean isExemptResourceArnForAccountAndDetector(String candidateArn, String accountId,
			String detectorName) {
		String LOGTAG2 = detectorName + "->>>> ";
		Account acct = accountCacheGet(accountId, true);
		if (acct == null) {
			logger.error(LOGTAG2 + "accountID " + accountId
					+ " not found in cache after refresh, candidateArn = '" + candidateArn + "'");
			return null;
		}

		//The format of the SRD/SRN exemption is SRD:DetectorName:arn:aws:...
		for (Property prop : ((List<Property>) acct.getProperty())) {
			String key = prop.getKey();
			if ("exempt".equals(prop.getValue()) && key.startsWith("SRD:")) {
				if (key.indexOf(detectorName) == 4) {
					// Make sure this is an SRD/ARN exemption as opposed to and SRD (only) exemption
					int indexOfArn = key.indexOf(":arn:");
					if (indexOfArn - 4 == detectorName.length()) {
						String exemptArn = key.substring(indexOfArn + 1).replaceAll("\\*", ".*").replaceAll("\\?", ".");

						Pattern exemptArnPattern = arnPatterns.get(exemptArn);
						if (exemptArnPattern == null) {
							exemptArnPattern = Pattern.compile(exemptArn);
							arnPatterns.put(exemptArn, exemptArnPattern);
						}
						if (exemptArnPattern.matcher(candidateArn).matches()) {
							return true;
						}
					}
				}
			}
		}
		return false;

	}

	@SuppressWarnings("unchecked")
	public Boolean isType0(String accountId) {
		String LOGTAG2 = "[isType0] accountId: " + accountId;
		Account acct = accountCacheGet(accountId, true);
		if (acct == null) {
			logger.error(LOGTAG2 + " not found in cache after refresh");
			return null;
		}
		// vpcType=0|1|null
		String vpcType = null;
		for (Property prop : ((List<Property>)acct.getProperty())) {
			if (prop.getKey().equalsIgnoreCase("vpctype")) {
				vpcType = prop.getValue();
			}
		}
		if (vpcType == null) {
			try {
				logger.warn(LOGTAG2 + " vpcType property is null in account: " + 
					accountId + "  Cache size: " + accountCache.size() 
					+ "  Account as xml: " + acct.toXmlString());
			}
			catch (XmlEnterpriseObjectException e) {
				e.printStackTrace();
			}
			return null;
		}
		logger.info(LOGTAG2 + "Returning vpcType; " + vpcType);
		if (vpcType.equals("0")) {
			return true;
		}
		return false;
	}

}
