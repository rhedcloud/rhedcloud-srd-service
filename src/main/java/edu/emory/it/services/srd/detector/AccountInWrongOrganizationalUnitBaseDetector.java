package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;
import edu.emory.it.services.srd.ComplianceClass;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.AWSOrgUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.utils.lock.Key;

/**
 * Detect if the account is not in a particular AWS Organization.
 */
public abstract class AccountInWrongOrganizationalUnitBaseDetector extends AbstractSecurityRiskDetector {
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext, ComplianceClass complianceClass) {

        // if the OU Shuffle lock can not be acquired then we'll try again next time
        Key ouShuffleLockKey = srContext.tryAcquireOuShuffleLock(accountId);
        if (ouShuffleLockKey == null) {
            // detector name does not match a DetectionType so ensure DetectionResult is set
            setDetectionSuccess(detection, DetectionType.AccountInWrongOrganizationalUnit, srContext.LOGTAG);
            return;  // messages already logged
        }

        try {
            AWSOrganizations orgClient = getOrganizationsClient(srContext);
            AWSOrgUtil awsOrgUtil = AWSOrgUtil.getInstance();

            String parentId = awsOrgUtil.getAccountParentId(orgClient, accountId, srContext.LOGTAG);
            if (parentId == null) {
                // it's odd that we couldn't determine the parent of the account
                // an INFO message was logged about it so just try again during the next run
                // detector name does not match a DetectionType so ensure DetectionResult is set
                setDetectionSuccess(detection, DetectionType.AccountInWrongOrganizationalUnit, srContext.LOGTAG);
                return;
            }

            String expectedParentId;
            switch (complianceClass) {
                case Standard:
                    expectedParentId = awsOrgUtil.getStandardOrganizationalUnitId();
                    break;
                case HIPAA:
                    expectedParentId = awsOrgUtil.getHipaaOrganizationalUnitId();
                    break;
                case EnhancedSecurity:
                    expectedParentId = awsOrgUtil.getEnhancedSecurityOrganizationalUnitId();
                    break;
                default:
                    setDetectionError(detection, DetectionType.AccountInWrongOrganizationalUnit,
                            srContext.LOGTAG, "Unknown compliance class " + complianceClass);
                    return;
            }
            if (parentId.equals(expectedParentId)) {
                // detector name does not match a DetectionType so ensure DetectionResult is set
                setDetectionSuccess(detection, DetectionType.AccountInWrongOrganizationalUnit, srContext.LOGTAG);
                return;  // looking good
            }

            String quarantineParentId = awsOrgUtil.getQuarantineOrganizationalUnitId();
            if (parentId.equals(quarantineParentId)) {
                // detector name does not match a DetectionType so ensure DetectionResult is set
                setDetectionSuccess(detection, DetectionType.AccountInWrongOrganizationalUnit, srContext.LOGTAG);
                return;  // something caused the account to be quarantined
            }

            // the account is in the wrong organizational unit
            AWSSecurityTokenService stsClient = getSecurityTokenServiceClient(srContext);
            GetCallerIdentityResult callerIdentityResult = stsClient.getCallerIdentity(new GetCallerIdentityRequest());
            String arn = "arn:aws:organizations::" + callerIdentityResult.getAccount() + ":account/" + parentId + "/" + accountId;
            addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.AccountInWrongOrganizationalUnit, arn);
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.AccountInWrongOrganizationalUnit,
                    srContext.LOGTAG, "Error in detector", e);
        }
        finally {
            srContext.releaseOuShuffleLock(accountId, ouShuffleLockKey);
        }
    }
}
