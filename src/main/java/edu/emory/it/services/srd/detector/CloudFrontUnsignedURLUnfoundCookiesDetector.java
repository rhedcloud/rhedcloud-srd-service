package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CacheBehavior;
import com.amazonaws.services.cloudfront.model.DefaultCacheBehavior;
import com.amazonaws.services.cloudfront.model.DistributionList;
import com.amazonaws.services.cloudfront.model.DistributionSummary;
import com.amazonaws.services.cloudfront.model.ListDistributionsRequest;
import com.amazonaws.services.cloudfront.model.ListDistributionsResult;
import com.amazonaws.services.cloudfront.model.Origin;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.HashSet;
import java.util.Set;

/**
 * Check all distributions for 'TrustedSigners' disabled and 'Cookies' not set to 'none' with s3 origin<br>
 *
 * @see edu.emory.it.services.srd.remediator.CloudFrontUnsignedURLUnfoundCookiesRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CloudFrontUnsignedURLUnfoundCookiesDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.CloudFrontUnsignedURLUnfoundCookies,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        ListDistributionsRequest listDistributionsRequest = new ListDistributionsRequest();
        DistributionList distributionList;

        do {
            try {
                ListDistributionsResult listDistributionsResult = cloudFront.listDistributions(listDistributionsRequest);
                distributionList = listDistributionsResult.getDistributionList();
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CloudFrontUnsignedURLUnfoundCookies, srContext.LOGTAG,
                        "Error listing CloudFront distributions", e);
                return;
            }

            for (DistributionSummary distributionSummary : distributionList.getItems()) {
                Set<String> originIds = new HashSet<>();

                DefaultCacheBehavior defaultCacheBehavior = distributionSummary.getDefaultCacheBehavior();
                if (!Boolean.TRUE.equals(defaultCacheBehavior.getTrustedSigners().isEnabled())
                        && !"none".equals(defaultCacheBehavior.getForwardedValues().getCookies().getForward())) {
                    originIds.add(defaultCacheBehavior.getTargetOriginId());
                }

                for (CacheBehavior cacheBehavior : distributionSummary.getCacheBehaviors().getItems()) {
                    if (!Boolean.TRUE.equals(cacheBehavior.getTrustedSigners().isEnabled())
                            && !"none".equals(cacheBehavior.getForwardedValues().getCookies().getForward())) {
                        originIds.add(cacheBehavior.getTargetOriginId());
                    }
                }

                for (Origin origin : distributionSummary.getOrigins().getItems()) {
                    if (originIds.contains(origin.getId()) && origin.getS3OriginConfig() != null) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.CloudFrontUnsignedURLUnfoundCookies, distributionSummary.getARN());
                        break;
                    }
                }
            }

            if (distributionList.getIsTruncated())
                listDistributionsRequest.setMarker(distributionList.getNextMarker());
        } while (distributionList.getIsTruncated());
    }

    @Override
    public String getBaseName() {
        return "CloudFrontUnsignedURLUnfoundCookies";
    }
}
