package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CustomOriginConfig;
import com.amazonaws.services.cloudfront.model.DistributionList;
import com.amazonaws.services.cloudfront.model.DistributionSummary;
import com.amazonaws.services.cloudfront.model.ListDistributionsRequest;
import com.amazonaws.services.cloudfront.model.ListDistributionsResult;
import com.amazonaws.services.cloudfront.model.Origin;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects CloudFront distributions where OriginProtocolPolicy is not equal to "https-only"<br>
 *
 * @see edu.emory.it.services.srd.remediator.CloudFrontNonHTTPSOriginEnabledRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CloudFrontNonHTTPSOriginEnabledDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.CloudFrontNonHTTPSOriginEnabled,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        ListDistributionsRequest listDistributionsRequest = new ListDistributionsRequest();
        DistributionList distributionList;

        do {
            try {
                ListDistributionsResult listDistributionsResult = cloudFront.listDistributions(listDistributionsRequest);
                distributionList = listDistributionsResult.getDistributionList();
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CloudFrontNonHTTPSOriginEnabled, srContext.LOGTAG,
                        "Error listing CloudFront distributions", e);
                return;
            }

            for (DistributionSummary distributionSummary : distributionList.getItems()) {
                for (Origin origin : distributionSummary.getOrigins().getItems()) {
                    CustomOriginConfig config = origin.getCustomOriginConfig();
                    if (config != null) {  //s3 buckets have config == null
                        if (!config.getOriginProtocolPolicy().equals("https-only")) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.CloudFrontNonHTTPSOriginEnabled, distributionSummary.getARN());
                        }
                    }
                }
            }

            if (distributionList.getIsTruncated())
                listDistributionsRequest.setMarker(distributionList.getNextMarker());
        } while (distributionList.getIsTruncated());
    }

    @Override
    public String getBaseName() {
        return "CloudFrontNonHTTPSOriginEnabled";
    }
}
