package edu.emory.it.services.srd.util;

import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.organizations.model.Account;
import com.amazonaws.services.organizations.model.DescribeAccountRequest;
import com.amazonaws.services.organizations.model.DescribeAccountResult;
import com.amazonaws.services.organizations.model.ListAccountsForParentRequest;
import com.amazonaws.services.organizations.model.ListAccountsForParentResult;
import com.amazonaws.services.organizations.model.ListOrganizationalUnitsForParentRequest;
import com.amazonaws.services.organizations.model.ListOrganizationalUnitsForParentResult;
import com.amazonaws.services.organizations.model.ListParentsRequest;
import com.amazonaws.services.organizations.model.ListParentsResult;
import com.amazonaws.services.organizations.model.ListRootsRequest;
import com.amazonaws.services.organizations.model.ListRootsResult;
import com.amazonaws.services.organizations.model.MoveAccountRequest;
import com.amazonaws.services.organizations.model.OrganizationalUnit;
import com.amazonaws.services.organizations.model.Parent;
import com.amazonaws.services.organizations.model.Root;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import java.util.ArrayList;
import java.util.List;

public class AWSOrgUtil {
    private static final Logger logger = OpenEaiObject.logger;

    private static final Object lock = new Object();

    private static AWSOrgUtil instance;
    private static OrganizationalUnit hipaaOrg;
    private static OrganizationalUnit standardOrg;
    private static OrganizationalUnit enhancedSecurityOrg;
    private static OrganizationalUnit quarantineOrg;
    private static OrganizationalUnit pendingDeleteOrg;
    private static OrganizationalUnit administrationOrg;

    public static AWSOrgUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new AWSOrgUtil();
                }
            }
        }
        return instance;
    }

    /**
     * Initialize the singleton.
     *
     * @param orgClient client connection
     * @param hipaaOrgName OU name
     * @param standardOrgName OU name
     * @param enhancedSecurityOrgName OU name (optional)
     * @param quarantineOrgName OU name
     * @param pendingDeleteOrgName OU name
     * @param administrationOrgName OU name
     * @throws InstantiationException on error
     */
    public void initialize(AWSOrganizations orgClient,
                           String hipaaOrgName, String standardOrgName, String enhancedSecurityOrgName,
                           String quarantineOrgName, String pendingDeleteOrgName,
                           String administrationOrgName)
            throws InstantiationException {

        List<OrganizationalUnit> organizationalUnits = new ArrayList<>();
        ListRootsRequest listRootsRequest = new ListRootsRequest();
        ListRootsResult listRootsResult;
        do {
            listRootsResult = orgClient.listRoots(listRootsRequest);

            for (Root root : listRootsResult.getRoots()) {
                getOrganizationalUnits(orgClient, organizationalUnits, root.getId());
            }

            listRootsRequest.setNextToken(listRootsResult.getNextToken());
        } while (listRootsResult.getNextToken() != null);

        for (OrganizationalUnit ou : organizationalUnits) {
            if (hipaaOrgName.equals(ou.getName())) {
                hipaaOrg = ou;
            }
            else if (standardOrgName.equals(ou.getName())) {
                standardOrg = ou;
            }
            else if (enhancedSecurityOrgName != null && enhancedSecurityOrgName.equals(ou.getName())) {
                enhancedSecurityOrg = ou;
            }
            else if (quarantineOrgName.equals(ou.getName())) {
                quarantineOrg = ou;
            }
            else if (pendingDeleteOrgName.equals(ou.getName())) {
                pendingDeleteOrg = ou;
            }
            else if (administrationOrgName.equals(ou.getName())) {
                administrationOrg = ou;
            }
        }

        if (hipaaOrg == null) {
            throw new InstantiationException("Missing OU: " + hipaaOrgName);
        }
        if (standardOrg == null) {
            throw new InstantiationException("Missing OU: " + standardOrgName);
        }
        if (enhancedSecurityOrgName != null && enhancedSecurityOrg == null) {
            throw new InstantiationException("Missing OU: " + enhancedSecurityOrgName);
        }
        if (quarantineOrg == null) {
            throw new InstantiationException("Missing OU: " + quarantineOrgName);
        }
        if (pendingDeleteOrg == null) {
            throw new InstantiationException("Missing OU: " + pendingDeleteOrgName);
        }
        if (administrationOrg == null) {
            throw new InstantiationException("Missing OU: " + administrationOrgName);
        }
    }

    private void getOrganizationalUnits(AWSOrganizations orgClient, List<OrganizationalUnit> organizationalUnits, String parentId) {
        ListOrganizationalUnitsForParentRequest listOrganizationalUnitsForParentRequest
                = new ListOrganizationalUnitsForParentRequest().withParentId(parentId);
        ListOrganizationalUnitsForParentResult listOrganizationalUnitsForParentResult;
        do {
            listOrganizationalUnitsForParentResult = orgClient.listOrganizationalUnitsForParent(listOrganizationalUnitsForParentRequest);

            for (OrganizationalUnit ou : listOrganizationalUnitsForParentResult.getOrganizationalUnits()) {
                organizationalUnits.add(ou);
                getOrganizationalUnits(orgClient, organizationalUnits, ou.getId());
            }

            listOrganizationalUnitsForParentRequest.setNextToken(listOrganizationalUnitsForParentResult.getNextToken());
        } while (listOrganizationalUnitsForParentResult.getNextToken() != null);
    }

    public Object findAccountParent(AWSOrganizations orgClient, String accountId, String LOGTAG) {
        for (int retry = 0; retry < 5; ++retry) {
            String reason;
            try {
                Object parent = findAccountParent0(orgClient, accountId);
                if (parent != null)
                    return parent;
                reason = ". Not in an OU or a Root";
            } catch (Exception e) {
                reason = ". The exception is: " + e.getMessage();
            }
            logger.info(LOGTAG + "Could not find parent for account " + accountId + " on retry " + retry + reason);
        }
        return null;
    }

    private Object findAccountParent0(AWSOrganizations orgClient, String accountId) {
        ListRootsRequest listRootsRequest = new ListRootsRequest();
        ListRootsResult listRootsResult;
        do {
            listRootsResult = orgClient.listRoots(listRootsRequest);

            for (Root root : listRootsResult.getRoots()) {
                ListOrganizationalUnitsForParentRequest listOrganizationalUnitsForParentRequest
                        = new ListOrganizationalUnitsForParentRequest().withParentId(root.getId());
                ListOrganizationalUnitsForParentResult listOrganizationalUnitsForParentResult;
                do {
                    listOrganizationalUnitsForParentResult = orgClient.listOrganizationalUnitsForParent(listOrganizationalUnitsForParentRequest);

                    for (OrganizationalUnit ou : listOrganizationalUnitsForParentResult.getOrganizationalUnits()) {
                        ListAccountsForParentRequest listAccountsForParentRequest = new ListAccountsForParentRequest().withParentId(ou.getId());
                        ListAccountsForParentResult listAccountsForParentResult;
                        do {
                            listAccountsForParentResult = orgClient.listAccountsForParent(listAccountsForParentRequest);
                            for (Account account : listAccountsForParentResult.getAccounts()) {
                                if (account.getId().equals(accountId)) {
                                    return ou;
                                }
                            }
                            listAccountsForParentRequest.setNextToken(listAccountsForParentResult.getNextToken());
                        } while (listAccountsForParentResult.getNextToken() != null);
                    }

                    listOrganizationalUnitsForParentRequest.setNextToken(listOrganizationalUnitsForParentResult.getNextToken());
                } while (listOrganizationalUnitsForParentResult.getNextToken() != null);

                // not found in any of the OUs - maybe the account is hanging directly off the root
                ListAccountsForParentRequest listAccountsForParentRequest = new ListAccountsForParentRequest().withParentId(root.getId());
                ListAccountsForParentResult listAccountsForParentResult;
                do {
                    listAccountsForParentResult = orgClient.listAccountsForParent(listAccountsForParentRequest);
                    for (Account account : listAccountsForParentResult.getAccounts()) {
                        if (account.getId().equals(accountId)) {
                            return root;
                        }
                    }
                    listAccountsForParentRequest.setNextToken(listAccountsForParentResult.getNextToken());
                } while (listAccountsForParentResult.getNextToken() != null);
            }

            listRootsRequest.setNextToken(listRootsResult.getNextToken());
        } while (listRootsResult.getNextToken() != null);

        // usually this doesn't happen because and account
        // is either in an OU or it's in the root but sometimes it does
        // and it is the callers responsibility to check
        return null;
    }

    public String getAccountParentId(AWSOrganizations orgClient, String accountId, String LOGTAG) {
        // don't strictly need a retry mechanism because AWS SDK provides one
        // but in testing it increases reliability of determining the parent
        for (int retry = 0; retry < 5; ++retry) {
            String reason;
            try {
                ListParentsRequest listParentsRequest = new ListParentsRequest().withChildId(accountId);
                ListParentsResult listParentsResult;
                do {
                    listParentsResult = orgClient.listParents(listParentsRequest);
                    for (Parent parent : listParentsResult.getParents()) {
                        return parent.getId();
                    }
                    listParentsRequest.setNextToken(listParentsResult.getNextToken());
                } while (listParentsResult.getNextToken() != null);

                reason = ". Unknown parent";
            } catch (Exception e) {
                reason = ". The exception is: " + e.getMessage();
            }
            logger.info(LOGTAG + "Could not get parent for account " + accountId + " on retry " + retry + reason);
        }

        return null;
    }

    public void moveAccountToAdministrationOrg(AWSOrganizations orgClient, String accountId, String sourceParentId) {
        MoveAccountRequest moveAccountRequest = new MoveAccountRequest()
                .withAccountId(accountId)
                .withSourceParentId(sourceParentId)
                .withDestinationParentId(administrationOrg.getId());

        orgClient.moveAccount(moveAccountRequest);
    }

    public void moveAccountFromAdministrationOrg(AWSOrganizations orgClient, String accountId, String destinationParentId) {
        MoveAccountRequest moveAccountRequest = new MoveAccountRequest()
                .withAccountId(accountId)
                .withSourceParentId(administrationOrg.getId())
                .withDestinationParentId(destinationParentId);

        orgClient.moveAccount(moveAccountRequest);
    }

    public void moveAccountToQuarantineOrg(AWSOrganizations orgClient, String accountId, String sourceParentId) {
        MoveAccountRequest moveAccountRequest = new MoveAccountRequest()
                .withAccountId(accountId)
                .withSourceParentId(sourceParentId)
                .withDestinationParentId(quarantineOrg.getId());

        orgClient.moveAccount(moveAccountRequest);
    }

    /**
     * Get the email address for the account.
     *
     * @param orgClient client connection
     * @param accountId member account id
     * @return member email address
     */
    public String getAccountEmailAddress(AWSOrganizations orgClient, String accountId) {
        DescribeAccountRequest describeAccountRequest = new DescribeAccountRequest()
                .withAccountId(accountId);
        DescribeAccountResult describeAccountResult = orgClient.describeAccount(describeAccountRequest);
        return describeAccountResult.getAccount().getEmail();
    }

    /**
     * Get the ID of the HIPAA Organizational Unit.
     * @return org id
     */
    public String getHipaaOrganizationalUnitId() { return hipaaOrg.getId(); }
    /**
     * Get the name of the HIPAA Organizational Unit.
     * @return org name
     */
    public String getHipaaOrganizationalUnitName() { return hipaaOrg.getName(); }

    /**
     * Get the ID of the Standard Organizational Unit.
     * @return org id
     */
    public String getStandardOrganizationalUnitId() { return standardOrg.getId(); }
    /**
     * Get the name of the Standard Organizational Unit.
     * @return org name
     */
    public String getStandardOrganizationalUnitName() { return standardOrg.getName(); }

    /**
     * Get the ID of the EnhancedSecurity Organizational Unit.
     * @return org id
     */
    public String getEnhancedSecurityOrganizationalUnitId() { return enhancedSecurityOrg.getId(); }
    /**
     * Get the name of the EnhancedSecurity Organizational Unit.
     * @return org name
     */
    public String getEnhancedSecurityOrganizationalUnitName() { return enhancedSecurityOrg.getName(); }

    /**
     * Get the ID of the Quarantine Organizational Unit.
     * @return org id
     */
    public String getQuarantineOrganizationalUnitId() { return quarantineOrg.getId(); }
    /**
     * Get the name of the Quarantine Organizational Unit.
     * @return org name
     */
    public String getQuarantineOrganizationalUnitName() { return quarantineOrg.getName(); }

    /**
     * Get the ID of the PendingDelete Organizational Unit.
     * @return org id
     */
    public String getPendingDeleteOrganizationalUnitId() { return pendingDeleteOrg.getId(); }
    /**
     * Get the name of the PendingDelete Organizational Unit.
     * @return org name
     */
    public String getPendingDeleteOrganizationalUnitName() { return pendingDeleteOrg.getName(); }

    /**
     * Get the ID of the Administration Organizational Unit.
     * @return org id
     */
    public String getAdministrationOrganizationalUnitId() { return administrationOrg.getId(); }
    /**
     * Get the name of the Administration Organizational Unit.
     * @return org name
     */
    public String getAdministrationOrganizationalUnitName() { return administrationOrg.getName(); }
}
