package edu.emory.it.services.srd.provider;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Properties;

/**
 * Security Risk Detection to Security Information and Event Management (SIEM) Integration.
 */
public interface SiemIntegrationProvider {
    /**
     * Initialize the provider.
     * @param properties additional configuration for the provider
     * @throws InstantiationException on error
     */
    void init(Properties properties) throws InstantiationException;

    /**
     * Send JSON message to SIEM system.
     * @param siemJson data
     * @throws ProviderException on error
     */
    void sendSiem(ObjectNode siemJson) throws ProviderException;
}
