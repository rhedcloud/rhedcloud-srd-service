package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;

/**
 * Simulates the remediation of tag policy violations and missing required tags.<br>
 * @see edu.emory.it.services.srd.detector.TagPolicyViolationDetector the detector
 */
public class TagPolicyViolationSimulationRemediator extends TagPolicyViolationRemediator  {
    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediate(accountId, detected, srContext);
    }

    @Override
    boolean isSimulation() {
        return true;
    }
}
