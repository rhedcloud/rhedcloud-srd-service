package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.elasticache.model.CacheCluster;
import com.amazonaws.services.elasticache.model.ReplicationGroup;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.function.Predicate;

/**
 * Detect ElastiCache for Redis clusters that have not enabled authentication tokens.<br>
 * See <a href="https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/auth.html">Authenticating Users with Redis AUTH</a>.<br>
 *
 * Redis clusters can be found in several different configurations and are all scanned for this risk.
 * This includes Redis clusters with no replication, Redis clusters with replication (cluster mode disabled),
 * Redis clusters with replication (cluster mode enabled), and Redis Global Datastores.
 *
 * @see edu.emory.it.services.srd.remediator.ElasticacheRedisDisabledRedisCommandsRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ElasticacheRedisDisabledRedisCommandsDetector extends ElastiCacheBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, DetectionType.ElasticacheRedisDisabledRedisCommands);
    }

    @Override
    protected Predicate<CacheCluster> getCacheClusterPredicate() {
        return cluster -> (cluster.getEngine().equals("redis")
                              && (!cluster.isTransitEncryptionEnabled() || !cluster.isAuthTokenEnabled()));
    }

    @Override
    protected Predicate<ReplicationGroup> getReplicationGroupPredicate() {
        // Memcached will never have replication groups so no need to check engine
        return replicationGroup -> (!replicationGroup.isTransitEncryptionEnabled() || !replicationGroup.isAuthTokenEnabled());
    }

    @Override
    public String getBaseName() {
        return "ElasticacheRedisDisabledRedisCommands";
    }
}
