package edu.emory.it.services.srd.util;

import com.amazonaws.Request;
import com.amazonaws.Response;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.metrics.MetricType;
import com.amazonaws.metrics.RequestMetricCollector;
import com.amazonaws.metrics.RequestMetricType;
import com.amazonaws.metrics.internal.cloudwatch.PredefinedMetricTransformer;
import com.amazonaws.metrics.internal.cloudwatch.spi.Dimensions;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricDatum;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.amazonaws.util.AWSRequestMetrics;
import com.amazonaws.util.TimingInfo;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static com.amazonaws.metrics.internal.cloudwatch.spi.RequestMetricTransformer.Utils.endTimestamp;
import static com.amazonaws.util.AWSRequestMetrics.Field.ClientExecuteTime;
import static com.amazonaws.util.AWSRequestMetrics.Field.ThrottleException;

public class SrdMetricCollector extends RequestMetricCollector {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SrdMetricCollector.class);

    private final PredefinedMetricTransformer transformer = new PredefinedMetricTransformer();
    private final BlockingQueue<MetricDatum> queue = new LinkedBlockingQueue<>();
    private final String LOGTAG;

    SrdMetricCollector(String LOGTAG) {
        this.LOGTAG = LOGTAG;
    }

    /**
     * Collects the metrics at the end of a request/response cycle, transforms
     * the metric data points into a CloudWatch metric datum representation,
     * and then adds it to a memory queue so it will get logged and summarized
     * by the SrdLogAnalysis tool.
     * Loosely based on com.amazonaws.metrics.internal.cloudwatch.RequestMetricCollectorSupport
     */
    @Override
    public void collectMetrics(Request<?> request, Response<?> response) {
        try {
            AWSRequestMetrics arm = request.getAWSRequestMetrics();
            if (arm == null || !arm.isEnabled()) {
                return;
            }
            for (MetricType type: AwsSdkMetrics.getPredefinedMetrics()) {
                if (!(type instanceof RequestMetricType))
                    continue;
                for (MetricDatum datum : transformer.toMetricData(type, request, response)) {
                    try {
                        if (!queue.offer(datum)) {
                            logger.debug(LOGTAG + "Failed to add to the metrics queue (due to no space available) for "
                                    + type.name() + ":" + request.getServiceName());
                        }
                    } catch (RuntimeException ex) {
                        logger.warn(LOGTAG + "Failed to add to the metrics queue for "
                                + type.name() + ":" + request.getServiceName(), ex);
                    }
                }
            }
        } catch (Exception ex) { // defensive code
            logger.debug(LOGTAG + "Ignoring unexpected failure", ex);
        }
    }

    public void collectMetrics(AWSRequestMetrics arm) {
        try {
            if (arm == null || !arm.isEnabled()) {
                return;
            }
            for (MetricType type : SrdMetricTypeField.values()) {
                for (MetricDatum datum : toMetricData(arm, type)) {
                    try {
                        if (!queue.offer(datum)) {
                            logger.debug(LOGTAG + "Failed to add to the metrics queue (due to no space available) for "
                                    + type.name());
                        }
                    } catch (RuntimeException ex) {
                        logger.warn(LOGTAG + "Failed to add to the metrics queue for "
                                + type.name(), ex);
                    }
                }
            }
        } catch (Exception ex) { // defensive code
            logger.debug(LOGTAG + "Ignoring unexpected failure", ex);
        }
    }

    private List<MetricDatum> toMetricData(AWSRequestMetrics m, MetricType metricType) {
        if (metricType instanceof SrdMetricTypeField) {
            switch ((SrdMetricTypeField) metricType) {
                case SRD_ThreadDelay:
                case SRD_DetectorExecution:
                case SRD_Detect:
                case SRD_Remediate:
                case SRD_DetectorSetup:
                case SRD_DetectorFinalizing:
                case SRD_SequenceGeneration:
                case SRD_BuildResults:
                case SRD_DetectorLockSet:
                case SRD_DetectorLockRelease:
                case SRD_DetectorOuShuffleLockSet:
                case SRD_DetectorOuShuffleLockRelease:
                case SRD_PublishSync:
                    return latencyMetricOf(m, metricType);

                case SRD_DetectedIssueCount:
                case SRD_DetectorCommandException:
                case SRD_DetectorLockSetWaitingCount:
                case SRD_DetectorLockReleaseWaitingCount:
                case SRD_SequenceGenerationWaitingCount:
                case SRD_PublishSyncWaitingCount:
                    return metricOfCount(m, metricType);
            }
        }
        return Collections.emptyList();
    }

    // see com.amazonaws.metrics.internal.cloudwatch.PredefinedMetricTransformer
    private List<MetricDatum> latencyMetricOf(AWSRequestMetrics m, MetricType metricType) {
        TimingInfo root = m.getTimingInfo();
        final String metricName = metricType.name();
        List<TimingInfo> subMeasures = root.getAllSubMeasurements(metricName);
        if (subMeasures != null) {
            List<MetricDatum> result = new ArrayList<>(subMeasures.size());
            for (TimingInfo sub : subMeasures) {
                if (sub.isEndTimeKnown()) { // being defensive
                    List<Dimension> dims = new ArrayList<>();
                    dims.add(new Dimension()
                            .withName(Dimensions.MetricType.name())
                            .withValue(metricName));
                    MetricDatum datum = new MetricDatum()
                            .withDimensions(dims)
                            .withUnit(StandardUnit.Milliseconds)
                            .withValue(sub.getTimeTakenMillisIfKnown());
                    result.add(datum);
                }
            }
            return result;
        }
        return Collections.emptyList();
    }
    // see com.amazonaws.metrics.internal.cloudwatch.PredefinedMetricTransformer
    private List<MetricDatum> metricOfCount(AWSRequestMetrics m, MetricType metricType) {
        TimingInfo ti = m.getTimingInfo();
        Number counter = ti.getCounter(metricType.name());
        if (counter == null) {
            return Collections.emptyList();
        }
        return Collections.singletonList(new MetricDatum()
                .withDimensions(new Dimension()
                        .withName(Dimensions.MetricType.name())
                        .withValue(metricType.name()))
                .withUnit(StandardUnit.Count)
                .withValue(counter.doubleValue())
                .withTimestamp(endTimestamp(ti)));
    }

    public void logMetricDatum(String accountId, String detectorName) {
        SrdMetricDetectorMeasure detectorAnalysis = new SrdMetricDetectorMeasure(accountId, detectorName);

        for (MetricDatum metricDatum : queue) {
            //logger.info(LOGTAG + "MetricDatum raw " + metricDatum);

            // ClientExecuteTime will have a RequestType
            // ThrottleException sometimes has one too
            String requestType = null;
            for (Dimension dim : metricDatum.getDimensions()) {
                if (dim.getName().equals(Dimensions.RequestType.name())) {
                    requestType = dim.getValue();
                    break;
                }
            }
            for (Dimension dim : metricDatum.getDimensions()) {
                if (dim.getName().equals(Dimensions.MetricType.name())) {
                    if (dim.getValue().equals(ClientExecuteTime.name())) {
                        // MetricDatum AWSSecurityTokenService::AssumeRoleRequest ClientExecuteTime 1373.479 Milliseconds
                        logger.info(LOGTAG + "MetricDatum " + metricDatum.getMetricName() + "::" + requestType
                                + " " + dim.getValue() + " " + BigDecimal.valueOf(metricDatum.getValue()).toPlainString() + " " + metricDatum.getUnit());
                        detectorAnalysis.addMeasure(metricDatum.getMetricName() + "::" + requestType, metricDatum.getValue(), metricDatum.getUnit(), dim.getValue());
                    }
                    else if (dim.getValue().equals(ThrottleException.name())) {
                        // MetricDatum AmazonIdentityManagement::GetAccountAuthorizationDetailsRequest ThrottleException 1.0 Count
                        logger.info(LOGTAG + "MetricDatum " + metricDatum.getMetricName() + "::" + requestType
                                + " " + dim.getValue() + " " + BigDecimal.valueOf(metricDatum.getValue()).toPlainString() + " " + metricDatum.getUnit());
                        detectorAnalysis.addMeasure(metricDatum.getMetricName() + "::" + requestType, metricDatum.getValue(), metricDatum.getUnit(), dim.getValue());
                    }
                    else if (Arrays.stream(SrdMetricTypeField.values()).anyMatch(metricType -> metricType.name().equals(dim.getValue()))) {
                        // MetricDatum SRD_BuildResults 0.887 Milliseconds
                        logger.info(LOGTAG + "MetricDatum"
                                + " " + dim.getValue() + " " + BigDecimal.valueOf(metricDatum.getValue()).toPlainString() + " " + metricDatum.getUnit());
                        detectorAnalysis.addMeasure("", metricDatum.getValue(), metricDatum.getUnit(), dim.getValue());
                    }
                }
            }
        }

        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();
        srdMetricUtil.cacheDetectorAnalysis(detectorAnalysis);
    }

    public void clearMetricDatum() {
        queue.clear();
    }
}
