package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.SetBucketAclRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by removing access to the bucket, remove all content and then delete the bucket.<br>
 * The detector runs so frequently, the customer shouldn't have much data in the bucket.
 *
 * <p>Security: Remove access, empty, and delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.S3BucketsResideOutsideOfUSRegionsDetector the detector
 */
public class S3BucketsResideOutsideOfUSRegionsRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:s3:::", srContext.LOGTAG, DetectionType.S3BucketsResideOutsideOfUSRegions))
            return;
        // like arn:aws:s3:::bucket-name
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        // TODO - Should have a whitelist functionality in case of any exceptions or Emory resources.
        String bucketName = arn[5];
        String location = null;

        try {
            AmazonS3 s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);

            location = s3.getBucketLocation(bucketName);
            remediateBucket(s3, bucketName);

            setSuccess(detected, srContext.LOGTAG, "S3 bucket '" + bucketName + "' in location '" + location + "' was deleted");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to remediate S3 bucket '" + bucketName + "' in location '" + location + "'", e);
        }
    }

    private void remediateBucket(AmazonS3 s3, String bucketName) {
        // remove all access to the bucket
        AccessControlList acl = s3.getBucketAcl(bucketName);
        acl.revokeAllPermissions(GroupGrantee.AllUsers);
        acl.revokeAllPermissions(GroupGrantee.AuthenticatedUsers);
        SetBucketAclRequest setBucketAclRequest = new SetBucketAclRequest(bucketName, acl);
        s3.setBucketAcl(setBucketAclRequest);

        // delete the bucket contents
        ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request().withBucketName(bucketName);
        ListObjectsV2Result listObjectsV2Result;
        do {
            listObjectsV2Result = s3.listObjectsV2(listObjectsV2Request);

            for (S3ObjectSummary summary : listObjectsV2Result.getObjectSummaries()) {
                s3.deleteObject(bucketName, summary.getKey());
            }

            listObjectsV2Request.setContinuationToken(listObjectsV2Result.getNextContinuationToken());
        } while (listObjectsV2Result.isTruncated());

        // delete the bucket
        s3.deleteBucket(bucketName);
    }
}
