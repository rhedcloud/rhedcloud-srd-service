package edu.emory.it.services.srd.util;

import com.amazonaws.services.mediastoredata.AWSMediaStoreData;
import com.amazonaws.services.mediastoredata.model.Item;
import com.amazonaws.services.mediastoredata.model.ListItemsRequest;
import com.amazonaws.services.mediastoredata.model.ListItemsResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MediaStoreUtil {
    private AWSMediaStoreData msdClient;

    public MediaStoreUtil(AWSMediaStoreData msdClient) {
        this.msdClient = msdClient;
    }

    /**
     * Get the objects stored in a container at every folder level.
     * @return List of container object paths
     */
    public List<String> getContainerObjects() {
        List<String> containerObjects = new ArrayList<>();

        String listItemsRequestToken = null;
        Stack<String> paths = new Stack<>();

        do {
            String path = null;

            ListItemsRequest listItemsRequest = new ListItemsRequest()
                    .withMaxResults(1000);

            // deal with pagination
            // when we're not processing a follow-on page, we look at the stack of sub-folders
            if (listItemsRequestToken != null) {
                listItemsRequest.setNextToken(listItemsRequestToken);
            } else if (!paths.empty()) {
                path = paths.pop();
                listItemsRequest.setPath(path);
            }

            ListItemsResult listItemsResult = msdClient.listItems(listItemsRequest);
            listItemsRequestToken = listItemsResult.getNextToken();

            for (Item item : listItemsResult.getItems()) {
                // form a fully qualified path of the object
                String fqPath = (path == null) ? item.getName() : (path + "/" + item.getName());

                if (item.getType().equals("FOLDER")) {
                    paths.push(fqPath);
                } else {
                    containerObjects.add(fqPath);
                }
            }
        } while (listItemsRequestToken != null || !paths.empty());

        return containerObjects;
    }
}
