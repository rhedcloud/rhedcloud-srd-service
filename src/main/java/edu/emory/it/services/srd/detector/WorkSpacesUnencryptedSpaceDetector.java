package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.workspaces.AmazonWorkspaces;
import com.amazonaws.services.workspaces.AmazonWorkspacesClient;
import com.amazonaws.services.workspaces.model.DescribeWorkspaceDirectoriesRequest;
import com.amazonaws.services.workspaces.model.DescribeWorkspaceDirectoriesResult;
import com.amazonaws.services.workspaces.model.DescribeWorkspacesRequest;
import com.amazonaws.services.workspaces.model.DescribeWorkspacesResult;
import com.amazonaws.services.workspaces.model.Workspace;
import com.amazonaws.services.workspaces.model.WorkspaceDirectory;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Check all WorkSpaces without an encrypted volume<br>
 *
 * @see edu.emory.it.services.srd.remediator.WorkSpacesUnencryptedSpaceRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class WorkSpacesUnencryptedSpaceDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonWorkspaces workspaces;
            try {
                workspaces = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonWorkspacesClient.class);

                String arnPrefix = "arn:aws:workspaces:" + region.getName() + ":" + accountId + ":workspace/";
                checkWorkspaces(workspaces, detection, arnPrefix, srContext.LOGTAG);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.WorkSpacesUnencryptedSpace,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    private void checkWorkspaces(AmazonWorkspaces workspaces, SecurityRiskDetection detection,
                                 String arnPrefix, String LOGTAG) {
        DescribeWorkspaceDirectoriesRequest describeWorkspaceDirectoriesRequest = new DescribeWorkspaceDirectoriesRequest();
        boolean done1 = false;

        try {
            while (!done1) {
                DescribeWorkspaceDirectoriesResult workspaceDirectoriesResult = workspaces.describeWorkspaceDirectories(describeWorkspaceDirectoriesRequest);

                for (WorkspaceDirectory directory : workspaceDirectoriesResult.getDirectories()) {

                    DescribeWorkspacesRequest request = new DescribeWorkspacesRequest().withDirectoryId(directory.getDirectoryId());
                    boolean done2 = false;
                    while (!done2) {
                        DescribeWorkspacesResult result = workspaces.describeWorkspaces(request);

                        for (Workspace item : result.getWorkspaces()) {
                            if (!item.getState().equals("TERMINATING") && !item.getState().equals("TERMINATED")) {
                                if (!Boolean.TRUE.equals(item.getRootVolumeEncryptionEnabled())
                                        || !Boolean.TRUE.equals(item.getUserVolumeEncryptionEnabled())) {
                                    addDetectedSecurityRisk(detection, LOGTAG,
                                            DetectionType.WorkSpacesUnencryptedSpace, arnPrefix + item.getWorkspaceId());
                                }
                            }
                        }

                        request.setNextToken(result.getNextToken());

                        if (result.getNextToken() == null) {
                            done2 = true;
                        }
                    }

                }

                describeWorkspaceDirectoriesRequest.setNextToken(workspaceDirectoriesResult.getNextToken());

                if (describeWorkspaceDirectoriesRequest.getNextToken() == null) {
                    done1 = true;
                }
            }

        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.WorkSpacesUnencryptedSpace,
                    LOGTAG, "Error describing Workspace directories", e);
        }
    }

    @Override
    public String getBaseName() {
        return "WorkSpacesUnencryptedSpace";
    }
}
