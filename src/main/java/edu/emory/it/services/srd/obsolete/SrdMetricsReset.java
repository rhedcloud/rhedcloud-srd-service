package edu.emory.it.services.srd.obsolete;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Delete all runtime metrics for SRD.
 * Very slow but use with caution!!!!
 */
public class SrdMetricsReset {
    private AmazonDynamoDB storageClient;
    private String storageTableName;

    public static void main(String[] args) {
        if (true) throw new RuntimeException("you sure you want to do this");

        SrdMetricsReset reset = new SrdMetricsReset();
        reset.run(args);
    }

    private void run(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: " + getClass().getSimpleName() + " tableName");
            System.exit(1);
        }

        storageClient = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withRegion("us-east-1")
                .build();
        storageTableName = args[0];


        ScanRequest scanRequest = new ScanRequest()
                .withTableName(storageTableName)
                .withFilterExpression("attribute_exists(measure)");
        ScanResult scanResult;

        Map<String, AttributeValue> key = new HashMap<>();
        AttributeValue detectorNameKeyValue = new AttributeValue("");
        AttributeValue propertyKeyValue = new AttributeValue("");
        key.put("detectorName", detectorNameKeyValue);
        key.put("property", propertyKeyValue);
        DeleteItemRequest deleteItemRequest = new DeleteItemRequest()
                .withTableName(storageTableName)
                .withKey(key);

        System.out.print("Deleting metrics ");

        do {
            scanResult = storageClient.scan(scanRequest);

            for (Map<String, AttributeValue> measure : scanResult.getItems()) {
                detectorNameKeyValue.setS(measure.get("detectorName").getS());
                propertyKeyValue.setS(measure.get("property").getS());

                storageClient.deleteItem(deleteItemRequest);
            }

            System.out.print(".");
            scanRequest.setExclusiveStartKey(scanResult.getLastEvaluatedKey());
        } while (scanResult.getLastEvaluatedKey() != null);
    }
}
