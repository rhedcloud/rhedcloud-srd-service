package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsResult;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Detects CloudWatch alarms that are created in Emory managed instances and does not have name start with RHEDcloud
 *
 * @see edu.emory.it.services.srd.remediator.CWEventsCustomerAlarmsOnEmoryInstancesRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CWEventsCustomerAlarmsOnEmoryInstancesDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonEC2 ec2;
            final AmazonCloudWatch cloudWatch;
            try {
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                cloudWatch = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonCloudWatchClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CWEventsCustomerAlarmsOnEmoryInstances,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }


            Set<String> instanceIds = getEmoryManagedInstanceIds(ec2);
            Set<String> alarmArns = getInstanceAlarmArns(cloudWatch, instanceIds);

            for (String alarmArn : alarmArns) {
                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                        DetectionType.CWEventsCustomerAlarmsOnEmoryInstances, alarmArn);
            }
        }
    }

    private Set<String> getEmoryManagedInstanceIds(AmazonEC2 ec2) {
        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest()
                .withFilters(new Filter("tag:RHEDcloud", Arrays.asList("True","true")));
        boolean done = false;

        Set<String> instanceIds = new HashSet<>();

        while (!done) {
            DescribeInstancesResult describeInstancesResult = ec2.describeInstances(describeInstancesRequest);
            for (Reservation reservation : describeInstancesResult.getReservations()) {
                for (Instance instance : reservation.getInstances()) {
                    instanceIds.add(instance.getInstanceId());
                }
            }

            describeInstancesRequest.setNextToken(describeInstancesResult.getNextToken());

            if (describeInstancesResult.getNextToken() == null) {
                done = true;
            }
        }

        return instanceIds;
    }

    private Set<String> getInstanceAlarmArns(AmazonCloudWatch cloudWatch, Set<String> instanceIds) {

        DescribeAlarmsRequest describeAlarmsRequest = new DescribeAlarmsRequest();
        Set<String> alarmArns = new HashSet<>();

        boolean done = false;
        while (!done) {
            DescribeAlarmsResult describeAlarmsResult = cloudWatch.describeAlarms(describeAlarmsRequest);

            for (MetricAlarm metricAlarm : describeAlarmsResult.getMetricAlarms()) {
                if (metricAlarm.getDimensions() == null
                        || !metricAlarm.getNamespace().equals("AWS/EC2")) {
                    continue;
                }

                boolean hasInstance = metricAlarm.getDimensions().stream().anyMatch( dimension -> dimension.getName().equals("InstanceId") && instanceIds.contains(dimension.getValue()));
                if (hasInstance) {
                    alarmArns.add(metricAlarm.getAlarmArn());
                }
            }

            describeAlarmsRequest.setNextToken(describeAlarmsResult.getNextToken());

            if (describeAlarmsResult.getNextToken() == null) {
                done = true;
            }
        }

        return alarmArns;
    }

    @Override
    public String getBaseName() {
        return "CWEventsCustomerAlarmsOnEmoryInstances";
    }
}
