package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.AccountQuerySpecification;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition;
import com.openii.openeai.commands.OpeniiScheduledCommand;
import org.apache.logging.log4j.Logger;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.EnterpriseObjectGenerateException;
import org.openeai.moa.EnterpriseObjectQueryException;
import org.openeai.threadpool.ThreadPool;
import org.openeai.threadpool.ThreadPoolException;

import javax.jms.JMSException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Predicate;

public class SecurityRiskDetectionScheduledCommand extends OpeniiScheduledCommand implements ScheduledCommand {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionScheduledCommand.class);

    private boolean m_useThreads;
    private ThreadPool m_threadPool;
    private int m_sleepInterval;
    private ProducerPool m_awsSrdProducerPool;
    private ProducerPool m_awsAccountProducerPool;

    private String[] m_standardDetectorNames;
    private String[] m_hipaaDetectorNames;
    private boolean m_remediate;

    /*
     * some detectors take longer than the scheduled run time which would cause jobs to pile up
     * in the thread pool.  instead, keep track of what jobs are running and don't Generate a new
     * request if the job is still running from the last schedule time.
     */
    private final ConcurrentSkipListMap<String, Long> jobsInProgress = new ConcurrentSkipListMap<>();


    public SecurityRiskDetectionScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        String LOGTAG = "[SrdScheduledCommand] ";

        // Set the properties for this command.
        try {
            PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("AwsSrdGenerationCommandProperties");
            setProperties(pConfig.getProperties());
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving a property config from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // setup our properties
        setUseThreads(getProperties().getProperty("useThreads", "false").equalsIgnoreCase("true"));
        setSleepInterval(Integer.parseInt(getProperties().getProperty("sleepInterval", "500")));
        setStandardDetectorNames(getProperties().getProperty("Standard-detectors").trim().replaceAll("[\n\t\r ]", "").split(","));
        setHipaaDetectorNames(getProperties().getProperty("HIPAA-detectors").trim().replaceAll("[\n\t\r ]", "").split(","));
        setRemediate(getProperties().getProperty("remediate", "false").equalsIgnoreCase("true"));

        logger.info(LOGTAG + "The value of useThreads is: " + getUseThreads());
        logger.info(LOGTAG + "The value of sleepInterval is: " + getSleepInterval());
        logger.info(LOGTAG + "The value of Standard-detectors is: " + Arrays.toString(getStandardDetectorNames()));
        logger.info(LOGTAG + "The value of HIPAA-detectors is: " + Arrays.toString(getHipaaDetectorNames()));
        logger.info(LOGTAG + "The value of remediate is: " + getRemediate());

        // If we are going to use threads, get a thread pool from AppConfig to use.
        if (getUseThreads()) {
            try {
                ThreadPool threadPool = (ThreadPool) getAppConfig().getObject("AwsSrdGenerationThreadPool");
                setThreadPool(threadPool);
            } catch (EnterpriseConfigurationObjectException e) {
                String errMsg = "An error occurred retrieving a thread pool from AppConfig. The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
        }

        // Get the producer pools we specified in the deployment descriptor from AppConfig
        try {
            ProducerPool producerPool = (ProducerPool) getAppConfig().getObject("AwsSrdP2pProducer");
            setAwsSrdProducerPool(producerPool);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the AwsSrdP2pProducer producer pool from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            ProducerPool producerPool = (ProducerPool) getAppConfig().getObject("AwsAccountP2pProducer");
            setAwsAccountProducerPool(producerPool);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred retrieving the AwsAccountP2pProducer producer pool from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public int execute() throws ScheduledCommandException {
        long start = System.currentTimeMillis();

        String LOGTAG = "[SrdScheduledCommand]";
        logger.info(LOGTAG + " Executing at " + start
                + (getUseThreads() ? (" with " + getThreadPool().getJobsInProgress() + " threads already in the pool") : ""));

        class SRDAccount {
            private String accountId;
            private boolean isHipaa;
        }

        List<SRDAccount> srdAccounts = new ArrayList<>();

        PointToPointProducer awsAccountProducer = null;
        try {
            awsAccountProducer = (PointToPointProducer) getAwsAccountProducerPool().getExclusiveProducer();

            Account account
                    = (Account) getAppConfig().getObject("Account");
            AccountQuerySpecification accountQuerySpecification
                    = (AccountQuerySpecification) getAppConfig().getObject("AccountQuerySpecification");

            long retrieveAccountStart = System.currentTimeMillis();
            @SuppressWarnings("unchecked")
            List<Account> awsAccounts = account.query(accountQuerySpecification, awsAccountProducer);
            long retrieveAccountTimeElapsed = System.currentTimeMillis() - retrieveAccountStart;

            StringBuilder activeMsg = new StringBuilder();
            StringBuilder exemptMsg = new StringBuilder();
            Predicate<Property> isSrdExempt = p -> "srdExempt".equals(p.getKey()) && "true".equals(p.getValue());
            for (Account act : awsAccounts) {
                String accountIdAnnotation;
                switch (act.getComplianceClass()) {
                    case "Standard":
                        accountIdAnnotation = act.getAccountId() + "~S";
                        break;
                    case "HIPAA":
                        accountIdAnnotation = act.getAccountId() + "~H";
                        break;
                    case "Enhanced Security":
                        accountIdAnnotation = act.getAccountId() + "~ES";
                        break;
                    default:
                        accountIdAnnotation = act.getAccountId() + "~" + act.getComplianceClass();
                        break;
                }

                if (act.getProperty().stream().anyMatch(isSrdExempt)) {
                    if (exemptMsg.length() > 0) exemptMsg.append(", ");
                    exemptMsg.append(accountIdAnnotation);
                }
                else {
                    if (activeMsg.length() > 0) activeMsg.append(", ");
                    activeMsg.append(accountIdAnnotation);

                    SRDAccount srdAccount = new SRDAccount();
                    srdAccount.accountId = act.getAccountId();
                    srdAccount.isHipaa = act.getComplianceClass().equals("HIPAA");
                    srdAccounts.add(srdAccount);
                }
            }
            logger.info(LOGTAG + " Retrieved " + awsAccounts.size() + " accounts in elapsed time " + retrieveAccountTimeElapsed
                    + " ms.  Active accounts: " + activeMsg.toString() + "  Exempt accounts: " + exemptMsg.toString());

        } catch (JMSException e) {
            String errMsg = "An error occurred while getting AWS Account Producer. "
                    + "The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " " + errMsg);
            throw new ScheduledCommandException(errMsg);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred while getting Account or AccountQuerySpecification objects. "
                    + "The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " " + errMsg);
            throw new ScheduledCommandException(errMsg);
        } catch (EnterpriseObjectQueryException e) {
            String errMsg = "An error occurred while sending Query-Request to AWS Account Producer. "
                    + "The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " " + errMsg);
            throw new ScheduledCommandException(errMsg);
        } finally {
            if (awsAccountProducer != null)
                getAwsAccountProducerPool().releaseProducer(awsAccountProducer);
        }

        // for information purpose, keep track of the longest running job
        String longestRunningJob = null;
        long longestRunning = Long.MAX_VALUE;
        LocalDateTime longestRunningDT = null;
        Duration longestRunningDuration = null;

        for (SRDAccount account : srdAccounts) {
            // check all the detectors for either HIPAA or Standard based on the account type
            String[] eligibleDetectors = (account.isHipaa) ? getHipaaDetectorNames() : getStandardDetectorNames();

            for (String detector : eligibleDetectors) {
                if ("".equals(detector)) continue;  // certain configurations cause this so just skip them

                String detectorName = detector.replace("*", "") + "Detector";
                String jobKey = detectorName + "-" + account.accountId;
                String jobLOGTAG = LOGTAG + "[" + jobKey + "] ";

                // if the job is still running since a previous schedule, don't run it again this time
                // but just in case a response is lost, don't assume the detector is running longer than 10 minutes
                Long runningSince;
                if ((runningSince = jobsInProgress.get(jobKey)) != null) {
                    LocalDateTime runningSinceDT = LocalDateTime.ofInstant(Instant.ofEpochMilli(runningSince), ZoneId.systemDefault());
                    Duration runningSinceDuration = Duration.between(Instant.ofEpochMilli(runningSince), Instant.ofEpochMilli(start));

                    // for information purpose, keep track of the longest running job
                    if (runningSince < longestRunning) {
                        longestRunningJob = jobKey;
                        longestRunning = runningSince;
                        longestRunningDT = runningSinceDT;
                        longestRunningDuration = runningSinceDuration;
                    }

                    logger.info(jobLOGTAG + "already running since (" + runningSince + ")(" + runningSinceDT + ")(" + runningSinceDuration + ")");
                    continue;

                    /*
                    if ((start - runningSince) > 600_000) {
                        logger.info(jobLOGTAG + "Evicting already running since (" + runningSince + ")(" + runningSinceDT + ")(" + runningSinceDuration + ")");
                    } else {
                        logger.info(jobLOGTAG + "already running since (" + runningSince + ")(" + runningSinceDT + ")(" + runningSinceDuration + ")");
                        continue;
                    }
                    */
                }
                // not running so start a new job
                jobsInProgress.put(jobKey, start);

                try {
                    SecurityRiskDetectionRequisition detectionRequisition
                            = (SecurityRiskDetectionRequisition) getAppConfig().getObject("SecurityRiskDetectionRequisition");

                    detectionRequisition.setAccountId(account.accountId);
                    detectionRequisition.setSecurityRiskDetector(detectorName);
                    if (m_remediate && !detector.contains("*")) {
                        detectionRequisition.setSecurityRiskRemediator(detector + "Remediator");
                    }

                    SrdJob job = new SrdJob(detectionRequisition, jobKey, jobLOGTAG);

                    // If we are using threads, the delegate the processing of this request to the threadpool.
                    // Otherwise, process it directly.
                    if (getUseThreads()) {
                        // If this thread pool is set to check for available threads before
                        // adding jobs to the pool, it may throw an exception indicating it
                        // is busy when we try to add a job. We need to catch that exception
                        // and try to add the job until we are successful.
                        boolean jobAdded = false;
                        while (!jobAdded) {
                            try {
                                logger.info(jobLOGTAG + "Adding job to thread pool");
                                getThreadPool().addJob(job);
                                jobAdded = true;
                            } catch (ThreadPoolException tpe) {
                                logger.warn(jobLOGTAG + "The thread pool is busy."
                                        + " Will retry after sleeping for " + getSleepInterval() + " ms.");
                                try {
                                    Thread.sleep(getSleepInterval());
                                } catch (InterruptedException ie) {
                                    String errMsg = "An error occurred while sleeping to allow "
                                            + "threads in the pool to clear for processing. The exception is: " + ie.getMessage();
                                    logger.error(jobLOGTAG + errMsg);
                                    throw new ScheduledCommandException(errMsg);
                                }
                            }
                        }
                    }
                    else {
                        logger.info(jobLOGTAG + "Directly running job");
                        job.run();
                    }

                } catch (EnterpriseConfigurationObjectException e) {
                    String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: " + e.getMessage();
                    logger.error(jobLOGTAG + errMsg);
                } catch (EnterpriseFieldException e) {
                    String errMsg = "An error occurred setting up SecurityRiskDetectionRequisition. The exception is: " + e.getMessage();
                    logger.error(jobLOGTAG + errMsg);
                }
            }
        }

        if (longestRunning != Long.MAX_VALUE) {
            logger.info(LOGTAG + "[" + longestRunningJob + "] Longest running job ("
                    + longestRunning + ")(" + longestRunningDT + ")(" + longestRunningDuration + ")");
        }

        long timeElapsed = System.currentTimeMillis() - start;
        logger.info(LOGTAG + " Execution done in elapsed time " + timeElapsed + " ms");

        return 0;
    }

    /*
     * Indicates whether or not to use threads.
     */
    private boolean getUseThreads() { return m_useThreads; }
    private void setUseThreads(boolean useThreads) { m_useThreads = useThreads; }

    /*
     * The thread pool for the command to use.
     */
    private ThreadPool getThreadPool() { return m_threadPool; }
    private void setThreadPool(ThreadPool threadPool) { m_threadPool = threadPool; }

    /*
     * The sleep interval or number of milliseconds to sleep for thread operations
     * such as waiting to add jobs or waiting for jobs to complete.
     */
    private int getSleepInterval() { return m_sleepInterval; }
    private void setSleepInterval(int sleepInterval) { m_sleepInterval = sleepInterval; }


    /*
     * Producer pools for the command to use to communicate with SRD and AWS Account services.
     */
    private ProducerPool getAwsSrdProducerPool() { return m_awsSrdProducerPool; }
    private void setAwsSrdProducerPool(ProducerPool producerPool) { m_awsSrdProducerPool = producerPool; }
    private ProducerPool getAwsAccountProducerPool() { return m_awsAccountProducerPool; }
    private void setAwsAccountProducerPool(ProducerPool producerPool) { m_awsAccountProducerPool = producerPool; }

    /*
     * Standard and HIPAA detectors are defined in:
     * https://serviceforge.atlassian.net/wiki/spaces/ESSRDS/pages/137953292/Security+Risk+Detector+Technical+Design+Document
     *
     * The (root) name of the detectors for Standard and HIPAA accounts are configured in the AppConfig.
     * Remediation is also configured in the AppConfig.
     */
    private String[] getStandardDetectorNames() { return this.m_standardDetectorNames; }
    private void setStandardDetectorNames(String[] detectorNames) { this.m_standardDetectorNames = detectorNames; }
    private String[] getHipaaDetectorNames() { return this.m_hipaaDetectorNames; }
    private void setHipaaDetectorNames(String[] detectorNames) { this.m_hipaaDetectorNames = detectorNames; }
    private boolean getRemediate() { return m_remediate; }
    private void setRemediate(boolean remediate) { m_remediate = remediate; }

    /**
     * The SrdJob grabs a P2P Producer and then sends the SecurityRiskDetectionRequisition request.
     */
    private class SrdJob implements Runnable {
        private SecurityRiskDetectionRequisition detectionRequisition;
        private String jobKey;
        private String LOGTAG;

        SrdJob(SecurityRiskDetectionRequisition detectionRequisition, String jobKey, String LOGTAG) {
            this.detectionRequisition = detectionRequisition;
            this.jobKey = jobKey;
            this.LOGTAG = LOGTAG;
        }

        @Override
        public void run() {
            long start = System.currentTimeMillis();
            logger.info(LOGTAG + "Running job");

            PointToPointProducer awsSrdProducer = null;
            List generateResponse = null;
            try {
                SecurityRiskDetection detection = (SecurityRiskDetection) getAppConfig().getObject("SecurityRiskDetection.v1_0");
                detection.setCommandName("SecurityRiskDetectionGenerateCommand");

                logger.info(LOGTAG + "Getting an exclusive producer");
                awsSrdProducer = (PointToPointProducer) getAwsSrdProducerPool().getExclusiveProducer();
                awsSrdProducer.setRequestTimeoutInterval(600000);

                logger.info(LOGTAG + "Sending the Generate-Request");
                generateResponse = detection.generate(detectionRequisition, awsSrdProducer);

            } catch (EnterpriseConfigurationObjectException e) {
                logger.fatal(LOGTAG + "An error occurred retrieving an object from AppConfig"
                        + ". The exception is: " + e.getMessage());
            } catch (EnterpriseObjectGenerateException e) {
                if (e.getMessage().toLowerCase().contains("time")) {
                    logger.fatal(LOGTAG + "Timed out waiting for reply");
                } else if (e.getMessage().contains("SrdService-1003")) {
                    logger.fatal(LOGTAG + "Detector is already running");
                } else {
                    logger.fatal(LOGTAG + "An error occurred in SecurityRiskDetectionRequisition.Generate-Request"
                            + ". The exception is: " + e.getMessage());
                }
            } catch (JMSException e) {
                logger.fatal(LOGTAG + "An error occurred getting a producer from the producer pool"
                        + ". The exception is: " + e.getMessage());
            } finally {
                if (awsSrdProducer != null)
                    getAwsSrdProducerPool().releaseProducer(awsSrdProducer);

                logger.info(LOGTAG + "Released the exclusive producer");

                jobsInProgress.remove(jobKey);

                StringBuilder buf = new StringBuilder(1024);
                if (generateResponse != null) {
                    for (Object result : generateResponse) {
                        buf.append(" :: ").append(result);
                    }
                }
                logger.info(LOGTAG + "Finished in elapsed time " + (System.currentTimeMillis() - start) + " ms" + buf.toString());
            }
        }
    }
}
