package edu.emory.it.services.srd.obsolete;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import edu.emory.it.services.srd.DetectionStatus;
import edu.emory.it.services.srd.RemediationStatus;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Early sync data had a different format than modern sync data.
 * This program was a one-time migration to the new format.
 * Ran it sometime in 2019.
 */
public class SrdSyncDataMigration {
    private String storageTableName;
    private boolean readOnly;

    public static void main(String[] args) {
        String tableName = args[0];
        SrdSyncDataMigration migration = new SrdSyncDataMigration(tableName);

        long start = System.currentTimeMillis();

        try {
            migration.migrateForDeleteSyncDataForExample999999999999();
            //migration.migrateForDetectionResult();
        }
        finally {
            System.out.println("migration of " + tableName + " took " + (System.currentTimeMillis() - start) + " ms");
        }
    }

    private SrdSyncDataMigration(String v) {
        this.storageTableName = v;
        this.readOnly = false;  // controls updates to DynamoDB
    }

    /**
     * By design, the integration team was using the Example SRD/SRR and account 999999999999 for nagios monitoring.
     * An unintended side effect of that was that the SecurityRiskDetection syncs were getting persisted.
     * That had to be dealt with in Splunk and other downstream processes.
     *
     * Now, the nagios monitoring uses the dedicated ServiceMonitoring SRD (no SRR) which will not be persisted.
     * So, clean up the sync data to get rid of these unintended and useless items.
     */
    private void migrateForDeleteSyncDataForExample999999999999() {
        AmazonDynamoDB storageClient = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withRegion("us-east-1")
                .build();

        DynamoDB dynamoDB = new DynamoDB(storageClient);
        Table table = dynamoDB.getTable(storageTableName);

        System.out.println();

        ValueMap valueMap = new ValueMap();
        valueMap.withString(":ta", "Example-999999999999");

        // scan the entire table
        ScanSpec scanSpec = new ScanSpec()
                .withFilterExpression("typeAccount = :ta")
                .withValueMap(valueMap);

        for (Item item : table.scan(scanSpec)) {
            String itemCapture = item.toJSON();

            System.out.println("Deleting |||" + (readOnly ? "read-only|||" : "") + itemCapture);

            if (!readOnly) {
                String item_typeAccount = item.getString("typeAccount");
                long item_createDatetime = item.getLong("createDatetime");
                PrimaryKey item_primaryKey = new PrimaryKey("typeAccount", item_typeAccount, "createDatetime", item_createDatetime);
                table.deleteItem(item_primaryKey);
            }
        }
    }

    /**
     * The initial design of the SecurityRiskDetection MOA didn't allow for a status tied to the detection phase.
     * The only status available was in the DetectedSecurityRisk.RemediationResult but that was problematic if the detector failed.
     * A change was made to the to add DetectionResult in https://bitbucket.org/itarch/aws-moas/commits/95e8ce6983aa0a6d5245e5af73fe8b94f9cd95bc
     *
     * Also, over time, the status values of the remediation changed and were eventually standardized to RemediationStatus.
     *
     * So, the sync data ended up having some old status values and some new and some hacks to record detection status in the remediation status.
     * This migration fixes all of that.
     */
    private void migrateForDetectionResult() {
        AmazonDynamoDB storageClient = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withRegion("us-east-1")
                .build();

        DynamoDB dynamoDB = new DynamoDB(storageClient);
        Table table = dynamoDB.getTable(storageTableName);

        System.out.println();

        // we have to scan the entire table
        Iterator<Item> values = table.scan(new ScanSpec()).iterator();

        while (values.hasNext()) {
            Item item = values.next();
            String itemCapture = item.toJSON();

            String setExpr = "";
            String removeExpr = "";
            NameMap nameMap = new NameMap();
            ValueMap valueMap = new ValueMap();

            String item_typeAccount = item.getString("typeAccount");
            long item_createDatetime = item.getLong("createDatetime");
            String item_securityRiskDetectionId = item.getString("securityRiskDetectionId");
            Map<String, Object> item_detection = item.getMap("detection");
            PrimaryKey item_primaryKey = new PrimaryKey("typeAccount", item_typeAccount, "createDatetime", item_createDatetime);

            // item_createDatetime has to be valid or getLong would throw new NumberFormatException
            if (item_typeAccount == null) {
                throw new RuntimeException("item.typeAccount is null " + itemCapture);
            }
            if (item_detection == null) {
                throw new RuntimeException("item.detection is null " + itemCapture);
            }
            if (item_securityRiskDetectionId == null) {
                /*
                - securityRiskDetectionId used to be found only in the detection sub-item but it was moved
                  to the top level so that it could be made into a secondary index for query support.

                - a lot of items in lower (non-PROD) environments don't have securityRiskDetectionId
                  at the top level but can get it from the detection sub-item. the items that don't have
                  it at all can not be migrated, so are deleted
                */

                if (storageTableName.equals("SecurityRiskDetectorSyncProd")) {
                    // luckily it does not happen in PROD
                    throw new RuntimeException("item.securityRiskDetectionId is null " + itemCapture);
                }
                else {
                    // in lower environments there is some repair possible for certain items
                    if (item_detection.get("securityRiskDetectionId") != null) {
                        // securityRiskDetectionId moved (early on) out of detection to the top level
                        setExpr += (!setExpr.isEmpty() ? "," : "") + " securityRiskDetectionId = :val_srdi";
                        valueMap.withString(":val_srdi", (String) item_detection.get("securityRiskDetectionId"));
                    }
                    else {
                        // data is irreparable so delete it but only because it is a non-PROD environment
                        System.out.println("Deleting |||" + (readOnly ? "read-only|||" : "") + itemCapture);
                        if (!readOnly) table.deleteItem(item_primaryKey);
                        continue;
                    }
                }
            }
            else if (item_securityRiskDetectionId.equals("9415466")) {
                System.currentTimeMillis(); // set breakpoint here for debugging
            }

            String item_detection_type = (String) item_detection.get("type");
            String item_detection_accountId = (String) item_detection.get("accountId");
            String item_detection_detectorName = (String) item_detection.get("detector");
            String item_detection_remediatorName = (String) item_detection.get("remediator");

            if (item_detection_type == null) {
                throw new RuntimeException("item.detection.type is null " + itemCapture);
            }
            if (item_detection_accountId == null) {
                throw new RuntimeException("item.detection.accountId is null " + itemCapture);
            }
            if (item_detection_detectorName == null) {
                throw new RuntimeException("item.detection.detector is null " + itemCapture);
            }
            if (item_detection_remediatorName == null) {
                throw new RuntimeException("item.detection.remediator is null " + itemCapture);
            }

            if (item_detection.get("instanceId") == null) {
                // instanceId was added late in the development cycle
                setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.instanceId = :val_iid";
                valueMap.withString(":val_iid", "ip-0-0-0-0");
            }

            /* see commentary above with item_securityRiskDetectionId */
            if (item_detection.get("securityRiskDetectionId") != null) {
                // securityRiskDetectionId moved (early on) out of detection to the top level
                item_detection.remove("securityRiskDetectionId");
                removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.securityRiskDetectionId";
            }


            @SuppressWarnings("unchecked")
            Map<String, Object> item_detection_detectionResult = (Map) item_detection.get("detectionResult");
            @SuppressWarnings("unchecked")
            Map<String, Object> item_detection_remediateResult = (Map) item_detection.get("remediateResult");

            // until the migration is complete most items will not have a DetectionResult
            // after the migration is complete all items must have a DetectionResult

            if (item_detection_detectionResult == null) {
                // the DynamoDB item may not have a DetectionResult until the data migration is complete
                // but it is required in SecurityRiskDetection so fabricate one now
                // inferring DetectionResult takes remediateResult into account

                if (item_detection_remediateResult != null) {

                    String item_detection_remediateResult_status = (String) item_detection_remediateResult.get("status");
                    String item_detection_remediateResult_description = (String) item_detection_remediateResult.get("description");

                    if (item_detection_remediateResult_description != null
                            && item_detection_remediateResult_description.equals("Exempt Error")
                            && item_detection_remediateResult_status != null
                            && (item_detection_remediateResult_status.equals("Error")
                             || item_detection_remediateResult_status.equals("Remediation Failed"))) {
                        // Exempt Error migrated to detection.detectionResult
                        item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                        item_detection_detectionResult.put("status", "Detection Account Exempt");
                        item_detection_detectionResult.put("error", Collections.singletonList("Account is exempt from Security Risk Detection scans"));

                        item_detection_remediateResult = null;
                        item_detection.remove("remediateResult");

                        removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.arn";
                        removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.remediateResult";

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                        valueMap.withMap(":dr", item_detection_detectionResult);
                    }
                    else if (item_detection_remediateResult_description != null
                            && item_detection_remediateResult_description.equals("Remediation Error")
                            && item_detection_remediateResult_status != null
                            && (item_detection_remediateResult_status.equals("Error")
                             || item_detection_remediateResult_status.equals("Remediation Failed"))) {
                        // for there to be a Remediation Error the detection must have been successful
                        // remediateResult.status will be updated to RemediationStatus.REMEDIATION_FAILED below
                        item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                        item_detection_detectionResult.put("status", DetectionStatus.DETECTION_SUCCESS.getStatus());

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                        valueMap.withMap(":dr", item_detection_detectionResult);
                    }
                    else if (item_detection_remediateResult_status != null
                            && (item_detection_remediateResult_status.equals("No Change")
                             || item_detection_remediateResult_status.equals("Remediation Unnecessary")
                             || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_SUCCESS.getStatus())
                             || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_FAILED.getStatus())
                             || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_POSTPONED.getStatus())
                             || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.getStatus())
                             || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()))) {
                        // detection must have been successful for a remediation with a recognized status or
                        // one of the old statuses like "No Change" or "Remediation Unnecessary"
                        // "No Change" and "Remediation Unnecessary" were the old name for REMEDIATION_NO_LONGER_REQUIRED which gets fixed below
                        item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                        item_detection_detectionResult.put("status", DetectionStatus.DETECTION_SUCCESS.getStatus());

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                        valueMap.withMap(":dr", item_detection_detectionResult);
                    }
                    else {
                        throw new RuntimeException("item.detection.detectionResult.status is null " + itemCapture);
                    }
                }
                else /* item_detection_remediateResult == null */ {
                    if (item_detection_remediatorName.equals("none")) {
                        if (item_detection.get("arn") != null) {
                            // no remediation requested and there is an ARN which only gets set on successful detection
                            item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                            item_detection_detectionResult.put("status", DetectionStatus.DETECTION_SUCCESS.getStatus());

                            setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                            valueMap.withMap(":dr", item_detection_detectionResult);
                        }
                        else {
                            // remediation was requested but there is no ARN so detection failed
                            item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                            item_detection_detectionResult.put("status", DetectionStatus.DETECTION_FAILED.getStatus());
                            item_detection_detectionResult.put("error", Collections.singletonList("Reason for detection failure was not recorded"));

                            setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                            valueMap.withMap(":dr", item_detection_detectionResult);
                        }
                    }
                    else if (item_detection.get("arn") != null) {
                        // no detection.remediateResult but there's a valid remediator name and an ARN
                        item_detection.put("detectionResult", (item_detection_detectionResult = new HashMap<>()));
                        item_detection_detectionResult.put("status", DetectionStatus.DETECTION_SUCCESS.getStatus());

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.detectionResult = :dr";
                        valueMap.withMap(":dr", item_detection_detectionResult);

                        item_detection.put("remediateResult", (item_detection_remediateResult = new HashMap<>()));
                        item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_FAILED.getStatus());
                        item_detection_remediateResult.put("description", "Description for remediation failure was not recorded");
                        item_detection_remediateResult.put("error", Collections.singletonList("Reason for remediation failure was not recorded"));

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult = :rr";
                        valueMap.withMap(":rr", item_detection_remediateResult);
                    }
                    else {
                        throw new RuntimeException("item.detection.detectionResult is null " + itemCapture);
                    }
                }
            }
            else {
                // a bug in some early migrations created item.detection.detectionResult.type
                // but as noted in SecurityRiskDetectionSyncCommand.java that shouldn't happen
                if (item_detection_detectionResult.get("type") != null) {
                    item_detection_detectionResult.remove("type");
                    removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.detectionResult.#drt";
                    nameMap.with("#drt", "type");
                }

                boolean isDetectionSuccess = DetectionStatus.DETECTION_SUCCESS.getStatus().equals(item_detection_detectionResult.get("status"));
                if (!isDetectionSuccess && item_detection_remediateResult != null) {
                    // this one is a bit odd because detection failed for some reason but
                    // not before creating at least one DetectedSecurityRisk (detection.remediateResult)
                    item_detection_remediateResult = null;
                    item_detection.remove("remediateResult");

                    removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.remediateResult";

                    System.out.println("item.detection.remediateResult is not null on not success |||" + itemCapture);
                }
            }

            // after the migration is complete all items must have a detectionResult

            String item_detection_detectionResult_status = (String) item_detection_detectionResult.get("status");
            if (item_detection_detectionResult_status == null) {
                throw new RuntimeException("item.detection.detectionResult.status is null " + itemCapture);
            }
            boolean anyDetectionStatusMatch = Arrays.stream(DetectionStatus.values())
                    .anyMatch(ds -> ds.getStatus().equals(item_detection_detectionResult_status));
            if (!anyDetectionStatusMatch) {
                throw new RuntimeException("item.detection.detectionResult.status is invalid " + itemCapture);
            }
            if (!item_detection_detectionResult_status.equals(DetectionStatus.DETECTION_SUCCESS.getStatus())) {
                @SuppressWarnings("unchecked")
                List<String> item_detection_detectionResult_errors = (List) item_detection_detectionResult.get("error");
                if (item_detection_detectionResult_errors == null || item_detection_detectionResult_errors.isEmpty()) {
                    throw new RuntimeException("item.detection.detectionResult.error is null " + itemCapture);
                }
            }

            if (item_detection_remediateResult == null) {
                // early development caused successful detection with no remediate results
                boolean isDetectionSuccess = DetectionStatus.DETECTION_SUCCESS.getStatus().equals(item_detection_detectionResult.get("status"));
                // Exempt Error migrated to detection.detectionResult
                boolean isAccountExempt = "Detection Account Exempt".equals(item_detection_detectionResult.get("status"));
                // failure in the detector means remediator could not have run
                boolean isDetectionFailed = DetectionStatus.DETECTION_FAILED.getStatus().equals(item_detection_detectionResult.get("status"));
                // timeout in the detector means remediator could not have run
                boolean isDetectionTimeout = DetectionStatus.DETECTION_TIMEOUT.getStatus().equals(item_detection_detectionResult.get("status"));
                // no remediation requested so no remediation results
                boolean isRemediationRequested = !item_detection_remediatorName.equals("none");

                if (!isAccountExempt && !isDetectionFailed && !isDetectionTimeout && isRemediationRequested) {
                    if (isDetectionSuccess) {
                        item_detection.put("remediateResult", (item_detection_remediateResult = new HashMap<>()));
                        item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_SUCCESS.getStatus());
                        item_detection_remediateResult.put("description", "Description for remediation success was not recorded");

                        setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult = :rr";
                        valueMap.withMap(":rr", item_detection_remediateResult);
                    }
                    else {
                        throw new RuntimeException("item.detection.remediateResult is null " + itemCapture);
                    }
                }
            }
            else {
                String item_detection_remediateResult_status = (String) item_detection_remediateResult.get("status");
                if (item_detection_remediateResult_status == null) {
                    throw new RuntimeException("item.detection.remediateResult.status is null " + itemCapture);
                }
                String item_detection_remediateResult_description = (String) item_detection_remediateResult.get("description");
                if (item_detection_remediateResult_description == null) {
                    throw new RuntimeException("item.detection.remediateResult.description is null " + itemCapture);
                }

                @SuppressWarnings("unchecked")
                List<String> item_detection_remediateResult_errors = (List) item_detection_remediateResult.get("error");
                final String remediateResult_status = item_detection_remediateResult_status;  // for the closure

                if (item_detection_remediatorName.equals("none")
                        && item_detection_remediateResult_description.equals("Description for remediation success was not recorded")) {
                    // because of a bug in a previous version of the migration script, some of these errors crept into the data
                    item_detection_remediateResult = null;
                    item_detection.remove("remediateResult");

                    removeExpr += (!removeExpr.isEmpty() ? "," : "") + " detection.remediateResult";
                }
                else if (item_detection_remediatorName.equals("none")) {
                    throw new RuntimeException("item.detection.remediatorName is none but found remediateResult " + itemCapture);
                }
                else {
                    boolean anyRemediationStatusMatch = Arrays.stream(RemediationStatus.values())
                            .anyMatch(ds -> ds.getStatus().equals(remediateResult_status));
                    if (!anyRemediationStatusMatch) {
                        if (item_detection_remediateResult_status.equals("Error")
                                && item_detection_remediateResult_description.equals("Remediation Error")) {
                            if (item_detection_remediateResult_errors == null || item_detection_remediateResult_errors.isEmpty()) {
                                throw new RuntimeException("item.detection.remediateResult.error is null " + itemCapture);
                            }
                            if (item_detection_remediateResult_errors.get(0).contains("connect timed out")) {
                                item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_TIMEOUT.getStatus());
                                item_detection_remediateResult_status = RemediationStatus.REMEDIATION_TIMEOUT.getStatus();

                                setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult.#rrstatus = :rrs";
                                valueMap.withString(":rrs", item_detection_remediateResult_status);
                                nameMap.with("#rrstatus", "status");
                            }
                            else {
                                // Error.Remediation Error becomes RemediationStatus.REMEDIATION_FAILED
                                item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_FAILED.getStatus());
                                item_detection_remediateResult_status = RemediationStatus.REMEDIATION_FAILED.getStatus();

                                setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult.#rrstatus = :rrs";
                                valueMap.withString(":rrs", item_detection_remediateResult_status);
                                nameMap.with("#rrstatus", "status");
                            }
                        }
                        else if (item_detection_remediateResult_status.equals("Remediation Unnecessary")
                                || item_detection_remediateResult_status.equals("No Change")) {
                            // "Remediation Unnecessary" and "No Change" become RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED
                            item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus());
                            item_detection_remediateResult_status = RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus();

                            setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult.#rrstatus = :rrs";
                            valueMap.withString(":rrs", item_detection_remediateResult_status);
                            nameMap.with("#rrstatus", "status");
                        }
                        else {
                            throw new RuntimeException("item.detection.remediateResult.status is invalid " + itemCapture);
                        }
                    }
                    if (item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_POSTPONED.getStatus())
                            || item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus())) {
                        // all good
                        if (item_detection_remediateResult_errors != null && !item_detection_remediateResult_errors.isEmpty()) {
                            throw new RuntimeException("item.detection.remediateResult.error is not null " + itemCapture);
                        }
                    }
                    else if (item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_FAILED.getStatus())) {
                        if (item_detection_remediateResult_errors == null || item_detection_remediateResult_errors.isEmpty()) {
                            throw new RuntimeException("item.detection.remediateResult.error is null " + itemCapture);
                        }
                        if (item_detection_remediateResult_errors.get(0).contains("connect timed out")) {
                            item_detection_remediateResult.put("status", RemediationStatus.REMEDIATION_TIMEOUT.getStatus());
                            item_detection_remediateResult_status = RemediationStatus.REMEDIATION_TIMEOUT.getStatus();

                            setExpr += (!setExpr.isEmpty() ? "," : "") + " detection.remediateResult.#rrstatus = :rrs";
                            valueMap.withString(":rrs", item_detection_remediateResult_status);
                            nameMap.with("#rrstatus", "status");
                        }
                    }
                    else if (!item_detection_remediateResult_status.equals(RemediationStatus.REMEDIATION_SUCCESS.getStatus())) {
                        if (item_detection_remediateResult_errors == null || item_detection_remediateResult_errors.isEmpty()) {
                            throw new RuntimeException("item.detection.remediateResult.error is null " + itemCapture);
                        }
                    }
                }
            }


            if (setExpr.isEmpty() && removeExpr.isEmpty()) {
                System.out.println("Unchanged |||" + itemCapture);
            }
            else {
                String updateExpression = (setExpr.isEmpty() ? "" : (" set " + setExpr))
                        + (removeExpr.isEmpty() ? "" : (" remove " + removeExpr));

                System.out.println("Updating |||" + (readOnly ? "read-only|||" : "") + updateExpression + "|||" + valueMap + "|||" + nameMap + "|||" + itemCapture);

                UpdateItemSpec updateItemSpec = new UpdateItemSpec()
                        .withPrimaryKey(item_primaryKey)
                        .withUpdateExpression(updateExpression)
                        .withValueMap(valueMap.isEmpty() ? null : valueMap)
                        .withNameMap(nameMap.isEmpty() ? null : nameMap);

                if (!readOnly) table.updateItem(updateItemSpec);
            }
        }
    }
}
