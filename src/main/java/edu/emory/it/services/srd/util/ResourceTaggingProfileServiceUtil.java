package edu.emory.it.services.srd.util;

import org.apache.logging.log4j.Logger;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.transport.TransportException;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.ResourceTaggingProfileQuerySpecification;

import javax.jms.JMSException;
import java.util.List;

public class ResourceTaggingProfileServiceUtil {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ResourceTaggingProfileServiceUtil.class);

    private static final Object lock = new Object();
    private static ResourceTaggingProfileServiceUtil instance;

    private boolean serviceReady;

    private AppConfig appConfig;
    private ProducerPool resourceTaggingProfileServiceProducerPool;
    private static final Integer producerRequestTimeoutInterval = 300_000;

    public static ResourceTaggingProfileServiceUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new ResourceTaggingProfileServiceUtil();
                }
            }
        }
        return instance;
    }

    public void initialize(AppConfig appConfig, String LOGTAG) {
        serviceReady = false;
        this.appConfig = appConfig;

        StringBuilder builder = new StringBuilder();
        try {
            this.resourceTaggingProfileServiceProducerPool = (ProducerPool) appConfig.getObject("RtpsP2PProducer");
        }
        catch (Exception e) {
            builder.append("Missing app config objects: RtpsP2PProducer");
        }

        try {
            // checking to see if the MOA is defined in the app config
            ResourceTaggingProfile unused = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile");
        }
        catch (Exception e) {
            if (builder.length() == 0) {
                builder.append("Missing app config objects:");
            }
            builder.append(" ResourceTaggingProfile");
        }

        try {
            // checking to see if the MOA is defined in the app config
            ResourceTaggingProfileQuerySpecification unused = (ResourceTaggingProfileQuerySpecification) appConfig.getObject("ResourceTaggingProfileQuerySpecification");
        }
        catch (Exception e) {
            if (builder.length() == 0) {
                builder.append("Missing app config objects:");
            }
            builder.append(" ResourceTaggingProfileQuerySpecification");
        }

        if (builder.length() > 0) {
            logger.error(LOGTAG + builder.toString());
        }
        else {
            serviceReady = true;
        }
    }

    public boolean isServiceReady() {
        return serviceReady;
    }

    public ResourceTaggingProfile query(String profileTagValue, String LOGTAG) {
        if (!serviceReady) {
            return null;
        }

        ResourceTaggingProfile resourceTaggingProfile;
        ResourceTaggingProfileQuerySpecification querySpec;
        try {
            resourceTaggingProfile = (ResourceTaggingProfile) appConfig.getObject("ResourceTaggingProfile");
            querySpec = (ResourceTaggingProfileQuerySpecification) appConfig.getObject("ResourceTaggingProfileQuerySpecification");
        }
        catch (Exception e) {
            String errMsg = "An error occurred retrieving an object from AppConfig. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
            return null;
        }

        // Convention: profile value is colon separated namespace and profile name and optional revision
        String namespace, profileName, revision;
        String[] segments = profileTagValue.split(":");

        if (segments.length == 2) {
            namespace = segments[0];
            profileName = segments[1];
            revision = null;
        }
        else if (segments.length == 3) {
            namespace = segments[0];
            profileName = segments[1];
            revision = segments[2];
        }
        else {
            logger.error(LOGTAG + "Invalid profile tag value: " + profileTagValue);
            return null;
        }

        PointToPointProducer p2pProducer = null;  // should be using a caching producer
        try {
            p2pProducer = (PointToPointProducer) resourceTaggingProfileServiceProducerPool.getExclusiveProducer();
            p2pProducer.setRequestTimeoutInterval(producerRequestTimeoutInterval);

            querySpec.setNamespace(namespace);
            querySpec.setProfileName(profileName);
            if (revision != null)
                querySpec.setRevision(revision);

            @SuppressWarnings("unchecked")
            List<ResourceTaggingProfile> profiles = p2pProducer.query(querySpec, resourceTaggingProfile);

            // if we queried for a specific profile revision then there can only be one
            if (revision != null) {
                if (profiles.size() == 0) {
                    logger.error(LOGTAG + "ResourceTaggingProfile query found no profile for " + profileTagValue);
                    return null;
                }
                else if (profiles.size() == 1) {
                    return profiles.get(0);
                }
                else {
                    logger.error(LOGTAG + "ResourceTaggingProfile query results unexpected - expected 1 profile for " + profileTagValue + " but got " + profiles.size());
                    return null;
                }
            }

            // otherwise the query was only for namespace and profile name so return the active profile
            for (ResourceTaggingProfile taggingProfile : profiles) {
                if (taggingProfile.getActive().equals("true")) {
                    return taggingProfile;
                }
            }

            // no active profile found which is odd so log everything to help with diagnostics
            StringBuilder buf = new StringBuilder(4096);
            buf.append("ResourceTaggingProfile query found no active profiles for ").append(profileTagValue);
            try {
                buf.append(" --- ResourceTaggingProfileQuerySpecification is ").append(querySpec.toXmlString());

                if (profiles.size() == 0) {
                    buf.append(" --- No results returned by the RTP Service");
                }
                else {
                    for (int i = 0; i < profiles.size(); i++) {
                        buf.append("--- ResourceTaggingProfile");
                        buf.append("[").append(i+1).append(" of ").append(profiles.size()).append("] ");
                        buf.append(profiles.get(i).toXmlString());
                    }
                }

                logger.error(LOGTAG + buf);
            }
            catch (XmlEnterpriseObjectException e) {
                String errMsg = "An error occurred serializing the object to XML. The exception is: " + e.getMessage()
                        + "  Diagnostics so far " + buf;
                logger.error(LOGTAG + errMsg);
            }

            return null;
        }
        catch (EnterpriseFieldException e) {
            logger.error(LOGTAG + "ResourceTaggingProfile query failed - error setting fields on ResourceTaggingProfileQuerySpecification", e);
        }
        catch (TransportException e) {
            logger.error(LOGTAG + "ResourceTaggingProfile query failed - for resource profile tag: " + profileTagValue, e);
        }
        catch (JMSException e) {
            logger.error(LOGTAG + "ResourceTaggingProfile query failed - Failed to obtain producer for resource profile tag: " + profileTagValue, e);
        }
        catch (Exception e) {
            logger.error(LOGTAG + "ResourceTaggingProfile query failed - Unhandled exception for resource profile tag: " + profileTagValue, e);
        }
        finally {
            if (p2pProducer != null) {
                resourceTaggingProfileServiceProducerPool.releaseProducer(p2pProducer);
            }
        }

        return null;
    }
}
