package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.directory.AWSDirectoryService;
import com.amazonaws.services.directory.AWSDirectoryServiceClient;
import com.amazonaws.services.directory.model.DescribeDirectoriesRequest;
import com.amazonaws.services.directory.model.DescribeDirectoriesResult;
import com.amazonaws.services.directory.model.DirectoryDescription;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects if an ineligible Directory Service is used<br>
 *
 * <p>The detector look for directories that have 'Edition' set to 'Standard'</p>
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class DSIneligibleDirectoryDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AWSDirectoryService directoryService;
            try {
                directoryService = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSDirectoryServiceClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.DSIneligibleDirectory,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:ds:" + region.getName() + ":" + accountId + ":directory/";
            checkDirectory(directoryService, detection, arnPrefix, srContext.LOGTAG);
        }
    }

    private void checkDirectory(AWSDirectoryService directoryService, SecurityRiskDetection detection,
                                String arnPrefix, String LOGTAG) {
        DescribeDirectoriesRequest request = new DescribeDirectoriesRequest();
        boolean done = false;
        while (!done) {
            DescribeDirectoriesResult result = directoryService.describeDirectories(request);

            for (DirectoryDescription description : result.getDirectoryDescriptions()) {
                if (description.getEdition() == null || !description.getEdition().equals("Enterprise")) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.DSIneligibleDirectory, arnPrefix + description.getDirectoryId());
                }
            }

            request.setNextToken(result.getNextToken());

            if (request.getNextToken() == null) {
                done = true;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "DSIneligibleDirectory";
    }
}
