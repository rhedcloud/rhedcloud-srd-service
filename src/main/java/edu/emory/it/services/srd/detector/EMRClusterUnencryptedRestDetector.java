package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.model.Cluster;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.function.BiPredicate;

/**
 * Detect Elastic Map Reduce clusters where encryption at-rest is not enabled.
 *
 * @see edu.emory.it.services.srd.remediator.EMRClusterUnencryptedRestRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EMRClusterUnencryptedRestDetector extends EMRClusterBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, DetectionType.EMRClusterUnencryptedRest);
    }

    @Override
    protected BiPredicate<AmazonElasticMapReduce, Cluster> getPredicate() {
        return (emrClient, cluster) ->
                ! super.hasSecurityConfigurationSetting(emrClient, cluster, "EnableAtRestEncryption");
    }

    @Override
    public String getBaseName() {
        return "EMRClusterUnencryptedRest";
    }
}
