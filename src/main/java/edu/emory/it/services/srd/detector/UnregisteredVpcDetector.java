package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.VirtualPrivateCloud;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.Vpc;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.PropertyConfig;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Check for VPCs that are not registered with AWS Account Service.

 * @see edu.emory.it.services.srd.remediator.UnregisteredVpcRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class UnregisteredVpcDetector extends AbstractSecurityRiskDetector {
    /**
     * Some regions, like us-iso-east-1, cause long connection attempts that ultimately timeout
     * so we want to ignore them completely.  Other regions like the us-gov regions are non-public and can be ignored.
     */
    private final Set<String> ignoredRegionNames = new HashSet<>();

    /**
     * Recently there was a case where a VPC for a different account was returned by AWS Account Service.
     * To help diagnose the problem, have a detector specific flag that will force it to not record detected risks.
     */
    private boolean disableRiskDetection = false;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
        super.init(aConfig, credentialsProvider, securityRole);

        // ignored region list and disableRiskDetection are optional
        Object o = appConfig.getObjects().get(getBaseName().toLowerCase());
        if (o instanceof PropertyConfig) {
            String property = ((PropertyConfig) o).getProperties().getProperty("ignoredRegions");
            if (property != null && !property.isEmpty()) {
                String[] ignoredRegions = property.replaceAll("[\n\t\r ]", "").split(",");
                for (String ignoredRegion : ignoredRegions)
                    if (!ignoredRegion.isEmpty())
                        this.ignoredRegionNames.add(ignoredRegion);
            }
            property = ((PropertyConfig) o).getProperties().getProperty("disableRiskDetection");
            if (property != null && !property.isEmpty()) {
                this.disableRiskDetection = Boolean.parseBoolean(property.replaceAll("[\n\t\r ]", ""));
            }
        }
    }

    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
        List<VirtualPrivateCloud> registeredVpcs;
        try {
            registeredVpcs = awsAccountServiceUtil.getRegisteredVpcs(accountId, srContext.LOGTAG);
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.UnregisteredVpc,
                    srContext.LOGTAG, "Error getting registered VPCs from AWS Account Service", e);
            return;
        }

        // VPCs are unconditionally created in every AWS region during provisioning so we want
        // to scan all of them and not just the RHEDcloud regions
        for (Region region : RegionUtils.getRegions()) {
            if (ignoredRegionNames.contains(region.getName())) {
                logger.info(srContext.LOGTAG + "Ignoring region: " + region.getName());
                continue;
            }
            DescribeVpcsResult describeVpcsResult;
            try {
                // get the VPCs in AWS
                AmazonEC2 ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                describeVpcsResult = ec2.describeVpcs();
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.UnregisteredVpc,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            // and compare - anything unregistered is flagged as a risk
            for (Vpc vpc : describeVpcsResult.getVpcs()) {
                boolean isVpcRegistered = registeredVpcs.stream()
                        .anyMatch(regVpc -> regVpc.getVpcId().equals(vpc.getVpcId()));
                if (!isVpcRegistered) {
                    if (disableRiskDetection) {
                        logger.info(srContext.LOGTAG + "Detection disabled for type " + DetectionType.UnregisteredVpc
                                + " with ARN " + "arn:aws:ec2:" + region.getName() + ":" + accountId + ":vpc/" + vpc.getVpcId());
                    }
                    else {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.UnregisteredVpc,
                                "arn:aws:ec2:" + region.getName() + ":" + accountId + ":vpc/" + vpc.getVpcId());
                    }
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "UnregisteredVpc";
    }
}
