package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.elasticache.AmazonElastiCache;
import com.amazonaws.services.elasticache.AmazonElastiCacheClient;
import com.amazonaws.services.elasticache.model.DeleteCacheClusterRequest;
import com.amazonaws.services.elasticache.model.DeleteGlobalReplicationGroupRequest;
import com.amazonaws.services.elasticache.model.DeleteReplicationGroupRequest;
import com.amazonaws.services.elasticache.model.DescribeGlobalReplicationGroupsRequest;
import com.amazonaws.services.elasticache.model.DescribeGlobalReplicationGroupsResult;
import com.amazonaws.services.elasticache.model.DescribeReplicationGroupsRequest;
import com.amazonaws.services.elasticache.model.DescribeReplicationGroupsResult;
import com.amazonaws.services.elasticache.model.DisassociateGlobalReplicationGroupRequest;
import com.amazonaws.services.elasticache.model.GlobalReplicationGroup;
import com.amazonaws.services.elasticache.model.GlobalReplicationGroupMember;
import com.amazonaws.services.elasticache.model.InvalidCacheClusterStateException;
import com.amazonaws.services.elasticache.model.InvalidGlobalReplicationGroupStateException;
import com.amazonaws.services.elasticache.model.InvalidReplicationGroupStateException;
import com.amazonaws.services.elasticache.model.ReplicationGroup;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

public abstract class ElastiCacheBaseRemediator extends AbstractSecurityRiskRemediator {
    private static final long TEN_MINUTES_IN_MILLIS = 10 * 60 * 1000;

    // only a single remediation mode is needed at this time
    protected enum RemediationMode {
        DELETE
    }
    protected void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext,
                             DetectionType detectionType, RemediationMode remediationMode) {
        if (remediatorPreConditionFailed(detected, "arn:aws:elasticache:", srContext.LOGTAG, detectionType))
            return;
        // like arn:aws:elasticache:us-east-1:123456789012:cluster:cache-cluster-id
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null)
            return;

        AmazonElastiCache client;
        try {
            client = getClient(accountId, arn.getRegion(), srContext.getMetricCollector(), AmazonElastiCacheClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error getting ElastiCache client");
            return;
        }

        if (remediationMode == RemediationMode.DELETE) {
            switch (arn.getResource().getResourceType()) {
                case "global-replication-group":
                    deleteGlobalReplicationGroup(accountId, detected, srContext, client, arn);
                    break;
                case "replication-group":
                    deleteReplicationGroup(detected, srContext, client, arn);
                    break;
                case "cluster":
                    deleteCacheCluster(detected, srContext, client, arn);
                    break;
                default:
                    setError(detected, srContext.LOGTAG, "Unsupported ElastiCache resource type");
                    break;
            }
        }
        else {
            setError(detected, srContext.LOGTAG, "Unsupported remediation mode: " + remediationMode);
        }
    }

    /**
     * Delete a "simple" ElastiCache cache cluster.  Simple cache clusters do not have replicas or shards.
     * All memcached clusters are "simple".
     */
    private void deleteCacheCluster(DetectedSecurityRisk detected, SecurityRiskContext srContext, AmazonElastiCache client, Arn arn) {
        try {
            client.deleteCacheCluster(new DeleteCacheClusterRequest().withCacheClusterId(arn.getResource().getResource()));
            setSuccess(detected, srContext.LOGTAG, "ElastiCache cache cluster deleted.");
        }
        catch (InvalidCacheClusterStateException e) {
            setPostponed(detected, srContext.LOGTAG, "Unable to delete the ElastiCache cache cluster at this time. " + e.getMessage());
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Unexpected error while deleting the ElastiCache cache cluster", e);
        }
    }

    /**
     * Delete a replication group.
     */
    private void deleteReplicationGroup(DetectedSecurityRisk detected, SecurityRiskContext srContext, AmazonElastiCache client, Arn arn) {
        try {
            client.deleteReplicationGroup(new DeleteReplicationGroupRequest().withReplicationGroupId(arn.getResource().getResource()));
            setSuccess(detected, srContext.LOGTAG, "ElastiCache replication group deleted.");
        }
        catch (InvalidReplicationGroupStateException e) {
            setPostponed(detected, srContext.LOGTAG, "Unable to delete the ElastiCache replication group at this time. " + e.getMessage());
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Unexpected error while deleting the ElastiCache replication group", e);
        }
    }

    /**
     * Delete a global replication group (aka, Redis Global Datastore).
     * This is a much more involved process than the two above because there are associated primary\
     * and secondary replication groups that have to be dealt with first.
     */
    private void deleteGlobalReplicationGroup(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext, AmazonElastiCache primaryClient, Arn arn) {
        String globalReplicationGroupId = arn.getResource().getResource();

        // gather info about the members
        String primaryReplicationGroupId = null;
        String secondaryReplicationGroupId = null;
        String secondaryReplicationGroupRegion = null;

        DescribeGlobalReplicationGroupsRequest describeGlobalReplicationGroupsRequest = new DescribeGlobalReplicationGroupsRequest()
                .withGlobalReplicationGroupId(globalReplicationGroupId)
                .withShowMemberInfo(true);
        DescribeGlobalReplicationGroupsResult describeGlobalReplicationGroupsResult;

        do {
            try {
                describeGlobalReplicationGroupsResult = primaryClient.describeGlobalReplicationGroups(describeGlobalReplicationGroupsRequest);
            }
            catch (Exception e) {
                setError(detected, srContext.LOGTAG, "Unexpected error while describing ElastiCache global replication groups", e);
                return;
            }

            // it would be odd to have more than one GlobalReplicationGroup but the API allows for there to be 0 and a set marker
            if (describeGlobalReplicationGroupsResult.getGlobalReplicationGroups().size() > 1) {
                setError(detected, srContext.LOGTAG, "Unexpected number of ElastiCache global replication groups: "
                        + describeGlobalReplicationGroupsResult.getGlobalReplicationGroups().size());
                return;
            }
            else if (describeGlobalReplicationGroupsResult.getGlobalReplicationGroups().size() == 1) {
                GlobalReplicationGroup globalReplicationGroup = describeGlobalReplicationGroupsResult.getGlobalReplicationGroups().get(0);
                for (GlobalReplicationGroupMember globalReplicationGroupMember : globalReplicationGroup.getMembers()) {
                    if (globalReplicationGroupMember.getRole().equals("PRIMARY")) {
                        primaryReplicationGroupId = globalReplicationGroupMember.getReplicationGroupId();
                    }
                    else if (globalReplicationGroupMember.getRole().equals("SECONDARY")) {
                        secondaryReplicationGroupId = globalReplicationGroupMember.getReplicationGroupId();
                        secondaryReplicationGroupRegion = globalReplicationGroupMember.getReplicationGroupRegion();
                    }
                }
            }

            describeGlobalReplicationGroupsRequest.setMarker(describeGlobalReplicationGroupsResult.getMarker());
        } while (describeGlobalReplicationGroupsResult.getMarker() != null);

        /*
         * if there's still a secondary replication group in the global datastore
         * 1) disassociate it from the global replication group
         * 2) that will cause it to change state so wait for it to become available
         * 3) then delete it
         */
        if (secondaryReplicationGroupId != null && secondaryReplicationGroupRegion != null) {
            disassociateGlobalReplicationGroup(srContext, primaryClient, globalReplicationGroupId, secondaryReplicationGroupId, secondaryReplicationGroupRegion);

            try {
                AmazonElastiCache secondaryClient;
                secondaryClient = getClient(accountId, secondaryReplicationGroupRegion, srContext.getMetricCollector(), AmazonElastiCacheClient.class);
                waitForDisassociate(srContext, secondaryClient, globalReplicationGroupId, secondaryReplicationGroupId);
                secondaryClient.deleteReplicationGroup(new DeleteReplicationGroupRequest().withReplicationGroupId(secondaryReplicationGroupId));
            }
            catch (Exception e) {
                String msg = "Ignoring error remediating the ElastiCache secondary in the global replication group (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }
        }

        /*
         * then, before the primary replication group can be deleted, there is kind of a "disassociate" step for
         * the primary where the global replication group is deleted but the primary is retained.
         * this step may have to be retried several times because disassociation of the secondary modifies the
         * global replication group which has to become ready again.
         */
        deleteGlobalReplicationGroupRetainPrimary(srContext, primaryClient, globalReplicationGroupId);

        /*
         * if there's still a primary replication group in the global datastore
         * 1) wait for it to become available
         * 2) then delete it
         */
        if (primaryReplicationGroupId != null) {
            try {
                waitForDisassociate(srContext, primaryClient, globalReplicationGroupId, primaryReplicationGroupId);
                primaryClient.deleteReplicationGroup(new DeleteReplicationGroupRequest().withReplicationGroupId(primaryReplicationGroupId));
            }
            catch (Exception e) {
                String msg = "Ignoring error remediating the ElastiCache primary in the global replication group (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }
        }

        setSuccess(detected, srContext.LOGTAG, "ElastiCache global datastore deleted.");
    }

    /**
     * When a global datastore is first being created, it looks like the primary and secondary
     * are created first as regular replication groups.  At the same time, the global datastore is
     * being created.  Once it's ready, the primary and secondary are modified to become part
     * of the global datastore.  The time for the cluster to become completely ready seems to
     * depend on the number of shards and replicas.
     *
     * So, given that this (disassociating the secondary) is the first step to remediation,
     * and could be happening moments after the start of the creation of the global datastore,
     * allow for extra time for the operation to succeed.
     */
    private void disassociateGlobalReplicationGroup(SecurityRiskContext srContext, AmazonElastiCache primaryClient, String globalReplicationGroupId,
                                                    String secondaryReplicationGroupId, String secondaryReplicationGroupRegion) {
        long waitingUntil = System.currentTimeMillis() + (TEN_MINUTES_IN_MILLIS + TEN_MINUTES_IN_MILLIS);

        while (System.currentTimeMillis() < waitingUntil) {
            try {
                DisassociateGlobalReplicationGroupRequest disassociateGlobalReplicationGroup = new DisassociateGlobalReplicationGroupRequest()
                        .withGlobalReplicationGroupId(globalReplicationGroupId)
                        .withReplicationGroupId(secondaryReplicationGroupId)
                        .withReplicationGroupRegion(secondaryReplicationGroupRegion);
                primaryClient.disassociateGlobalReplicationGroup(disassociateGlobalReplicationGroup);

                return;
            }
            catch (InvalidGlobalReplicationGroupStateException e) {
                String msg = "Unable to disassociate the ElastiCache secondary from the global replication group (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }
            catch (Exception e) {
                String msg = "Ignoring unexpected error while disassociate the ElastiCache secondary from the global replication group (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }

            sleepForSeconds(29);
        }
    }

    /**
     * The global replication group is deleted but the primary must be retained and handled later.
     * It is not possible to delete primary at the same time as the global replication group.
     */
    private void deleteGlobalReplicationGroupRetainPrimary(SecurityRiskContext srContext, AmazonElastiCache primaryClient, String globalReplicationGroupId) {
        long waitingUntil = System.currentTimeMillis() + TEN_MINUTES_IN_MILLIS;

        while (System.currentTimeMillis() < waitingUntil) {
            try {
                DeleteGlobalReplicationGroupRequest deleteGlobalReplicationGroupRequest = new DeleteGlobalReplicationGroupRequest()
                        .withGlobalReplicationGroupId(globalReplicationGroupId)
                        .withRetainPrimaryReplicationGroup(true);
                primaryClient.deleteGlobalReplicationGroup(deleteGlobalReplicationGroupRequest);

                return;
            }
            catch (InvalidGlobalReplicationGroupStateException e) {
                String msg = "Ignoring error while deleting the ElastiCache global datastore (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }
            catch (Exception e) {
                String msg = "Ignoring unexpected error while deleting the ElastiCache global datastore (" + globalReplicationGroupId + ")."
                        + " The exception is: " + e.getMessage();
                logger.info(srContext.LOGTAG + msg);
            }

            sleepForSeconds(32);
        }
    }

    /**
     * When certain operations are performed on the global replication group, it makes modifications to the
     * primary and/or secondary replication groups.  We have to wait for those to modifications to finish before
     * we can continue.
     */
    private void waitForDisassociate(SecurityRiskContext srContext, AmazonElastiCache client,
                                     String globalReplicationGroupId, String replicationGroupId) {
        long waitingUntil = System.currentTimeMillis() + TEN_MINUTES_IN_MILLIS;

        waitingLoop:
        while (System.currentTimeMillis() < waitingUntil) {
            DescribeReplicationGroupsRequest describeReplicationGroupsRequest = new DescribeReplicationGroupsRequest()
                    .withReplicationGroupId(replicationGroupId);
            DescribeReplicationGroupsResult describeReplicationGroupsResult;
            ReplicationGroup replicationGroup = null;

            findReplicationGroup:
            do {
                try {
                    describeReplicationGroupsResult = client.describeReplicationGroups(describeReplicationGroupsRequest);
                }
                catch (Exception e) {
                    String msg = "Ignoring unexpected error while describing the ElastiCache replication group."
                            + " The exception is: " + e.getMessage();
                    logger.info(srContext.LOGTAG + msg);
                    continue waitingLoop;
                }

                for (ReplicationGroup rg : describeReplicationGroupsResult.getReplicationGroups()) {
                    if (rg.getGlobalReplicationGroupInfo().getGlobalReplicationGroupId() != null
                            && rg.getGlobalReplicationGroupInfo().getGlobalReplicationGroupId().equals(globalReplicationGroupId)) {
                        replicationGroup = rg;
                        break findReplicationGroup;
                    }
                    else if (rg.getReplicationGroupId().equals(replicationGroupId)) {
                        replicationGroup = rg;
                        break findReplicationGroup;
                    }
                }

                describeReplicationGroupsRequest.setMarker(describeReplicationGroupsResult.getMarker());
            } while (describeReplicationGroupsResult.getMarker() != null);

            if (replicationGroup == null)
                return; // odd that it's gone
            if (replicationGroup.getGlobalReplicationGroupInfo().getGlobalReplicationGroupId() == null
                    && replicationGroup.getStatus().equals("available"))
                return;

            sleepForSeconds(31);
        }
    }

    /**
     * Simple wrapper because we have to catch interrupts.
     * @param seconds number of seconds to sleep
     */
    private void sleepForSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        }
        catch (InterruptedException e) {
            /* ignore */
        }
    }
}
