package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.util.List;

/**
 * Detect DB instances running SQL Server Express Edition.
 * Encryption at-rest is not available for Express Edition so they are identified as a security risk.
 *
 * @see edu.emory.it.services.srd.remediator.RdsHipaaIneligibleSqlServerDatabaseRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsHipaaIneligibleSqlServerDatabaseDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonRDS client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);

                String describeDBInstancesRequestMarker = null;
                do {
                    DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest()
                            .withMaxRecords(100);
                    // deal with pagination
                    if (describeDBInstancesRequestMarker != null)
                        describeDBInstancesRequest.setMarker(describeDBInstancesRequestMarker);
                    DescribeDBInstancesResult describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest);
                    describeDBInstancesRequestMarker = describeDBInstancesResult.getMarker();

                    List<DBInstance> dbInstances = describeDBInstancesResult.getDBInstances();
                    for (DBInstance dbInstance : dbInstances) {
                        // this detector is only for SQL Server Express Edition
                        if (dbInstance.getEngine().equals("sqlserver-ex")) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsHipaaIneligibleSqlServerDatabase, dbInstance.getDBInstanceArn(),
                                    new SrdNameValuePair("DBInstanceIdentifier", dbInstance.getDBInstanceIdentifier()),
                                    new SrdNameValuePair("DBInstanceStatus", dbInstance.getDBInstanceStatus()));
                        }
                    }
                } while (describeDBInstancesRequestMarker != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsHipaaIneligibleSqlServerDatabase,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "RdsHipaaIneligibleSqlServerDatabase";
    }
}
