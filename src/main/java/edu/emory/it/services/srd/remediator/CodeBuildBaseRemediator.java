package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.codebuild.AWSCodeBuild;
import com.amazonaws.services.codebuild.AWSCodeBuildClient;
import com.amazonaws.services.codebuild.model.DeleteProjectRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates CodeBuild project by deleting the project<br>
 */
public class CodeBuildBaseRemediator extends AbstractSecurityRiskRemediator {
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext, DetectionType detectionType) {
        if (remediatorPreConditionFailed(detected, "arn:aws:codebuild:", srContext.LOGTAG, detectionType))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];
        String projectName = arn[5].replace("project/", "");

        final AWSCodeBuild codeBuild;
        try {
            codeBuild = getClient(accountId, region, srContext.getMetricCollector(), AWSCodeBuildClient.class);
            codeBuild.deleteProject(new DeleteProjectRequest().withName(projectName));
            setSuccess(detected, srContext.LOGTAG, "CodeBuild Project '" + projectName + "' has been deleted.");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "CodeBuild Project '" + projectName + "'", e);
        }
    }
}
