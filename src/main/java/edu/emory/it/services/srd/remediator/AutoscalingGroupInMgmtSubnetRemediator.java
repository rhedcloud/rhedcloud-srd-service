package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.AutoScalingGroup;
import com.amazonaws.services.autoscaling.model.DeleteAutoScalingGroupRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Remediates Auto Scaling groups that are in a disallowed subnet<br>
 *
 * <p>The remediator deletes the Auto Scaling group that is launched into a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.AutoscalingGroupInMgmtSubnetDetector the detector
 */
public class AutoscalingGroupInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:autoscaling:", srContext.LOGTAG, DetectionType.AutoscalingGroupInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 8, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String groupName = arn[7].replace("autoScalingGroupName/", "");
        String region = arn[3];

        final AmazonEC2 ec2;
        final AmazonAutoScaling asClient;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
            asClient = getClient(accountId, region, srContext.getMetricCollector(), AmazonAutoScalingClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Auto Scaling group '" + groupName + "'", e);
            return;
        }

        DescribeAutoScalingGroupsRequest request = new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(groupName);
        DescribeAutoScalingGroupsResult result = asClient.describeAutoScalingGroups(request);


        if (result.getAutoScalingGroups() == null || result.getAutoScalingGroups().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Auto Scaling group '" + groupName + "' cannot be found. It may have already been deleted.");
            return;
        }


        AutoScalingGroup theGroup = result.getAutoScalingGroups().get(0);
        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

        if ("Delete in progress".equals(theGroup.getStatus())) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Auto Scaling group '" + groupName + "' is deleting.  No change needed.");
            return;
        }

        if (theGroup.getVPCZoneIdentifier() == null || theGroup.getVPCZoneIdentifier().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Auto Scaling group '" + groupName + "' is not associated with any subnets.  No change needed.");
            return;
        }


        boolean containsDisallowedSubnet = false;
        String[] VpcZoneIds = theGroup.getVPCZoneIdentifier().split(",");
        for (String VpcZoneId : VpcZoneIds) {
            if (disallowedSubnets.contains(VpcZoneId)) {
                containsDisallowedSubnet = true;
                break;
            }
        }

        if (!containsDisallowedSubnet) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Auto Scaling group '" + groupName + "' is not in a disallowed subnet.  No change needed.");
            return;
        }

        //remediate
        DeleteAutoScalingGroupRequest deleteAutoScalingGroupRequest = new DeleteAutoScalingGroupRequest().withForceDelete(true).withAutoScalingGroupName(groupName);
        asClient.deleteAutoScalingGroup(deleteAutoScalingGroupRequest);

        setSuccess(detected, srContext.LOGTAG, "Auto Scaling group '" + groupName + "' has been deleted");
    }
}
