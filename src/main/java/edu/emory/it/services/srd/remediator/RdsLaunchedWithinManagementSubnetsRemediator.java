package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by deleting the DB instance.<br>
 * The snapshot identifier prefix is "rds-launched-in-disallowed-subnet-".
 *
 * <p>See the
 * {@linkplain edu.emory.it.services.srd.remediator.AbstractRdsRemediator#remediateDBInstance(DetectedSecurityRisk, String, DetectionType, String, RemediationAction, SecurityRiskContext) remediateDBInstance}
 * method for addition information.</p>
 *
 * @see edu.emory.it.services.srd.detector.RdsLaunchedWithinManagementSubnetsDetector the detector
 */
public class RdsLaunchedWithinManagementSubnetsRemediator extends AbstractRdsRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediateDBInstance(detected, accountId,
                DetectionType.RdsLaunchedWithinManagementSubnets,
                "rds-launched-in-disallowed-subnet-",
                AbstractRdsRemediator.RemediationAction.DELETE,
                srContext);
    }
}
