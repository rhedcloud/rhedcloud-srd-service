package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.DistributionList;
import com.amazonaws.services.cloudfront.model.DistributionSummary;
import com.amazonaws.services.cloudfront.model.ListDistributionsRequest;
import com.amazonaws.services.cloudfront.model.ListDistributionsResult;
import com.amazonaws.services.cloudfront.model.Origin;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects cloud distributions backed by S3 buckets (static site) and has no WebAcl attached<br>
 *
 * @see edu.emory.it.services.srd.remediator.CloudFrontStaticSiteACLRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CloudFrontStaticSiteACLDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.CloudFrontStaticSiteACL,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        ListDistributionsRequest listDistributionsRequest = new ListDistributionsRequest();
        DistributionList distributionList;

        do {
            try {
                ListDistributionsResult listDistributionsResult = cloudFront.listDistributions(listDistributionsRequest);
                distributionList = listDistributionsResult.getDistributionList();
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CloudFrontStaticSiteACL, srContext.LOGTAG,
                        "Error listing CloudFront distributions", e);
                return;
            }

            for (DistributionSummary distributionSummary : distributionList.getItems()) {
                if (distributionSummary.getWebACLId() == null || distributionSummary.getWebACLId().isEmpty()) {
                    for (Origin origin : distributionSummary.getOrigins().getItems()) {
                        if (origin.getS3OriginConfig() != null) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.CloudFrontStaticSiteACL, distributionSummary.getARN());
                            break;
                        }
                    }
                }
            }

            if (distributionList.getIsTruncated())
                listDistributionsRequest.setMarker(distributionList.getNextMarker());
        } while (distributionList.getIsTruncated());
    }

    @Override
    public String getBaseName() {
        return "CloudFrontStaticSiteACL";
    }
}
