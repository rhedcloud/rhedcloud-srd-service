package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.FunctionConfiguration;
import com.amazonaws.services.lambda.model.ListFunctionsRequest;
import com.amazonaws.services.lambda.model.ListFunctionsResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Detect Lambda functions in a disallowed subnet.
 *
 * @see edu.emory.it.services.srd.remediator.LambdaResidesWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class LambdaResidesWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonEC2 ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                final AWSLambda lambdaClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSLambdaClient.class);

                // collect the IDs of the disallowed subnets
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);

                ListFunctionsRequest listFunctionsRequest = new ListFunctionsRequest();
                ListFunctionsResult listFunctionsResult;
                do {
                    listFunctionsResult = lambdaClient.listFunctions(listFunctionsRequest);

                    for (FunctionConfiguration fc : listFunctionsResult.getFunctions()) {
                        if (fc.getVpcConfig() != null) {
                            for (String subnetId : fc.getVpcConfig().getSubnetIds()) {
                                if (disallowedSubnets.contains(subnetId)) {
                                    addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                            DetectionType.LambdaResidesWithinManagementSubnets,
                                            fc.getFunctionArn());
                                    break;
                                }
                            }
                        }
                    }

                    listFunctionsRequest.setMarker(listFunctionsResult.getNextMarker());
                } while (listFunctionsResult.getNextMarker() != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.LambdaResidesWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "LambdaResidesWithinManagementSubnets";
    }
}
