package edu.emory.it.services.srd.provider;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionQuerySpecification;
import org.openeai.config.AppConfig;

import java.util.Collection;


public interface SecurityRiskDetectionQueryProvider {
    /**
     * Initialize the provider.
     * <P>
     *
     * @param aConfig, an AppConfig object with all this provider needs.
     * <P>
     * @throws InstantiationException with details of the initialization error.
     */
    void init(AppConfig aConfig) throws InstantiationException;

    /**
     * Query for the validity of a security risk detection object.
     * <P>
     *
     * @param spec query spec
     * @return SecurityRiskDetection, list of detect objects for the id.
     * <P>
     * @throws ProviderException with details of the providing the list.
     */
    Collection<SecurityRiskDetection> query(SecurityRiskDetectionQuerySpecification spec) throws ProviderException;
}
