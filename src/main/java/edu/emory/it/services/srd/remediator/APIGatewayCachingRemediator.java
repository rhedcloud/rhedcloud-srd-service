package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClient;
import com.amazonaws.services.apigateway.model.GetStageRequest;
import com.amazonaws.services.apigateway.model.GetStageResult;
import com.amazonaws.services.apigateway.model.NotFoundException;
import com.amazonaws.services.apigateway.model.PatchOperation;
import com.amazonaws.services.apigateway.model.UpdateStageRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates API Gateway where caching is enabled<br>
 *
 * <p>For HIPAA compliance class accounts</p>
 *
 * <p>Security: Enforce<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.APIGatewayCachingDetector the detector
 */
public class APIGatewayCachingRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:apigateway:", srContext.LOGTAG, DetectionType.ApiGatewayCaching))
            return;
        // like arn:aws:apigateway:us-east-1:123456789012:onq4ydr730/api-deployment-stage
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String[] apiIdAndStageName =  arn[5].split("/");
        String apiId = apiIdAndStageName[0];
        String stageName = apiIdAndStageName[1];
        String region = arn[3];

        final AmazonApiGateway gateway;
        try {
            gateway = getClient(accountId, region, srContext.getMetricCollector(), AmazonApiGatewayClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "REST API id '" + apiId + "' with stage name '" + stageName + "'", e);
            return;
        }

        GetStageRequest getStageRequest = new GetStageRequest()
                .withRestApiId(apiId)
                .withStageName(stageName);
        GetStageResult getStageResult;
        try {
            getStageResult = gateway.getStage(getStageRequest);
        } catch (NotFoundException e) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "REST API id '" + apiId + "' with stage name '" + stageName + "' cannot be found.");
            return;
        }

        if (!getStageResult.getCacheClusterEnabled()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "REST API id '" + apiId + "' with stage name '" + stageName + "' already has caching disabled.");
            return;
        }

        UpdateStageRequest updateStageRequest = new UpdateStageRequest()
                .withRestApiId(apiId)
                .withStageName(stageName)
                .withPatchOperations(
                        new PatchOperation().withOp("replace").withPath("/cacheClusterEnabled").withValue("false"));
        gateway.updateStage(updateStageRequest);

        setSuccess(detected, srContext.LOGTAG,
                "REST API id '" + apiId + "' with stage name '" + stageName + "' has been updated to disable caching");
    }
}
