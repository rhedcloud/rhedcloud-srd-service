package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBCluster;
import com.amazonaws.services.rds.model.DBClusterMember;
import com.amazonaws.services.rds.model.DeleteDBClusterRequest;
import com.amazonaws.services.rds.model.DeleteDBInstanceRequest;
import com.amazonaws.services.rds.model.DeleteGlobalClusterRequest;
import com.amazonaws.services.rds.model.DescribeDBClustersRequest;
import com.amazonaws.services.rds.model.DescribeDBClustersResult;
import com.amazonaws.services.rds.model.DescribeGlobalClustersRequest;
import com.amazonaws.services.rds.model.DescribeGlobalClustersResult;
import com.amazonaws.services.rds.model.GlobalCluster;
import com.amazonaws.services.rds.model.GlobalClusterMember;
import com.amazonaws.services.rds.model.InvalidDBClusterStateException;
import com.amazonaws.services.rds.model.InvalidDBInstanceStateException;
import com.amazonaws.services.rds.model.ModifyDBClusterRequest;
import com.amazonaws.services.rds.model.ModifyDBInstanceRequest;
import com.amazonaws.services.rds.model.ModifyGlobalClusterRequest;
import com.amazonaws.services.rds.model.StartDBClusterRequest;
import com.amazonaws.services.rds.model.StopDBClusterRequest;
import com.amazonaws.services.rds.model.StopDBInstanceRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Remediation utilities for RDS instances.
 */
public class AbstractRdsRemediator extends AbstractSecurityRiskRemediator {
    public enum RemediationAction {DELETE, STOP}

    /**
     * Remediate an RDS DB instance or cluster.<br>
     * There are two actions that can be taken: Delete or Stop the database.
     *
     * <p>For either stopping or deleting, a snapshot will be taken.  The name of the snapshot is the
     * snapshotIdentifierPrefix with the current date appended to the end.  For example, if the snapshot is
     * taken just before noon with the snapshotIdentifierPrefix as "mysnapshot-" then the snapshot
     * will be called "mysnapshot-2018-06-11-115959".  If the remediationAction is DELETE then this is
     * considered the final snapshot.</p>
     *
     * <p>Depending on the status of the database, remediation may be postponed or not be needed at all.  Primarily,
     * this is done because remediation actions like modifying or stopping will not work when the database is in
     * certain states.
     * For example, if the remediationAction is STOP and the status of the instance is <code>stopping</code>
     * then remediation is postponed until the instance reaches a final status - typically <code>stopped</code>.
     * See <a href="https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Status.html">DB Instance
     * Status</a> for an overview of the DB instances statuses.</p>
     *
     * <p>Specifically, remediation will be postponed if the status of the DB instance is one of <code>creating</code>,
     * <code>modifying</code>, <code>starting</code>, <code>rebooting</code>, <code>backing-up</code>,
     * <code>deleting</code>, <code>stopping</code>.<br>
     * There are several other states, that are not enumerated below in the code, that will prevent the remediation
     * from happening at this time.  They will be caught as state-exceptions and remediation will be postponed.<br>
     * Remediation is not necessary if the remediationAction is STOP and the status of the instance is
     * <code>stopped</code>.</p>
     *
     * <p>Note: some databases can not be stopped (one example being, Amazon RDS for SQL Server DB
     * instance in a Multi-AZ configuration) and remediation will fail if remediationAction is STOP.
     * The remediationResult will provide details as to why remediation failed.</p>
     *
     * @param detected details of the detected security risk
     * @param accountId account ID
     * @param detectionType Detection type
     * @param snapshotIdentifierPrefix prefix for snapshot identifier
     * @param remediationAction DELETE or STOP
     * @param srContext Security Risk Context
     */
    public void remediateDBInstance(DetectedSecurityRisk detected,
                                    String accountId, DetectionType detectionType, String snapshotIdentifierPrefix,
                                    RemediationAction remediationAction, SecurityRiskContext srContext) {

        if (remediatorPreConditionFailed(detected, "arn:aws:rds:", srContext.LOGTAG, detectionType))
            return;
        // like arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier
        //  or  arn:aws:rds:us-east-1:250049456440:cluster:Cluster_Identifier
        //  or  arn:aws:rds::250049456440:global-cluster:Global_Cluster_Identifier
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null)
            return;

        // instance/cluster information comes from the detector
        String instanceIdentifier = detected.getProperties().getProperty("DBInstanceIdentifier");
        String clusterIdentifier = detected.getProperties().getProperty("DBClusterIdentifier");
        String globalClusterIdentifier = detected.getProperties().getProperty("DBGlobalClusterIdentifier");
        String region = arn.getRegion();  // global clusters don't have a region

        String dbResourceType = arn.getResource().getResourceType();
        String dbStatus;
        String rdsThing;
        switch (dbResourceType) {
            case "db":
                dbStatus = detected.getProperties().getProperty("DBInstanceStatus");
                rdsThing = "RDS db " + instanceIdentifier;
                break;
            case "cluster":
                dbStatus = detected.getProperties().getProperty("DBClusterStatus");
                rdsThing = "RDS cluster " + clusterIdentifier;
                break;
            case "global-cluster":
                dbStatus = detected.getProperties().getProperty("DBGlobalClusterStatus");
                rdsThing = "RDS global cluster " + globalClusterIdentifier;
                break;
            default:
                setError(detected, srContext.LOGTAG, "Unsupported RDS resource type " + dbResourceType);
                return;
        }

        // understanding the status of an instance or cluster
        // https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Status.html
        // https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.Status.html
        // https://docs.aws.amazon.com/documentdb/latest/developerguide/monitoring_docdb-cluster_status.html

        switch (dbStatus) {
            case "creating":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " creation in-progress");
                break;

            case "modifying":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " modification in-progress");
                break;

            case "starting":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " start up in-progress");
                break;

            case "rebooting":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " reboot in-progress");
                break;

            case "backing-up":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " backup in-progress");
                break;

            case "deleting":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " deletion in-progress");
                break;

            case "stopping":
                setPostponed(detected, srContext.LOGTAG, rdsThing + " stopping in-progress");
                break;

            case "stopped":
                if (remediationAction == RemediationAction.STOP) {
                    setIgnoreRisk(detected, rdsThing + " already stopped");
                    break;
                }
                if (remediationAction == RemediationAction.DELETE && dbResourceType.equals("cluster")) {
                    // a cluster can not be deleted when it's in a stopped state
                    // trying would result in: InvalidDBClusterStateException: Db cluster blah is in stopped state
                    // remediate by putting the cluster in the correct state for deletion, which means starting it
                    // once it has become available one of the next remediations will fix it
                    try {
                        AmazonRDS client = getClient(accountId, region, srContext.getMetricCollector(), AmazonRDSClient.class);
                        StartDBClusterRequest startDBClusterRequest = new StartDBClusterRequest()
                                .withDBClusterIdentifier(clusterIdentifier);
                        client.startDBCluster(startDBClusterRequest);
                        setPostponed(detected, srContext.LOGTAG, rdsThing + " is starting so it can be deleted");
                        break;
                    }
                    catch (Exception e) {
                        setError(detected, srContext.LOGTAG, "Failed to start " + rdsThing + " for deletion", e);
                        break;
                    }
                }

                // otherwise - fall through

            default:
                // snapshot identifiers are case-insensitive
                String snapshotIdentifier = snapshotIdentifierPrefix
                        + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HHmmss"));
                AmazonRDS client;

                try {
                    if (region.isEmpty())
                        client = getClient(accountId, srContext.getMetricCollector(), AmazonRDSClient.class);
                    else
                        client = getClient(accountId, region, srContext.getMetricCollector(), AmazonRDSClient.class);
                }
                catch (Exception e) {
                    setError(detected, srContext.LOGTAG, "Failed to get client for " + rdsThing, e);
                    break;
                }

                if (remediationAction == RemediationAction.STOP) {
                    try {
                        /*
                         * some database configuration can't be stopped (ie, Amazon RDS for SQL Server DB instance
                         *  in a Multi-AZ configuration, or Aurora global databases).  More information is available
                         *  https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-cluster-stop-start.html#aurora-cluster-stop-limitations
                         * so those will fail and we'll report the problem as a remediation failure
                         */
                        switch (dbResourceType) {
                            case "db":
                                StopDBInstanceRequest stopDBInstanceRequest = new StopDBInstanceRequest()
                                        .withDBInstanceIdentifier(instanceIdentifier)
                                        .withDBSnapshotIdentifier(snapshotIdentifier);
                                client.stopDBInstance(stopDBInstanceRequest);

                                setSuccess(detected, srContext.LOGTAG, rdsThing + " stopped with snapshot " + snapshotIdentifier);
                                break;
                            case "cluster":
                                StopDBClusterRequest stopDBClusterRequest = new StopDBClusterRequest()
                                        .withDBClusterIdentifier(clusterIdentifier); // snapshot not possible
                                client.stopDBCluster(stopDBClusterRequest);

                                setSuccess(detected, srContext.LOGTAG, rdsThing + " stopped");
                                break;
                            case "global-cluster":
                                setError(detected, srContext.LOGTAG, "It is not possible to stop " + rdsThing);
                                break;
                        }
                    }
                    catch (InvalidDBClusterStateException | InvalidDBInstanceStateException e) {
                        // special case - some clusters (aurora serverless postgres) can't be stopped
                        // but otherwise, the cluster just isn't in the correct state to stop
                        if (e.getMessage().contains("is not supported for these configurations"))
                            setError(detected, srContext.LOGTAG, "It is not possible to stop " + rdsThing, e);
                        else
                            setPostponed(detected, srContext.LOGTAG, rdsThing + " is in the wrong state to stop");
                        break;
                    }
                    catch (Exception e) {
                        setError(detected, srContext.LOGTAG, "Failed to stop " + rdsThing, e);
                        break;
                    }
                }

                if (remediationAction == RemediationAction.DELETE) {
                    try {
                        switch (dbResourceType) {
                            case "db":
                                deleteInstance(client, instanceIdentifier, snapshotIdentifier);
                                setSuccess(detected, srContext.LOGTAG, rdsThing + " deleted with snapshot " + snapshotIdentifier);
                                break;
                            case "cluster":
                                deleteCluster(client, clusterIdentifier, snapshotIdentifier);
                                setSuccess(detected, srContext.LOGTAG, rdsThing + " deleted with snapshot " + snapshotIdentifier);
                                break;
                            case "global-cluster":
                                deleteGlobalCluster(client, globalClusterIdentifier, snapshotIdentifier);
                                setSuccess(detected, srContext.LOGTAG, rdsThing + " deleted with snapshot " + snapshotIdentifier);
                                break;
                        }
                    }
                    catch (InvalidDBClusterStateException | InvalidDBInstanceStateException e) {
                        setPostponed(detected, srContext.LOGTAG, rdsThing + " is in the wrong state to delete");
                        break;
                    }
                    catch (Exception e) {
                        setError(detected, srContext.LOGTAG, "Failed to delete " + rdsThing, e);
                        break;
                    }
                }
                break;
        }
    }

    private void deleteCluster(AmazonRDS client, String clusterIdentifier, String snapshotIdentifier) {
        // disable deletion protection
        ModifyDBClusterRequest modifyDBClusterRequest = new ModifyDBClusterRequest()
                .withDBClusterIdentifier(clusterIdentifier)
                .withDeletionProtection(false)
                .withApplyImmediately(true);
        client.modifyDBCluster(modifyDBClusterRequest);

        // find the members of the cluster so they can be deleted
        // reader instances will be deleted first to avoid failover
        DescribeDBClustersRequest describeDBClustersRequest = new DescribeDBClustersRequest()
                .withDBClusterIdentifier(clusterIdentifier);
        DescribeDBClustersResult describeDBClustersResult;
        do {
            describeDBClustersResult = client.describeDBClusters(describeDBClustersRequest);

            for (DBCluster dbCluster : describeDBClustersResult.getDBClusters()) {
                if (clusterIdentifier.equals(dbCluster.getDBClusterIdentifier())) {
                    List<DBClusterMember> dbClusterMembers = dbCluster.getDBClusterMembers();
                    if (dbClusterMembers != null && !dbClusterMembers.isEmpty()) {
                        DeleteDBInstanceRequest deleteDBInstanceRequest = new DeleteDBInstanceRequest();

                        // delete reader instances first to avoid failover
                        for (DBClusterMember dbClusterMember : dbClusterMembers) {
                            if (!dbClusterMember.isClusterWriter()) {
                                deleteDBInstanceRequest.setDBInstanceIdentifier(dbClusterMember.getDBInstanceIdentifier());
                                client.deleteDBInstance(deleteDBInstanceRequest);
                            }
                        }
                        // delete writer instances last
                        for (DBClusterMember dbClusterMember : dbClusterMembers) {
                            if (dbClusterMember.isClusterWriter()) {
                                deleteDBInstanceRequest.setDBInstanceIdentifier(dbClusterMember.getDBInstanceIdentifier());
                                client.deleteDBInstance(deleteDBInstanceRequest);
                            }
                        }
                    }
                }
            }

            describeDBClustersRequest.setMarker(describeDBClustersResult.getMarker());
        } while (describeDBClustersResult.getMarker() != null);

        // finally, delete the cluster with a final snapshot
        DeleteDBClusterRequest deleteDBClusterRequest = new DeleteDBClusterRequest()
                .withDBClusterIdentifier(clusterIdentifier)
                .withFinalDBSnapshotIdentifier(snapshotIdentifier);
        client.deleteDBCluster(deleteDBClusterRequest);
    }

    private void deleteGlobalCluster(AmazonRDS client, String globalClusterIdentifier, String snapshotIdentifier) {
        // disable deletion protection for the global cluster
        ModifyGlobalClusterRequest modifyGlobalClusterRequest = new ModifyGlobalClusterRequest()
                .withGlobalClusterIdentifier(globalClusterIdentifier)
                .withDeletionProtection(false);
        client.modifyGlobalCluster(modifyGlobalClusterRequest);

        // find the secondary clusters within the global database cluster so they can be deleted
        // currently, the javadoc for GlobalCluster says there is only one, but handle as many as we're given
        DescribeGlobalClustersRequest describeGlobalClustersRequest = new DescribeGlobalClustersRequest();
        DescribeGlobalClustersResult describeGlobalClustersResult;
        do {
            describeGlobalClustersResult = client.describeGlobalClusters(describeGlobalClustersRequest);

            for (GlobalCluster globalCluster : describeGlobalClustersResult.getGlobalClusters()) {
                if (globalClusterIdentifier.equals(globalCluster.getGlobalClusterIdentifier())) {
                    if (globalCluster.getGlobalClusterMembers() != null) {
                        // delete the secondary clusters with a snapshot
                        for (GlobalClusterMember globalClusterMember : globalCluster.getGlobalClusterMembers()) {
                            String clusterIdentifier = Arn.fromString(globalClusterMember.getDBClusterArn()).getResource().getResource();
                            deleteCluster(client, clusterIdentifier, snapshotIdentifier);
                        }
                    }
                }
            }

            describeGlobalClustersRequest.setMarker(describeGlobalClustersResult.getMarker());
        } while (describeGlobalClustersResult.getMarker() != null);

        // finally, delete the global cluster itself
        DeleteGlobalClusterRequest deleteGlobalClusterRequest = new DeleteGlobalClusterRequest()
                .withGlobalClusterIdentifier(globalClusterIdentifier);
        client.deleteGlobalCluster(deleteGlobalClusterRequest);
    }

    private void deleteInstance(AmazonRDS client, String instanceIdentifier, String snapshotIdentifier) {
        // disable deletion protection
        ModifyDBInstanceRequest modifyDBInstanceRequest = new ModifyDBInstanceRequest()
                .withDBInstanceIdentifier(instanceIdentifier)
                .withDeletionProtection(false)
                .withApplyImmediately(true);
        client.modifyDBInstance(modifyDBInstanceRequest);

        // then, delete the instance with a final snapshot
        DeleteDBInstanceRequest deleteDBInstanceRequest = new DeleteDBInstanceRequest()
                .withDBInstanceIdentifier(instanceIdentifier)
                .withFinalDBSnapshotIdentifier(snapshotIdentifier);
        client.deleteDBInstance(deleteDBInstanceRequest);
    }
}
