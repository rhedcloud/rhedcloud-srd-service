package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CompleteMultipartUploadRequest;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.amazonaws.services.s3.model.CopyPartRequest;
import com.amazonaws.services.s3.model.CopyPartResult;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetBucketEncryptionResult;
import com.amazonaws.services.s3.model.GetObjectMetadataRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PartETag;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.ServerSideEncryptionByDefault;
import com.amazonaws.services.s3.model.ServerSideEncryptionConfiguration;
import com.amazonaws.services.s3.model.ServerSideEncryptionRule;
import com.amazonaws.services.s3.model.SetBucketEncryptionRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Remediates S3 buckets that are unencrypted<br>
 *
 * <p>Enables default encryption on the bucket.  Traverses each object in the bucket and enables encryption</p>
 *
 * <p>Security: HIPAA workloads: Enforce encryption. Non-HIPAA workloads: Maybe. Further discussion. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.S3UnencryptedBucketDetector the detector
 */
public class S3UnencryptedBucketRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:s3:::", srContext.LOGTAG, DetectionType.S3UnencryptedBucket))
            return;
        // like arn:aws:s3:::bucket-name
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        String bucketName =  detected.getAmazonResourceName().replace("arn:aws:s3:::", "").trim();
        if (bucketName.isEmpty()) {
            setError(detected, srContext.LOGTAG,
                    "Security risk input to S3UnencryptedBucketRemediator is missing bucket name");
            return;
        }

        final AmazonS3 s3;
        try {
            s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "S3 Unencrypted Bucket '" + bucketName + "'", e);
            return;
        }

        //get s3 bucket encryption to make sure that it is not currently encrypted
        GetBucketEncryptionResult result = null;
        try {
            result = s3.getBucketEncryption(bucketName);

        } catch ( AmazonS3Exception e) {
            if (e.getStatusCode() != 404) {  // ignore not found exception
                setError(detected, srContext.LOGTAG, "Error connecting to S3 bucket: " + bucketName);
                return;
            }
        }

        ServerSideEncryptionConfiguration encConfig = null;
        if (result != null) {
            encConfig = result.getServerSideEncryptionConfiguration();
        }
        if (result != null && encConfig != null && encConfig.getRules() != null && !encConfig.getRules().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    bucketName + " is already encrypted");
            return;
        }


        encConfig = new ServerSideEncryptionConfiguration()
                .withRules(new ServerSideEncryptionRule()
                        .withApplyServerSideEncryptionByDefault(
                                new ServerSideEncryptionByDefault().withSSEAlgorithm("AES256")));

        SetBucketEncryptionRequest encRequest = new SetBucketEncryptionRequest()
                .withBucketName(bucketName)
                .withServerSideEncryptionConfiguration(encConfig);
        s3.setBucketEncryption(encRequest);

        // There is no change to the encryption of the objects that existed in the bucket before bucket encryption was enabled
        encryptAllFiles(s3, bucketName);

        setSuccess(detected, srContext.LOGTAG, bucketName + " successfully encrypted.");
    }

    private void encryptAllFiles(AmazonS3 s3, String bucketName) {
        ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName);
        ListObjectsV2Result listObjectsV2Result;
        do {
            listObjectsV2Result = s3.listObjectsV2(req);

            for (S3ObjectSummary summary : listObjectsV2Result.getObjectSummaries()) {
                GetObjectMetadataRequest getObjectMetadataRequest = new GetObjectMetadataRequest(bucketName, summary.getKey());
                ObjectMetadata objectMetadata = s3.getObjectMetadata(getObjectMetadataRequest);

                if (objectMetadata.getSSEAlgorithm() == null || objectMetadata.getSSEAlgorithm().isEmpty()) {
                    encryptFile(s3, bucketName, summary.getKey(), objectMetadata.getContentLength());
                }
            }

            req.setContinuationToken(listObjectsV2Result.getNextContinuationToken());
        } while (listObjectsV2Result.isTruncated());
    }

    /*
     * Encrypt all files by copying them since new files will be encrypted (now that the bucket is)
     */
    private void encryptFile(AmazonS3 s3, String bucketName, String fileKey, long fileSize) {
        if (fileKey.startsWith("temp/")) {
            return;
        }

        String tempKey = "temp/" + fileKey;
        boolean isGreaterThan5gb =  (fileSize > (5L * 1024L * 1024L * 1024L));

        if (!isGreaterThan5gb) {
            s3.copyObject(new CopyObjectRequest(bucketName, fileKey, bucketName, tempKey));
            s3.deleteObject(new DeleteObjectRequest(bucketName, fileKey));
            s3.copyObject(new CopyObjectRequest(bucketName, tempKey, bucketName, fileKey));
            s3.deleteObject(new DeleteObjectRequest(bucketName, tempKey));
        }
        else {
            multipartCopy(bucketName, fileKey, bucketName, tempKey, fileSize, s3);
            s3.deleteObject(new DeleteObjectRequest(bucketName, fileKey));

            multipartCopy(bucketName, tempKey, bucketName, fileKey, fileSize, s3);
            s3.deleteObject(new DeleteObjectRequest(bucketName, tempKey));
        }
    }

    private void multipartCopy(String sourceBucketName, String sourceObjectKey, String targetBucketName, String targetObjectKey, long objectSize, AmazonS3 s3) {
        // Create lists to hold copy responses
        List<CopyPartResult> copyResponses = new ArrayList<>();

        // Step 2: Initialize
        InitiateMultipartUploadRequest initiateRequest =
                new InitiateMultipartUploadRequest(targetBucketName, targetObjectKey);

        InitiateMultipartUploadResult initResult = s3.initiateMultipartUpload(initiateRequest);

        // Step 3: Save upload Id.
        String uploadId = initResult.getUploadId();

        long partSize = 5 * (long) Math.pow(2.0, 20.0); // 5 MB
        long bytePosition = 0;
        for (int i = 1; bytePosition < objectSize; i++) {
            // Step 5. Save copy response.
            CopyPartRequest copyRequest = new CopyPartRequest()
                    .withDestinationBucketName(targetBucketName)
                    .withDestinationKey(targetObjectKey)
                    .withSourceBucketName(sourceBucketName)
                    .withSourceKey(sourceObjectKey)
                    .withUploadId(uploadId)
                    .withFirstByte(bytePosition)
                    .withLastByte(((bytePosition + partSize - 1) >= objectSize) ? objectSize - 1 : bytePosition + partSize - 1)
                    .withPartNumber(i);

            copyResponses.add(s3.copyPart(copyRequest));
            bytePosition += partSize;
        }

        CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest(
                targetBucketName, targetObjectKey, uploadId, extractETags(copyResponses));
        s3.completeMultipartUpload(completeRequest);
    }

    // Helper function that constructs ETags.
    private static List<PartETag> extractETags(List<CopyPartResult> responses) {
        List<PartETag> eTags = new ArrayList<>();
        for (CopyPartResult response : responses) {
            eTags.add(new PartETag(response.getPartNumber(), response.getETag()));
        }
        return eTags;
    }
}
