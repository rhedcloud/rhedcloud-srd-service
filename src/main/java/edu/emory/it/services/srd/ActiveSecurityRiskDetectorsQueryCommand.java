package edu.emory.it.services.srd;

import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.objects.resources.Error;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import com.amazon.aws.moa.jmsobjects.security.v1_0.ActiveSecurityRiskDetectors;
import com.openii.openeai.commands.OpeniiRequestCommand;

public class ActiveSecurityRiskDetectorsQueryCommand extends OpeniiRequestCommand implements RequestCommand {
    private static final String LOGTAG = "[ActiveSrdsQueryCommand] ";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(ActiveSecurityRiskDetectorsQueryCommand.class);



    private Document m_responseDoc;  // the primed XML response document
//    
//    private class SoLogger {
//    	void info(String m) { System.out.println(m); }
//    	void debug(String m) { System.out.println(m); }
//    	void warn(String m) { System.out.println(m); }
//    	void fatal(String m) { System.err.println(m); }
//    	void error(String m) { System.err.println(m); }
//    }
//    
//    SoLogger logger = new SoLogger();

    public ActiveSecurityRiskDetectorsQueryCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Initialize response document.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            logger.debug(LOGTAG + " responseDocumentUri: " + getProperties().getProperty("responseDocumentUri"));
            m_responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"), getOutboundXmlValidation());
            if (m_responseDoc == null) {
                String errMsg = "Missing 'responseDocumentUri' property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + " " + errMsg);
                throw new InstantiationException(errMsg);
            }
        }
        catch (XmlDocumentReaderException e) {
            logger.fatal(LOGTAG + " Error initializing the primed documents.");
            throw new InstantiationException(e.getMessage());
        }
        
        try {
        	getAppConfig().getObject("ActiveSecurityRiskDetectors.v1_0");
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'ActiveSecurityRiskDetectors.v1_0' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);        	
        }       
        
        try {
        	getAppConfig().getObject("ActiveSecurityRiskDetectorsQuerySpecification");
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'ActiveSecurityRiskDetectorsQuerySpecification' from AppConfig: The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);        	
        }       
 
        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        long executionStart = System.currentTimeMillis();
        logger.info(LOGTAG + "[execute] - Start message number " + messageNumber);
        try {
			logger.info(LOGTAG + "[execute] - text " + ((TextMessage) aMessage).getText());
		} catch (JMSException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        // Make a local copy of the response document to use in the replies.
        Document localResponseDoc = (Document) m_responseDoc.clone();
        String replyContents;

        // Convert the JMS Message to an XML Document
        logger.info(LOGTAG + " Convert the JMS Message to an XML Document");
        Document inDoc;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        }
        catch (Exception e) {
            String errMsg = "Exception occurred processing input message in " +
                    "org.openeai.jms.consumer.commands.Command.  Exception: " +
                    e.getMessage();
            throw new CommandException(errMsg);
        }
        
        
        logger.info(LOGTAG + " Retrieve text portion of message.");
        // Retrieve text portion of message.
        TextMessage msg = (TextMessage)aMessage;
        try {
            // Clear the message body for the reply, so we do not have to do it later.
            msg.clearBody();
        }
        catch (Exception e) {
            String errMsg = "Error clearing the message body.";
            throw new CommandException(errMsg + ". The exception is: " + e.getMessage());
        }
        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the ControlArea element.
        String msgObject = eControlArea.getAttribute("messageObject").getValue();
        String msgAction = eControlArea.getAttribute("messageAction").getValue();


        // Verify that the message object we are dealing with is an ActiveSecurityRiskDetectors; if not, reply with an error.
        if (!msgObject.equalsIgnoreCase("ActiveSecurityRiskDetectors")) {
            String errType = "application";
            String errCode = "OpenEAI-1001";
            String errDesc = "Unsupported message object: " + msgObject + ". This command expects 'ActiveSecurityRiskDetectors'.";
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal(LOGTAG + " Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        if (!msgAction.equalsIgnoreCase("Query")) {
            // The messageAction is invalid; it is not a query.
            String errType = "application";
            String errCode = "OpenEAI-1003";
            String errDesc = "Unsupported message action: " + msgAction + ". This command only supports 'query'.";
            logger.fatal(LOGTAG + " " + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        }

        // Handle a Query-Request.
        logger.info(LOGTAG + "Handling an ActiveSecurityRiskDetectors.Query-Request message.");
        

        // Serialize the response object and place it into the reply.
        try {
            String[] activeDetectors = getActiveDectectorsFromPropertyConfig();
            ActiveSecurityRiskDetectors activeSecurityRiskDetectors = (ActiveSecurityRiskDetectors) getAppConfig()
            		.getObject("ActiveSecurityRiskDetectors.v1_0");
            localResponseDoc.getRootElement().getChild("DataArea").removeContent();
            for (String activeDetector : activeDetectors) {
            	activeSecurityRiskDetectors.addDetectorName(activeDetector);
            }
            try {
				logger.info(LOGTAG + "activeSecurityRiskDetectors = " + activeSecurityRiskDetectors.toXmlString());
			} catch (XmlEnterpriseObjectException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            localResponseDoc.getRootElement()
            	.getChild("DataArea")
            	.addContent((Element) activeSecurityRiskDetectors.buildOutputFromObject());
            replyContents = buildReplyDocument(eControlArea, localResponseDoc);
        } catch (EnterpriseLayoutException ele) {
            // There was an EnterpriseLayoutException error building the ActiveSecurityRiskDetectors element from the activeDetector object.
            String errType = "application";
            String errCode = "OpenEAI-1040";
            String errDesc = "Error building ActiveSecurityRiskDetectors element.  The exception is: " + ele.getMessage();
            logger.fatal(LOGTAG + " " + errDesc);
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
        } catch (EnterpriseConfigurationObjectException e) {
            // There was an EnterpriseConfigurationObjectException building the ActiveSecurityRiskDetectors element from the activeDetector object.
            String errType = "application";
            String errCode = "OpenEAI-1040";
            String errDesc = "Error building ActiveSecurityRiskDetectors element.  The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + " " + errDesc);
            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
            return getMessage(msg, replyContents);
		}

        // Return the response with status success.
        long timeElapsed = System.currentTimeMillis() - executionStart;
        logger.info(LOGTAG + "[execute] - Finished in elapsed time " + timeElapsed + " ms");
        return getMessage(msg, replyContents);
    }

	private String[] getActiveDectectorsFromPropertyConfig() throws EnterpriseConfigurationObjectException /*throws EnterpriseConfigurationObjectException*/ {
		return getAppConfig().getMainAppConfig()
			.getProperties("ActiveSrdsQueryCommandProperties")
			.getProperty("active-detectors")
			.trim().replaceAll("[\n\t\r ]", "")
			.split(",");
	}
}
