package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.AmazonIdentityManagementException;
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.AttachUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteGroupPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteGroupRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest;
import com.amazonaws.services.identitymanagement.model.DeleteSigningCertificateRequest;
import com.amazonaws.services.identitymanagement.model.DeleteUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DeleteUserRequest;
import com.amazonaws.services.identitymanagement.model.DetachGroupPolicyRequest;
import com.amazonaws.services.identitymanagement.model.DetachRolePolicyRequest;
import com.amazonaws.services.identitymanagement.model.DetachUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetGroupRequest;
import com.amazonaws.services.identitymanagement.model.GetGroupResult;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedGroupPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedGroupPoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedRolePoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListAttachedUserPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListAttachedUserPoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListGroupPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListGroupPoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListGroupsForUserRequest;
import com.amazonaws.services.identitymanagement.model.ListGroupsForUserResult;
import com.amazonaws.services.identitymanagement.model.ListRolePoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListRolePoliciesResult;
import com.amazonaws.services.identitymanagement.model.ListSigningCertificatesRequest;
import com.amazonaws.services.identitymanagement.model.ListSigningCertificatesResult;
import com.amazonaws.services.identitymanagement.model.ListUserPoliciesRequest;
import com.amazonaws.services.identitymanagement.model.ListUserPoliciesResult;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.Policy;
import com.amazonaws.services.identitymanagement.model.RemoveUserFromGroupRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.CUSTOM_ROLE_IAM_PATH;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.RHEDCLOUD_IAM_PATH;

/**
 * Remediate IAM resources that are missing important RHEDcloud policies.
 *
 * <p>For IAM users, an attempt is made to attach the missing policies.
 * That can fail, with a <code>LimitExceededException</code> in which case the user is deleted.<br>
 * Deleting the user could also fail, so as a last attempt at remediation, remove all permissions from the user.<br>
 * Users in the /rhedcloud/ path can raise a security risk but are ignored by the remediator.</p>
 *
 * <p>For IAM groups, remediation involves deleting the group. This only includes groups that are not in the
 * /rhedcloud/ path.  See the detector for additional details about groups.<br>
 * Deleting the group could also fail, so as a last attempt at remediation, remove all permissions from the group.</p>
 *
 * <p>For IAM roles, an attempt is made to attach the missing policies.
 * That can fail, with a <code>LimitExceededException</code> in which case the role is deleted.<br>
 * Deleting the role could also fail, so as a last attempt at remediation, remove all permissions from the role.</p>
 *
 * @see edu.emory.it.services.srd.detector.IamRestrictionPoliciesDetachedDetector the detector
 */
public class IamRestrictionPoliciesDetachedRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:iam:", srContext.LOGTAG, DetectionType.IamRestrictionPoliciesDetached))
            return;
        // like arn:aws:iam::123456789012:user/username
        //  or  arn:aws:iam::123456789012:group/group-name
        //  or  arn:aws:iam::123456789012:role/role-name
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        // additional information comes from the detector
        boolean isHIPAA = Boolean.parseBoolean(detected.getProperties().getProperty("isHIPAA"));
        boolean missingAdminPolicy = Boolean.parseBoolean(detected.getProperties().getProperty("missingAdminPolicy"));
        boolean missingAdminHipaaPolicy = Boolean.parseBoolean(detected.getProperties().getProperty("missingAdminHipaaPolicy"));
        boolean missingSubnetPolicy = Boolean.parseBoolean(detected.getProperties().getProperty("missingSubnetPolicy"));
        String path = detected.getProperties().getProperty("path");
        Boolean isType0 = AwsAccountServiceUtil.getInstance().isType0(accountId);

        try {
            AmazonIdentityManagement iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);

            /*
             * We will attach the missing policies but we need the ARN to do that,
             * so find the policies and then assert that they're present in the account.
             *
             * At least the Administrator Role policy and one of the Management Subnet policies must exist.
             * If the detector indicates that it's a HIPAA account then the AdministratorRoleHipaa policy must exist.
             * If a required policy doesn't exist then the account has not been configured correctly.
             */
            IamRestrictionPoliciesDetachedUtil.RestrictionPolicies policies = IamRestrictionPoliciesDetachedUtil.getRestrictionPolicies(iam);

            if (policies.RHEDcloudAdministratorRolePolicy == null) {
                setError(detected, srContext.LOGTAG, "Missing required policy: RHEDcloudAdministratorRolePolicy");
                return;
            }
            if (policies.RHEDcloudAdministratorRoleHipaaPolicy == null && isHIPAA) {
                setError(detected, srContext.LOGTAG, "Missing required policy: RHEDcloudAdministratorRoleHipaaPolicy");
                return;
            }
            // Type 0 accounts don't have a VPC so if it's a type 0 account
            // it doesn't need the subnet policy attached
            if (!isType0 && policies.RHEDcloudManagementSubnetPolicies == null) {
                setError(detected, srContext.LOGTAG, "Missing required policy: RHEDcloudManagementSubnetPolicy");
                return;
            }
            if (!missingAdminHipaaPolicy && !isHIPAA) {
                logger.warn(srContext.LOGTAG + "Should not have HIPAA policy for Standard account");
            }

            /*
             * generally, remediation involves attaching the missing policy to the resource.
             * if it's a HIPAA account BOTH Administrator policies (and the subnet policy) will be attached.
             *
             * there is a hard limit of 10 policies attached to a resource.
             * exceeding the limit looks like this:
             * com.amazonaws.services.identitymanagement.model.LimitExceededException: Cannot exceed quota for PoliciesPerUser: 10
             *
             * if we fail to attach our required policies, or fail for any other reason, we delete the resource.
             * and if that fails, we remove all permissions from the resource.
             */


            if (detected.getAmazonResourceName().contains("iam::" + accountId + ":user/")
                    && !RHEDCLOUD_IAM_PATH.equals(path)) {

                // user is in the /rhedcloud/ path are handled differently - see the comments in remediateUser()

                remediateUser(detected, srContext, iam, isHIPAA,
                        missingAdminPolicy, missingAdminHipaaPolicy, missingSubnetPolicy, policies);

            } else if (detected.getAmazonResourceName().contains("iam::" + accountId + ":group/")) {

                remediateGroup(detected, srContext, iam, isHIPAA,
                        missingAdminPolicy, missingAdminHipaaPolicy, missingSubnetPolicy, policies);

            } else if (detected.getAmazonResourceName().contains("iam::" + accountId + ":role/")) {

                remediateRole(detected, srContext, iam, isHIPAA,
                        missingAdminPolicy, missingAdminHipaaPolicy, missingSubnetPolicy, policies);

            }
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to remediate " + detected.getAmazonResourceName(), e);
        }
    }

    private void remediateUser(DetectedSecurityRisk detected, SecurityRiskContext srContext,
                               AmazonIdentityManagement iam, boolean isHIPAA,
                               boolean missingAdminPolicy, boolean missingAdminHipaaPolicy, boolean missingSubnetPolicy,
                               IamRestrictionPoliciesDetachedUtil.RestrictionPolicies policies) {

        /*
         * the RHEDcloud service control policies (SCPs) prohibit the creation of users.
         * if they exist they are typically there for long term access keys (as opposed to TKI)
         * or for data migration activities.
         *
         * if the user is in the /rhedcloud/ path then it is NOT remediated.
         * the reasoning is that these users can only be created by administrators and serve a special purpose.
         */

        String userName = detected.getProperties().getProperty("userName");

        try {
            AttachUserPolicyRequest attachUserPolicyRequest = new AttachUserPolicyRequest()
                    .withUserName(userName);

            String description = "";

            if (missingAdminPolicy) {
                attachUserPolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRolePolicy.getArn());
                iam.attachUserPolicy(attachUserPolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRolePolicy.getPolicyName() + " to user " + userName + ".  ";
            }
            if (missingAdminHipaaPolicy && isHIPAA) {
                attachUserPolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRoleHipaaPolicy.getArn());
                iam.attachUserPolicy(attachUserPolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRoleHipaaPolicy.getPolicyName() + " to user " + userName + ".  ";
            }
            if (missingSubnetPolicy) {
                for (Policy policy : policies.RHEDcloudManagementSubnetPolicies) {
                    attachUserPolicyRequest.setPolicyArn(policy.getArn());
                    iam.attachUserPolicy(attachUserPolicyRequest);
                    description += "Attached policy " + policy.getPolicyName() + " to user " + userName + ".  ";
                }
            }

            setSuccess(detected, srContext.LOGTAG, description.trim());
        }
        catch (NoSuchEntityException nsee) {
            // the user may have been deleted in the middle of our remediation steps
            setPostponed(detected, srContext.LOGTAG, "User " + userName + " may have been deleted.");
        }
        catch (AmazonIdentityManagementException e) {
            try {
                // attaching the missing policies failed (maybe exceeded policy quota) ...
                // attempt to delete the user instead
                deleteUser(iam, userName);

                setSuccess(detected, srContext.LOGTAG, "Could not attach required policy so deleted user " + userName + ".");
            }
            catch (NoSuchEntityException nsee) {
                // the user may have been deleted in the middle of our remediation steps
                setPostponed(detected, srContext.LOGTAG, "User " + userName + " may have been deleted.");
            }
            catch (AmazonIdentityManagementException e2) {
                try {

                    // attaching the missing policies failed ...
                    // deleting the user failed ...
                    // as a final attempt at remediation, try to remove all permissions from the user
                    deleteAllUserPolicies(iam, userName);

                    setSuccess(detected, srContext.LOGTAG, "Could not attach required policy or delete the user so removed all permissions from user " + userName + ".");
                }
                catch (NoSuchEntityException nsee) {
                    // the user may have been deleted in the middle of our remediation steps
                    setPostponed(detected, srContext.LOGTAG, "User " + userName + " may have been deleted.");
                }
                catch (AmazonIdentityManagementException e3) {
                    // attaching the missing policies failed ...
                    // deleting the user failed ...
                    // remove all permissions from the user failed...
                    setError(detected, srContext.LOGTAG, "Could not attach required policy or delete the user or remove all permissions from user " + userName, e3);
                }
            }
        }
    }

    /**
     * Deletes the specified IAM user.
     * The user must not belong to any groups or have any access keys, signing certificates, or attached policies.
     *
     * @param iam client
     * @param userName username
     */
    private void deleteUser(AmazonIdentityManagement iam, String userName) {

        ListGroupsForUserRequest listGroupsForUserRequest = new ListGroupsForUserRequest()
                .withUserName(userName);
        ListGroupsForUserResult listGroupsForUserResult;
        RemoveUserFromGroupRequest removeUserFromGroupRequest = new RemoveUserFromGroupRequest()
                .withUserName(userName);
        do {
            listGroupsForUserResult = iam.listGroupsForUser(listGroupsForUserRequest);

            listGroupsForUserResult.getGroups().forEach(group ->
                    iam.removeUserFromGroup(removeUserFromGroupRequest.withGroupName(group.getGroupName()))
            );

            listGroupsForUserRequest.setMarker(listGroupsForUserResult.getMarker());
        } while (listGroupsForUserResult.isTruncated());


        ListAccessKeysRequest listAccessKeysRequest = new ListAccessKeysRequest()
                .withUserName(userName);
        ListAccessKeysResult listAccessKeysResult;
        DeleteAccessKeyRequest deleteAccessKeyRequest = new DeleteAccessKeyRequest()
                .withUserName(userName);
        do {
            listAccessKeysResult = iam.listAccessKeys(listAccessKeysRequest);

            listAccessKeysResult.getAccessKeyMetadata().forEach(accessKeyMetadata ->
                    iam.deleteAccessKey(deleteAccessKeyRequest.withAccessKeyId(accessKeyMetadata.getAccessKeyId()))
            );

            listAccessKeysRequest.setMarker(listAccessKeysResult.getMarker());
        } while (listAccessKeysResult.isTruncated());


        ListSigningCertificatesRequest listSigningCertificatesRequest = new ListSigningCertificatesRequest()
                .withUserName(userName);
        ListSigningCertificatesResult listSigningCertificatesResult;
        DeleteSigningCertificateRequest deleteSigningCertificateRequest = new DeleteSigningCertificateRequest()
                .withUserName(userName);
        do {
            listSigningCertificatesResult = iam.listSigningCertificates(listSigningCertificatesRequest);

            listSigningCertificatesResult.getCertificates().forEach(signingCertificate ->
                iam.deleteSigningCertificate(deleteSigningCertificateRequest.withCertificateId(signingCertificate.getCertificateId()))
            );

            listSigningCertificatesRequest.setMarker(listSigningCertificatesResult.getMarker());
        } while (listSigningCertificatesResult.isTruncated());


        deleteAllUserPolicies(iam, userName);


        DeleteUserRequest deleteUserRequest = new DeleteUserRequest()
                .withUserName(userName);
        iam.deleteUser(deleteUserRequest);
    }

    /**
     * Delete permissions for the specified IAM user.<br>
     * Deletes all attached and inline policies for the user.
     *
     * @param iam client
     * @param userName username
     */
    private void deleteAllUserPolicies(AmazonIdentityManagement iam, String userName) {
        ListAttachedUserPoliciesRequest listAttachedUserPoliciesRequest = new ListAttachedUserPoliciesRequest()
                .withUserName(userName);
        ListAttachedUserPoliciesResult listAttachedUserPoliciesResult;
        DetachUserPolicyRequest detachUserPolicyRequest = new DetachUserPolicyRequest()
                .withUserName(userName);
        do {
            listAttachedUserPoliciesResult = iam.listAttachedUserPolicies(listAttachedUserPoliciesRequest);

            listAttachedUserPoliciesResult.getAttachedPolicies().forEach(attachedPolicy ->
                    iam.detachUserPolicy(detachUserPolicyRequest.withPolicyArn(attachedPolicy.getPolicyArn()))
            );

            listAttachedUserPoliciesRequest.setMarker(listAttachedUserPoliciesResult.getMarker());
        } while (listAttachedUserPoliciesResult.isTruncated());


        ListUserPoliciesRequest listUserPoliciesRequest = new ListUserPoliciesRequest()
                .withUserName(userName);
        ListUserPoliciesResult listUserPoliciesResult;
        DeleteUserPolicyRequest deleteUserPolicyRequest = new DeleteUserPolicyRequest()
                .withUserName(userName);
        do {
            listUserPoliciesResult = iam.listUserPolicies(listUserPoliciesRequest);

            listUserPoliciesResult.getPolicyNames().forEach(policyName ->
                    iam.deleteUserPolicy(deleteUserPolicyRequest.withPolicyName(policyName)));

            listUserPoliciesRequest.setMarker(listUserPoliciesResult.getMarker());
        } while (listUserPoliciesResult.isTruncated());
    }

    private void remediateGroup(DetectedSecurityRisk detected, SecurityRiskContext srContext,
                                AmazonIdentityManagement iam, boolean isHIPAA,
                                boolean missingAdminPolicy, boolean missingAdminHipaaPolicy, boolean missingSubnetPolicy,
                                IamRestrictionPoliciesDetachedUtil.RestrictionPolicies policies) {

        String groupName = detected.getProperties().getProperty("groupName");

        /*
         * The RHEDcloud service control policies (SCPs) prohibit the creation of groups.
         *
         * But groups could be used by central IT where they might like to use a Group
         * as a container for long-term key pair users in an account that have the same policy.
         * In this case they will put the group in the /rhedcloud/ path (which the detector ignores).
         *
         * All other groups will be deleted.  If the deletion fails, all group permissions will be deleted.
         *
         * TODO - steering committee will consider this decision to simply delete the group vs. attaching missing policies
         */

        try {
            deleteGroup(iam, groupName);

            setSuccess(detected, srContext.LOGTAG, "Group " + groupName + " deleted.");
        }
        catch (NoSuchEntityException nsee) {
            // the group may have been deleted in the middle of our remediation steps
            setPostponed(detected, srContext.LOGTAG, "Group " + groupName + " may have been deleted.");
        }
        catch (AmazonIdentityManagementException e2) {
            try {
                // deleting the group failed ...
                // as a final attempt at remediation, try to remove all permissions from the group
                deleteAllGroupPolicies(iam, groupName);

                setSuccess(detected, srContext.LOGTAG, "Could not delete the group so removed all permissions from group " + groupName + ".");
            }
            catch (AmazonIdentityManagementException e3) {
                setError(detected, srContext.LOGTAG, "Could not delete the group or remove all permissions from group " + groupName, e3);
            }
        }

        /*
         * in case the decision is changed, here's the code that simply attaches the missing policy to the group
         */
        /*
        try {
            AttachGroupPolicyRequest attachGroupPolicyRequest = new AttachGroupPolicyRequest()
                    .withGroupName(groupName);

            String description = "";
            if (missingAdminPolicy) {
                attachGroupPolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRolePolicy.getArn());
                iam.attachGroupPolicy(attachGroupPolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRolePolicy.getPolicyName() + " to group " + groupName + ".  ";
            }
            if (missingAdminHipaaPolicy && isHIPAA) {
                attachGroupPolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRoleHipaaPolicy.getArn());
                iam.attachGroupPolicy(attachGroupPolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRoleHipaaPolicy.getPolicyName() + " to group " + groupName + ".  ";
            }
            if (missingSubnetPolicy) {
                for (Policy policy : policies.RHEDcloudManagementSubnetPolicies) {
                    attachGroupPolicyRequest.setPolicyArn(policy.getArn());
                    iam.attachGroupPolicy(attachGroupPolicyRequest);
                    description += "Attached policy " + policy.getPolicyName() + " to group " + groupName + ".  ";
                }
            }

            remediationResult.setStatus(RemediationStatus.REMEDIATION_SUCCESS.getStatus());
            remediationResult.setDescription(description.trim());
            detected.setRemediationResult(remediationResult);

        } catch (AmazonIdentityManagementException e) {
            // possibly exceeded policy quota
            deleteGroup(iam, groupName);

            remediationResult.setStatus(RemediationStatus.REMEDIATION_SUCCESS.getStatus());
            remediationResult.setDescription("Could not attach required policy so deleted group.");
            detected.setRemediationResult(remediationResult);
        }
        */
    }

    /**
     * Delete the specified IAM group.
     * The group must not contain any users or have any attached policies.
     *
     * @param iam client
     * @param groupName group name
     */
    private void deleteGroup(AmazonIdentityManagement iam, String groupName) {

        GetGroupRequest getGroupRequest = new GetGroupRequest()
                .withGroupName(groupName);
        GetGroupResult getGroupResult;
        RemoveUserFromGroupRequest removeUserFromGroupRequest = new RemoveUserFromGroupRequest()
                .withGroupName(groupName);
        do {
            getGroupResult = iam.getGroup(getGroupRequest);

            getGroupResult.getUsers().forEach(user ->
                iam.removeUserFromGroup(removeUserFromGroupRequest.withUserName(user.getUserName()))
            );

            getGroupRequest.setMarker(getGroupResult.getMarker());
        } while (getGroupResult.isTruncated());


        deleteAllGroupPolicies(iam, groupName);


        DeleteGroupRequest deleteGroupRequest = new DeleteGroupRequest()
                .withGroupName(groupName);
        iam.deleteGroup(deleteGroupRequest);
    }

    /**
     * Delete permissions for the specified IAM group.<br>
     * Deletes all attached and inline policies for the group.
     *
     * @param iam client
     * @param groupName group name
     */
    private void deleteAllGroupPolicies(AmazonIdentityManagement iam, String groupName) {
        ListAttachedGroupPoliciesRequest listAttachedGroupPoliciesRequest = new ListAttachedGroupPoliciesRequest()
                .withGroupName(groupName);
        ListAttachedGroupPoliciesResult listAttachedGroupPoliciesResult;
        DetachGroupPolicyRequest detachGroupPolicyRequest = new DetachGroupPolicyRequest()
                .withGroupName(groupName);
        do {
            listAttachedGroupPoliciesResult = iam.listAttachedGroupPolicies(listAttachedGroupPoliciesRequest);

            listAttachedGroupPoliciesResult.getAttachedPolicies().forEach(attachedPolicy ->
                    iam.detachGroupPolicy(detachGroupPolicyRequest.withPolicyArn(attachedPolicy.getPolicyArn()))
            );

            listAttachedGroupPoliciesRequest.setMarker(listAttachedGroupPoliciesResult.getMarker());
        } while (listAttachedGroupPoliciesResult.isTruncated());


        ListGroupPoliciesRequest listGroupPoliciesRequest = new ListGroupPoliciesRequest()
                .withGroupName(groupName);
        ListGroupPoliciesResult listGroupPoliciesResult;
        DeleteGroupPolicyRequest deleteGroupPolicyRequest = new DeleteGroupPolicyRequest()
                .withGroupName(groupName);
        do {
            listGroupPoliciesResult = iam.listGroupPolicies(listGroupPoliciesRequest);

            listGroupPoliciesResult.getPolicyNames().forEach(policyName ->
                    iam.deleteGroupPolicy(deleteGroupPolicyRequest.withPolicyName(policyName))
            );

            listGroupPoliciesRequest.setMarker(listGroupPoliciesResult.getMarker());
        } while (listGroupPoliciesResult.isTruncated());
    }

    private void remediateRole(DetectedSecurityRisk detected, SecurityRiskContext srContext,
                               AmazonIdentityManagement iam, boolean isHIPAA,
                               boolean missingAdminPolicy, boolean missingAdminHipaaPolicy, boolean missingSubnetPolicy,
                               IamRestrictionPoliciesDetachedUtil.RestrictionPolicies policies) {

        String roleName = detected.getProperties().getProperty("roleName");
        String rolePath = detected.getProperties().getProperty("path");
        boolean isCustomRole = CUSTOM_ROLE_IAM_PATH.equals(rolePath);

        try {
            AttachRolePolicyRequest attachRolePolicyRequest = new AttachRolePolicyRequest()
                    .withRoleName(roleName);

            String description = "";

            if (missingAdminPolicy && !isCustomRole) {
                attachRolePolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRolePolicy.getArn());
                iam.attachRolePolicy(attachRolePolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRolePolicy.getPolicyName() + " to role " + roleName + ".  ";
            }
            if (missingAdminHipaaPolicy && isHIPAA) {
                attachRolePolicyRequest.setPolicyArn(policies.RHEDcloudAdministratorRoleHipaaPolicy.getArn());
                iam.attachRolePolicy(attachRolePolicyRequest);
                description += "Attached policy " + policies.RHEDcloudAdministratorRoleHipaaPolicy.getPolicyName() + " to role " + roleName + ".  ";
            }
            if (missingSubnetPolicy) {
                for (Policy policy : policies.RHEDcloudManagementSubnetPolicies) {
                    attachRolePolicyRequest.setPolicyArn(policy.getArn());
                    iam.attachRolePolicy(attachRolePolicyRequest);
                    description += "Attached policy " + policy.getPolicyName() + " to role " + roleName + ".  ";
                }
            }

            setSuccess(detected, srContext.LOGTAG, description.trim());
        }
        catch (NoSuchEntityException nsee) {
            // the role may have been deleted in the middle of our remediation steps
            setPostponed(detected, srContext.LOGTAG, "Role " + roleName + " may have been deleted.");
        }
        catch (AmazonIdentityManagementException e) {
            try {
                // attaching the missing policies failed (maybe exceeded policy quota) ...
                // attempt to delete the role instead
                deleteRole(iam, roleName);

                setSuccess(detected, srContext.LOGTAG, "Could not attach required policy so deleted role " + roleName + ".");

            }
            catch (NoSuchEntityException nsee) {
                // the role may have been deleted in the middle of our remediation steps
                setPostponed(detected, srContext.LOGTAG, "Role " + roleName + " may have been deleted.");
            }
            catch (AmazonIdentityManagementException e2) {
                try {
                    // attaching the missing policies failed ...
                    // deleting the role failed ...
                    // as a final attempt at remediation, try to remove all permissions from the role
                    deleteAllRolePolicies(iam, roleName);

                    setSuccess(detected, srContext.LOGTAG, "Could not attach required policy or delete the role so removed all permissions from role " + roleName + ".");
                }
                catch (AmazonIdentityManagementException e3) {
                    setError(detected, srContext.LOGTAG, "Could not attach required policy or delete the role or remove all permissions from role " + roleName, e3);
                }
            }
        }
    }

    /**
     * Deletes the specified role.
     * The role must not have any policies attached.
     *
     * @param iam client
     * @param roleName role name
     */
    private void deleteRole(AmazonIdentityManagement iam, String roleName) {
        deleteAllRolePolicies(iam, roleName);

        DeleteRoleRequest deleteRoleRequest = new DeleteRoleRequest()
                .withRoleName(roleName);
        iam.deleteRole(deleteRoleRequest);
    }

    /**
     * Delete permissions for the specified IAM role.<br>
     * Deletes all attached and inline policies for the role.
     *
     * @param iam client
     * @param roleName role name
     */
    private void deleteAllRolePolicies(AmazonIdentityManagement iam, String roleName) {
        ListRolePoliciesRequest listRolePoliciesRequest = new ListRolePoliciesRequest()
                .withRoleName(roleName);
        ListRolePoliciesResult listRolePoliciesResult;
        DeleteRolePolicyRequest deleteRolePolicyRequest = new DeleteRolePolicyRequest()
                .withRoleName(roleName);
        do {
            listRolePoliciesResult = iam.listRolePolicies(listRolePoliciesRequest);

            listRolePoliciesResult.getPolicyNames().forEach(policyName ->
                    iam.deleteRolePolicy(deleteRolePolicyRequest.withPolicyName(policyName))
            );

            listRolePoliciesRequest.setMarker(listRolePoliciesResult.getMarker());
        } while (listRolePoliciesResult.isTruncated());


        ListAttachedRolePoliciesRequest listAttachedRolePoliciesRequest = new ListAttachedRolePoliciesRequest()
                .withRoleName(roleName);
        ListAttachedRolePoliciesResult listAttachedRolePoliciesResult;
        DetachRolePolicyRequest detachRolePolicyRequest = new DetachRolePolicyRequest()
                .withRoleName(roleName);
        do {
            listAttachedRolePoliciesResult = iam.listAttachedRolePolicies(listAttachedRolePoliciesRequest);

            listAttachedRolePoliciesResult.getAttachedPolicies().forEach(attachedPolicy ->
                    iam.detachRolePolicy(detachRolePolicyRequest.withPolicyArn(attachedPolicy.getPolicyArn()))
            );
            listAttachedRolePoliciesRequest.setMarker(listAttachedRolePoliciesResult.getMarker());
        } while (listAttachedRolePoliciesResult.isTruncated());
    }
}
