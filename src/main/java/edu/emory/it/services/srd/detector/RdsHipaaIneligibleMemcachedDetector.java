package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.elasticache.model.CacheCluster;
import com.amazonaws.services.elasticache.model.ReplicationGroup;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.function.Predicate;

/**
 * For HIPAA compliance class accounts only, detect ElastiCache clusters where the cluster engine is memcached.<br>
 * Encryption at-rest is not possible with memcached so they are identified as a security risk.
 *
 * @see edu.emory.it.services.srd.remediator.RdsHipaaIneligibleMemcachedRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsHipaaIneligibleMemcachedDetector extends ElastiCacheBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, DetectionType.RdsHipaaIneligibleMemcached);
    }

    @Override
    protected Predicate<CacheCluster> getCacheClusterPredicate() {
        return cluster -> cluster.getEngine().equals("memcached");
    }

    @Override
    protected Predicate<ReplicationGroup> getReplicationGroupPredicate() {
        // Memcached will never have replication groups so no need to check engine
        return replicationGroup -> false;
    }

    @Override
    public String getBaseName() {
        return "RdsHipaaIneligibleMemcached";
    }
}
