package edu.emory.it.services.srd.util;

import java.util.Objects;

public class SrdNameValuePair {
    private final String name;
    private final String value;

    /**
     * Default Constructor taking a name and a value. The value may be null.
     *
     * @param name The name.
     * @param value The value.
     */
    public SrdNameValuePair(final String name, final String value) {
        Objects.requireNonNull(name, "Name must not be null");
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }
    public String getValue() {
        return this.value;
    }
    @Override public String toString() { return this.name + "=" + this.value; }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object instanceof SrdNameValuePair) {
            final SrdNameValuePair that = (SrdNameValuePair) object;
            return this.name.equals(that.name)
                  && Objects.equals(this.value, that.value);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.value);
    }
}
