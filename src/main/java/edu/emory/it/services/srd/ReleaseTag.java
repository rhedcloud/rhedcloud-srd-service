package edu.emory.it.services.srd;

/**
 * A release tag for the service.
 */
public abstract class ReleaseTag {
    private static final String space = " ";
    private static final String notice = "***";
    private static final String releaseName = "RHEDcloud Security Risk Detection Service";
    private static final String releaseNumber = "Release 1.0";
    private static final String buildNumber = "Build ####";
    private static final String copyRight = "Copyright 2019 Emory University. All Rights Reserved.";

    public static String getReleaseInfo() {
        StringBuilder buf = new StringBuilder();
        buf.append(notice);
        buf.append(space);
        buf.append(releaseName);
        buf.append(space);
        buf.append(releaseNumber);
        buf.append(space);
        buf.append(buildNumber);
        buf.append(space);
        buf.append(copyRight);
        buf.append(space);
        buf.append(notice);
        return buf.toString();
    }
}
