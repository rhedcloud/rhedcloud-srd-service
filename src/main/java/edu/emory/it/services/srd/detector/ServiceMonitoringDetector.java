package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Pseudo detector for synthetic monitoring that has no remediator.
 * This "detector" must return a successful status so that sync events are not persisted and account owners are not notified.
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ServiceMonitoringDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        setDetectionSuccess(detection, DetectionType.ServiceMonitoring, srContext.LOGTAG);
    }

    @Override
    public String getBaseName() {
        return "ServiceMonitoring";
    }
}
