package edu.emory.it.services.srd.provider;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TLockService {
    private final TLockRepository repository;

    public TLockService(TLockRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public long countByLockNameLike(String like) {
        return repository.countByLockNameLike(like);
    }
}
