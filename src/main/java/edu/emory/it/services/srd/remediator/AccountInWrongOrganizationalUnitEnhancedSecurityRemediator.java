package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Notify only - passive remediation.
 *
 * <p>For EnhancedSecurity compliance class accounts</p>
 *
 * @see edu.emory.it.services.srd.detector.AccountInWrongOrganizationalUnitEnhancedSecurityDetector the detector
 */
public class AccountInWrongOrganizationalUnitEnhancedSecurityRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:organizations:", srContext.LOGTAG, DetectionType.AccountInWrongOrganizationalUnit))
            return;
        // like arn:aws:organizations::987654321098:account/*/123456789012
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        setNotificationOnly(detected, srContext.LOGTAG);
    }
}
