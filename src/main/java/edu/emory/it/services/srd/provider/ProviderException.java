package edu.emory.it.services.srd.provider;

/**
 * An exception for user object provider errors.
 * <p>
 *
 * @author Steve Wheat (swheat@emory.edu)
 * @version 1.0  - 29 April 2013
 */
public class ProviderException extends Exception {
    /**
     * Default constructor.
     */
    public ProviderException() {
        super();
    }

    /**
     * Message constructor.
     * @param msg message
     */
    public ProviderException(String msg) {
        super(msg);
    }

    /**
     * Throwable constructor.
     * @param msg message
     * @param e exception
     */
    public ProviderException(String msg, Throwable e) {
        super(msg, e);
    }
}
