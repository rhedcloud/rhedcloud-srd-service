package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskResourceTaggingProfile;
import com.amazonaws.arn.Arn;
import com.amazonaws.regions.Region;
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPI;
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPIClient;
import com.amazonaws.services.resourcegroupstaggingapi.model.GetResourcesRequest;
import com.amazonaws.services.resourcegroupstaggingapi.model.GetResourcesResult;
import com.amazonaws.services.resourcegroupstaggingapi.model.ResourceTagMapping;
import com.amazonaws.services.resourcegroupstaggingapi.model.Tag;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.ResourceTaggingProfileServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile;
import org.rhedcloud.moa.objects.resources.v1_0.AccountMetadataFilter;
import org.rhedcloud.moa.objects.resources.v1_0.ManagedTag;
import org.rhedcloud.moa.objects.resources.v1_0.ServiceFilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Check all account resources for tag policy violations and missing required tags.
 *
 * @see edu.emory.it.services.srd.remediator.TagPolicyViolationRemediator
 * @see edu.emory.it.services.srd.remediator.TagPolicyViolationSimulationRemediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class TagPolicyViolationDetector extends AbstractSecurityRiskDetector {

	private static final String PROFILE_TAG_KEY = "srd:profile";

	@Override
	public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
		ResourceTaggingProfileServiceUtil taggingUtil = ResourceTaggingProfileServiceUtil.getInstance();

		if (!taggingUtil.isServiceReady()) {
			logger.error(srContext.LOGTAG+" ResourceTaggingProfileServiceUtil not ready - skipping detection");
			setDetectionError(detection, DetectionType.TagPolicyViolationNoTag,
					srContext.LOGTAG, "ResourceTaggingProfileServiceUtil not ready - skipping detection");
			return;
		}

		SecurityRiskResourceTaggingProfile srResourceTaggingProfile = srContext.requisition.getSecurityRiskResourceTaggingProfile();
		String rtpPartialKey; // namespace + profile name
		String rtpKey;        // namespace + profile name + revision
		if (srResourceTaggingProfile == null) {
			rtpPartialKey = rtpKey = null;
		}
		else {
			rtpPartialKey = srResourceTaggingProfile.getNamespace() + ":" + srResourceTaggingProfile.getProfileName();
			rtpKey = rtpPartialKey + ":" + srResourceTaggingProfile.getRevision();
		}

		AWSResourceGroupsTaggingAPI client;
		logger.info(srContext.LOGTAG+" getRegions() = "+getRegions());
		for (Region region : getRegions()) {
			try {
				client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSResourceGroupsTaggingAPIClient.class);
			}
			catch (EmoryAwsClientBuilderException e) {
				// The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
				logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
				continue;
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.TagPolicyViolationNoTag,
						srContext.LOGTAG, "On region: " + region.getName(), e);
				return;
			}

			List<ResourceTagMapping> mappingsWithTags;
			try {
				List<ResourceTagMapping> mappings = findResourcesWithTagHistory(client);

				if (mappings.size()==0) {
					setDetectionError(detection, DetectionType.TagPolicyViolationNoTag,
							srContext.LOGTAG, "Error while scanning resources for tag policy violations: There are no resources with tag histories for account "
									+accountId+" in region "+region);
					logger.error(srContext.LOGTAG+ "Error while scanning resources for tag policy violations: There are no resources with tag histories for account "
							+accountId+" in region "+region);
					return;

				} 

				// remove exempt resources from mappings
				mappings = mappings.stream()
						.filter(rtm -> {
							Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
									.isExemptResourceArnForAccountAndDetector(rtm.getResourceARN(),accountId,getBaseName());
							boolean exceptionStatusUnderminable = resourceIsExempt == null;
							if (exceptionStatusUnderminable) {
								logger.error(srContext.LOGTAG + "Exemption status of resource " + rtm.getResourceARN() + " can not be determined on account " + accountId);	
								return false; // if you can't determine if the resource is exempt of not you have to assume it is
							}
							if (resourceIsExempt) {
								logger.info(srContext.LOGTAG + "Ignoring exempt resource "+rtm.getResourceARN()+" for account "+accountId);
							} 
							return !resourceIsExempt; 
						})
						.collect(Collectors.toList());

				// when a resource tagging profile was specified, we filter the mappings to only those 
				// that are not exempt and have the RTP key.
				// the SRD profile tag is a partial key only (no revision).

				if (rtpPartialKey != null) {
					mappingsWithTags = mappings.stream()
							.filter(rtm -> rtm.getTags().stream()
									.anyMatch(tag -> tag.getKey().equals(PROFILE_TAG_KEY) && tag.getValue().equals(rtpPartialKey)))
							.collect(Collectors.toList());
				}
				else {
					mappingsWithTags = mappings;
				}
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.TagPolicyViolationNoTag,
						srContext.LOGTAG, "Unable to get resources with tag history", e);
				return;
			}

			// If there are no mappings then it means that none of the resources on the account have a tag history.
			// This is unlikely especially with the Profile Tag Hunter.
			// Except, if a specific resource tagging profile was requested, perhaps there aren't any resource tagged with it yet.
			// Regardless, we can't continue so add a risk and return.
			if (mappingsWithTags.isEmpty()) {
				// arn:${Partition}:${service}:${Region}:${Account}:${resource}/${resourceId}
				String synthesizedArn = "arn:aws:*:" + region + ":" + accountId + ":*/*";
				addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.TagPolicyViolationNoTagsOnAccount, synthesizedArn);
				return;
			}
			logger.info(srContext.LOGTAG + resourceTagMappingToString(accountId, region.getName(), mappingsWithTags));


			try {
				scanResourcesForTagPolicyViolations(detection, mappingsWithTags, accountId, region.getName(), rtpKey, srContext.LOGTAG);
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.TagPolicyViolationNoTag,
						srContext.LOGTAG, "Error while scanning resources for tag policy violations", e);
				return;
			}
		}

		// detector name does not match a DetectionType so ensure DetectionResult is set
		if (detection.getDetectionResult() == null) {
			setDetectionSuccess(detection, DetectionType.TagPolicyViolationNoTag, srContext.LOGTAG);
		}
	}

	List<ResourceTagMapping> findResourcesWithTagHistory(AWSResourceGroupsTaggingAPI client) {
		GetResourcesRequest request = new GetResourcesRequest()
				.withResourcesPerPage(100)
				.withTagFilters()
				.withResourceTypeFilters(Collections.emptyList());

		List<ResourceTagMapping> allMappings = new ArrayList<>();

		while (true) {
			GetResourcesResult result = client.getResources(request);

			List<ResourceTagMapping> mapping = result.getResourceTagMappingList();

			allMappings.addAll(mapping);

			if (result.getPaginationToken() == null || result.getPaginationToken().isEmpty()) {
				break;
			}
			request.setPaginationToken(result.getPaginationToken());
		}

		return allMappings;
	}

	void scanResourcesForTagPolicyViolations(SecurityRiskDetection detection, List<ResourceTagMapping> mappingsWithTags,
			String accountId, String region, String rtpKey, String logtag) {

		ResourceTaggingProfileServiceUtil taggingUtil = ResourceTaggingProfileServiceUtil.getInstance();
		ResourceTaggingProfile taggingProfile = null;

		if (rtpKey != null) {
			// this is probably a simulation. regardless, use the specified resource tagging profile key
			// instead of the one found on the resource.  only need to get the profile once.
			taggingProfile = taggingUtil.query(rtpKey, logtag);
			if (taggingProfile == null) {
				String synthesizedArn = "arn:aws:*:" + region + ":" + accountId + ":*/*";
				addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoRTPSEntry, synthesizedArn);
				return;
			}
		}

		for (ResourceTagMapping resourceTagMapping : mappingsWithTags) {
			if (rtpKey == null) {
				Optional<Tag> profileTagOptional = resourceTagMapping.getTags().stream()
						.filter(t -> t.getKey().equals(PROFILE_TAG_KEY))
						.findAny();
				if (!profileTagOptional.isPresent()) {
					addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoProfileTag, resourceTagMapping.getResourceARN());
					continue;
				}
				Tag profileTag = profileTagOptional.get();
				if (profileTag.getValue() == null || profileTag.getValue().isEmpty()) {
					addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoTag, resourceTagMapping.getResourceARN(),
							new SrdNameValuePair(profileTag.getKey(), profileTag.getValue()));
					continue;
				}
				taggingProfile = taggingUtil.query(profileTag.getValue(), logtag);
				if (taggingProfile == null) {
					addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoRTPSEntry, resourceTagMapping.getResourceARN());
					continue;
				}
			}

			@SuppressWarnings("unchecked")
			List<ManagedTag> managedTags = taggingProfile.getManagedTag();
			managedTags.forEach(managedTag -> checkResourceTags(detection, resourceTagMapping, managedTag, accountId, logtag));
		}
	}

	/**
	 * For all tags of a given resource, check for compliance with managed tag. This entails scanning all tags
	 * of the resource and comparing against the provided managed tag.  Filtering includes any ResourceType value that
	 * might optionally exist in the managed tag, to match the resource type of the given resource.
	 *
	 * ManagedTag scenarios:
	 *
	 * 1) Add tag to all resources - no ServiceFilter and no AccountMetadataFilter
	 * 2) Add tag to specific services only - ServiceFilter and no AccountMetadataFilter
	 * 3) Add tag to specific services and accounts - ServiceFilter and AccountMetadataFilter
	 *
	 * @param tagMapping - All tags for a given resource
	 * @param managedTag - Managed tag provided by RTPS for the resource's profile tag value
	 * @param accountId - current account's id
	 */
	void checkResourceTags(SecurityRiskDetection detection, ResourceTagMapping tagMapping, ManagedTag managedTag, String accountId, String logtag) {
		Arn arn;
		try {
			arn = Arn.fromString(tagMapping.getResourceARN());
		}
		catch (IllegalArgumentException e) {
			// really unlikely to happen but be safe
			setDetectionError(detection, DetectionType.TagPolicyViolationBadValue, logtag, e.getMessage() + " for " + tagMapping.getResourceARN());
			return;
		}

		ServiceFilter serviceFilter = managedTag.getServiceFilter();
		AccountMetadataFilter accountMetadataFilter = managedTag.getAccountMetadataFilter();

		List<Tag> awsResourceTags = tagMapping.getTags();

		if (awsResourceTags.isEmpty()) {
			// Tag history but no current tags.
			addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoTag, tagMapping.getResourceARN(),
					new SrdNameValuePair(managedTag.getTagName(), managedTag.getTagValue()));
			return;
		}

		// Resource name is optional - if not present match by service name only.
		boolean serviceFilterPresent = serviceFilter != null
				&& serviceFilter.getServiceName() != null
				&& !serviceFilter.getServiceName().isEmpty();

		boolean accountMetadataFilterPresent = accountMetadataFilter != null
				&& accountMetadataFilter.getPropertyName() != null
				&& !accountMetadataFilter.getPropertyName().isEmpty()
				&& accountMetadataFilter.getPropertyValue() != null
				&& !accountMetadataFilter.getPropertyValue().isEmpty();

		// Check if there are filters present in the managed tag. These may
		// short-circuit the required tag check. If there are no filters,
		// it's a fall through, and required tag check will occur.
		boolean checkForRequiredTag;
		if (serviceFilterPresent) {
			if (accountMetadataFilterPresent) {
				checkForRequiredTag = matchServiceFilter(serviceFilter, arn)
						&& matchAccountMetaDataFilter(accountMetadataFilter, accountId, logtag);
			}
			else {
				checkForRequiredTag = matchServiceFilter(serviceFilter, arn);
			}
		}
		else if (accountMetadataFilterPresent) {
			checkForRequiredTag = matchAccountMetaDataFilter(accountMetadataFilter, accountId, logtag);
		}
		else {
			checkForRequiredTag = true;
		}

		if (checkForRequiredTag) {
			String requiredTagKey = managedTag.getTagName();
			String requiredTagValue = managedTag.getTagValue();

			Optional<Tag> requiredTag = awsResourceTags.stream()
					.filter(art -> art.getKey().equals(requiredTagKey))
					.findAny();
			if (!requiredTag.isPresent()) {
				addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationNoTag, tagMapping.getResourceARN(),
						new SrdNameValuePair(requiredTagKey, requiredTagValue));
			}
			else if (!requiredTag.get().getValue().equalsIgnoreCase(requiredTagValue)) {
				addDetectedSecurityRisk(detection, logtag, DetectionType.TagPolicyViolationBadValue, tagMapping.getResourceARN(),
						new SrdNameValuePair(requiredTagKey, requiredTagValue));       // Expected value
			}
		}
	}

	/**
	 * Check resource service and name against provided service filter
	 *
	 * @param serviceFilter - RTPS service filter from ManagedTag retrieved by profile id
	 * @param arn - Containing service name and resource
	 * @return true if either service filter's service name and resource name matches respective aws resource values, or
	 *              if service filter's service name matches aws resource's service name when
	 *              no resource names are provided in the filter
	 */
	boolean matchServiceFilter(ServiceFilter serviceFilter, Arn arn) {
		boolean matched = true;

		String serviceName = serviceFilter.getServiceName();
		@SuppressWarnings("unchecked")
		List<String> resourceNames = serviceFilter.getResourceName();

		if (serviceName != null 
				&& serviceName.equalsIgnoreCase(arn.getService()) 
				&& resourceNames != null 
				&& !resourceNames.isEmpty()) 
		{
			Optional<String> resourceNameOptional = resourceNames.stream()
					.filter(rn -> rn.equals(arn.getResource().getResourceType()))
					.findAny();
			if (!resourceNameOptional.isPresent()) {
				matched = false;
			}
		}
		else if (resourceNames == null || resourceNames.isEmpty()) {
			matched = serviceName != null && serviceName.equalsIgnoreCase(arn.getService());
		}

		return matched;
	}

	boolean matchAccountMetaDataFilter(AccountMetadataFilter accountMetadataFilter, String accountId, String logtag) {
		boolean matched = true;
		AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
		List<Property> accountProperties = awsAccountServiceUtil.getAccountProperties(accountId, logtag);
		String propertyName = accountMetadataFilter.getPropertyName();
		@SuppressWarnings("unchecked")
		List<String> propertyValues = accountMetadataFilter.getPropertyValue();

		Optional<Property> accountPropertyOptional = accountProperties.stream()
				.filter(ap -> ap.getKey().equalsIgnoreCase(propertyName))
				.findAny();
		if (accountPropertyOptional.isPresent()) {
			Optional<String> valueOptional = propertyValues.stream()
					.filter(pv -> pv.equalsIgnoreCase(accountPropertyOptional.get().getValue()))
					.findAny();
			if (!valueOptional.isPresent()) {
				matched = false;
			}
		}

		return matched;
	}

	private String resourceTagMappingToString(String accountId, String region, List<ResourceTagMapping> mappings) {
		StringBuilder buf = new StringBuilder(2048);
		buf.append("\n******************** [").append(accountId).append("]:").append(region).append(" ********************").append("\n");

		mappings.forEach(m -> {
			buf.append("===> Resource ARN: ").append(m.getResourceARN()).append("\n");
			if (m.getTags().isEmpty()) {
				buf.append("    *** NO TAGS! ***").append("\n");
			}
			else {
				m.getTags().forEach(tag -> buf.append("    ").append(tag.getKey()).append(" -> ").append(tag.getValue()).append("\n"));
			}
		});

		buf.append("Total resources: ").append(mappings.size()).append("\n");

		return buf.toString();
	}

	@Override
	public String getBaseName() {
		return "TagPolicyViolation";
	}
}
