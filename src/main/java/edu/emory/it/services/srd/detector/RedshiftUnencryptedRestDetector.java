package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.redshift.AmazonRedshift;
import com.amazonaws.services.redshift.AmazonRedshiftClient;
import com.amazonaws.services.redshift.model.Cluster;
import com.amazonaws.services.redshift.model.DescribeClustersRequest;
import com.amazonaws.services.redshift.model.DescribeClustersResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Check all Redshift clusters to make sure encryption at rest is enabled.
 * See <a href="https://docs.aws.amazon.com/redshift/latest/mgmt/security-server-side-encryption.html">Encryption at rest</a>.
 *
 * @see edu.emory.it.services.srd.remediator.RedshiftUnencryptedRestRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RedshiftUnencryptedRestDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            AmazonRedshift client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRedshiftClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RedshiftUnencryptedRest,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            DescribeClustersRequest describeClustersRequest = new DescribeClustersRequest();
            DescribeClustersResult describeClustersResult;

            do {
                try {
                    describeClustersResult = client.describeClusters(describeClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RedshiftUnencryptedRest, srContext.LOGTAG,
                            "Error describing clusters", e);
                    return;
                }

                for (Cluster cluster : describeClustersResult.getClusters()) {
                    if (!cluster.isEncrypted()) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.RedshiftUnencryptedRest,
                                "arn:aws:redshift:" + region.getName() + ":" + accountId + ":cluster:" + cluster.getClusterIdentifier());
                    }
                }

                describeClustersRequest.setMarker(describeClustersResult.getMarker());
            } while (describeClustersResult.getMarker() != null);
        }
    }

    @Override
    public String getBaseName() {
        return "RedshiftUnencryptedRest";
    }
}
