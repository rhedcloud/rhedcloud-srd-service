package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.amazonaws.services.rds.model.DescribeOptionGroupsRequest;
import com.amazonaws.services.rds.model.DescribeOptionGroupsResult;
import com.amazonaws.services.rds.model.Option;
import com.amazonaws.services.rds.model.OptionGroup;
import com.amazonaws.services.rds.model.OptionGroupMembership;
import com.amazonaws.services.rds.model.OptionSetting;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.util.List;

/**
 * Detect Oracle RDS instances where encryption in-transit is not enabled.
 *
 * <p>See <a href="https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.Oracle.Options.NetworkEncryption.html">Oracle Native Network Encryption</a>.</p>
 *
 * @see edu.emory.it.services.srd.remediator.RdsOracleUnencryptedTransportRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsOracleUnencryptedTransportDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        // Oracle Native Network Encryption
        //   https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.Oracle.Options.NetworkEncryption.html

        for (Region region : getRegions()) {
            try {
                final AmazonRDS client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);

                String describeDBInstancesRequestMarker = null;
                do {
                    DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest()
                            .withMaxRecords(100);
                    // deal with pagination
                    if (describeDBInstancesRequestMarker != null)
                        describeDBInstancesRequest.setMarker(describeDBInstancesRequestMarker);
                    DescribeDBInstancesResult describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest);
                    describeDBInstancesRequestMarker = describeDBInstancesResult.getMarker();

                    List<DBInstance> dbInstances = describeDBInstancesResult.getDBInstances();
                    for (DBInstance dbInstance : dbInstances) {
                        // this detector is only for Oracle
                        if (!dbInstance.getEngine().startsWith("oracle")) {
                            continue;
                        }

                        boolean transportEncrypted = false;

                        optionGroupMemberships:
                        for (OptionGroupMembership optionGroupMembership : dbInstance.getOptionGroupMemberships()) {
                            String describeOptionGroupsResultMarker = null;
                            do {
                                DescribeOptionGroupsRequest describeOptionGroupsRequest = new DescribeOptionGroupsRequest()
                                        .withOptionGroupName(optionGroupMembership.getOptionGroupName());
                                if (describeOptionGroupsResultMarker != null)
                                    describeOptionGroupsRequest.setMarker(describeOptionGroupsResultMarker);

                                DescribeOptionGroupsResult describeOptionGroupsResult = client.describeOptionGroups(describeOptionGroupsRequest);
                                describeOptionGroupsResultMarker = describeOptionGroupsResult.getMarker();

                                for (OptionGroup optionGroup : describeOptionGroupsResult.getOptionGroupsList()) {
                                    for (Option option : optionGroup.getOptions()) {
                                        if (option.getOptionName().equals("NATIVE_NETWORK_ENCRYPTION")) {
                                            for (OptionSetting optionSetting : option.getOptionSettings()) {
                                                if (optionSetting.getName().equals("SQLNET.ENCRYPTION_SERVER")
                                                        && optionSetting.getValue().equals("REQUIRED")) {
                                                    transportEncrypted = true;
                                                    break optionGroupMemberships;
                                                }
                                            }
                                        }
                                    }
                                }
                            } while (describeOptionGroupsResultMarker != null);
                        }

                        if (!transportEncrypted) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsOracleUnencryptedTransport, dbInstance.getDBInstanceArn(),
                                    new SrdNameValuePair("DBInstanceIdentifier", dbInstance.getDBInstanceIdentifier()),
                                    new SrdNameValuePair("DBInstanceStatus", dbInstance.getDBInstanceStatus()));
                        }
                    }
                } while (describeDBInstancesRequestMarker != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsOracleUnencryptedTransport,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "RdsOracleUnencryptedTransport";
    }
}
