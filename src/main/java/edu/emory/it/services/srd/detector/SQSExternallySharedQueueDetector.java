package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.ListQueuesResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Detects SQS Queues that are externally shared<br>
 *
 * <p>The logic for check if a principal is external goes as follows, if any of the follow is true, the principal is external</p>
 * <ul>
 *     <li>principal is federated</li>
 *     <li>not principal is used</li>
 *     <li>principal = *</li>
 *     <li>principal arn starts with arn:aws:iam: and the account id is an external account</li>
 *     <li>principal arn starts with arn:aws:sts: and the account id is an external account</li>
 * </ul>
 *
 * @see edu.emory.it.services.srd.remediator.SQSExternallySharedQueueRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class SQSExternallySharedQueueDetector extends AbstractSecurityRiskDetector {
	private static final List<String> GET_QUEUE_ATTRIBUTES_NAMES = Arrays.asList("Policy", "QueueArn");

	@Override
	public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
		for (Region region : getRegions()) {
			final AmazonSQS sqs;
			try {
				sqs = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonSQSClient.class);
			}
			catch (EmoryAwsClientBuilderException e) {
				// The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
				logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
				continue;
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.SQSExternallySharedQueue,
						srContext.LOGTAG, "On region: " + region.getName(), e);
				return;
			}

			checkSQSQueues(sqs, detection, accountId, srContext.LOGTAG);
		}
	}

	private void checkSQSQueues(AmazonSQS sqs, SecurityRiskDetection detection, String accountId, String LOGTAG) {
		ListQueuesResult listQueuesResult;
		try {
			listQueuesResult = sqs.listQueues();
		}
		catch (Exception e) {
			setDetectionError(detection, DetectionType.SQSExternallySharedQueue,
					LOGTAG, "Error listing queues", e);
			return;
		}

		Set<String> allowlistedAccounts = Collections.singleton(accountId);


		for (String queueUrl : listQueuesResult.getQueueUrls()) {
			GetQueueAttributesResult getQueueAttributesResult;
			try {
				getQueueAttributesResult = sqs.getQueueAttributes(queueUrl, GET_QUEUE_ATTRIBUTES_NAMES);
			}
			catch (Exception e) {
				setDetectionError(detection, DetectionType.SQSExternallySharedQueue,
						LOGTAG, "Error getting queue attributes", e);
				return;
			}
			if (getQueueAttributesResult.getAttributes() != null) {
				String queueArn = getQueueAttributesResult.getAttributes().get("QueueArn");

				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
						.isExemptResourceArnForAccountAndDetector(queueArn,accountId,getBaseName());
				boolean exceptionStatusUnderminable = resourceIsExempt == null;
				if (exceptionStatusUnderminable) {
					logger.error(LOGTAG + "Exemption status of queue " + queueArn + " can not be determined on account " + accountId);
					continue;					
				}
				if (resourceIsExempt) {
					logger.info(LOGTAG + "Ignoring exempted role " + queueArn + " on account " + accountId);
					continue;
				}


				String policy = getQueueAttributesResult.getAttributes().get("Policy");
				if (AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(allowlistedAccounts, policy)) {                 
					addDetectedSecurityRisk(detection, LOGTAG, DetectionType.SQSExternallySharedQueue, queueArn);
				}
			}
		}
	}

	@Override
	public String getBaseName() {
		return "SQSExternallySharedQueue";
	}
}
