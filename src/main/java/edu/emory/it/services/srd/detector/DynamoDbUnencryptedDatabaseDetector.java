package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.DescribeTableResult;
import com.amazonaws.services.dynamodbv2.model.ListTablesResult;
import com.amazonaws.services.dynamodbv2.model.SSEDescription;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detect DynamoDB databases where encryption at-rest is not enabled.
 *
 * @see edu.emory.it.services.srd.remediator.DynamoDbUnencryptedDatabaseRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class DynamoDbUnencryptedDatabaseDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonDynamoDB client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonDynamoDBClient.class);

                ListTablesResult desc = client.listTables();

                for (String tableName : desc.getTableNames()) {
                    DescribeTableResult describeTableResult = client.describeTable(tableName);
                    TableDescription tableDescription = describeTableResult.getTable();

                    // check the server-side encryption setting
                    SSEDescription sseDescription = tableDescription.getSSEDescription();
                    if (sseDescription == null
                            || (!sseDescription.getStatus().equals("ENABLING")
                            && !sseDescription.getStatus().equals("ENABLED"))) {

                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.DynamoDbUnencryptedDatabase, tableDescription.getTableArn());
                    }
                }
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.DynamoDbUnencryptedDatabase,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "DynamoDbUnencryptedDatabase";
    }
}
