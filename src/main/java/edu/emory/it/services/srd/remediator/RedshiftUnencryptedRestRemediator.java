package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.redshift.AmazonRedshift;
import com.amazonaws.services.redshift.AmazonRedshiftClient;
import com.amazonaws.services.redshift.model.InvalidClusterStateException;
import com.amazonaws.services.redshift.model.ModifyClusterRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * A Redshift cluster should be encrypted during creation but if it wasn't,
 * the cluster will be modified to enable encryption at rest which forces an immediate "resizing" operation.<br>
 *
 * See <a href="https://docs.aws.amazon.com/redshift/latest/mgmt/changing-cluster-encryption.html">Changing cluster encryption</a>
 * and <a href="https://docs.aws.amazon.com/redshift/latest/mgmt/managing-cluster-operations.html#classic-resize">Classic resize</a>
 *
 * @see edu.emory.it.services.srd.detector.RedshiftUnencryptedRestDetector the detector
 */
public class RedshiftUnencryptedRestRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:redshift:", srContext.LOGTAG, DetectionType.RedshiftUnencryptedRest))
            return;
        // like arn:aws:redshift:us-east-1:123456789012:cluster:cluster-name
        Arn arn = remediatorCheckResourceName(detected, accountId, srContext.LOGTAG);
        if (arn == null)
            return;

        AmazonRedshift client;
        try {
            client = getClient(accountId, arn.getRegion(), srContext.getMetricCollector(), AmazonRedshiftClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error getting Redshift client");
            return;
        }

        try {
            ModifyClusterRequest modifyClusterRequest = new ModifyClusterRequest()
                    .withClusterIdentifier(arn.getResource().getResource())
                    .withEncrypted(true);
            client.modifyCluster(modifyClusterRequest);

            setSuccess(detected, srContext.LOGTAG, "Redshift cluster modified to enable encryption at rest.");
        }
        catch (InvalidClusterStateException e) {
            // a previous remediation may still be in progress
            setPostponed(detected, srContext.LOGTAG, "Unable to modify the Redshift cluster at this time to enable encryption at rest. " + e.getMessage());
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Unexpected error while modifying the Redshift cluster to enable encryption at rest", e);
        }
    }
}
