package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.SdkClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolRequest;
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolResult;
import com.amazonaws.services.cognitoidp.model.ListUserPoolsRequest;
import com.amazonaws.services.cognitoidp.model.ListUserPoolsResult;
import com.amazonaws.services.cognitoidp.model.PasswordPolicyType;
import com.amazonaws.services.cognitoidp.model.UserPoolDescriptionType;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects if a user pool's policy has non-complex passwords<br>
 *
 * <p>Password must have the following criteria:</p>
 * <ul>
 *     <li>Minimum length: greater or equal to 9</li>
 *     <li>Require numbers: Yes</li>
 *     <li>Require special character: Yes to all</li>
 *     <li>Require uppercase: Yes</li>
 *     <li>Require lowercase: Yes</li>
 * </ul>
 *
 * @see edu.emory.it.services.srd.remediator.CognitoUncomplexedPasswordsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CognitoUncomplexedPasswordsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AWSCognitoIdentityProvider cognito;
            try {
                cognito = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSCognitoIdentityProviderClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CognitoUncomplexedPasswords,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:cognito-idp:" + region.getName() + ":" + accountId + ":userpool/";
            checkPasswordComplexity(cognito, detection, arnPrefix, srContext.LOGTAG);
        }
    }

    private void checkPasswordComplexity(AWSCognitoIdentityProvider cognito, SecurityRiskDetection detection,
                                         String arnPrefix, String LOGTAG) {
        ListUserPoolsRequest request = new ListUserPoolsRequest().withMaxResults(25);
        boolean done = false;
        while (!done) {
            ListUserPoolsResult result;
            try {
                result = cognito.listUserPools(request);
            } catch (SdkClientException e) {
                break;  //Some regions fail on this call because it is not supported
            }

            for (UserPoolDescriptionType userPool : result.getUserPools()) {
                DescribeUserPoolRequest describeUserPoolRequest = new DescribeUserPoolRequest().withUserPoolId(userPool.getId());
                DescribeUserPoolResult describeUserPoolResult = cognito.describeUserPool(describeUserPoolRequest);
                PasswordPolicyType passwordPolicy = describeUserPoolResult.getUserPool().getPolicies().getPasswordPolicy();

                if ( passwordPolicy.getMinimumLength() == null
                        || passwordPolicy.getMinimumLength() < 9
                        || !Boolean.TRUE.equals(passwordPolicy.getRequireLowercase())
                        || !Boolean.TRUE.equals(passwordPolicy.getRequireUppercase())
                        || !Boolean.TRUE.equals(passwordPolicy.getRequireNumbers())
                        || !Boolean.TRUE.equals(passwordPolicy.getRequireSymbols())) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.CognitoUncomplexedPasswords, arnPrefix + userPool.getId());
                }
            }

            request.setNextToken(result.getNextToken());

            if (request.getNextToken() == null) {
                done = true;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "CognitoUncomplexedPasswords";
    }
}
