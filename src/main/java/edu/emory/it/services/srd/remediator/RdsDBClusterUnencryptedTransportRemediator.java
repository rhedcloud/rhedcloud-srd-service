package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by stopping the DB cluster.<br>
 * The snapshot identifier prefix is "rds-unencrypted-transport-snapshot-".
 *
 * <p>See the
 * {@linkplain AbstractRdsRemediator#remediateDBInstance(DetectedSecurityRisk, String, DetectionType, String, RemediationAction, SecurityRiskContext) remediateDBInstance}
 * method for addition information.</p>
 *
 * @see edu.emory.it.services.srd.remediator.RdsDBClusterUnencryptedTransportRemediator the remediator
 */
public class RdsDBClusterUnencryptedTransportRemediator extends AbstractRdsRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        /*
         * see comments in the detector, but basically, the information about in-transit encryption
         * isn't available until the cluster has been rebooted
         */
        if (detected.getType().equals(DetectionType.RdsDBClusterPendingReboot.name())) {
            setNotificationOnly(detected, srContext.LOGTAG);
            return;
        }

        super.remediateDBInstance(detected, accountId,
                DetectionType.RdsDBClusterUnencryptedTransport,
                "rds-unencrypted-transport-snapshot-",
                RemediationAction.STOP,
                srContext);
    }
}
