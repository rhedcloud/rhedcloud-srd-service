package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.AttachedPolicy;
import com.amazonaws.services.identitymanagement.model.GetAccountAuthorizationDetailsRequest;
import com.amazonaws.services.identitymanagement.model.GetAccountAuthorizationDetailsResult;
import com.amazonaws.services.identitymanagement.model.GroupDetail;
import com.amazonaws.services.identitymanagement.model.Policy;
import com.amazonaws.services.identitymanagement.model.RoleDetail;
import com.amazonaws.services.identitymanagement.model.UserDetail;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.ADMINISTRATOR_ROLE_NAME;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.CUSTOM_ROLE_IAM_PATH;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.RHEDCLOUD_IAM_PATH;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.AWS_RESERVED_IAM_PATH;;

/**
 * Look for IAM resources that are missing important RHEDcloud IAM policies.<br>
 * For HIPAA accounts, "RHEDcloudAdministratorRoleHipaaPolicy" must be attached.<br>
 * For both compliance class accounts (HIPAA and Standard), 
 * "RHEDcloudAdministratorRolePolicy" and "RHEDcloudManagementSubnetPolicy" (type1 or type2) must be attached.
 * The exception is for accounts that do not have a VPC (type 0): the "RHEDcloudAdministratorRolePolicy" must be attached
 * but not the "RHEDcloudManagementSubnetPolicy" because there are no subnets in a type 0 account.
 *
 * <p>For IAM users in the /rhedcloud/ path that are missing the policies, a security risk is identified but
 * see the special handling in the remediator.</p>
 *
 * <p>For IAM groups, the RHEDcloud service control policies (SCPs) prohibit the creation of groups but they
 * may be created and used for specific purposes by the Central Administrator.  In this case they will be put in
 * the /rhedcloud/ path and will be ignored by the detector.</p>
 *
 * <p>For IAM roles, roles in the /rhedcloud/ path will be ignored by the detector.  The exception is the
 * <code>RHEDcloudAdministratorRole</code> role which must have the required policies.<br>
 * Some roles are protected (like those in the /aws-service-role/ path) and can not be modified or deleted.
 * They will typically be added to the <code>ignoreArns</code> configuration property and be ignored.
 * If not, they will have a security risk identified, even though they fail remediation.<br>
 * For roles in the /customroles/ path, the "RHEDcloudAdministratorRolePolicy" policy is never attached.</p>
 *
 * @see edu.emory.it.services.srd.remediator.IamRestrictionPoliciesDetachedRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class IamRestrictionPoliciesDetachedDetector extends AbstractSecurityRiskDetector {

    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        try {
            final AmazonIdentityManagement iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);

            IamRestrictionPoliciesDetachedUtil.RestrictionPolicies policies = IamRestrictionPoliciesDetachedUtil.getRestrictionPolicies(iam);

            /*
             * At least the Administrator Role policy and one of the Management Subnet policies must exist.
             * If not then the account has not been configured correctly.
             * If the AdministratorRoleHipaa policy is found then the account must be HIPAA.
             */
            if (policies.RHEDcloudAdministratorRolePolicy == null) {
                setDetectionError(detection, DetectionType.IamRestrictionPoliciesDetached, srContext.LOGTAG,
                        "Missing required policy: RHEDcloudAdministratorRolePolicy");
                return;
            }

            Boolean isType0 = AwsAccountServiceUtil.getInstance().isType0(accountId);
            if (isType0 == null) {
                setDetectionError(detection, DetectionType.IamRestrictionPoliciesDetached, srContext.LOGTAG,
                        "vpcType is not set in this account.  Is this account currently being provisioned?");
                return;
            }
            if (isType0==false) {
                if ( policies.RHEDcloudManagementSubnetPolicies == null) {
                    setDetectionError(detection, DetectionType.IamRestrictionPoliciesDetached, srContext.LOGTAG,
                            "Missing required policy: RHEDcloudManagementSubnetPolicy");
                    return;
                }
            }
            else {
            	// it's a type 0, don't care about subnet stuff
            }
           
//            String complianceClass = AwsAccountServiceUtil.getInstance().accountCacheAccountComplianceClass(accountId, srContext.LOGTAG);
//            boolean isHIPAA = true;
//            if (complianceClass != null && complianceClass.equalsIgnoreCase("standard")) {
//            	isHIPAA = false;
//            }

            boolean isHIPAA = policies.RHEDcloudAdministratorRoleHipaaPolicy != null;

            // because of pagination, gather all details first
            List<UserDetail> userDetailList = new ArrayList<>();
            List<GroupDetail> groupDetailList = new ArrayList<>();
            List<RoleDetail> roleDetailList = new ArrayList<>();

            GetAccountAuthorizationDetailsRequest getAccountAuthorizationDetailsRequest = new GetAccountAuthorizationDetailsRequest();
            GetAccountAuthorizationDetailsResult getAccountAuthorizationDetailsResult;
            do {
                getAccountAuthorizationDetailsResult = iam.getAccountAuthorizationDetails(getAccountAuthorizationDetailsRequest);
                userDetailList.addAll(getAccountAuthorizationDetailsResult.getUserDetailList());
                groupDetailList.addAll(getAccountAuthorizationDetailsResult.getGroupDetailList());
                roleDetailList.addAll(getAccountAuthorizationDetailsResult.getRoleDetailList());
                getAccountAuthorizationDetailsRequest.setMarker(getAccountAuthorizationDetailsResult.getMarker());
            } while (getAccountAuthorizationDetailsResult.isTruncated());

            /*
             * authorization details give information about users, groups and roles.
             * the rules for each are pretty straight forward except, users can have the policy
             * either directly or through group membership and resources created by the RHEDcloud Cloud
             * Formation template generally don't need to be checked.
             */
            for (UserDetail userDetail : userDetailList) {
                // users in the /rhedcloud/ path that don't have the policies attached are added as a risk but
                // are not remediated.  see the remediator for additional details.

                boolean hasAdminPolicy = hasAttachedManagedPolicy(userDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRolePolicy);
                boolean hasAdminHipaaPolicy = isHIPAA && hasAttachedManagedPolicy(userDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRoleHipaaPolicy);
                boolean hasSubnetPolicy = hasAttachedManagedPolicy(userDetail.getAttachedManagedPolicies(), policies.RHEDcloudManagementSubnetPolicies);

                // if the user doesn't have the policy attached directly they could have it through group membership
                for (String groupNameContainingUser : userDetail.getGroupList()) {
                    for (GroupDetail groupDetail : groupDetailList) {
                        if (groupNameContainingUser.equals(groupDetail.getGroupName())) {
                            if (!hasAdminPolicy
                                    && hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRolePolicy))
                                hasAdminPolicy = true;  // has policy through group
                            if (!hasAdminHipaaPolicy && isHIPAA
                                    && hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRoleHipaaPolicy))
                                hasAdminHipaaPolicy = true;  // has policy through group
                            if (!hasSubnetPolicy
                                    && hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudManagementSubnetPolicies))
                                hasSubnetPolicy = true;  // has policy through group
                        }
                    }
                }

                if ( hasAdminPolicy == false 
                		|| (hasAdminHipaaPolicy == false && isHIPAA) 
                		|| (hasSubnetPolicy == false && isType0 != true)) {
    				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
    						.isExemptResourceArnForAccountAndDetector(userDetail.getArn(),accountId,getBaseName());
    				boolean exceptionStatusUnderminable = resourceIsExempt == null;
    				if (exceptionStatusUnderminable) {
    					logger.error(srContext.LOGTAG + "Exemption status of role " + userDetail.getArn() + " can not be determined on account " + accountId);
    				} else if (resourceIsExempt) {
        				logger.info(srContext.LOGTAG + "Ignoring exempted role with missing restriction policies" 
        					+ userDetail.getArn() + " on account " + accountId);
        			} else {
                        newRisk(detection, srContext, userDetail.getArn(),
                                userDetail.getUserName(), null, null, userDetail.getPath(),
                                isHIPAA, hasAdminPolicy, hasAdminHipaaPolicy, hasSubnetPolicy, isType0);
                    }
                }
            }

            /*
             * The RHEDcloud service control policies (SCPs) prohibit the creation of groups.
             *
             * But groups could be used by central IT where they might like to use a Group
             * as a container for long-term key pair users in an account that have the same policy.
             * In this case they will put the group in the /rhedcloud/ path (and ignored).
             *
             * All other groups will be deleted.
             */

            for (GroupDetail groupDetail : groupDetailList) {
                if (RHEDCLOUD_IAM_PATH.equals(groupDetail.getPath())) {
                    // these groups are reserved for RHEDcloud purposes and are exempt
                    continue;
                }

                boolean hasAdminPolicy = hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRolePolicy);
                boolean hasAdminHipaaPolicy = isHIPAA && hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRoleHipaaPolicy);
                boolean hasSubnetPolicy = hasAttachedManagedPolicy(groupDetail.getAttachedManagedPolicies(), policies.RHEDcloudManagementSubnetPolicies);

                if ( hasAdminPolicy  == false
                		|| ( hasAdminHipaaPolicy == false && isHIPAA ) 
                		|| ( hasSubnetPolicy == false && isType0 != true )) {
    				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
    						.isExemptResourceArnForAccountAndDetector(groupDetail.getArn(),accountId,getBaseName());
    				boolean exceptionStatusUnderminable = resourceIsExempt == null;
    				if (exceptionStatusUnderminable) {
    					logger.error(srContext.LOGTAG + "Exemption status of group " + groupDetail.getArn() + " can not be determined on account " + accountId);					
    				} else if (resourceIsExempt) {
                        logger.debug(srContext.LOGTAG + "Ignoring exempted group with missing restriction policies " + groupDetail.getGroupName() + " on account " + accountId);
                    } else {
                        newRisk(detection, srContext, groupDetail.getArn(),
                                null, groupDetail.getGroupName(), null, groupDetail.getPath(),
                                isHIPAA, hasAdminPolicy, hasAdminHipaaPolicy, hasSubnetPolicy, isType0);
                    }
                }
            }

            for (RoleDetail roleDetail : roleDetailList) {
                /*
                 * RHEDcloudAdministratorRole must have the policies so should be checked and remediated.
                 * All other /rhedcloud/ roles are exempt, including the RHEDcloudCentralAdministratorRole
                 * which is only assumed by account administrators (i.e., Cloud Engineers)
                 */
                if ((RHEDCLOUD_IAM_PATH.equals(roleDetail.getPath()) && 
                	!ADMINISTRATOR_ROLE_NAME.equals(roleDetail.getRoleName())) ||
                	(AWS_RESERVED_IAM_PATH.equals(roleDetail.getPath()))) {
                    continue;
                }

                /*
                 * Custom roles (in the /customroles/ path) created by a user must have the RHEDcloudManagementSubnetPolicy policy,
                 * and if it's a HIPAA account, they must have the the RHEDcloudAdministratorRoleHipaaPolicy policy.
                 * But they never have the RHEDcloudAdministratorRolePolicy.
                 */

                boolean hasAdminPolicy = hasAttachedManagedPolicy(roleDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRolePolicy);
                boolean hasAdminHipaaPolicy = isHIPAA && hasAttachedManagedPolicy(roleDetail.getAttachedManagedPolicies(), policies.RHEDcloudAdministratorRoleHipaaPolicy);
                boolean hasSubnetPolicy = !isType0 && hasAttachedManagedPolicy(roleDetail.getAttachedManagedPolicies(), policies.RHEDcloudManagementSubnetPolicies);
                boolean isCustomRole = CUSTOM_ROLE_IAM_PATH.equals(roleDetail.getPath());

//                logger.info("TEMP_DEBUG("+accountId+"/" + roleDetail.getRoleName() + " ): hasAdminPolicy: " + hasAdminPolicy
//                		+ " isCustomRole: " + isCustomRole
//                		+ " hasAdminHipaaPolicy: " + hasAdminHipaaPolicy
//                		+ " isHIPAA: " + isHIPAA
//                		+ " hasSubnetPolicy: " + hasSubnetPolicy 
//                		+ " isType0: " + isType0);
                
                if ( ( hasAdminPolicy == false && isCustomRole == false) 
                		|| ( hasAdminHipaaPolicy == false && isHIPAA) 
                		|| ( hasSubnetPolicy == false && isType0 != true )) {
    				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
    						.isExemptResourceArnForAccountAndDetector(roleDetail.getArn(),accountId,getBaseName());
    				boolean exceptionStatusUnderminable = resourceIsExempt == null;
    				if (exceptionStatusUnderminable) {
    					logger.error(srContext.LOGTAG + "Exemption status of role " + roleDetail.getArn() + " can not be determined on account " + accountId);
    										
    				} else if (resourceIsExempt) {
                       logger.debug(srContext.LOGTAG + "Ignoring exempted role with missing restriction policies " + roleDetail.getRoleName() + " on account " + accountId);
                    } else {
                        newRisk(detection, srContext, roleDetail.getArn(),
                                null, null, roleDetail.getRoleName(), roleDetail.getPath(),
                                isHIPAA, hasAdminPolicy, hasAdminHipaaPolicy, hasSubnetPolicy, isType0);
                    }
                }
            }
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
        }
        catch (Exception e) {
        	e.printStackTrace();
            setDetectionError(detection, DetectionType.IamRestrictionPoliciesDetached,
                    srContext.LOGTAG, "On region: global", e);
        }
    }


    private static boolean hasAttachedManagedPolicy(List<AttachedPolicy> attachedManagedPolicies, Policy... policies) {
    	if (policies == null) return false;
        for (Policy policy : policies) {
            boolean found = false;
            for (AttachedPolicy attachedPolicy : attachedManagedPolicies) {
                if (policy.getPolicyName().equals(attachedPolicy.getPolicyName())) {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }
        return true;
    }

    private void newRisk(SecurityRiskDetection detection, SecurityRiskContext srContext, String arn,
                         String userName, String groupName, String roleName, String path,
                         boolean isHIPAA,
                         boolean hasAdminPolicy, boolean hasAdminHipaaPolicy, boolean hasSubnetPolicy, Boolean isType0) {

        // pass some information to the remediator
        List<SrdNameValuePair> properties = new ArrayList<>();
        properties.add(new SrdNameValuePair("isHIPAA", String.valueOf(isHIPAA)));
        properties.add(new SrdNameValuePair("isType0", String.valueOf(isType0)));
        properties.add(new SrdNameValuePair("missingAdminPolicy", String.valueOf(!hasAdminPolicy)));
        properties.add(new SrdNameValuePair("missingAdminHipaaPolicy", String.valueOf(!hasAdminHipaaPolicy)));
        properties.add(new SrdNameValuePair("missingSubnetPolicy", String.valueOf(!hasSubnetPolicy)));
        if (userName != null)
            properties.add(new SrdNameValuePair("userName", userName));
        if (groupName != null)
            properties.add(new SrdNameValuePair("groupName", groupName));
        if (roleName != null)
            properties.add(new SrdNameValuePair("roleName", roleName));
        properties.add(new SrdNameValuePair("path", path));

        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                DetectionType.IamRestrictionPoliciesDetached, arn, properties.toArray(new SrdNameValuePair[0]));
    }

    @Override
    public String getBaseName() {
        return "IamRestrictionPoliciesDetached";
    }
}
