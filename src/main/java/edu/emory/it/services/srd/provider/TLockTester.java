package edu.emory.it.services.srd.provider;

import edu.emory.it.services.srd.util.SpringIntegration;
import org.openeai.utils.lock.UuidKey;

/**
 * Non-production code used to test the Spring Data JPA integration.
 */
public class TLockTester {
    public static void main(String[] args) {
        SpringIntegration springIntegration = SpringIntegration.getInstance();
        springIntegration.initialize("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/rhedcloud_srd_dev",
                "rhedcloud_dev", "g9VAq37Q9qXF4a9XyXxEB2Ap7Ug", "edu.emory.it.services.srd");
        //AbstractApplicationContext context = new AnnotationConfigApplicationContext("edu.emory.it.services.srd");
        TLockService service = springIntegration.getBean(TLockService.class);
        TLockRepository repository = springIntegration.getBean(TLockRepository.class);

        System.out.println("=== test 1 ========================================================================");
        System.out.println("counted (r) " + repository.countByLockNameLike("%") + " T_LOCK rows");
        System.out.println("=== test 2 ========================================================================");
        System.out.println("counted (s) " + service.countByLockNameLike("%") + " T_LOCK rows");
        System.out.println("=== test 3 ========================================================================");
        System.out.println("counted (k) " + service.countByLockNameLike("KHB_%") + " T_LOCK rows");
        System.out.println("=== test 4 ========================================================================");
        repository.findAll().forEach(t -> System.out.println("found T_LOCK " + t));
        System.out.println("=== test 5 ========================================================================");
        repository.findAll().stream().filter(t -> t.getLockName().startsWith("KHB_")).forEach(repository::delete);
        System.out.println("=== test 6 ========================================================================");
        UuidKey uuidKey = new UuidKey();
        TLock tlock = new TLock();
        tlock.setLockName("KHB_testing");
        tlock.setKeyValue(uuidKey.getValue());
        tlock.setCreateUser("khb");
        TLock savedTlock = repository.save(tlock);
        System.out.println("saved T_LOCK " + savedTlock); // CreateDate should have been set for us
        System.out.println("=== test X ========================================================================");
        //repository.deleteByLockNameAndKeyValue("KHB_testing", uuidKey.getValue());
    }
}
