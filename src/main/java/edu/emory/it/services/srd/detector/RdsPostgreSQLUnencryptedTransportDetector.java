package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DBParameterGroup;
import com.amazonaws.services.rds.model.DBParameterGroupStatus;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.amazonaws.services.rds.model.DescribeDBParameterGroupsRequest;
import com.amazonaws.services.rds.model.DescribeDBParameterGroupsResult;
import com.amazonaws.services.rds.model.DescribeDBParametersRequest;
import com.amazonaws.services.rds.model.DescribeDBParametersResult;
import com.amazonaws.services.rds.model.Parameter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.util.List;

/**
 * Detect PostgreSQL RDS instances where encryption in-transit is not enabled.
 *
 * @see edu.emory.it.services.srd.remediator.RdsPostgreSQLUnencryptedTransportRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsPostgreSQLUnencryptedTransportDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonRDS client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);

                String describeDBInstancesRequestMarker = null;
                do {
                    DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest()
                            .withMaxRecords(100);
                    // deal with pagination
                    if (describeDBInstancesRequestMarker != null)
                        describeDBInstancesRequest.setMarker(describeDBInstancesRequestMarker);
                    DescribeDBInstancesResult describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest);
                    describeDBInstancesRequestMarker = describeDBInstancesResult.getMarker();

                    List<DBInstance> dbInstances = describeDBInstancesResult.getDBInstances();
                    for (DBInstance dbInstance : dbInstances) {
                        // this detector is only for Postgres
                        if (!dbInstance.getEngine().equals("postgres")) {
                            continue;
                        }

                        boolean transportEncrypted = false;

                        dbParameterGroups:
                        for (DBParameterGroupStatus dbParameterGroupStatus : dbInstance.getDBParameterGroups()) {

                            String describeDBParameterGroupsRequestMarker = null;
                            do {
                                DescribeDBParameterGroupsRequest describeDBParameterGroupsRequest = new DescribeDBParameterGroupsRequest()
                                        .withDBParameterGroupName(dbParameterGroupStatus.getDBParameterGroupName());
                                if (describeDBParameterGroupsRequestMarker != null)
                                    describeDBParameterGroupsRequest.setMarker(describeDBParameterGroupsRequestMarker);

                                DescribeDBParameterGroupsResult describeDBParameterGroupsResult = client.describeDBParameterGroups(describeDBParameterGroupsRequest);
                                describeDBParameterGroupsRequestMarker = describeDBParameterGroupsResult.getMarker();

                                for (DBParameterGroup dbParameterGroup : describeDBParameterGroupsResult.getDBParameterGroups()) {

                                    String describeDBParametersRequestMarker = null;
                                    do {
                                        DescribeDBParametersRequest describeDBParametersRequest = new DescribeDBParametersRequest()
                                                .withDBParameterGroupName(dbParameterGroup.getDBParameterGroupName());
                                        if (describeDBParametersRequestMarker != null)
                                            describeDBParametersRequest.setMarker(describeDBParametersRequestMarker);

                                        DescribeDBParametersResult describeDBParametersResult = client.describeDBParameters(describeDBParametersRequest);
                                        describeDBParametersRequestMarker = describeDBParametersResult.getMarker();

                                        for (Parameter parameter : describeDBParametersResult.getParameters()) {
                                            if ("rds.force_ssl".equals(parameter.getParameterName())
                                                    && "1".equals(parameter.getParameterValue())) {
                                                transportEncrypted = true;
                                                break dbParameterGroups;
                                            }
                                        }
                                    } while (describeDBParametersRequestMarker != null);
                                }
                            } while (describeDBParameterGroupsRequestMarker != null);
                        }

                        if (!transportEncrypted) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                    DetectionType.RdsPostgreSQLUnencryptedTransport, dbInstance.getDBInstanceArn(),
                                    new SrdNameValuePair("DBInstanceIdentifier", dbInstance.getDBInstanceIdentifier()),
                                    new SrdNameValuePair("DBInstanceStatus", dbInstance.getDBInstanceStatus()));
                        }
                    }
                } while (describeDBInstancesRequestMarker != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsPostgreSQLUnencryptedTransport,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "RdsPostgreSQLUnencryptedTransport";
    }
}
