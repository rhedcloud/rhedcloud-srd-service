package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.sagemaker.AmazonSageMaker;
import com.amazonaws.services.sagemaker.AmazonSageMakerClient;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceRequest;
import com.amazonaws.services.sagemaker.model.DescribeNotebookInstanceResult;
import com.amazonaws.services.sagemaker.model.ListNotebookInstancesRequest;
import com.amazonaws.services.sagemaker.model.ListNotebookInstancesResult;
import com.amazonaws.services.sagemaker.model.NotebookInstanceSummary;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Checks for SageMaker resources not located within provisioned VPC.
 *
 * <p>When a SageMaker notebook instance is created without putting it in a VPC subnet, AWS defaults to this:
 * <code>Your notebook instance will be provided with SageMaker provided internet access because a VPC setting is not specified.</code>
 * But that is not a secure setup for our purposes.</p>
 *
 * @see edu.emory.it.services.srd.remediator.SageMakerVPCMisconfigurationRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class SageMakerVPCMisconfigurationDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                AmazonSageMaker sageMakerClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonSageMakerClient.class);
                inspectNotebookInstances(detection, srContext, sageMakerClient);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.SageMakerVPCMisconfiguration,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    private void inspectNotebookInstances(SecurityRiskDetection detection, SecurityRiskContext srContext,
                                          AmazonSageMaker sageMakerClient) {
        ListNotebookInstancesRequest listNotebookInstancesRequest = new ListNotebookInstancesRequest();
        ListNotebookInstancesResult listNotebookInstancesResult;
        do {
            listNotebookInstancesResult = sageMakerClient.listNotebookInstances(listNotebookInstancesRequest);

            for (NotebookInstanceSummary notebookInstanceSummary : listNotebookInstancesResult.getNotebookInstances()) {
                DescribeNotebookInstanceRequest describeNotebookInstanceRequest = new DescribeNotebookInstanceRequest()
                        .withNotebookInstanceName(notebookInstanceSummary.getNotebookInstanceName());
                DescribeNotebookInstanceResult describeNotebookInstanceResult
                        = sageMakerClient.describeNotebookInstance(describeNotebookInstanceRequest);

                if (describeNotebookInstanceResult.getSubnetId() == null) {
                    addDetectedSecurityRisk(detection, srContext.LOGTAG,
                            DetectionType.SageMakerVPCMisconfiguration, describeNotebookInstanceResult.getNotebookInstanceArn());
                }
            }

            listNotebookInstancesRequest.setNextToken(listNotebookInstancesResult.getNextToken());
        } while (listNotebookInstancesResult.getNextToken() != null);
    }

    @Override
    public String getBaseName() {
        return "SageMakerVPCMisconfiguration";
    }
}
