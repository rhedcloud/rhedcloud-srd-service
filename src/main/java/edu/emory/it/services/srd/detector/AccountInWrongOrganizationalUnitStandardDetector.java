package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import edu.emory.it.services.srd.ComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detect if the account is not in the Standard AWS Organization when it should be.<br>
 *
 * @see edu.emory.it.services.srd.remediator.AccountInWrongOrganizationalUnitStandardRemediator the remediator
 */
@StandardComplianceClass
public class AccountInWrongOrganizationalUnitStandardDetector extends AccountInWrongOrganizationalUnitBaseDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        super.detect(detection, accountId, srContext, ComplianceClass.Standard);
    }

    @Override
    public String getBaseName() {
        return "AccountInWrongOrganizationalUnitStandard";
    }
}
