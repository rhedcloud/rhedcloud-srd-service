package edu.emory.it.services.srd.util;

import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition;
import com.amazonaws.util.AWSRequestMetrics;
import com.amazonaws.util.AWSRequestMetricsFullSupport;
import org.apache.logging.log4j.Logger;
import org.openeai.utils.lock.Key;
import org.openeai.utils.lock.Lock;
import org.openeai.utils.lock.LockAlreadySetException;
import org.openeai.utils.lock.LockException;

import java.util.Objects;

public class SecurityRiskContext {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskContext.class);

    // do not prefix with SRD_ or else they will be counted as running detectors
    // see HealthSecurityRiskDetectionMetricQueryProvider.TotalDetectorsExecutingQuery
    private static final String OU_SHUFFLE_LOCK_NAME = "OU_Shuffle";

    // top level metrics are ThreadDelay and DetectorExecution
    private AWSRequestMetrics threadDelayMetric;
    private AWSRequestMetrics detectorExecutionMetric;
    private final SrdMetricCollector metricCollector;

    // public to avoid tedium of getters
    public final String LOGTAG;
    public final Lock detectorLock;
    public final SecurityRiskDetectionRequisition requisition;

    public static final String ERR_CODE_DETECTOR_ALREADY_RUNNING = "SrdService-1030";
    public static final String ERR_CODE_SKIPPED_SRD_EXEMPT = "SrdService-1035";  // not really an error
    public static final String ERR_CODE_UNCAUGHT_AMAZON_SERVICE_EXCEPTION = "SrdService-1040";
    public static final String ERR_CODE_UNCAUGHT_AWS_SDK_EXCEPTION = "SrdService-1050";
    public static final String ERR_CODE_UNCAUGHT_EXCEPTION = "SrdService-1060";
    public static final String ERR_CODE_MISSING_FROM_APP_CONFIG = "OpenEAI-1002";
    public static final String ERR_CODE_ENTERPRISE_FIELD_EXCEPTION = "OpenEAI-1010";
    public static final String ERR_CODE_INVALID_DETECTOR = "OpenEAI-1013";
    public static final String ERR_CODE_ENTERPRISE_LAYOUT_EXCEPTION = "OpenEAI-1020";
    public static final String ERR_CODE_ENTERPRISE_OBJECT_SYNC_EXCEPTION = "OpenEAI-1030";
    public static final String ERR_CODE_JMS_EXCEPTION = "OpenEAI-1040";
    public static final String ERR_CODE_LOCK_EXCEPTION = "OpenEAI-1050";

    public String errCode;
    public String errDesc;


    public SecurityRiskContext(String LOGTAG, Lock detectorLock, SecurityRiskDetectionRequisition requisition) {
        this.LOGTAG = Objects.requireNonNull(LOGTAG);
        this.metricCollector = new SrdMetricCollector(LOGTAG);
        this.detectorLock = Objects.requireNonNull(detectorLock);
        this.requisition = Objects.requireNonNull(requisition);
    }

    public SrdMetricCollector getMetricCollector() { return metricCollector; }

    public Key acquireOuShuffleLock(String accountId) {
        long intraTime = System.currentTimeMillis();

        String lockName = formOuShuffleLockName(accountId);

        for (int retry = 0; retry < 100; ++retry) {
            try {
                Key key = detectorLock.set(lockName);
                logger.info(LOGTAG + "Success acquiring the " + lockName + " lock " + key.getValue()
                        + " took " + (System.currentTimeMillis() - intraTime) + " ms");
                return key;
            }
            catch (LockException e) {
                logger.info(LOGTAG + "Error acquiring the " + lockName + " lock on retry " + retry
                        + ". The exception is: " + e.getMessage());
            }
            catch (LockAlreadySetException e) {
                logger.info(LOGTAG + "Lock already set while acquiring the " + lockName + " lock on retry " + retry);
            }
            try {
                Thread.sleep(3000);
            }
            catch (InterruptedException e) {
                logger.error(LOGTAG + "Interrupted while acquiring the " + lockName + " lock on retry " + retry);
                break;
            }
        }

        logger.error(LOGTAG + "Failed acquiring the " + lockName + " lock");
        return null;
    }

    public Key tryAcquireOuShuffleLock(String accountId) {
        long intraTime = System.currentTimeMillis();

        String lockName = formOuShuffleLockName(accountId);

        detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorLockSet);
        try {
            Key key = detectorLock.set(lockName);
            logger.info(LOGTAG + "Success acquiring the " + lockName + " lock " + key.getValue()
                    + " took " + (System.currentTimeMillis() - intraTime) + " ms");
            return key;
        }
        catch (LockAlreadySetException e) {
            logger.info(LOGTAG + "Lock already set while acquiring the " + lockName + " lock");
        }
        catch (Exception e) {
            logger.info(LOGTAG + "Error acquiring the " + lockName + " lock"
                    + ". The exception is: " + e.getMessage());
        }
        finally {
            detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorLockSet);
        }

        // we tried but could not get the lock
        return null;
    }

    public void releaseOuShuffleLock(String accountId, Key key) {
        if (key == null)
            return;

        long intraTime = System.currentTimeMillis();
        String lockName = formOuShuffleLockName(accountId);

        detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorLockRelease);
        try {
            detectorLock.release(lockName, key);
            logger.info(LOGTAG + "Success de-acquiring the " + lockName + " lock " + key.getValue()
                    + " took " + (System.currentTimeMillis() - intraTime) + " ms");
        }
        catch (LockException e) {
            logger.warn(LOGTAG + "Error de-acquiring the " + lockName + " lock " + key.getValue()
                    + ". The exception is: " + e.getMessage());
        }
        finally {
            detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorLockRelease);
        }
    }

    private String formOuShuffleLockName(String accountId) {
        return OU_SHUFFLE_LOCK_NAME + "_" + accountId;
    }

    public void reschedule() {
        this.errCode = null;
        this.errDesc = null;
    }

    public void withApplicationError(String errCode, String errDesc) {
        this.errCode = errCode;
        this.errDesc = errDesc;
    }

    public boolean hasApplicationError() {
        return this.errCode != null;
    }
    public boolean hasERR_CODE_DETECTOR_ALREADY_RUNNING() {
        return this.errCode != null && this.errCode.equals(ERR_CODE_DETECTOR_ALREADY_RUNNING);
    }
    public boolean hasERR_CODE_SKIPPED_SRD_EXEMPT() {
        return this.errCode != null && this.errCode.equals(ERR_CODE_SKIPPED_SRD_EXEMPT);
    }

    // top level metrics are ThreadDelay and DetectorExecution
    // the rest are nested events
    public void startThreadDelayEvent() {
        threadDelayMetric = new AWSRequestMetricsFullSupport();
        threadDelayMetric.startEvent(SrdMetricTypeField.SRD_ThreadDelay);
    }
    public void endThreadDelayEvent() {
        threadDelayMetric.endEvent(SrdMetricTypeField.SRD_ThreadDelay);
        threadDelayMetric.getTimingInfo().endTiming();
        metricCollector.collectMetrics(threadDelayMetric);
    }
    public void startDetectorExecutionEvent() {
        detectorExecutionMetric = new AWSRequestMetricsFullSupport();
        detectorExecutionMetric.startEvent(SrdMetricTypeField.SRD_DetectorExecution);
    }
    public void endDetectorExecutionEvent() {
        detectorExecutionMetric.endEvent(SrdMetricTypeField.SRD_DetectorExecution);
        detectorExecutionMetric.getTimingInfo().endTiming();
        metricCollector.collectMetrics(detectorExecutionMetric);
    }
    public void detectorExecutionStartEvent(SrdMetricTypeField metricType) {
        detectorExecutionMetric.startEvent(metricType);
    }
    public void detectorExecutionEndEvent(SrdMetricTypeField metricType) {
        detectorExecutionMetric.endEvent(metricType);
    }
    public void detectorExecutionSetCounter(SrdMetricTypeField metricType, long count) {
        detectorExecutionMetric.setCounter(metricType, count);
    }
    public void detectorExecutionIncrementCounter(SrdMetricTypeField metricType) {
        detectorExecutionMetric.incrementCounter(metricType);
    }
}
