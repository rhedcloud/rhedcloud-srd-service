package edu.emory.it.services.srd.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Splunk implementation of the
 * Security Risk Detection to Security Information and Event Management (SIEM) Integration.
 * <p>
 * HEC is the standard and least complicated method of sending data to Splunk so forms a general solution.
 * Also, the message format is general and directly maps the SecurityRiskDetection object contents so is
 * the default format.  If a different format is needed, the XmlLayout mechanism in OpenEAI can be leveraged.
 * </p>
 * <p>
 * See <a href="https://docs.splunk.com/Documentation/Splunk/latest/Data/UsetheHTTPEventCollector">HTTP Event Collector</a>
 * for more information about HEC.
 * </p>
 *
 * The following AppConfig settings are used:
 * <ul>
 *     <li>Required - <pre>hecEndpoint</pre> - the Splunk HTTP Event Collector REST API endpoint.</li>
 *     <li>Required - <pre>hecToken</pre> - the Splunk authentication token.</li>
 * </ul>
 */
public class SplunkSiemIntegrationProvider implements SiemIntegrationProvider {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SplunkSiemIntegrationProvider.class);
    private static final String LOGTAG = "[SplunkSiemIntegrationProvider] ";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private PoolingHttpClientConnectionManager httpClientConnectionManager;
    private CloseableHttpClient httpClient;
    private final AtomicLong httpClientExecuteInProgressCount = new AtomicLong();

    /** Required AppConfig - the Splunk HTTP Event Collector REST API endpoint */
    private String hecEndpoint;
    /** Required AppConfig - the Splunk authentication token */
    private String hecToken;


    @Override
    public void init(Properties properties) throws InstantiationException {
        hecEndpoint = properties.getProperty("HECEndpoint");
        if (hecEndpoint == null || hecEndpoint.equals("")) {
            String errMsg = "SIEM integration - no HECEndpoint property specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        hecToken = properties.getProperty("HECToken");
        if (hecToken == null || hecToken.equals("")) {
            String errMsg = "SIEM integration - no HECToken property specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // for our purposes, there is a single route (the HEC endpoint) with lots of concurrent requests
        httpClientConnectionManager = new PoolingHttpClientConnectionManager();
        httpClientConnectionManager.setMaxTotal(100);
        httpClientConnectionManager.setDefaultMaxPerRoute(100);
        httpClient = HttpClients.custom().setConnectionManager(httpClientConnectionManager).build();

        IdleConnectionMonitorThread t = new IdleConnectionMonitorThread("Splunk-IdleConnectionMonitor", httpClientConnectionManager);
        t.start();

        logger.info(LOGTAG + "SIEM integration - initializing Complete");
    }

    /**
     * Send the event to Splunk using the HTTP Event Collector (HEC).<br>
     * The event data is sent as a JSON message in the HTTP request (not using raw requests).
     * Event data is assigned to the "event" key within the JSON object.<br>
     * Event metadata can also be added but we don't need any just yet because Splunk can add it based on the HEC token.<br>
     * See here for details on the <a
     * href="https://docs.splunk.com/Documentation/Splunk/latest/Data/FormateventsforHTTPEventCollector">HEC event format</a>.
     *
     * @param siemJson data
     * @throws ProviderException on error
     */
    public void sendSiem(ObjectNode siemJson) throws ProviderException {
        ObjectNode splunkEvent = objectMapper.createObjectNode();
        splunkEvent.set("event", siemJson);

        String splunkEventString;
        try {
            splunkEventString = objectMapper.writeValueAsString(splunkEvent);
        }
        catch (JsonProcessingException e) {
            String msg = "SIEM integration error serializing JSON with event data " + splunkEvent.toString();
            logger.error(LOGTAG + msg, e);
            throw new ProviderException(msg, e);
        }

        HttpPost post;
        try {
            post = new HttpPost(hecEndpoint);
            post.addHeader("Authorization", "Splunk " + hecToken);
            post.setEntity(new StringEntity(splunkEventString));
        }
        catch (UnsupportedEncodingException e) {
            String msg = "SIEM integration error setting entity for POST with event data " + splunkEventString;
            logger.error(LOGTAG + msg, e);
            throw new ProviderException(msg, e);
        }

        /*
         * Success returns status code 200 with a response body like: {"text":"Success","code":0}
         * Errors return a status code in the 400 and 500 ranges with a body like: {"text":"Data channel is missing","code":10}
         *   The http client already does retries for certain errors so don't retry here
         */
        final long atomic = httpClientExecuteInProgressCount.incrementAndGet();
        try (CloseableHttpResponse response = httpClient.execute(post)) {
            int statusCode = response.getStatusLine().getStatusCode();

            String msg = "SIEM integration POSTed with status code " + statusCode + " with event data " + splunkEventString
                    + " with pool stats " + httpClientConnectionManager.getTotalStats().toString() + " and in progress " + atomic;
            if (response.getEntity() != null) {
                msg += " and response " + EntityUtils.toString(response.getEntity());
            }

            if (statusCode == 200) {
                logger.info(LOGTAG + msg);
            }
            else {
                logger.error(LOGTAG + msg);
                throw new ProviderException(msg);
            }
        }
        catch (ProviderException e) {
            throw e;  // message already logged above so just rethrow
        }
        catch (Exception e) {
            String msg = "SIEM integration POST failed with event data " + splunkEventString
                    + " with pool stats " + httpClientConnectionManager.getTotalStats().toString() + " and in progress " + atomic
                    + ".  The exception is: " + e.getMessage();
            logger.error(LOGTAG + msg, e);
            throw new ProviderException(msg, e);
        }
        finally {
            httpClientExecuteInProgressCount.decrementAndGet();
        }
    }
}
