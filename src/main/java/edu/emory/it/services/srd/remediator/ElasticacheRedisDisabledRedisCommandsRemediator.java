package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Delete the cluster.
 *
 * @see edu.emory.it.services.srd.detector.ElasticacheRedisDisabledRedisCommandsDetector the detector
 */
public class ElasticacheRedisDisabledRedisCommandsRemediator extends ElastiCacheBaseRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediate(accountId, detected, srContext, DetectionType.ElasticacheRedisDisabledRedisCommands, ElastiCacheBaseRemediator.RemediationMode.DELETE);
    }
}
