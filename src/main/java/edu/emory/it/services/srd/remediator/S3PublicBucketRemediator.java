package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AccessControlList;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetPublicAccessBlockRequest;
import com.amazonaws.services.s3.model.GetPublicAccessBlockResult;
import com.amazonaws.services.s3.model.Grant;
import com.amazonaws.services.s3.model.Grantee;
import com.amazonaws.services.s3.model.GroupGrantee;
import com.amazonaws.services.s3.model.PublicAccessBlockConfiguration;
import com.amazonaws.services.s3.model.SetBucketAclRequest;
import com.amazonaws.services.s3.model.SetObjectAclRequest;
import com.amazonaws.services.s3.model.SetPublicAccessBlockRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate S3 buckets that are public<br>
 * <p>Check each bucket's permissions and remove permissions to AllUsers and AllAuthenticatedUsers </p>
 *
 * @see edu.emory.it.services.srd.detector.S3PublicBucketDetector the detector
 */
public class S3PublicBucketRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:s3:::", srContext.LOGTAG,
                DetectionType.S3PublicBucketAcl, DetectionType.S3PublicBucketObject, DetectionType.S3PublicAccessBlockMisconfiguration))
            return;
        // like arn:aws:s3:::bucket-name
        // or   arn:aws:s3:::bucket-name/key
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        String bucketName = arn[5].trim();
        String objectKey = null;

        final AmazonS3 s3;
        try {
            s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "S3 Public Bucket '" + bucketName + "'", e);
            return;
        }

        if (detected.getType().equals(DetectionType.S3PublicBucketObject.name())) {
            String[] bucketAndObject = bucketName.split("/", 2);
            bucketName = bucketAndObject[0];
            objectKey = bucketAndObject[1];
        }

        if (bucketName.isEmpty()) {
            setError(detected, srContext.LOGTAG,
                    "Security risk input for " + detected.getType() + " is missing bucket name");
            return;
        }

        if (detected.getType().equals(DetectionType.S3PublicBucketObject.name()) && (objectKey == null || objectKey.isEmpty())) {
            setError(detected, srContext.LOGTAG,
                    "Security risk input for " + detected.getType() + " is missing object key");
            return;
        }

        try {
            if (detected.getType().equals(DetectionType.S3PublicBucketAcl.name())) {
                remediateS3PublicBucketAcl(s3, bucketName, detected, srContext.LOGTAG);
            } else if (detected.getType().equals(DetectionType.S3PublicAccessBlockMisconfiguration.name())) {
                remediateS3PublicAccessBlockMisconfiguration(s3, bucketName, detected, srContext.LOGTAG);
            } else if (detected.getType().equals(DetectionType.S3PublicBucketObject.name())) {
                remediateS3PublicBucketObject(s3, bucketName, objectKey, detected, srContext.LOGTAG);
            }
        } catch (AmazonS3Exception e) {
            setError(detected, srContext.LOGTAG, "S3 Public Bucket '" + bucketName + "'", e);
        }
    }

    private void remediateS3PublicBucketAcl(AmazonS3 s3, String bucketName,
                                            DetectedSecurityRisk detected, String LOGTAG) {
        AccessControlList acl = s3.getBucketAcl(bucketName);
        boolean isPublic = false;

        for (Grant grant : acl.getGrantsAsList()) {
            Grantee grantee = grant.getGrantee();
            if (grantee instanceof GroupGrantee) {
                GroupGrantee groupGrantee = (GroupGrantee) grantee;
                if (groupGrantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                    isPublic = true;
                    break;
                }
            }
        }

        if (!isPublic) {
            setNoLongerRequired(detected, LOGTAG, bucketName + " is already private");
        } else {
            acl.revokeAllPermissions(GroupGrantee.AllUsers);
            acl.revokeAllPermissions(GroupGrantee.AuthenticatedUsers);

            SetBucketAclRequest setBucketAclRequest = new SetBucketAclRequest(bucketName, acl);
            s3.setBucketAcl(setBucketAclRequest);

            setSuccess(detected, LOGTAG, bucketName + " successfully made private.");
        }
    }

    private void remediateS3PublicAccessBlockMisconfiguration(AmazonS3 s3, String bucketName,
                                                              DetectedSecurityRisk detected, String LOGTAG) {
        GetPublicAccessBlockRequest getPublicAccessBlockRequest = new GetPublicAccessBlockRequest()
                .withBucketName(bucketName);
        GetPublicAccessBlockResult getPublicAccessBlockResult;
        PublicAccessBlockConfiguration publicAccessBlockConfiguration;
        SetPublicAccessBlockRequest setPublicAccessBlockRequest = new SetPublicAccessBlockRequest()
                .withBucketName(null); // bucket name set if remediation required
        String pabSettings = "";

        try {
            getPublicAccessBlockResult = s3.getPublicAccessBlock(getPublicAccessBlockRequest);
            publicAccessBlockConfiguration = getPublicAccessBlockResult.getPublicAccessBlockConfiguration();
        }
        catch (AmazonS3Exception e) {
            // com.amazonaws.services.s3.model.AmazonS3Exception: The public access block configuration was not found
            //   (Service: Amazon S3; Status Code: 404; Error Code: NoSuchPublicAccessBlockConfiguration)
            if (e.getErrorCode().equals("NoSuchPublicAccessBlockConfiguration")) {
                setPublicAccessBlockRequest.setBucketName(bucketName);
                publicAccessBlockConfiguration = new PublicAccessBlockConfiguration();
            }
            else {
                throw e;
            }
        }

        if (!Boolean.TRUE.equals(publicAccessBlockConfiguration.getIgnorePublicAcls())) {
            publicAccessBlockConfiguration.setIgnorePublicAcls(true);
            setPublicAccessBlockRequest.setBucketName(bucketName);
            pabSettings += "IgnorePublicAcls";
        }
        if (!Boolean.TRUE.equals(publicAccessBlockConfiguration.getBlockPublicPolicy())) {
            publicAccessBlockConfiguration.setBlockPublicPolicy(true);
            setPublicAccessBlockRequest.setBucketName(bucketName);
            pabSettings += ((pabSettings.isEmpty() ? "" : ",") + "BlockPublicPolicy");
        }
        if (!Boolean.TRUE.equals(publicAccessBlockConfiguration.getRestrictPublicBuckets())) {
            publicAccessBlockConfiguration.setRestrictPublicBuckets(true);
            setPublicAccessBlockRequest.setBucketName(bucketName);
            pabSettings += ((pabSettings.isEmpty() ? "" : ",") + "RestrictPublicBuckets");
        }
        /*
         * as mentioned in the SRD class level javadoc, BlockPublicAcls is not checked by the detector or set
         * by the remediator.  the reason being: Setting this will require the customer to change the way they
         * upload files to make sure they include a non-public ACL.  but because IgnorePublicAcls is considered,
         * any object will still not be public since the public ACL will be ignored.
         */

        if (setPublicAccessBlockRequest.getBucketName() != null) {
            setPublicAccessBlockRequest.setPublicAccessBlockConfiguration(publicAccessBlockConfiguration);
            s3.setPublicAccessBlock(setPublicAccessBlockRequest);
            setSuccess(detected, LOGTAG, "PublicAccessBlock for bucket " + bucketName
                    + " configured for " + pabSettings);
        }
        else {
            setNoLongerRequired(detected, LOGTAG, "PublicAccessBlockMisconfiguration for bucket " + bucketName
                    + " is already configured correctly");
        }

    }

    private void remediateS3PublicBucketObject(AmazonS3 s3, String bucketName, String objectKey,
                                               DetectedSecurityRisk detected, String LOGTAG) {
        AccessControlList acl = s3.getObjectAcl(bucketName, objectKey);
        boolean isPublic = false;

        for (Grant grant : acl.getGrantsAsList()) {
            Grantee grantee = grant.getGrantee();
            if (grantee instanceof GroupGrantee) {
                GroupGrantee groupGrantee = (GroupGrantee) grantee;
                if (groupGrantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                    isPublic = true;
                    break;
                }
            }
        }

        if (!isPublic) {
            setNoLongerRequired(detected, LOGTAG, bucketName + "/" + objectKey + " is already private");
        } else {
            acl.revokeAllPermissions(GroupGrantee.AllUsers);
            acl.revokeAllPermissions(GroupGrantee.AuthenticatedUsers);

            SetObjectAclRequest setObjectAclRequest = new SetObjectAclRequest(bucketName, objectKey, acl);
            s3.setObjectAcl(setObjectAclRequest);

            setSuccess(detected, LOGTAG, bucketName + "/" + objectKey + " successfully made private.");
        }
    }
}
