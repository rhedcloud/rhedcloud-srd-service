package edu.emory.it.services.srd.util;

import com.amazonaws.AmazonWebServiceClient;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.metrics.RequestMetricCollector;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.organizations.AWSOrganizationsClient;
import com.amazonaws.services.organizations.AWSOrganizationsClientBuilder;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import edu.emory.it.services.srd.exceptions.ConnectTimedOutException;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class to help manage regions and to build AmazonWebServiceClient.
 */
public class SecurityRiskAwsRegionsAndClientBuilder {
    // AWS client builders are not thread safe
    public static final Object builderLock = new Object();

    // Credentials for the account series master account (like aws-dev-master or aws-account-master)
    private AWSCredentialsProvider credentialsProvider;
    private String securityRole;

    public void init(AWSCredentialsProvider credentialsProvider, String securityRole) {
        this.credentialsProvider = credentialsProvider;
        this.securityRole = securityRole;
    }

    /**
     * Restrict regions to US.
     * AWS policy restrictions will be put in place restricting users to only these regions,
     * so there's no need to run SRD against any others though there are some exceptions.
     */
    private static final Set<String> rhedcloudRegions;
    static {
        HashSet<String> hs = new HashSet<>();
        hs.add("us-east-1");
        hs.add("us-east-2");
        hs.add("us-west-1");
        hs.add("us-west-2");
        rhedcloudRegions = Collections.unmodifiableSet(hs);
    }

    public static final String DEFAULT_REGION = "us-east-1";
    public static final String CLOUDTRAIL_CREATE_REGION = "us-east-1";

    public static final Set<String> US_REGIONS;
    static {
        Set<String> r = new HashSet<>();
        r.add("US");
        r.add("us-east-1");
        r.add("us-east-2");
        r.add("us-west-1");
        r.add("us-west-2");
        US_REGIONS = Collections.unmodifiableSet(r);
    }


    // override regions and builders during testing
    private Map<String, AmazonWebServiceClient> clientBuilderOverrides;
    private List<Region> overrideRegions;


    public List<Region> getRegions() {
        if (overrideRegions != null) {
            return overrideRegions;
        }

        List<Region> regions = new ArrayList<>(rhedcloudRegions.size());
        for (Region region : RegionUtils.getRegions()) {
            if (rhedcloudRegions.contains(region.getName())) {
                regions.add(region);
            }
        }
        return regions;
    }

    public <D extends SecurityRiskAwsRegionsAndClientBuilder> D withRegionOverride(List<Region> regions) {
        overrideRegions = regions;
        @SuppressWarnings("unchecked")
        D thiz = (D) this;
        return thiz;
    }

    public <T extends AmazonWebServiceClient> T getClient(String accountId,
                                                          RequestMetricCollector requestMetricCollector, Class<T> clazz)
            throws EmoryAwsClientBuilderException, ConnectTimedOutException {
        return getClient(accountId, null, null, requestMetricCollector, clazz);
    }
    public <T extends AmazonWebServiceClient> T getClient(String accountId, String region,
                                                          RequestMetricCollector requestMetricCollector, Class<T> clazz)
            throws EmoryAwsClientBuilderException, ConnectTimedOutException {
        return getClient(accountId, region, null, requestMetricCollector, clazz);
    }
    public <T extends AmazonWebServiceClient> T getClient(String accountId, String region, String endpoint,
                                                          RequestMetricCollector requestMetricCollector, Class<T> clazz)
            throws EmoryAwsClientBuilderException, ConnectTimedOutException {
        if (clientBuilderOverrides != null && clientBuilderOverrides.containsKey(clazz.getName())) {
            @SuppressWarnings("unchecked")
            T t = (T) clientBuilderOverrides.get(clazz.getName());
            return t;
        }

        return new EmoryAwsClientBuilder()
                .withAccountId(accountId)
                .withCredentials(credentialsProvider)
                .withRole(securityRole)
                .withSessionName(this.getClass().getSimpleName())
                .withRequestMetricCollector(requestMetricCollector)
                .withRegion(region)
                .withEndpoint(endpoint)
                .build(clazz);
    }

    public <D extends SecurityRiskAwsRegionsAndClientBuilder, T extends AmazonWebServiceClient> D withClientBuilderOverride(T awsClient) {
        if (clientBuilderOverrides == null)
            clientBuilderOverrides = new HashMap<>();
        clientBuilderOverrides.put(awsClient.getClass().getAnnotatedSuperclass().getType().getTypeName(), awsClient);
        @SuppressWarnings("unchecked")
        D thiz = (D) this;
        return thiz;
    }

    /**
     * Build a new AWSOrganizations client.
     * Organizations are global (not regional) so don't need a region.
     * Also, don't assume any roles just use the (master account) credentials.
     *
     * @param srContext Security Risk Context
     * @return client
     */
    protected AWSOrganizations getOrganizationsClient(SecurityRiskContext srContext) {
        if (clientBuilderOverrides != null && clientBuilderOverrides.containsKey(AWSOrganizationsClient.class.getName())) {
            return (AWSOrganizations) clientBuilderOverrides.get(AWSOrganizationsClient.class.getName());
        }
        synchronized (builderLock) {
            return AWSOrganizationsClientBuilder
                    .standard()
                    .withRegion(DEFAULT_REGION)
                    .withCredentials(credentialsProvider)
                    .withMetricsCollector(srContext.getMetricCollector())
                    .build();
        }
    }

    /**
     * Build a new AWSSecurityTokenService client.
     * Don't assume any roles just use the (master account) credentials.
     *
     * @param srContext Security Risk Context
     * @return client
     */
    protected AWSSecurityTokenService getSecurityTokenServiceClient(SecurityRiskContext srContext) {
        if (clientBuilderOverrides != null && clientBuilderOverrides.containsKey(AWSSecurityTokenServiceClient.class.getName())) {
            return (AWSSecurityTokenService) clientBuilderOverrides.get(AWSSecurityTokenServiceClient.class.getName());
        }
        synchronized (builderLock) {
            return AWSSecurityTokenServiceClientBuilder
                    .standard()
                    .withRegion(DEFAULT_REGION)
                    .withCredentials(credentialsProvider)
                    .withMetricsCollector(srContext.getMetricCollector())
                    .build();
        }
    }
}
