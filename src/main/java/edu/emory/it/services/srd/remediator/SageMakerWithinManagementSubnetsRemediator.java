package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by deleting or stopping the SageMaker resources.
 *
 * <p>SageMaker notebook instances and models are deleted.</p>
 * <p>SageMaker training jobs can not be deleted so they are stopped.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.SageMakerWithinManagementSubnetsDetector the detector
 */
public class SageMakerWithinManagementSubnetsRemediator extends SageMakerBaseRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        remediate(accountId, detected, srContext, DetectionType.SageMakerWithinManagementSubnets);
    }
}
