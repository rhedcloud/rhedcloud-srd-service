package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClient;
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersRequest;
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersResult;
import com.amazonaws.services.simplesystemsmanagement.model.ParameterMetadata;
import com.amazonaws.services.simplesystemsmanagement.model.ParametersFilter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects unencrypted Systems Manager Parameter Store.<br>
 *
 * @see edu.emory.it.services.srd.remediator.SSMUnencryptedParametersRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class SSMUnencryptedParametersDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AWSSimpleSystemsManagement ssm;
            try {
                ssm = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSSimpleSystemsManagementClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.SSMUnencryptedParameters,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:ssm:" + region.getName() + ":" + accountId + ":parameter/";

            // find all parameters that are not SecureString
            DescribeParametersRequest describeParametersRequest = new DescribeParametersRequest()
                    .withFilters(new ParametersFilter().withKey("Type").withValues("String", "StringList"));
            DescribeParametersResult describeParametersResult = ssm.describeParameters(describeParametersRequest);
            for (ParameterMetadata parameter : describeParametersResult.getParameters()) {
                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                        DetectionType.SSMUnencryptedParameters, arnPrefix + parameter.getName());
            }
        }
    }

    @Override
    public String getBaseName() {
        return "SSMUnencryptedParameters";
    }
}
