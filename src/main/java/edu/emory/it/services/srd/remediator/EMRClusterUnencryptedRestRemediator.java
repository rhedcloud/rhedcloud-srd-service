package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * The Elastic Map Reduce cluster will be terminated.<br>
 * If the cluster has already been terminated or termination is in-progress then the remediator takes no action.
 *
 * <p>Enable encryption at-rest when creating the cluster.<br>
 * See <a href="https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-data-encryption.html">Encrypt Data in Transit and At Rest</a>.</p>
 *
 * <p>Security: Enforce, stop, delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.EMRClusterUnencryptedRestDetector the detector
 */
public class EMRClusterUnencryptedRestRemediator extends AbstractEMRRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:elasticmapreduce:", srContext.LOGTAG, DetectionType.EMRClusterUnencryptedRest))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        super.terminateCluster(detected, accountId, srContext);
    }
}
