package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediation, ideally, would be to stop the database but that is not possible.<br>
 * Instead, the database could be deleted but in order to prevent disruption to the user, make this a passive remediation.<br>
 *
 * <p>Enable encryption at-rest.<br>
 * See <a href="https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/EncryptionAtRest.html">Amazon DynamoDB Encryption at Rest</a>.</p>
 *
 * <p>Security: Enforce, delete is fine<br>
 * Preference: Medium</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.DynamoDbUnencryptedDatabaseDetector the detector
 */
public class DynamoDbUnencryptedDatabaseRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:dynamodb:", srContext.LOGTAG, DetectionType.DynamoDbUnencryptedDatabase))
            return;
        // like arn:aws:dynamodb:us-east-1:123456789012:table/table-name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        setNotificationOnly(detected, srContext.LOGTAG);
    }
}
