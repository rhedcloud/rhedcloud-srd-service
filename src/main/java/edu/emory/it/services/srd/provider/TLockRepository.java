package edu.emory.it.services.srd.provider;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TLockRepository extends JpaRepository<TLock, String> {
    @Query("select count(1) from TLock where lockName like ?1")
    long countByLockNameLike(String like);
}
