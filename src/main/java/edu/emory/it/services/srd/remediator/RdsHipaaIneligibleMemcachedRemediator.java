package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Delete the cluster.
 * <p>Instead of a memcached backed cluster, choose the Redis engine as it allows encryption at-rest.</p>
 *
 * @see edu.emory.it.services.srd.detector.RdsHipaaIneligibleMemcachedDetector the detector
 */
public class RdsHipaaIneligibleMemcachedRemediator extends ElastiCacheBaseRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediate(accountId, detected, srContext, DetectionType.RdsHipaaIneligibleMemcached, RemediationMode.DELETE);
    }
}
