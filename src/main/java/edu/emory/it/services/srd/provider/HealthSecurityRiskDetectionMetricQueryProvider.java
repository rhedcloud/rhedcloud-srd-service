package edu.emory.it.services.srd.provider;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetectionMetric;
import com.amazon.aws.moa.objects.resources.v1_0.DetectorAccount;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionMetricQuerySpecification;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import edu.emory.it.services.srd.RemediationStatus;
import edu.emory.it.services.srd.util.SpringIntegration;
import edu.emory.it.services.srd.util.SrdMetricDetectorMeasure;
import edu.emory.it.services.srd.util.SrdMetricUtil;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import edu.emory.it.services.srd.util.SrdSyncStorageUtil;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.logging.log4j.Logger;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.moa.XmlEnterpriseObjectException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiFunction;

public class HealthSecurityRiskDetectionMetricQueryProvider implements SecurityRiskDetectionMetricQueryProvider {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(HealthSecurityRiskDetectionMetricQueryProvider.class);
    private static final String LOGTAG = "[HealthSecurityRiskDetectionMetricQueryProvider] ";

    // no way to get this from any of the OpenEAI objects so get it directly
    private static final String OPEN_EAI_INSTANCE_ID = System.getProperty("org.openeai.instanceName");

    private Map<String, BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException>> queryImpls;
    private String[] allMetricsQueries;
    private final NotYetImplementedQuery QueryNotYetImplemented = new NotYetImplementedQuery();

    @Override
    public void init() throws InstantiationException {
        queryImpls = new HashMap<>();
        queryImpls.put("DetectionRatePerSecond", new DetectionRatePerSecondQuery());
        queryImpls.put("ExemptAccounts", new ExemptAccountsQuery());
        queryImpls.put("AccountCounts", new AccountCountsQuery());
        queryImpls.put("TotalDetectorsToExecute", new TotalDetectorsToExecuteQuery());
        queryImpls.put("TotalDetectorsExecuting", new TotalDetectorsExecutingQuery());
        queryImpls.put("SyncProcessingLag", new SyncProcessingLagQuery());
        queryImpls.put("AllMetrics", new AllMetricsQuery());
        queryImpls.put("RemediationReportOneWeek", new RemediatedReportByTimeUnitQuery(7));
        queryImpls.put("RemediationReportTwoWeek", new RemediatedReportByTimeUnitQuery(14));
        queryImpls.put("RemediationReportOneMonth", new RemediatedReportByTimeUnitQuery(30));
        queryImpls.put("WIP_Analysis", new AnalysisQuery());

        // "AllMetrics" is a meta-metric that includes the following
        allMetricsQueries = new String[] {
                "DetectionRatePerSecond",
                "ExemptAccounts",
                "AccountCounts",
                "TotalDetectorsToExecute",
                "TotalDetectorsExecuting",
                "SyncProcessingLag"
        };
    }

    @Override
    public void query(SecurityRiskDetectionMetricQuerySpecification spec,
                      SecurityRiskDetectionMetric metric) throws ProviderException {
        logger.info(LOGTAG + "[query] - MetricName is " + spec.getMetricName());

        ProviderException providerException = queryImpls
                .getOrDefault(spec.getMetricName(), QueryNotYetImplemented)
                .apply(spec, metric);

        if (providerException != null)
            throw providerException;
    }

    private static class DetectionRatePerSecondQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                // no DetectorAccount children for this MetricName
                metric.setName(spec.getMetricName());
                metric.setValue(String.format("%.2f", srdMetricUtil.detectionRatePerSecond()));

                logger.info(LOGTAG + metric.getName() + " is " + metric.getValue());
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class ExemptAccountsQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                // no DetectorAccount children for this MetricName
                metric.setName(spec.getMetricName());
                metric.setValue(String.valueOf(srdMetricUtil.getNbrExemptAccounts()));

                logger.info(LOGTAG + metric.getName() + " is " + metric.getValue());
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class AccountCountsQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                metric.setName(spec.getMetricName());
                metric.setValue("0");

                // DetectorAccount children give additional detail
                DetectorAccount detectorAccount;

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrActiveStandardAccounts");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrActiveStandardAccounts()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrActiveHipaaAccounts");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrActiveHipaaAccounts()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrActiveEnhancedSecurityAccounts");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrActiveEnhancedSecurityAccounts()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrExemptAccounts");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrExemptAccounts()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrStandardDetectors");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrStandardDetectors()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrHipaaDetectors");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrHipaaDetectors()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("NbrEnhancedSecurityDetectors");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getNbrEnhancedSecurityDetectors()));

                logger.info(LOGTAG + metric.getName() + " is in the details");
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class TotalDetectorsToExecuteQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                // no DetectorAccount children for this MetricName
                metric.setName(spec.getMetricName());
                metric.setValue(String.valueOf(srdMetricUtil.getNbrRelevantDetectors()));

                logger.info(LOGTAG + metric.getName() + " is " + metric.getValue());
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class TotalDetectorsExecutingQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            long startExec = System.currentTimeMillis();

            SpringIntegration springIntegration = SpringIntegration.getInstance();
            TLockService tLockService = springIntegration.getBean(TLockService.class);
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                // active detectors is defined as the number of detectors that are running (# of detector locks)
                // divided by the number of detectors that should be running
                // as a percent
                long lockCount = tLockService.countByLockNameLike("SRD_%");
                int nbrRelevantDetectors = srdMetricUtil.getNbrRelevantDetectors();

                double totalDetectorsExecutingAsPercent = (double) lockCount / nbrRelevantDetectors * 100;
                String totalDetectorsExecutingAsPercentRounded = String.format("%.2f", totalDetectorsExecutingAsPercent);

                // no DetectorAccount children for this MetricName
                metric.setName(spec.getMetricName());
                metric.setValue(totalDetectorsExecutingAsPercentRounded);

                long finishExec = System.currentTimeMillis();

                logger.info(LOGTAG + metric.getName() + " is " + metric.getValue()
                        + " with " + lockCount + " locks"
                        + " and " + nbrRelevantDetectors + " relevant detectors"
                        + " with elapsed time " + (finishExec - startExec) + " ms.");
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class SyncProcessingLagQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                metric.setName(spec.getMetricName());
                metric.setValue("0");

                // DetectorAccount children give additional detail
                DetectorAccount detectorAccount;

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("LastSecurityRiskDetectionIdGenerated");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getLastSecurityRiskDetectionIdGenerated()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("LastSecurityRiskDetectionIdSyncPersister");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getLastSecurityRiskDetectionIdSyncPersister()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("LastSecurityRiskDetectionIdSyncAccountNotification");
                detectorAccount.setAccountId(String.valueOf(srdMetricUtil.getLastSecurityRiskDetectionIdSyncAccountNotification()));

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("OPEN_EAI_INSTANCE_ID");
                detectorAccount.setAccountId(OPEN_EAI_INSTANCE_ID);

                logger.info(LOGTAG + metric.getName() + " is in the details");
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private class AllMetricsQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            long startExec = System.currentTimeMillis();

            try {
                for (String mq : allMetricsQueries) {
                    spec.setMetricName("AllMetrics/" + mq);
                    ProviderException providerException = queryImpls
                            .getOrDefault(mq, QueryNotYetImplemented)
                            .apply(spec, metric);

                    if (providerException != null)
                        return providerException;

                    DetectorAccount detectorAccount = metric.newDetectorAccount();
                    metric.addDetectorAccount(detectorAccount);

                    detectorAccount.setDetectorName(mq);
                    detectorAccount.setAccountId(metric.getValue());
                }

                // DetectorAccount children set above
                metric.setName("AllMetrics");
                metric.setValue("0");

                long finishExec = System.currentTimeMillis();
                String allMetricsLog;

                try {
                    allMetricsLog = metric.toXmlString();
                } catch (XmlEnterpriseObjectException e) {
                    allMetricsLog = e.getMessage();
                }

                logger.info(LOGTAG + metric.getName()
                        + " with " + allMetricsLog
                        + " with elapsed time " + (finishExec - startExec) + " ms.");
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class AnalysisQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            try {
                metric.setName(spec.getMetricName());
                metric.setValue("0");

                // DetectorAccount children give additional detail
                DetectorAccount detectorAccount;
                Map<String, CircularFifoQueue<SrdMetricDetectorMeasure>> detectorAnalysis = srdMetricUtil.getDetectorAnalysis();

                for (Map.Entry<String, CircularFifoQueue<SrdMetricDetectorMeasure>> da : detectorAnalysis.entrySet()) {
                    SrdMetricDetectorMeasure daMetric = da.getValue().peek();
                    if (daMetric != null) {
                        detectorAccount = metric.newDetectorAccount();
                        metric.addDetectorAccount(detectorAccount);
                        detectorAccount.setDetectorName(da.getKey());
                        detectorAccount.setAccountId(daMetric.toJson());
                    }
                }

                detectorAccount = metric.newDetectorAccount();
                metric.addDetectorAccount(detectorAccount);
                detectorAccount.setDetectorName("OPEN_EAI_INSTANCE_ID");
                detectorAccount.setAccountId(OPEN_EAI_INSTANCE_ID);

                logger.info(LOGTAG + metric.getName() + " is in the details");
                metric.dumpData();
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class RemediatedReportByTimeUnitQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        private final int duration;

        RemediatedReportByTimeUnitQuery(int duration) {
            this.duration = duration;
        }

        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            logger.info(LOGTAG + "[" + spec.getMetricName() + "] - starting");

            long startExec = System.currentTimeMillis();

            List<Map<String, AttributeValue>> detectedSecurityRisks = new ArrayList<>();
            long oneWeekAgoMillis = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(duration);
            ZoneId systemDefaultZoneId = ZoneId.systemDefault();

            SrdSyncStorageUtil srdSyncStorageUtil = SrdSyncStorageUtil.getInstance();

            try {
                ScanRequest scanRequest;
                ScanResult scanResult;

                Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
                expressionAttributeValues.put(":cdt", new AttributeValue().withN(String.valueOf(oneWeekAgoMillis)));
                expressionAttributeValues.put(":remediated", new AttributeValue().withS(RemediationStatus.REMEDIATION_SUCCESS.getStatus()));
                Map<String, String> expressionAttributeNames = new HashMap<>();
                expressionAttributeNames.put("#dt", "type");
                expressionAttributeNames.put("#rrs", "status");

                // slurp in all the sync items
                scanRequest = new ScanRequest()
                        .withTableName(srdSyncStorageUtil.getStorageTable().getTableName())
                        .withProjectionExpression("createDatetime, detection.accountId, detection.#dt, detection.arn, detection.remediateResult.description")
                        .withFilterExpression("createDatetime >= :cdt AND detection.remediateResult.#rrs = :remediated")
                        .withExpressionAttributeValues(expressionAttributeValues)
                        .withExpressionAttributeNames(expressionAttributeNames);

                do {
                    scanResult = srdSyncStorageUtil.getAmazonDynamoDB().scan(scanRequest);
                    detectedSecurityRisks.addAll(scanResult.getItems());
                    scanRequest.setExclusiveStartKey(scanResult.getLastEvaluatedKey());
                } while (scanResult.getLastEvaluatedKey() != null);

                // and then run some analysis
                Map<SrdNameValuePair, AtomicLong> byTypeAccount = new HashMap<>();
                Map<String, AtomicLong> byAccount = new HashMap<>();

                metric.setName(spec.getMetricName());
                metric.setValue(String.valueOf(detectedSecurityRisks.size()));

                for (Map<String, AttributeValue> dsr : detectedSecurityRisks) {
                    String type = dsr.get("detection").getM().get("type").getS();
                    String accountId = dsr.get("detection").getM().get("accountId").getS();
                    long createDatetime = Long.parseLong(dsr.get("createDatetime").getN());

                    byTypeAccount.computeIfAbsent(new SrdNameValuePair(type, accountId), l -> new AtomicLong()).incrementAndGet();
                    byAccount.computeIfAbsent(accountId, l -> new AtomicLong()).incrementAndGet();

                    DetectorAccount detectorAccount = metric.newDetectorAccount();
                    metric.addDetectorAccount(detectorAccount);

                    String annotatedDetectorName = type + " ";
                    annotatedDetectorName += "{" + LocalDateTime.ofInstant(Instant.ofEpochMilli(createDatetime), systemDefaultZoneId) + "}";
                    annotatedDetectorName += "{" + dsr.get("detection").getM().get("arn").getS() + "}";
                    annotatedDetectorName += "{" + dsr.get("detection").getM().get("remediateResult").getM().get("description").getS() + "}";
                    detectorAccount.setDetectorName(annotatedDetectorName);
                    detectorAccount.setAccountId(accountId);
                }

                for (Map.Entry<SrdNameValuePair, AtomicLong> entry : byTypeAccount.entrySet()) {
                    DetectorAccount detectorAccount = metric.newDetectorAccount();
                    metric.addDetectorAccount(detectorAccount);

                    String annotatedDetectorName = entry.getKey().getName() + " ";
                    annotatedDetectorName += "{" + entry.getValue().get() + "}";
                    detectorAccount.setDetectorName(annotatedDetectorName);
                    detectorAccount.setAccountId(entry.getKey().getValue());
                }

                for (Map.Entry<String, AtomicLong> entry : byAccount.entrySet()) {
                    DetectorAccount detectorAccount = metric.newDetectorAccount();
                    metric.addDetectorAccount(detectorAccount);

                    String annotatedDetectorName = "*" + " ";
                    annotatedDetectorName += "{" + entry.getValue().get() + "}";
                    detectorAccount.setDetectorName(annotatedDetectorName);
                    detectorAccount.setAccountId(entry.getKey());
                }

                long finishExec = System.currentTimeMillis();

                logger.info(LOGTAG + metric.getName() + " is " + metric.getValue()
                        + " with " + detectedSecurityRisks.size() + " detected security risks"
                        + " in the " + srdSyncStorageUtil.getStorageTable().getTableName() + " table"
                        + " since " + LocalDateTime.ofInstant(Instant.ofEpochMilli(oneWeekAgoMillis), systemDefaultZoneId)
                        + " with elapsed time " + (finishExec - startExec) + " ms.");
                return null;
            }
            catch (EnterpriseFieldException e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " setting field in the 'SecurityRiskDetectionMetric' object. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
            catch (Exception e) {
                String errMsg = "Error processing " + spec.getMetricName()
                        + " gathering sync data. The exception is: " + e.getMessage();
                return new ProviderException(errMsg);
            }
        }
    }

    private static class NotYetImplementedQuery
            implements BiFunction<SecurityRiskDetectionMetricQuerySpecification, SecurityRiskDetectionMetric, ProviderException> {
        @Override
        public ProviderException apply(SecurityRiskDetectionMetricQuerySpecification spec, SecurityRiskDetectionMetric metric) {
            String errMsg = "Unsupported metric name (" + spec.getMetricName() + ") in SecurityRiskDetectionMetricQuerySpecification.";
            return new ProviderException(errMsg);
        }
    }
}
