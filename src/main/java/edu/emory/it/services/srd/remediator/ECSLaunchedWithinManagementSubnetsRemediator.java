package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClient;
import com.amazonaws.services.ecs.model.AmazonECSException;
import com.amazonaws.services.ecs.model.DeleteServiceRequest;
import com.amazonaws.services.ecs.model.UpdateServiceRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediate by deleting the ECS service.
 *
 * <p>Security: Enforce, delete<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.ECSLaunchedWithinManagementSubnetsDetector the detector
 */
public class ECSLaunchedWithinManagementSubnetsRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ecs:", srContext.LOGTAG, DetectionType.ECSLaunchedWithinManagementSubnets))
            return;
        // like arn:aws:ecs:us-east-1:123456789012:service/ECS_Service_Name
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String region = arn[3];
        String clusterArn = detected.getProperties().getProperty("clusterArn");

        final AmazonECS ecsClient;
        try {
            ecsClient = getClient(accountId, region, srContext.getMetricCollector(), AmazonECSClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed for ECS service", e);
            return;
        }

        try {
            // desired count must be zero to delete the service
            UpdateServiceRequest updateServiceRequest = new UpdateServiceRequest()
                    .withCluster(clusterArn)
                    .withService(detected.getAmazonResourceName())
                    .withDesiredCount(0);

            ecsClient.updateService(updateServiceRequest);
        }
        catch (AmazonECSException e) {
            setError(detected, srContext.LOGTAG, "Failed to set ECS service instance count to 0", e);
            return;
        }

        try {
            DeleteServiceRequest deleteServiceRequest = new DeleteServiceRequest()
                    .withCluster(clusterArn)
                    .withService(detected.getAmazonResourceName());

            ecsClient.deleteService(deleteServiceRequest);
        }
        catch (AmazonECSException e) {
            setError(detected, srContext.LOGTAG, "Failed to delete ECS service", e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG, "ECS service deleted");
    }
}
