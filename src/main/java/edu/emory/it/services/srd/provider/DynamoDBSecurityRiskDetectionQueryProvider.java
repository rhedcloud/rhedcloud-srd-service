package edu.emory.it.services.srd.provider;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult;
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionQuerySpecification;
import com.amazonaws.services.dynamodbv2.document.Index;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import edu.emory.it.services.srd.util.SrdSyncStorageUtil;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class DynamoDBSecurityRiskDetectionQueryProvider implements SecurityRiskDetectionQueryProvider {
    private static final Logger logger = OpenEaiObject.logger;

    private static final String LOGTAG = "[DynamoDBSecurityRiskDetectionQueryProvider] ";

    private AppConfig appConfig;


    @Override
    public void init(AppConfig aConfig) throws InstantiationException{
        this.appConfig = aConfig;
    }

    @Override
    public Collection<SecurityRiskDetection> query(SecurityRiskDetectionQuerySpecification spec) throws ProviderException {

        Iterator<Item> values = queryDynamoDB(spec);

        /*
         * for detection failures, one item was stored with a detectionResult indicating the error.
         * otherwise, there can be multiple items stored (each DetectedSecurityRisk) for a single SecurityRiskDetection.
         * certain information is duplicated across each stored item
         *
         * now, de-serializing the DynamoDB query results we must aggregate retrieved items back into a single SecurityRiskDetection.
         * cache them by securityRiskDetectionId.
         */
        Map<String, SecurityRiskDetection> result = new HashMap<>();


        try {
            while (values.hasNext()) {
                Item item = values.next();

                String item_securityRiskDetectionId = item.getString("securityRiskDetectionId");
                long item_createDatetime = item.getLong("createDatetime");
                Map<String, Object> item_detection = item.getMap("detection");

                // we may be aggregating retrieved items into a single SecurityRiskDetection so have a look in the cache
                SecurityRiskDetection detection = result.get(item_securityRiskDetectionId);
                if (detection == null) {
                    detection = (SecurityRiskDetection) appConfig.getObjectByType(SecurityRiskDetection.class.getName());
                    result.put(item_securityRiskDetectionId, detection);

                    detection.setSecurityRiskDetectionId(item_securityRiskDetectionId);
                    detection.setCreateUser("rhedcloud-srd-service");
                }

                // the create datetime of the SecurityRiskDetection is considered to be
                // the earliest of all the DetectedSecurityRisk's
                if (detection.getCreateDatetime() == null
                        ||  item_createDatetime < detection.getCreateDatetime().toCalendar().getTimeInMillis()) {
                    detection.setCreateDatetime(new Datetime(new Date(item_createDatetime)));
                }

                if (item_detection != null) {
                    String item_detection_type = (String) item_detection.get("type");
                    @SuppressWarnings("unchecked")
                    Map<String, Object> item_detection_detectionResult = (Map<String, Object>) item_detection.get("detectionResult");
                    @SuppressWarnings("unchecked")
                    Map<String, Object> item_detection_remediateResult = (Map<String, Object>) item_detection.get("remediateResult");

                    if (detection.getAccountId() == null) {
                        detection.setAccountId((String) item_detection.get("accountId"));
                    }
                    if (detection.getSecurityRiskDetector() == null) {
                        detection.setSecurityRiskDetector((String) item_detection.get("detector"));
                    }
                    if (detection.getSecurityRiskRemediator() == null) {
                        detection.setSecurityRiskRemediator((String) item_detection.get("remediator"));
                    }

                    if (detection.getDetectionResult() == null) {
                        DetectionResult detectionResult = detection.newDetectionResult();
                        detection.setDetectionResult(detectionResult);

                        detectionResult.setType(item_detection_type);
                        detectionResult.setStatus((String) item_detection_detectionResult.get("status"));
                        @SuppressWarnings("unchecked")
                        List<String> errors = (List<String>) item_detection_detectionResult.get("error");
                        if (errors != null) {
                            for (String error : errors) {
                                detectionResult.addError(error);
                            }
                        }
                    }

                    if (item_detection_remediateResult != null) {
                        @SuppressWarnings("unchecked")
                        List<String> errors = (List<String>) item_detection_remediateResult.get("error");

                        DetectedSecurityRisk detectedSecurityRisk = detection.newDetectedSecurityRisk();
                        detection.addDetectedSecurityRisk(detectedSecurityRisk);

                        detectedSecurityRisk.setType(item_detection_type);
                        detectedSecurityRisk.setAmazonResourceName((String) item_detection.get("arn"));

                        RemediationResult remediationResult = detectedSecurityRisk.newRemediationResult();
                        detectedSecurityRisk.setRemediationResult(remediationResult);

                        remediationResult.setStatus((String) item_detection_remediateResult.get("status"));
                        remediationResult.setDescription((String) item_detection_remediateResult.get("description"));
                        if (errors != null) {
                            for (String error : errors) {
                                remediationResult.addError(error);
                            }
                        }
                    }
                }
            }

            return result.values();
        }
        catch (EnterpriseConfigurationObjectException eoce) {
            logger.fatal(LOGTAG + "Error retrieving a SecurityRiskDetection or DetectedSecurityRisk or RemediationResult " +
                    "object from AppConfig: The exception is: " + eoce.getMessage());
            throw new ProviderException(eoce.getMessage());
        }
        catch (EnterpriseFieldException e) {
            logger.fatal(LOGTAG + "Error setting a field in SecurityRiskDetection or DetectedSecurityRisk or RemediationResult " +
                    "object: The exception is: " + e.getMessage());
            throw new ProviderException(e.getMessage());
        }
    }



    private Iterator<Item> queryDynamoDB(SecurityRiskDetectionQuerySpecification querySpec) {
        Table table = SrdSyncStorageUtil.getInstance().getStorageTable();
        Index index = table.getIndex("SecurityRiskDetectionIdIndex");

        ValueMap valueMap = new ValueMap();
        StringBuilder filterExpression = new StringBuilder();

         if (querySpec.getAccountId() != null) {
            filterExpression.append("detection.accountId = :accountId");
            valueMap.withString(":accountId", querySpec.getAccountId());
        }

        if (querySpec.getSecurityRiskRemediator() != null) {
            if (filterExpression.length() != 0 ) {
                filterExpression.append(" and ");
            }
            filterExpression.append("detection.remediator = :remediator");
            valueMap.withString(":remediator", querySpec.getSecurityRiskRemediator());
        }

        if (querySpec.getSecurityRiskDetector() != null) {
            if (filterExpression.length() != 0 ) {
                filterExpression.append(" and ");
            }
            filterExpression.append("detection.detector = :detector");
            valueMap.withString(":detector", querySpec.getSecurityRiskDetector());
        }

        if (querySpec.getDetectedSecurityRisk() != null && !querySpec.getDetectedSecurityRisk().isEmpty() && querySpec.getDetectedSecurityRisk().get(0) != null) {
            DetectedSecurityRisk securityRisk = (DetectedSecurityRisk) querySpec.getDetectedSecurityRisk().get(0);

            if (securityRisk.getAmazonResourceName() != null) {
                if (filterExpression.length() != 0 ) {
                    filterExpression.append(" and ");
                }
                filterExpression.append("detection.arn = :arn");
                valueMap.withString(":arn", securityRisk.getAmazonResourceName());
            }

            if (securityRisk.getType() != null) {
                if (filterExpression.length() != 0 ) {
                    filterExpression.append(" and ");
                }
                filterExpression.append("detection.type = :type");
                valueMap.withString(":type", securityRisk.getType());
            }

            if (securityRisk.getRemediationResult() != null) {
                RemediationResult remediationResult = securityRisk.getRemediationResult();

                if (remediationResult.getStatus() != null) {
                    if (filterExpression.length() != 0) {
                        filterExpression.append(" and ");
                    }
                    filterExpression.append("detection.remediateResult.status = :remediateStatus");
                    valueMap.withString(":remediateStatus", remediationResult.getStatus());
                }

                if (remediationResult.getDescription() != null) {
                    if (filterExpression.length() != 0) {
                        filterExpression.append(" and ");
                    }
                    filterExpression.append("detection.remediateResult.description = :remediateDescription");
                    valueMap.withString(":remediateDescription", remediationResult.getDescription());
                }

                if (remediationResult.getError() != null && !remediationResult.getError().isEmpty()) {

                    if (filterExpression.length() != 0) {
                        filterExpression.append(" and ");
                    }
                    filterExpression.append("detection.remediateResult.error = :error");
                    valueMap.withList(":error", remediationResult.getError());
                }
            }
        }

        if (querySpec.getSecurityRiskDetectionId() != null) {
            // we can query using the index
            valueMap.withString(":v_id", querySpec.getSecurityRiskDetectionId());
            valueMap.withNumber(":v_zero", 0);

            QuerySpec spec = new QuerySpec()
                    .withKeyConditionExpression("securityRiskDetectionId = :v_id and createDatetime > :v_zero")
                    .withValueMap(valueMap);

            if (filterExpression.length() > 0) {
                spec.withFilterExpression(filterExpression.toString());
            }

            return index.query(spec).iterator();
        }
        else {
            // we have to scan the entire table
            ScanSpec spec = new ScanSpec()
                    .withValueMap(valueMap);

            if (filterExpression.length() > 0) {
                spec.withFilterExpression(filterExpression.toString());
            }

            return table.scan(spec).iterator();
        }
    }
}
