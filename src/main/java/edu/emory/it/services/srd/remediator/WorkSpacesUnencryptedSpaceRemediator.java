package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.workspaces.AmazonWorkspaces;
import com.amazonaws.services.workspaces.AmazonWorkspacesClient;
import com.amazonaws.services.workspaces.model.DescribeWorkspacesRequest;
import com.amazonaws.services.workspaces.model.DescribeWorkspacesResult;
import com.amazonaws.services.workspaces.model.TerminateRequest;
import com.amazonaws.services.workspaces.model.TerminateWorkspacesRequest;
import com.amazonaws.services.workspaces.model.Workspace;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;


/**
 * Remediates WorkSpaces with unencrypted volumes<br>
 *
 * <p>The remediator terminates the workspace</p>
 *
 * <p>The remediator is currently not running due to a security policy that does not allow worksspaces to be listed.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Alert. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.WorkSpacesUnencryptedSpaceDetector the detector
 */
public class WorkSpacesUnencryptedSpaceRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:workspaces:", srContext.LOGTAG, DetectionType.WorkSpacesUnencryptedSpace))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String workspaceId =  arn[5].substring(arn[5].indexOf("/") + 1);
        String region = arn[3];

        final AmazonWorkspaces workspaces;
        try {
            workspaces = getClient(accountId, region, srContext.getMetricCollector(), AmazonWorkspacesClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Workspace '" + workspaceId + "'", e);
            return;
        }

        DescribeWorkspacesRequest request = new DescribeWorkspacesRequest().withWorkspaceIds(workspaceId);
        DescribeWorkspacesResult result = workspaces.describeWorkspaces(request);

        if (result.getWorkspaces().isEmpty()) {
            setError(detected, srContext.LOGTAG, "Workspace '" + workspaceId + "' no longer exists.");
            return;
        }
        Workspace workspace = result.getWorkspaces().get(0);

        if (workspace.getState().equals("TERMINATING") || workspace.getState().equals("TERMINATED")) {
            setNoLongerRequired(detected, srContext.LOGTAG, "Workspace '" + workspaceId + "' is terminating / terminated.");
            return;
        }

        if (Boolean.TRUE.equals(workspace.isRootVolumeEncryptionEnabled()) && Boolean.TRUE.equals(workspace.isUserVolumeEncryptionEnabled())) {
            setNoLongerRequired(detected, srContext.LOGTAG, "Workspace '" + workspaceId + "' already has encryption enabled.");
            return;
        }

        TerminateWorkspacesRequest terminateRequest = new TerminateWorkspacesRequest().withTerminateWorkspaceRequests(new TerminateRequest().withWorkspaceId(workspaceId));
        try {
            workspaces.terminateWorkspaces(terminateRequest);
        } catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Unable to terminate workspace '" + workspaceId, e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG, "Workspace '" + workspaceId + "' successfully marked for termination.");
    }
}
