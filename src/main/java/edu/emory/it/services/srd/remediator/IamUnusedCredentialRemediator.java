package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.AccessKeyLastUsed;
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata;
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.UpdateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.UpdateAccessKeyResult;
import com.amazonaws.services.identitymanagement.model.DeleteLoginProfileRequest;
import com.amazonaws.services.identitymanagement.model.EntityTemporarilyUnmodifiableException;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedRequest;
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedResult;
import com.amazonaws.services.identitymanagement.model.GetLoginProfileRequest;
import com.amazonaws.services.identitymanagement.model.GetUserRequest;
import com.amazonaws.services.identitymanagement.model.GetUserResult;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult;
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysRequest;
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysResult;
import com.amazonaws.services.identitymanagement.model.ListServiceSpecificCredentialsRequest;
import com.amazonaws.services.identitymanagement.model.ListServiceSpecificCredentialsResult;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.SSHPublicKeyMetadata;
import com.amazonaws.services.identitymanagement.model.ServiceSpecificCredentialMetadata;
import com.amazonaws.services.identitymanagement.model.StatusType;
import com.amazonaws.services.identitymanagement.model.UpdateSSHPublicKeyRequest;
import com.amazonaws.services.identitymanagement.model.UpdateServiceSpecificCredentialRequest;
import com.amazonaws.services.identitymanagement.model.User;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static java.lang.Thread.sleep;

/**
 * This remediator will delete the login if an account has not been used for 30 days.  When a login is deleted,
 * it will also delete http credentials for git and other applications, as well as ssh keys for git / other applications. <br>
 *
 * <p>It will delete any access keys if it has not been used for 30 days</p>
 *
 * <p>The 30 day expiry criteria can be changed inside the config file by modifying
 * IamUnusedCredential : DeleteCredentialAfterInactiveForDays property</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce<br> Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.IamUnusedCredentialDetector the detector
 */
public class IamUnusedCredentialRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private int daysBeforeExpiry,
    			daysBeforeDisable;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
        try {
            Properties properties = appConfig.getProperties("IamUnusedCredential");
            daysBeforeExpiry = Integer.parseInt(
            		properties.getProperty("DeleteCredentialAfterInactiveForDays"));
            daysBeforeDisable = Integer.parseInt(
            		properties.getProperty("DisableCredentialAfterInactiveForDays"));
            logger.info("IamUnusedCredential properties:");
            logger.info("DeleteCredentialAfterInactiveForDays="+daysBeforeExpiry);
            logger.info("DisableCredentialAfterInactiveForDays="+daysBeforeDisable);
        } catch (NumberFormatException | EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:iam:", srContext.LOGTAG,
                DetectionType.IamUnusedCredentialAwsConsole, 
                DetectionType.IamUnusedCredentialDeleteApiKey,
                DetectionType.IamUnusedCredentialDisableApiKey,
                DetectionType.IamUnusedCredentialWarningApiKey))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String userName = arn[5].replaceFirst("user/", "");
        String accessKey = null;
        if (detected.getType().equals(DetectionType.IamUnusedCredentialDeleteApiKey.name())
        		|| detected.getType().equals(DetectionType.IamUnusedCredentialDisableApiKey.name())) {
            int stringInd = userName.lastIndexOf("/access-key/");
            accessKey = userName.substring(stringInd + 12);  //12 is strlen of "/access-key/"
            userName = userName.substring(0, stringInd);
        }
        // usernames can have path components so strip them off if present
        if (userName.contains("/")) {
            userName = userName.substring(userName.lastIndexOf("/") + 1);
        }


        final AmazonIdentityManagement iam;
        try {
            iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "IAM Unused Credential", e);
            return;
        }

        switch (DetectionType.valueOf(detected.getType())) {
            case IamUnusedCredentialAwsConsole:
                remediateLogin(iam, userName, detected, srContext.LOGTAG);
                break;
            case IamUnusedCredentialDeleteApiKey:
            case IamUnusedCredentialDisableApiKey:
            	remediateAccessKey(iam, userName, accessKey, detected, srContext.LOGTAG);
            	break;
            case IamUnusedCredentialWarningApiKey:
            	break;
            default:
                // can't happen because of remediatorPreConditionFailed checks but appease SpotBugs
                setError(detected, srContext.LOGTAG, "Invalid risk type: " + detected.getType());
                break;
        }
    }

    private void remediateLogin(AmazonIdentityManagement iam, String userName, DetectedSecurityRisk detected, String LOGTAG) {
        GetUserRequest getUserRequest = new GetUserRequest().withUserName(userName);
        GetUserResult getUserResult = iam.getUser(getUserRequest);
        User user = getUserResult.getUser();
        List<String> changes = new ArrayList<>();

        if (user == null) {
            setError(detected, LOGTAG, "userName not found in AWS for ARN: " + detected.getAmazonResourceName());
            return;
        }

        Date passwordLastUsed = user.getCreateDate();

        if (user.getPasswordLastUsed() != null) {
            passwordLastUsed = user.getPasswordLastUsed();
        }

        GetLoginProfileRequest getLoginProfileRequest = new GetLoginProfileRequest().withUserName(user.getUserName());
        try {
            // call for side-effect of throwing exception if user does not exist
            iam.getLoginProfile(getLoginProfileRequest);
        } catch (NoSuchEntityException e) {
            disableSshPublicKeys(iam, userName, changes);
            disableServiceSpecificCredentials(iam, userName, changes);
            if (changes.isEmpty()) {
                setNoLongerRequired(detected, LOGTAG, "Password login is already deleted");
            } else {
                setSuccess(detected, LOGTAG, "CodeCommit credentials deleted for user: " + userName);
            }
            return;
        }

        if (System.currentTimeMillis() - passwordLastUsed.getTime() <= ((long)daysBeforeExpiry) * 24L * 60L * 60L * 1000L) {
            setNoLongerRequired(detected, LOGTAG, "Password login has not yet expired");
            return;
        }

        disableSshPublicKeys(iam, userName, changes);
        disableServiceSpecificCredentials(iam, userName, changes);

        //remediate
        boolean deleteSuccess = false;
        for (int retry = 0; retry < 5; retry++) {
            try {
                DeleteLoginProfileRequest deleteLoginProfileRequest = new DeleteLoginProfileRequest().withUserName(userName);
                iam.deleteLoginProfile(deleteLoginProfileRequest);
                deleteSuccess = true;
                break;
            } catch (EntityTemporarilyUnmodifiableException e) {
                try {
                    logger.info(LOGTAG + "Login Profile temporarily unmodifiable on retry " + retry);
                    sleep(retry * 15000L);
                } catch (InterruptedException e1) {
                    logger.info(LOGTAG + "Interrupted while waiting for Login Profile. Ignoring.");
                    break;
                }
            }
        }
        if (!deleteSuccess) {
            setError(detected, LOGTAG, "Login Profile cannot be deleted because it is currently in use.");
            return;
        }

        if (changes.isEmpty()) {
            setSuccess(detected, LOGTAG,
                    "Password for AWS console successfully deleted for user " + userName);
        } else {
            setSuccess(detected, LOGTAG,
                    "CodeCommit credentials are disabled and password for AWS console is deleted for user " + userName);
        }
    }

    private void disableSshPublicKeys(AmazonIdentityManagement iam, String userName, List<String> changes) {
        ListSSHPublicKeysRequest listSSHPublicKeysRequest = new ListSSHPublicKeysRequest().withUserName(userName);
        boolean done = false;
        while (!done) {
            ListSSHPublicKeysResult listSSHPublicKeysResult = iam.listSSHPublicKeys(listSSHPublicKeysRequest);

            for (SSHPublicKeyMetadata publicKey : listSSHPublicKeysResult.getSSHPublicKeys()) {
                if (publicKey.getStatus().equals(StatusType.Active.name())) {
                    UpdateSSHPublicKeyRequest updateSSHPublicKeyRequest = new UpdateSSHPublicKeyRequest()
                            .withUserName(userName)
                            .withSSHPublicKeyId(publicKey.getSSHPublicKeyId())
                            .withStatus(StatusType.Inactive);
                    iam.updateSSHPublicKey(updateSSHPublicKeyRequest);
                    changes.add("SSH Key " + publicKey.getSSHPublicKeyId() + " marked as inactive");
                }
            }

            listSSHPublicKeysRequest.setMarker(listSSHPublicKeysResult.getMarker());

            if (!listSSHPublicKeysResult.getIsTruncated()) {
                done = true;
            }

        }
    }

    private void disableServiceSpecificCredentials(AmazonIdentityManagement iam, String userName, List<String> changes) {
        ListServiceSpecificCredentialsRequest listServiceSpecificCredentialsRequest = new ListServiceSpecificCredentialsRequest().withUserName(userName);
        ListServiceSpecificCredentialsResult listServiceSpecificCredentialsResult = iam.listServiceSpecificCredentials(listServiceSpecificCredentialsRequest);

        for (ServiceSpecificCredentialMetadata metadata : listServiceSpecificCredentialsResult.getServiceSpecificCredentials()) {
            if (metadata.getStatus().equals(StatusType.Active.name())) {
                UpdateServiceSpecificCredentialRequest updateServiceSpecificCredentialRequest = new UpdateServiceSpecificCredentialRequest()
                        .withUserName(userName)
                        .withServiceSpecificCredentialId(metadata.getServiceSpecificCredentialId())
                        .withStatus(StatusType.Inactive);
                iam.updateServiceSpecificCredential(updateServiceSpecificCredentialRequest);
                changes.add("Credential id " + metadata.getServiceSpecificCredentialId() + " marked as inactive");
            }
        }
    }


    private void remediateAccessKey(AmazonIdentityManagement iam, String userName, String accessKey, DetectedSecurityRisk detected, String LOGTAG) {

    	AccessKeyMetadata metadata = getAccessKeyMetadata(iam, userName, accessKey);

        if (metadata == null) {
            setNoLongerRequired(detected, LOGTAG, "Access Key already deleted.");
            return;
        }

        GetAccessKeyLastUsedRequest lastUsedRequest = new GetAccessKeyLastUsedRequest()
                .withAccessKeyId(metadata.getAccessKeyId());

        GetAccessKeyLastUsedResult lastUsedResponse = iam.getAccessKeyLastUsed(lastUsedRequest);

        AccessKeyLastUsed lastUsed = lastUsedResponse.getAccessKeyLastUsed();
        Date lastTimeUsed = metadata.getCreateDate();
        if (lastUsed.getLastUsedDate() != null) {
            lastTimeUsed = lastUsed.getLastUsedDate();
        }

        switch (DetectionType.valueOf(detected.getType())) {
        case IamUnusedCredentialDeleteApiKey:
            if (System.currentTimeMillis() - lastTimeUsed.getTime() <= ((long)daysBeforeExpiry) * 24L * 60L * 60L * 1000L) {
                setNoLongerRequired(detected, LOGTAG, "Access key has not yet expired.");
                return;
            }

            //remediate
            DeleteAccessKeyRequest deleteAccessKeyRequest = new DeleteAccessKeyRequest()
                    .withUserName(userName)
                    .withAccessKeyId(accessKey);

            iam.deleteAccessKey(deleteAccessKeyRequest);
            setSuccess(detected, LOGTAG, "Access key '" + accessKey + "' successfully deleted.");
            break;
        case IamUnusedCredentialDisableApiKey:
            if (System.currentTimeMillis() - lastTimeUsed.getTime() <= ((long)daysBeforeDisable) * 24L * 60L * 60L * 1000L) {
                setNoLongerRequired(detected, LOGTAG, "Access key is not inactive long enough for it to be disabled.");
                return;
            }
            if ("Inactive".equals(metadata.getStatus())) {
                //setNotificationOnly(detected, LOGTAG);
                return;            	
            }
            UpdateAccessKeyRequest request = new UpdateAccessKeyRequest()
            	    .withAccessKeyId(accessKey)
            	    .withUserName(userName)
            	    .withStatus(StatusType.Inactive);

            iam.updateAccessKey(request);
            setSuccess(detected, LOGTAG, "Access key '" + accessKey + "' successfully disabled.");
            break;
        case IamUnusedCredentialWarningApiKey:
        default:
            break;
    }

    }


    private AccessKeyMetadata getAccessKeyMetadata(AmazonIdentityManagement iam, String userName, String accessKey) {
        boolean done = false;
        ListAccessKeysRequest accessKeysRequest = new ListAccessKeysRequest()
                .withUserName(userName);

        while (!done) {

            ListAccessKeysResult accessKeys = iam.listAccessKeys(accessKeysRequest);

            for (AccessKeyMetadata metadata : accessKeys.getAccessKeyMetadata()) {
                if (metadata.getAccessKeyId().equals(accessKey)) {
                    return metadata;
                }
            }

            accessKeysRequest.setMarker(accessKeys.getMarker());

            if (!accessKeys.getIsTruncated()) {
                done = true;
            }
        }

        return null;
    }
}
