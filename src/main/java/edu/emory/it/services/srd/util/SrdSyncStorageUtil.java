package edu.emory.it.services.srd.util;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public class SrdSyncStorageUtil {
    private static final Object lock = new Object();

    private static SrdSyncStorageUtil instance;

    private AmazonDynamoDB amazonDynamoDB;
    /** see SrdSyncStorage/storageTableName deployment descriptor property */
    private Table storageTable;

    public static SrdSyncStorageUtil getInstance() {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new SrdSyncStorageUtil();
                }
            }
        }
        return instance;
    }

    /**
     * Initialize the singleton.
     *
     * @param amazonDynamoDB Amazon DynamoDB
     * @param storageTable storage table
     */
    public void initialize(AmazonDynamoDB amazonDynamoDB, Table storageTable) {
        this.amazonDynamoDB = amazonDynamoDB;
        this.storageTable = storageTable;
    }

    public AmazonDynamoDB getAmazonDynamoDB() {
        return amazonDynamoDB;
    }

    public Table getStorageTable() {
        return storageTable;
    }
}
