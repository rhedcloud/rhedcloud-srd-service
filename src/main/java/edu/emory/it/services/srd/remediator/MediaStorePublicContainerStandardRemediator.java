package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * No remediation.<br>
 *
 * <p>Security: HIPAA workloads: Enforce, delete. (see MediaStorePublicContainerHipaaRemediator)<br>
 * Security: Standard workloads: revisit. (this remediator)<br>
 * Preference: Strong</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.MediaStorePublicContainerStandardDetector the detector
 */
public class MediaStorePublicContainerStandardRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:mediastore:", srContext.LOGTAG, DetectionType.MediaStorePublicContainer))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        setNotificationOnly(detected, srContext.LOGTAG);
    }
}
