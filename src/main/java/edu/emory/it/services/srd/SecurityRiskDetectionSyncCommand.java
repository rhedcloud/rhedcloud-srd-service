package edu.emory.it.services.srd;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.Message;

import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.SyncCommand;
import org.openeai.layouts.EnterpriseLayoutException;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult;
import com.amazonaws.arn.Arn;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.openii.openeai.commands.OpeniiSyncCommand;

import edu.emory.it.services.srd.provider.ProviderException;
import edu.emory.it.services.srd.provider.SiemIntegrationProvider;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SrdMetricUtil;
import edu.emory.it.services.srd.util.SrdSyncStorageUtil;

/**
 * Handle SecurityRiskDetection Create-Sync messages.<br>
 * This command has two responsibilities:
 * <ol>
 *     <li>For "interesting" SecurityRiskDetection objects, persist them to a DynamoDB table.</li>
 *     <li>Send the SecurityRiskDetection to a Security Information and Event Management (SIEM) system.</li>
 * </ol>
 * Care is taken to persist only a single copy of a SecurityRiskDetection object since duplicates can be expected.
 * Details of what is "interesting" and why duplicates are expected can be found in code comments below.<br>
 * The following AppConfig settings are used:
 * <ul>
 *     <li>Optional - <pre>siemIntegrationProviderClassName</pre> - class name of the SIEM Integration provider.</li>
 *     <li>Optional - <pre>environment</pre> - arbitrary string to identify environment, like DEV or PROD.</li>
 * </ul>
 */
public class SecurityRiskDetectionSyncCommand extends OpeniiSyncCommand implements SyncCommand {
    //private static final String LOGTAG = "[SrdSyncCommand] ";
    private static final String LOGTAG = "";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionSyncCommand.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /** Optional AppConfig - class name of the SIEM Integration provider */
    private SiemIntegrationProvider siemIntegrationProvider = null;
    /** Optional AppConfig - arbitrary string to identify environment, like DEV or PROD */
    private final String environment;
	private int m_comingOfAgeInMinutes;

    public SecurityRiskDetectionSyncCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Initialize a provider
        String siemIntegrationProviderClassName = getProperties().getProperty("siemIntegrationProviderClassName");
        if (siemIntegrationProviderClassName == null || siemIntegrationProviderClassName.equals("")) {
            String errMsg = "SIEM integration - no siemIntegrationProviderClassName property specified.";
            logger.info(LOGTAG + errMsg);
        }
        else {
            try {
                siemIntegrationProvider = (SiemIntegrationProvider) Class.forName(siemIntegrationProviderClassName).newInstance();
                logger.info(LOGTAG + "SIEM integration - initializing provider: " + siemIntegrationProvider.getClass().getName());
                siemIntegrationProvider.init(getProperties());
            }
            catch (ClassNotFoundException e) {
                String errMsg = "SIEM integration - class named " + siemIntegrationProviderClassName + " not found on the classpath"
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
            catch (IllegalAccessException e) {
                String errMsg = "SIEM integration - an error occurred getting a class for name: " + siemIntegrationProviderClassName
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
            catch (InstantiationException e) {
                String errMsg = "SIEM integration - an error occurred initializing the " + siemIntegrationProviderClassName
                        + ". The exception is: " + e.getMessage();
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
        }

        environment = getProperties().getProperty("Environment");
        if (environment == null || environment.equals("")) {
            String errMsg = "SIEM integration - no Environment property specified.";
            logger.info(LOGTAG + errMsg);
        }
        
        String comingOfAgeInMinutesString = getProperties().getProperty("ComingOfAgeInMinutes");
        if (comingOfAgeInMinutesString == null || comingOfAgeInMinutesString.equals("")) {
            String errMsg = "SIEM integration - no ComingOfAgeInMinutes property specified.";
            logger.info(LOGTAG + errMsg);
        } else {
	        try {
	        	m_comingOfAgeInMinutes = Integer.parseInt(comingOfAgeInMinutesString);
	        } catch (NumberFormatException e) {
	            String errMsg = "SIEM integration - invalid ComingOfAgeInMinutes property specified. Must be an integer.";
	            logger.error(LOGTAG + errMsg);     	
	        }
        }
        
        logger.info(LOGTAG + "SIEM integration - m_comingOfAgeInMinutes = "+m_comingOfAgeInMinutes);
        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public void execute(int messageNumber, Message aMessage) throws CommandException {
        // get the message body from the JMS message
        Document inDoc;
        
        try {
            inDoc = initializeInput(messageNumber, aMessage);
        }
        catch (Exception e) {
            String errMsg = "Exception occurred initializing inputs. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the ControlArea element.
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgObject = eControlArea.getAttribute("messageObject").getValue();

        // Verify that the message is a SecurityRiskDetection-Create-Sync
        // since both SecurityRiskDetection sync and Account sync messages are routed to the
        // SrdServiceTopic we just ignore the messages we don't handle
        if (!msgObject.equalsIgnoreCase("SecurityRiskDetection")) {
            return;
        }
        if (!msgAction.equalsIgnoreCase("Create")) {
            return;
        }

        // Verify that SecurityRiskDetection element is not null; if it is, reply with an error.
        Element newData = inDoc.getRootElement().getChild("DataArea").getChild("NewData").getChild("SecurityRiskDetection");
        if (newData == null) {
            String errDesc = "Invalid NewData element found in the Create-Sync message. This command expects a SecurityRiskDetection.";
            logger.fatal(LOGTAG + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            throw new CommandException(errDesc);
        }

        SecurityRiskDetection detection;
        try {
            detection = (SecurityRiskDetection) getAppConfig().getObjectByType(SecurityRiskDetection.class.getName());
            detection.buildObjectFromInput(newData);
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error retrieving a SecurityRiskDetection object from AppConfig: The exception is: " + e.getMessage());
            String errDesc = "Error retrieving EO.";
            throw new CommandException(errDesc);
        }
        catch (EnterpriseLayoutException ele) {
            String errDesc = "An error occurred building object from the " +
                    "SecurityRiskDetection element in the Create-Sync message. The exception is: " + ele.getMessage();
            logger.fatal(LOGTAG + errDesc);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
            throw new CommandException(errDesc);
        }
        
        String accountId = detection.getAccountId();
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
        logger.info( LOGTAG + "accountId = "+accountId);
		Account account = awsAccountServiceUtil.accountCacheGet(accountId);
		boolean accountAgeNotDetermined = account==null ? true : false;
		boolean accountAgeDetermined = !accountAgeNotDetermined;
		long accountAgeInMillis = 0;

        // gather metrics as we process the sync messages
        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();
        srdMetricUtil.cacheLastSecurityRiskDetectionIdSyncPersister(detection.getSecurityRiskDetectionId());

        // create SecurityRiskDetector DynamoDB Items only if
        // there are detected security risks or the detection was not successful
        // the items are considered immutable so the putItem will use a condition expression.
        // at the same time that the Item is created, a JSON document is created for the SIEM Integration.
        List<Map.Entry<Item, ObjectNode>> newItems = new ArrayList<>();

        if (detection.getDetectedSecurityRiskLength() > 0) {
            for (int i = 0; i < detection.getDetectedSecurityRiskLength(); i++) {
                newItems.add(ni(detection, i, detection.getDetectedSecurityRisk(i)));
            }
        }
        else if (!DetectionStatus.DETECTION_SUCCESS.getStatus().equals(detection.getDetectionResult().getStatus())) {
            newItems.add(ni(detection, 0, null));
        }
        else {
            logger.info(LOGTAG + "Skipping persist detection for "
                    + detection.getDetectionResult().getType() + "-" + detection.getAccountId()
                    + " for sync message number " + messageNumber
                    + " and detection status '" + detection.getDetectionResult().getStatus() + "'"
                    + " and number of risks " + detection.getDetectedSecurityRisk().size()
                    + " with securityRiskDetectionId " + detection.getSecurityRiskDetectionId()
                    + " with instanceId " + detection.getInstanceId());
        }

        if (newItems.size() > 0) {
            Table table = SrdSyncStorageUtil.getInstance().getStorageTable();

            for (int i = 0; i < newItems.size(); i++) {
                long persistStart = System.currentTimeMillis();
                Map.Entry<Item, ObjectNode> ne = newItems.get(i);
                Item item = ne.getKey();
                ObjectNode siemJson = ne.getValue();
                boolean needSiemIntegration;

                String persistMsg = "persist detection " + (i + 1) + " of " + newItems.size()
                        + " to " + table.getTableName() + " item " + item
                        + " for sync message number " + messageNumber
                        + " with securityRiskDetectionId " + item.getString("securityRiskDetectionId")
                        + " with instanceId " + detection.getInstanceId();

                try {
                    // we can receive duplicate SecurityRiskDetection sync messages because we can't use a MessageBalancer
                    // by adding the ConditionExpression we guarantee that duplicate SecurityRiskDetection aren't persisted
                    // this works by, first, using the primary key (typeAccount (hash) and createDatetime (range)), find
                    // an item that matches and then applying the ConditionExpression.  which mean that if an item is found
                    // then the ConditionExpression will fail because the typeAccount attribute must exist in the item.

                    table.putItem(item, "attribute_not_exists(typeAccount)", null, null);
                    logger.info(LOGTAG + "successfully " + persistMsg
                            + " in elapsed time " + (System.currentTimeMillis() - persistStart) + " ms");

                    // perform SIEM Integration with the SecurityRiskDetection sync message
                    needSiemIntegration = true;
                }
                catch (ConditionalCheckFailedException e) {
                    logger.info(LOGTAG + "already " + persistMsg
                            + " in elapsed time " + (System.currentTimeMillis() - persistStart) + " ms");

                    // duplicate SecurityRiskDetection sync messages should not be sent to the SIEM
                    needSiemIntegration = false;
                }
                catch (Exception e) {
                    logger.error(LOGTAG + "failed to " + persistMsg
                            + " with exception " + e.getMessage()
                            + " in elapsed time " + (System.currentTimeMillis() - persistStart) + " ms");

                    // still need SIEM Integration even when we fail to persist the SecurityRiskDetection sync message
                    needSiemIntegration = true;
                }

                // Send SecurityRiskDetection information to a Security Information and Event Management (SIEM) System.
                if (needSiemIntegration && siemIntegrationProvider != null) {
                    long siemIntegrationStart = System.currentTimeMillis();
                    try {
                		if ( accountAgeDetermined ) {
                			accountAgeInMillis = getAccountAgeInMillis(account);
                			logger.info( LOGTAG + accountId + " accountAgeInMillis = "+ accountAgeInMillis);
                			logger.info( LOGTAG + accountId + " accountAgeInMinutes = "+ accountAgeInMillis/1000/60);
                			logger.info( LOGTAG + accountId + " accountAgeInHours = "+ accountAgeInMillis/1000/60/60);
                			logger.info( LOGTAG + accountId + " createDatetime = "+ account.getCreateDatetime());
                        	if (accountAgeInMillis > 1000 * 60 * m_comingOfAgeInMinutes) { 
                        		siemIntegrationProvider.sendSiem(siemJson);
                        		logger.info(LOGTAG + "SIEM sent for account ID "+accountId);
                        	} else {
                        		logger.info(LOGTAG + "Not sending to SIEM because account ID, \""
                        				+ accountId + "\" was created less than "+m_comingOfAgeInMinutes+" minutes ago.");
                        	}
                		}
                    }
                    catch (ProviderException e) {
                        // logging for success or failure is expected to be done by the provider so just
                        // swallow the exception because there is nothing more to be done and
                        // the rest of the items have to be persisted and sent to SIEM.
                    }
                    finally {
                        logger.info(LOGTAG + "SIEM integration for payload " + (i + 1) + " of " + newItems.size()
                                + " with securityRiskDetectionId " + item.getString("securityRiskDetectionId")
                                + " in elapsed time " + (System.currentTimeMillis() - siemIntegrationStart) + " ms");
                    }
                }
            }
        }
    }

    private long getAccountAgeInMillis(Account account) {
    	Datetime rdt = new Datetime();
    	Datetime cdt = account.getCreateDatetime();
    	if (!rdt.getTimezone().equals(cdt.getTimezone())) {
    		logger.error(LOGTAG + "The timezone of the server is different than the one in the account create datetime.");
    	}
     	long accountCreateDatetimeMillis = cdt.toCalendar().getTimeInMillis();
		return System.currentTimeMillis() - accountCreateDatetimeMillis;
	}

	/**
     * Create a new Item to put to DynamoDB and a JSON message for SIEM Integration.<br>
     * The JSON message is a flattened version of the SecurityRiskDetection object.<br>
     * <pre>
     * {
     *   "type": "Example",
     *   "accountId": "123456789012",
     *   "awsAccountName": "aws-account-76",
     *   "complianceClass": "Standard | HIPAA | Enhanced Security",
     *   "detector": "ExampleDetector",
     *   "remediator": "ExampleRemediator",
     *   "arn": "arn:aws:fake:us-east-1:123456789012:example",
     *   "region": "us-east-1",
     *   "detectionResultStatus": "Detection Success",
     *   "detectionResultError": "Example error.  Error 2",
     *   "remediateResultStatus": "Remediation Failed",
     *   "remediateResultDescription": "Remediation Error",
     *   "remediateResultError": "Example error.  Error 2",
     *   "securityRiskDetectionId": "647668",
     *   "instanceId": "tug",
     *   "environment": "DEV | TEST | STAGE | PROD | WOMBAT | GOOSE | WHATEVER",
     *   "createDatetime": "2020-10-07T15:13:53+00:00"  -- ISO 8601
     * }
     * </pre>
     *
     * @param detection SecurityRiskDetection
     * @param count risk number - to uniquify the range key
     * @param securityRisk DetectedSecurityRisk
     * @return DynamoDB Item and JSON message for SIEM Integration
     */
    private Map.Entry<Item, ObjectNode> ni(SecurityRiskDetection detection, int count, DetectedSecurityRisk securityRisk) {
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();

        /*
         * NOTE:
         * In the DynamoDB item, detection.type is required so it has to come from somewhere.
         * Normally, it comes from the DetectedSecurityRisk but when detection fails there will
         *  not be a DetectedSecurityRisk.  To handle this case, type was added to DetectionResult.
         * So, while DetectionResult.type is a mandatory MOA field, it is not persisted
         *  to the detection.detectionResult item because it would be redundant with the detection.type field.
         */
        String type = (securityRisk != null) ? securityRisk.getType() : detection.getDetectionResult().getType();
        String arn = (securityRisk != null) ? securityRisk.getAmazonResourceName() : null;
        RemediationResult remediationResult = (securityRisk != null) ? securityRisk.getRemediationResult() : null;

        Map<String, Object> itemDetection = new HashMap<>();
        ObjectNode riskJson = objectMapper.createObjectNode();

        String accountId = detection.getAccountId();
        String complianceClass = awsAccountServiceUtil.accountCacheAccountComplianceClass(accountId, LOGTAG);
        String accountAlias = awsAccountServiceUtil.getCachedAccountAlias(accountId);
        if (accountAlias == null) {
            logger.error(LOGTAG + "Unknown account alias for account id " + accountId);
            accountAlias = "unknown";
        }
        
        itemDetection.put("accountId", accountId);
        riskJson.put("accountId", accountId);
        // itemDetection doesn't need "awsAccountName" or "complianceClass"
        riskJson.put("awsAccountName", accountAlias);
        riskJson.put("complianceClass", complianceClass);
        itemDetection.put("type", type);
        riskJson.put("type", type);
        if (arn != null) {
            itemDetection.put("arn", arn);
            riskJson.put("arn", arn);
            // itemDetection doesn't need "region"
            String region = Arn.fromString(arn).getRegion();
            riskJson.put("region", (region == null || region.isEmpty()) ? "global" : region);
        }
        itemDetection.put("detector", detection.getSecurityRiskDetector());
        riskJson.put("detector", detection.getSecurityRiskDetector());
        itemDetection.put("remediator", detection.getSecurityRiskRemediator());
        riskJson.put("remediator", detection.getSecurityRiskRemediator());
        itemDetection.put("instanceId", detection.getInstanceId());
        riskJson.put("instanceId", detection.getInstanceId());
        itemDetection.put("index", count);
        // riskJson doesn't need "index"

        Map<String, Object> detectionResultItem = new HashMap<>();
        detectionResultItem.put("status", detection.getDetectionResult().getStatus());
        riskJson.put("detectionResultStatus", detection.getDetectionResult().getStatus());
        if (detection.getDetectionResult().getError() != null && !detection.getDetectionResult().getError().isEmpty()) {
            List<String> stringyErrors = new ArrayList<>();
            String concatenatedErrors = "";
            for (Object oError : detection.getDetectionResult().getError()) {
                stringyErrors.add((String) oError);
                concatenatedErrors += (concatenatedErrors.isEmpty() ? "" : "  ") + oError;
            }
            detectionResultItem.put("error", stringyErrors);
            riskJson.put("detectionResultError", concatenatedErrors);
        }
        itemDetection.put("detectionResult", detectionResultItem);

        if (remediationResult != null) {
            Map<String, Object> remediateResultItem = new HashMap<>();
            remediateResultItem.put("status", remediationResult.getStatus());
            riskJson.put("remediateResultStatus", remediationResult.getStatus());
            remediateResultItem.put("description", remediationResult.getDescription());
            riskJson.put("remediateResultDescription", remediationResult.getDescription());
            if (remediationResult.getError() != null && !remediationResult.getError().isEmpty()) {
                List<String> stringyErrors = new ArrayList<>();
                String concatenatedErrors = "";
                for (Object oError : remediationResult.getError()) {
                    stringyErrors.add((String) oError);
                    concatenatedErrors += (concatenatedErrors.isEmpty() ? "" : "  ") + oError;
                }
                remediateResultItem.put("error", stringyErrors);
                riskJson.put("remediateResultError", concatenatedErrors);
            }
            itemDetection.put("remediateResult", remediateResultItem);
        }


        // the primary key fields - make createDatetime unique for multiple risks using the counter
        String typeAccount = type + "-" + accountId;
        Calendar createDatetimeCal = detection.getCreateDatetime().toCalendar();
        Long createDatetime = count + createDatetimeCal.getTimeInMillis();

        // riskJson doesn't need "typeAccount"
        riskJson.put("createDatetime", createDatetimeCal.toInstant().toString());
        riskJson.put("securityRiskDetectionId", detection.getSecurityRiskDetectionId());
        if (environment != null) {
            // itemDetection doesn't need "environment"
            riskJson.put("environment", environment);
        }

        Item item = new Item()
                .withPrimaryKey("typeAccount", typeAccount, "createDatetime", createDatetime)
                .withString("securityRiskDetectionId", detection.getSecurityRiskDetectionId())
                .withMap("detection", itemDetection);

        return new AbstractMap.SimpleEntry<>(item, riskJson);
    }
    
}
