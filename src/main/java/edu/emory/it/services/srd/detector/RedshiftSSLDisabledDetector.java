package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.redshift.AmazonRedshift;
import com.amazonaws.services.redshift.AmazonRedshiftClient;
import com.amazonaws.services.redshift.model.Cluster;
import com.amazonaws.services.redshift.model.ClusterParameterGroupStatus;
import com.amazonaws.services.redshift.model.ClusterParameterStatus;
import com.amazonaws.services.redshift.model.DescribeClusterParametersRequest;
import com.amazonaws.services.redshift.model.DescribeClusterParametersResult;
import com.amazonaws.services.redshift.model.DescribeClustersRequest;
import com.amazonaws.services.redshift.model.DescribeClustersResult;
import com.amazonaws.services.redshift.model.Parameter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Check all Redshift clusters to make sure encryption in transit is enabled.
 *
 * @see edu.emory.it.services.srd.remediator.RedshiftSSLDisabledRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RedshiftSSLDisabledDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            AmazonRedshift client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRedshiftClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RedshiftSSLDisabled,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            // inspect all of the Redshift clusters
            List<Cluster> allClusters = new ArrayList<>();
            DescribeClustersRequest describeClustersRequest = new DescribeClustersRequest();
            DescribeClustersResult describeClustersResult;

            do {
                try {
                    describeClustersResult = client.describeClusters(describeClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RedshiftSSLDisabled, srContext.LOGTAG,
                            "Error describing Redshift clusters", e);
                    return;
                }
                allClusters.addAll(describeClustersResult.getClusters());
                describeClustersRequest.setMarker(describeClustersResult.getMarker());
            } while (describeClustersResult.getMarker() != null);

            /*
             * Parameter groups can be changed for a cluster but may not take effect until a reboot.
             * There are several parameter group statuses that indicates a reboot is required but
             *  the most important is when the parameter value is not in sync with the database.
             * Until the cluster is rebooted we can't tell what the actual value of the "require_ssl" parameter is.
             * So, we mark the cluster at-risk though it'll probably just be a notification.
             */
            for (Cluster cluster : allClusters) {
                String parameterGroupName = null;
                String parameterApplyStatus = null;

                // pretty sure there can only be one cluster parameter group
                for (ClusterParameterGroupStatus clusterParameterGroupStatus : cluster.getClusterParameterGroups()) {
                    parameterGroupName = clusterParameterGroupStatus.getParameterGroupName();
                    parameterApplyStatus = clusterParameterGroupStatus.getParameterApplyStatus();  // overridden if require_ssl is found

                    // cardinality
                    // zero.  for example, new cluster with default parameter group
                    // or one.  for example, cluster is pending reboot
                    Optional<ClusterParameterStatus> paramStatus = clusterParameterGroupStatus.getClusterParameterStatusList().stream()
                            .filter(p -> p.getParameterName().equals("require_ssl"))
                            .findAny();
                    if (paramStatus.isPresent()) {
                        parameterApplyStatus = paramStatus.get().getParameterApplyStatus();
                        break;
                    }
                }
                if (parameterGroupName == null || parameterApplyStatus == null) {
                    // very odd that the apply-status isn't known but can't say anything about the cluster
                    continue;
                }

                // if the require_ssl parameter is in-sync with the cluster then its value is current
                // we need another API call to get the current value and, as usual, it is a paging API which complicates the code
                if (parameterApplyStatus.equals("in-sync")) {
                    DescribeClusterParametersRequest describeClusterParametersRequest = new DescribeClusterParametersRequest()
                            .withParameterGroupName(parameterGroupName);
                    DescribeClusterParametersResult describeClusterParametersResult;
                    boolean inTransitEncryptionEnabled = false;

                    do {
                        try {
                            describeClusterParametersResult = client.describeClusterParameters(describeClusterParametersRequest);
                        }
                        catch (Exception e) {
                            setDetectionError(detection, DetectionType.RedshiftSSLDisabled, srContext.LOGTAG,
                                    "Error describing Redshift cluster parameters", e);
                            return;
                        }

                        // cardinality - one and only one across every page of results
                        Optional<Parameter> requireSslParameter = describeClusterParametersResult.getParameters().stream()
                                .filter(clusterParameterStatus -> clusterParameterStatus.getParameterName().equals("require_ssl"))
                                .findAny();
                        if (requireSslParameter.isPresent()) {
                            inTransitEncryptionEnabled = requireSslParameter.get().getParameterValue().equals("true");
                            break;
                        }

                        describeClusterParametersRequest.setMarker(describeClusterParametersResult.getMarker());
                    } while (describeClusterParametersResult.getMarker() != null);

                    if (!inTransitEncryptionEnabled) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.RedshiftSSLDisabled,
                                "arn:aws:redshift:" + region.getName() + ":" + accountId + ":cluster:" + cluster.getClusterIdentifier());
                    }
                }
                else {
                    // require_ssl parameter is not current until a reboot
                    addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.RedshiftPendingReboot,
                            "arn:aws:redshift:" + region.getName() + ":" + accountId + ":cluster:" + cluster.getClusterIdentifier());
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "RedshiftSSLDisabled";
    }
}
