package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.Cluster;
import com.amazonaws.services.elasticmapreduce.model.ClusterSummary;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterResult;
import com.amazonaws.services.elasticmapreduce.model.ListClustersRequest;
import com.amazonaws.services.elasticmapreduce.model.ListClustersResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Detect Elastic Map Reduce clusters in a disallowed subnet.
 *
 * <p>For instance fleets, the cluster may not currently be running in a disallowed subnet
 * but could switch to one of them the next time it is launched.  So, even though the cluster
 * is not currently launched in a disallowed subnet it will be detected as a risk.</p>
 *
 * @see edu.emory.it.services.srd.remediator.EMRLaunchedWithinManagementSubnetsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EMRLaunchedWithinManagementSubnetsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            try {
                final AmazonEC2 ec2Client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
                final AmazonElasticMapReduce emrClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonElasticMapReduceClient.class);

                // collect the IDs of the disallowed subnets
                Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client);

                // we'll get the details about the each Elastic Map Reduce cluster.
                // the details tell us about the subnets that it is allowed to be launched in.
                // the MGMT and Attachment subnets must not be used.

                String listClustersRequestMarker = null;
                do {
                    ListClustersRequest listClustersRequest = new ListClustersRequest();
                    // deal with pagination
                    if (listClustersRequestMarker != null)
                        listClustersRequest.setMarker(listClustersRequestMarker);
                    ListClustersResult listClustersResult = emrClient.listClusters(listClustersRequest);
                    listClustersRequestMarker = listClustersResult.getMarker();

                    for (ClusterSummary clusterSummary : listClustersResult.getClusters()) {
                        DescribeClusterRequest describeClusterRequest = new DescribeClusterRequest()
                                .withClusterId(clusterSummary.getId());
                        DescribeClusterResult describeClusterResult = emrClient.describeCluster(describeClusterRequest);
                        Cluster cluster = describeClusterResult.getCluster();

                        // terminated EMR clusters stay on the dashboard for a long time (up to two weeks)
                        // so just ignore them here instead of flagging them as "Remediation Unnecessary"
                        if ("TERMINATED".equals(cluster.getStatus().getState())
                                || "TERMINATED_WITH_ERRORS".equals(cluster.getStatus().getState())) {
                            continue;
                        }

                        // check the current launch subnet
                        if (disallowedSubnets.contains(cluster.getEc2InstanceAttributes().getEc2SubnetId())) {
                            String arn = "arn:aws:elasticmapreduce:" + region.getName() + ":" + accountId + ":cluster/" + cluster.getId();
                            addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.EMRLaunchedWithinManagementSubnets, arn,
                                    new SrdNameValuePair("region", region.getName()),
                                    new SrdNameValuePair("clusterId", cluster.getId()),
                                    new SrdNameValuePair("clusterName", cluster.getName()),
                                    new SrdNameValuePair("clusterState", cluster.getStatus().getState()));
                            continue;
                        }

                        // for instance fleets, the cluster may not currently be running in a
                        // disallowed subnet (Ec2SubnetId in the test above) but could switch to
                        // one of the RequestedEc2SubnetIds the next time it is launched
                        for (String subnet : cluster.getEc2InstanceAttributes().getRequestedEc2SubnetIds()) {
                            if (disallowedSubnets.contains(subnet)) {
                                String arn = "arn:aws:elasticmapreduce:" + region.getName() + ":" + accountId + ":cluster/" + cluster.getId();
                                addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.EMRLaunchedWithinManagementSubnets, arn,
                                        new SrdNameValuePair("region", region.getName()),
                                        new SrdNameValuePair("clusterId", cluster.getId()),
                                        new SrdNameValuePair("clusterName", cluster.getName()),
                                        new SrdNameValuePair("clusterState", cluster.getStatus().getState()));
                                break;
                            }
                        }
                    }
                } while (listClustersRequestMarker != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.EMRLaunchedWithinManagementSubnets,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "EMRLaunchedWithinManagementSubnets";
    }
}
