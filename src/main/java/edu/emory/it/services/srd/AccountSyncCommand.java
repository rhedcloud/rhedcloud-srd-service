package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.openii.openeai.commands.OpeniiSyncCommand;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.SyncCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.objects.resources.Error;

import javax.jms.Message;
import java.util.ArrayList;
import java.util.List;

public class AccountSyncCommand extends OpeniiSyncCommand implements SyncCommand {
    private static final String LOGTAG = "[AccountSyncCommand] ";
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(AccountSyncCommand.class);

    public AccountSyncCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);
        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public void execute(int messageNumber, Message message) throws CommandException {
        long executeStart = System.currentTimeMillis();

        // get the message body from the JMS message
        Document inDoc;
        try {
            inDoc = initializeInput(messageNumber, message);
        } catch (Exception e) {
            String errMsg = "Exception occurred initializing inputs. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Get messageAction and messageObject attributes from the ControlArea element.
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgObject = eControlArea.getAttribute("messageObject").getValue();

        // Verify that the message is a Account.Create-Sync or Account.Update-Sync
        // since both SecurityRiskDetection sync and Account sync messages are routed to the
        // SrdServiceTopic we just ignore the messages we don't handle
        if (!msgObject.equalsIgnoreCase("Account")) {
            return;
        }
        if (!msgAction.equalsIgnoreCase("Create") && !msgAction.equalsIgnoreCase("Update") && !msgAction.equalsIgnoreCase("Delete")) {
            return;
        }

        // Verify that Account element is not null; if it is, reply with an error.
        Element accountData;
        try {
            if (msgAction.equalsIgnoreCase("Create") || msgAction.equalsIgnoreCase("Update")) {
                accountData = inDoc.getRootElement().getChild("DataArea").getChild("NewData").getChild("Account");
            }
            else /* msgAction == "Delete" */ {
                accountData = inDoc.getRootElement().getChild("DataArea").getChild("DeleteData").getChild("Account");
            }
        }
        catch (Exception e) {
            accountData = null;
        }
        if (accountData == null) {
            String errType = "application";
            String errCode = "AccountSyncCommand-1001";
            String errDesc = "Invalid NewData/DeleteData element found in the sync message. This command expects an Account.";
            logger.error(LOGTAG + errDesc + "\n" + getMessageBody(inDoc));

            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            publishSyncError(eControlArea, errors);

            return;
        }

        Account account;
        try {
            account = (Account) getAppConfig().getObjectByType(Account.class.getName());
            account.buildObjectFromInput(accountData);
        } catch (EnterpriseConfigurationObjectException e) {
            String errType = "application";
            String errCode = "AccountSyncCommand-1002";
            String errDesc = "Error retrieving an Account object from AppConfig"
                    + ". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errDesc);

            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            publishSyncError(eControlArea, errors);

            return;
        } catch (EnterpriseLayoutException e) {
            String errType = "application";
            String errCode = "AccountSyncCommand-1003";
            String errDesc = "An error occurred building the Account object from the element passed in"
                    + ". The exception is: " + e.getMessage();
            logger.error(LOGTAG + errDesc);
            logger.fatal(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));

            List<Error> errors = new ArrayList<>();
            errors.add(buildError(errType, errCode, errDesc));
            publishSyncError(eControlArea, errors);

            return;
        }

        String why = msgObject + "." + msgAction + "-Sync";
        if (msgAction.equalsIgnoreCase("Delete")) {
            AwsAccountServiceUtil.getInstance().accountCacheRemove(account, LOGTAG, why);
        }
        else {
            AwsAccountServiceUtil.getInstance().accountCachePut(account, LOGTAG, why);
        }

        logger.info(LOGTAG + "Finished handling " + why
                + " for message number " + messageNumber
                + " for account " + account.getAccountId()
                + " in elapsed time " + (System.currentTimeMillis() - executeStart) + " ms");
    }
}
