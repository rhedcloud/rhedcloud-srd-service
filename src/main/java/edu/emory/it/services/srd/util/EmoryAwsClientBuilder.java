package edu.emory.it.services.srd.util;

import com.amazonaws.AmazonWebServiceClient;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.metrics.RequestMetricCollector;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.ListAccountAliasesRequest;
import com.amazonaws.services.identitymanagement.model.ListAccountAliasesResult;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import edu.emory.it.services.srd.exceptions.ConnectTimedOutException;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import org.apache.logging.log4j.Logger;
import org.openeai.OpenEaiObject;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Build an AWS Client.
 */
public class EmoryAwsClientBuilder {
    private static final Logger logger = OpenEaiObject.logger;

    /*
     * Setup our own timeouts.
     * ConnectionTimeout sets the amount of time to wait (in milliseconds) when initially establishing a connection
     *  before giving up and timing out.  The default is ClientConfiguration.DEFAULT_CONNECTION_TIMEOUT (10 seconds).
     * RequestTimeout sets the amount of time to wait (in milliseconds) for the request to complete before giving up
     *  and timing out.  Currently, we use the default of no timeout (ClientConfiguration.DEFAULT_REQUEST_TIMEOUT).
     */
    private static final int CONNECTION_TIMEOUT = 30000;
    private static final ClientConfiguration SR_CLIENT_CONFIGURATION = new ClientConfiguration()
            .withConnectionTimeout(CONNECTION_TIMEOUT);

    private String accountId;
    private String role;
    private String sessionName;
    private String region;
    private AWSCredentialsProvider credentialsProvider;
    private String endpoint;
    private RequestMetricCollector requestMetricCollector;

    public EmoryAwsClientBuilder withAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public EmoryAwsClientBuilder withRole(String role) {
        this.role = role;
        return this;
    }

    public EmoryAwsClientBuilder withSessionName(String sessionName) {
        this.sessionName = sessionName;
        return this;
    }

    public EmoryAwsClientBuilder withRegion(String region) {
        this.region = region;
        return this;
    }

    public EmoryAwsClientBuilder withEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }

    public EmoryAwsClientBuilder withCredentials(AWSCredentialsProvider credentialsProvider) {
        this.credentialsProvider = credentialsProvider;
        return this;
    }

    public EmoryAwsClientBuilder withRequestMetricCollector(RequestMetricCollector requestMetricCollector) {
        this.requestMetricCollector = requestMetricCollector;
        return this;
    }

    public <T extends AmazonWebServiceClient> T build(Class<T> clazz)
            throws EmoryAwsClientBuilderException, ConnectTimedOutException {

        long start = System.currentTimeMillis();

        if (accountId == null || sessionName == null || credentialsProvider == null) {
            String errDesc = "Account Id or Session Name or Credentials are missing";
            logger.fatal("[EmoryAwsClientBuilder] " + errDesc);
            throw new EmoryAwsClientBuilderException(errDesc);
        }
        String LOGTAG = "[" + sessionName + "-" + accountId + "] ";

        if (region == null) {
            region = SecurityRiskAwsRegionsAndClientBuilder.DEFAULT_REGION;
        }

        AWSSecurityTokenService sts;
        synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
            sts = AWSSecurityTokenServiceClientBuilder.standard()
                    .withCredentials(credentialsProvider)
                    .withRegion(region)
                    .withMetricsCollector(requestMetricCollector)
                    .withClientConfiguration(SR_CLIENT_CONFIGURATION)
                    .build();
        }

        AssumeRoleResult assumeRoleResult;
        try {
            AssumeRoleRequest assumeRoleRequest = new AssumeRoleRequest()
                    .withRoleArn("arn:aws:iam::" + accountId + ":role/" + role)
                    .withRoleSessionName(sessionName);

            assumeRoleResult = sts.assumeRole(assumeRoleRequest);
        }
        catch (SdkClientException e) {
            if (ConnectTimedOutException.isConnectTimedOutException(e)) {
                throw new ConnectTimedOutException(e);
            }
            String errDesc = "Cannot assume role with account " + accountId + " in region " + region
                    + " with builder " + clazz.getSimpleName()
                    + " with session " + sessionName
                    + ".  Error from Amazon (" + e.getClass().getName() + "): " + e.getMessage();

            // the assume role is expected to fail in some regions (like cn-north-1 or one of the us-gov regions)
            // so we let that be logged by the detector (Skipping region).
            // but, it is generally unexpected to fail in one of the US regions so log it as an ERROR
            if (SecurityRiskAwsRegionsAndClientBuilder.US_REGIONS.contains(region)) {
                logger.error(LOGTAG + errDesc);
            }
            throw new EmoryAwsClientBuilderException(errDesc);
        }

        AWSStaticCredentialsProvider sessionCredentials = new AWSStaticCredentialsProvider(
                new BasicSessionCredentials(
                        assumeRoleResult.getCredentials().getAccessKeyId(),
                        assumeRoleResult.getCredentials().getSecretAccessKey(),
                        assumeRoleResult.getCredentials().getSessionToken()
                ));

        /*
         * The SIEM integration needs access to the AWS account alias.
         * The only way for SRD to get that information is to do it after assuming our role in the account.
         * So, that has been centralized here, and is aggressively cached since it rarely changes, if ever.
         */
        cacheAccountAlias(sessionCredentials, LOGTAG);

        try {
            synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
                AwsClientBuilder builder;
                builder = (AwsClientBuilder) clazz.getMethod("builder").invoke(null);
                builder = (AwsClientBuilder) builder.getClass().getMethod("standard").invoke(null);

                // finish building the client

                if (clazz == AmazonS3Client.class) {
                    ((AmazonS3ClientBuilder) builder).enableForceGlobalBucketAccess();
                }

                builder.setCredentials(sessionCredentials);

                if (endpoint != null) {
                    builder.setEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region));
                } else {
                    builder.setRegion(region);
                }

                builder.setMetricsCollector(requestMetricCollector);
                builder.setClientConfiguration(SR_CLIENT_CONFIGURATION);

                @SuppressWarnings("unchecked")
                T client = (T) builder.build();

                logger.info(LOGTAG + "Retrieved AWS client (" + clazz.getSimpleName() + ") in region " + region
                        + " in elapsed time " + (System.currentTimeMillis() - start) + " (ms)");

                // TODO - AWS recommends caching the temporary session credentials
                // https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_temp_request.html
                // probably keyed by accountId-region-clazz.getSimpleName() and the
                // cached sessions would have to expire several hundred seconds
                //   before AssumeRoleRequest.durationSeconds (3600) so that the entire request has time to finish
                // for example, com.google.common.cache.Cache supports an expiry
                return client;
            }
        }
        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            String errDesc = "Cannot build client with " + accountId + " in region " + region
                    + " with builder " + clazz.getSimpleName()
                    + " with session " + sessionName + ". Class Type " + clazz.getName() + " is not supported";
            logger.error(LOGTAG + errDesc);
            throw new EmoryAwsClientBuilderException(errDesc);
        }
        catch (SdkClientException e) {
            if (ConnectTimedOutException.isConnectTimedOutException(e)) {
                throw new ConnectTimedOutException(e);
            }
            String errDesc = "Cannot build client with " + accountId + " in region " + region
                    + " with builder " + clazz.getSimpleName()
                    + " with session " + sessionName + ".  Error from Amazon: " + e.getMessage();
            logger.error(LOGTAG + errDesc);
            throw new EmoryAwsClientBuilderException(errDesc);
        }
    }

    private void cacheAccountAlias(AWSStaticCredentialsProvider sessionCredentials, String LOGTAG) {
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();

        if (!awsAccountServiceUtil.needCachedAccountAliasRefresh(accountId))
            return;

        try {
            AmazonIdentityManagement iam;
            synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
                iam = AmazonIdentityManagementClientBuilder.standard()
                        .withCredentials(sessionCredentials)
                        .withRegion(region)
                        .withMetricsCollector(requestMetricCollector)
                        .build();
            }

            // even though there can only be one account alias, the API forces us to deal with pagination
            List<String> accountAliases = new ArrayList<>();
            ListAccountAliasesRequest listAccountAliasesRequest = new ListAccountAliasesRequest();
            ListAccountAliasesResult listAccountAliasesResult;
            do {
                listAccountAliasesResult = iam.listAccountAliases(listAccountAliasesRequest);
                accountAliases.addAll(listAccountAliasesResult.getAccountAliases());
                listAccountAliasesRequest.setMarker(listAccountAliasesResult.getMarker());
            } while (listAccountAliasesResult.isTruncated());

            if (accountAliases.size() == 0) {
                // very odd
                logger.error(LOGTAG + "Caching account alias failed because AWS reports no aliases");
            }
            else {
                String accountAlias = accountAliases.get(0);
                awsAccountServiceUtil.putCachedAccountAlias(accountId, accountAlias);
                if (accountAliases.size() > 1) {
                    // very odd
                    logger.warn(LOGTAG + "Caching account alias " + accountAlias
                            + ". AWS returned " + accountAliases.size() + " aliases: " + String.join(", ", accountAliases));
                }
                else {
                    // very normal
                    logger.info(LOGTAG + "Caching account alias " + accountAlias);
                }
            }
        }
        catch (SdkClientException e) {
            String errDesc = "Caching account alias failed while listing AWS aliases in account " + accountId + " in region " + region
                    + " with session " + sessionName + ".  Error from Amazon: " + e.getMessage();
            logger.error(LOGTAG + errDesc);
            // getting account alias is best effort so don't propagate the exception
        }
    }
}
