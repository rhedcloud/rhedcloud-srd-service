package edu.emory.it.services.srd.exceptions;

public class ConnectTimedOutException extends Exception {
    public ConnectTimedOutException(String message) {
        super(message);
    }

    public ConnectTimedOutException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectTimedOutException(Throwable cause) {
        super(cause);
    }

    public static boolean isConnectTimedOutException(Exception exception) {
        if (exception == null)
            return false;
        if (exception instanceof ConnectTimedOutException)
            return true;
        if (exception.getMessage() == null)
            return false;
        return exception.getMessage().contains("connect timed out");
    }
}
