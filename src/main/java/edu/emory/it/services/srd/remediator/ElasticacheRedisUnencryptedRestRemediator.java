package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Delete the cluster.
 *
 * @see edu.emory.it.services.srd.detector.ElasticacheRedisUnencryptedRestDetector the detector
 */
public class ElasticacheRedisUnencryptedRestRemediator extends ElastiCacheBaseRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        super.remediate(accountId, detected, srContext, DetectionType.ElasticacheRedisUnencryptedRest, RemediationMode.DELETE);
    }
}
