package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.List;

/**
 * Detect S3 buckets that reside outside of the US regions (us-east-1, us-east-2, us-west-1, and us-west-2).
 *
 * @see edu.emory.it.services.srd.remediator.S3BucketsResideOutsideOfUSRegionsRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class S3BucketsResideOutsideOfUSRegionsDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        final AmazonS3 s3;
        try {
            s3 = getClient(accountId, srContext.getMetricCollector(), AmazonS3Client.class);
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
            return;
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3BucketsResideOutsideOfUSRegions,
                    srContext.LOGTAG, "On region: global", e);
            return;
        }

        List<Bucket> buckets;
        try {
            buckets = s3.listBuckets();
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.S3BucketsResideOutsideOfUSRegions,
                    srContext.LOGTAG, "Error listing buckets", e);
            return;
        }
        for (Bucket bucket : buckets) {
        	
        	final String bucketArn = "arn:aws:s3:::" + bucket.getName();
			Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
					.isExemptResourceArnForAccountAndDetector(bucketArn,accountId,getBaseName());
			boolean exceptionStatusUnderminable = resourceIsExempt == null;
			if (exceptionStatusUnderminable) {
				logger.error(srContext.LOGTAG + "Exemption status of bucket " + bucketArn + " can not be determined on account " + accountId);
				continue;					
			}

            if (resourceIsExempt) {
                logger.info(srContext.LOGTAG + "Ignoring exempted bucket \"" + bucketArn + "\" on account " + accountId);
                continue;
            }
        	
        	
        	
            // there is a note in the help page for 'GET Bucket location' and while
            // I haven't been able to see this behaviour, handle it anyway
            //   When the bucket's region is US East (N. Virginia), Amazon S3 returns an empty string for the bucket's region
            String location;
            try {
                location = s3.getBucketLocation(bucket.getName());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.S3BucketsResideOutsideOfUSRegions,
                        srContext.LOGTAG, "Error getting bucket location for bucket " + bucket.getName(), e);
                return;
            }
            if (location == null || location.equals(""))
                location = "US";

            if (!US_REGIONS.contains(location)) {
                addDetectedSecurityRisk(detection, srContext.LOGTAG,
                        DetectionType.S3BucketsResideOutsideOfUSRegions, "arn:aws:s3:::" + bucket.getName());
            }
        }
    }

    @Override
    public String getBaseName() {
        return "S3BucketsResideOutsideOfUSRegions";
    }
}
