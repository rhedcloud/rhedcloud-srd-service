package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClient;
import com.amazonaws.services.elasticsearch.model.DeleteElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainResult;
import com.amazonaws.services.elasticsearch.model.ElasticsearchDomainStatus;
import com.amazonaws.services.elasticsearch.model.ResourceNotFoundException;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;

/**
 * Remediates Elasticsearch domains that are in disallowed subnet<br>
 *
 * <p>The remediator deletes the Elasticsearch domain that is launched into a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.ElasticSearchInMgmtSubnetDetector the detector
 */
public class ElasticSearchInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:es:", srContext.LOGTAG, DetectionType.ElasticSearchInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String domainName = arn[5].replace("domain/", "");
        String region = arn[3];

        final AWSElasticsearch elasticsearch;
        final AmazonEC2 ec2;
        try {
            elasticsearch = getClient(accountId, region, srContext.getMetricCollector(), AWSElasticsearchClient.class);
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Domain '" + domainName + "'", e);
            return;
        }

        DescribeElasticsearchDomainRequest describeElasticsearchDomainRequest= new DescribeElasticsearchDomainRequest().withDomainName(domainName);
        DescribeElasticsearchDomainResult describeElasticsearchDomainResult = null;
        try {
            describeElasticsearchDomainResult = elasticsearch.describeElasticsearchDomain(describeElasticsearchDomainRequest);
        } catch (ResourceNotFoundException e) {
            //ignore
        }

        if (describeElasticsearchDomainResult == null || describeElasticsearchDomainResult.getDomainStatus() == null) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Domain '" + domainName + "' cannot be found. It may have already been deleted.");
            return;
        }

        ElasticsearchDomainStatus domainInfo = describeElasticsearchDomainResult.getDomainStatus();

        if (Boolean.TRUE.equals(domainInfo.getDeleted())) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Domain '" + domainName + "' is already deleted. No need to remediate.");
            return;
        }

        if (domainInfo.getVPCOptions() == null || domainInfo.getVPCOptions().getSubnetIds() == null) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Domain '" + domainName + "' is not associated with a subnet. No need to remediate.");
            return;
        }

        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

        boolean found = false;
        for (String subnet : domainInfo.getVPCOptions().getSubnetIds()) {
            if (disallowedSubnets.contains(subnet)) {
                found = true;
                break;
            }
        }

        if (!found) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Domain '" + domainName + "' is not in a disallowed subnet. No need to remediate.");
            return;
        }

        //remediate
        DeleteElasticsearchDomainRequest deleteElasticsearchDomainRequest = new DeleteElasticsearchDomainRequest().withDomainName(domainName);
        elasticsearch.deleteElasticsearchDomain(deleteElasticsearchDomainRequest);

        setSuccess(detected, srContext.LOGTAG, "Domain '" + domainName + "' has been deleted.");
    }
}
