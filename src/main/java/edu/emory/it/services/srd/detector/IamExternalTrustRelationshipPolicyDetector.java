package edu.emory.it.services.srd.detector;

import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.ADMINISTRATOR_ROLE_NAME;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.AWS_RESERVED_IAM_PATH;
import static edu.emory.it.services.srd.util.IamRestrictionPoliciesDetachedUtil.RHEDCLOUD_IAM_PATH;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.ListRolesRequest;
import com.amazonaws.services.identitymanagement.model.ListRolesResult;
import com.amazonaws.services.identitymanagement.model.Role;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityRequest;
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult;

import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Look at all roles in the account and check that the Policy document for anything with Effect = "Allow"
 * and Principal that isn't the owner account or the account series master account.<br>
 *
 * <p>The logic to check if a principal is external goes as follows, if any of the follow is true, the principal is external</p>
 * <ul>
 *     <li>principal is federated</li>
 *     <li>not principal is used</li>
 *     <li>principal = *</li>
 *     <li>principal arn starts with arn:aws:iam: and the account id is an external account</li>
 *     <li>principal arn starts with arn:aws:sts: and the account id is an external account</li>
 * </ul>
 *
 * @see edu.emory.it.services.srd.remediator.IamExternalTrustRelationshipPolicyRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class IamExternalTrustRelationshipPolicyDetector extends AbstractSecurityRiskDetector {

	@Override
	public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
		AmazonIdentityManagement iam;
		try {
			iam = getClient(accountId, srContext.getMetricCollector(), AmazonIdentityManagementClient.class);
		}
		catch (EmoryAwsClientBuilderException e) {
			// The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
			logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
			return;
		}
		catch (Exception e) {
			setDetectionError(detection, DetectionType.IamExternalTrustRelationshipPolicy,
					srContext.LOGTAG, "On region: global", e);
			return;
		}

		GetCallerIdentityResult identityResult;
		try {
			AWSSecurityTokenService stsClient = getSecurityTokenServiceClient(srContext);
			identityResult = stsClient.getCallerIdentity(new GetCallerIdentityRequest());
		}
		catch (Exception e) {
			setDetectionError(detection, DetectionType.IamExternalTrustRelationshipPolicy,
					srContext.LOGTAG, "Error getting caller identity", e);
			return;
		}

		Set<String> allowlistedAccounts = new HashSet<>();
		allowlistedAccounts.add(accountId);
		allowlistedAccounts.add(identityResult.getAccount());

		boolean done = false;
		ListRolesRequest request = new ListRolesRequest();

		while (!done) {
			ListRolesResult response = iam.listRoles(request);

			for (Role role : response.getRoles()) {
						
				Boolean resourceIsExempt = AwsAccountServiceUtil.getInstance()
						.isExemptResourceArnForAccountAndDetector(role.getArn(),accountId,getBaseName());
				boolean exceptionStatusUnderminable = resourceIsExempt == null;
				if (exceptionStatusUnderminable) {
					logger.error(srContext.LOGTAG + "Exemption status of role " + role.getRoleName() + " can not be determined on account " + accountId);
					continue;					
				}
				if (resourceIsExempt) {
					logger.info(srContext.LOGTAG + "Ignoring exempted role " + role.getRoleName() + " on account " + accountId);
					continue;
				}
                if (AWS_RESERVED_IAM_PATH.equals(role.getPath())) {
					logger.info(srContext.LOGTAG + "Ignoring AWS reserved role " + role.getRoleName() + " on account " + accountId);
                    continue;
                }

				try {
					// policy documents have to be decoded
					// see https://docs.aws.amazon.com/IAM/latest/APIReference/API_PolicyVersion.html
					String policy = URLDecoder.decode(role.getAssumeRolePolicyDocument(), StandardCharsets.UTF_8.name());
					if (AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(allowlistedAccounts, policy)) {
						addDetectedSecurityRisk(detection, srContext.LOGTAG,
								DetectionType.IamExternalTrustRelationshipPolicy, role.getArn());
					}
				} catch (UnsupportedEncodingException e) {
					setDetectionError(detection, DetectionType.IamExternalTrustRelationshipPolicy,
							srContext.LOGTAG, "Error decoding role policy document", e);
					return;
				}
			}

			request.setMarker(response.getMarker());

			if (!response.getIsTruncated()) {
				done = true;
			}
		}
	}


	@Override
	public String getBaseName() {
		return "IamExternalTrustRelationshipPolicy";
	}
}
