package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.Account;
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazon.aws.moa.objects.resources.v1_0.Datetime;
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult;
import com.amazon.aws.moa.objects.resources.v1_0.Property;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.GlobalSecondaryIndex;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.Projection;
import com.amazonaws.services.dynamodbv2.model.ProjectionType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughputExceededException;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.amazonaws.services.organizations.AWSOrganizations;
import com.amazonaws.services.organizations.AWSOrganizationsClientBuilder;
import com.openii.openeai.commands.OpeniiRequestCommand;
import edu.emory.it.services.srd.detector.AbstractSecurityRiskDetector;
import edu.emory.it.services.srd.exceptions.ConnectTimedOutException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.remediator.AbstractSecurityRiskRemediator;
import edu.emory.it.services.srd.remediator.SecurityRiskRemediator;
import edu.emory.it.services.srd.util.AWSOrgUtil;
import edu.emory.it.services.srd.util.AwsAccountServiceUtil;
import edu.emory.it.services.srd.util.ResourceTaggingProfileServiceUtil;
import edu.emory.it.services.srd.util.SecurityRiskAwsRegionsAndClientBuilder;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SpringIntegration;
import edu.emory.it.services.srd.util.SrdMetricTypeField;
import edu.emory.it.services.srd.util.SrdMetricUtil;
import edu.emory.it.services.srd.util.SrdRuntimeStorageUtil;
import edu.emory.it.services.srd.util.SrdSyncStorageUtil;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.dbpool.EnterpriseConnectionPool;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.jms.producer.ProducerPool;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.EnterpriseObjectSyncException;
import org.openeai.moa.objects.resources.Error;
import org.openeai.threadpool.ThreadPool;
import org.openeai.threadpool.ThreadPoolException;
import org.openeai.utils.lock.Key;
import org.openeai.utils.lock.Lock;
import org.openeai.utils.lock.LockAlreadySetException;
import org.openeai.utils.lock.LockException;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Handles SecurityRiskDetection.Generate-Request messages.
 *
 * Allow SecurityRiskDetectionGenerateCommand to run with an embedded broker.
 *
 * Prior to this, we have found that when SRD runs in clusters of three or more
 * instances, the requests from the scheduled command don't get routed to all
 * instances so the cluster performance is degraded.
 *
 * Going forward, there will be two consumers with this command configured.
 * One receives requests from the normal broker connection but the other
 * only receives requests from the embedded broker.  The embedded broker is
 * used to communicate between the scheduled command and this command.
 *
 * To avoid duplicating the large set of configuration properties for this
 * command, they were moved to the "main" app config so they can be shared.
 * But the main app config can only be accessed once the application has fully
 * initialized.  To support that, the final round of configuration is done
 * at the beginning of the execute method.  Synchronization is required but
 * has been optimized using the double-fence check.
 */
public class SecurityRiskDetectionGenerateCommand extends OpeniiRequestCommand implements RequestCommand {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionGenerateCommand.class);

    // no way to get this from any of the OpenEAI objects so get it directly
    private static final String OPEN_EAI_INSTANCE_ID = System.getProperty("org.openeai.instanceName");

    private final Document m_responseDoc;  // the primed XML response document
    private final ProducerPool m_pubSubProducerPool;  // for publishing syncs

    private final ThreadPool srdGenerationThreadPool;  // for running detector executions concurrently
    private final Lock srdDetectorLock;

    private final AtomicLong sequenceGenerationWaitingCount = new AtomicLong();
    private final AtomicLong publishSyncWaitingCount = new AtomicLong();
    private final AtomicLong detectorLockSetWaitingCount = new AtomicLong();
    private final AtomicLong detectorLockReleaseWaitingCount = new AtomicLong();

    /*
     * some detectors take longer than the scheduled run time which would cause jobs to pile up
     * in the thread pool.  instead, keep track of what jobs are running and don't start a new one
     * if the job is still running from the last schedule time.
     */
    private final ConcurrentSkipListMap<String, Long> batchedJobsInProgress = new ConcurrentSkipListMap<>();

    /**
     * keep track of the final service initialization step.
     * variables initialized here must be static since the initialization step is performed only once for all
     * instances of this command handler.
     */
    private static boolean finishServiceInitializationStarted = false;
    private static boolean finishServiceInitializationNeeded = true;

    private static String securityRiskDetectionIdSequenceName;
    private static MessageDigest securityRiskDetectionIdSequenceDigest;
    private static String srdDetectorLockNameInfix;

    /** signal running detector jobs to not reschedule */
    private static final AtomicBoolean shuttingDown = new AtomicBoolean();
    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> shuttingDown.set(true)));
    }

    private static final Map<String, AbstractSecurityRiskDetector> securityRiskDetectorMap = new HashMap<>();
    private static final Map<String, SecurityRiskRemediator> securityRiskRemediatorMap = new HashMap<>();

    static {
        // IAM
          securityRiskDetectorMap.put("IamUnusedCredentialDetector", null);
        securityRiskRemediatorMap.put("IamUnusedCredentialRemediator", null);
          securityRiskDetectorMap.put("IamExternalTrustRelationshipPolicyDetector", null);
        securityRiskRemediatorMap.put("IamExternalTrustRelationshipPolicyRemediator", null);
          securityRiskDetectorMap.put("IamRestrictionPoliciesDetachedDetector", null);
        securityRiskRemediatorMap.put("IamRestrictionPoliciesDetachedRemediator", null);
          securityRiskDetectorMap.put("RootUserPasswordChangeDetector", null);
        securityRiskRemediatorMap.put("RootUserPasswordChangeRemediator", null);

        // Organization
          securityRiskDetectorMap.put("AccountInWrongOrganizationalUnitStandardDetector", null);
        securityRiskRemediatorMap.put("AccountInWrongOrganizationalUnitStandardRemediator", null);
          securityRiskDetectorMap.put("AccountInWrongOrganizationalUnitHipaaDetector", null);
        securityRiskRemediatorMap.put("AccountInWrongOrganizationalUnitHipaaRemediator", null);
          securityRiskDetectorMap.put("AccountInWrongOrganizationalUnitEnhancedSecurityDetector", null);
        securityRiskRemediatorMap.put("AccountInWrongOrganizationalUnitEnhancedSecurityRemediator", null);

        // S3
          securityRiskDetectorMap.put("S3UnencryptedBucketDetector", null);
        securityRiskRemediatorMap.put("S3UnencryptedBucketRemediator", null);
          securityRiskDetectorMap.put("S3PublicBucketDetector", null);
        securityRiskRemediatorMap.put("S3PublicBucketRemediator", null);
          securityRiskDetectorMap.put("S3BucketsResideOutsideOfUSRegionsDetector", null);
        securityRiskRemediatorMap.put("S3BucketsResideOutsideOfUSRegionsRemediator", null);

        // RDS
          securityRiskDetectorMap.put("RdsUnencryptedDatabaseDetector", null);
        securityRiskRemediatorMap.put("RdsUnencryptedDatabaseRemediator", null);
          securityRiskDetectorMap.put("RdsPostgreSQLUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("RdsPostgreSQLUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("RdsOracleUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("RdsOracleUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("RdsSqlServerUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("RdsSqlServerUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("RdsDBClusterUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("RdsDBClusterUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("RdsHipaaIneligibleSqlServerDatabaseDetector", null);
        securityRiskRemediatorMap.put("RdsHipaaIneligibleSqlServerDatabaseRemediator", null);
          securityRiskDetectorMap.put("RdsLaunchedWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("RdsLaunchedWithinManagementSubnetsRemediator", null);
        // RdsMysqlUnencryptedTransportDetector
        //   - In order to detect, we need Administrator level access to the RDS-MySQL tables.
        //     This is not feasible, so we will skip this detector.
        // RdsMariaDbUnencryptedTransportDetector
        //   - In order to detect, we need Administrator level access to the RDS-MariaDB tables.
        //     This is not feasible, so we will skip this detector.
        // RdsSqlServerAuditingDisabledDetector
        //   - Discussed with Zach. This is redundant. Please skip this SRD
        // AuroraUnencryptedDBDetector
        //   - We can skip this detector.

        // DynamoDB
          securityRiskDetectorMap.put("DynamoDbUnencryptedDatabaseDetector", null);
        securityRiskRemediatorMap.put("DynamoDbUnencryptedDatabaseRemediator", null);
          securityRiskDetectorMap.put("DAXClusterInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("DAXClusterInMgmtSubnetRemediator", null);
        // DynamoDbUnencryptedTransportDetector
        //   - AWS advised that data in transit is always encrypted for DynamoDB,
        //     unless an application intentionally instructs the DynamoDB client to use an HTTP service endpoint
        //     instead of the default HTTPS service endpoint in the code.
        //     As our endpoints will be setup for https service, we can skip this detector.

        // ElastiCache
          securityRiskDetectorMap.put("RdsHipaaIneligibleMemcachedDetector", null);
        securityRiskRemediatorMap.put("RdsHipaaIneligibleMemcachedRemediator", null);
          securityRiskDetectorMap.put("ElasticacheRedisUnencryptedRestDetector", null);
        securityRiskRemediatorMap.put("ElasticacheRedisUnencryptedRestRemediator", null);
          securityRiskDetectorMap.put("ElasticacheRedisUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("ElasticacheRedisUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("ElasticacheRedisDisabledRedisCommandsDetector", null);
        securityRiskRemediatorMap.put("ElasticacheRedisDisabledRedisCommandsRemediator", null);

        // Redshift
          securityRiskDetectorMap.put("RedshiftUnencryptedRestDetector", null);
        securityRiskRemediatorMap.put("RedshiftUnencryptedRestRemediator", null);
          securityRiskDetectorMap.put("RedshiftSSLDisabledDetector", null);
        securityRiskRemediatorMap.put("RedshiftSSLDisabledRemediator", null);

        // Elastic Beanstalk
        // TODO - unimplemented requirement is to stop or delete the environment if the update failed
          securityRiskDetectorMap.put("ElasticBeanstalkDisabledManagedUpdatesDetector", null);
        securityRiskRemediatorMap.put("ElasticBeanstalkDisabledManagedUpdatesRemediator", null);
          securityRiskDetectorMap.put("ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator", null);
          securityRiskDetectorMap.put("EbsUnencryptedSecondaryVolumeDetector", null);
        securityRiskRemediatorMap.put("EbsUnencryptedSecondaryVolumeRemediator", null);

        // API Gateway
          securityRiskDetectorMap.put("APIGatewayCachingDetector", null);
        securityRiskRemediatorMap.put("APIGatewayCachingRemediator", null);

        // Auto Scaling Group
          securityRiskDetectorMap.put("AutoscalingGroupInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("AutoscalingGroupInMgmtSubnetRemediator", null);

        // Cloud Front
          securityRiskDetectorMap.put("CloudFrontNonHTTPSOriginEnabledDetector", null);
        securityRiskRemediatorMap.put("CloudFrontNonHTTPSOriginEnabledRemediator", null);
          securityRiskDetectorMap.put("CloudFrontStaticSiteACLDetector", null);
        securityRiskRemediatorMap.put("CloudFrontStaticSiteACLRemediator", null);
          securityRiskDetectorMap.put("CloudFrontUnsignedURLUnfoundCookiesDetector", null);
        securityRiskRemediatorMap.put("CloudFrontUnsignedURLUnfoundCookiesRemediator", null);

        // EC2
          securityRiskDetectorMap.put("ELBLoggingDisabledDetector", null);
        securityRiskRemediatorMap.put("ELBLoggingDisabledRemediator", null);
          securityRiskDetectorMap.put("VPCFlowLogsDisabledDetector", null);
        securityRiskRemediatorMap.put("VPCFlowLogsDisabledRemediator", null);
          securityRiskDetectorMap.put("EC2CustomerInstancesInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("EC2CustomerInstancesInMgmtSubnetRemediator", null);
          securityRiskDetectorMap.put("EC2FleetInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("EC2FleetInMgmtSubnetRemediator", null);
          securityRiskDetectorMap.put("EC2SpotScheduledInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("EC2SpotScheduledInMgmtSubnetRemediator", null);
          securityRiskDetectorMap.put("UnregisteredVpcDetector", null);
        securityRiskRemediatorMap.put("UnregisteredVpcRemediator", null);
        // EC2ScheduledInMgmtSubnetDetector
        //   - TODO - notes from Henry
        //     Parse results from describe-scheduled-instances and look for subnetId that is in a disallowed subnet
        //     Launch configurations are only inputted via run-scheduled-instances api command,
        //     and not at the creation of the schedule.  There is no way to detect MGMT or Attachment subnets in a schedule.
        //     We can rely on EC2CustomerInstancesInMgmtSubnetDetector once the instance is running.
        // EC2OpenSecurityGroupDetector
        //   - TODO

        // Elastic Container Service
          securityRiskDetectorMap.put("ECSLaunchedWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("ECSLaunchedWithinManagementSubnetsRemediator", null);

        // Elastic Map Reduce
        // TODO - unimplemented requirement is to delete the cluster if the terminate failed
          securityRiskDetectorMap.put("EMRClusterUnencryptedRestDetector", null);
        securityRiskRemediatorMap.put("EMRClusterUnencryptedRestRemediator", null);
          securityRiskDetectorMap.put("EMRClusterUnencryptedTransportDetector", null);
        securityRiskRemediatorMap.put("EMRClusterUnencryptedTransportRemediator", null);
          securityRiskDetectorMap.put("EMRLaunchedWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("EMRLaunchedWithinManagementSubnetsRemediator", null);

        // CloudTrail
          securityRiskDetectorMap.put("CloudTrailDisabledDetector", null);
        //securityRiskRemediatorMap.put("CloudTrailDisabledRemediator", null);  // commented out so that we can use less configs

        // Simple System Management
          securityRiskDetectorMap.put("SSMUnencryptedParametersDetector", null);
        securityRiskRemediatorMap.put("SSMUnencryptedParametersRemediator", null);

        // Directory Service
        // TODO - This detector is currently not running due to a security policy that does not allow directories to be listed
          securityRiskDetectorMap.put("DSIneligibleDirectoryDetector", null);
        // no remediator

        // WorkDocs
        // TODO - This detector is currently not running because the organization Id infrastructure is not set
          securityRiskDetectorMap.put("WorkDocsNonEmoryDomainDetector", null);
        securityRiskRemediatorMap.put("WorkDocsNonEmoryDomainRemediator", null);

        // Lambda Functions
          securityRiskDetectorMap.put("LambdaResidesWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("LambdaResidesWithinManagementSubnetsRemediator", null);

        // Work Spaces
        // TODO - This detector is currently not running due to a security setting where WorkSpaces cannot be listed
          securityRiskDetectorMap.put("WorkSpacesUnencryptedSpaceDetector", null);
        securityRiskRemediatorMap.put("WorkSpacesUnencryptedSpaceRemediator", null);

        // SQS
          securityRiskDetectorMap.put("SQSExternallySharedQueueDetector", null);
        securityRiskRemediatorMap.put("SQSExternallySharedQueueRemediator", null);

        // KMS
          securityRiskDetectorMap.put("KMSExternallySharedKeyDetector", null);
        securityRiskRemediatorMap.put("KMSExternallySharedKeyRemediator", null);

        // GuardDuty
          securityRiskDetectorMap.put("GuardDutyMisconfigurationDetector", null);
        securityRiskRemediatorMap.put("GuardDutyMisconfigurationRemediator", null);

        // Cognito
        // TODO - This detector is currently not running due to a security policy that does not allow user pools to be listed
          securityRiskDetectorMap.put("CognitoUncomplexedPasswordsDetector", null);
        securityRiskRemediatorMap.put("CognitoUncomplexedPasswordsRemediator", null);

        // Elemental MediaStore
          securityRiskDetectorMap.put("MediaStorePublicContainerStandardDetector", null);
        securityRiskRemediatorMap.put("MediaStorePublicContainerStandardRemediator", null);
          securityRiskDetectorMap.put("MediaStorePublicContainerHipaaDetector", null);
        securityRiskRemediatorMap.put("MediaStorePublicContainerHipaaRemediator", null);

        // Elasticsearch
          securityRiskDetectorMap.put("ElasticSearchInMgmtSubnetDetector", null);
        securityRiskRemediatorMap.put("ElasticSearchInMgmtSubnetRemediator", null);

        // Cloudwatch
          securityRiskDetectorMap.put("CWEventsCustomerAlarmsOnEmoryInstancesDetector", null);
        securityRiskRemediatorMap.put("CWEventsCustomerAlarmsOnEmoryInstancesRemediator", null);

        // SageMaker
          securityRiskDetectorMap.put("SageMakerVPCMisconfigurationDetector", null);
        securityRiskRemediatorMap.put("SageMakerVPCMisconfigurationRemediator", null);
          securityRiskDetectorMap.put("SageMakerWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("SageMakerWithinManagementSubnetsRemediator", null);

        // CodeBuild
          securityRiskDetectorMap.put("CodeBuildVPCMisconfigurationDetector", null);
        securityRiskRemediatorMap.put("CodeBuildVPCMisconfigurationRemediator", null);
          securityRiskDetectorMap.put("CodeBuildWithinManagementSubnetsDetector", null);
        securityRiskRemediatorMap.put("CodeBuildWithinManagementSubnetsRemediator", null);

        // Resource Tagging
          securityRiskDetectorMap.put("TagPolicyViolationDetector", null);
        securityRiskRemediatorMap.put("TagPolicyViolationRemediator", null);
        securityRiskRemediatorMap.put("TagPolicyViolationSimulationRemediator", null);

        // WAF
        // WAFUnencryptedTransitDetector
        //   - This will require encryption for data in transit that goes through the WAF.
        //     Encryption will be configured on the AWS services that handle the data and not configured on the WAF.
        //     Can remove this entry.
        // CloudSearchWAFACLDetector
        //   - I am not able to find any use of WAF or ACL within the CloudSearch service.

        // MQ
        // MQBrokerInMgmtSubnetDetector
        //   - TODO

        // these two aren't really detectors.
        // Example is used by test suite, and
        // ServiceMonitoring is used by Integration team for monitoring.
          securityRiskDetectorMap.put("ExampleDetector", null);
        securityRiskRemediatorMap.put("ExampleRemediator", null);
          securityRiskDetectorMap.put("ServiceMonitoringDetector", null);
        // no remediator for ServiceMonitoring
    }


    public SecurityRiskDetectionGenerateCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        String LOGTAG = "[SrdGenerateCommand] ";
        logger.info(LOGTAG + "Initializing at release " + ReleaseTag.getReleaseInfo());

        Properties generalProperties;
        try {
            generalProperties = getAppConfig().getProperties("GeneralProperties");
            setProperties(generalProperties);
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing response document " + generalProperties.getProperty("responseDocumentUri"));
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            m_responseDoc = xmlReader.initializeDocument(generalProperties.getProperty("responseDocumentUri"), getOutboundXmlValidation());
            if (m_responseDoc == null) {
                String errMsg = "Missing 'responseDocumentUri' property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
        }
        catch (XmlDocumentReaderException e) {
            String errMsg = "Error initializing the primed documents. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(e.getMessage());
        }

        // Initialize a pub/sub producer pool.
        try {
            logger.info(LOGTAG + "Initializing getting SyncCommandPublisher");
            m_pubSubProducerPool = (ProducerPool) getAppConfig().getObject("SyncCommandPublisher");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving a 'SyncCommandPublisher' object from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(LOGTAG + "Initializing detector lock");
            srdDetectorLock = (Lock) getAppConfig().getObject("SrdDetectorLock");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving a 'SrdDetectorLock' object from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        try {
            logger.info(LOGTAG + "Initializing generation thread pool");
            srdGenerationThreadPool = (ThreadPool) getAppConfig().getObject("AwsSrdGenerationThreadPool");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving the thread pool from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        String LOGTAG = "[SrdGenerateCommand][" + messageNumber + "] ";

        /*
         * execute can't proceed until the final service initialization is finished.
         * we keep track of if it's started and if it's finished.
         * the checks _here_ are not completely accurate
         *  - we must be in the synchronized method to be sure.
         */
        if (finishServiceInitializationNeeded) {
            finishServiceInitialization(LOGTAG);
        }
        if (finishServiceInitializationNeeded) {
            String errMsg = "Final service initialization still in progress.";
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }


        // initialize inputs and prepare for execution
        Document inDoc;
        TextMessage msg = (TextMessage) aMessage;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
            msg.clearBody(); // clear so new content can be added later
        }
        catch (Exception e) {
            String errMsg = "Exception occurred initializing inputs. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Verify that the request can be handled by this command
        String msgObject = eControlArea.getAttribute("messageObject").getValue();
        String msgAction = eControlArea.getAttribute("messageAction").getValue();

        if (!msgObject.equalsIgnoreCase("SecurityRiskDetection")) {
            String errDesc = "Unsupported message object: " + msgObject + ". This command expects 'SecurityRiskDetection'.";
            return replyWithApplicationError("OpenEAI-1001", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
        if (!msgAction.equalsIgnoreCase("Generate")) {
            String errDesc = "Unsupported message action: " + msgAction + ". This command only supports 'Generate'.";
            return replyWithApplicationError("OpenEAI-1003", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }

        // Get a configured SecurityRiskDetectionRequisition from AppConfig
        // and build the requisition object from the incoming message
        SecurityRiskDetectionRequisition detectionRequisition;
        try {
            detectionRequisition = (SecurityRiskDetectionRequisition) getAppConfig().getObjectByType(SecurityRiskDetectionRequisition.class.getName());

            Element eRequisition = inDoc.getRootElement().getChild("DataArea").getChild("SecurityRiskDetectionRequisition");
            if (eRequisition == null) {
                String errDesc = "Invalid Generate-Request message. This command expects a SecurityRiskDetectionRequisition.";
                return replyWithApplicationError("OpenEAI-1004", errDesc, eControlArea, inDoc, msg, LOGTAG);
            }

            detectionRequisition.buildObjectFromInput(eRequisition);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errDesc = "Error retrieving a 'SecurityRiskDetectionRequisition' object from AppConfig. The exception is: " + e.getMessage();
            return replyWithApplicationError("OpenEAI-1002", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
        catch (EnterpriseLayoutException e) {
            String errDesc = "Error building an object from the SecurityRiskDetectionRequisition element in the Generate-Request message. The exception is: " + e.getMessage();
            return replyWithApplicationError("OpenEAI-1005", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }

        // Handle the SecurityRiskDetection.Generate-Request

        // a special message is used to process all detectors as a batch
        if (detectionRequisition.getAccountId().equals("...")) {
            return processBatchRequest(msg, eControlArea, inDoc, detectionRequisition);
        }

        // process as a regular generate request
        return processSingleRequest(msg, eControlArea, inDoc, detectionRequisition);
    }

    /**
     * See class javadoc concerning running with an embedded broker.
     *
     * @param LOGTAG log tag
     * @throws CommandException on error
     */
    private synchronized void finishServiceInitialization(String LOGTAG) throws CommandException {
        // must check if needed now that we're in the synchronized method
        if (!finishServiceInitializationNeeded) {
            logger.info(LOGTAG + "Final service initialization not needed");
            return;
        }
        // a previous call could still be in progress (i.e., started and still needed)
        if (finishServiceInitializationStarted) {
            logger.info(LOGTAG + "Final service initialization already started");
            return;
        }
        finishServiceInitializationStarted = true;

        logger.info(LOGTAG + "Final service initialization starting");

        AppConfig mainAppConfig = getAppConfig().getMainAppConfig();

        logger.info(LOGTAG + "Initializing singletons");
        AWSOrgUtil awsOrgUtil = AWSOrgUtil.getInstance();
        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
        SrdRuntimeStorageUtil srdRuntimeStorageUtil = SrdRuntimeStorageUtil.getInstance();
        SrdSyncStorageUtil srdSyncStorageUtil = SrdSyncStorageUtil.getInstance();
        ResourceTaggingProfileServiceUtil resourceTaggingProfileServiceUtil = ResourceTaggingProfileServiceUtil.getInstance();
        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();
        SpringIntegration springIntegration = SpringIntegration.getInstance();


        logger.info(LOGTAG + "Initializing credentials");
        AWSCredentialsProvider credentialsProvider;
        String roleName;
        try {
            Properties properties = mainAppConfig.getProperties("AwsCredentials");
            credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(
                    properties.getProperty("accessKeyId"), properties.getProperty("secretKey")));
            roleName = properties.getProperty("securityRole");
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error getting the AwsCredentials property. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(e.getMessage());
        }


        logger.info(LOGTAG + "Initializing Organizations utils");
        AWSOrganizations orgClient;
        try {
            synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
                orgClient = AWSOrganizationsClientBuilder.standard()
                        .withCredentials(credentialsProvider)
                        .withRegion(SecurityRiskAwsRegionsAndClientBuilder.DEFAULT_REGION)
                        .build();
            }
            Properties properties = mainAppConfig.getProperties("OrgNames");
            String hipaaOrgName = properties.getProperty("HipaaOrgName");
            String standardOrgName = properties.getProperty("StandardOrgName");
            String enhancedSecurityOrgName = properties.getProperty("EnhancedSecurityOrgName", null);
            String quarantineOrgName = properties.getProperty("QuarantineOrgName");
            String pendingDeleteOrgName = properties.getProperty("PendingDeleteOrgName");
            String administrationOrgName = properties.getProperty("AdministrationOrgName");
            awsOrgUtil.initialize(orgClient, hipaaOrgName, standardOrgName, enhancedSecurityOrgName,
                    quarantineOrgName, pendingDeleteOrgName, administrationOrgName);
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error getting the OrgNames property.");
            throw new CommandException(e.getMessage());
        }
        catch (Exception e) {
            logger.fatal(LOGTAG + "Error building Organizations client.");
            throw new CommandException(e.getMessage());
        }


        try {
            logger.info(LOGTAG + "Initializing sequences");
            // This provider needs a sequence to generate a unique SecurityRiskDetectionId for
            //  each transaction in multiple threads and multiple instances.
            Properties properties = mainAppConfig.getProperties("SequenceInfo");
            securityRiskDetectionIdSequenceName = properties.getProperty("SecurityRiskDetectionIdSequenceName");
            srdDetectorLockNameInfix = properties.getProperty("SrdDetectorLockNameInfix", "_");
            logger.info(LOGTAG + "Initialized sequence with name " + securityRiskDetectionIdSequenceName
                    + (srdDetectorLockNameInfix.equals("_") ? "" : (" and lock name infix " + srdDetectorLockNameInfix)));
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error getting the SequenceInfo property.");
            throw new CommandException(e.getMessage());
        }
        try {
            logger.info(LOGTAG + "Initializing sequence fallback");
            securityRiskDetectionIdSequenceDigest = MessageDigest.getInstance("SHA-256");
            logger.info(LOGTAG + "Initialized sequence fallback " + securityRiskDetectionIdSequenceDigest);
        }
        catch (NoSuchAlgorithmException e) {
            logger.info(LOGTAG + "Error getting the sequence fallback.", e);
            securityRiskDetectionIdSequenceDigest = null;
        }

        try {
            logger.info(LOGTAG + "Initializing spring integration");
            EnterpriseConnectionPool c = (EnterpriseConnectionPool) getAppConfig().getObject("SrdDetectorLockCountPool");
            springIntegration.initialize(c.getDriverName(), c.getConnectString(), c.getConnectUserId(), c.getConnectPassword(),
                    this.getClass().getPackage().getName());
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error getting the SrdDetectorLockCountPool configuration.");
            throw new CommandException(e.getMessage());
        }

        try {
            logger.info(LOGTAG + "Initializing AwsAccountService utils");
            awsAccountServiceUtil.initialize(getAppConfig(), LOGTAG);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving a 'AwsAccountP2pProducer' object from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }
        try {
            // app config property AwsAccountServiceAccountCacheRefresher/rate is completely optional and
            // typically used in development.  if not set, or a bad value, silently set to default in millis
            Object o = mainAppConfig.getObjects().get("AwsAccountServiceAccountCacheRefresher".toLowerCase());
            long refresherRate;
            if (o instanceof PropertyConfig) {
                String rate = ((PropertyConfig) o).getProperties().getProperty("rate");
                try {
                    refresherRate = Long.parseLong(rate);
                }
                catch (NumberFormatException e) {
                    refresherRate = 3 * 1000;
                }
            }
            else {
                refresherRate = 3 * 1000;
            }
            logger.info(LOGTAG + "Initializing account cache refresher with rate " + refresherRate + " millis.");
            srdGenerationThreadPool.addJob(new AwsAccountServiceAccountCacheRefresher(refresherRate));
        }
        catch (ThreadPoolException e) {
            String errMsg = "Non fatal error starting the account cache refresher. The exception is: " + e.getMessage();
            logger.error(LOGTAG + errMsg);
        }


        logger.info(LOGTAG + "Initializing ResourceTaggingProfileService utils");
        resourceTaggingProfileServiceUtil.initialize(getAppConfig(), LOGTAG);


        // Initialize detectors and remediators
        try {
            for (String name : securityRiskDetectorMap.keySet()) {
                securityRiskDetectorMap.put(name, initializeDetector(name, mainAppConfig, credentialsProvider, roleName));
            }
        }
        catch (InstantiationException e) {
            String errMsg = "Error initializing detectors. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(e.getMessage());
        }
        try {
            for (String name : securityRiskRemediatorMap.keySet()) {
                securityRiskRemediatorMap.put(name, initializeRemediator(name, mainAppConfig, credentialsProvider, roleName));
            }
        }
        catch (InstantiationException e) {
            String errMsg = "Error initializing remediators. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(e.getMessage());
        }


        logger.info(LOGTAG + "Initializing runtime and sync storage");
        setupRuntimeStorage(mainAppConfig, srdRuntimeStorageUtil);
        setupSyncStorage(mainAppConfig, srdSyncStorageUtil);


        finishServiceInitializationNeeded = false;

        logger.info(LOGTAG + "Final service initialization complete");
    }

    private String generateNextSecurityRiskDetectionId(String LOGTAG) {
        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();
        String nextSecurityRiskDetectionId;

        /*
         * this method must not fail to return a sequence number
         * because we've already detected and remediated the risk
         * and don't want to loose the report
         */
        String fallbackReason;

        /*
         * for now, use DynamoDB atomic counters directly.
         * eventually, this will simply be another implementation in the OpenEAI sequence foundation classes.
         */
        if (securityRiskDetectionIdSequenceName != null) {
            try {
                nextSecurityRiskDetectionId = SrdRuntimeStorageUtil.getInstance().getNextSecurityRiskDetectionId(securityRiskDetectionIdSequenceName);
                srdMetricUtil.trackSecurityRiskDetectionId(nextSecurityRiskDetectionId);
                return nextSecurityRiskDetectionId;
            }
            catch (ProvisionedThroughputExceededException e) {
                // handle specifically with the future goal of tracking these failures somewhere like Datadog
                fallbackReason = "ProvisionedThroughputExceeded on DynamoDB table "
                        + SrdRuntimeStorageUtil.getInstance().getStorageTable().getTableName()
                        + ". The exception is: " + e.getMessage();
            }
            catch (Exception e) {
                fallbackReason = "Error on DynamoDB table "
                        + SrdRuntimeStorageUtil.getInstance().getStorageTable().getTableName()
                        + ". The exception is: " + e.getMessage();
            }
        }
        else {
            fallbackReason = "the DynamoDB table sequence name is not configured";
        }

        /*
         * use a short identifier (https://gnugat.github.io/2018/06/15/short-identifier.html)
         * as a fallback sequence generator if the DynamoDB version fails or is not configured
         *
         *  6 first characters: covers          16,777,216 hashes, but first collision happens after      4,096 hashes generated
         *  7 first characters: covers         268,435,456 hashes, but first collision happens after     16,384 hashes generated
         *  8 first characters: covers       4,294,967,296 hashes, but first collision happens after     65,536 hashes generated
         * 12 first characters: covers 281,474,976,710,656 hashes, but first collision happens after 16,777,216 hashes generated
         *
         * so choose 12 because people are used to 12 digit AWS account ids and first collision won't happen for a while
         */
        if (securityRiskDetectionIdSequenceDigest != null) {
            logger.info(LOGTAG + "Getting the next sequence value from the short identifier fallback because " + fallbackReason);
            StringBuilder uuidHexString = new StringBuilder();
            String randomUUID = UUID.randomUUID().toString();
            byte[] uuidHash = securityRiskDetectionIdSequenceDigest.digest(randomUUID.getBytes(StandardCharsets.UTF_8));
            for (byte b : uuidHash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    uuidHexString.append('0');
                uuidHexString.append(hex);
            }
            nextSecurityRiskDetectionId = uuidHexString.substring(0, 12);
            srdMetricUtil.trackSecurityRiskDetectionIdFallback(nextSecurityRiskDetectionId);
            return nextSecurityRiskDetectionId;
        }
        else {
            fallbackReason += " and the short identifier fallback is not configured";
        }

        /*
         * We can no longer fallback on OpenEAI org.openeai.utils.sequence.DbSequence.
         * The DB lock taken by this sequence generator can be heavily contended in SRD so we have to retry several times.
         * Worse still, DbSequence.next() is a synchronized method and requires a monitor lock
         *  which has a huge impact on concurrency and detection rate.  All the detection execution threads tend
         *  to "pile up" behind the synchronization causing large delays. In testing, this sequence generator
         *  causes the number of concurrent detectors to drop to less than 20.
         */

        // final fallback to UUID sequence implementation
        logger.info(LOGTAG + "Getting the next sequence value from UUID fallback because " + fallbackReason);
        nextSecurityRiskDetectionId = UUID.randomUUID().toString();
        srdMetricUtil.trackSecurityRiskDetectionIdFallback(nextSecurityRiskDetectionId);
        return nextSecurityRiskDetectionId;
    }

    private Message replyWithApplicationError(String errCode, String errDesc,
                                              Element eControlArea, Document inDoc, TextMessage msg, String LOGTAG)
            throws CommandException {

        Document localResponseDoc = (Document) m_responseDoc.clone();

        logger.fatal(LOGTAG + errDesc);
        if (inDoc != null) {
            logger.fatal(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));
        }
        List<Error> errors = new ArrayList<>();
        errors.add(buildError("application", errCode, errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }

    private AbstractSecurityRiskDetector initializeDetector(String detectorName, AppConfig appConfig, AWSCredentialsProvider credentialsProvider, String securityRole)
            throws InstantiationException {

        String LOGTAG = "[SrdGenerateCommand][" + detectorName + "] ";
        String detectorClassName = "edu.emory.it.services.srd.detector." + detectorName;

        try {
            logger.info(LOGTAG + "Initializing detector");
            Class<?> detectorClass = Class.forName(detectorClassName);
            AbstractSecurityRiskDetector detectorInstance = (AbstractSecurityRiskDetector) detectorClass.newInstance();
            detectorInstance.init(appConfig, credentialsProvider, securityRole);

            return detectorInstance;
        }
        catch (ClassNotFoundException e) {
            String errMsg = "Class named " + detectorClassName + " not found on the classpath. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (IllegalAccessException e) {
            String errMsg = "An error occurred getting a class for name: " + detectorClassName + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (SecurityRiskDetectionException e) {
            String errMsg = "An error occurred initializing the detector. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

    private SecurityRiskRemediator initializeRemediator(String remediatorName, AppConfig appConfig, AWSCredentialsProvider credentialsProvider, String securityRole)
            throws InstantiationException {

        String LOGTAG = "[SrdGenerateCommand][" + remediatorName + "] ";
        String remediatorClassName = "edu.emory.it.services.srd.remediator." + remediatorName;

        try {
            logger.info(LOGTAG + "Initializing remediator");
            Class<?> remediatorClass = Class.forName(remediatorClassName);
            SecurityRiskRemediator remediatorInstance = (SecurityRiskRemediator) remediatorClass.newInstance();
            remediatorInstance.init(appConfig, credentialsProvider, securityRole);

            return remediatorInstance;
        }
        catch (ClassNotFoundException e) {
            String errMsg = "Class named " + remediatorClassName + " not found on the classpath. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (IllegalAccessException e) {
            String errMsg = "An error occurred getting a class for name: " + remediatorClassName + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (SecurityRiskRemediationException e) {
            String errMsg = "An error occurred initializing the remediator. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
    }

    /*------------------------------------------------------------------
     * Detector Execution either as a Batch or Single mode
     */

    /**
     * Process a regular generate request.
     */
    private Message processSingleRequest(TextMessage msg, Element eControlArea, Document inDoc,
                                         SecurityRiskDetectionRequisition detectionRequisition) throws CommandException {

        long start = System.currentTimeMillis();

        String jobKey = detectionRequisition.getSecurityRiskDetector() + "-" + detectionRequisition.getAccountId();
        String jobLOGTAG = "[DetectorExecution][" + jobKey + "] ";

        Long runningSince = batchedJobsInProgress.putIfAbsent(jobKey, start);
        if (runningSince == null) {
            // not running so start a new job
            long nano = Math.abs(System.nanoTime());
            logger.info(jobLOGTAG + "Running job directly for nano " + nano);
            DetectorExecution de = new DetectorExecution(detectionRequisition, jobKey,
                    inDoc, eControlArea, msg, nano, false);
            de.run();
            if (de.reply != null) {
                return de.reply;
            } else {
                throw de.commandException;
            }
        }
        else {
            // still running
            LocalDateTime runningSinceDT = LocalDateTime.ofInstant(Instant.ofEpochMilli(runningSince), ZoneId.systemDefault());
            Duration runningSinceDuration = Duration.between(Instant.ofEpochMilli(runningSince), Instant.ofEpochMilli(start));

            String message = jobLOGTAG + "already running since (" + runningSince + ")(" + runningSinceDT + ")(" + runningSinceDuration + ")";
            logger.info(message);
            throw new CommandException(message);
        }
    }

    /**
     * Process the special 'batch' request.
     */
    private Message processBatchRequest(TextMessage msg, Element eControlArea, Document inDoc,
                                        SecurityRiskDetectionRequisition detectionRequisition) throws CommandException {
        long start = System.currentTimeMillis();

        // gather metrics as we process the batch request
        SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

        String LOGTAG = "[SrdBatchGenerateCommand] ";
        logger.info(LOGTAG + "Starting batch with thread pool stats "
                + srdGenerationThreadPool.getStats().toString().replace("\n", ", "));

        AwsAccountServiceUtil awsAccountServiceUtil = AwsAccountServiceUtil.getInstance();
        logger.info( LOGTAG + "awsAccountServiceUtil="+awsAccountServiceUtil);

        List<Account> allAwsAccounts;
        try {
            allAwsAccounts = awsAccountServiceUtil.getAllAccounts(LOGTAG);
        }
        catch (Exception e) {
            logger.error(LOGTAG + "An error occurred while getting accounts from the AWS Account Producer"
                    + ". The exception is: " + e.getMessage());
            throw new CommandException(e);
        }

        // for the batch request, the SecurityRiskDetector is a comma separated list of detector names
        // and the SecurityRiskRemediator is a boolean indicating whether remediation should be done
        String[] detectors = detectionRequisition.getSecurityRiskDetector().split(",");
        boolean remediateRequested = Boolean.parseBoolean(detectionRequisition.getSecurityRiskRemediator());
        SrdRuntimeStorageUtil srdRuntimeStorageUtil = SrdRuntimeStorageUtil.getInstance();

        // a bunch of stats
        int nbrJobsAddedToPool = 0;
        int nbrJobsStillRunning = 0;
        int nbrJobsFrequencyBackoff = 0;
        int nbrActiveStandardAccounts = 0;
        int nbrActiveHipaaAccounts = 0;
        int nbrActiveEnhancedSecurityAccounts = 0;
        int nbrExemptAccounts = 0;
        int nbrRelevantDetectors = 0;
        int nbrStandardDetectors = 0;
        int nbrHipaaDetectors = 0;
        int nbrEnhancedSecurityDetectors = 0;
        // for log messages
        String skippedExemptAccountsMessage = "";  // list of accounts skipped because of srdExempt

        // do the cross product of all accounts and detectors.
        // if the account is exempt then it is skipped.
        // otherwise, if the compliance class of the account and detector match then the detector is considered
        // relevant for starting.  a final check is done to see if it is already running according to our in-memory
        // list of jobs in progress so that we do not run multiple copies of the same detector/account.
        for (Account account : allAwsAccounts) {
            String accountId = account.getAccountId();
//            if (!"803008756714".equals(accountId)) {
//            	logger.info(LOGTAG + "Skipping acct '"+account.getAccountName()+"' for debugging");
//            	continue;
//            }
            String accountIdAnnotation;
            ComplianceClass accountComplianceClass;
            switch (account.getComplianceClass()) {
                case "Standard":
                    accountIdAnnotation = accountId + "~S";
                    accountComplianceClass = ComplianceClass.Standard;
                    break;
                case "HIPAA":
                    accountIdAnnotation = accountId + "~H";
                    accountComplianceClass = ComplianceClass.HIPAA;
                    break;
                case "Enhanced Security":
                    accountIdAnnotation = accountId + "~ES";
                    accountComplianceClass = ComplianceClass.EnhancedSecurity;
                    break;
                default:
                    logger.error(LOGTAG + "Unknown compliance class (" + account.getComplianceClass() + ") for account " + accountId);
                    continue;
            }

            // count (and skip) the number of exempt accounts
            if (awsAccountServiceUtil.accountCacheIsSrdExempt(accountId, LOGTAG)) {
                skippedExemptAccountsMessage += accountIdAnnotation + " ";
                nbrExemptAccounts++;
            	logger.info(LOGTAG + "[srdExempt-check] The account "+accountId+" srdExempt=true, skipping this account");
                continue;
            }
            else {
            	logger.info(LOGTAG + "[srdExempt-check] The account "+accountId+" srdExempt=false, processing this account");
            }

            // count the number of active accounts
            switch (account.getComplianceClass()) {
                case "Standard":          nbrActiveStandardAccounts++;break;
                case "HIPAA":             nbrActiveHipaaAccounts++;break;
                case "Enhanced Security": nbrActiveEnhancedSecurityAccounts++;break;
            }

            nbrStandardDetectors = 0;
            nbrHipaaDetectors = 0;
            nbrEnhancedSecurityDetectors = 0;

            for (String detectorNameRoot : detectors) {
                String detectorName = detectorNameRoot + "Detector";
               
                if (detectorIsExempt(detectorName,account.getProperty())) {
                	logger.info(LOGTAG + "The "+detectorName+" is exempt for account, "+accountId);
                	continue;
                }
                
                String remediatorName = remediateRequested ? (detectorNameRoot + "Remediator") : null;

                String jobKey = detectorName + "-" + accountId;
                String jobLOGTAG = "[DetectorExecution][" + jobKey + "] " + accountIdAnnotation;

                AbstractSecurityRiskDetector srd = securityRiskDetectorMap.get(detectorName);
                if (srd == null) {
                    logger.info(jobLOGTAG + " skipping unknown detector");
                    continue;
                }

                nbrStandardDetectors += (srd.isStandardComplianceClass() ? 1 : 0);
                nbrHipaaDetectors += (srd.isHipaaComplianceClass() ? 1 : 0);
                nbrEnhancedSecurityDetectors += (srd.isEnhancedSecurityComplianceClass() ? 1 : 0);

                if (srd.isDetectorRelevantToAccount(accountComplianceClass)) {
                    nbrRelevantDetectors++;

                    // if the job is still running since a previous schedule, don't run it again this time
                    Long runningSince = batchedJobsInProgress.putIfAbsent(jobKey, start);
                    if (runningSince == null) {
                        // job is not running so we may start a new job depending on frequency backoff
                        long detectorExecutionFinishTime = srdRuntimeStorageUtil.retrieveDetectorExecutionFinishTime(srd.getBaseName(), accountId, logger, jobLOGTAG);
                        long nextStartTime = detectorExecutionFinishTime + srd.getFrequencyBackoff() * 1000;
                        if (nextStartTime < start) {
                            // start a new job
                            long nano = Math.abs(System.nanoTime());
                            try {
                                SecurityRiskDetectionRequisition requisition = (SecurityRiskDetectionRequisition) detectionRequisition.clone();
                                requisition.setAccountId(accountId);
                                requisition.setSecurityRiskDetector(detectorName);
                                requisition.setSecurityRiskRemediator(remediatorName);

                                logger.debug(jobLOGTAG + " adding job to thread pool for nano " + nano);
                                DetectorExecution de = new DetectorExecution(requisition, jobKey,
                                        inDoc, eControlArea, msg, nano, true);
                                srdGenerationThreadPool.addJob(de);
                                nbrJobsAddedToPool++;
                            }
                            catch (ThreadPoolException e) {
                                // failed to add job so undo any counters and tracking
                                batchedJobsInProgress.remove(jobKey);
                                logger.error(jobLOGTAG + " error adding job to thread pool for nano " + nano + ". The exception is: " + e.getMessage());
                            }
                            catch (EnterpriseFieldException | CloneNotSupportedException e) {
                                // failed to add job so undo any counters and tracking
                                batchedJobsInProgress.remove(jobKey);
                                logger.error(jobLOGTAG + " error cloning requisition for nano " + nano + ". The exception is: " + e.getMessage());
                            }
                        }
                        else {
                            // frequency backoff not finished so undo any counters and tracking
                            batchedJobsInProgress.remove(jobKey);
                            nbrJobsFrequencyBackoff++;
                        }
                    }
                    else {
                        // still running
                        nbrJobsStillRunning++;

                        LocalDateTime runningSinceDT = LocalDateTime.ofInstant(Instant.ofEpochMilli(runningSince), ZoneId.systemDefault());
                        Duration runningSinceDuration = Duration.between(Instant.ofEpochMilli(runningSince), Instant.ofEpochMilli(start));
                        logger.debug(jobLOGTAG + " already running since (" + runningSince + ")(" + runningSinceDT + ")(" + runningSinceDuration + ")");
                    }
                }
            }
        }

        srdMetricUtil.cacheAccountMetrics(nbrActiveStandardAccounts, nbrActiveHipaaAccounts, nbrActiveEnhancedSecurityAccounts,
                nbrExemptAccounts, nbrStandardDetectors, nbrHipaaDetectors, nbrEnhancedSecurityDetectors, nbrRelevantDetectors);

        logger.info(LOGTAG + "Completed batch. Computed "
                + nbrRelevantDetectors + " relevant detectors for the account series. "
                + nbrActiveStandardAccounts + " standard accounts with " + nbrStandardDetectors + " detectors"
                + " and " + nbrActiveHipaaAccounts + " HIPAA accounts with " + nbrHipaaDetectors + " detectors"
                + " and " + nbrActiveEnhancedSecurityAccounts + " EnhancedSecurity accounts with " + nbrEnhancedSecurityDetectors + " detectors. "
                + nbrExemptAccounts + " accounts are SRD exempt.");
        logger.info(LOGTAG + "Completed batch"
                + " adding " + nbrJobsAddedToPool + " of " + nbrRelevantDetectors + " relevant detectors to the thread pool. "
                + nbrJobsStillRunning + " jobs are still running from last time"
                + " and " + nbrJobsFrequencyBackoff + " jobs were frequency backoff");
        logger.info(LOGTAG + "Completed batch"
                + " thread pool stats " + srdGenerationThreadPool.getStats().toString().replace("\n", ", "));
        logger.info(LOGTAG + "Completed batch. Skipped exempt accounts " + skippedExemptAccountsMessage);

        // a successful reply with nothing in the DataArea
        Document responseDoc = (Document) m_responseDoc.clone();
        responseDoc.getRootElement().getChild("DataArea").removeContent();
        return getMessage(msg, buildReplyDocument(eControlArea, responseDoc));
    }

	/**
	 * detectorIsExempt will search for a property in the account whose key starts with 
	 * "SRD:". This key is either an SRD (Only) or SRD:ARN exemption. If it is an SRD 
	 * exemption then it will not contain the string ":arn:". Conversely, if it contains
	 * the ":arn:" string then it is an SRD:ARN exemption. 
	 * 
	 * @param detectorName - the name of the detector in question
	 * @param propertiesList - a list of account properties that may contain SRD or SRD:ARN exemptions
	 * @return a boolean true if the detectorName matches an SRD exemption in the account properties
	 * and a false otherwise
	 * 
	 * 
	 */
	private boolean detectorIsExempt(String detectorName, List<Property> propertiesList) {
		for(Property property: propertiesList) {
			String key = property.getKey();
			if (key.startsWith("SRD:")) {
				if ("exempt".equals(property.getValue())) {
					// Make sure this is not an SRD:ARN exemption
					// ARN exemption implies that detector is NOT exempt
					if (key.indexOf(":arn:") == -1) { 
						String exemptDetector = key.split(":")[1];
						if ((exemptDetector+"Detector").equals(detectorName)) {
							return true;
						}
					}  
				}
			}
		}
		return false;
	}

	/**
     * Model the detector execution as a thread.
     */
    private class DetectorExecution implements Runnable {
        private final String LOGTAG;
        private final SecurityRiskContext srContext;
        private final String accountId;
        private final String detectorName;
        private final String remediatorName;
        private final String jobKey;
        private final String detectorLockName;
        private final Document inDoc;
        private final Element eControlArea;
        private final TextMessage msg;

        /** one of commandException or reply is set - not generally available when reschedule is true */
        private Message reply;
        private CommandException commandException;
        /** nano is used in log messages */
        private long nano;
        /** typically true for batch but not for single requests */
        private boolean reschedule;

        DetectorExecution(SecurityRiskDetectionRequisition requisition, String jobKey,
                          Document inDoc, Element eControlArea, TextMessage msg, long nano, boolean reschedule) {

            this.accountId = requisition.getAccountId();
            this.detectorName = requisition.getSecurityRiskDetector();
            this.remediatorName = requisition.getSecurityRiskRemediator();

            this.LOGTAG = "[DetectorExecution][" + this.detectorName + "-" + this.accountId + "] ";
            this.srContext = new SecurityRiskContext(LOGTAG, srdDetectorLock, requisition);
            this.srContext.startThreadDelayEvent();

            this.jobKey = jobKey;
            this.detectorLockName = "SRD_" + this.detectorName + srdDetectorLockNameInfix + this.accountId;

            this.inDoc = inDoc;
            this.eControlArea = eControlArea;
            this.msg = msg;

            this.nano = nano;
            this.reschedule = reschedule;
        }

        @Override
        public void run() {
            srContext.endThreadDelayEvent();
            srContext.startDetectorExecutionEvent();
            logger.info(LOGTAG + "Starting detector execution for nano " + nano);

            SrdMetricUtil srdMetricUtil = SrdMetricUtil.getInstance();

            SecurityRiskDetection detection = null;
            AbstractSecurityRiskDetector detector = null;
            SecurityRiskRemediator remediator;

            try {
                srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorSetup);
                try {
                    // while unlikely, there can be a delay from the time the thread was added to the pool
                    // to the time that it is executed.  in that time the account could be marked SRD exempt so we
                    // check now that we're about to actually execute the detector
                    if (AwsAccountServiceUtil.getInstance().accountCacheIsSrdExempt(accountId, LOGTAG)) {
                        batchedJobsInProgress.remove(jobKey);
                        String errDesc = "Account is exempt from Security Risk Detection scans";
                        srContext.withApplicationError(SecurityRiskContext.ERR_CODE_SKIPPED_SRD_EXEMPT, errDesc);
                        reply = replyWithApplicationError(SecurityRiskContext.ERR_CODE_SKIPPED_SRD_EXEMPT, errDesc, eControlArea, inDoc, msg, LOGTAG);
                        return;
                    }

                    // get the pre-initialized detector and remediator
                    detector = securityRiskDetectorMap.get(detectorName);
                    remediator = securityRiskRemediatorMap.get(remediatorName);
                    if (detector == null) {
                        batchedJobsInProgress.remove(jobKey);
                        String errDesc = "Detector " + detectorName + " is invalid.";
                        srContext.withApplicationError(SecurityRiskContext.ERR_CODE_INVALID_DETECTOR, errDesc);
                        reply = replyWithApplicationError(SecurityRiskContext.ERR_CODE_INVALID_DETECTOR, errDesc, eControlArea, inDoc, msg, LOGTAG);
                        return;
                    }

                    try {
                        detection = (SecurityRiskDetection) getAppConfig().getObjectByType(SecurityRiskDetection.class.getName());
                    }
                    catch (EnterpriseConfigurationObjectException e) {
                        batchedJobsInProgress.remove(jobKey);
                        String errDesc = "Error retrieving a 'SecurityRiskDetection' object from AppConfig. The exception is: " + e.getMessage();
                        srContext.withApplicationError(SecurityRiskContext.ERR_CODE_MISSING_FROM_APP_CONFIG, errDesc);
                        reply = replyWithApplicationError(SecurityRiskContext.ERR_CODE_MISSING_FROM_APP_CONFIG, errDesc, eControlArea, inDoc, msg, LOGTAG);
                        return;
                    }
                }
                finally {
                    srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorSetup);
                }

                try {
                    Key detectorLockKey = null;

                    try {
                        /*
                         * A detector/remediator should not run concurrently for a same type and account.
                         * So, we lock for the duration of the execution of the detector and remediator.
                         */
                        srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorLockSet);
                        final long atomic = detectorLockSetWaitingCount.incrementAndGet();
                        try {
                            detectorLockKey = srContext.detectorLock.set(detectorLockName);
                            logger.info(LOGTAG + "Set detector lock " + detectorLockKey.getValue() + " with number of waiters " + atomic);
                        }
                        finally {
                            detectorLockSetWaitingCount.decrementAndGet();
                            srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorLockSet);
                            srContext.detectorExecutionSetCounter(SrdMetricTypeField.SRD_DetectorLockSetWaitingCount, atomic);
                        }

                        srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_Detect);
                        Exception uncaughtException = null;
                        try {
                            detector.detect(detection, accountId, srContext);
                        }
                        catch (Exception e) {
                            uncaughtException = e;
                        }
                        finally {
                            /*
                             * ensure that a DetectionResult is always set.
                             * the DetectionType has to be approximated using the detector name
                             * this mostly works because the detector name matches the DetectionType enum values.
                             * where it doesn't, special care is taken to ensure DetectionResult is set.
                             * for example, see the S3PublicBucketDetector class
                             */
                            if (detection.getDetectionResult() == null) {
                                DetectionStatus detectionStatus;
                                if (uncaughtException != null) {
                                    if (ConnectTimedOutException.isConnectTimedOutException(uncaughtException)) {
                                        detectionStatus = DetectionStatus.DETECTION_TIMEOUT;
                                    }
                                    else {
                                        detectionStatus = DetectionStatus.DETECTION_FAILED;
                                        logger.error(srContext.LOGTAG + "Uncaught Detection exception", uncaughtException);
                                    }
                                }
                                else {
                                    detectionStatus = DetectionStatus.DETECTION_SUCCESS;
                                }

                                DetectionResult detectionResult = detection.newDetectionResult();
                                detection.setDetectionResult(detectionResult);
                                detectionResult.setType(detectorName.replace("Detector", ""));
                                detectionResult.setStatus(detectionStatus.getStatus());
                                if (uncaughtException != null) {
                                    detectionResult.addError("Uncaught Detection exception: " + uncaughtException.getMessage());
                                }
                            }
                            srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_Detect);
                            srContext.detectorExecutionSetCounter(SrdMetricTypeField.SRD_DetectedIssueCount, detection.getDetectedSecurityRisk().size());
                        }

                        logger.info(LOGTAG + "Finished detection with detected risk size " + detection.getDetectedSecurityRisk().size());

                        srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_Remediate);
                        try {
                            boolean isDetectionSuccess = DetectionStatus.DETECTION_SUCCESS.getStatus().equals(detection.getDetectionResult().getStatus());
                            for (Object issue : detection.getDetectedSecurityRisk()) {
                                DetectedSecurityRisk dsr = (DetectedSecurityRisk) issue;

                                try {
                                    if (remediator != null && isDetectionSuccess) {
                                        remediator.remediate(accountId, dsr, srContext);
                                    }
                                }
                                catch (Exception e) {
                                    RemediationStatus remediationStatus;
                                    if (ConnectTimedOutException.isConnectTimedOutException(e)) {
                                        remediationStatus = RemediationStatus.REMEDIATION_TIMEOUT;
                                    }
                                    else {
                                        remediationStatus = RemediationStatus.REMEDIATION_FAILED;
                                        logger.error(srContext.LOGTAG + "Uncaught Remediation exception", e);
                                    }
                                    AbstractSecurityRiskRemediator.newRemediationResult(LOGTAG, dsr, remediationStatus, "Remediation Error",
                                            "Uncaught Remediation exception: " + e.getMessage());
                                }

                                if (logger.isInfoEnabled()) {
                                    String msg;
                                    if (AbstractSecurityRiskRemediator.isRiskIgnored(dsr)) {
                                        msg = "Ignoring risk because " + AbstractSecurityRiskRemediator.getRiskIgnoredReason(dsr);
                                    }
                                    else if (remediator == null) {
                                        msg = "Skipped remediation";
                                    }
                                    else if (!isDetectionSuccess) {
                                        // this one is a bit odd because detection failed for some reason but not before
                                        // creating at least one DetectedSecurityRisk which we did not try to remediate
                                        msg = "Detection not successful";
                                    }
                                    else {
                                        msg = "Finished remediation";
                                    }
                                    logger.info(LOGTAG + msg
                                            + " for " + remediatorName
                                            + " for arn " + dsr.getAmazonResourceName()
                                            + " for type " + dsr.getType());
                                }
                            }
                        }
                        finally {
                            srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_Remediate);
                        }
                    }
                    finally {
                        if (detectorLockKey != null) {
                            srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorLockRelease);
                            final long atomic = detectorLockReleaseWaitingCount.incrementAndGet();
                            try {
                                srContext.detectorLock.release(detectorLockName, detectorLockKey);
                                logger.info(LOGTAG + "Released detector lock " + detectorLockKey.getValue() + " with number of waiters " + atomic);
                            }
                            catch (LockException e) {
                                // detector and remediator have run so don't prevent the response from being sent
                                logger.warn(LOGTAG + "Ignoring detector lock release failure (" + detectorLockName
                                        + " / " + detectorLockKey.getValue() + "). The exception is: " + e.getMessage());
                            }
                            finally {
                                detectorLockReleaseWaitingCount.decrementAndGet();
                                srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorLockRelease);
                                srContext.detectorExecutionSetCounter(SrdMetricTypeField.SRD_DetectorLockReleaseWaitingCount, atomic);
                            }
                        }
                    }

                    String nextSecurityRiskDetectionId;
                    srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_SequenceGeneration);
                    long atomic = sequenceGenerationWaitingCount.incrementAndGet();
                    try {
                        nextSecurityRiskDetectionId = generateNextSecurityRiskDetectionId(LOGTAG);
                    }
                    finally {
                        sequenceGenerationWaitingCount.decrementAndGet();
                        srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_SequenceGeneration);
                        srContext.detectorExecutionSetCounter(SrdMetricTypeField.SRD_SequenceGenerationWaitingCount, atomic);
                    }

                    srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_BuildResults);
                    try {
                        // setup the response and serialize it into the reply
                        detection.setAccountId(accountId);
                        detection.setSecurityRiskDetector(detectorName);
                        detection.setSecurityRiskRemediator((remediator != null) ? remediatorName : "none");
                        Iterator<?> iterator = detection.getDetectedSecurityRisk().iterator();
                        while (iterator.hasNext()) {
                            DetectedSecurityRisk dsr = (DetectedSecurityRisk) iterator.next();
                            if (AbstractSecurityRiskRemediator.isRiskIgnored(dsr))
                                iterator.remove();
                        }
                        detection.setInstanceId(OPEN_EAI_INSTANCE_ID);
                        detection.setCreateUser("rhedcloud-srd-service");
                        detection.setCreateDatetime(new Datetime(new Date()));
                        detection.setSecurityRiskDetectionId(nextSecurityRiskDetectionId);


                        // build the success reply message.
                        Document responseDoc = (Document) m_responseDoc.clone();
                        responseDoc.getRootElement().getChild("DataArea").removeContent();
                        responseDoc.getRootElement().getChild("DataArea").addContent((Element) detection.buildOutputFromObject());
                        reply = getMessage(msg, buildReplyDocument(eControlArea, responseDoc));
                    }
                    finally {
                        srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_BuildResults);
                    }

                    srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_PublishSync);
                    atomic = publishSyncWaitingCount.incrementAndGet();
                    try {
                        // Publish the SecurityRiskDetection.Create-Sync message.
                        PubSubProducer pub = (PubSubProducer) m_pubSubProducerPool.getProducer();
                        detection.createSync(pub);

                        // gather metrics after we generate the sync message because it is the commands handling
                        // the sync that compare against the latest value of the sequence
                        srdMetricUtil.cacheLastSecurityRiskDetectionIdGenerated(nextSecurityRiskDetectionId);
                    }
                    finally {
                        publishSyncWaitingCount.decrementAndGet();
                        srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_PublishSync);
                        srContext.detectorExecutionSetCounter(SrdMetricTypeField.SRD_PublishSyncWaitingCount, atomic);
                    }
                }
                catch (AmazonServiceException e) {
                    // generally, exceptions should be dealt with in the detectors and remediators but some
                    // cases aren't handled yet.  in the mean time catch them here so they can be properly
                    // logged instead of making their way to catalina.out
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_UNCAUGHT_AMAZON_SERVICE_EXCEPTION,
                            "An error occurred, uncaught Amazon Service exception. The exception is (" + e.getClass().getName() + "): " + e.getMessage());
                }
                catch (SdkClientException e) {
                    // generally, exceptions should be dealt with in the detectors and remediators but some
                    // cases aren't handled yet.  in the mean time catch them here so they can be properly
                    // logged instead of making their way to catalina.out
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_UNCAUGHT_AWS_SDK_EXCEPTION,
                            "An error occurred, uncaught AWS SDK exception. The exception is (" + e.getClass().getName() + "): " + e.getMessage());
                }
                catch (EnterpriseFieldException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_ENTERPRISE_FIELD_EXCEPTION,
                            "An error occurred setting the value of the SecurityRiskDetection object. The exception is: " + e.getMessage());
                }
                catch (EnterpriseLayoutException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_ENTERPRISE_LAYOUT_EXCEPTION,
                            "An error occurred building SecurityRiskDetection element. The exception is: " + e.getMessage());
                }
                catch (EnterpriseObjectSyncException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_ENTERPRISE_OBJECT_SYNC_EXCEPTION,
                            "An error occurred publishing the Detection.Create-Sync message. The exception is: " + e.getMessage());
                }
                catch (JMSException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_JMS_EXCEPTION,
                            "An error occurred retrieving a pub/sub producer to use to publish the Create-Sync. The exception is: " + e.getMessage());
                }
                catch (LockException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_LOCK_EXCEPTION,
                            "An error occurred, detector lock operation failed (" + detectorLockName + "). The exception is: " + e.getMessage());
                }
                catch (LockAlreadySetException e) {
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_DETECTOR_ALREADY_RUNNING,
                            "Detector " + detectorName + " is already running for account " + accountId + ".");
                }
                catch (Exception e) {
                    // every type of exception should have been handled but some
                    // cases aren't handled yet.  in the mean time catch them here so they can be properly
                    // logged instead of making their way to catalina.out
                    srContext.withApplicationError(SecurityRiskContext.ERR_CODE_UNCAUGHT_EXCEPTION,
                            "An error occurred, uncaught exception. The exception is (" + e.getClass().getName() + "): " + e.getMessage());
                    // serious enough to log a stack trace
                    logger.error(LOGTAG + "An error occurred, uncaught exception.", e);
                }
                finally {
                    srContext.detectorExecutionStartEvent(SrdMetricTypeField.SRD_DetectorFinalizing);
                    try {
                        if (srContext.hasApplicationError()) {
                            String replyContents = buildReplyDocumentWithErrors(eControlArea, (Document) m_responseDoc.clone(),
                                    Collections.singletonList(buildError("application", srContext.errCode, srContext.errDesc)));
                            reply = getMessage(msg, replyContents);
                        }
                    }
                    finally {
                        srContext.detectorExecutionEndEvent(SrdMetricTypeField.SRD_DetectorFinalizing);
                    }
                }
            }
            catch (CommandException e) {
                reply = null;
                commandException = e;
                srContext.detectorExecutionIncrementCounter(SrdMetricTypeField.SRD_DetectorCommandException);
            }
            finally {
                srContext.endDetectorExecutionEvent();

                srContext.getMetricCollector().logMetricDatum(accountId, detectorName);
                srContext.getMetricCollector().clearMetricDatum();

                if (detector != null)
                    SrdRuntimeStorageUtil.getInstance().updateDetectorExecutionFinishTime(detector.getBaseName(), accountId, logger, LOGTAG);

                logger.info(LOGTAG + "Finished detector execution for nano " + nano
                        + " with securityRiskDetectionId " + ((detection == null) ? "null" : detection.getSecurityRiskDetectionId())
                        + " with instanceId " + OPEN_EAI_INSTANCE_ID
                        + (!srContext.hasApplicationError() ? ""
                            : (" with errCode " + srContext.errCode + " with errDesc " + srContext.errDesc + "!")));


                /*
                 * if a reschedule was asked for, we may decide otherwise.
                 *
                 * if the system is shutting down we will not reschedule.
                 *
                 * if the frequency backoff is non-zero then we will not reschedule.
                 *
                 * if the job is already running (in another instance) or if the account is srdExempt
                 *  then do not restart now but wait for the next scheduled run to start it.
                 *
                 * this relies on the scheduled job to fire often enough that the
                 *  detectors get run again in a reasonable time.
                 */
                if (reschedule && shuttingDown.get()) {
                    logger.info(LOGTAG + "Asked to reschedule but decided otherwise because shutting down");
                    reschedule = false;
                }
                if (reschedule && srContext.hasERR_CODE_DETECTOR_ALREADY_RUNNING()) {
                    logger.info(LOGTAG + "Asked to reschedule but decided otherwise because detector is already running");
                    reschedule = false;
                }
                if (reschedule && srContext.hasERR_CODE_SKIPPED_SRD_EXEMPT()) {
                    logger.info(LOGTAG + "Asked to reschedule but decided otherwise because account is srdExempt");
                    reschedule = false;
                }
                if (reschedule && detector != null && detector.getFrequencyBackoff() > 0) {
                    logger.info(LOGTAG + "Asked to reschedule but decided otherwise because of frequency backoff");
                    reschedule = false;
                }
                if (reschedule) {
                    /*
                     * put ourselves back into the thread pool.
                     * a few of the housekeeping variables need to be reset first.
                     * this will reset 'reply' and 'commandException' but the idea is that they were not
                     *  required and the caller is just interested in the SecurityRiskDetection.Create-Sync message.
                     */
                    nano = Math.abs(System.nanoTime());
                    reply = null;
                    commandException = null;
                    srContext.reschedule();
                    batchedJobsInProgress.put(jobKey, System.currentTimeMillis()); // update start time

                    try {
                        logger.info(LOGTAG + "reschedule adding job to thread pool for nano " + nano);
                        srContext.startThreadDelayEvent();
                        srdGenerationThreadPool.addJob(this);
                    }
                    catch (ThreadPoolException e) {
                        // failed to add job so undo any counters and tracking
                        batchedJobsInProgress.remove(jobKey);
                        logger.error(LOGTAG + " error adding job to thread pool for nano " + nano + ". The exception is: " + e.getMessage());
                    }
                }
                else {
                    // not putting ourselves back into the thread pool so mark the job as done.
                    batchedJobsInProgress.remove(jobKey);
                }
            }
        }
    }

    /**
     * Account information is important to the operation of the SRD service.
     * Whenever we request account information from AWS Account Service it is cached.
     * This allows other components of SRD to have access to up to date account info.
     * To be a little more pro-active and redundant, we setup a self-rescheduling thread that updates the account cache.
     */
    private class AwsAccountServiceAccountCacheRefresher implements Runnable {
        private static final String LOGTAG = "[SecurityRiskDetectionGenerateCommand][AwsAccountServiceAccountCacheRefresher] ";
        private final long refresherRate;

        public AwsAccountServiceAccountCacheRefresher(long refresherRate) {
            this.refresherRate = refresherRate;
        }

        @Override
        public void run() {
            try {
                // do not need the results, the side effect of refreshing the cache is what we are after
                AwsAccountServiceUtil.getInstance().getAllAccounts(LOGTAG);
            }
            catch (Exception e) {
                logger.error(LOGTAG + "An error occurred while getting accounts from the AWS Account Producer"
                        + ". The exception is: " + e.getMessage());
            }

            // if the JVM is shutting down, no need to sleep
            if (shuttingDown.get()) {
                logger.info(LOGTAG + "AwsAccountServiceAccountCacheRefresher shutting down");
                return;
            }

            try {
                Thread.sleep(refresherRate);
            }
            catch (InterruptedException e) {
                logger.error(LOGTAG + "Ignoring error while sleeping. The exception is: " + e.getMessage());
            }

            // just finished sleeping so check if we should put ourselves back in the thread pool
            if (shuttingDown.get()) {
                logger.info(LOGTAG + "AwsAccountServiceAccountCacheRefresher shutting down");
                return;
            }

            try {
                srdGenerationThreadPool.addJob(this);
            }
            catch (ThreadPoolException e) {
                logger.error(LOGTAG + "Error adding job back to thread pool. Running manually. The exception is: " + e.getMessage());
                new AwsAccountServiceAccountCacheRefresher(refresherRate).run();
            }
        }
    }

    private void setupRuntimeStorage(AppConfig appConfig, SrdRuntimeStorageUtil srdRuntimeStorageUtil) throws CommandException {
        String LOGTAG = "[SrdGenerateCommand] ";

        AmazonDynamoDB dynamoClient;
        Table storageTable;

        String accessKeyId;
        String secretKey;
        String tableRegion;
        String storageTableName;
        try {
            Properties storageProperties = appConfig.getProperties("SrdRuntimeStorage");
            accessKeyId = storageProperties.getProperty("accessKeyId");
            secretKey = storageProperties.getProperty("secretKey");
            tableRegion = storageProperties.getProperty("tableRegion");
            storageTableName = storageProperties.getProperty("storageTableName");
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error getting the SrdRuntimeStorage property.");
            throw new CommandException(e.getMessage());
        }

        // runtime/metrics storage can be disabled to support the test suite but, otherwise, doesn't make sense
        if (storageTableName == null) {
            logger.info(LOGTAG + "SRD Runtime/Metrics Storage disabled because table name is empty");
            return;
        }

        try {
            synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
                dynamoClient = AmazonDynamoDBClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey)))
                        .withRegion(tableRegion)
                        .build();
            }
        }
        catch (Exception e) {
            logger.fatal(LOGTAG + "Error creating SRD Runtime Storage client");
            throw new CommandException(e.getMessage());
        }

        DynamoDB dynamoDB = new DynamoDB(dynamoClient);
        TableDescription tableDescription;

        try {
            storageTable = dynamoDB.getTable(storageTableName);
            try {
                tableDescription = storageTable.describe();
            }
            catch (ResourceNotFoundException e) {
                tableDescription = null;
            }
            if (tableDescription == null) {
                CreateTableRequest createTableRequest = new CreateTableRequest()
                        .withTableName(storageTableName)
                        .withKeySchema(
                                new KeySchemaElement("detectorName", KeyType.HASH), // Partition key
                                new KeySchemaElement("property", KeyType.RANGE))
                        .withAttributeDefinitions(
                                new AttributeDefinition("detectorName", ScalarAttributeType.S),
                                new AttributeDefinition("property", ScalarAttributeType.S))
                        .withProvisionedThroughput(new ProvisionedThroughput(5L, 5L));

                storageTable = dynamoDB.createTable(createTableRequest);
                tableDescription = storageTable.waitForActive();

                logger.info(LOGTAG + "Created SRD Runtime Storage table: " + tableDescription);
            }
            else {
                logger.info(LOGTAG + "Using SRD Runtime Storage table: " + tableDescription);
            }
        }
        catch (Exception e) {
            logger.fatal(LOGTAG + "Error creating SRD Runtime Storage table: " + storageTableName);
            throw new CommandException(e.getMessage());
        }

        srdRuntimeStorageUtil.initialize(storageTable);
    }

    private void setupSyncStorage(AppConfig appConfig, SrdSyncStorageUtil srdSyncStorageUtil) throws CommandException {
        String LOGTAG = "[SrdGenerateCommand] ";

        AmazonDynamoDB dynamoClient;
        Table storageTable;

        String accessKeyId;
        String secretKey;
        String tableRegion;
        String storageTableName;
        try {
            Properties storageProperties = appConfig.getProperties("SrdSyncStorage");
            accessKeyId = storageProperties.getProperty("accessKeyId");
            secretKey = storageProperties.getProperty("secretKey");
            tableRegion = storageProperties.getProperty("tableRegion");
            storageTableName = storageProperties.getProperty("storageTableName");
        }
        catch (EnterpriseConfigurationObjectException e) {
            logger.fatal(LOGTAG + "Error getting the SrdSyncStorage property.");
            throw new CommandException(e.getMessage());
        }

        // sync storage can be disabled to support the test suite but, otherwise, doesn't make sense
        if (storageTableName == null) {
            logger.info(LOGTAG + "SRD Sync Storage disabled because table name is empty");
            return;
        }

        try {
            synchronized (SecurityRiskAwsRegionsAndClientBuilder.builderLock) {
                dynamoClient = AmazonDynamoDBClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKeyId, secretKey)))
                        .withRegion(tableRegion)
                        .build();
            }
        }
        catch (Exception e) {
            logger.fatal(LOGTAG + "Error creating SRD Sync Storage client");
            throw new CommandException(e.getMessage());
        }

        DynamoDB dynamoDB = new DynamoDB(dynamoClient);
        TableDescription tableDescription;

        try {
            storageTable = dynamoDB.getTable(storageTableName);
            try {
                tableDescription = storageTable.describe();
            }
            catch (ResourceNotFoundException e) {
                tableDescription = null;
            }
            if (tableDescription == null) {
                /*
                 * we'd like to have ProvisionedThroughput be auto scaling but only
                 * in PROD (because it is PROD) and TEST (because performance metrics are often gathered there).
                 * but not in the other environments to save cost.
                 *
                 * but it isn't implemented so provisioning is adjusted manually after the table is created
                 * see https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/AutoScaling.HowTo.SDK.html
                 */
                long readUnits = 5;
                long writeUnits = 5;
                CreateTableRequest createTableRequest = new CreateTableRequest()
                        .withTableName(storageTableName)
                        .withKeySchema(
                                new KeySchemaElement("typeAccount", KeyType.HASH), // Partition key
                                new KeySchemaElement("createDatetime", KeyType.RANGE))
                        .withAttributeDefinitions(
                                new AttributeDefinition("typeAccount", ScalarAttributeType.S),
                                new AttributeDefinition("createDatetime", ScalarAttributeType.N),
                                new AttributeDefinition("securityRiskDetectionId", ScalarAttributeType.S))
                        .withProvisionedThroughput(new ProvisionedThroughput(readUnits, writeUnits))
                        .withGlobalSecondaryIndexes(new GlobalSecondaryIndex()
                                .withIndexName("SecurityRiskDetectionIdIndex")
                                .withProvisionedThroughput(new ProvisionedThroughput(readUnits, writeUnits))
                                .withKeySchema(
                                        new KeySchemaElement("securityRiskDetectionId", KeyType.HASH), // Partition key
                                        new KeySchemaElement("createDatetime", KeyType.RANGE))
                                .withProjection(new Projection().withProjectionType(ProjectionType.ALL)));

                storageTable = dynamoDB.createTable(createTableRequest);
                tableDescription = storageTable.waitForActive();

                logger.info(LOGTAG + "Created SRD Sync Storage table: " + tableDescription);
            }
            else {
                logger.info(LOGTAG + "Using SRD Sync Storage table: " + tableDescription);

                // if there is a need to update the table then this code might be useful
                // it was once used to add the SecurityRiskDetectionIdIndex GlobalSecondaryIndex
                /*
                DescribeTableResult result = dynamoDBClient.describeTable(dynamoDbTableName);
                if (result.getTable().getGlobalSecondaryIndexes() == null || result.getTable().getGlobalSecondaryIndexes().isEmpty()) {
                    UpdateTableRequest updateTableRequest = new UpdateTableRequest()
                            .withTableName(dynamoDbTableName)
                            .withGlobalSecondaryIndexUpdates(new GlobalSecondaryIndexUpdate()
                                    .withCreate(new CreateGlobalSecondaryIndexAction()
                                            .withIndexName("SecurityRiskDetectionIdIndex")
                                            .withProvisionedThroughput(new ProvisionedThroughput(readUnits, writeUnits))
                                            .withKeySchema(new KeySchemaElement("securityRiskDetectionId", KeyType.HASH), // Partition key
                                                           new KeySchemaElement("createDatetime", KeyType.RANGE))
                                            .withProjection(new Projection().withProjectionType(ProjectionType.ALL)))
                            )
                            .withAttributeDefinitions(new AttributeDefinition("typeAccount", ScalarAttributeType.S),
                                    new AttributeDefinition("createDatetime", ScalarAttributeType.N),
                                    new AttributeDefinition("securityRiskDetectionId", ScalarAttributeType.S))
                            ;
                    dynamoDBClient.updateTable(updateTableRequest);
                }
                */
            }
        }
        catch (Exception e) {
            logger.fatal(LOGTAG + "Error creating SRD Sync Storage table: " + storageTableName);
            throw new CommandException(e.getMessage());
        }

        srdSyncStorageUtil.initialize(dynamoClient, storageTable);
    }
}
