package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.DistributionConfig;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigRequest;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigResult;
import com.amazonaws.services.cloudfront.model.InvalidWebACLIdException;
import com.amazonaws.services.cloudfront.model.Origin;
import com.amazonaws.services.cloudfront.model.UpdateDistributionRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.SecurityRiskRemediationException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.Properties;

/**
 * Remediates cloud distributions backed by S3 buckets (static site) and has no WebAcl attached<br>
 * <p>The remediator updates the configuration and sets WebAclId to what is described in the config file under CloudFrontStaticSiteACL : webAclId</p>
 *
 * @see edu.emory.it.services.srd.detector.CloudFrontStaticSiteACLDetector the detector
 */
public class CloudFrontStaticSiteACLRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private String webAclId;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskRemediationException {
        super.init(aConfig, credentialsProvider, securityRole);
        try {
            Properties properties = appConfig.getProperties("CloudFrontStaticSiteACL");
            webAclId = properties.getProperty("webAclId");
        } catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskRemediationException(e);
        }
    }

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudfront:", srContext.LOGTAG, DetectionType.CloudFrontStaticSiteACL))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String distributionPath = arn[5];
        String distributionId = distributionPath.substring(distributionPath.lastIndexOf("/") + 1);

        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Distribution with id '" + distributionId + "'", e);
            return;
        }

        GetDistributionConfigRequest request = new GetDistributionConfigRequest().withId(distributionId);
        GetDistributionConfigResult result = cloudFront.getDistributionConfig(request);

        boolean hasChange = false;
        DistributionConfig config = result.getDistributionConfig();
        if (config.getWebACLId() == null || config.getWebACLId().isEmpty()) {
            for (Origin origin : config.getOrigins().getItems()) {
                if (origin.getS3OriginConfig() != null) {
                    hasChange = true;
                    break;
                }
            }
        }

        if (!hasChange) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Distribution with id '" + distributionId + "' only already has Web Acl set for static content");
            return;
        }


        //TODO need to make sure WAF defined in master account is accessible by other accounts in the organization, perhaps we need to use WAF api to associate the resource if the below doesn't work
        config.setWebACLId(webAclId);

        UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest()
                .withId(distributionId)
                .withIfMatch(result.getETag())
                .withDistributionConfig(config);

        try {
            cloudFront.updateDistribution(updateDistributionRequest);
        } catch (InvalidWebACLIdException e) {
            logger.error(srContext.LOGTAG + "Configuration property CloudFrontStaticSiteACL > webAclId is incorrect.  The id cannot be found");
            setError(detected, srContext.LOGTAG,
                    "Remediator not complete, there is a configuration error on the server", e);
            return;
        }

        setSuccess(detected, srContext.LOGTAG,
                "Distribution with id '" + distributionId + "' has been updated with Web ACL '" + webAclId + "'");
    }
}
