package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.cloudtrail.AWSCloudTrail;
import com.amazonaws.services.cloudtrail.AWSCloudTrailClient;
import com.amazonaws.services.cloudtrail.model.DescribeTrailsRequest;
import com.amazonaws.services.cloudtrail.model.DescribeTrailsResult;
import com.amazonaws.services.cloudtrail.model.GetTrailStatusRequest;
import com.amazonaws.services.cloudtrail.model.GetTrailStatusResult;
import com.amazonaws.services.cloudtrail.model.Trail;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects if CloudTrail logging is disabled.<br>
 *
 * @see edu.emory.it.services.srd.remediator.CloudTrailDisabledRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class CloudTrailDisabledDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final  AWSCloudTrail cloudTrail;
            try {
                cloudTrail = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSCloudTrailClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CloudTrailDisabled,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arn = "arn:aws:cloudtrail:" + region.getName() + ":" + accountId + ":trail";

            checkCloudTrail(cloudTrail, detection, arn, accountId, srContext);
        }
    }

    private void checkCloudTrail(AWSCloudTrail cloudTrail, SecurityRiskDetection detection,
                                 String arn, String accountId, SecurityRiskContext srContext) {
        DescribeTrailsRequest request = new DescribeTrailsRequest().withIncludeShadowTrails(true);
        DescribeTrailsResult result = cloudTrail.describeTrails(request);

        boolean hasActiveTrail = false;
        for (Trail trail : result.getTrailList()) {
            AWSCloudTrail regionClient;
            try {
                regionClient = getClient(accountId, trail.getHomeRegion(), srContext.getMetricCollector(), AWSCloudTrailClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + trail.getHomeRegion() + ". AWS Error: " + e.getMessage());
                return;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.CloudTrailDisabled,
                        srContext.LOGTAG, "On region: " + trail.getHomeRegion(), e);
                return;
            }
            GetTrailStatusRequest getTrailStatusRequest = new GetTrailStatusRequest().withName(trail.getName());
            GetTrailStatusResult getTrailSTatusResult = regionClient.getTrailStatus(getTrailStatusRequest);
            if (getTrailSTatusResult.isLogging()) {
                hasActiveTrail = true;
                break;
            }
        }

        if (!hasActiveTrail) {
            addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.CloudTrailDisabled, arn);
        }
    }

    @Override
    public String getBaseName() {
        return "CloudTrailDisabled";
    }
}
