package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.elasticache.AmazonElastiCache;
import com.amazonaws.services.elasticache.AmazonElastiCacheClient;
import com.amazonaws.services.elasticache.model.CacheCluster;
import com.amazonaws.services.elasticache.model.DescribeCacheClustersRequest;
import com.amazonaws.services.elasticache.model.DescribeCacheClustersResult;
import com.amazonaws.services.elasticache.model.DescribeReplicationGroupsRequest;
import com.amazonaws.services.elasticache.model.DescribeReplicationGroupsResult;
import com.amazonaws.services.elasticache.model.ReplicationGroup;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.function.Predicate;

/**
 * Check all ElastiCache clusters for risks.<br>
 * The specifics of what is checked is defined by the actual detectors.
 */
public abstract class ElastiCacheBaseDetector extends AbstractSecurityRiskDetector {
    /**
     * Is the cache cluster at risk?
     * @return true when at risk
     */
    protected abstract Predicate<CacheCluster> getCacheClusterPredicate();

    /**
     * Is the replication group at risk?
     * @return true when at risk
     */
    protected abstract Predicate<ReplicationGroup> getReplicationGroupPredicate();

    /**
     * Detect risks in ElastiCache clusters.
     * If the predicate tests true then the cluster is at risk.
     *
     * @param detection Security Risk Detection
     * @param accountId account ID
     * @param srContext Security Risk Context
     * @param riskType type of identified risk
     */
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext, DetectionType riskType) {

        for (Region region : getRegions()) {
            AmazonElastiCache client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonElastiCacheClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, riskType,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            DescribeReplicationGroupsRequest describeReplicationGroupsRequest = new DescribeReplicationGroupsRequest();
            DescribeReplicationGroupsResult describeReplicationGroupsResult;
            do {
                try {
                    describeReplicationGroupsResult = client.describeReplicationGroups(describeReplicationGroupsRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, riskType, srContext.LOGTAG, "Error describing replication groups", e);
                    return;
                }

                for (ReplicationGroup replicationGroup : describeReplicationGroupsResult.getReplicationGroups()) {
                    // for global replication groups, only consider the primary
                    // otherwise, it just a "regular" (non-global) replication group and it is scanned for risks
                    boolean isGlobalReplicationGroup = replicationGroup.getGlobalReplicationGroupInfo().getGlobalReplicationGroupId() != null;
                    boolean isGlobalReplicationGroupPrimary = replicationGroup.getGlobalReplicationGroupInfo().getGlobalReplicationGroupMemberRole() != null
                            && replicationGroup.getGlobalReplicationGroupInfo().getGlobalReplicationGroupMemberRole().equals("PRIMARY");

                    if (!isGlobalReplicationGroup || isGlobalReplicationGroupPrimary) {
                        // if the predicate tests true then the cluster is at risk
                        if (getReplicationGroupPredicate().test(replicationGroup)) {
                            String resource = isGlobalReplicationGroupPrimary
                                    ? ("global-replication-group:" + replicationGroup.getGlobalReplicationGroupInfo().getGlobalReplicationGroupId())
                                    : ("replication-group:" + replicationGroup.getReplicationGroupId());
                            addDetectedSecurityRisk(detection, srContext.LOGTAG, riskType,
                                    "arn:aws:elasticache:" + region.getName() + ":" + accountId + ":" + resource);
                        }
                    }
                }

                describeReplicationGroupsRequest.setMarker(describeReplicationGroupsResult.getMarker());
            } while (describeReplicationGroupsResult.getMarker() != null);


            DescribeCacheClustersRequest describeCacheClustersRequest = new DescribeCacheClustersRequest();
            DescribeCacheClustersResult describeCacheClustersResult;
            do {
                try {
                    describeCacheClustersResult = client.describeCacheClusters(describeCacheClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, riskType, srContext.LOGTAG, "Error describing cache clusters", e);
                    return;
                }

                for (CacheCluster cluster : describeCacheClustersResult.getCacheClusters()) {
                    // clusters that are part of a replication group (global datastore, multiple replicas) are handled above
                    boolean notInReplicationGroup = cluster.getReplicationGroupId() == null;

                    if (notInReplicationGroup) {
                        // if the predicate tests true then the cluster is at risk
                        if (getCacheClusterPredicate().test(cluster)) {
                            addDetectedSecurityRisk(detection, srContext.LOGTAG, riskType,
                                    "arn:aws:elasticache:" + region.getName() + ":" + accountId + ":cluster:" + cluster.getCacheClusterId());
                        }
                    }
                }

                describeCacheClustersRequest.setMarker(describeCacheClustersResult.getMarker());
            } while (describeCacheClustersResult.getMarker() != null);
        }
    }
}
