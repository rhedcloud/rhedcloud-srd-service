package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.workdocs.AmazonWorkDocs;
import com.amazonaws.services.workdocs.AmazonWorkDocsClient;
import com.amazonaws.services.workdocs.model.DeleteUserRequest;
import com.amazonaws.services.workdocs.model.DescribeUsersRequest;
import com.amazonaws.services.workdocs.model.DescribeUsersResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Remediates WorkDocs users without an Emory Domain Account<br>
 *
 * <p>The remediator deletes the user without a emory domain email</p>
 *
 * <p>This remediator is currently not running due to organization id not yet set up.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Alert. Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.WorkDocsNonEmoryDomainDetector the detector
 */
public class WorkDocsNonEmoryDomainRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:workdocs:", srContext.LOGTAG, DetectionType.WorkDocsNonEmoryDomain))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String userId =  arn[5].substring(arn[5].lastIndexOf("/") + 1);
        String region = arn[3];

        final AmazonWorkDocs workDocs;
        try {
            workDocs = getClient(accountId, region, srContext.getMetricCollector(), AmazonWorkDocsClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "User '" + userId + "'", e);
            return;
        }

        DescribeUsersRequest usersRequest = new DescribeUsersRequest().withUserIds(userId);
        DescribeUsersResult usersResult = workDocs.describeUsers(usersRequest);
        if (usersResult.getUsers().isEmpty()) {
            //if already deleted, no change
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "WorkDocs user '" + userId + "' already deleted.");
            return;
        }

        String email = usersResult.getUsers().get(0).getEmailAddress();
        if (email.endsWith("emory.edu") || email.endsWith("emoryhealthcare.org")) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "WorkDocs user '" + userId + "' already has an Emory email.  Their email must have updated since the time of detection.");
            return;
        }


        DeleteUserRequest deleteUserRequest = new DeleteUserRequest().withUserId(userId);
        workDocs.deleteUser(deleteUserRequest);

        setSuccess(detected, srContext.LOGTAG, "WorkDocs user '" + userId + "' has been deleted.");
    }
}
