package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.services.cloudtrail.AWSCloudTrail;
import com.amazonaws.services.cloudtrail.AWSCloudTrailClient;
import com.amazonaws.services.cloudtrail.model.Event;
import com.amazonaws.services.cloudtrail.model.LookupAttribute;
import com.amazonaws.services.cloudtrail.model.LookupEventsRequest;
import com.amazonaws.services.cloudtrail.model.LookupEventsResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdRuntimeStorageUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Detect if the account root user password was changed or simply requested.<br>
 *
 * There is a sample solution written up on the 'AWS Management Tools Blog' here:
 * https://aws.amazon.com/blogs/mt/monitor-and-notify-on-aws-account-root-user-activity/ <br>
 *
 * That is not the solution that was used.  Instead, we leverage CloudTrail that has already
 * been setup as part of the "RHEDcloud AWS CloudFormation Template for a research service account setup".<br>
 *
 * An account root user password change is a two step process - click 'Forgot password?' link then follow link in email.
 * Each step generates a separate event - <code>PasswordRecoveryRequested</code> and <code>PasswordRecoveryCompleted</code> respectively.
 * Each will generate a risk since the very act of requesting a root user password change is questionable.
 *
 * The first version of this detector kept track of the date that it was last run and used them to limit the range
 * of the search.  Because of the "eventual consistency" model that AWS uses, this caused events to be missed.
 * So, instead, the detector now queries for all events and keeps track of which ones have already been seen.
 * There are a lot of events but CloudTrail only retains 90 days worth so the number doesn't become unbounded.
 * Finally, because the detector keeps track of "seen" events they will eventually expire out of CloudTrail so
 * the detector has a mechanism of cleaning up these events from the detector runtime storage table.
 *
 * @see edu.emory.it.services.srd.remediator.RootUserPasswordChangeRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RootUserPasswordChangeDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        SrdRuntimeStorageUtil srdRuntimeStorageUtil = SrdRuntimeStorageUtil.getInstance();

        // as described in the class javadoc, keep track of events that we've seen so that we don't
        // create risks for them over and over
        Set<String> alreadyKnownPasswordChangeEvents
                = srdRuntimeStorageUtil.retrieveRootUserPasswordChangeEvents(getBaseName(), accountId, logger, srContext.LOGTAG);
        if (alreadyKnownPasswordChangeEvents == null) {
            setDetectionError(detection, DetectionType.RootUserPasswordChange,
                    srContext.LOGTAG, "Error getting already known password change events");
            return;
        }
        // need this for "expired" event management below
        Set<String> activeEvents = new HashSet<>();


        try {
            // the rhedcloud-aws-rs-account-cfn.json has IsMultiRegionTrail set to true
            // so no region is needed in the client connection
            AWSCloudTrail cloudTrailClient = getClient(accountId, srContext.getMetricCollector(), AWSCloudTrailClient.class);

            // LookupAttributes can only contain a single item so find all events by the root user
            // and then filter for the events we're interested in
            LookupEventsRequest lookupEventsRequest = new LookupEventsRequest()
                    .withMaxResults(50)
                    .withLookupAttributes(new LookupAttribute().withAttributeKey("Username").withAttributeValue("root"));
            LookupEventsResult lookupEventsResult;

            List<Event> passwordRecoveryEvents = new ArrayList<>();

            do {
                lookupEventsResult = cloudTrailClient.lookupEvents(lookupEventsRequest);

                for (Event event : lookupEventsResult.getEvents()) {
                    // only interested in PasswordRecovery events
                    if (!event.getEventName().equals("PasswordRecoveryRequested")
                            && !event.getEventName().equals("PasswordRecoveryCompleted")) {
                        continue;
                    }
                    // need this for "expired" event management below
                    activeEvents.add(event.getEventId());
                    // only interested in events we haven't seen before
                    if (!alreadyKnownPasswordChangeEvents.contains(event.getEventId())) {
                        passwordRecoveryEvents.add(event);
                        alreadyKnownPasswordChangeEvents.add(event.getEventId());
                    }
                }

                lookupEventsRequest.setNextToken(lookupEventsResult.getNextToken());
            } while (lookupEventsResult.getNextToken() != null);

            // finally, add every password recovery event as a risk
            for (Event event : passwordRecoveryEvents) {
                // Amazon hasn't defined an ARN syntax for CloudTrail events so make one up that retains the event ID and name
                addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.RootUserPasswordChange,
                        "arn:aws:cloudtrail::" + accountId + ":event/" + event.getEventId() + "/" + event.getEventName());
            }

            // as described in the class javadoc, expire stored events when CloudTrail no longer tracks them
            Set<String> expiredEvents = new HashSet<>(alreadyKnownPasswordChangeEvents);
            expiredEvents.removeAll(activeEvents);
            alreadyKnownPasswordChangeEvents.removeAll(expiredEvents);

            // store the set of non-expired events - when none left just remove the item
            if (alreadyKnownPasswordChangeEvents.isEmpty()) {
                srdRuntimeStorageUtil.removeRootUserPasswordChangeEvents(getBaseName(), accountId, logger, srContext.LOGTAG);
            } else {
                srdRuntimeStorageUtil.updateRootUserPasswordChangeEvents(getBaseName(), accountId, alreadyKnownPasswordChangeEvents, logger, srContext.LOGTAG);
            }
        }
        catch (EmoryAwsClientBuilderException e) {
            // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
            logger.info(srContext.LOGTAG + "Skipping region: global. AWS Error: " + e.getMessage());
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.RootUserPasswordChange,
                    srContext.LOGTAG, "On region: global", e);
        }
    }

    @Override
    public String getBaseName() {
        return "RootUserPasswordChange";
    }
}
