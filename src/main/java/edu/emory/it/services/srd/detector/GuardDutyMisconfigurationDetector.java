package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.guardduty.AmazonGuardDuty;
import com.amazonaws.services.guardduty.AmazonGuardDutyClient;
import com.amazonaws.services.guardduty.model.GetDetectorRequest;
import com.amazonaws.services.guardduty.model.GetDetectorResult;
import com.amazonaws.services.guardduty.model.GetMasterAccountRequest;
import com.amazonaws.services.guardduty.model.GetMasterAccountResult;
import com.amazonaws.services.guardduty.model.ListDetectorsRequest;
import com.amazonaws.services.guardduty.model.ListDetectorsResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.exceptions.SecurityRiskDetectionException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;
import org.openeai.config.AppConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Detect GuardDuty misconfiguration.<br>
 * If GuardDuty is not configured or has been suspended or disabled then you are at risk.
 * A risk is also detected if the current account is not a member to the GuardDuty Master account.
 *
 * <p>As recommended by Amazon, there should be a GuardDuty detector in every supported AWS region.
 * See the 'Important' note in
 * <a href="https://docs.aws.amazon.com/guardduty/latest/ug/guardduty_regions.html">Amazon GuardDuty Supported Regions</a></p>
 *
 * @see edu.emory.it.services.srd.remediator.GuardDutyMisconfigurationRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class GuardDutyMisconfigurationDetector extends AbstractSecurityRiskDetector {
    private String guardDutyMasterAccountId;

    @Override
    public void init(AppConfig aConfig, AWSCredentialsProvider credentialsProvider, String securityRole) throws SecurityRiskDetectionException {
        super.init(aConfig, credentialsProvider, securityRole);

        try {
            Properties properties = appConfig.getProperties(getBaseName());
            guardDutyMasterAccountId = properties.getProperty("GuardDutyMasterAccountID");
        }
        catch (EnterpriseConfigurationObjectException e) {
            throw new SecurityRiskDetectionException(e);
        }
    }

    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        // GuardDuty best practices say to check every AWS region and not just the RHEDcloud regions
        for (Region region : RegionUtils.getRegions()) {
            try {
                final AmazonGuardDuty client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonGuardDutyClient.class);

                ListDetectorsRequest listDetectorsRequest = new ListDetectorsRequest()
                        .withMaxResults(50);  // get as many as is allowed
                ListDetectorsResult listDetectorsResult;

                do {
                    listDetectorsResult = client.listDetectors(listDetectorsRequest);

                    if (listDetectorsResult.getDetectorIds().size() == 0) {
                        // at risk - detector is missing
                        addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.GuardDutyMisconfiguration,
                                "arn:aws:guardduty:" + region.getName() + ":" + accountId + ":detector/");
                    } else {
                        for (String detectorId : listDetectorsResult.getDetectorIds()) {
                            GetDetectorRequest getDetectorRequest = new GetDetectorRequest()
                                    .withDetectorId(detectorId);
                            GetDetectorResult getDetectorResult = client.getDetector(getDetectorRequest);

                            GetMasterAccountRequest getMasterAccountRequest = new GetMasterAccountRequest()
                                    .withDetectorId(detectorId);
                            GetMasterAccountResult getMasterAccountResult = client.getMasterAccount(getMasterAccountRequest);

                            boolean atRisk;

                            if (!getDetectorResult.getStatus().equals("ENABLED")) {
                                atRisk = true;  // detector is not enabled
                            } else if (getMasterAccountResult.getMaster() == null) {
                                atRisk = true;  // member account is not associated to any GuardDuty Master account
                            } else if (!guardDutyMasterAccountId.equals(getMasterAccountResult.getMaster().getAccountId())) {
                                atRisk = true;  // member account is not associated to the proper GuardDuty Master account
                            } else {
                                atRisk = false;
                            }

                            if (atRisk) {
                                List<SrdNameValuePair> properties = new ArrayList<>();
                                properties.add(new SrdNameValuePair("detectorId", detectorId));
                                properties.add(new SrdNameValuePair("status", getDetectorResult.getStatus()));
                                if (getMasterAccountResult.getMaster() != null)
                                    properties.add(new SrdNameValuePair("masterAccountId", getMasterAccountResult.getMaster().getAccountId()));

                                addDetectedSecurityRisk(detection, srContext.LOGTAG, DetectionType.GuardDutyMisconfiguration,
                                        "arn:aws:guardduty:" + region.getName() + ":" + accountId + ":detector/" + detectorId,
                                        properties.toArray(new SrdNameValuePair[0]));
                            }
                        }
                    }

                    listDetectorsRequest.setNextToken(listDetectorsResult.getNextToken());
                } while (listDetectorsResult.getNextToken() != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.GuardDutyMisconfiguration,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "GuardDutyMisconfiguration";
    }
}
