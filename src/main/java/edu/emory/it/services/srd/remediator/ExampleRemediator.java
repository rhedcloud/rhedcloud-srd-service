package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Example remediator for the test suite and Integration team monitoring.
 *
 * @see edu.emory.it.services.srd.detector.ExampleDetector the detector
 */
public class ExampleRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:fake:", srContext.LOGTAG, DetectionType.Example))
            return;
        // like arn:aws:fake:region:123456789012:example
        String[] arn = remediatorCheckResourceName(detected, 6, null, 0, srContext.LOGTAG);
        if (arn == null)
            return;

        setError(detected, srContext.LOGTAG, "Example remediator always gives an error");
    }
}
