package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeVolumesRequest;
import com.amazonaws.services.ec2.model.DescribeVolumesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Volume;
import com.amazonaws.services.ec2.model.VolumeAttachment;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Detects EBS Secondary Volumes that are unencrypted<br>
 *
 * @see edu.emory.it.services.srd.remediator.EbsUnencryptedSecondaryVolumeRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EbsUnencryptedSecondaryVolumeDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonEC2 ec2;
            try {
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);

            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.EbsUnencryptedSecondaryVolume,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Map<String, String> instanceRootVolume = retrieveRootDeviceToIgnore(ec2);
            if (instanceRootVolume == null || instanceRootVolume.keySet().isEmpty()) {
                //if there are no instances, there are no secondary volumes
                continue;
            }

            boolean done = false;
            DescribeVolumesRequest request = new DescribeVolumesRequest();
            while (!done) {
                DescribeVolumesResult response = ec2.describeVolumes(request);

                for (Volume volume : response.getVolumes()) {

                    if (volume.getState() == null || !volume.getState().equals("in-use") ||
                            volume.getEncrypted() ||
                            volume.getAttachments() == null || volume.getAttachments().isEmpty()) {
                        //not attached or already encrypted, skip
                        continue;
                    }

                    boolean hasAttachment = false;
                    for (VolumeAttachment attachment : volume.getAttachments()) {
                        if (attachment.getState().equals("attached")) {
                            hasAttachment = true;
                            break;
                        }
                    }

                    if (!hasAttachment) {
                        continue;
                    }

                    boolean isSecondaryVolumeAndInstanceIsRunning = false;
                    for (VolumeAttachment attachment : volume.getAttachments()) {
                        String rootDeviceName = instanceRootVolume.get(attachment.getInstanceId());
                        if (rootDeviceName == null || !rootDeviceName.equals(attachment.getDevice())) {
                            if (isInstanceRunning(ec2, attachment.getInstanceId())) {
                                isSecondaryVolumeAndInstanceIsRunning = true;
                            }
                        }
                    }

                    if (isSecondaryVolumeAndInstanceIsRunning) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.EbsUnencryptedSecondaryVolume,
                                "arn:aws:ec2:" + region.getName() + ":" + accountId + ":" + "volume/" + volume.getVolumeId());
                    }
                }

                request.setNextToken(response.getNextToken());

                if (response.getNextToken() == null) {
                    done = true;
                }
            }
        }
    }


    private Map<String, String> retrieveRootDeviceToIgnore(AmazonEC2 ec2) {
        boolean done = false;
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        Map<String, String> instanceRootVolume = new HashMap<>();
        while(!done) {

            DescribeInstancesResult response = ec2.describeInstances(request);
            for(Reservation reservation : response.getReservations()) {
                for(Instance instance : reservation.getInstances()) {
                    if (instance.getRootDeviceType().equalsIgnoreCase("ebs")) {
                        instanceRootVolume.put(instance.getInstanceId(), instance.getRootDeviceName());
                    }
                }
            }

            request.setNextToken(response.getNextToken());

            if(response.getNextToken() == null) {
                done = true;
            }
        }

        return instanceRootVolume;
    }

    private boolean isInstanceRunning(AmazonEC2 ec2, String instanceId) {
        DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest().withInstanceIds(instanceId);
        DescribeInstancesResult instancesResult = ec2.describeInstances(describeInstanceRequest);
        String state;
        try {
            state = instancesResult.getReservations().get(0).getInstances().get(0).getState().getName();
        } catch (NullPointerException e) {
            return false;
        }
        return "running".equals(state);
    }

    @Override
    public String getBaseName() {
        return "EbsUnencryptedSecondaryVolume";
    }
}
