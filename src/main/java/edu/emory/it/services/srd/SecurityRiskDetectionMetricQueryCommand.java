package edu.emory.it.services.srd;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetectionMetric;
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionMetricQuerySpecification;
import com.openii.openeai.commands.OpeniiRequestCommand;
import edu.emory.it.services.srd.provider.ProviderException;
import edu.emory.it.services.srd.provider.SecurityRiskDetectionMetricQueryProvider;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.RequestCommand;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.objects.resources.Error;
import org.openeai.xml.XmlDocumentReader;
import org.openeai.xml.XmlDocumentReaderException;

import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;

public class SecurityRiskDetectionMetricQueryCommand extends OpeniiRequestCommand implements RequestCommand {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(SecurityRiskDetectionMetricQueryCommand.class);

    private final Document m_responseDoc;  // the primed XML response document
    private final Document m_provideDoc;  // the primed XML response document

    private final SecurityRiskDetectionMetricQueryProvider metricQueryProvider;

    public SecurityRiskDetectionMetricQueryCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        String LOGTAG = "[SrdMetricQueryCommand] ";
        logger.info(LOGTAG + "Initializing at release " + ReleaseTag.getReleaseInfo());

        try {
            setProperties(getAppConfig().getProperties("GeneralProperties"));
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "Error retrieving 'GeneralProperties' from AppConfig. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        // Initialize response document.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        try {
            m_responseDoc = xmlReader.initializeDocument(getProperties().getProperty("responseDocumentUri"), getOutboundXmlValidation());
            if (m_responseDoc == null) {
                String errMsg = "Missing 'responseDocumentUri' property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
            m_provideDoc = xmlReader.initializeDocument(getProperties().getProperty("provideDocumentUri"), getOutboundXmlValidation());
            if (m_provideDoc == null) {
                String errMsg = "Missing 'provideDocumentUri' property in the deployment descriptor.  Can't continue.";
                logger.fatal(LOGTAG + errMsg);
                throw new InstantiationException(errMsg);
            }
        }
        catch (XmlDocumentReaderException e) {
            String errMsg = "Error initializing the primed documents. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(e.getMessage());
        }

        // Initialize a provider
        String metricQueryProviderClassName = getProperties().getProperty("metricQueryProviderClassName");
        if (metricQueryProviderClassName == null || metricQueryProviderClassName.equals("")) {
            String errMsg = "No metricQueryProviderClassName property specified. Can't continue.";
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        logger.info(LOGTAG + "metricQueryProviderClassName is: " + metricQueryProviderClassName);

        try {
            metricQueryProvider = (SecurityRiskDetectionMetricQueryProvider) Class.forName(metricQueryProviderClassName).newInstance();
            logger.info(LOGTAG + "Initializing metricQueryProviderClassName: " + metricQueryProvider.getClass().getName());
            metricQueryProvider.init();
            logger.info(LOGTAG + "Initialized metricQueryProviderClassName: " + metricQueryProvider.getClass().getName());
        }
        catch (ClassNotFoundException e) {
            String errMsg = "Class named " + metricQueryProviderClassName + " not found on the classpath"
                    + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (IllegalAccessException e) {
            String errMsg = "An error occurred getting a class for name: " + metricQueryProviderClassName
                    + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }
        catch (InstantiationException e) {
            String errMsg = "An error occurred initializing the " + metricQueryProviderClassName
                    + ". The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new InstantiationException(errMsg);
        }

        logger.info(LOGTAG + "Initializing Complete " + ReleaseTag.getReleaseInfo());
    }

    @Override
    public Message execute(int messageNumber, Message aMessage) throws CommandException {
        String LOGTAG = "[SrdMetricQueryCommand] ";
        logger.info(LOGTAG + "[execute] - Start message number " + messageNumber);

        // initialize inputs and prepare for execution
        Document inDoc;
        TextMessage msg = (TextMessage) aMessage;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
            msg.clearBody(); // clear so new content can be added later
        }
        catch (Exception e) {
            String errMsg = "Exception occurred initializing inputs. The exception is: " + e.getMessage();
            logger.fatal(LOGTAG + errMsg);
            throw new CommandException(errMsg);
        }

        // Get the ControlArea from XML document.
        Element eControlArea = getControlArea(inDoc.getRootElement());

        // Verify that the request can be handled by this command
        String msgObject = eControlArea.getAttribute("messageObject").getValue();
        String msgAction = eControlArea.getAttribute("messageAction").getValue();
        String msgType = eControlArea.getAttribute("messageType").getValue();
        String msgFullyQualifiedName = msgObject + "." + msgAction + "-" + msgType;

        if (!msgObject.equalsIgnoreCase("SecurityRiskDetectionMetric")) {
            String errDesc = "Unsupported message object: " + msgObject + ". This command expects 'SecurityRiskDetectionMetric'.";
            return replyWithApplicationError("OpenEAI-1001", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
        if (!msgAction.equalsIgnoreCase("Query")) {
            String errDesc = "Unsupported message action: " + msgAction + ". This command only supports 'Query'.";
            return replyWithApplicationError("OpenEAI-1003", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }

        // Get a configured SecurityRiskDetectionMetricQuerySpecification from AppConfig
        // and build the query object from the incoming message
        SecurityRiskDetectionMetricQuerySpecification metricQuerySpecification;
        try {
            metricQuerySpecification = (SecurityRiskDetectionMetricQuerySpecification) getAppConfig().getObjectByType(SecurityRiskDetectionMetricQuerySpecification.class.getName());

            Element child = inDoc.getRootElement().getChild("DataArea").getChild("SecurityRiskDetectionMetricQuerySpecification");
            if (child == null) {
                String errDesc = "Invalid " + msgFullyQualifiedName + " message. This command expects a SecurityRiskDetectionMetricQuerySpecification.";
                return replyWithApplicationError("OpenEAI-1004", errDesc, eControlArea, inDoc, msg, LOGTAG);
            }

            metricQuerySpecification.buildObjectFromInput(child);
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errDesc = "Error retrieving a 'SecurityRiskDetectionMetricQuerySpecification' object from AppConfig. The exception is: " + e.getMessage();
            return replyWithApplicationError("OpenEAI-1002", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
        catch (EnterpriseLayoutException e) {
            String errDesc = "Error building an object from the SecurityRiskDetectionMetricQuerySpecification element in the " + msgFullyQualifiedName + " message. The exception is: " + e.getMessage();
            return replyWithApplicationError("OpenEAI-1005", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
        // Get a configured SecurityRiskDetectionMetric from AppConfig
        // this will be the results sent in the reply
        SecurityRiskDetectionMetric metric;
        try {
            metric = (SecurityRiskDetectionMetric) getAppConfig().getObjectByType(SecurityRiskDetectionMetric.class.getName());
        }
        catch (EnterpriseConfigurationObjectException e) {
            String errDesc = "Error retrieving a 'SecurityRiskDetectionMetric' object from AppConfig. The exception is: " + e.getMessage();
            return replyWithApplicationError("OpenEAI-1008", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }

        // Handle the SecurityRiskDetectionMetric.Query-Request
        try {
            metricQueryProvider.query(metricQuerySpecification, metric);
            Document provideDoc = (Document) m_provideDoc.clone();
            provideDoc.getRootElement().getChild("DataArea").removeContent();
            provideDoc.getRootElement().getChild("DataArea").addContent((Element) metric.buildOutputFromObject());
            return getMessage(msg, buildReplyDocument(eControlArea, provideDoc));
        } catch (ProviderException e) {
            String errDesc = "Error running the SecurityRiskDetectionMetricQuerySpecification message. The exception is: " + e.getMessage();
            return replyWithApplicationError("SRD-Metric-Query-1000", errDesc, eControlArea, inDoc, msg, LOGTAG);
        } catch (EnterpriseLayoutException e) {
            String errDesc = "Error building response for the SecurityRiskDetectionMetricQuerySpecification message. The exception is: " + e.getMessage();
            return replyWithApplicationError("SRD-Metric-Query-1001", errDesc, eControlArea, inDoc, msg, LOGTAG);
        }
    }

    private Message replyWithApplicationError(String errCode, String errDesc,
                                              Element eControlArea, Document inDoc, TextMessage msg, String LOGTAG)
            throws CommandException {

        Document localResponseDoc = (Document) m_responseDoc.clone();

        logger.fatal(LOGTAG + errDesc);
        if (inDoc != null) {
            logger.fatal(LOGTAG + "Message sent in is: \n" + getMessageBody(inDoc));
        }
        List<Error> errors = new ArrayList<>();
        errors.add(buildError("application", errCode, errDesc));
        String replyContents = buildReplyDocumentWithErrors(eControlArea, localResponseDoc, errors);
        return getMessage(msg, replyContents);
    }
}
