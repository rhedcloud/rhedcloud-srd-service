package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DeleteFleetsRequest;
import com.amazonaws.services.ec2.model.DescribeFleetsRequest;
import com.amazonaws.services.ec2.model.DescribeFleetsResult;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsRequest;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsResult;
import com.amazonaws.services.ec2.model.FleetData;
import com.amazonaws.services.ec2.model.FleetLaunchTemplateConfig;
import com.amazonaws.services.ec2.model.FleetLaunchTemplateOverrides;
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateVersion;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.List;
import java.util.Set;

/**
 * Remediates EC2 Fleet configured to launch in a disallowed subnet<br>
 *
 * <p>The remediator deletes the ec2 fleet configured to launched in a disallowed subnet</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.EC2SpotScheduledInMgmtSubnetDetector the detector
 */
public class EC2FleetInMgmtSubnetRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG, DetectionType.EC2FleetInMgmtSubnet))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String fleetId = arn[5].replace("fleet/", "");
        String region = arn[3];

        final AmazonEC2 ec2;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Fleet '" + fleetId + "'", e);
            return;
        }

        DescribeFleetsRequest describeFleetsRequest = new DescribeFleetsRequest().withFleetIds(fleetId);
        DescribeFleetsResult describeFleetsResult = ec2.describeFleets(describeFleetsRequest);

        List<FleetData> fleets = describeFleetsResult.getFleets();

        if (fleets == null
                || fleets.isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Fleet '" + fleetId + "' cannot be found. It may have already been deleted.");
            return;
        }

        FleetData fleet = fleets.get(0);

        if (fleet.getFleetState().equals("deleted") || fleet.getFleetState().equals("deleted_terminating")) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Fleet '" + fleetId + "' is already deleted.");
            return;
        }

        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);
        boolean found = false;
        if (fleet.getLaunchTemplateConfigs() != null) {
            for (FleetLaunchTemplateConfig template : fleet.getLaunchTemplateConfigs()) {

                String subnetId = null;
                for (FleetLaunchTemplateOverrides overrides : template.getOverrides()) {
                    if (overrides.getSubnetId() != null) {
                        subnetId = overrides.getSubnetId();
                    }
                    if (disallowedSubnets.contains(overrides.getSubnetId())) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }
                if (subnetId == null) {
                    //check launch template
                    String templateId = template.getLaunchTemplateSpecification().getLaunchTemplateId();
                    String templateVersion = template.getLaunchTemplateSpecification().getVersion();
                    DescribeLaunchTemplateVersionsRequest describeLaunchTemplateVersionsRequest = new DescribeLaunchTemplateVersionsRequest()
                            .withLaunchTemplateId(templateId)
                            .withVersions(templateVersion);

                    DescribeLaunchTemplateVersionsResult launchTemplatesResult = ec2.describeLaunchTemplateVersions(describeLaunchTemplateVersionsRequest);

                    LaunchTemplateVersion launchTemplate = launchTemplatesResult.getLaunchTemplateVersions().get(0);

                    for (LaunchTemplateInstanceNetworkInterfaceSpecification networkInterface : launchTemplate.getLaunchTemplateData().getNetworkInterfaces()) {
                        String subnet = networkInterface.getSubnetId();
                        if (disallowedSubnets.contains(subnet)) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        break;
                    }

                }
            }
        }

        if (!found) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Fleet '" + fleetId + "' is not in a disallowed subnet.  Remediation not required.");
            return;
        }

        //remediate
        DeleteFleetsRequest deleteFleetsRequest = new DeleteFleetsRequest().withFleetIds(fleetId).withTerminateInstances(true);
        ec2.deleteFleets(deleteFleetsRequest);

        setSuccess(detected, srContext.LOGTAG, "Fleet '" + fleetId + "' has been deleted.");
    }
}
