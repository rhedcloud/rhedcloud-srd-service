package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.apigateway.AmazonApiGateway;
import com.amazonaws.services.apigateway.AmazonApiGatewayClient;
import com.amazonaws.services.apigateway.model.Deployment;
import com.amazonaws.services.apigateway.model.GetDeploymentsRequest;
import com.amazonaws.services.apigateway.model.GetDeploymentsResult;
import com.amazonaws.services.apigateway.model.GetRestApisRequest;
import com.amazonaws.services.apigateway.model.GetRestApisResult;
import com.amazonaws.services.apigateway.model.GetStagesRequest;
import com.amazonaws.services.apigateway.model.GetStagesResult;
import com.amazonaws.services.apigateway.model.RestApi;
import com.amazonaws.services.apigateway.model.Stage;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Detects API Gateway where caching is enabled<br>
 *
 * @see edu.emory.it.services.srd.remediator.APIGatewayCachingRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class APIGatewayCachingDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonApiGateway gateway;
            try {
                gateway = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonApiGatewayClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ApiGatewayCaching,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:apigateway:" + region.getName() + ":" + accountId + ":";
            checkRestApis(gateway, detection, arnPrefix, srContext.LOGTAG);
        }
    }

    private void checkRestApis(AmazonApiGateway gateway,
                               SecurityRiskDetection detection, String arnPrefix, String LOGTAG) {

        GetRestApisRequest getRestApisRequest = new GetRestApisRequest();
        GetRestApisResult getRestApisResult;

        do {
            try {
                getRestApisResult = gateway.getRestApis(getRestApisRequest);
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ApiGatewayCaching,
                        LOGTAG, "Error getting RestApis", e);
                return;
            }

            for (RestApi item : getRestApisResult.getItems()) {
                checkDeployments(gateway, item.getId(), detection, arnPrefix, LOGTAG);
            }

            getRestApisRequest.setPosition(getRestApisResult.getPosition());
        } while (getRestApisResult.getPosition() != null);
    }

    private void checkDeployments(AmazonApiGateway gateway, String restApiId,
                                  SecurityRiskDetection detection, String arnPrefix, String LOGTAG) {

        GetDeploymentsRequest getDeploymentsRequest = new GetDeploymentsRequest()
                .withRestApiId(restApiId);
        GetDeploymentsResult getDeploymentsResult;

        do {
            try {
                getDeploymentsResult = gateway.getDeployments(getDeploymentsRequest);
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ApiGatewayCaching,
                        LOGTAG, "Error getting Deployments for RestApiId " + restApiId, e);
                return;
            }

            for (Deployment item : getDeploymentsResult.getItems()) {
                checkStages(gateway, restApiId, item.getId(), detection, arnPrefix, LOGTAG);
            }

            getDeploymentsRequest.setPosition(getDeploymentsResult.getPosition());
        } while (getDeploymentsResult.getPosition() != null);
    }

    private void checkStages(AmazonApiGateway gateway, String restApiId, String deploymentId,
                             SecurityRiskDetection detection, String arnPrefix, String LOGTAG) {

        GetStagesRequest getStagesRequest = new GetStagesRequest()
                .withRestApiId(restApiId)
                .withDeploymentId(deploymentId);
        GetStagesResult getStagesResult;
        try {
            getStagesResult = gateway.getStages(getStagesRequest);
        }
        catch (Exception e) {
            setDetectionError(detection, DetectionType.ApiGatewayCaching,
                    LOGTAG, "Error getting Stages for DeploymentId " + deploymentId + " and RestApiId " + restApiId, e);
            return;
        }

        for (Stage stage : getStagesResult.getItem()) {
            if (stage.getCacheClusterEnabled()) {
                String arn = arnPrefix + restApiId + "/" + stage.getStageName();
                addDetectedSecurityRisk(detection, LOGTAG, DetectionType.ApiGatewayCaching, arn);
            }
        }
    }

    @Override
    public String getBaseName() {
        return "APIGatewayCaching";
    }
}
