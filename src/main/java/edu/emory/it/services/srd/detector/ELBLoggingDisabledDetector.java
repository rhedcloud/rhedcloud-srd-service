package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.elasticloadbalancing.model.AccessLog;
import com.amazonaws.services.elasticloadbalancing.model.LoadBalancerDescription;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancerAttributesRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancerAttributesResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersResult;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancerAttribute;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Detects ELBs where logging is disabled<br>
 *
 * <p>Detects ELBs where logging is disabled or that the S3 bucket location is different than what is specified in the configuration parameter
 *    ELBLoggingDisabled : S3LoggingBucketPath-{region name}</p>
 *
 * @see edu.emory.it.services.srd.remediator.ELBLoggingDisabledRemediator the remediator
 */
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ELBLoggingDisabledDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonElasticLoadBalancingClient elbV2;
            final com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient elbV1;
            try {
                elbV2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonElasticLoadBalancingClient.class);
                elbV1 = getClient(accountId, region.getName(), srContext.getMetricCollector(), com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ELBLoggingDisabled,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            String arnPrefix = "arn:aws:elasticloadbalancing:"+ region.getName() + ":" + accountId + ":";
            detectV2LoadBalancers(elbV2, detection, srContext.LOGTAG);
            detectV1LoadBalancers(elbV1, detection, arnPrefix, srContext.LOGTAG);
        }
    }

    private void detectV1LoadBalancers(com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient elb,
                                       SecurityRiskDetection detection, String arnPrefix, String LOGTAG) {
        boolean done = false;
        com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersRequest describeLoadBalancersRequest = new com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersRequest();

        while (!done) {
            com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancersResult describeLoadBalancersResult = elb.describeLoadBalancers(describeLoadBalancersRequest);

            for (LoadBalancerDescription loadBalancer : describeLoadBalancersResult.getLoadBalancerDescriptions()) {

                com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesRequest describeLoadBalancerAttributesRequest = new com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesRequest().withLoadBalancerName(loadBalancer.getLoadBalancerName());
                com.amazonaws.services.elasticloadbalancing.model.DescribeLoadBalancerAttributesResult describeLoadBalancerAttributesResult = elb.describeLoadBalancerAttributes(describeLoadBalancerAttributesRequest);

                AccessLog accessLog = describeLoadBalancerAttributesResult.getLoadBalancerAttributes().getAccessLog();
                boolean loggingIsCorrect =
                        accessLog != null &&
                        accessLog.getEnabled();

                if (!loggingIsCorrect) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.ELBLoggingDisabled,
                            arnPrefix + "loadbalancer/" + loadBalancer.getLoadBalancerName());
                }
            }

            describeLoadBalancersRequest.setMarker(describeLoadBalancersResult.getNextMarker());

            if (describeLoadBalancersResult.getNextMarker() == null) {
                done = true;
            }
        }
    }

    private void detectV2LoadBalancers(AmazonElasticLoadBalancingClient elb,
                                       SecurityRiskDetection detection, String LOGTAG) {
        boolean done = false;
        DescribeLoadBalancersRequest describeLoadBalancersRequest = new DescribeLoadBalancersRequest();

        while (!done) {
            DescribeLoadBalancersResult describeLoadBalancersResult = elb.describeLoadBalancers(describeLoadBalancersRequest);

            for (LoadBalancer loadBalancer : describeLoadBalancersResult.getLoadBalancers()) {

                DescribeLoadBalancerAttributesRequest describeLoadBalancerAttributesRequest = new DescribeLoadBalancerAttributesRequest().withLoadBalancerArn(loadBalancer.getLoadBalancerArn());
                DescribeLoadBalancerAttributesResult describeLoadBalancerAttributesResult = elb.describeLoadBalancerAttributes(describeLoadBalancerAttributesRequest);

                Map<String, String> attributes = describeLoadBalancerAttributesResult.getAttributes().stream().collect(Collectors.toMap(LoadBalancerAttribute::getKey, LoadBalancerAttribute::getValue));
                boolean loggingIsCorrect =
                                "true".equals(attributes.get("access_logs.s3.enabled"));

                if (!loggingIsCorrect) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.ELBLoggingDisabled,
                            loadBalancer.getLoadBalancerArn());
                }
            }

            describeLoadBalancersRequest.setMarker(describeLoadBalancersResult.getNextMarker());

            if (describeLoadBalancersResult.getNextMarker() == null) {
                done = true;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "ELBLoggingDisabled";
    }
}
