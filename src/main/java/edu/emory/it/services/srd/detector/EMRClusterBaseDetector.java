package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce;
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient;
import com.amazonaws.services.elasticmapreduce.model.Cluster;
import com.amazonaws.services.elasticmapreduce.model.ClusterSummary;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeClusterResult;
import com.amazonaws.services.elasticmapreduce.model.DescribeSecurityConfigurationRequest;
import com.amazonaws.services.elasticmapreduce.model.DescribeSecurityConfigurationResult;
import com.amazonaws.services.elasticmapreduce.model.ListClustersRequest;
import com.amazonaws.services.elasticmapreduce.model.ListClustersResult;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.io.IOException;
import java.util.Map;
import java.util.function.BiPredicate;

/**
 * Elastic Map Reduce detector utilities.
 */
public abstract class EMRClusterBaseDetector extends AbstractSecurityRiskDetector {
    /**
     * Detect risks in Elastic Map Reduce clusters.
     * If the supplied predicate tests true then the cluster is at risk.
     *
     * @param detection Security Risk Detection
     * @param accountId account ID
     * @param srContext Security Risk Context
     * @param riskType type of identified risk
     */
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext, DetectionType riskType) {

        for (Region region : getRegions()) {
            try {
                final AmazonElasticMapReduce emrClient = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonElasticMapReduceClient.class);

                String listClustersRequestMarker = null;
                do {
                    ListClustersRequest listClustersRequest = new ListClustersRequest();
                    // deal with pagination
                    if (listClustersRequestMarker != null)
                        listClustersRequest.setMarker(listClustersRequestMarker);
                    ListClustersResult listClustersResult = emrClient.listClusters(listClustersRequest);
                    listClustersRequestMarker = listClustersResult.getMarker();

                    for (ClusterSummary clusterSummary : listClustersResult.getClusters()) {
                        DescribeClusterRequest describeClusterRequest = new DescribeClusterRequest()
                                .withClusterId(clusterSummary.getId());
                        DescribeClusterResult describeClusterResult = emrClient.describeCluster(describeClusterRequest);
                        Cluster cluster = describeClusterResult.getCluster();

                        // terminated EMR clusters stay on the dashboard for a long time (up to two weeks)
                        // so just ignore them here instead of flagging them as "Remediation Unnecessary"
                        if ("TERMINATED".equals(cluster.getStatus().getState())
                                || "TERMINATED_WITH_ERRORS".equals(cluster.getStatus().getState())) {
                            continue;
                        }

                        if (getPredicate().test(emrClient, cluster)) {
                            String arn = "arn:aws:elasticmapreduce:" + region.getName() + ":" + accountId + ":cluster/" + cluster.getId();
                            addDetectedSecurityRisk(detection, srContext.LOGTAG, riskType, arn,
                                    new SrdNameValuePair("region", region.getName()),
                                    new SrdNameValuePair("clusterId", cluster.getId()),
                                    new SrdNameValuePair("clusterName", cluster.getName()),
                                    new SrdNameValuePair("clusterState", cluster.getStatus().getState()));
                        }
                    }
                } while (listClustersRequestMarker != null);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
            }
            catch (Exception e) {
                setDetectionError(detection, riskType,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }
        }
    }

    /**
     * Inspect the security configuration for an EMR cluster.
     *
     * @param emrClient EMR client
     * @param cluster EMR cluster
     * @param key security configuration setting
     * @return true is setting is true
     */
    boolean hasSecurityConfigurationSetting(AmazonElasticMapReduce emrClient, Cluster cluster, String key) {
        /*
         * see https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-create-security-configuration.html
         * the security configuration string looks like this
         * {
         *     "EncryptionConfiguration": {
         *         "EnableAtRestEncryption": true,
         *         "EnableInTransitEncryption": true,
         *         "AtRestEncryptionConfiguration": { ... },
         *         "InTransitEncryptionConfiguration": { ... }
         *     }
         * }
         */

        if (cluster.getSecurityConfiguration() == null) {
            return false;
        }

        DescribeSecurityConfigurationRequest describeSecurityConfigurationRequest = new DescribeSecurityConfigurationRequest()
                .withName(cluster.getSecurityConfiguration());
        DescribeSecurityConfigurationResult describeSecurityConfigurationResult = emrClient.describeSecurityConfiguration(describeSecurityConfigurationRequest);

        ObjectMapper om = new ObjectMapper();
        Map<String, Object> map;
        try {
            map = om.readValue(describeSecurityConfigurationResult.getSecurityConfiguration(),
                    new TypeReference<Map<String, Object>>(){});
        } catch (IOException e) {
            return false;
        }

        if (!map.containsKey("EncryptionConfiguration") || !(map.get("EncryptionConfiguration") instanceof Map))
            return false;

        map = (Map<String, Object>) map.get("EncryptionConfiguration");

        if (map == null || !map.containsKey(key))
            return false;

        Object setting = map.get(key);

        if (!(setting instanceof Boolean))
            return false;

        return (Boolean) setting;
    }

    protected abstract BiPredicate<AmazonElasticMapReduce, Cluster> getPredicate();
}
