package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsRequest;
import com.amazonaws.services.ec2.model.DescribeLaunchTemplateVersionsResult;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsResult;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest;
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateConfig;
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.LaunchTemplateVersion;
import com.amazonaws.services.ec2.model.SpotFleetLaunchSpecification;
import com.amazonaws.services.ec2.model.SpotFleetRequestConfig;
import com.amazonaws.services.ec2.model.SpotInstanceRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.List;
import java.util.Set;

/**
 * Detects Spot Instance / Block / Fleet requests configured to launch in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.EC2SpotScheduledInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class EC2SpotScheduledInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AmazonEC2 ec2;
            try {
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.EC2SpotScheduledInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);
            String arnPrefix = "arn:aws:ec2:" + region.getName() + ":" + accountId + ":spot-request/";

            checkInstanceRequests(ec2, accountId, detection, disallowedSubnets, arnPrefix, srContext.LOGTAG);
            checkFleetRequests(ec2, accountId, detection, disallowedSubnets, arnPrefix, srContext.LOGTAG);
        }
    }

    private void checkInstanceRequests(AmazonEC2 ec2, String accountId, SecurityRiskDetection detection,
                                       Set<String> disallowedSubnets, String arnPrefix, String LOGTAG) {
        DescribeSpotInstanceRequestsRequest spotInstanceRequestsRequest = new DescribeSpotInstanceRequestsRequest();
        DescribeSpotInstanceRequestsResult describeSpotInstanceRequestsResult = ec2.describeSpotInstanceRequests(spotInstanceRequestsRequest);

        List<SpotInstanceRequest> spotInstanceRequests = describeSpotInstanceRequestsResult.getSpotInstanceRequests();

        for (SpotInstanceRequest spotInstanceRequest : spotInstanceRequests) {
            if (spotInstanceRequest.getState().equals("cancelled")) {
                continue;
            }
            if (spotInstanceRequest.getLaunchSpecification() != null) {
                if (disallowedSubnets.contains(spotInstanceRequest.getLaunchSpecification().getSubnetId())) {
                    addDetectedSecurityRisk(detection, LOGTAG,
                            DetectionType.EC2SpotScheduledInMgmtSubnet,
                            arnPrefix + spotInstanceRequest.getSpotInstanceRequestId());
                    continue;
                }
                if (spotInstanceRequest.getLaunchSpecification().getNetworkInterfaces() != null) {
                    for (InstanceNetworkInterfaceSpecification networkInterface : spotInstanceRequest.getLaunchSpecification().getNetworkInterfaces()) {
                        String subnet = networkInterface.getSubnetId();
                        if (disallowedSubnets.contains(subnet)) {
                            addDetectedSecurityRisk(detection, LOGTAG,
                                    DetectionType.EC2SpotScheduledInMgmtSubnet,
                                    arnPrefix + spotInstanceRequest.getSpotInstanceRequestId());
                            break;
                        }
                    }
                }
            }

        }
    }


    private void checkFleetRequests(AmazonEC2 ec2, String accountId, SecurityRiskDetection detection,
                                    Set<String> disallowedSubnets, String arnPrefix, String LOGTAG) {
        DescribeSpotFleetRequestsRequest describeSpotFleetRequestsRequest = new DescribeSpotFleetRequestsRequest();
        boolean done = false;

        while (!done) {
            DescribeSpotFleetRequestsResult describeSpotFleetRequestsResult = ec2.describeSpotFleetRequests(describeSpotFleetRequestsRequest);
            for (SpotFleetRequestConfig requestConfig : describeSpotFleetRequestsResult.getSpotFleetRequestConfigs()) {
                if (requestConfig.getSpotFleetRequestState().equals("cancelled_terminating") || requestConfig.getSpotFleetRequestState().equals("cancelled")) {
                    continue;
                }
                boolean found = false;
                if (requestConfig.getSpotFleetRequestConfig().getLaunchSpecifications() != null) {
                    for (SpotFleetLaunchSpecification launchSpecification : requestConfig.getSpotFleetRequestConfig().getLaunchSpecifications()) {
                        if (disallowedSubnets.contains(launchSpecification.getSubnetId())) {
                            addDetectedSecurityRisk(detection, LOGTAG,
                                    DetectionType.EC2SpotFleetScheduledInMgmtSubnet,
                                    arnPrefix + requestConfig.getSpotFleetRequestId());
                            found = true;
                            break;
                        }
                    }
                }
                if (!found && requestConfig.getSpotFleetRequestConfig().getLaunchTemplateConfigs() != null) {
                    for (LaunchTemplateConfig launchTemplateConfig : requestConfig.getSpotFleetRequestConfig().getLaunchTemplateConfigs()) {

                        String templateId = launchTemplateConfig.getLaunchTemplateSpecification().getLaunchTemplateId();
                        String templateVersion = launchTemplateConfig.getLaunchTemplateSpecification().getVersion();
                        DescribeLaunchTemplateVersionsRequest describeLaunchTemplateVersionsRequest = new DescribeLaunchTemplateVersionsRequest()
                                .withLaunchTemplateId(templateId)
                                .withVersions(templateVersion);

                        DescribeLaunchTemplateVersionsResult launchTemplatesResult = ec2.describeLaunchTemplateVersions(describeLaunchTemplateVersionsRequest);

                        LaunchTemplateVersion launchTemplate = launchTemplatesResult.getLaunchTemplateVersions().get(0);

                        for (LaunchTemplateInstanceNetworkInterfaceSpecification networkInterface : launchTemplate.getLaunchTemplateData().getNetworkInterfaces()) {
                            String subnet = networkInterface.getSubnetId();
                            if (disallowedSubnets.contains(subnet)) {
                                found = true;
                                addDetectedSecurityRisk(detection, LOGTAG,
                                        DetectionType.EC2SpotFleetScheduledInMgmtSubnet,
                                        arnPrefix + requestConfig.getSpotFleetRequestId());
                                break;
                            }
                        }

                        if (found) {
                            break;
                        }
                    }
                }
            }

            describeSpotFleetRequestsRequest.setNextToken(describeSpotFleetRequestsResult.getNextToken());

            if (describeSpotFleetRequestsRequest.getNextToken() == null) {
                done = true;
            }
        }
    }

    @Override
    public String getBaseName() {
        return "EC2SpotScheduledInMgmtSubnet";
    }
}
