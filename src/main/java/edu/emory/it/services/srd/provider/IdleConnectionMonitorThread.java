package edu.emory.it.services.srd.provider;

import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

/**
 * Thread to periodically check connection pools for idle connections.
 * Very similar to com.amazonaws.http.IdleConnectionReaper from aws-java-sdk-core jar
 * and the https://www.baeldung.com/httpclient-connection-management site.
 * <p/>
 * It is made a daemon thread so that it doesn't prevent the shutdown of the JVM.
 * <p/>
 * Connections sitting around idle in the HTTP connection pool for too long will
 * eventually be terminated by the other end of the connection, and will go into
 * CLOSE_WAIT. If this happens, sockets will sit around in CLOSE_WAIT, still
 * using resources on the client side to manage that socket. Many sockets stuck
 * in CLOSE_WAIT can prevent the OS from creating new connections.
 * <p/>
 * This class closes idle connections before they can move into the CLOSE_WAIT
 * state.
 * <p/>
 * This thread is important because by default, we disable Apache HttpClient's
 * stale connection checking, so without this thread running in the background,
 * cleaning up old/inactive HTTP connections, we'd see more IO exceptions when
 * stale connections (i.e. closed on the Splunk side) are left in the connection
 * pool, and requests grab one of them to begin executing a request.
 */
class IdleConnectionMonitorThread extends Thread {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(IdleConnectionMonitorThread.class);

    private static final long WAIT_TIMEOUT = 1000;

    private final HttpClientConnectionManager connMgr;

    public IdleConnectionMonitorThread(String name, PoolingHttpClientConnectionManager connMgr) {
        super(name);
        setDaemon(true);
        this.connMgr = connMgr;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                try {
                    wait(WAIT_TIMEOUT);  // basically a sleep since nothing is going to notify us
                }
                catch (InterruptedException e) {
                    logger.info("["+ getName() + "] Shutting down due to InterruptedException");
                    break;
                }
                try {
                    connMgr.closeExpiredConnections();
                    connMgr.closeIdleConnections(30, TimeUnit.SECONDS);
                }
                catch (Throwable e) {
                    logger.warn("["+ getName() + "] Error while closing idle connections", e);
                }
            }
        }
    }
}
