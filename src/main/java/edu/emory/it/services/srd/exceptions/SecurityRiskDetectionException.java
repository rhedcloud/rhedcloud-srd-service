package edu.emory.it.services.srd.exceptions;

public class SecurityRiskDetectionException extends Exception {
    public SecurityRiskDetectionException(String message) {
        super(message);
    }

    public SecurityRiskDetectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public SecurityRiskDetectionException(Throwable cause) {
        super(cause);
    }
}
