package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.rds.AmazonRDS;
import com.amazonaws.services.rds.AmazonRDSClient;
import com.amazonaws.services.rds.model.DBCluster;
import com.amazonaws.services.rds.model.DBInstance;
import com.amazonaws.services.rds.model.DescribeDBClustersRequest;
import com.amazonaws.services.rds.model.DescribeDBClustersResult;
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest;
import com.amazonaws.services.rds.model.DescribeDBInstancesResult;
import com.amazonaws.services.rds.model.DescribeGlobalClustersRequest;
import com.amazonaws.services.rds.model.DescribeGlobalClustersResult;
import com.amazonaws.services.rds.model.GlobalCluster;
import com.amazonaws.services.rds.model.GlobalClusterMember;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdNameValuePair;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Detect unencrypted storage in RDS instances and DB clusters (Aurora, Neptune, and DocumentDB).
 *
 * @see edu.emory.it.services.srd.remediator.RdsUnencryptedDatabaseRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class RdsUnencryptedDatabaseDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        // for keeping track of RDS global clusters and their members
        Set<String> globalClusterArns = new HashSet<>();
        Set<String> globalClusterMemberArns = new HashSet<>();

        for (Region region : getRegions()) {
            AmazonRDS client;
            try {
                client = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonRDSClient.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.RdsUnencryptedDatabase,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            // global clusters - things like Aurora (MySQL)
            DescribeGlobalClustersRequest describeGlobalClustersRequest = new DescribeGlobalClustersRequest();
            DescribeGlobalClustersResult describeGlobalClustersResult;
            do {
                try {
                    describeGlobalClustersResult = client.describeGlobalClusters(describeGlobalClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsUnencryptedDatabase,
                            srContext.LOGTAG, "Error describing DB global clusters", e);
                    return;
                }

                for (GlobalCluster globalCluster : describeGlobalClustersResult.getGlobalClusters()) {
                    // because global clusters are outside of any region, we'll get the same cluster
                    // as we iterate over the regions.  keep track so we don't process them more than once.
                    if (globalClusterArns.contains(globalCluster.getGlobalClusterArn()))
                        continue;
                    globalClusterArns.add(globalCluster.getGlobalClusterArn());

                    // keep track of cluster members for use in the (regular) cluster handling
                    globalCluster.getGlobalClusterMembers().stream()
                            .map(GlobalClusterMember::getDBClusterArn)
                            .collect(Collectors.toCollection(() -> globalClusterMemberArns));

                    if (!globalCluster.getStorageEncrypted()) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.RdsUnencryptedDatabase, globalCluster.getGlobalClusterArn(),
                                new SrdNameValuePair("DBGlobalClusterIdentifier", globalCluster.getGlobalClusterIdentifier()),
                                new SrdNameValuePair("DBGlobalClusterStatus", globalCluster.getStatus()));
                    }
                }

                describeGlobalClustersRequest.setMarker(describeGlobalClustersResult.getMarker());
            } while (describeGlobalClustersResult.getMarker() != null);

            // regular databases - things like postgres, SQL server, etc
            DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest();
            DescribeDBInstancesResult describeDBInstancesResult;
            do {
                try {
                    describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsUnencryptedDatabase,
                            srContext.LOGTAG, "Error describing DB instances", e);
                    return;
                }

                for (DBInstance dbInstance : describeDBInstancesResult.getDBInstances()) {
                    /*
                     * the cluster databases (aurora, document db, neptune, etc) may have multiple instances
                     * that generally can't be operated on individually.  skip them here and
                     * inspect them below in the cluster section
                     */
                    if (dbInstance.getDBClusterIdentifier() != null)
                        continue;

                    if (!dbInstance.getStorageEncrypted()) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.RdsUnencryptedDatabase, dbInstance.getDBInstanceArn(),
                                new SrdNameValuePair("DBInstanceIdentifier", dbInstance.getDBInstanceIdentifier()),
                                new SrdNameValuePair("DBInstanceStatus", dbInstance.getDBInstanceStatus()));
                    }
                }

                describeDBInstancesRequest.setMarker(describeDBInstancesResult.getMarker());
            } while (describeDBInstancesResult.getMarker() != null);


            // cluster databases - things like aurora, document db, neptune, etc
            DescribeDBClustersRequest describeDBClustersRequest = new DescribeDBClustersRequest();
            DescribeDBClustersResult describeDBClustersResult;
            do {
                try {
                    describeDBClustersResult = client.describeDBClusters(describeDBClustersRequest);
                }
                catch (Exception e) {
                    setDetectionError(detection, DetectionType.RdsUnencryptedDatabase,
                            srContext.LOGTAG, "Error describing DB clusters", e);
                    return;
                }

                for (DBCluster dbCluster : describeDBClustersResult.getDBClusters()) {
                    // global clusters were handled above
                    if (globalClusterMemberArns.contains(dbCluster.getDBClusterArn()))
                        continue;

                    if (!dbCluster.getStorageEncrypted()) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.RdsUnencryptedDatabase, dbCluster.getDBClusterArn(),
                                new SrdNameValuePair("DBClusterIdentifier", dbCluster.getDBClusterIdentifier()),
                                new SrdNameValuePair("DBClusterStatus", dbCluster.getStatus()));
                    }
                }

                describeDBClustersRequest.setMarker(describeDBClustersResult.getMarker());
            } while (describeDBClustersResult.getMarker() != null);
        }
    }

    @Override
    public String getBaseName() {
        return "RdsUnencryptedDatabase";
    }
}
