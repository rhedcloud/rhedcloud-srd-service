package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.QueueDoesNotExistException;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.exceptions.AWSPolicyUtilNoChangeException;
import edu.emory.it.services.srd.util.AWSPolicyUtil;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Remediates SQS Queues that are externally shared<br>
 *
 * <p>Rewrites the target key's policy by removing any policy statement with Effect = "Allow" and an external principal to
 * to the owner account instead.   The rest of the policy will be kept as before.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Alert. <br> Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.SQSExternallySharedQueueDetector the detector
 */
public class SQSExternallySharedQueueRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    private static final List<String> GET_QUEUE_ATTRIBUTES_NAMES = Arrays.asList("Policy", "QueueArn");

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:sqs:", srContext.LOGTAG, DetectionType.SQSExternallySharedQueue))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String queueName = arn[5];
        String region = arn[3];

        AmazonSQS sqs;
        try {
            sqs = getClient(accountId, region, srContext.getMetricCollector(), AmazonSQSClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Queue name '" + queueName + "'", e);
            return;
        }

        GetQueueUrlResult getQueueUrlResult;
        GetQueueAttributesResult getQueueAttributesResult;
        try {
            getQueueUrlResult = sqs.getQueueUrl(queueName);
        }
        catch (QueueDoesNotExistException e) {
            setError(detected, srContext.LOGTAG, "Queue name '" + queueName + "' doesn't exist", e);
            return;
        }
        try {
            getQueueAttributesResult = sqs.getQueueAttributes(getQueueUrlResult.getQueueUrl(), GET_QUEUE_ATTRIBUTES_NAMES);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error getting queue attributes", e);
            return;
        }

        String updatedPolicy;
        try {
            Set<String> whitelistedAccounts = Collections.singleton(accountId);
            String policy = getQueueAttributesResult.getAttributes().get("Policy");
            updatedPolicy = AWSPolicyUtil.removeAllowToNonWhitelistedAccounts(whitelistedAccounts, policy);
        }
        catch (AWSPolicyUtilNoChangeException e) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Policy does not have externally shared account.  No changes have been made.");
            return;
        }

        try {
            getQueueAttributesResult.getAttributes().put("Policy", updatedPolicy);
            sqs.setQueueAttributes(getQueueUrlResult.getQueueUrl(), getQueueAttributesResult.getAttributes());

            setSuccess(detected, srContext.LOGTAG,
                    "Queue policy successfully modified to remove any statements that permits sharing the queue.");
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Error updating queue attributes", e);
        }
    }
}
