package edu.emory.it.services.srd.obsolete;

import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Analyse and report runtime metrics for SRD.
 * All data is stored in a DynamoDB table by the SRD service.
 *
 * The AWS credentials are used to read the DynamoDB and are expected in the environment.
 * The command line argument specifies the table to analyse:
 *   SecurityRiskDetectorMetricsDev, SecurityRiskDetectorMetricsTest, SecurityRiskDetectorMetricsStage, and
 *   SecurityRiskDetectorMetricsProd for production.
 */
public class SrdMetrics {
    private AmazonDynamoDB storageClient;
    private String storageTableName;
    private long executionStartFrom = Long.MIN_VALUE;
    private long executionStartTo = Long.MAX_VALUE;

    private List<Map<String, AttributeValue>> measures = new ArrayList<>();
    private List<String> detectorNames = new ArrayList<>();  // sorted
    private Set<String> accounts = new HashSet<>();
    private int maxDetectorNameLen = 0;  // for formatting the output
    private long lastRestartAt;

    private Comparator<Map<String, AttributeValue>> sortMeasures = (m1, m2) -> {
        int i = m1.get("detectorAndAccount").getS().compareTo(m2.get("detectorAndAccount").getS());
        if (i != 0)
            return i;
        Long e1 = Long.valueOf(m1.get("timeMillis").getN());
        Long e2 = Long.valueOf(m2.get("timeMillis").getN());
        return e1.compareTo(e2);
    };


    public static void main(String[] args) {
        SrdMetrics metrics = new SrdMetrics();
        metrics.init(args);
        metrics.run(args);
    }

    private void run(String[] args) {
        gatherMeasures();

        if (measures.size() == 0) {
            System.out.println("Nothing to analyse");
            System.exit(0);
        }

        // debugging dump of all retrieved metrics
        //measures.stream().forEach(System.out::println);
        /* debugging dump of non-restart metrics
        measures.stream().filter(measure -> !measure.containsKey("restart"))
                .map(measure -> {
                    LocalDateTime dt;
                    dt = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(measure.get("ttl").getN())), ZoneId.systemDefault());
                    measure.put("ttlDT", new AttributeValue().withS(dt.toString()));
                    dt = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(measure.get("timeMillis").getN())), ZoneId.systemDefault());
                    measure.put("timeMillisDT", new AttributeValue().withS(dt.toString()));
                    return measure;
                })
                .forEach(System.out::println);
        */

        System.out.println();
        LocalDateTime lastRestartDT = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastRestartAt), ZoneId.systemDefault());
        System.out.println("Analysing " + measures.size() + " data points for " + storageTableName + " from " + lastRestartDT
                + " at " + LocalDateTime.now());

        Map<String, Double> durations = new HashMap<>();
        Map<String, Double> timesBetweenSuccess = new HashMap<>();
        Map<String, Double> abortedRates = new HashMap<>();

        calculateKeyStats(durations, timesBetweenSuccess);
        calculateAbortedRates(abortedRates);

        reportErrors();
        System.out.println("\n");
        reportStats(durations, timesBetweenSuccess, abortedRates);
        System.out.println("\n");

        System.out.println("Finished analysing " + measures.size() + " data points for " + storageTableName + " from " + lastRestartDT);
    }

    private void calculateKeyStats(Map<String, Double> durations,
                                   Map<String, Double> timesBetweenSuccess) {
        String detectorAndAccount = null;
        long durationTotalSum = 0;
        long durationCountSum = 0;
        long timeBetweenSuccessTotalSum = 0;
        long timeBetweenSuccessCountSum = 0;

        for (Map<String, AttributeValue> measure : measures) {
            if (!measure.containsKey("durationTotal")) {
                continue;
            }

            String dn = measure.get("detectorAndAccount").getS();
            long durationTotal = Long.parseLong(measure.get("durationTotal").getN());
            long durationCount = Long.parseLong(measure.get("durationCount").getN());
            long timeBetweenSuccessTotal = Long.parseLong(measure.get("timeBetweenSuccessTotal").getN());
            long timeBetweenSuccessCount = Long.parseLong(measure.get("timeBetweenSuccessCount").getN());

            if (detectorAndAccount == null) {
                detectorAndAccount = dn;
                durationTotalSum = durationTotal;
                durationCountSum = durationCount;
                timeBetweenSuccessTotalSum = timeBetweenSuccessTotal;
                timeBetweenSuccessCountSum = timeBetweenSuccessCount;
            } else if (!detectorAndAccount.equals(dn)) {
                durations.put(detectorAndAccount, (durationCountSum == 0) ? 0.0 : (durationTotalSum / durationCountSum));
                timesBetweenSuccess.put(detectorAndAccount, (timeBetweenSuccessCountSum == 0) ? 0.0 : (timeBetweenSuccessTotalSum / timeBetweenSuccessCountSum));

                detectorAndAccount = dn;
                durationTotalSum = durationTotal;
                durationCountSum = durationCount;
                timeBetweenSuccessTotalSum = timeBetweenSuccessTotal;
                timeBetweenSuccessCountSum = timeBetweenSuccessCount;
            } else {
                durationTotalSum += durationTotal;
                durationCountSum += durationCount;
                timeBetweenSuccessTotalSum += timeBetweenSuccessTotal;
                timeBetweenSuccessCountSum += timeBetweenSuccessCount;
            }
        }

        // last one
        if (durationCountSum > 0)
            durations.put(detectorAndAccount, (1.0 * durationTotalSum / durationCountSum));
        if (timeBetweenSuccessCountSum > 0)
            timesBetweenSuccess.put(detectorAndAccount, (1.0 * timeBetweenSuccessTotalSum / timeBetweenSuccessCountSum));
    }

    private void reportStats(Map<String, Double> durations,
                                Map<String, Double> timesBetweenSuccess,
                                Map<String, Double> abortedRates) {
        String formatHeader     = "%" + maxDetectorNameLen + "s %10s %10s %12s\n";
        String formatDetailLine = "%" + maxDetectorNameLen + "s %10.0f %10.0f     %2.6f\n";
        String formatOverall    = "%" + maxDetectorNameLen + "s %10.0f %10.0f     %2.6f\n";
        String formatActCnt     = "%" + maxDetectorNameLen + "s %10d\n";

        System.out.printf(formatHeader, "detector /     account", "betw", "duration", "per 10min");

        double overallMeanDuration = 0;
        long overallCountDuration = 0;
        double overallMeanBetween = 0;
        long overallCountBetween = 0;
        double overallMeanAbort = 0;
        long overallCountAbort = 0;

        for (String detector : detectorNames) {
            if (!durations.containsKey(detector)
                    && !timesBetweenSuccess.containsKey(detector)
                    && !abortedRates.containsKey(detector))
                continue;

            double meanDuration = durations.containsKey(detector) ? durations.get(detector) : 0.0;
            double meanBetween = timesBetweenSuccess.containsKey(detector) ? timesBetweenSuccess.get(detector) : 0.0;
            double abortRate = abortedRates.containsKey(detector) ? abortedRates.get(detector) : 0.0;

            System.out.printf(formatDetailLine, detector, meanBetween, meanDuration, abortRate);

            // this is a bit gross but IamUnusedCredentialDetector is scheduled to run hourly
            // so don't let it skew the overall mean totals
            if (!detector.startsWith("IamUnusedCredentialDetector")) {
                if (durations.containsKey(detector)) {
                    overallMeanDuration += meanDuration;
                    overallCountDuration++;
                }
                if (timesBetweenSuccess.containsKey(detector)) {
                    overallMeanBetween += meanBetween;
                    overallCountBetween++;
                }
                if (abortedRates.containsKey(detector)) {
                    overallMeanAbort += abortRate;
                    overallCountAbort++;
                }
            }
        }

        System.out.println();
        System.out.printf(formatOverall, "overall",
                (overallCountBetween == 0)  ? 0.0 : (overallMeanBetween / overallCountBetween),
                (overallCountDuration == 0) ? 0.0 : (overallMeanDuration / overallCountDuration),
                (overallCountAbort == 0)    ? 0.0 : (overallMeanAbort / overallCountAbort));
        System.out.printf(formatActCnt, "accounts", accounts.size());
    }

    private void calculateAbortedRates(Map<String, Double> abortedRates) {
        String detectorAndAccount = "";
        Map<String, List<Map<String, AttributeValue>>> abortsOnly = new HashMap<>();
        Map<String, String> lastTimeMillis = new HashMap<>();

        for (Map<String, AttributeValue> measure : measures) {
            String dn = measure.get("detectorAndAccount").getS();
            if (measure.containsKey("aborted")) {
                abortsOnly.computeIfAbsent(dn, v -> new ArrayList<>()).add(measure);
            }
            // since measures is sorted in ascending order on timeMillis this gets the last value per detector
            lastTimeMillis.put(dn, measure.get("timeMillis").getN());
        }

        for (Map.Entry<String, List<Map<String, AttributeValue>>> entry : abortsOnly.entrySet()) {
            String dn = entry.getKey();
            List<Map<String, AttributeValue>> m = entry.getValue();
            long timeMillis = Long.parseLong(lastTimeMillis.get(dn));

            long totalRuntime = timeMillis - lastRestartAt;
            if (totalRuntime < (10 * 60 * 1000)) {
                return;  // need a minimum 10 minutes run time
            }
            double rate = m.size() / ((double) totalRuntime / 1000 / 60 / 10);
            abortedRates.put(dn, rate);
        }
    }

    private void reportErrors() {
        String detector = "";

        for (Map<String, AttributeValue> measure : measures) {
            if (measure.containsKey("errDesc")) {
                String dn = measure.get("detectorAndAccount").getS();
                if (!detector.equals(dn)) {
                    detector = dn;
                    System.out.println();
                    System.out.println(detector);
                }
                long es = Long.parseLong(measure.get("timeMillis").getN());
                LocalDateTime esdt = LocalDateTime.ofInstant(Instant.ofEpochMilli(es), ZoneId.systemDefault());
                System.out.print("    " + esdt + " - " + measure.get("errDesc").getS());
            }
        }
    }

    private void gatherMeasures() {
        ScanRequest scanRequest;
        ScanResult scanResult;

        System.out.print("Gathering restarts ");

        List<Map<String, AttributeValue>> restarts = new ArrayList<>();

        scanRequest = new ScanRequest()
                .withTableName(storageTableName)
                .withFilterExpression("attribute_exists(restart)");

        do {
            scanResult = storageClient.scan(scanRequest);

            restarts.addAll(scanResult.getItems());

            System.out.print((scanResult.getItems().size() == 0) ? "." : (scanResult.getItems().size() + "."));
            scanRequest.setExclusiveStartKey(scanResult.getLastEvaluatedKey());
        } while (scanResult.getLastEvaluatedKey() != null);

        System.out.println();

        restarts.sort(sortMeasures);
        Map<String, AttributeValue> lastRestart = restarts.get(restarts.size() - 1);
        lastRestartAt = Long.parseLong(lastRestart.get("timeMillis").getN());
        LocalDateTime lastRestartDT = LocalDateTime.ofInstant(Instant.ofEpochMilli(lastRestartAt), ZoneId.systemDefault());

        System.out.print("Gathering metrics since " + lastRestartAt + " (" + lastRestartDT + ") ");

        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":tm", new AttributeValue().withN(String.valueOf(lastRestartAt)));

        scanRequest = new ScanRequest()
                .withTableName(storageTableName)
                .withFilterExpression("attribute_not_exists(restart) AND (timeMillis >= :tm)")
                .withExpressionAttributeValues(expressionAttributeValues);

        do {
            scanResult = storageClient.scan(scanRequest);

            measures.addAll(scanResult.getItems());

            System.out.print((scanResult.getItems().size() == 0) ? "." : (scanResult.getItems().size() + "."));
            scanRequest.setExclusiveStartKey(scanResult.getLastEvaluatedKey());
        } while (scanResult.getLastEvaluatedKey() != null);

        System.out.println();

        Set<String> dns = new HashSet<>();  // use a set to make the detector names unique

        for (Map<String, AttributeValue> measure : measures) {
            String dn = measure.get("detectorAndAccount").getS();
            long timeMillis = Long.parseLong(measure.get("timeMillis").getN());

            maxDetectorNameLen = Math.max(maxDetectorNameLen, dn.length());
            dns.add(dn);
        }

        detectorNames.addAll(dns);
        detectorNames.sort(String::compareTo);

        // fabricate per-detector restart metrics
        for (String detector : detectorNames) {
            Map<String, AttributeValue> restartC = new HashMap<>();
            restartC.put("detectorAndAccount", new AttributeValue().withS(detector));
            restartC.put("nano",               new AttributeValue().withN(lastRestart.get("nano").getN()));
            restartC.put("timeMillis",         new AttributeValue().withN(lastRestart.get("timeMillis").getN()));
            restartC.put("ttl",                new AttributeValue().withN(lastRestart.get("ttl").getN()));
            restartC.put("restart",            new AttributeValue().withNULL(lastRestart.get("restart").getNULL()));
            measures.add(restartC);

            accounts.add(detector.split("/")[1]);
        }

        measures.sort(sortMeasures);
    }

    public void init(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: " + getClass().getSimpleName() + " tableName");
            System.exit(1);
        }
        storageClient = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withRegion("us-east-1")
                .build();
        storageTableName = args[0];
    }
}
