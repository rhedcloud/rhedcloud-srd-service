package edu.emory.it.services.srd;

import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.SrdMetricTypeField;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

public class SrdLogAnalysis {
    // causes the program to not store information that uses a ton of memory and only benefits debugging
    private static final boolean PRESERVE_MEMORY = true;

    private static final ZoneId DEFAULT_ZONE = ZoneId.systemDefault();

    private static final DateTimeFormatter logDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS/z");
    private static final String pLogPrefix = "^(\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d,\\d\\d\\d/...)\\s+(\\S+)\\s+\\[([^]]+)]\\s+-\\s+";

    private static final Pattern pDetectorStart = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] Starting detector execution for nano (\\d+).*$");
    private static final Pattern pDetectorFinished = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] Finished detector execution for nano (\\d+) with securityRiskDetectionId ([^ ]+) with instanceId (.*) *- *SecurityRiskDetectionGenerateCommand.*$");
    private static final Pattern pDetectorFinishedWithErrCode = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] Finished detector execution for nano (\\d+) with securityRiskDetectionId ([^ ]+) with instanceId (.*) with errCode (.*) with errDesc (.*)!.*$");
    private static final Pattern pDetectorSrdMetricDatum = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] MetricDatum (SRD_[a-zA-Z]+) ([^ ]+) (Milliseconds|Count).*$");
    private static final Pattern pDetectorAwsMetricDatum = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] MetricDatum (.*) (ClientExecuteTime) ([^ ]+) (Milliseconds|Count).*$");
    private static final Pattern pDetectorSrdStatus = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] Detection status ([^ ]*) for type .* with errDesc (.*)!.*$");
    private static final Pattern pDetectorSrrStatus = Pattern.compile(pLogPrefix + "\\[DetectorExecution]\\[([a-zA-Z0-9-]+)] Remediation status ([^ ]*) with errDesc (.*) for desc.*$");

    // try to classify any AWS errors
    private static final Pattern pAmazonServiceException = Pattern.compile("^.*\\(Service: ([^;]+);.*Error Code: ([^;]+);.*$");
    private static final Pattern pUncaughtAmazonServiceException = Pattern.compile("^.*uncaught Amazon Service exception.*\\(Service: ([^;]+);.*Error Code: ([^;]+);.*$");
    private static final Pattern pAmazonS3Exception = Pattern.compile("^.*com.amazonaws.services.s3.model.AmazonS3Exception.*\\(Service: ([^;]+);.*Error Code: ([^;]+);.*$");
    private static final Pattern pAmazonConnectTimedOutException1 = Pattern.compile("^.*Unable to execute HTTP request.*failed: connect timed out.*$");
    private static final Pattern pAmazonConnectTimedOutException2 = Pattern.compile("^.*Unable to execute HTTP request: Request did not complete before the request timeout configuration.*$");

    final List<LogEvent> allLogEvents = new ArrayList<>();
    final Set<String> allInstanceId = new HashSet<>();
    Duration totalRuntimeDuration;
    int maxJobKeyLen = 0;  // for formatting the output

    Map<String, List<LogEvent>> eventTimeline = new HashMap<>();

    final String[] cmdArgs;

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Usage: SrdLogAnalysis logfile...");
            System.exit(1);
        }

        SrdLogAnalysis ana = new SrdLogAnalysis(args);
        ana.collect();
        if (ana.allLogEvents.size() == 0) {
            System.exit(0);
        }

        ana.analyseByDetectorAndAccount();
    }

    private SrdLogAnalysis(String[] args) {
        cmdArgs = args;
    }

    private void collect() throws IOException {
        for (String filename : cmdArgs) {
            if (!filename.contains("/")) {
                System.out.println("Filename expected as tag/file (i.e., var2/log/tomcat8/logfile");
                System.exit(1);
            }

            Stream<String> stream;

            if (filename.contains(".gz")) {
                stream = GzipFiles.lines(Paths.get(filename));
            } else {
                stream = Files.lines(Paths.get(filename));
            }

            stream.forEach(l -> {
                LogEvent event = null;
                Matcher matcher;

                if (l.contains("16813579")) {
                    System.currentTimeMillis();
                }

                if ((matcher = pDetectorStart.matcher(l)).find()) {
                    event = new LogEvent(LogType.DETECTOR_START, matcher, true, filename, l);
                    event.nano = Long.parseLong(matcher.group(5));
                }
                else if ((matcher = pDetectorFinishedWithErrCode.matcher(l)).find()) {
                    event = new LogEvent(LogType.DETECTOR_FINISHED, matcher, true, filename, l);
                    event.nano = Long.parseLong(matcher.group(5));
                    event.securityRiskDetectionId = matcher.group(6);
                    event.instanceId = matcher.group(7);
                    event.errCode = matcher.group(8);
                    event.errDesc = matcher.group(9);
                    allInstanceId.add(event.instanceId);
                }
                else if ((matcher = pDetectorFinished.matcher(l)).find()) {
                    event = new LogEvent(LogType.DETECTOR_FINISHED, matcher, true, filename, l);
                    event.nano = Long.parseLong(matcher.group(5));
                    event.securityRiskDetectionId = matcher.group(6);
                    event.instanceId = matcher.group(7);
                    allInstanceId.add(event.instanceId);
                }
                else if ((matcher = pDetectorSrdMetricDatum.matcher(l)).find()) {
                    event = new LogEvent(LogType.DETECTOR_METRIC_DATUM, matcher, true, filename, l);
                    event.metricDatumType = matcher.group(5);
                    event.metricDatumValue = Double.parseDouble(matcher.group(6));
                    event.metricDatumUnits = matcher.group(7);
                }
                else if ((matcher = pDetectorAwsMetricDatum.matcher(l)).find()) {
                    event = new LogEvent(LogType.DETECTOR_METRIC_DATUM, matcher, true, filename, l);
                    event.metricDatumType = matcher.group(6);
                    event.metricDatumValue = Double.parseDouble(matcher.group(7));
                    event.metricDatumUnits = matcher.group(8);
                    event.metricDatumRequestType = matcher.group(5);
                }
                else if ((matcher = pDetectorSrdStatus.matcher(l)).find()) {
                    // for convenience, abuse DETECTOR_METRIC_DATUM, for DETECTION_TIMEOUT etc.
                    event = new LogEvent(LogType.DETECTOR_METRIC_DATUM, matcher, true, filename, l);
                    event.metricDatumType = matcher.group(5);
                    event.metricDatumUnits = matcher.group(6);
                }
                else if ((matcher = pDetectorSrrStatus.matcher(l)).find()) {
                    // for convenience, abuse DETECTOR_METRIC_DATUM, for REMEDIATION_TIMEOUT etc.
                    event = new LogEvent(LogType.DETECTOR_METRIC_DATUM, matcher, true, filename, l);
                    event.metricDatumType = matcher.group(5);
                    event.metricDatumUnits = matcher.group(6);
                }
                else {
                    //System.out.println("ELSE: " + l);
                }

                // add it to our list once we have an event
                if (event != null) {
                    allLogEvents.add(event);
                    if (event.jobKey != null) {
                        maxJobKeyLen = Math.max(maxJobKeyLen, event.jobKey.length());
                    }
                    if (allLogEvents.size() % 10_000 == 0)
                        System.out.println("Collected " + allLogEvents.size() + " events so far");
                }

                // debugging
                /*
                String debugging = "EbsUnencryptedSecondaryVolumeDetector-828157132492";
                if (event == null) {
                    System.currentTimeMillis();
                } else {
                    switch (event.logType) {
                        case DETECTOR_START:
                            if (event.jobKey != null && event.jobKey.equals(debugging))
                                System.currentTimeMillis();
                            else
                                System.currentTimeMillis();
                            break;
                        default:
                            if (event.jobKey != null) {
                                if (event.jobKey.equals(debugging))
                                    System.currentTimeMillis();
                                else
                                    System.currentTimeMillis();
                            } else {
                                System.currentTimeMillis();
                            }
                    }
                }
                */
            });
        }

        allLogEvents.sort((e1, e2) -> {
            Long l1, l2;
            int c;

            l1 = e1.dateMillis;
            l2 = e2.dateMillis;
            c = l1.compareTo(l2);
            if (c != 0) return c;

            if (e1.jobKey != null) {
                if (e2.jobKey != null) {
                    c = e1.jobKey.compareTo(e2.jobKey);
                    if (c != 0) return c;
                } else {
                    return 1; // null sorts after
                }
            } else {
                if (e2.jobKey != null) {
                    return -1; // null sorts before
                }
                // both null so considered equal so go to next sort term
            }

            // some events from a thread happen in quick succession (within the same milli)
            // but they have a specific order that is defined by the enumeration ordering
            return Integer.compare(e1.logType.ordinal(), e2.logType.ordinal());
        });

        System.out.println("Collected " + allLogEvents.size() + " events");

        if (allLogEvents.size() == 0) {
            System.exit(0);
        }
        else {
            totalRuntimeDuration = Duration.between(Instant.ofEpochMilli(allLogEvents.get(0).dateMillis),
                    Instant.ofEpochMilli(allLogEvents.get(allLogEvents.size() - 1).dateMillis));

            System.out.println("First sample at " + allLogEvents.get(0).date);
            System.out.println(" Last sample at " + allLogEvents.get(allLogEvents.size() - 1).date);
            System.out.println("Total runtime " + totalRuntimeDuration);
            System.out.println("Instance ids " + String.join(", ", allInstanceId));
        }
    }

    private void analyseByDetectorAndAccount() {
        Map<String, List<Double>> durationDetectorExecution = new HashMap<>();
        Map<String, List<Double>> durationAws = new HashMap<>();
        Map<String, AtomicLong> awsApiCallCount = new HashMap<>();
        Map<String, List<Double>> durationDetector = new HashMap<>();
        Map<String, List<Double>> durationRemediator = new HashMap<>();
        Map<String, AtomicLong> nbrIssuesDetected = new HashMap<>();
        Map<String, List<Double>> durationLocking = new HashMap<>();
        Map<String, List<Double>> durationSequenceGeneration = new HashMap<>();
        Map<String, List<Double>> durationBuildResults = new HashMap<>();
        Map<String, List<Double>> durationPublishSync = new HashMap<>();
        Map<String, List<Double>> threadPoolDelay = new HashMap<>();
        Map<String, List<Double>> waitingCountSequenceGeneration = new HashMap<>();
        Map<String, List<Double>> waitingCountDetectorLockSet = new HashMap<>();
        Map<String, List<Double>> waitingCountDetectorLockRelease = new HashMap<>();
        Map<String, List<Double>> waitingCountPublishSync = new HashMap<>();
        Map<String, List<Long>> finishToStartDelay = new HashMap<>();
        Map<String, AtomicLong> successFinishesByJobKey = new HashMap<>();
        Map<String, AtomicLong> errorFinishesByJobKey = new HashMap<>();
        Map<String, Map<String, AtomicInteger>> amazonServiceExceptionByJobKey = new HashMap<>();
        Set<String> amazonServiceExceptionCodes = new HashSet<>();

        summarizeByDetectorAndAccount(durationDetectorExecution, durationAws, awsApiCallCount, durationDetector,
                durationRemediator, nbrIssuesDetected, durationLocking, durationSequenceGeneration, durationBuildResults,
                durationPublishSync, threadPoolDelay,
                waitingCountSequenceGeneration, waitingCountDetectorLockSet, waitingCountDetectorLockRelease, waitingCountPublishSync,
                finishToStartDelay, successFinishesByJobKey, errorFinishesByJobKey,
                amazonServiceExceptionByJobKey, amazonServiceExceptionCodes);

        reportToCSV(durationDetectorExecution, durationAws, awsApiCallCount, durationDetector,
                durationRemediator, nbrIssuesDetected, durationLocking, durationSequenceGeneration, durationBuildResults,
                durationPublishSync, threadPoolDelay,
                waitingCountSequenceGeneration, waitingCountDetectorLockSet, waitingCountDetectorLockRelease, waitingCountPublishSync,
                finishToStartDelay, successFinishesByJobKey, errorFinishesByJobKey,
                amazonServiceExceptionByJobKey, amazonServiceExceptionCodes);
    }

    private void summarizeByDetectorAndAccount(
            Map<String, List<Double>> durationDetectorExecution,
            Map<String, List<Double>> durationAws,
            Map<String, AtomicLong> awsApiCallCount,
            Map<String, List<Double>> durationDetector,
            Map<String, List<Double>> durationRemediator,
            Map<String, AtomicLong> nbrIssuesDetected,
            Map<String, List<Double>> durationLocking,
            Map<String, List<Double>> durationSequenceGeneration,
            Map<String, List<Double>> durationBuildResults,
            Map<String, List<Double>> durationPublishSync,
            Map<String, List<Double>> threadPoolDelay,
            Map<String, List<Double>> waitingCountSequenceGeneration,
            Map<String, List<Double>> waitingCountDetectorLockSet,
            Map<String, List<Double>> waitingCountDetectorLockRelease,
            Map<String, List<Double>> waitingCountPublishSync,
            Map<String, List<Long>> finishToStartDelay,
            Map<String, AtomicLong> successFinishesByJobKey,
            Map<String, AtomicLong> errorFinishesByJobKey,
            Map<String, Map<String, AtomicInteger>> amazonServiceExceptionByJobKey,
            Set<String> amazonServiceExceptionCodes
    ) {
        List<LogEvent> jobTracker = new ArrayList<>();

        boolean debug = false;
        if (debug) {
            for (LogEvent event : allLogEvents) {
                if (event.jobKey != null && event.jobKey.equals("AutoscalingGroupInMgmtSubnetDetector-1142211425870")) {
                    System.out.print("DEBUG: " + event);
                    //System.out.print("  ((" + event.getLogLine() + "))");
                    System.out.println();
                }
            }
            debug = !debug;
            if (debug) {
                System.out.println("NOT SUMMARIZING");
                return;
            }
        }


        for (LogEvent event : allLogEvents) {
            LogEvent oe;  // other event

            /*
            */
            if (event.jobKey != null && event.jobKey.equals("AutoscalingGroupInMgmtSubnetDetector-1142211425870")) {
                System.out.print("DEBUG: " + event);
                //System.out.print("  ((" + event.getLogLine() + "))");
                System.out.println();
            }

            if (event.logType == LogType.DETECTOR_START) {
                for (Iterator<LogEvent> it = jobTracker.iterator(); it.hasNext(); ) {
                    oe = it.next();
                    if (oe.matchesDetectorStart(event) && oe.nano == event.nano) {
                        System.out.println("Event already in progress\nother  " + oe + "\nevent  " + event);
                        System.exit(1);
                    }
                    if (oe.matchesDetectorFinish(event)) {
                        event.lastFinishedEvent = oe;
                    }
                }
                jobTracker.add(event);
            }
            else if (event.logType == LogType.DETECTOR_METRIC_DATUM) {
                boolean found = false;
                for (Iterator<LogEvent> it = jobTracker.iterator(); it.hasNext(); ) {
                    oe = it.next();
                    if (oe.matchesDetectorStart(event)) {
                        oe.addMetricDatum(event);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    System.currentTimeMillis();
                    //System.out.println("Missing start event for DETECTOR_METRIC_DATUM " + event);
                }
            }
            else if (event.logType == LogType.DETECTOR_FINISHED) {
                // all terminal events - success or failure

                boolean withApplicationError = (event.errCode != null);
                boolean skippedSrdExempt = SecurityRiskContext.ERR_CODE_SKIPPED_SRD_EXEMPT.equals(event.errCode);
                boolean detectorAlreadyRunning = SecurityRiskContext.ERR_CODE_DETECTOR_ALREADY_RUNNING.equals(event.errCode);
                // these two mean that absolutely no work was done
                boolean detectorInvalid = SecurityRiskContext.ERR_CODE_INVALID_DETECTOR.equals(event.errCode);
                boolean missingObjectAppConfig = SecurityRiskContext.ERR_CODE_MISSING_FROM_APP_CONFIG.equals(event.errCode);

                // remove and capture the start event
                Optional<LogEvent> detectorStartLogEvent = IntStream.range(0, jobTracker.size())
                        .filter(i -> jobTracker.get(i).matchesDetectorStart(event))
                        .boxed()
                        .findFirst()
                        .map(i -> jobTracker.remove((int) i));

                if (!detectorAlreadyRunning && !skippedSrdExempt && !detectorInvalid && !missingObjectAppConfig) {
                    // replace the last finished event with this event
                    IntStream.range(0, jobTracker.size())
                            .filter(i -> jobTracker.get(i).matchesDetectorFinish(event))
                            .boxed()
                            .findFirst()
                            .map(i -> jobTracker.remove((int) i));
                    jobTracker.add(event);
                }
                if (detectorStartLogEvent.isPresent()) {
                    oe = detectorStartLogEvent.get();
                    oe.finishEvent = event;

                    if (!detectorAlreadyRunning && !skippedSrdExempt && !detectorInvalid && !missingObjectAppConfig) {
                        List<LogEvent> timeline = eventTimeline.computeIfAbsent(event.jobKey, l -> new ArrayList<>());
                        timeline.add(oe);

                        if (oe.lastFinishedEvent != null) {
                            long m = oe.dateMillis - oe.lastFinishedEvent.dateMillis;
                            finishToStartDelay.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(m);
                            if (event.jobKey != null && event.jobKey.equals("AutoscalingGroupInMgmtSubnetDetector-1142211425870")) {
                                System.out.println("DEBUG f2s: " + m + "\n" + "    st " + oe + "\n" + "    lf " + oe.lastFinishedEvent);
                            }
                        }

                        durationDetectorExecution.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getDetectorExecutionTime());
                        durationAws.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getAwsClientExecuteTime());
                        awsApiCallCount.computeIfAbsent(event.jobKey, l -> new AtomicLong()).addAndGet(oe.getAwsApiCallCount());
                        durationDetector.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getDetectorTime());
                        durationRemediator.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getRemediatorTime());
                        durationLocking.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getLockingTime());
                        durationSequenceGeneration.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getSequenceGenerationTime());
                        durationBuildResults.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getBuildResultsTime());
                        durationPublishSync.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getSyncPublishTime());
                        threadPoolDelay.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getThreadPoolDelayTime());
                        nbrIssuesDetected.computeIfAbsent(event.jobKey, l -> new AtomicLong()).addAndGet(oe.getNbrIssuesDetected());
                        waitingCountSequenceGeneration.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getSequenceGenerationWaitingCount());
                        waitingCountDetectorLockSet.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getDetectorLockSetWaitingCount());
                        waitingCountDetectorLockRelease.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getDetectorLockReleaseWaitingCount());
                        waitingCountPublishSync.computeIfAbsent(event.jobKey, l -> new ArrayList<>()).add(oe.getPublishSyncWaitingCount());

                        Matcher matcher;
                        String service;
                        String errorCode;
                        String fqError = null;

                        for (LogEvent mde : oe.metricDatumEvents) {
                            if (mde.metricDatumType.equals(DetectionStatus.DETECTION_TIMEOUT.name())) {
                                service = "AwsSDK";
                                errorCode = "ConnectTimedOut";
                                fqError = errorCode + " __ " + service;
                                break;
                            }
                            else if (mde.metricDatumType.equals(DetectionStatus.DETECTION_FAILED.name())) {
                                matcher = pAmazonServiceException.matcher(mde.metricDatumUnits);
                                if (matcher.find()) {
                                    service = matcher.group(1);
                                    errorCode = matcher.group(2);
                                    fqError = errorCode + " __ " + service;
                                }
                                else if (mde.metricDatumUnits.contains("Error getting registered VPCs from AWS Account Service")
                                        && mde.metricDatumUnits.contains("Timed out waiting for response to request")) {
                                    service = "AWS Account Service";
                                    errorCode = "Timeout";
                                    fqError = errorCode + " __ " + service;
                                }
                                else {
                                    System.out.println("DEBUG MDU: " + mde.metricDatumUnits);
                                }
                                break;
                            }
                            else if (mde.metricDatumType.equals(RemediationStatus.REMEDIATION_TIMEOUT.name())) {
                                service = "AwsSDK";
                                errorCode = "ConnectTimedOut";
                                fqError = errorCode + " __ " + service;
                                break;
                            }
                            else if (mde.metricDatumType.equals(RemediationStatus.REMEDIATION_FAILED.name())) {
                                matcher = pAmazonServiceException.matcher(mde.metricDatumUnits);
                                if (matcher.find()) {
                                    service = matcher.group(1);
                                    errorCode = matcher.group(2);
                                    fqError = errorCode + " __ " + service;
                                }
                                else {
                                    System.out.println("DEBUG MDU: " + mde.metricDatumUnits);
                                }
                                break;
                            }
                        }
                        if (fqError != null) {
                            amazonServiceExceptionByJobKey
                                    .computeIfAbsent(event.jobKey, m -> new HashMap<>())
                                    .computeIfAbsent(fqError, c -> new AtomicInteger()).incrementAndGet();
                            amazonServiceExceptionCodes.add(fqError);
                        }

                        // try to classify any AWS errors
                        if (withApplicationError) {
                            errorFinishesByJobKey.computeIfAbsent(event.jobKey, l -> new AtomicLong()).incrementAndGet();

                            if (fqError == null) {
                                if (event.errCode.equals(SecurityRiskContext.ERR_CODE_JMS_EXCEPTION)) {
                                    service = "openeai";
                                    errorCode = "Create-Sync publish error";
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                matcher = pUncaughtAmazonServiceException.matcher(event.errDesc);
                                if (matcher.find()) {
                                    service = matcher.group(1);
                                    errorCode = matcher.group(2);
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                matcher = pAmazonConnectTimedOutException1.matcher(event.errDesc);
                                if (matcher.find()) {
                                    service = "AwsSDK";
                                    errorCode = "ConnectTimedOut";
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                matcher = pAmazonConnectTimedOutException2.matcher(event.errDesc);
                                if (matcher.find()) {
                                    service = "AwsSDK";
                                    errorCode = "ConnectTimedOut";
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                matcher = pAmazonS3Exception.matcher(event.errDesc);
                                if (matcher.find()) {
                                    service = matcher.group(1);
                                    errorCode = matcher.group(2);
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                if (event.errDesc.contains("uncaught exception")
                                        && event.errDesc.contains("org.openeai.jms.consumer.commands.CommandException")) {
                                    service = "openeai";
                                    errorCode = "CommandException";
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError == null) {
                                if (event.errDesc.contains("with errCode " + SecurityRiskContext.ERR_CODE_JMS_EXCEPTION)
                                        || event.errDesc.contains("An error occurred retrieving a pub/sub producer to use to publish the Create-Sync")) {
                                    service = "openeai";
                                    errorCode = "Create-Sync publish error";
                                    fqError = errorCode + " __ " + service;
                                }
                            }
                            if (fqError != null) {
                                amazonServiceExceptionByJobKey
                                        .computeIfAbsent(event.jobKey, m -> new HashMap<>())
                                        .computeIfAbsent(fqError, c -> new AtomicInteger()).incrementAndGet();
                                amazonServiceExceptionCodes.add(fqError);
                            }
                            else {
                                System.out.println("DEBUG ERROR: " + event + "  ((" + event.getLogLine() + "))");
                            }
                        }
                        else {
                            successFinishesByJobKey.computeIfAbsent(event.jobKey, l -> new AtomicLong()).incrementAndGet();
                        }
                    }
                }
                else {
                    System.currentTimeMillis();
                    //System.out.println("Missing start event for DETECTOR_FINISHED " + event);
                }
            }
        }
        List<LogEvent> timeline = eventTimeline.get("AutoscalingGroupInMgmtSubnetDetector-1142211425870");
        if (timeline != null && !timeline.isEmpty()) {
            timeline.sort((e1, e2) -> {
                Long l1 = e1.dateMillis;
                Long l2 = e2.dateMillis;
                return l1.compareTo(l2);
            });
            // https://rdrr.io/snippets/
        /*
library(IRanges)

ir <- IRanges(c(1, 8, 14, 15, 19, 34, 40),
              width = c(12, 6, 6, 15, 6, 2, 7))
bins <- disjointBins(IRanges(start(ir), end(ir) + 1))
dat <- cbind(as.data.frame(ir), bin = bins)
library(ggplot2)
ggplot(dat) + geom_rect(aes(xmin = start, xmax = end, ymin = bin, ymax = bin + 0.9)) + theme_bw()
         */
            List<String> startTimes = new ArrayList<>();
            List<String> endTimes = new ArrayList<>();
            List<String> durations = new ArrayList<>();
            for (LogEvent event : timeline) {
                System.out.println("TIMELINE: " + event);
                startTimes.add(String.valueOf(event.dateMillis));
                endTimes.add(String.valueOf(event.finishEvent.dateMillis));
                durations.add(String.valueOf(event.finishEvent.dateMillis - event.dateMillis));
            }
            while (true) {
                boolean common = true;
                String k = startTimes.get(0).substring(0,1);
                for (int i = 0; i < startTimes.size(); i++) {
                    if (!startTimes.get(i).startsWith(k) || !endTimes.get(i).startsWith(k)) {
                        common = false;
                        break;
                    }
                }
                if (!common)
                    break;
                for (int i = 0; i < startTimes.size(); i++) {
                    startTimes.set(i, startTimes.get(i).substring(1));
                    endTimes.set(i, endTimes.get(i).substring(1));
                }
            }
            //System.out.println("ir <- IRanges(c(" + String.join(",", startTimes) + ")\n,width = c(" + String.join(",", durations) + "))");
            System.out.println("ir <- IRanges(start=c(" + String.join(",", startTimes) + ")\n,end=c(" + String.join(",", endTimes) + "))");
        }
    }

    private void reportToCSV(
            Map<String, List<Double>> durationDetectorExecution,
            Map<String, List<Double>> durationAws,
            Map<String, AtomicLong> awsApiCallCount,
            Map<String, List<Double>> durationDetector,
            Map<String, List<Double>> durationRemediator,
            Map<String, AtomicLong> nbrIssuesDetected,
            Map<String, List<Double>> durationLocking,
            Map<String, List<Double>> durationSequenceGeneration,
            Map<String, List<Double>> durationBuildResults,
            Map<String, List<Double>> durationPublishSync,
            Map<String, List<Double>> threadPoolDelay,
            Map<String, List<Double>> waitingCountSequenceGeneration,
            Map<String, List<Double>> waitingCountDetectorLockSet,
            Map<String, List<Double>> waitingCountDetectorLockRelease,
            Map<String, List<Double>> waitingCountPublishSync,
            Map<String, List<Long>> finishToStartDelay,
            Map<String, AtomicLong> successFinishesByJobKey,
            Map<String, AtomicLong> errorFinishesByJobKey,
            Map<String, Map<String, AtomicInteger>> amazonServiceExceptionByJobKey,
            Set<String> amazonServiceExceptionCodes
    ) {

        ZonedDateTime et = ZonedDateTime.ofInstant(Instant.ofEpochMilli(allLogEvents.get(allLogEvents.size() - 1).dateMillis), DEFAULT_ZONE);
        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
        String csvFilename = "SrdLogAnalysis-" + f.format(et) + ".csv";

        Path path = Paths.get(csvFilename);
        try (BufferedWriter csvWriter = Files.newBufferedWriter(path, StandardCharsets.UTF_8)) {

            // for sorted output
            List<String> jobKeys = new ArrayList<>(durationDetectorExecution.keySet());
            jobKeys.sort(Comparator.naturalOrder());

            // lots of metrics
            DoubleSummaryStatistics durationDetectorExecutionSS;
            DoubleSummaryStatistics durAwsSS;
            DoubleSummaryStatistics durLockingSS;
            DoubleSummaryStatistics durationDetectorSS;
            DoubleSummaryStatistics durationRemediatorSS;
            DoubleSummaryStatistics durationSequenceGenerationSS;
            DoubleSummaryStatistics durationBuildResultsSS;
            DoubleSummaryStatistics durationSyncPublishSS;
            DoubleSummaryStatistics threadPoolDelaySS;
            DoubleSummaryStatistics waitingCountSequenceGenerationSS;
            DoubleSummaryStatistics waitingCountDetectorLockSetSS;
            DoubleSummaryStatistics waitingCountDetectorLockReleaseSS;
            DoubleSummaryStatistics waitingCountPublishSyncSS;
            LongSummaryStatistics finishToStartDelaySS;


            csvWriter.write(
                    "detector/account" +
                    ",dur count" +
                    ",issue count" +
                    ",f2s count" +
                    ",aws api count" +
                    ",success count" +
                    ",error count" +
                    ",f2s avg" +
                    ",thr delay" +
                    ",dur avg" +
                    ",aws dur avg" +
                    ",aws dur pct" +
                    ",locking avg" +
                    ",locking pct" +
                    ",detection avg" +
                    ",remediate avg" +
                    ",seq gen avg" +
                    ",build result avg" +
                    ",publish sync avg" +
                    ",seq gen waits" +
                    ",lock set waits" +
                    ",lock rel waits" +
                    ",publish sync waits" +

                    // the details
                    ",dur min" +
                    ",dur max" +
                    ",locking min" +
                    ",locking max" +
                    ",detection min" +
                    ",detection max" +
                    ",remediate min" +
                    ",remediate max" +
                    ",seq gen min" +
                    ",seq gen max" +
                    ",build result min" +
                    ",build result max" +
                    ",publish sync min" +
                    ",publish sync max" +
                    ",f2s min" +
                    ",f2s max");
            // the errors - multiple columns for amazonServiceException
            for (String aseCode : amazonServiceExceptionCodes) {
                csvWriter.write("," + aseCode);
            }
            csvWriter.newLine();

            for (String jobKey : jobKeys) {
                if ("AccountInWrongOrganizationalUnitHipaaDetector-753445921657".equals(jobKey)) {
                    System.currentTimeMillis();
                }

                durationDetectorExecutionSS = durationDetectorExecution.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durAwsSS = durationAws.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durLockingSS = durationLocking.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durationDetectorSS = durationDetector.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durationRemediatorSS = durationRemediator.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durationSequenceGenerationSS = durationSequenceGeneration.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durationBuildResultsSS = durationBuildResults.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                durationSyncPublishSS = durationPublishSync.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                threadPoolDelaySS = threadPoolDelay.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                waitingCountSequenceGenerationSS = waitingCountSequenceGeneration.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                waitingCountDetectorLockSetSS = waitingCountDetectorLockSet.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                waitingCountDetectorLockReleaseSS = waitingCountDetectorLockRelease.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                waitingCountPublishSyncSS = waitingCountPublishSync.get(jobKey).stream().mapToDouble(x -> x).summaryStatistics();
                finishToStartDelaySS = (finishToStartDelay.get(jobKey) != null && finishToStartDelay.get(jobKey).size() > 0) ?
                        finishToStartDelay.get(jobKey).stream().mapToLong(x -> x).summaryStatistics() : new LongSummaryStatistics();

                csvWriter.write(jobKey);
                csvWriter.write(",");
                csvWriter.write(String.valueOf(durationDetectorExecutionSS.getCount()));
                csvWriter.write(",");
                csvWriter.write(nbrIssuesDetected.containsKey(jobKey) ? String.valueOf(nbrIssuesDetected.get(jobKey)) : "0");
                csvWriter.write(",");
                csvWriter.write(String.valueOf(finishToStartDelaySS.getCount()));
                csvWriter.write(",");
                csvWriter.write(awsApiCallCount.containsKey(jobKey) ? String.valueOf(awsApiCallCount.get(jobKey)) : "0");
                csvWriter.write(",");
                csvWriter.write(successFinishesByJobKey.containsKey(jobKey) ? String.valueOf(successFinishesByJobKey.get(jobKey).get()) : "0");
                csvWriter.write(",");
                csvWriter.write(errorFinishesByJobKey.containsKey(jobKey) ? String.valueOf(errorFinishesByJobKey.get(jobKey).get()) : "0");
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", finishToStartDelaySS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", threadPoolDelaySS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationDetectorExecutionSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durAwsSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.1f", durAwsSS.getAverage()/durationDetectorExecutionSS.getAverage()*100.0)); // percent
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durLockingSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.1f", durLockingSS.getAverage()/durationDetectorExecutionSS.getAverage()*100.0)); // percent
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationDetectorSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationRemediatorSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.4f", durationSequenceGenerationSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationBuildResultsSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationSyncPublishSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.2f", waitingCountSequenceGenerationSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.2f", waitingCountDetectorLockSetSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.2f", waitingCountDetectorLockReleaseSS.getAverage()));
                csvWriter.write(",");
                csvWriter.write(String.format("%.2f", waitingCountPublishSyncSS.getAverage()));
                csvWriter.write(",");

                // the details
                csvWriter.write(m2s("%.1f", durationDetectorExecutionSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationDetectorExecutionSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durLockingSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durLockingSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationDetectorSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationDetectorSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationRemediatorSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationRemediatorSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.4f", durationSequenceGenerationSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.4f", durationSequenceGenerationSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationBuildResultsSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationBuildResultsSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationSyncPublishSS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", durationSyncPublishSS.getMax()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", finishToStartDelaySS.getMin()));
                csvWriter.write(",");
                csvWriter.write(m2s("%.1f", finishToStartDelaySS.getMax()));

                // the errors - multiple columns for amazonServiceException
                amazonServiceExceptionCodes.stream().sorted().forEach(aseCode -> {
                    try {
                        Map<String, AtomicInteger> aseCounts = amazonServiceExceptionByJobKey.get(jobKey);
                        if (aseCounts != null) {
                            AtomicInteger aseCount = aseCounts.get(aseCode);
                            if (aseCount != null) {
                                csvWriter.write("," + aseCount);
                            }
                            else {
                                csvWriter.write(",");
                            }
                        }
                        else {
                            csvWriter.write(",");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

                // end of CSV line for this jobkey
                csvWriter.newLine();
            }

            csvWriter.newLine();
            csvWriter.write("\"Collected " + allLogEvents.size() + " events\"");
            csvWriter.newLine();
            csvWriter.write("\"First sample at " + allLogEvents.get(0).date + "\"");
            csvWriter.newLine();
            csvWriter.write("\"Last sample at " + allLogEvents.get(allLogEvents.size() - 1).date + "\"");
            csvWriter.newLine();
            csvWriter.write("\"Total runtime " + totalRuntimeDuration + "\"");
            csvWriter.newLine();
            csvWriter.write("\"Instance ids " + String.join(", ", allInstanceId) + "\"");
            csvWriter.newLine();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("open -a '/Applications/Microsoft Excel.app' " + path.toAbsolutePath());
    }

    // formatting functions
    private String pct(double dur, double durAws) {
        dur = m2s(dur);
        durAws = m2s(durAws);
        return String.format("%.1f / %4.1f%%", durAws, durAws/dur*100.0);
    }
    private double m2s(long millis) {
        return ((double) millis) / 1000;
    }
    private double m2s(double millis) {
        return millis / 1000;
    }
    private String m2s(String format, double millis) {
        return String.format(format, m2s(millis));
    }
    private double m2s(long count, long millis) {
        return count == 0 ? 0 : m2s(millis);
    }
    private double m2s(long count, double millis) {
        return count == 0 ? 0 : m2s(millis);
    }


    // some events from a thread happen in quick succession (within the same milli)
    // but they have a specific order that is defined by the enumeration ordering
    private enum LogType {
        DETECTOR_METRIC_DATUM, /* ordinal 0 */
        DETECTOR_FINISHED, /* ordinal 1 */
        DETECTOR_START, /* ordinal 2 */
    }

    private static class LogEvent {
        LogType logType;
        String logId;
        String logFile;
        String logLine;
        String logLevel;
        String date;
        long dateMillis;
        long nano;
        String thread;
        String jobKey;
        String detector;
        String account;
        String errCode;
        String errDesc;
        String metricDatumType;
        double metricDatumValue;
        String metricDatumUnits;
        String metricDatumRequestType;
        String securityRiskDetectionId;
        String instanceId;

        List<LogEvent> metricDatumEvents;
        LogEvent finishEvent;
        LogEvent lastFinishedEvent;


        LogEvent(LogType logType, Matcher matcher, boolean hasJobKey, String logFile, String logLine) {
            this.logType = logType;
            this.logId = logFile.substring(0, logFile.indexOf("/"));
            if (!PRESERVE_MEMORY) {
                this.logFile = logFile;
                this.logLine = logLine;
            }

            this.date = matcher.group(1);
            this.dateMillis = dateToEpoch(date);

            this.logLevel = matcher.group(2);
            this.thread = matcher.group(3);

            if (hasJobKey) {
                this.jobKey = matcher.group(4);
                String[] split = this.jobKey.split("-");
                this.detector = split[0];
                this.account = split[1];
            }
        }

        @Override
        public String toString() {
            String s = "";
            switch (logType) {
                case DETECTOR_START:
                    s += " nano " + nano;
                    break;
                case DETECTOR_FINISHED:
                    s += " nano " + nano;
                    s += " securityRiskDetectionId " + securityRiskDetectionId;
                    s += " instanceId " + instanceId;
                    if (errCode != null) s += " errCode " + errCode;
                    if (errDesc != null) s += " errDesc " + errDesc;
                    break;
                case DETECTOR_METRIC_DATUM:
                    s += " metricDatumType " + metricDatumType;
                    s += " metricDatumTime " + metricDatumValue + " " + metricDatumUnits;
                    s += " metricDatumRequestType " + metricDatumRequestType;
                    break;
                default:
                    throw new RuntimeException("missing enum " + logType);
            }
            return date + " " + thread + " " + logType + ((jobKey == null) ? "" : (" " + jobKey)) + s;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LogEvent logEvent = (LogEvent) o;
            return logId.equals(logEvent.logId) &&
                    thread.equals(logEvent.thread) &&
                    jobKey.equals(logEvent.jobKey);
        }

        @Override
        public int hashCode() {
            return Objects.hash(logId, thread, jobKey);
        }

        private long dateToEpoch(String dateStr) {
            return LocalDateTime.parse(dateStr, logDateFormatter).atZone(DEFAULT_ZONE).toInstant().toEpochMilli();
        }

        void addMetricDatum(LogEvent event) {
            if (metricDatumEvents == null) {
                metricDatumEvents = new ArrayList<>();
            }
            metricDatumEvents.add(event);
        }

        double getLockingTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> (e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorLockSet.name())
                            || e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorLockRelease.name())
                            || e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorOuShuffleLockSet.name())
                            || e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorOuShuffleLockRelease.name())))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getDetectorExecutionTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorExecution.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getAwsClientExecuteTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals("ClientExecuteTime"))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        long getAwsApiCallCount() {
            if (metricDatumEvents == null) {
                return 0;
            }
            return metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals("ClientExecuteTime"))
                    .count();
        }
        double getDetectorTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_Detect.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getRemediatorTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_Remediate.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getSequenceGenerationTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_SequenceGeneration.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getBuildResultsTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_BuildResults.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getSyncPublishTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_PublishSync.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        // elapsed time between adding job to thread pool to when it starts running
        double getThreadPoolDelayTime() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_ThreadDelay.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        long getNbrIssuesDetected() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectedIssueCount.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return Math.round(summaryStatistics.getSum());
        }
        double getSequenceGenerationWaitingCount() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_SequenceGenerationWaitingCount.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getDetectorLockSetWaitingCount() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorLockSetWaitingCount.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getDetectorLockReleaseWaitingCount() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_DetectorLockReleaseWaitingCount.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }
        double getPublishSyncWaitingCount() {
            if (metricDatumEvents == null) {
                return 0;
            }
            DoubleSummaryStatistics summaryStatistics = metricDatumEvents.stream()
                    .filter(e -> e.metricDatumType.equals(SrdMetricTypeField.SRD_PublishSyncWaitingCount.name()))
                    .mapToDouble(e -> e.metricDatumValue)
                    .summaryStatistics();
            return summaryStatistics.getSum();
        }

        boolean matchesDetectorStart(LogEvent event) {
            return logType == LogType.DETECTOR_START
                    && jobKey.equals(event.jobKey)
                    && logId.equals(event.logId)
                    && thread.equals(event.thread);
        }

        boolean matchesDetectorFinish(LogEvent event) {
            return (logType == LogType.DETECTOR_FINISHED)
                    && jobKey.equals(event.jobKey);
        }

        String getLogLine() {
            return (logLine == null) ? "logLine not available" : logLine;
        }
    }

    private static class GzipFiles {
        /**
         * Get a lazily loaded stream of lines from a gzipped file, similar to {@link Files#lines(Path)}.
         *
         * @param path The path to the gzipped file.
         * @return stream with lines.
         */
        static Stream<String> lines(Path path) {
            InputStream fileIs = null;
            BufferedInputStream bufferedIs = null;
            GZIPInputStream gzipIs = null;
            try {
                fileIs = Files.newInputStream(path);
                // Even though GZIPInputStream has a buffer it reads individual bytes
                // when processing the header, better add a buffer in-between
                bufferedIs = new BufferedInputStream(fileIs, 65535);
                gzipIs = new GZIPInputStream(bufferedIs);
            } catch (IOException e) {
                closeSafely(gzipIs);
                closeSafely(bufferedIs);
                closeSafely(fileIs);
                throw new UncheckedIOException(e);
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(gzipIs));
            return reader.lines().onClose(() -> closeSafely(reader));
        }

        private static void closeSafely(Closeable closeable) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException e) {
                    // Ignore
                }
            }
        }
    }
}
