package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.cloudwatch.AmazonCloudWatch;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.DeleteAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsRequest;
import com.amazonaws.services.cloudwatch.model.DescribeAlarmsResult;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.MetricAlarm;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Remediates CloudWatch alarms that is launched on an Emory managed instance and does not have a name that starts with RHEDcloud
 *
 * <p>The remediator deletes the CloudWatch alarm that is launched on an Emory managed instance.</p>
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * <p>Security: Enforce, delete, alert<br>
 * Preference: Strong</p>
 *
 * @see edu.emory.it.services.srd.detector.CWEventsCustomerAlarmsOnEmoryInstancesDetector the detector
 */
public class CWEventsCustomerAlarmsOnEmoryInstancesRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudwatch:", srContext.LOGTAG, DetectionType.CWEventsCustomerAlarmsOnEmoryInstances))
            return;
        String[] arn = remediatorCheckResourceName(detected, 7, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        if (!"alarm".equals(arn[5])) {
            setError(detected, srContext.LOGTAG, "Security risk input to the " + DetectionType.CWEventsCustomerAlarmsOnEmoryInstances
                    + " remediator has incorrectly formatted ARN: " + detected.getAmazonResourceName());
            return;
        }
        String alarmName = arn[6];
        String region = arn[3];

        final AmazonCloudWatch cloudWatch;
        final AmazonEC2 ec2;
        try {
            cloudWatch = getClient(accountId, region, srContext.getMetricCollector(), AmazonCloudWatchClient.class);
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Alarm '" + alarmName + "'", e);
            return;
        }

        DescribeAlarmsRequest describeAlarmsRequest = new DescribeAlarmsRequest().withAlarmNames(alarmName);
        DescribeAlarmsResult describeAlarmsResult = cloudWatch.describeAlarms(describeAlarmsRequest);

        if (describeAlarmsResult == null
                || describeAlarmsResult.getMetricAlarms() == null
                || describeAlarmsResult.getMetricAlarms().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Alarm '" + alarmName + "' cannot be found. It may have already been deleted.");
            return;
        }

        List<MetricAlarm> metricAlarms = describeAlarmsResult.getMetricAlarms();

        // check alarm has a managed instance
        List<String> instanceIds = metricAlarms.stream()
                .filter( metricAlarm -> metricAlarm.getNamespace().equals("AWS/EC2") && metricAlarm.getDimensions() != null)
                .flatMap( metricAlarm -> metricAlarm.getDimensions().stream())
                .filter( dimension -> dimension.getName().equals("InstanceId") )
                .map(Dimension::getValue)
                .collect(Collectors.toList());

        DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest().withInstanceIds(instanceIds).withFilters(new Filter("tag:RHEDcloud", Arrays.asList("True","true")));

        DescribeInstancesResult describeInstancesResult = null;
        try {
            describeInstancesResult = ec2.describeInstances(describeInstancesRequest);
        } catch (AmazonEC2Exception e) {
            if (!e.getErrorCode().equals("InvalidInstanceID.NotFound")) {
                setError(detected, srContext.LOGTAG, "Internal Error: Error retrieving ec2 instances", e);
                return;
            }
        }

        if (describeInstancesResult == null || describeInstancesResult.getReservations() == null || describeInstancesResult.getReservations().isEmpty()) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Alarm '" + alarmName + "' is not in a managed instance.  No need to remediate.");
            return;
        }


        // remediate
        DeleteAlarmsRequest deleteAlarmsRequest = new DeleteAlarmsRequest().withAlarmNames(alarmName);
        cloudWatch.deleteAlarms(deleteAlarmsRequest);

        setSuccess(detected, srContext.LOGTAG, "Alarm '" + alarmName + "' is successfully deleted.");
    }
}
