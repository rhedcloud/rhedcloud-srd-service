package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DeleteDhcpOptionsRequest;
import com.amazonaws.services.ec2.model.DeleteInternetGatewayRequest;
import com.amazonaws.services.ec2.model.DeleteNetworkAclRequest;
import com.amazonaws.services.ec2.model.DeleteRouteTableRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DeleteSubnetRequest;
import com.amazonaws.services.ec2.model.DeleteVpcRequest;
import com.amazonaws.services.ec2.model.DescribeInternetGatewaysRequest;
import com.amazonaws.services.ec2.model.DescribeInternetGatewaysResult;
import com.amazonaws.services.ec2.model.DescribeNetworkAclsRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkAclsResult;
import com.amazonaws.services.ec2.model.DescribeRouteTablesRequest;
import com.amazonaws.services.ec2.model.DescribeRouteTablesResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.DescribeSubnetsRequest;
import com.amazonaws.services.ec2.model.DescribeSubnetsResult;
import com.amazonaws.services.ec2.model.DescribeVpcsRequest;
import com.amazonaws.services.ec2.model.DescribeVpcsResult;
import com.amazonaws.services.ec2.model.DetachInternetGatewayRequest;
import com.amazonaws.services.ec2.model.DisassociateRouteTableRequest;
import com.amazonaws.services.ec2.model.Filter;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

/**
 * Delete VPCs that are not registered with AWS Account Service.
 *
 * <br><br>
 * as outlined here: https://aws.amazon.com/premiumsupport/knowledge-center/troubleshoot-dependency-error-delete-vpc/
 * we'll try to find and delete all dependent network resources before deleting the VPC itself.
 *
 * generally, we will log errors when the delete of dependent network resources fails, and
 * the success or failure of the remediation is based on whether the VPC itself could be deleted.
 *
 * please note that we don't handle all networking resources because we're focused on the VPC
 * that was created for us by AWS and it is in a minimal state.
 *
 * <br><br>
 * this forum post gave more information about the order of deletion for VPC resource:
 * https://forums.aws.amazon.com/thread.jspa?threadID=92407
 *
 * <ul>
 *     <li>Terminate all instances in your VPC</li>
 *     <li>Delete all ENI's associated with subnets within your VPC</li>
 *     <li>Detach all Internet and Virtual Private Gateways (you can then delete them and any VPN connections,
 *         but that's not required to delete the VPC object)</li>
 *     <li>Disassociate all route tables from all the subnets in your VPC</li>
 *     <li>Delete all route tables other than the "Main" table</li>
 *     <li>Disassociate all Network ACL's from all the subnets in your VPC</li>
 *     <li>Delete all Network ACL's other than the Default one</li>
 *     <li>Delete all Security groups other than the Default one (note: if one group has a rule that references another,
 *         you have to delete that rule before you can delete the other security group)</li>
 *     <li>Delete all subnets</li>
 *     <li>Delete your VPC</li>
 *     <li>Delete any DHCP Option Sets that had been used by the VPC</li>
 * </ul>
 *
 * There's some flexibility in that list. For example, Instances, Gateways, Route Tables, NACLs and Sec. Groups
 * can be done in any order.
 *
 * <p>For both compliance class accounts (HIPAA and Standard)</p>
 *
 * @see edu.emory.it.services.srd.detector.UnregisteredVpcDetector the detector
 */
public class UnregisteredVpcRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {

    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:ec2:", srContext.LOGTAG, DetectionType.UnregisteredVpc))
            return;
        // like arn:aws:ec2:us-east-1:123456789012:vpc/vpc-9a9a9a9
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String[] vpcBits = arn[5].split("/");
        if (vpcBits.length != 2) {
            setError(detected, srContext.LOGTAG, "Security risk input has incorrectly formatted ARN (vpc): " + detected.getAmazonResourceName());
            return;
        }

        String region = arn[3];
        String vpcId = vpcBits[1];

        AmazonEC2 ec2;
        try {
            ec2 = getClient(accountId, region, srContext.getMetricCollector(), AmazonEC2Client.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Failed to remediate", e);
            return;
        }

        // a common filter for network resource searches and diagnostic info in case of errors
        Filter vpcIdFilter = new Filter().withName("vpc-id").withValues(vpcId);
        StringBuilder dependencyText = new StringBuilder(2048);


        // Internet Gateways
        DescribeInternetGatewaysRequest describeInternetGatewaysRequest = new DescribeInternetGatewaysRequest()
                .withFilters(new Filter().withName("attachment.vpc-id").withValues(vpcId));
        DescribeInternetGatewaysResult describeInternetGatewaysResult = ec2.describeInternetGateways(describeInternetGatewaysRequest);
        describeInternetGatewaysResult.getInternetGateways().forEach(nr -> {
            try {
                DetachInternetGatewayRequest detachInternetGatewayRequest = new DetachInternetGatewayRequest()
                        .withInternetGatewayId(nr.getInternetGatewayId())
                        .withVpcId(vpcId);
                ec2.detachInternetGateway(detachInternetGatewayRequest);
                dependencyText.append(" -- detach internet gateway: ").append(nr);
            } catch (AmazonEC2Exception e) {
                dependencyText.append(" -- unable to detach internet gateway: ").append(nr).append(". AWS Error: ").append(e.getMessage());
            }
            try {
                DeleteInternetGatewayRequest deleteInternetGatewayRequest = new DeleteInternetGatewayRequest()
                        .withInternetGatewayId(nr.getInternetGatewayId());
                ec2.deleteInternetGateway(deleteInternetGatewayRequest);
                dependencyText.append(" -- delete internet gateway: ").append(nr);
            } catch (AmazonEC2Exception e) {
                dependencyText.append(" -- unable to delete internet gateway: ").append(nr).append(". AWS Error: ").append(e.getMessage());
            }
        });

        // Subnets
        DescribeSubnetsRequest describeSubnetsRequest = new DescribeSubnetsRequest()
                .withFilters(vpcIdFilter);
        DescribeSubnetsResult describeSubnetsResult = ec2.describeSubnets(describeSubnetsRequest);
        describeSubnetsResult.getSubnets().forEach(nr -> {
            try {
                DeleteSubnetRequest deleteSubnetRequest = new DeleteSubnetRequest()
                        .withSubnetId(nr.getSubnetId());
                ec2.deleteSubnet(deleteSubnetRequest);
                dependencyText.append(" -- delete subnet: ").append(nr);
            } catch (AmazonEC2Exception e) {
                dependencyText.append(" -- unable to delete subnet: ").append(nr).append(". AWS Error: ").append(e.getMessage());
            }
        });

        // Network Interfaces
        /*
        DescribeNetworkInterfacesRequest describeNetworkInterfacesRequest = new DescribeNetworkInterfacesRequest()
                .withFilters(vpcIdFilter);
        DescribeNetworkInterfacesResult describeNetworkInterfacesResult;
        do {
            describeNetworkInterfacesResult = ec2.describeNetworkInterfaces(describeNetworkInterfacesRequest);

            describeNetworkInterfacesResult.getNetworkInterfaces().forEach(nr -> {
                try {
                    DetachNetworkInterfaceRequest detachNetworkInterfaceRequest = new DetachNetworkInterfaceRequest()
                            .withAttachmentId(nr.getAttachment().getAttachmentId())
                            .withForce(Boolean.TRUE);
                    ec2.detachNetworkInterface(detachNetworkInterfaceRequest);
                } catch (AmazonEC2Exception e) {
                    logger.error(LOGTAG + "Unable to detach network interface: " + nr + ". AWS Error: " + e.getMessage() + diagInfo);
                }

                try {
                    DeleteNetworkInterfaceRequest deleteNetworkInterfaceRequest = new DeleteNetworkInterfaceRequest()
                            .withNetworkInterfaceId(nr.getNetworkInterfaceId());
                    ec2.deleteNetworkInterface(deleteNetworkInterfaceRequest);
                } catch (AmazonEC2Exception e) {
                    logger.error(LOGTAG + "Unable to delete network interface: " + nr + ". AWS Error: " + e.getMessage() + diagInfo);
                }
            });

            describeNetworkInterfacesRequest.setNextToken(describeNetworkInterfacesResult.getNextToken());
        } while (describeNetworkInterfacesResult.getNextToken() != null);
        */

        // Network ACLs - delete the non-default ones
        DescribeNetworkAclsRequest describeNetworkAclsRequest = new DescribeNetworkAclsRequest()
                .withFilters(vpcIdFilter);
        DescribeNetworkAclsResult describeNetworkAclsResult = ec2.describeNetworkAcls(describeNetworkAclsRequest);
        describeNetworkAclsResult.getNetworkAcls().stream().filter(nr -> !nr.isDefault()).forEach(nr -> {
            try {
                DeleteNetworkAclRequest deleteNetworkAclRequest = new DeleteNetworkAclRequest()
                        .withNetworkAclId(nr.getNetworkAclId());
                ec2.deleteNetworkAcl(deleteNetworkAclRequest);
                dependencyText.append(" -- delete network ACL: ").append(nr);
            } catch (AmazonEC2Exception e) {
                dependencyText.append(" -- unable to delete network ACL: ").append(nr).append(". AWS Error: ").append(e.getMessage());
            }
        });

        // Route Tables
        DescribeRouteTablesRequest describeRouteTablesRequest = new DescribeRouteTablesRequest()
                .withFilters(vpcIdFilter);
        DescribeRouteTablesResult describeRouteTablesResult;
        do {
            describeRouteTablesResult = ec2.describeRouteTables(describeRouteTablesRequest);

            describeRouteTablesResult.getRouteTables().forEach(nr -> {
                nr.getAssociations().stream().filter(ass -> !ass.isMain()).forEach(ass -> {
                    try {
                        DisassociateRouteTableRequest disassociateRouteTableRequest = new DisassociateRouteTableRequest()
                                .withAssociationId(ass.getRouteTableAssociationId());
                        ec2.disassociateRouteTable(disassociateRouteTableRequest);
                        dependencyText.append(" -- disassociate route table: ").append(nr);
                    } catch (AmazonEC2Exception e) {
                        dependencyText.append(" -- unable to disassociate route table: ").append(nr).append(". AWS Error: ").append(e.getMessage());
                    }
                });

                try {
                    DeleteRouteTableRequest deleteRouteTableRequest = new DeleteRouteTableRequest()
                            .withRouteTableId(nr.getRouteTableId());
                    ec2.deleteRouteTable(deleteRouteTableRequest);
                    dependencyText.append(" -- delete route table: ").append(nr);
                } catch (AmazonEC2Exception e) {
                    dependencyText.append(" -- unable to delete route table: ").append(nr).append(". AWS Error: ").append(e.getMessage());
                }
            });

            describeRouteTablesRequest.setNextToken(describeRouteTablesResult.getNextToken());
        } while (describeRouteTablesResult.getNextToken() != null);

        // VPC Endpoints
        /*
        DescribeVpcEndpointsRequest describeVpcEndpointsRequest = new DescribeVpcEndpointsRequest()
                .withFilters(vpcIdFilter);
        DescribeVpcEndpointsResult describeVpcEndpointsResult;
        do {
            describeVpcEndpointsResult = ec2.describeVpcEndpoints(describeVpcEndpointsRequest);

            describeVpcEndpointsResult.getVpcEndpoints().forEach(nr -> {
                DeleteVpcEndpointsRequest deleteVpcEndpointsRequest = new DeleteVpcEndpointsRequest()
                        .withVpcEndpointIds(nr.getVpcEndpointId());
                try {
                    ec2.deleteVpcEndpoints(deleteVpcEndpointsRequest);
                } catch (AmazonEC2Exception e) {
                    logger.error(LOGTAG + "Unable to delete VPC endpoint: " + nr + ". AWS Error: " + e.getMessage() + diagInfo);
                }
            });

            describeVpcEndpointsRequest.setNextToken(describeVpcEndpointsResult.getNextToken());
        } while (describeVpcEndpointsResult.getNextToken() != null);
        */

        // VPC Peering Connections
        /*
        DescribeVpcPeeringConnectionsRequest describeVpcPeeringConnectionsRequest = new DescribeVpcPeeringConnectionsRequest()
                .withFilters(new Filter().withName("accepter-vpc-info.vpc-id").withValues(vpcId),
                        new Filter().withName("requester-vpc-info.vpc-id").withValues(vpcId));
        DescribeVpcPeeringConnectionsResult describeVpcPeeringConnectionsResult = ec2.describeVpcPeeringConnections(describeVpcPeeringConnectionsRequest);
        describeVpcPeeringConnectionsResult.getVpcPeeringConnections().forEach(nr -> {
            DeleteVpcPeeringConnectionRequest deleteVpcPeeringConnectionRequest = new DeleteVpcPeeringConnectionRequest()
                    .withVpcPeeringConnectionId(nr.getVpcPeeringConnectionId());
            try {
                ec2.deleteVpcPeeringConnection(deleteVpcPeeringConnectionRequest);
            } catch (AmazonEC2Exception e) {
                logger.error(LOGTAG + "Unable to delete VPC peering connection: " + nr + ". AWS Error: " + e.getMessage() + diagInfo);
            }
        });
        */

        // NAT Gateways
        /*
        DescribeNatGatewaysRequest describeNatGatewaysRequest = new DescribeNatGatewaysRequest()
                .withFilter(vpcIdFilter);
        DescribeNatGatewaysResult describeNatGatewaysResult;
        do {
            describeNatGatewaysResult = ec2.describeNatGateways(describeNatGatewaysRequest);

            describeNatGatewaysResult.getNatGateways().forEach(nr -> {
                DeleteNatGatewayRequest deleteNatGatewayRequest = new DeleteNatGatewayRequest()
                        .withNatGatewayId(nr.getNatGatewayId());
                try {
                    ec2.deleteNatGateway(deleteNatGatewayRequest);
                } catch (AmazonEC2Exception e) {
                    logger.error(LOGTAG + "Unable to delete NAT gateway: " + nr + ". AWS Error: " + e.getMessage() + diagInfo);
                }
            });

            describeNatGatewaysRequest.setNextToken(describeNatGatewaysResult.getNextToken());
        } while (describeNatGatewaysResult.getNextToken() != null);
        */

        // Security Groups
        DescribeSecurityGroupsRequest describeSecurityGroupsRequest = new DescribeSecurityGroupsRequest()
                .withFilters(vpcIdFilter);
        DescribeSecurityGroupsResult describeSecurityGroupsResult;
        do {
            describeSecurityGroupsResult = ec2.describeSecurityGroups(describeSecurityGroupsRequest);

            describeSecurityGroupsResult.getSecurityGroups().stream().filter(nr -> !"default".equals(nr.getGroupName())).forEach(nr -> {
                DeleteSecurityGroupRequest deleteSecurityGroupRequest = new DeleteSecurityGroupRequest()
                        .withGroupId(nr.getGroupId());
                try {
                    ec2.deleteSecurityGroup(deleteSecurityGroupRequest);
                    dependencyText.append(" -- delete security group: ").append(nr);
                } catch (AmazonEC2Exception e) {
                    dependencyText.append(" -- unable to delete security group: ").append(nr).append(". AWS Error: ").append(e.getMessage());
                }
            });

            describeSecurityGroupsRequest.setNextToken(describeSecurityGroupsResult.getNextToken());
        } while (describeSecurityGroupsResult.getNextToken() != null);

        // DHCP Options Sets
        // can only be deleted after the VPC is deleted, so get the DHCP options set ID and delete below
        DescribeVpcsRequest describeVpcsRequest = new DescribeVpcsRequest()
                .withVpcIds(vpcId);
        DescribeVpcsResult describeVpcsResult = ec2.describeVpcs(describeVpcsRequest);

        // and finally, the VPC itself
        try {
            ec2.deleteVpc(new DeleteVpcRequest(vpcId));
            dependencyText.append(" -- delete VPC: ").append(describeVpcsResult);

            // now that the VPC is gone we can delete the DHCP options set
            // but failure here doesn't change the overall success status though it is logged
            describeVpcsResult.getVpcs().forEach(nr -> {
                try {
                    DeleteDhcpOptionsRequest deleteDhcpOptionsRequest = new DeleteDhcpOptionsRequest()
                            .withDhcpOptionsId(nr.getDhcpOptionsId());
                    ec2.deleteDhcpOptions(deleteDhcpOptionsRequest);
                    dependencyText.append(" -- delete DHCP options set: ").append(nr);
                } catch (AmazonEC2Exception e) {
                    dependencyText.append(" -- unable to delete DHCP options set: ").append(nr).append(". AWS Error: ").append(e.getMessage());
                }
            });

            setSuccess(detected, srContext.LOGTAG, "VPC " + detected.getAmazonResourceName() + " has been deleted"
                    + ". With dependencies " + dependencyText);
        } catch (AmazonEC2Exception e) {
            dependencyText.append(" -- unable to delete VPC").append(". AWS Error: ").append(e.getMessage());
            setError(detected, srContext.LOGTAG, "Unable to delete VPC " + detected.getAmazonResourceName()
                    + ". With dependencies " + dependencyText, e);
        }
    }
}
