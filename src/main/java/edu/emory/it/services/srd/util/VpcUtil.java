package edu.emory.it.services.srd.util;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VpcUtil {
    public static final Predicate<Tag> DISALLOWED_SUBNETS_TAG_PREDICATE = tag ->
            tag.getKey().equals("Name") &&
                    (tag.getValue().startsWith("MGMT1")
                  || tag.getValue().startsWith("MGMT2")
                  || tag.getValue().equals("Attachment Tier AZ-a")
                  || tag.getValue().equals("Attachment Tier AZ-b"));

    /**
     * Get the subnet IDs that a resource should not use.
     * These are MGMT subnets (AWS @ Emory) and Attachment Tier subnets (CIMP)
     * See https://bitbucket.org/rhedcloud/rhedcloud-aws-vpc-type1-cfn/src/master/rhedcloud-aws-vpc-type1-cfn.json
     *  for the CloudFormation script that creates the MGMT subnets.
     *
     * @param ec2Client AmazonEC2 client
     * @return set of subnet IDs
     */
    public static Set<String> getDisallowedSubnets(AmazonEC2 ec2Client) {
        List<Subnet> allSubnets = ec2Client.describeSubnets().getSubnets();
        Stream<Subnet> disallowedSubnets = allSubnets.stream()
                .filter(subnet -> subnet.getTags().stream().anyMatch(DISALLOWED_SUBNETS_TAG_PREDICATE));
        return disallowedSubnets
                .map(Subnet::getSubnetId)
                .collect(Collectors.toSet());
    }
}
