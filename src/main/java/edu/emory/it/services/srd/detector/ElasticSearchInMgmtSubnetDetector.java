package edu.emory.it.services.srd.detector;

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.elasticsearch.AWSElasticsearch;
import com.amazonaws.services.elasticsearch.AWSElasticsearchClient;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainsRequest;
import com.amazonaws.services.elasticsearch.model.DescribeElasticsearchDomainsResult;
import com.amazonaws.services.elasticsearch.model.DomainInfo;
import com.amazonaws.services.elasticsearch.model.ElasticsearchDomainStatus;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesRequest;
import com.amazonaws.services.elasticsearch.model.ListDomainNamesResult;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass;
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass;
import edu.emory.it.services.srd.annotations.StandardComplianceClass;
import edu.emory.it.services.srd.exceptions.EmoryAwsClientBuilderException;
import edu.emory.it.services.srd.util.SecurityRiskContext;
import edu.emory.it.services.srd.util.VpcUtil;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Detects Elasticsearch domains configured to launch in a disallowed subnet<br>
 *
 * @see edu.emory.it.services.srd.remediator.ElasticSearchInMgmtSubnetRemediator the remediator
 */
@StandardComplianceClass
@HIPAAComplianceClass
@EnhancedSecurityComplianceClass
public class ElasticSearchInMgmtSubnetDetector extends AbstractSecurityRiskDetector {
    @Override
    public void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {
        for (Region region : getRegions()) {
            final AWSElasticsearch elasticsearch;
            final AmazonEC2 ec2;
            try {
                elasticsearch = getClient(accountId, region.getName(), srContext.getMetricCollector(), AWSElasticsearchClient.class);
                ec2 = getClient(accountId, region.getName(), srContext.getMetricCollector(), AmazonEC2Client.class);
            }
            catch (EmoryAwsClientBuilderException e) {
                // The amazon key we have doesn't work in some regions (like us-gov-east-1), so ignoring them
                logger.info(srContext.LOGTAG + "Skipping region: " + region.getName() + ". AWS Error: " + e.getMessage());
                continue;
            }
            catch (Exception e) {
                setDetectionError(detection, DetectionType.ElasticSearchInMgmtSubnet,
                        srContext.LOGTAG, "On region: " + region.getName(), e);
                return;
            }

            Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2);

            String arnPrefix = "arn:aws:es:" + region.getName() + ":" + accountId + ":domain/";

            ListDomainNamesResult listDomainNamesResult = elasticsearch.listDomainNames(new ListDomainNamesRequest());

            DescribeElasticsearchDomainsRequest describeElasticsearchDomainsRequest = new DescribeElasticsearchDomainsRequest().withDomainNames(listDomainNamesResult.getDomainNames().stream().map(DomainInfo::getDomainName).collect(Collectors.toList()));
            DescribeElasticsearchDomainsResult describeElasticsearchDomainsResult = elasticsearch.describeElasticsearchDomains(describeElasticsearchDomainsRequest);

            for (ElasticsearchDomainStatus domainInfo : describeElasticsearchDomainsResult.getDomainStatusList()) {
                if (Boolean.TRUE.equals(domainInfo.getDeleted())) {
                    continue;
                }

                if (domainInfo.getVPCOptions() == null || domainInfo.getVPCOptions().getSubnetIds() == null) {
                    continue;
                }
                for (String subnet : domainInfo.getVPCOptions().getSubnetIds()) {
                    if (disallowedSubnets.contains(subnet)) {
                        addDetectedSecurityRisk(detection, srContext.LOGTAG,
                                DetectionType.ElasticSearchInMgmtSubnet, arnPrefix + domainInfo.getDomainName());
                        break;
                    }
                }
            }
        }
    }

    @Override
    public String getBaseName() {
        return "ElasticSearchInMgmtSubnet";
    }
}
