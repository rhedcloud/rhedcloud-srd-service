package edu.emory.it.services.srd.remediator;

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk;
import com.amazonaws.services.cloudfront.AmazonCloudFront;
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient;
import com.amazonaws.services.cloudfront.model.CacheBehavior;
import com.amazonaws.services.cloudfront.model.DefaultCacheBehavior;
import com.amazonaws.services.cloudfront.model.DistributionConfig;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigRequest;
import com.amazonaws.services.cloudfront.model.GetDistributionConfigResult;
import com.amazonaws.services.cloudfront.model.Origin;
import com.amazonaws.services.cloudfront.model.UpdateDistributionRequest;
import edu.emory.it.services.srd.DetectionType;
import edu.emory.it.services.srd.util.SecurityRiskContext;

import java.util.HashMap;
import java.util.Map;

/**
 * Remediates all distributions with 'TrustedSigners' disabled and 'Cookies' not set to 'none' with s3 origin<br>
 * <p>The remediator sets cookies value to none</p>
 *
 * @see edu.emory.it.services.srd.detector.CloudFrontUnsignedURLUnfoundCookiesDetector the detector
 */
public class CloudFrontUnsignedURLUnfoundCookiesRemediator extends AbstractSecurityRiskRemediator implements SecurityRiskRemediator {
    @Override
    public void remediate(String accountId, DetectedSecurityRisk detected, SecurityRiskContext srContext) {
        if (remediatorPreConditionFailed(detected, "arn:aws:cloudfront:", srContext.LOGTAG, DetectionType.CloudFrontUnsignedURLUnfoundCookies))
            return;
        String[] arn = remediatorCheckResourceName(detected, 6, accountId, 4, srContext.LOGTAG);
        if (arn == null)
            return;

        String distributionPath = arn[5];
        String distributionId = distributionPath.substring(distributionPath.lastIndexOf("/") + 1);

        final AmazonCloudFront cloudFront;
        try {
            cloudFront = getClient(accountId, srContext.getMetricCollector(), AmazonCloudFrontClient.class);
        }
        catch (Exception e) {
            setError(detected, srContext.LOGTAG, "Distribution with id '" + distributionId + "'", e);
            return;
        }

        GetDistributionConfigRequest request = new GetDistributionConfigRequest().withId(distributionId);
        GetDistributionConfigResult result = cloudFront.getDistributionConfig(request);

        DistributionConfig distributionConfig = result.getDistributionConfig();

        boolean hasChange = false;
        Map<String, Object> originIds = new HashMap<>();
        DefaultCacheBehavior defaultCacheBehavior = distributionConfig.getDefaultCacheBehavior();
        if (!Boolean.TRUE.equals(defaultCacheBehavior.getTrustedSigners().isEnabled())
                && !"none".equals(defaultCacheBehavior.getForwardedValues().getCookies().getForward())) {
            originIds.put(defaultCacheBehavior.getTargetOriginId(), defaultCacheBehavior);
        }

        for (CacheBehavior cacheBehavior : distributionConfig.getCacheBehaviors().getItems()) {
            if (!Boolean.TRUE.equals(cacheBehavior.getTrustedSigners().isEnabled())
                    && !"none".equals(cacheBehavior.getForwardedValues().getCookies().getForward())) {
                originIds.put(cacheBehavior.getTargetOriginId(), cacheBehavior);
            }
        }

        for (Origin origin : distributionConfig.getOrigins().getItems()) {
            if (originIds.get(origin.getId()) != null && origin.getS3OriginConfig() != null) {
                if (originIds.get(origin.getId()) instanceof DefaultCacheBehavior) {
                    DefaultCacheBehavior item = (DefaultCacheBehavior) originIds.get(origin.getId());
                    item.getForwardedValues().getCookies().setForward("none");
                } else {
                    CacheBehavior item = (CacheBehavior) originIds.get(origin.getId());
                    item.getForwardedValues().getCookies().setForward("none");
                }
                hasChange = true;
            }
        }


        if (!hasChange) {
            setNoLongerRequired(detected, srContext.LOGTAG,
                    "Distribution with id '" + distributionId + "' already has signer / non-forwarded cookie.");
            return;
        }

        UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest()
                .withId(distributionId)
                .withIfMatch(result.getETag())
                .withDistributionConfig(distributionConfig);

        cloudFront.updateDistribution(updateDistributionRequest);

        setSuccess(detected, srContext.LOGTAG,
                "Distribution with id '" + distributionId + "' has been updated by removing static site from the CloudFront configuration.");
    }
}
