package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.elasticache.AmazonElastiCacheClient
import com.amazonaws.services.elasticache.model.AmazonElastiCacheException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class RdsHipaaIneligibleMemcachedRemediatorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "delete the cache cluster"() {
        setup:
        AmazonElastiCacheClient elastiCacheClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsHipaaIneligibleMemcached,
                "arn:aws:elasticache:us-east-1:123456789012:cluster:cache-cluster-id")

        RdsHipaaIneligibleMemcachedRemediator remediator = init(new RdsHipaaIneligibleMemcachedRemediator())
                .withClientBuilderOverride(elastiCacheClient)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "make sure it is deleted"
        1 * elastiCacheClient.deleteCacheCluster(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description == "ElastiCache cache cluster deleted."
    }

    def "defensive against exceptions"() {
        setup:
        AmazonElastiCacheClient elastiCacheClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsHipaaIneligibleMemcached,
                "arn:aws:elasticache:us-east-1:123456789012:cluster:cache-cluster-id")

        RdsHipaaIneligibleMemcachedRemediator remediator = init(new RdsHipaaIneligibleMemcachedRemediator())
                .withClientBuilderOverride(elastiCacheClient)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "exception during delete"
        1 * elastiCacheClient.deleteCacheCluster(_) >> { throw new AmazonElastiCacheException("exception") }

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.description == "Remediation Error"
        detected.remediationResult.getError(0).startsWith("Unexpected error while deleting the ElastiCache cache cluster")
    }
}
