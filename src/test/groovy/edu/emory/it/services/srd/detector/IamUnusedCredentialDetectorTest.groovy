package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.AccessKeyLastUsed
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedResult
import com.amazonaws.services.identitymanagement.model.GetLoginProfileResult
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult
import com.amazonaws.services.identitymanagement.model.ListUsersResult
import com.amazonaws.services.identitymanagement.model.LoginProfile
import com.amazonaws.services.identitymanagement.model.User
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock

import java.time.LocalDateTime
import java.time.ZoneId

@Category(UTCategoryFast.class)
class IamUnusedCredentialDetectorTest extends UTSpecification {
    static int expiryInDays = 30
    static ZoneId systemDefaultZoneId = ZoneId.systemDefault()
    static Hashtable additionalObjects = new Hashtable()
    static {
        Properties properties = new Properties()
        properties.setProperty("IamUnusedCredentialExpiryInDays", String.valueOf(expiryInDays))
        PropertyConfig propertyConfig = new PropertyConfig()
        propertyConfig.setProperties(properties)
        additionalObjects.put("IamUnusedCredential".toLowerCase(), propertyConfig)
    }

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "detection when accesskey is expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        GetLoginProfileResult getLoginProfileResult = new GetLoginProfileResult()
        ListUsersResult listUsersResult = setupListUsersResult(now, expiryInDays - 1)
        ListAccessKeysResult listAccessKeysResult = setupListAccessKeysResult(now)
        GetAccessKeyLastUsedResult getAccessKeyLastUsedResult = setupGetAccessKeyLastUsedResult(now, expiryInDays + 1)

        IamUnusedCredentialDetector detector = init(new IamUnusedCredentialDetector(), additionalObjects)
                .withClientBuilderOverride(iam)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.IamUnusedCredentialApiKey.name()
        risks.get(0).amazonResourceName == "arn:aws:iam::123456789012:user/jenny/access-key/accessKeyId"
        1 * iam.listUsers(_) >> listUsersResult
        1 * iam.getLoginProfile(_) >> getLoginProfileResult
        1 * iam.listAccessKeys(_) >> listAccessKeysResult
        1 * iam.getAccessKeyLastUsed(_) >> getAccessKeyLastUsedResult
    }

    def "detection when credentials is expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        GetLoginProfileResult getLoginProfileResult = new GetLoginProfileResult().withLoginProfile(new LoginProfile())
        ListUsersResult listUsersResult = setupListUsersResult(now, expiryInDays + 1)
        ListAccessKeysResult listAccessKeysResult = setupListAccessKeysResult(now)
        GetAccessKeyLastUsedResult getAccessKeyLastUsedResult = setupGetAccessKeyLastUsedResult(now, expiryInDays - 1)

        IamUnusedCredentialDetector detector = init(new IamUnusedCredentialDetector(), additionalObjects)
                .withClientBuilderOverride(iam)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.IamUnusedCredentialAwsConsole.name()
        risks.get(0).amazonResourceName == "arn:aws:iam::123456789012:user/jenny"
        1 * iam.listUsers(_) >> listUsersResult
        1 * iam.getLoginProfile(_) >> getLoginProfileResult
        1 * iam.listAccessKeys(_) >> listAccessKeysResult
        1 * iam.getAccessKeyLastUsed(_) >> getAccessKeyLastUsedResult
    }

    def "detection when both credentials and accessKey is expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        GetLoginProfileResult getLoginProfileResult = new GetLoginProfileResult().withLoginProfile(new LoginProfile())
        ListUsersResult listUsersResult = setupListUsersResult(now, expiryInDays + 1)
        ListAccessKeysResult listAccessKeysResult = setupListAccessKeysResult(now)
        GetAccessKeyLastUsedResult getAccessKeyLastUsedResult = setupGetAccessKeyLastUsedResult(now, expiryInDays + 1)

        IamUnusedCredentialDetector detector = init(new IamUnusedCredentialDetector(), additionalObjects)
                .withClientBuilderOverride(iam)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 2
        risks.get(0).type == DetectionType.IamUnusedCredentialAwsConsole.name()
        risks.get(0).amazonResourceName == "arn:aws:iam::123456789012:user/jenny"
        risks.get(1).type == DetectionType.IamUnusedCredentialApiKey.name()
        risks.get(1).amazonResourceName == "arn:aws:iam::123456789012:user/jenny/access-key/accessKeyId"
        1 * iam.listUsers(_) >> listUsersResult
        1 * iam.getLoginProfile(_) >> getLoginProfileResult
        1 * iam.listAccessKeys(_) >> listAccessKeysResult
        1 * iam.getAccessKeyLastUsed(_) >> getAccessKeyLastUsedResult
    }

    private static ListUsersResult setupListUsersResult(LocalDateTime now, int daysAgoLastUsed) {
        User user = new User()
                .withArn("arn:aws:iam::123456789012:user/jenny")
                .withCreateDate(calcDaysAgo(now, 60))
                .withPasswordLastUsed(calcDaysAgo(now, daysAgoLastUsed))

        return new ListUsersResult()
                .withUsers(user)
                .withIsTruncated(Boolean.FALSE)
    }

    private static ListAccessKeysResult setupListAccessKeysResult(LocalDateTime now) {
        return new ListAccessKeysResult()
                .withIsTruncated(Boolean.FALSE)
                .withAccessKeyMetadata(new AccessKeyMetadata()
                        .withAccessKeyId("accessKeyId")
                        .withCreateDate(calcDaysAgo(now, 60)))
    }

    private static GetAccessKeyLastUsedResult setupGetAccessKeyLastUsedResult(LocalDateTime now, int daysAgoLastUsed) {
        return new GetAccessKeyLastUsedResult()
                .withAccessKeyLastUsed(new AccessKeyLastUsed()
                        .withLastUsedDate(calcDaysAgo(now, daysAgoLastUsed)))
    }

    private static Date calcDaysAgo(LocalDateTime now, int daysAgo) {
        Date.from(now.minusDays(daysAgo).atZone(systemDefaultZoneId).toInstant())
    }
}
