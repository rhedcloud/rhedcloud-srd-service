package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.AccessKeyLastUsed
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata
import com.amazonaws.services.identitymanagement.model.DeleteLoginProfileResult
import com.amazonaws.services.identitymanagement.model.EntityTemporarilyUnmodifiableException
import com.amazonaws.services.identitymanagement.model.GetAccessKeyLastUsedResult
import com.amazonaws.services.identitymanagement.model.GetLoginProfileResult
import com.amazonaws.services.identitymanagement.model.GetUserResult
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysRequest
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysResult
import com.amazonaws.services.identitymanagement.model.ListServiceSpecificCredentialsResult
import com.amazonaws.services.identitymanagement.model.LoginProfile
import com.amazonaws.services.identitymanagement.model.SSHPublicKeyMetadata
import com.amazonaws.services.identitymanagement.model.ServiceSpecificCredentialMetadata
import com.amazonaws.services.identitymanagement.model.StatusType
import com.amazonaws.services.identitymanagement.model.User
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock

import java.time.LocalDateTime
import java.time.ZoneId

@Category(UTCategoryFast.class)
class IamUnusedCredentialRemediatorTest extends UTSpecification {
    static int expiryInDays = 30
    static ZoneId systemDefaultZoneId = ZoneId.systemDefault()
    static Hashtable additionalObjects = new Hashtable()
    static {
        Properties properties = new Properties()
        properties.setProperty("IamUnusedCredentialExpiryInDays", String.valueOf(expiryInDays))
        PropertyConfig propertyConfig = new PropertyConfig()
        propertyConfig.setProperties(properties)
        additionalObjects.put("IamUnusedCredential".toLowerCase(), propertyConfig)
    }

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "remediation when accesskey is expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        ListAccessKeysResult listAccessKeysResult = setupListAccessKeysResult(now)
        GetAccessKeyLastUsedResult getAccessKeyLastUsedResult = setupGetAccessKeyLastUsedResult(now, expiryInDays + 1)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.IamUnusedCredentialApiKey, "arn:aws:iam::123456789012:user/jenny/access-key/accessKeyId")

        IamUnusedCredentialRemediator remediator = init(new IamUnusedCredentialRemediator(), additionalObjects)
                .withClientBuilderOverride(iam)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then:
        1 * iam.listAccessKeys(_) >> listAccessKeysResult
        1 * iam.getAccessKeyLastUsed(_) >> getAccessKeyLastUsedResult

        then: "accesskey is disabled"
        1 * iam.deleteAccessKey(_)

        then: "successful remediation"
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
    }

    def "remediation no longer required when accesskey is not expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        ListAccessKeysResult listAccessKeysResult = setupListAccessKeysResult(now)
        GetAccessKeyLastUsedResult getAccessKeyLastUsedResult = setupGetAccessKeyLastUsedResult(now, expiryInDays - 1)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.IamUnusedCredentialApiKey, "arn:aws:iam::123456789012:user/jenny/access-key/accessKeyId")

        IamUnusedCredentialRemediator remediator = init(new IamUnusedCredentialRemediator(), additionalObjects)
                .withClientBuilderOverride(iam)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then:
        1 * iam.listAccessKeys(_) >> listAccessKeysResult
        1 * iam.getAccessKeyLastUsed(_) >> getAccessKeyLastUsedResult

        then: "remediation no longer required"
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Access key has not yet expired."
    }

    def "remediation no longer required when accesskey was deleted"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.IamUnusedCredentialApiKey, "arn:aws:iam::123456789012:user/jenny/access-key/accessKeyId")

        IamUnusedCredentialRemediator remediator = init(new IamUnusedCredentialRemediator(), additionalObjects)
                .withClientBuilderOverride(iam)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then:
        1 * iam.listAccessKeys(_) >> new ListAccessKeysResult().withIsTruncated(Boolean.FALSE)  // no access key

        then: "remediation no longer required"
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Access Key already deleted."
    }

    def "remediation when credentials is expired"() {
        setup:
        AmazonIdentityManagementClient iam = Mock()

        LocalDateTime now = LocalDateTime.now()

        GetLoginProfileResult getLoginProfileResult = new GetLoginProfileResult().withLoginProfile(new LoginProfile())
        GetUserResult getUserResult = setupGetUserResult(now, expiryInDays + 1)
        ListSSHPublicKeysResult listSSHPublicKeysResult = new ListSSHPublicKeysResult()
                .withSSHPublicKeys(new SSHPublicKeyMetadata().withStatus(StatusType.Active))
                .withIsTruncated(Boolean.FALSE)

        ListServiceSpecificCredentialsResult listServiceSpecificCredentialsResult = new ListServiceSpecificCredentialsResult()
                .withServiceSpecificCredentials(new ServiceSpecificCredentialMetadata().withStatus(StatusType.Active))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.IamUnusedCredentialAwsConsole, "arn:aws:iam::123456789012:user/jenny")

        IamUnusedCredentialRemediator remediator = init(new IamUnusedCredentialRemediator(), additionalObjects)
                .withClientBuilderOverride(iam)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then:
        1 * iam.getUser(_) >> getUserResult
        1 * iam.getLoginProfile(_) >> getLoginProfileResult
        1 * iam.listSSHPublicKeys(_ as ListSSHPublicKeysRequest) >> listSSHPublicKeysResult

        then: "ssh public key for git is disabled"
        1 * iam.updateSSHPublicKey({ it.getStatus() == StatusType.Inactive.name() })

        then:
        1 * iam.listServiceSpecificCredentials(_) >> listServiceSpecificCredentialsResult

        then: "http credentials for git is disabled"
        1 * iam.updateServiceSpecificCredential({ it.getStatus() == StatusType.Inactive.name() })

        // it would be nice to test the EntityTemporarilyUnmodifiableException for all returns
        // but that would make the running time of the test toooooo long (2.5 min) so try just one retry
        then: "login is disabled after 1 retry"
        2 * iam.deleteLoginProfile(_) >> { throw new EntityTemporarilyUnmodifiableException() } >> { new DeleteLoginProfileResult() }

        then: "successful remediation"
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
    }

    private static GetUserResult setupGetUserResult(LocalDateTime now, int daysAgoLastUsed) {
        User user = new User()
                .withArn("arn:aws:iam::123456789012:user/jenny")
                .withCreateDate(calcDaysAgo(now, 60))
                .withPasswordLastUsed(calcDaysAgo(now, daysAgoLastUsed))

        return new GetUserResult()
                .withUser(user)
    }

    private static ListAccessKeysResult setupListAccessKeysResult(LocalDateTime now) {
        return new ListAccessKeysResult()
                .withIsTruncated(Boolean.FALSE)
                .withAccessKeyMetadata(new AccessKeyMetadata()
                        .withAccessKeyId("accessKeyId")
                        .withCreateDate(calcDaysAgo(now, 60)))
    }

    private static GetAccessKeyLastUsedResult setupGetAccessKeyLastUsedResult(LocalDateTime now, int daysAgoLastUsed) {
        return new GetAccessKeyLastUsedResult()
                .withAccessKeyLastUsed(new AccessKeyLastUsed()
                        .withLastUsedDate(calcDaysAgo(now, daysAgoLastUsed)))
    }

    private static Date calcDaysAgo(LocalDateTime now, int daysAgo) {
        Date.from(now.minusDays(daysAgo).atZone(systemDefaultZoneId).toInstant())
    }
}
