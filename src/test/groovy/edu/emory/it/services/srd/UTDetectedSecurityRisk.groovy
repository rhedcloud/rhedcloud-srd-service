package edu.emory.it.services.srd

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult

class UTDetectedSecurityRisk extends DetectedSecurityRisk {
    RemediationResult remediationResult

    @Override
    void setRemediationResult(RemediationResult rr) {
        super.setRemediationResult(rr)
        this.remediationResult = rr
    }

    @Override
    RemediationResult newRemediationResult() {
        return new RemediationResult()
    }

    static create(DetectionType detectionType, String arn) {
        UTDetectedSecurityRisk detected = new UTDetectedSecurityRisk()
        detected.setType(detectionType.name())
        detected.setAmazonResourceName(arn)
        return detected
    }
}
