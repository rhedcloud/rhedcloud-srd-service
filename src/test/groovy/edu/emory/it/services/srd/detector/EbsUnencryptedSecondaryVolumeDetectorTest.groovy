package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import com.amazonaws.services.ec2.model.DescribeVolumesResult
import com.amazonaws.services.ec2.model.DeviceType
import com.amazonaws.services.ec2.model.Instance
import com.amazonaws.services.ec2.model.InstanceState
import com.amazonaws.services.ec2.model.Reservation
import com.amazonaws.services.ec2.model.Volume
import com.amazonaws.services.ec2.model.VolumeAttachment
import com.amazonaws.services.ec2.model.VolumeAttachmentState
import com.amazonaws.services.ec2.model.VolumeState
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class EbsUnencryptedSecondaryVolumeDetectorTest extends UTSpecification {
    static String INSTANCE_ID = "i-016d590b0bd35b091"
    static String ANOTHER_INSTANCE_ID = "i-07065dc8f212f92ce"
    static String VOLUME_ID = "vol-0b07321771c04af00"
    static String ROOT_DEVICE_NAME = "/dev/xvda"
    static String ATTACHED_DEVICE_NAME = "/dev/sda"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when no EC2 instances are available"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        EbsUnencryptedSecondaryVolumeDetector detector = init(new EbsUnencryptedSecondaryVolumeDetector())
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "with no reservations"
        1 * ec2.describeInstances(_) >> new DescribeInstancesResult()
        0 * ec2.describeVolumes(_)

        then:
        risks.size() == 0
    }

    def "no detection when no EC2 volume instances are available"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        EbsUnencryptedSecondaryVolumeDetector detector = init(new EbsUnencryptedSecondaryVolumeDetector())
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult

        then: "but no volumes"
        1 * ec2.describeVolumes(_) >> new DescribeVolumesResult()

        then:
        risks.size() == 0
    }

    def "no detection when only primary volume instances are available"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE))  // <--- not encrypted

        EbsUnencryptedSecondaryVolumeDetector detector = init(new EbsUnencryptedSecondaryVolumeDetector())
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult

        then: "but only primary volume"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        risks.size() == 0
    }

    def "no detection when volume instance is encrypted"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.TRUE)) // <--- encrypted

        EbsUnencryptedSecondaryVolumeDetector detector = init(new EbsUnencryptedSecondaryVolumeDetector())
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult

        then: "but with an encrypted volume"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        risks.size() == 0
    }

    def "detection when volume instance is secondary and not encrypted"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))
        DescribeInstancesResult anotherInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(ANOTHER_INSTANCE_ID)
                                .withState(new InstanceState().withName("running"))))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE) // <--- not encrypted
                        .withAttachments(new VolumeAttachment()
                                .withInstanceId(ANOTHER_INSTANCE_ID)
                                .withDevice(ATTACHED_DEVICE_NAME)
                                .withState(VolumeAttachmentState.Attached)))

        EbsUnencryptedSecondaryVolumeDetector detector = init(new EbsUnencryptedSecondaryVolumeDetector())
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult

        then: "and an unencrypted volume"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then: "and the attached instance is running"
        1 * ec2.describeInstances(_) >> anotherInstancesResult

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.EbsUnencryptedSecondaryVolume.name()
        risks.get(0).amazonResourceName == "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}"
    }
}
