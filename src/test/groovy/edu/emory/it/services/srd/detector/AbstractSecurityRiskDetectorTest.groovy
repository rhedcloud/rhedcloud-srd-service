package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import edu.emory.it.services.srd.ComplianceClass
import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass
import edu.emory.it.services.srd.annotations.StandardComplianceClass
import edu.emory.it.services.srd.util.SecurityRiskContext
import spock.lang.Specification

class AbstractSecurityRiskDetectorTest extends Specification {
    void "test detector compliance class annotations"() {
        given:
        AbstractSecurityRiskDetector srd

        when:
        srd = new StandardComplianceClassDetector()
        then:
        srd.isStandardComplianceClass()
        ! srd.isHipaaComplianceClass()
        ! srd.isEnhancedSecurityComplianceClass()

        when:
        srd = new HIPAAComplianceClassDetector()
        then:
        ! srd.isStandardComplianceClass()
        srd.isHipaaComplianceClass()
        ! srd.isEnhancedSecurityComplianceClass()

        when:
        srd = new EnhancedSecurityComplianceClassDetector()
        then:
        ! srd.isStandardComplianceClass()
        ! srd.isHipaaComplianceClass()
        srd.isEnhancedSecurityComplianceClass()

        when:
        srd = new AllComplianceClassDetector()
        then:
        srd.isStandardComplianceClass()
        srd.isHipaaComplianceClass()
        srd.isEnhancedSecurityComplianceClass()

        when:
        srd = new NoComplianceClassDetector()
        then:
        ! srd.isStandardComplianceClass()
        ! srd.isHipaaComplianceClass()
        ! srd.isEnhancedSecurityComplianceClass()
    }

    void "test if detector relevant to account"() {
        given:
        AbstractSecurityRiskDetector srd

        when:
        srd = new StandardComplianceClassDetector()
        then:
        srd.isDetectorRelevantToAccount(ComplianceClass.Standard)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.HIPAA)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.EnhancedSecurity)

        when:
        srd = new HIPAAComplianceClassDetector()
        then:
        ! srd.isDetectorRelevantToAccount(ComplianceClass.Standard)
        srd.isDetectorRelevantToAccount(ComplianceClass.HIPAA)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.EnhancedSecurity)

        when:
        srd = new EnhancedSecurityComplianceClassDetector()
        then:
        ! srd.isDetectorRelevantToAccount(ComplianceClass.Standard)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.HIPAA)
        srd.isDetectorRelevantToAccount(ComplianceClass.EnhancedSecurity)

        when:
        srd = new AllComplianceClassDetector()
        then:
        srd.isDetectorRelevantToAccount(ComplianceClass.Standard)
        srd.isDetectorRelevantToAccount(ComplianceClass.HIPAA)
        srd.isDetectorRelevantToAccount(ComplianceClass.EnhancedSecurity)

        when:
        srd = new NoComplianceClassDetector()
        then:
        ! srd.isDetectorRelevantToAccount(ComplianceClass.Standard)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.HIPAA)
        ! srd.isDetectorRelevantToAccount(ComplianceClass.EnhancedSecurity)
    }

    @StandardComplianceClass
    class StandardComplianceClassDetector extends AbstractSecurityRiskDetector {
        @Override void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {}
        @Override String getBaseName() { return "StandardComplianceClass" }
    }
    @HIPAAComplianceClass
    class HIPAAComplianceClassDetector extends AbstractSecurityRiskDetector {
        @Override void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {}
        @Override String getBaseName() { return "HIPAAComplianceClass" }
    }
    @EnhancedSecurityComplianceClass
    class EnhancedSecurityComplianceClassDetector extends AbstractSecurityRiskDetector {
        @Override void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {}
        @Override String getBaseName() { return "EnhancedSecurityComplianceClass" }
    }
    @StandardComplianceClass
    @HIPAAComplianceClass
    @EnhancedSecurityComplianceClass
    class AllComplianceClassDetector extends AbstractSecurityRiskDetector {
        @Override void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {}
        @Override String getBaseName() { return "AllComplianceClass" }
    }
    class NoComplianceClassDetector extends AbstractSecurityRiskDetector {
        @Override void detect(SecurityRiskDetection detection, String accountId, SecurityRiskContext srContext) {}
        @Override String getBaseName() { return "NoComplianceClass" }
    }
}

