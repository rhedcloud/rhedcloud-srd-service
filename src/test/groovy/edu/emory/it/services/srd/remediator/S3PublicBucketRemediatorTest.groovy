package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AccessControlList
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.GetPublicAccessBlockRequest
import com.amazonaws.services.s3.model.GetPublicAccessBlockResult
import com.amazonaws.services.s3.model.GroupGrantee
import com.amazonaws.services.s3.model.Permission
import com.amazonaws.services.s3.model.PublicAccessBlockConfiguration
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class S3PublicBucketRemediatorTest extends UTSpecification {
    static bucketName = "bucket"
    static bucketKey = "bucketKey"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "remediate public AllUsers bucket"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AllUsers, Permission.FullControl)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketAcl, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "AllUsers with FullControl"
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.setBucketAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AllUsers }) == null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AuthenticatedUsers }) == null
    }

    def "remediate public AuthenticatedUsers bucket"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AuthenticatedUsers, Permission.FullControl)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketAcl, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "AuthenticatedUsers with FullControl"
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.setBucketAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AllUsers }) == null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AuthenticatedUsers }) == null
    }

    def "remediate public AllUsers bucket object"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AllUsers, Permission.FullControl)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketObject, "arn:aws:s3:::${bucketName}/${bucketKey}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "AllUsers with FullControl"
        1 * s3.getObjectAcl(bucketName, bucketKey) >> acl
        1 * s3.setObjectAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AllUsers }) == null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AuthenticatedUsers }) == null
    }

    def "remediate public AuthenticatedUsers bucket object"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AuthenticatedUsers, Permission.FullControl)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketObject, "arn:aws:s3:::${bucketName}/${bucketKey}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "AuthenticatedUsers with FullControl"
        1 * s3.getObjectAcl(bucketName, bucketKey) >> acl
        1 * s3.setObjectAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AllUsers }) == null
        acl.getGrantsAsList().find({ it.getGrantee() == GroupGrantee.AuthenticatedUsers }) == null
    }

    def "remediate no longer required when bucket is no longer public"() throws EnterpriseFieldException {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketAcl, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "no AllUsers or AuthenticatedUsers"
        1 * s3.getBucketAcl(bucketName) >> acl
        0 * s3.setBucketAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description != null
    }

    def "remediate no longer required when bucket object is no longer public"() throws EnterpriseFieldException {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AccessControlList acl = new AccessControlList()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicBucketObject, "arn:aws:s3:::${bucketName}/${bucketKey}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "no AllUsers or AuthenticatedUsers"
        1 * s3.getObjectAcl(bucketName, bucketKey) >> acl
        0 * s3.setObjectAcl(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description != null
    }

    def "remediate bucket public access block missing"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        AmazonS3Exception NoSuchPublicAccessBlockConfigurationException = new AmazonS3Exception("exception")
        NoSuchPublicAccessBlockConfigurationException.setErrorCode("NoSuchPublicAccessBlockConfiguration")

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicAccessBlockMisconfiguration, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "NoSuchPublicAccessBlockConfigurationException"
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> { throw NoSuchPublicAccessBlockConfigurationException }
        1 * s3.setPublicAccessBlock(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        detected.remediationResult.description.contains("IgnorePublicAcls")
        detected.remediationResult.description.contains("BlockPublicPolicy")
        detected.remediationResult.description.contains("RestrictPublicBuckets")
    }

    def "remediate bucket public access block misconfiguration"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(false)
                        .withBlockPublicPolicy(false)
                        .withRestrictPublicBuckets(false))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicAccessBlockMisconfiguration, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "misconfiguration"
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult
        1 * s3.setPublicAccessBlock(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        detected.remediationResult.description.contains("IgnorePublicAcls")
        detected.remediationResult.description.contains("BlockPublicPolicy")
        detected.remediationResult.description.contains("RestrictPublicBuckets")
    }

    def "remediate no longer required on correct bucket public access block configuration"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketRemediator remediator = init(new S3PublicBucketRemediator())
                .withClientBuilderOverride(s3)

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(true)
                        .withBlockPublicPolicy(true)
                        .withRestrictPublicBuckets(true))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3PublicAccessBlockMisconfiguration, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then: "proper configuration"
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult
        0 * s3.setPublicAccessBlock(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description != null
        detected.remediationResult.description.contains("is already configured correctly")
    }
}
