package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.apigateway.AmazonApiGatewayClient
import com.amazonaws.services.apigateway.model.GetStageResult
import com.amazonaws.services.apigateway.model.NotFoundException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class APIGatewayCachingRemediatorTest extends UTSpecification {
    static String REST_API_ID = "APIGatewayCachingDetectorTest_API"
    static String STAGE_NAME = "APIGatewayCachingDetectorTest_Stage"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "remediation no longer required when Stage can not be found"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.ApiGatewayCaching, "arn:aws:apigateway:us-east-1:123456789012:${REST_API_ID}/${STAGE_NAME}")

        APIGatewayCachingRemediator remediator = init(new APIGatewayCachingRemediator())
                .withClientBuilderOverride(gateway)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with Stage not found"
        1 * gateway.getStage(_) >> { throw new NotFoundException() }

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "REST API id '${REST_API_ID}' with stage name '${STAGE_NAME}' cannot be found."
    }

    def "remediation no longer required when Stage already has cache disabled"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        GetStageResult getStageResult = new GetStageResult()
                .withStageName(STAGE_NAME)
                .withCacheClusterEnabled(Boolean.FALSE)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.ApiGatewayCaching, "arn:aws:apigateway:us-east-1:123456789012:${REST_API_ID}/${STAGE_NAME}")

        APIGatewayCachingRemediator remediator = init(new APIGatewayCachingRemediator())
                .withClientBuilderOverride(gateway)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with Stage already cache disabled"
        1 * gateway.getStage(_) >> getStageResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "REST API id '${REST_API_ID}' with stage name '${STAGE_NAME}' already has caching disabled."
    }

    def "remediation success when Stage has cache enabled"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        GetStageResult getStageResult = new GetStageResult()
                .withStageName(STAGE_NAME)
                .withCacheClusterEnabled(Boolean.TRUE)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.ApiGatewayCaching, "arn:aws:apigateway:us-east-1:123456789012:${REST_API_ID}/${STAGE_NAME}")

        APIGatewayCachingRemediator remediator = init(new APIGatewayCachingRemediator())
                .withClientBuilderOverride(gateway)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with Stage cache enabled"
        1 * gateway.getStage(_) >> getStageResult

        then: "and Stage cache is disabled"
        1 * gateway.updateStage({
            it.getRestApiId() == REST_API_ID &&
                    it.getStageName() == STAGE_NAME &&
                    it.getPatchOperations().get(0).getOp() == "replace" &&
                    it.getPatchOperations().get(0).getPath() == "/cacheClusterEnabled" &&
                    it.getPatchOperations().get(0).getValue() == "false" })

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description == "REST API id '${REST_API_ID}' with stage name '${STAGE_NAME}' has been updated to disable caching"
    }
}
