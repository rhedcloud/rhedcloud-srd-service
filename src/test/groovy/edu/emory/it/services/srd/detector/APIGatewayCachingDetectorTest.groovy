package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.apigateway.AmazonApiGatewayClient
import com.amazonaws.services.apigateway.model.Deployment
import com.amazonaws.services.apigateway.model.GetDeploymentsResult
import com.amazonaws.services.apigateway.model.GetRestApisResult
import com.amazonaws.services.apigateway.model.GetStagesResult
import com.amazonaws.services.apigateway.model.RestApi
import com.amazonaws.services.apigateway.model.Stage
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class APIGatewayCachingDetectorTest extends UTSpecification {
    static String REST_API_ID = "APIGatewayCachingDetectorTest_API"
    static String DEPLOYMENT_ID = "APIGatewayCachingDetectorTest_Deployment"
    static String STAGE_NAME = "APIGatewayCachingDetectorTest_Stage"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when no REST APIs are available"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        APIGatewayCachingDetector detector = init(new APIGatewayCachingDetector())
                .withClientBuilderOverride(gateway)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "no APIs"
        1 * gateway.getRestApis(_) >> new GetRestApisResult().withItems([])
        0 * gateway.getDeployments(_)
        0 * gateway.getStages(_)

        then:
        risks.size() == 0
    }

    def "no detection when no Deployments are available"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        GetRestApisResult getRestApisResult = new GetRestApisResult()
                .withItems(new RestApi().withId(REST_API_ID))

        APIGatewayCachingDetector detector = init(new APIGatewayCachingDetector())
                .withClientBuilderOverride(gateway)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "a REST API"
        1 * gateway.getRestApis(_) >> getRestApisResult

        then: "but no Deployments"
        1 * gateway.getDeployments(_) >> new GetDeploymentsResult().withItems([])
        0 * gateway.getStages(_)

        then:
        risks.size() == 0
    }

    def "no detection when Stage has cache disabled"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        GetRestApisResult getRestApisResult = new GetRestApisResult()
                .withItems(new RestApi().withId(REST_API_ID))
        GetDeploymentsResult getDeploymentsResult = new GetDeploymentsResult()
                .withItems(new Deployment()
                        .withId(DEPLOYMENT_ID))
        GetStagesResult getStagesResult = new GetStagesResult()
                .withItem(new Stage()
                        .withCacheClusterEnabled(Boolean.FALSE))

        APIGatewayCachingDetector detector = init(new APIGatewayCachingDetector())
                .withClientBuilderOverride(gateway)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "a REST API"
        1 * gateway.getRestApis(_) >> getRestApisResult

        then: "with a Deployment"
        1 * gateway.getDeployments(_) >> getDeploymentsResult

        then: "but Stage already has cache disabled"
        1 * gateway.getStages(_) >> getStagesResult

        then:
        risks.size() == 0
    }

    def "detection when Stage has cache enabled"() {
        setup:
        AmazonApiGatewayClient gateway = Mock()

        GetRestApisResult getRestApisResult = new GetRestApisResult()
                .withItems(new RestApi().withId(REST_API_ID))
        GetDeploymentsResult getDeploymentsResult = new GetDeploymentsResult()
                .withItems(new Deployment()
                        .withId(DEPLOYMENT_ID))
        GetStagesResult getStagesResult = new GetStagesResult()
                .withItem(new Stage()
                        .withStageName(STAGE_NAME)
                        .withCacheClusterEnabled(Boolean.TRUE))

        APIGatewayCachingDetector detector = init(new APIGatewayCachingDetector())
                .withClientBuilderOverride(gateway)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "a REST API"
        1 * gateway.getRestApis(_) >> getRestApisResult

        then: "with a Deployment"
        1 * gateway.getDeployments(_) >> getDeploymentsResult

        then: "and Stage has cache enabled"
        1 * gateway.getStages(_) >> getStagesResult

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.ApiGatewayCaching.name()
        risks.get(0).amazonResourceName == "arn:aws:apigateway:us-east-1:123456789012:${REST_API_ID}/${STAGE_NAME}"
    }
}
