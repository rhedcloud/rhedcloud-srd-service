package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.ListRolesResult
import com.amazonaws.services.identitymanagement.model.Role
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClient
import com.amazonaws.services.securitytoken.model.GetCallerIdentityResult
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class IamExternalTrustRelationshipPolicyDetectorTest extends UTSpecification {
    static Hashtable additionalObjects = new Hashtable()
    static {
        Properties properties = new Properties()
        properties.setProperty("ignoreArns", "arn:aws:iam::*:role/wildcard/*, ,arn:aws:iam::*:role/specific/Whitelisted")
        PropertyConfig propertyConfig = new PropertyConfig()
        propertyConfig.setProperties(properties)
        additionalObjects.put("IamExternalTrustRelationshipPolicy".toLowerCase(), propertyConfig)
    }
    String permissivePolicy = """{
              "Version": "2012-10-17",
              "Id": "arn:aws:sqs:us-east-1:456789123456:srdtest.fifo./SQSDefaultPolicy",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": [
                      "arn:aws:iam::456789123456:root",
                      "*"
                    ]
                  },
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:456789123456:srdtest.fifo"
                }
              ]
            }
        """

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when no roles"() {
        setup:
        AWSSecurityTokenServiceClient stsClient = Mock()
        AmazonIdentityManagementClient identityManagementClient = Mock()

        IamExternalTrustRelationshipPolicyDetector detector = init(new IamExternalTrustRelationshipPolicyDetector(), additionalObjects)
                .withClientBuilderOverride(stsClient)
                .withClientBuilderOverride(identityManagementClient)
                .withRegionOverride(JUST_US_EAST_1)

        def listRolesResult_noRoles = new ListRolesResult().withIsTruncated(false)
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "no roles"
        1 * stsClient.getCallerIdentity(_) >> new GetCallerIdentityResult().withAccount("210987654321")
        1 * identityManagementClient.listRoles(_) >> listRolesResult_noRoles

        then:
        risks.size() == 0
    }

    def "no detection when roles whitelisted"() {
        setup:
        AWSSecurityTokenServiceClient stsClient = Mock()
        AmazonIdentityManagementClient identityManagementClient = Mock()

        IamExternalTrustRelationshipPolicyDetector detector = init(new IamExternalTrustRelationshipPolicyDetector(), additionalObjects)
                .withClientBuilderOverride(stsClient)
                .withClientBuilderOverride(identityManagementClient)
                .withRegionOverride(JUST_US_EAST_1)

        def listRolesResult_withRoles = new ListRolesResult().withIsTruncated(false).withRoles(
                new Role().withRoleName("role1").withArn("arn:aws:iam::123456789012:role/wildcard/Whitelisted").withAssumeRolePolicyDocument(permissivePolicy),
                new Role().withRoleName("role2").withArn("arn:aws:iam::123456789012:role/specific/Whitelisted").withAssumeRolePolicyDocument(permissivePolicy))
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "list roles"
        1 * stsClient.getCallerIdentity(_) >> new GetCallerIdentityResult().withAccount("210987654321")
        1 * identityManagementClient.listRoles(_) >> listRolesResult_withRoles

        then:
        risks.size() == 0
    }

    def "detection when roles not whitelisted"() {
        setup:
        AWSSecurityTokenServiceClient stsClient = Mock()
        AmazonIdentityManagementClient identityManagementClient = Mock()

        IamExternalTrustRelationshipPolicyDetector detector = init(new IamExternalTrustRelationshipPolicyDetector(), additionalObjects)
                .withClientBuilderOverride(stsClient)
                .withClientBuilderOverride(identityManagementClient)
                .withRegionOverride(JUST_US_EAST_1)

        def listRolesResult_withRoles = new ListRolesResult().withIsTruncated(false).withRoles(
                new Role().withRoleName("role1").withArn("arn:aws:iam::123456789012:role/wildcard/Whitelisted").withAssumeRolePolicyDocument(permissivePolicy),
                new Role().withRoleName("role2").withArn("arn:aws:iam::123456789012:role/specific/NotWhitelisted").withAssumeRolePolicyDocument(permissivePolicy))
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "list roles"
        1 * stsClient.getCallerIdentity(_) >> new GetCallerIdentityResult().withAccount("210987654321")
        1 * identityManagementClient.listRoles(_) >> listRolesResult_withRoles

        then:
        risks.size() == 0
    }
}
