package edu.emory.it.services.srd.util

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import edu.emory.it.services.srd.UTCategoryFast
import org.junit.experimental.categories.Category
import spock.lang.Specification

/**
 * Unit test for AWSPolicyUtil.
 */
@Category(UTCategoryFast.class)
class AWSPolicyUtilTest extends Specification {

    static ObjectMapper mapper = new ObjectMapper()
    static String ACCOUNT_ID = "123456789012"
    static Set<String> whitelistedAccounts = Collections.singleton(ACCOUNT_ID)


    def "isPolicyPrincipalContainingOtherAccounts: when child node is federated"() {
        when : "principal www.amazon.com"
        JsonNode principal = mapper.readTree("""
            {
                "Principal": {
                     "Federated": "www.amazon.com"
                }
            }
        """).get("Principal")
        boolean containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true
    }


    def "isPolicyPrincipalContainingOtherAccounts: when child node is aws"() {
        when : "only account is owner account (role)"
        JsonNode principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "arn:aws:iam::${ACCOUNT_ID}:role/rolename"
                }
            }
        """).get("Principal")
        boolean containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should not contain other accounts"
        containsOther == false

        when : "only account is owner account (user)"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "arn:aws:iam::${ACCOUNT_ID}:user/username"
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should not contain other accounts"
        containsOther == false

        when : "only account is owner account (root)"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "arn:aws:iam::${ACCOUNT_ID}:root"
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should not contain other accounts"
        containsOther == false


        when : "only account is owner account (assumed role)"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "arn:aws:sts::${ACCOUNT_ID}:assumed-role/role-name/role-session-name"
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should not contain other accounts"
        containsOther == false


        when : "only account is foreign account"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "arn:aws:iam::111111111:root"
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true

        when : "only account is *"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": "*"
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true

    }


    def "isPolicyPrincipalContainingOtherAccounts: when principal is an aws array"() {
        when : "principal is only owner"
        JsonNode principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": [
                        "arn:aws:iam::${ACCOUNT_ID}:root"
                    ]
                }
            }
        """).get("Principal")
        boolean containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should not contain other accounts"
        containsOther == false

        when : "principal is multiple accounts"
         principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": [
                        "arn:aws:iam::${ACCOUNT_ID}:root",
                        "arn:aws:iam::1111111:root"
                    ]
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true

        when : "principal has *"
        principal = mapper.readTree("""
            {
                "Principal": {
                    "AWS": [
                        "arn:aws:iam::${ACCOUNT_ID}:root",
                        "*"
                    ]
                }
            }
        """).get("Principal")
        containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true

    }

    def "isPolicyPrincipalContainingOtherAccounts: when principal is a literal"() {

        when : "principal is *"
        JsonNode principal = mapper.readTree("""
            {
                "Principal": "*"
            }
        """).get("Principal")
        boolean containsOther = AWSPolicyUtil.isPolicyPrincipalContainingOtherAccounts(whitelistedAccounts, principal)

        then: "should contain other accounts"
        containsOther == true

    }

    def "policyIsAllowAndHasPrincipalNotEqualToAccount test"() {
        when : "principal has *"
        String policy = """
            {
              "Version": "2012-10-17",
              "Id": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo./SQSDefaultPolicy",
              "Statement": [
                {
                  "Sid": "Sid1524766957922",
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": [
                      "arn:aws:iam::${ACCOUNT_ID}:root",
                      "*"
                    ]
                  },
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo"
                }
              ]
            }
        """
        boolean containsOther = AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy)

        then: "should contain other accounts"
        containsOther == true

        when : "principal has is owner"
        policy = """
            {
              "Version": "2012-10-17",
              "Id": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo./SQSDefaultPolicy",
              "Statement": [
                {
                  "Sid": "Sid1524766957922",
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": [
                      "arn:aws:iam::${ACCOUNT_ID}:root"
                    ]
                  },
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo"
                }
              ]
            }
        """
        containsOther = AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy)

        then: "should not contain other accounts"
        containsOther == false
    }

    def "removeAllowToNonOwnerAccount test"() {
        when : "principal has *"
        String policy = """
            {
              "Version": "2012-10-17",
              "Id": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo./SQSDefaultPolicy",
              "Statement": [
                {
                  "Sid": "Sid1524766957921",
                  "Effect": "Allow",
                  "Principal": "*",
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo"
                },
                {
                  "Sid": "Sid1524766957922",
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": [
                      "arn:aws:iam::${ACCOUNT_ID}:root",
                      "*"
                    ]
                  },
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo"
                },
                {
                  "Sid": "Sid1524766957923",
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": [
                      "arn:aws:iam::${ACCOUNT_ID}:root"
                    ]
                  },
                  "Action": "SQS:*",
                  "Resource": "arn:aws:sqs:us-east-1:${ACCOUNT_ID}:srdtest.fifo"
                }
              ]
            }
        """
        String updatedPolicy = AWSPolicyUtil.removeAllowToNonWhitelistedAccounts(whitelistedAccounts, policy)
        boolean containsOther = AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, updatedPolicy)
        JsonNode newPolicyNode = new ObjectMapper().readTree(updatedPolicy)

        then: "should not contain other accounts"
        containsOther == false
        newPolicyNode.get("Statement").size() == 2 //deleted statement Sid1524766957921 but kept the other two.
    }
}
