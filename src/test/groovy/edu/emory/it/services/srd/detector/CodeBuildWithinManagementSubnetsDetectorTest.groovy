package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.codebuild.AWSCodeBuildClient
import com.amazonaws.services.codebuild.model.BatchGetProjectsResult
import com.amazonaws.services.codebuild.model.ListProjectsResult
import com.amazonaws.services.codebuild.model.Project
import com.amazonaws.services.codebuild.model.VpcConfig
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.Subnet
import com.amazonaws.services.ec2.model.Tag
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class CodeBuildWithinManagementSubnetsDetectorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when no projects"() {
        setup:
        AWSCodeBuildClient codeBuild = Mock()
        AmazonEC2Client ec2 = Mock()

        CodeBuildWithinManagementSubnetsDetector detector = init(new CodeBuildWithinManagementSubnetsDetector())
                .withClientBuilderOverride(codeBuild)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        ListProjectsResult listProjectsResult_noProject = new ListProjectsResult()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "no projects"
        1 * ec2.describeSubnets() >> getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        1 * codeBuild.listProjects(_) >> listProjectsResult_noProject

        then:
        risks.size() == 0
    }

    def "no detection when project has no VPC"() {
        setup:
        AWSCodeBuildClient codeBuild = Mock()
        AmazonEC2Client ec2 = Mock()

        CodeBuildWithinManagementSubnetsDetector detector = init(new CodeBuildWithinManagementSubnetsDetector())
                .withClientBuilderOverride(codeBuild)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        ListProjectsResult listProjectsResult_noVpc = new ListProjectsResult().withProjects("p1")
        BatchGetProjectsResult batchGetProjectsResult_noVpc = new BatchGetProjectsResult().withProjects(
                new Project().withName("p1")
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "project has no VPC"
        1 * ec2.describeSubnets() >> getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        1 * codeBuild.listProjects(_) >> listProjectsResult_noVpc
        1 * codeBuild.batchGetProjects(_) >> batchGetProjectsResult_noVpc

        then:
        risks.size() == 0
    }

    def "no detection when project in allowed subnet"() {
        setup:
        AWSCodeBuildClient codeBuild = Mock()
        AmazonEC2Client ec2 = Mock()

        CodeBuildWithinManagementSubnetsDetector detector = init(new CodeBuildWithinManagementSubnetsDetector())
                .withClientBuilderOverride(codeBuild)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def disallowedSubnetsInteractionEc2DescribeSubnetsResult = getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        Subnet allowedSubnet = disallowedSubnetsInteractionEc2DescribeSubnetsResult.subnets.find({
            ((Subnet) it).tags.any({((Tag) it).key == "Name" && ((Tag) it).value == "Private Subnet 1" })
        })

        ListProjectsResult listProjectsResult_allowedSubnet = new ListProjectsResult().withProjects("p1")
        BatchGetProjectsResult batchGetProjectsResult_allowedSubnet = new BatchGetProjectsResult().withProjects(
                new Project().withName("p1").withVpcConfig(new VpcConfig().withVpcId(allowedSubnet.vpcId).withSubnets(allowedSubnet.subnetId))
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "one project"
        1 * ec2.describeSubnets() >> disallowedSubnetsInteractionEc2DescribeSubnetsResult
        1 * codeBuild.listProjects(_) >> listProjectsResult_allowedSubnet
        1 * codeBuild.batchGetProjects(_) >> batchGetProjectsResult_allowedSubnet

        then:
        risks.size() == 0
    }

    def "risk detection when project in disallowed subnet"() {
        setup:
        AWSCodeBuildClient codeBuild = Mock()
        AmazonEC2Client ec2 = Mock()

        CodeBuildWithinManagementSubnetsDetector detector = init(new CodeBuildWithinManagementSubnetsDetector())
                .withClientBuilderOverride(codeBuild)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def disallowedSubnetsInteractionEc2DescribeSubnetsResult = getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        Subnet disallowedSubnet = disallowedSubnetsInteractionEc2DescribeSubnetsResult.subnets.find({
            ((Subnet) it).tags.any({((Tag) it).key == "Name" && ((Tag) it).value == "Attachment Tier AZ-a" })
        })

        ListProjectsResult listProjectsResult_disallowedSubnet = new ListProjectsResult().withProjects("p1")
        BatchGetProjectsResult batchGetProjectsResult_disallowedSubnet = new BatchGetProjectsResult().withProjects(
                new Project().withName("p1").withVpcConfig(new VpcConfig().withVpcId(disallowedSubnet.vpcId).withSubnets(disallowedSubnet.subnetId))
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "one project"
        1 * ec2.describeSubnets() >> disallowedSubnetsInteractionEc2DescribeSubnetsResult
        1 * codeBuild.listProjects(_) >> listProjectsResult_disallowedSubnet
        1 * codeBuild.batchGetProjects(_) >> batchGetProjectsResult_disallowedSubnet

        then:
        risks.size() == 1
    }
}
