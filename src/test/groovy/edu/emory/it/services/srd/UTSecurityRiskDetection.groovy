package edu.emory.it.services.srd

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult

class UTSecurityRiskDetection extends SecurityRiskDetection {
    DetectionResult detectionResult
    List<DetectedSecurityRisk> detectedSecurityRisks = []

    @Override
    DetectionResult newDetectionResult() {
        return new DetectionResult()
    }
    @Override
    void setDetectionResult(DetectionResult dr) {
        super.setDetectionResult(dr)
        this.detectionResult = dr;
    }
    @Override
    DetectionResult getDetectionResult() {
        return detectionResult
    }
    @Override
    DetectedSecurityRisk newDetectedSecurityRisk() {
        return UTDetectedSecurityRisk.create(DetectionType.Example, "aws")
    }
    @Override
    void addDetectedSecurityRisk(DetectedSecurityRisk dsr) {
        super.addDetectedSecurityRisk(dsr)
        detectedSecurityRisks.add(dsr)
    }
    @Override
    List getDetectedSecurityRisk() {
        return detectedSecurityRisks
    }

    static create() {
        UTSecurityRiskDetection detected = new UTSecurityRiskDetection()
        return detected
    }
}
