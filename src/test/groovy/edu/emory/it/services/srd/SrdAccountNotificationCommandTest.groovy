package edu.emory.it.services.srd

import com.amazon.aws.moa.jmsobjects.provisioning.v1_0.AccountNotification
import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import org.jdom.Document
import org.jdom.Element
import org.junit.experimental.categories.Category
import org.openeai.config.AppConfig
import org.openeai.config.CommandConfig
import org.openeai.jms.producer.PointToPointProducer
import org.openeai.jms.producer.ProducerPool
import org.openeai.jms.producer.PubSubProducer
import org.openeai.jms.producer.QueueRequestor
import org.openeai.layouts.XmlLayout
import org.openeai.moa.XmlEnterpriseObject
import org.openeai.moa.objects.resources.Authentication
import org.openeai.moa.objects.resources.MessageId
import org.openeai.transport.ProducerId
import spock.lang.Specification

import javax.jms.BytesMessage
import javax.jms.Destination
import javax.jms.JMSException
import javax.jms.MapMessage
import javax.jms.Message
import javax.jms.MessageConsumer
import javax.jms.MessageListener
import javax.jms.MessageProducer
import javax.jms.ObjectMessage
import javax.jms.Queue
import javax.jms.QueueBrowser
import javax.jms.QueueReceiver
import javax.jms.QueueSender
import javax.jms.QueueSession
import javax.jms.StreamMessage
import javax.jms.TemporaryQueue
import javax.jms.TemporaryTopic
import javax.jms.TextMessage
import javax.jms.Topic
import javax.jms.TopicSubscriber
import java.util.function.Function

@Category(UTCategoryFast.class)
class SrdAccountNotificationCommandTest extends Specification {
    def "Low priority for failed detection"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetectionWithDetectionStatus(DetectionStatus.DETECTION_FAILED,
                "failed detection")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Low"
        annotation == "SRDOBJECT,Example,DETECTION_FAILED,null,arn:partition:service:region:123456789012"
    }

    def "Low priority for detection timed out"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetectionWithDetectionStatus(DetectionStatus.DETECTION_TIMEOUT,
                "detection timed out")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Low"
        annotation == "SRDOBJECT,Example,DETECTION_TIMEOUT,null,arn:partition:service:region:123456789012"
    }

    def "Low priority for no remediator for certain detectors"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetection(DetectionType.Example, "none",
                RemediationStatus.REMEDIATION_SUCCESS, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Low"
        annotation == "SRDOBJECT,Example,DETECTION_SUCCESS,REMEDIATION_NOTIFICATION_ONLY,arn:aws"
    }

    def "Low priority for certain notification-only remediator"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        SecurityRiskDetection securityRiskDetection = createSecurityRiskDetection(DetectionType.Example, "NotNone",
                RemediationStatus.REMEDIATION_NOTIFICATION_ONLY, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Low"
        annotation == "SRDOBJECT,Example,DETECTION_SUCCESS,REMEDIATION_NOTIFICATION_ONLY,arn:aws"
    }

    def "High priority for notification-only RedshiftSSLDisabled remediator"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        SecurityRiskDetection securityRiskDetection = createSecurityRiskDetection(DetectionType.RedshiftSSLDisabled, "NotNone",
                RemediationStatus.REMEDIATION_NOTIFICATION_ONLY, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "High"
        annotation == "SRDOBJECT,RedshiftSSLDisabled,DETECTION_SUCCESS,REMEDIATION_NOTIFICATION_ONLY,arn:aws"
    }

    def "Medium priority for failed remediation"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetection(DetectionType.UnregisteredVpc, "NotNone",
                RemediationStatus.REMEDIATION_FAILED, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Medium"
        annotation == "SRDOBJECT,UnregisteredVpc,DETECTION_SUCCESS,REMEDIATION_FAILED,arn:aws"
    }

    def "High priority for remediation timed out"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetection(DetectionType.MediaStorePublicContainer, "NotNone",
                RemediationStatus.REMEDIATION_TIMEOUT, "remediation timed out")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "High"
        annotation == "SRDOBJECT,MediaStorePublicContainer,DETECTION_SUCCESS,REMEDIATION_TIMEOUT,arn:aws"
    }

    def "High priority for successfully remediated risk"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetection(DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet, "NotNone",
                RemediationStatus.REMEDIATION_SUCCESS, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "High"
        annotation == "SRDOBJECT,CodeBuildVPCMisconfigurationInMgmtSubnet,DETECTION_SUCCESS,REMEDIATION_SUCCESS,arn:aws"
    }

    def "Low priority for postponed remediation"() {
        setup:
        boolean xeoInstanceofAccountNotification = false
        String accountId = null
        String priority = null
        String annotation = null

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                if (xeo instanceof AccountNotification) {
                    AccountNotification accountNotification = xeo
                    xeoInstanceofAccountNotification = true
                    accountId = accountNotification.getAccountId()
                    priority = accountNotification.getPriority()
                    annotation = accountNotification.getAnnotation(0).getText()
                }

                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetection(DetectionType.Example, "NotNone",
                RemediationStatus.REMEDIATION_POSTPONED, "because")
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        xeoInstanceofAccountNotification
        accountId == "123456789012"
        priority == "Low"
        annotation == "SRDOBJECT,Example,DETECTION_SUCCESS,REMEDIATION_POSTPONED,arn:aws"
    }

    def "Skipping account notification for no risks"() {
        setup:
        boolean accountNotificationWasSkipped = true

        Function setAssertionVariables = new Function<XmlEnterpriseObject, Object>() {
            @Override
            Object apply(XmlEnterpriseObject xeo) {
                accountNotificationWasSkipped = false
                return null
            }
        }

        def securityRiskDetection = createSecurityRiskDetectionNoRisks()
        def command = createSrdAccountNotificationCommand(securityRiskDetection, setAssertionVariables)

        when:
        command.execute(1, getMinimalSecurityRiskDetectionCreateSyncMessage())

        then:
        accountNotificationWasSkipped
    }

    def "All risks have an account notification subject"() {
        setup:
        Function noop = new Function<XmlEnterpriseObject, Object>() {
            @Override Object apply(XmlEnterpriseObject xeo) { return null }
        }

        def securityRiskDetection = createSecurityRiskDetectionNoRisks()
        def command = createSrdAccountNotificationCommand(securityRiskDetection, noop)

        expect:
        for (DetectionType dt : DetectionType.values()) {
            String subject = command.getSubjectForRemediation(dt, RemediationStatus.REMEDIATION_SUCCESS)
            if (subject.startsWith("Remediated a risk for")) {
                throw new Exception("DetectionType " + dt.name() + " is missing an account notification subject")
            }
        }
    }

    def "All risks have an account notification priority mapping"() {
        setup:
        Function noop = new Function<XmlEnterpriseObject, Object>() {
            @Override Object apply(XmlEnterpriseObject xeo) { return null }
        }

        def securityRiskDetection = createSecurityRiskDetectionNoRisks()
        def command = createSrdAccountNotificationCommand(securityRiskDetection, noop)

        expect:
        for (DetectionType dt : DetectionType.values()) {
            String priorityMapping = command.priorityMappingForRemediation(dt, RemediationStatus.REMEDIATION_SUCCESS)
            if (priorityMapping == null) {
                throw new Exception("DetectionType " + dt.name() + " is missing a priority mapping for remediation")
            }
        }
    }

    SrdAccountNotificationCommand createSrdAccountNotificationCommand(def securityRiskDetection, Function setAssertionVariables) {
        Hashtable appConfigObjects = new Hashtable()
        appConfigObjects.put("SyncErrorPublisher".toLowerCase(), createSyncErrorPublisherPubSubProducer())
        appConfigObjects.put("AwsAccountP2pProducer".toLowerCase(), createAwsAccountP2pProducerPool())
        appConfigObjects.put(SecurityRiskDetection.class.getName(), securityRiskDetection)
        appConfigObjects.put("AccountNotification.v1_0".toLowerCase(), createAccountNotification(setAssertionVariables))

        AppConfig appConfig = Mock()
        appConfig.getObjects() >> appConfigObjects

        CommandConfig commandConfig = new CommandConfig()
        commandConfig.setAppConfig(appConfig)

        return new SrdAccountNotificationCommand(commandConfig)
    }

    TextMessage getMinimalSecurityRiskDetectionCreateSyncMessage() {
        // need a very basic message with a ControlAreaSync and a DataArea/NewData/SecurityRiskDetection
        def theText = '''<?xml version="1.0" encoding="UTF-8"?>
            <com.amazon.aws.Security.SecurityRiskDetection.Create>
              <ControlAreaSync messageObject="SecurityRiskDetection" messageAction="Create" messageType="Sync">
              </ControlAreaSync>
              <DataArea><NewData><SecurityRiskDetection></SecurityRiskDetection></NewData></DataArea>
            </com.amazon.aws.Security.SecurityRiskDetection.Create>'''

        return createTestingTextMessage(theText)
    }

    SecurityRiskDetection createSecurityRiskDetection(DetectionType detectionType,
                                                      String remediatorName,
                                                      RemediationStatus remediationStatus,
                                                      String remediationDescription) {
        XmlLayout mockLayout = Mock()

        RemediationResult remediationResult = new RemediationResult()
        remediationResult.setStatus(remediationStatus.getStatus())
        remediationResult.setDescription(remediationDescription)

        DetectedSecurityRisk detectedSecurityRisk = new DetectedSecurityRisk()
        detectedSecurityRisk.setType(detectionType.name())
        detectedSecurityRisk.setAmazonResourceName("arn:aws")
        detectedSecurityRisk.setRemediationResult(remediationResult)
        detectedSecurityRisk.setInputLayoutManager(mockLayout)
        detectedSecurityRisk.setOutputLayoutManager(mockLayout)

        SecurityRiskDetection securityRiskDetectionClone = new SecurityRiskDetection() {
            DetectionResult getDetectionResult() {
                DetectionResult detectionResult = new DetectionResult()
                detectionResult.setType(DetectionType.Example.name())
                detectionResult.setStatus(DetectionStatus.DETECTION_SUCCESS.getStatus())
                return detectionResult
            }
        }
        securityRiskDetectionClone.setAccountId("123456789012")
        securityRiskDetectionClone.setSecurityRiskRemediator(remediatorName)
        securityRiskDetectionClone.addDetectedSecurityRisk(detectedSecurityRisk)
        securityRiskDetectionClone.setInputLayoutManager(mockLayout)

        return new SecurityRiskDetection() {
            Object clone() { return securityRiskDetectionClone }
        }
    }

    SecurityRiskDetection createSecurityRiskDetectionNoRisks() {
        XmlLayout mockLayout = Mock()

        SecurityRiskDetection securityRiskDetectionClone = new SecurityRiskDetection() {
            DetectionResult getDetectionResult() {
                DetectionResult detectionResult = new DetectionResult()
                detectionResult.setType(DetectionType.Example.name())
                detectionResult.setStatus(DetectionStatus.DETECTION_SUCCESS.getStatus())
                return detectionResult
            }
        }
        securityRiskDetectionClone.setAccountId("123456789012")
        securityRiskDetectionClone.setSecurityRiskRemediator("NotNone")
        securityRiskDetectionClone.setInputLayoutManager(mockLayout)

        return new SecurityRiskDetection() {
            Object clone() { return securityRiskDetectionClone }
        }
    }

    SecurityRiskDetection createSecurityRiskDetectionWithDetectionStatus(DetectionStatus detectionStatus, String error) {
        XmlLayout mockLayout = Mock()

        SecurityRiskDetection securityRiskDetectionClone = new SecurityRiskDetection() {
            DetectionResult getDetectionResult() {
                DetectionResult detectionResult = new DetectionResult()
                detectionResult.setType(DetectionType.Example.name())
                detectionResult.setStatus(detectionStatus.getStatus())
                detectionResult.addError(error);
                return detectionResult
            }
        }
        securityRiskDetectionClone.setAccountId("123456789012")
        securityRiskDetectionClone.setSecurityRiskRemediator("NotNone")
        securityRiskDetectionClone.setInputLayoutManager(mockLayout)

        return new SecurityRiskDetection() {
            Object clone() { return securityRiskDetectionClone }
        }
    }

    AccountNotification createAccountNotification(Function setAssertionVariables) {
        XmlLayout setAssertionVariablesLayout = new XmlLayout() {
            Object buildOutputFromObject(XmlEnterpriseObject xeo) {
                // gets called during processing of the AccountNotification.create() call with the
                // AccountNotification object created from createAccountNotification()
                // so we now have access to the fields set like priority or annotation

                setAssertionVariables.apply(xeo)
                return new Element("AccountNotification")
            }
            void buildObjectFromInput(Object input, XmlEnterpriseObject xeo) {}
        }
        setAssertionVariablesLayout.init("", new Document(new Element("name")))


        AccountNotification accountNotificationClone = new AccountNotification() {
            Element setControlArea(Element controlArea) {
                return null
            }
        }

        Element createDocRootElement = new Element("RootElement")
                .addContent(new Element("ControlArea"))
                .addContent(new Element("DataArea")
                        .addContent(new Element("NewData")
                                .addContent(new Element("AccountNotification"))))
                .setNamespace(null)

        XmlLayout mockLayout = Mock()

        HashMap iManagers = new HashMap()
        iManagers.put("xml", mockLayout)

        accountNotificationClone.setInputLayoutManager(mockLayout)
        accountNotificationClone.setInputLayoutManagers(iManagers)
        accountNotificationClone.setOutputLayoutManager(setAssertionVariablesLayout)
        accountNotificationClone.setOutputLayoutManagers(iManagers)
        accountNotificationClone.setMessageId(new MessageId())
        accountNotificationClone.setAuthentication(new Authentication())
        accountNotificationClone.setCreateDoc(new Document(createDocRootElement))

        return new AccountNotification() {
            Object clone() { return accountNotificationClone }
        }
    }

    PubSubProducer createSyncErrorPublisherPubSubProducer() {
        PubSubProducer p = new PubSubProducer()
        p.setProducerStatus("Started")
        return p
    }

    ProducerPool createAwsAccountP2pProducerPool() {
        QueueSession queueSession = createQueueSession()
        Queue q = new Queue() {
            @Override String getQueueName() throws JMSException { return "" }
        }
        QueueRequestor queueRequestor = new QueueRequestor(queueSession, q) {
            // need a very basic message with a ControlAreaSync with a Result
            Message request(Message message) {
                def theText = '''<?xml version="1.0" encoding="UTF-8"?>
                    <com.amazon.aws.Security.SecurityRiskDetection.Create>
                      <ControlAreaSync messageCategory="com.amazon.aws.Security" messageObject="SecurityRiskDetection" messageAction="Create" messageRelease="1.0" messagePriority="9" messageType="Sync">
                        <Result></Result>
                      </ControlAreaSync>
                    </com.amazon.aws.Security.SecurityRiskDetection.Create>'''

                return createTestingTextMessage(theText)
            }
        }

        PointToPointProducer awsAccountP2pProducer = new PointToPointProducer()
        awsAccountP2pProducer.setProducerStatus("Started")
        awsAccountP2pProducer.setProducerId(new ProducerId())
        awsAccountP2pProducer.setQueueSession(queueSession)
        awsAccountP2pProducer.setQueueRequestor(queueRequestor)

        return new ProducerPool([awsAccountP2pProducer])
    }

    TextMessage createTestingTextMessage(String theText) {
        return new TextMessage() {
            @Override
            String getText() throws JMSException {
                return theText
            }
            @Override void setText(String s) throws JMSException {}
            @Override String getJMSMessageID() throws JMSException { return null }
            @Override void setJMSMessageID(String s) throws JMSException {}
            @Override long getJMSTimestamp() throws JMSException { return 0 }
            @Override void setJMSTimestamp(long l) throws JMSException {}
            @Override byte[] getJMSCorrelationIDAsBytes() throws JMSException { return new byte[0] }
            @Override void setJMSCorrelationIDAsBytes(byte[] bytes) throws JMSException {}
            @Override void setJMSCorrelationID(String s) throws JMSException {}
            @Override String getJMSCorrelationID() throws JMSException { return null }
            @Override Destination getJMSReplyTo() throws JMSException { return null }
            @Override void setJMSReplyTo(Destination destination) throws JMSException {}
            @Override Destination getJMSDestination() throws JMSException { return null }
            @Override void setJMSDestination(Destination destination) throws JMSException {}
            @Override int getJMSDeliveryMode() throws JMSException { return 0 }
            @Override void setJMSDeliveryMode(int i) throws JMSException {}
            @Override boolean getJMSRedelivered() throws JMSException { return false }
            @Override void setJMSRedelivered(boolean b) throws JMSException {}
            @Override String getJMSType() throws JMSException { return null }
            @Override void setJMSType(String s) throws JMSException {}
            @Override long getJMSExpiration() throws JMSException { return 0 }
            @Override void setJMSExpiration(long l) throws JMSException {}
            @Override int getJMSPriority() throws JMSException { return 0 }
            @Override void setJMSPriority(int i) throws JMSException {}
            @Override void clearProperties() throws JMSException {}
            @Override boolean propertyExists(String s) throws JMSException { return false }
            @Override boolean getBooleanProperty(String s) throws JMSException { return false }
            @Override byte getByteProperty(String s) throws JMSException { return 0 }
            @Override short getShortProperty(String s) throws JMSException { return 0 }
            @Override int getIntProperty(String s) throws JMSException { return 0 }
            @Override long getLongProperty(String s) throws JMSException { return 0 }
            @Override float getFloatProperty(String s) throws JMSException { return 0 }
            @Override double getDoubleProperty(String s) throws JMSException { return 0 }
            @Override String getStringProperty(String s) throws JMSException { return null }
            @Override Object getObjectProperty(String s) throws JMSException { return null }
            @Override Enumeration getPropertyNames() throws JMSException { return null }
            @Override void setBooleanProperty(String s, boolean b) throws JMSException {}
            @Override void setByteProperty(String s, byte b) throws JMSException {}
            @Override void setShortProperty(String s, short i) throws JMSException {}
            @Override void setIntProperty(String s, int i) throws JMSException {}
            @Override void setLongProperty(String s, long l) throws JMSException {}
            @Override void setFloatProperty(String s, float v) throws JMSException {}
            @Override void setDoubleProperty(String s, double v) throws JMSException {}
            @Override void setStringProperty(String s, String s1) throws JMSException {}
            @Override void setObjectProperty(String s, Object o) throws JMSException {}
            @Override void acknowledge() throws JMSException {}
            @Override void clearBody() throws JMSException {}
        }
    }

    QueueSession createQueueSession() {
        QueueSender queueSender = Mock()
        def textMessage = createTestingTextMessage("")

        return new QueueSession() {
            @Override QueueSender createSender(Queue queue) throws JMSException { return queueSender }
            @Override TextMessage createTextMessage() throws JMSException { return textMessage }
            @Override Queue createQueue(String s) throws JMSException { return null }
            @Override QueueReceiver createReceiver(Queue queue) throws JMSException { return null }
            @Override QueueReceiver createReceiver(Queue queue, String s) throws JMSException { return null }
            @Override QueueBrowser createBrowser(Queue queue) throws JMSException { return null }
            @Override QueueBrowser createBrowser(Queue queue, String s) throws JMSException { return null }
            @Override TemporaryQueue createTemporaryQueue() throws JMSException { return null }
            @Override BytesMessage createBytesMessage() throws JMSException { return null }
            @Override MapMessage createMapMessage() throws JMSException { return null }
            @Override Message createMessage() throws JMSException { return null }
            @Override ObjectMessage createObjectMessage() throws JMSException { return null }
            @Override ObjectMessage createObjectMessage(Serializable serializable) throws JMSException { return null }
            @Override StreamMessage createStreamMessage() throws JMSException { return null }
            @Override TextMessage createTextMessage(String s) throws JMSException { return null }
            @Override boolean getTransacted() throws JMSException { return false }
            @Override int getAcknowledgeMode() throws JMSException { return 0 }
            @Override void commit() throws JMSException {}
            @Override void rollback() throws JMSException {}
            @Override void close() throws JMSException {}
            @Override void recover() throws JMSException {}
            @Override MessageListener getMessageListener() throws JMSException { return null }
            @Override void setMessageListener(MessageListener messageListener) throws JMSException {}
            @Override void run() {}
            @Override MessageProducer createProducer(Destination destination) throws JMSException { return null }
            @Override MessageConsumer createConsumer(Destination destination) throws JMSException { return null }
            @Override MessageConsumer createConsumer(Destination destination, String s) throws JMSException { return null }
            @Override MessageConsumer createConsumer(Destination destination, String s, boolean b) throws JMSException { return null }
            @Override Topic createTopic(String s) throws JMSException { return null }
            @Override TopicSubscriber createDurableSubscriber(Topic topic, String s) throws JMSException { return null }
            @Override TopicSubscriber createDurableSubscriber(Topic topic, String s, String s1, boolean b) throws JMSException { return null }
            @Override TemporaryTopic createTemporaryTopic() throws JMSException { return null }
            @Override void unsubscribe(String s) throws JMSException {}
        }
    }
}
