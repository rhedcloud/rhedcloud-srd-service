package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AccessControlList
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.CanonicalGrantee
import com.amazonaws.services.s3.model.GetPublicAccessBlockRequest
import com.amazonaws.services.s3.model.GetPublicAccessBlockResult
import com.amazonaws.services.s3.model.GroupGrantee
import com.amazonaws.services.s3.model.Permission
import com.amazonaws.services.s3.model.PublicAccessBlockConfiguration
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class S3PublicBucketDetectorTest extends UTSpecification {
    static bucketName = "bucket"
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "detection when bucket grant includes all users"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AllUsers, Permission.FullControl)

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(true)
                        .withBlockPublicPolicy(true)
                        .withRestrictPublicBuckets(true))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "AllUsers with FullControl"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3PublicBucketAcl.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
    }

    def "detection when bucket grant includes all authenticated users"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(GroupGrantee.AuthenticatedUsers, Permission.FullControl)

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(true)
                        .withBlockPublicPolicy(true)
                        .withRestrictPublicBuckets(true))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "AuthenticatedUsers with FullControl"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3PublicBucketAcl.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
    }

    def "no detection when bucket grants only canonical users"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()
        acl.grantPermission(new CanonicalGrantee("id"), Permission.FullControl)

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(true)
                        .withBlockPublicPolicy(true)
                        .withRestrictPublicBuckets(true))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "no AllUsers or AuthenticatedUsers"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult

        then:
        risks.size() == 0
    }

//    def "detect: should detect when bucket object grant includes all users"() {
//    def "detect: should detect when bucket object grant includes all authenticated users"() {
//
//        s3.getObjectAcl(bucketName, bucketKey) >> new AccessControlList() {{
//            grantPermission(new CanonicalGrantee("id"), Permission.FullControl)
//            for (Grant grant : grants) {
//                grantPermission(grant.getGrantee(), grant.getPermission())
//            }
//        }}

    def "detection when bucket public access block missing"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()

        AmazonS3Exception NoSuchPublicAccessBlockConfigurationException = new AmazonS3Exception("exception")
        NoSuchPublicAccessBlockConfigurationException.setErrorCode("NoSuchPublicAccessBlockConfiguration")

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "NoSuchPublicAccessBlockConfigurationException"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> { throw NoSuchPublicAccessBlockConfigurationException }

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3PublicAccessBlockMisconfiguration.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
    }

    def "detection when bucket public access block misconfiguration"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(false)  // <-- the problems
                        .withBlockPublicPolicy(false)
                        .withRestrictPublicBuckets(false))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "misconfiguration"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3PublicAccessBlockMisconfiguration.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
    }

    def "no detection on correct bucket public access block configuration"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3PublicBucketDetector detector = init(new S3PublicBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        AccessControlList acl = new AccessControlList()

        AmazonS3Exception NoSuchPublicAccessBlockConfigurationException = new AmazonS3Exception("exception")
        NoSuchPublicAccessBlockConfigurationException.setErrorCode("NoSuchPublicAccessBlockConfiguration")

        GetPublicAccessBlockResult getPublicAccessBlockResult = new GetPublicAccessBlockResult()
                .withPublicAccessBlockConfiguration(new PublicAccessBlockConfiguration()
                        .withBlockPublicAcls(false)  // BlockPublicAcls is not checked
                        .withIgnorePublicAcls(true)
                        .withBlockPublicPolicy(true)
                        .withRestrictPublicBuckets(true))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then: "proper configuration"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketAcl(bucketName) >> acl
        1 * s3.getPublicAccessBlock(_ as GetPublicAccessBlockRequest) >> getPublicAccessBlockResult

        then:
        risks.size() == 0
    }
}
