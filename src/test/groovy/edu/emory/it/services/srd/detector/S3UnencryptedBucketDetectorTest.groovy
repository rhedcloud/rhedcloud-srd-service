package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.GetBucketEncryptionResult
import com.amazonaws.services.s3.model.ServerSideEncryptionConfiguration
import com.amazonaws.services.s3.model.ServerSideEncryptionRule
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class S3UnencryptedBucketDetectorTest extends UTSpecification {
    static bucketName = "bucket"
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "detection when bucket is not encrypted variation 1"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketDetector detector = init(new S3UnencryptedBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3UnencryptedBucket.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketEncryption(_) >> null
    }

    def "detection when bucket is not encrypted variation 2"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketDetector detector = init(new S3UnencryptedBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3UnencryptedBucket.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketEncryption(_) >> getBucketEncryptionResult
    }

    def "detection when bucket is not encrypted variation 3"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketDetector detector = init(new S3UnencryptedBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        ServerSideEncryptionConfiguration serverSideEncryptionConfiguration = new ServerSideEncryptionConfiguration()
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()
                .withServerSideEncryptionConfiguration(serverSideEncryptionConfiguration)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.S3UnencryptedBucket.name()
        risks.get(0).amazonResourceName == "arn:aws:s3:::${bucketName}"
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketEncryption(_) >> getBucketEncryptionResult
    }

    def "no detection when bucket is encrypted"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketDetector detector = init(new S3UnencryptedBucketDetector())
                .withClientBuilderOverride(s3)

        List<Bucket> buckets = Collections.singletonList(new Bucket(bucketName))

        ServerSideEncryptionConfiguration serverSideEncryptionConfiguration = new ServerSideEncryptionConfiguration()
                .withRules(new ServerSideEncryptionRule())
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()
                .withServerSideEncryptionConfiguration(serverSideEncryptionConfiguration)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
        1 * s3.listBuckets() >> buckets
        1 * s3.getBucketEncryption(_) >> getBucketEncryptionResult
    }
}
