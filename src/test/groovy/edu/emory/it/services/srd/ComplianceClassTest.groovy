package edu.emory.it.services.srd

import edu.emory.it.services.srd.annotations.EnhancedSecurityComplianceClass
import edu.emory.it.services.srd.annotations.HIPAAComplianceClass
import edu.emory.it.services.srd.annotations.StandardComplianceClass
import spock.lang.Specification

class ComplianceClassTest extends Specification {
    void "verify ComplianceClass innards"() {
        expect:
        ComplianceClass.Standard.clazz == StandardComplianceClass
        ComplianceClass.HIPAA.clazz == HIPAAComplianceClass
        ComplianceClass.EnhancedSecurity.clazz == EnhancedSecurityComplianceClass
    }
}
