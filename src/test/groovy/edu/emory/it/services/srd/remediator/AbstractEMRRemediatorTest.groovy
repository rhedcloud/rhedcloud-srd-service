package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient
import com.amazonaws.services.elasticmapreduce.model.AmazonElasticMapReduceException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class AbstractEMRRemediatorTest extends UTSpecification {
    static String clusterId = "AbstractEMRRemediatorTest_ClusterId"
    static String clusterName = "AbstractEMRRemediatorTest_ClusterName"
    static String region = "us-east-1"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "Risk ignored because cluster is TERMINATING"() {
        setup:
        AmazonElasticMapReduceClient emrClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EMRClusterUnencryptedRest,
                "arn:aws:elasticmapreduce:${region}:123456789012:${clusterId}/${clusterName}")

        initProperties(detected, "TERMINATING")

        // can use any of the implementations of AbstractEMRRemediator since they are all exactly the same
        AbstractEMRRemediator remediator = init(new EMRClusterUnencryptedRestRemediator())
                .withClientBuilderOverride(emrClient)

        when:
        remediator.terminateCluster(detected, "123456789012", srContext)

        then:
        AbstractSecurityRiskRemediator.isRiskIgnored(detected)
        AbstractSecurityRiskRemediator.getRiskIgnoredReason(detected).contains("already terminating")
    }

    def "Risk ignored because cluster is TERMINATED"() {
        setup:
        AmazonElasticMapReduceClient emrClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EMRClusterUnencryptedRest,
                "arn:aws:elasticmapreduce:${region}:123456789012:${clusterId}/${clusterName}")

        initProperties(detected, "TERMINATED")

        AbstractEMRRemediator remediator = init(new EMRClusterUnencryptedRestRemediator())
                .withClientBuilderOverride(emrClient)

        when:
        remediator.terminateCluster(detected, "123456789012", srContext)

        then:
        AbstractSecurityRiskRemediator.isRiskIgnored(detected)
        AbstractSecurityRiskRemediator.getRiskIgnoredReason(detected).contains("already terminated")
    }

    def "Risk ignored because cluster is TERMINATED_WITH_ERRORS"() {
        setup:
        AmazonElasticMapReduceClient emrClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EMRClusterUnencryptedRest,
                "arn:aws:elasticmapreduce:${region}:123456789012:${clusterId}/${clusterName}")

        initProperties(detected, "TERMINATED_WITH_ERRORS")

        AbstractEMRRemediator remediator = init(new EMRClusterUnencryptedRestRemediator())
                .withClientBuilderOverride(emrClient)

        when:
        remediator.terminateCluster(detected, "123456789012", srContext)

        then:
        AbstractSecurityRiskRemediator.isRiskIgnored(detected)
        AbstractSecurityRiskRemediator.getRiskIgnoredReason(detected).contains("already terminated_with_errors")
    }

    def "remediation success when cluster is READY"() {
        setup:
        AmazonElasticMapReduceClient emrClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EMRClusterUnencryptedRest,
                "arn:aws:elasticmapreduce:${region}:123456789012:${clusterId}/${clusterName}")

        initProperties(detected, "READY")

        AbstractEMRRemediator remediator = init(new EMRClusterUnencryptedRestRemediator())
                .withClientBuilderOverride(emrClient)

        when:
        remediator.terminateCluster(detected, "123456789012", srContext)

        then: "first turn off termination protection"
        1 * emrClient.setTerminationProtection({ /* it instanceof SetTerminationProtectionRequest */
                it.getJobFlowIds().get(0) == clusterId &&
                ! it.getTerminationProtected() })

        then: "then terminate"
        1 * emrClient.terminateJobFlows({ /* it instanceof TerminateJobFlowsRequest */
                it.getJobFlowIds().get(0) == clusterId })

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description == "EMR cluster '${clusterName}' (${clusterId}) terminated"
    }

    /*
     * TODO - requirement is to delete the cluster if the terminate failed
     *  but that's not implemented yet but demonstrate the current functionality anyway
     */
    def "remediation failure when cluster fails to terminate"() {
        setup:
        AmazonElasticMapReduceClient emrClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EMRClusterUnencryptedRest,
                "arn:aws:elasticmapreduce:${region}:123456789012:${clusterId}/${clusterName}")

        initProperties(detected, "READY")

        AbstractEMRRemediator remediator = init(new EMRClusterUnencryptedRestRemediator())
                .withClientBuilderOverride(emrClient)

        when:
        remediator.terminateCluster(detected, "123456789012", srContext)

        then: "first turn off termination protection"
        1 * emrClient.setTerminationProtection({ /* it instanceof SetTerminationProtectionRequest */
                it.getJobFlowIds().get(0) == clusterId &&
                ! it.getTerminationProtected() })

        then: "then fail to terminate"
        1 * emrClient.terminateJobFlows(_) >> { throw new AmazonElasticMapReduceException() }

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.getError(0).startsWith("Failed to terminate EMR cluster '${clusterName}' (${clusterId})")
    }

    def initProperties(DetectedSecurityRisk detected, String instanceStatus) {
        detected.getProperties().setProperty("clusterState", instanceStatus)
        detected.getProperties().setProperty("clusterId", clusterId)
        detected.getProperties().setProperty("clusterName", clusterName)
        detected.getProperties().setProperty("region", region)
    }
}
