package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.elasticache.AmazonElastiCacheClient
import com.amazonaws.services.elasticache.model.AmazonElastiCacheException
import com.amazonaws.services.elasticache.model.CacheCluster
import com.amazonaws.services.elasticache.model.DescribeCacheClustersResult
import com.amazonaws.services.elasticache.model.DescribeReplicationGroupsResult
import edu.emory.it.services.srd.DetectionStatus
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class RdsHipaaIneligibleMemcachedDetectorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no cluster no risk"() {
        setup:
        AmazonElastiCacheClient elastiCacheClient = Mock()

        RdsHipaaIneligibleMemcachedDetector detector = init(new RdsHipaaIneligibleMemcachedDetector())
                .withClientBuilderOverride(elastiCacheClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def describeReplicationGroups_none = new DescribeReplicationGroupsResult()
        def describeCacheClusters_none = new DescribeCacheClustersResult()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * elastiCacheClient.describeReplicationGroups(_) >> describeReplicationGroups_none
        1 * elastiCacheClient.describeCacheClusters(_) >> describeCacheClusters_none

        then:
        risks.size() == 0
    }

    def "any cluster is a risk"() {
        setup:
        AmazonElastiCacheClient elastiCacheClient = Mock()

        RdsHipaaIneligibleMemcachedDetector detector = init(new RdsHipaaIneligibleMemcachedDetector())
                .withClientBuilderOverride(elastiCacheClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def describeReplicationGroups_none = new DescribeReplicationGroupsResult()
        def describeCacheClustersResult1 = new DescribeCacheClustersResult()
                .withCacheClusters(  // multiple cache clusters for inner detector loop
                        new CacheCluster().withCacheClusterId("test1a").withEngine("memcached"),
                        new CacheCluster().withCacheClusterId("test1b").withEngine("memcached"))
                .withMarker("marker")  // marker for outer detector loop
        def describeCacheClustersResult2 = new DescribeCacheClustersResult()
                .withCacheClusters(new CacheCluster().withCacheClusterId("test2").withEngine("memcached"))
                .withMarker(null)

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * elastiCacheClient.describeReplicationGroups(_) >> describeReplicationGroups_none
        2 * elastiCacheClient.describeCacheClusters(_) >>> [describeCacheClustersResult1, describeCacheClustersResult2]

        then:
        risks.size() == 3
    }

    def "defensive against exceptions"() {
        setup:
        AmazonElastiCacheClient elastiCacheClient = Mock()

        RdsHipaaIneligibleMemcachedDetector detector = init(new RdsHipaaIneligibleMemcachedDetector())
                .withClientBuilderOverride(elastiCacheClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def describeReplicationGroups_none = new DescribeReplicationGroupsResult()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()
        DetectionResult detectionResult = detection.detectionResult

        then:
        1 * elastiCacheClient.describeReplicationGroups(_) >> describeReplicationGroups_none
        1 * elastiCacheClient.describeCacheClusters(_) >> { throw new AmazonElastiCacheException("exception") }

        then:
        risks.size() == 0  // no risks but failed detection
        detectionResult.type == DetectionType.RdsHipaaIneligibleMemcached.name()
        detectionResult.status == DetectionStatus.DETECTION_FAILED.status
        detectionResult.getError(0).startsWith("Error describing cache clusters")
    }
}
