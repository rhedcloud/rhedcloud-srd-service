package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.AmazonRDSException
import com.amazonaws.services.rds.model.DBCluster
import com.amazonaws.services.rds.model.DBClusterMember
import com.amazonaws.services.rds.model.DescribeDBClustersResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class AbstractRdsRemediatorTest extends UTSpecification {
    static String dbInstanceIdentifier = "Instance_Identifier"
    static String dbClusterIdentifier = "Cluster_Identifier"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "Remediation Postponed because of instance state"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "creation in-progress"
        initInstanceProperties(detected, "creating")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "snapshotIdentifierPrefix",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} creation in-progress"

        when: "modification in-progress"
        initInstanceProperties(detected, "modifying")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} modification in-progress"

        when: "start up in-progress"
        initInstanceProperties(detected, "starting")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} start up in-progress"

        when: "reboot in-progress"
        initInstanceProperties(detected, "rebooting")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} reboot in-progress"

        when: "backup in-progress"
        initInstanceProperties(detected, "backing-up")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} backup in-progress"

        when: "deletion in-progress"
        initInstanceProperties(detected, "deleting")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} deletion in-progress"

        when: "stopping in-progress"
        initInstanceProperties(detected, "stopping")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS db ${dbInstanceIdentifier} stopping in-progress"

        when: "stopped for cluster"
        detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:cluster:${dbInstanceIdentifier}")
        initClusterProperties(detected, "stopped")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.DELETE, srContext)

        then: "starting so it can be deleted"
        1 * rdsClient.startDBCluster(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_POSTPONED.getStatus()
        detected.remediationResult.description == "RDS cluster ${dbClusterIdentifier} is starting so it can be deleted"
    }

    def "Remediation Ignored because of instance state"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "already stopped"
        initInstanceProperties(detected, "stopped")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then:
        AbstractSecurityRiskRemediator.isRiskIgnored(detected)
        AbstractSecurityRiskRemediator.getRiskIgnoredReason(detected) == "RDS db ${dbInstanceIdentifier} already stopped"
    }

    def "remediation success when stopping instance"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "ready"
        initInstanceProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then: "stop as instructed"
        1 * rdsClient.stopDBInstance(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description.startsWith("RDS db ${dbInstanceIdentifier} stopped with snapshot")
    }

    def "remediation success when stopping cluster"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:cluster:${dbClusterIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "ready"
        initClusterProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then: "stop as instructed"
        1 * rdsClient.stopDBCluster(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description.startsWith("RDS cluster ${dbClusterIdentifier} stopped")
    }

    def "remediation success when deleting instance"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "ready"
        initInstanceProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.DELETE, srContext)

        then: "delete as instructed"
        1 * rdsClient.deleteDBInstance(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description.startsWith("RDS db ${dbInstanceIdentifier} deleted with snapshot")
    }

    def "remediation success when deleting cluster"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:cluster:${dbClusterIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        DescribeDBClustersResult describeDBClustersResult = new DescribeDBClustersResult()
                .withDBClusters(new DBCluster().withDBClusterIdentifier(dbClusterIdentifier).withDBClusterMembers(
                        new DBClusterMember()
                                .withDBInstanceIdentifier("reader instance")
                                .withIsClusterWriter(false),
                        new DBClusterMember()
                                .withDBInstanceIdentifier("writer instance")
                                .withIsClusterWriter(true)))

        when: "ready"
        initClusterProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.DELETE, srContext)

        then: "delete as instructed"
        1 * rdsClient.modifyDBCluster(_)
        1 * rdsClient.describeDBClusters(_) >> describeDBClustersResult
        2 * rdsClient.deleteDBInstance(_)
        1 * rdsClient.deleteDBCluster(_)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description.startsWith("RDS cluster ${dbClusterIdentifier} deleted with snapshot")
    }

    def "remediation failed when stopping instance"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "ready"
        initInstanceProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.STOP, srContext)

        then: "fail to stop as instructed"
        1 * rdsClient.stopDBInstance(_) >> { throw new AmazonRDSException() }

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.description == "Remediation Error"
        detected.remediationResult.getError(0).startsWith("Failed to stop RDS db ${dbInstanceIdentifier}")
    }

    def "remediation failed when deleting instance"() {
        setup:
        AmazonRDSClient rdsClient = Mock()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.RdsUnencryptedDatabase,
                "arn:aws:rds:us-east-1:123456789012:db:${dbInstanceIdentifier}")

        AbstractRdsRemediator remediator = init(new RdsSqlServerUnencryptedTransportRemediator())
                .withClientBuilderOverride(rdsClient)

        when: "ready"
        initInstanceProperties(detected, "ready")
        remediator.remediateDBInstance(detected,
                "123456789012", DetectionType.RdsUnencryptedDatabase, "sip",
                AbstractRdsRemediator.RemediationAction.DELETE, srContext)

        then: "fail to delete as instructed"
        1 * rdsClient.deleteDBInstance(_) >> { throw new AmazonRDSException() }

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.description == "Remediation Error"
        detected.remediationResult.getError(0).startsWith("Failed to delete RDS db ${dbInstanceIdentifier}")
    }

    def initInstanceProperties(DetectedSecurityRisk detected, String instanceStatus) {
        detected.getProperties().setProperty("DBInstanceStatus", instanceStatus)
        detected.getProperties().setProperty("DBInstanceIdentifier", dbInstanceIdentifier)
    }

    def initClusterProperties(DetectedSecurityRisk detected, String clusterStatus) {
        detected.getProperties().setProperty("DBClusterStatus", clusterStatus)
        detected.getProperties().setProperty("DBClusterIdentifier", dbClusterIdentifier)
    }
}
