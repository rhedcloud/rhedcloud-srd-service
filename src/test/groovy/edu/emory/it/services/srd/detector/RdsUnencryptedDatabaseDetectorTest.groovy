package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.AmazonRDSException
import com.amazonaws.services.rds.model.DBCluster
import com.amazonaws.services.rds.model.DBInstance
import com.amazonaws.services.rds.model.DescribeDBClustersResult
import com.amazonaws.services.rds.model.DescribeDBInstancesResult
import com.amazonaws.services.rds.model.DescribeGlobalClustersResult
import com.amazonaws.services.rds.model.GlobalCluster
import com.amazonaws.services.rds.model.GlobalClusterMember
import edu.emory.it.services.srd.DetectionStatus
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class RdsUnencryptedDatabaseDetectorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no risks when no instances or clusters"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([])

        then:
        risks.size() == 0
    }

    def "no risks when instances is encrypted"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance = new DBInstance()
                .withStorageEncrypted(true)

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([instance])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([])

        then:
        risks.size() == 0
    }

    def "no risks when cluster is encrypted"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBCluster cluster = new DBCluster()
                .withStorageEncrypted(true)

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([cluster])

        then:
        risks.size() == 0
    }

    def "risks when instances are not encrypted"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance1 = new DBInstance()
                .withStorageEncrypted(false)
                .withDBInstanceIdentifier("Instance_Identifier1")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1")
        DBInstance instance2 = new DBInstance()
                .withStorageEncrypted(true)
                .withDBInstanceIdentifier("Instance_Identifier2")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier2")

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([instance1, instance2])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([])

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.RdsUnencryptedDatabase.name()
        risks.get(0).amazonResourceName == "arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1"
        risks.get(0).properties.size() == 2
        risks.get(0).properties.getProperty("DBInstanceIdentifier") == "Instance_Identifier1"
        risks.get(0).properties.getProperty("DBInstanceStatus") == "available"
    }

    def "risks when clusters are not encrypted"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance = new DBInstance()
                .withStorageEncrypted(false)
                .withDBInstanceIdentifier("Instance_Identifier1")
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1")

        DBCluster cluster1 = new DBCluster()
                .withStorageEncrypted(false)
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier1")
        DBCluster cluster2 = new DBCluster()
                .withStorageEncrypted(true)
                .withDBClusterIdentifier("Cluster_Identifier2")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier2")

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([instance])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([cluster1, cluster2])

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.RdsUnencryptedDatabase.name()
        risks.get(0).amazonResourceName == "arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier1"
        risks.get(0).properties.size() == 2
        risks.get(0).properties.getProperty("DBClusterIdentifier") == "Cluster_Identifier1"
        risks.get(0).properties.getProperty("DBClusterStatus") == "available"
    }

    def "risks when global clusters are not encrypted"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance = new DBInstance()
                .withStorageEncrypted(false)
                .withDBInstanceIdentifier("Instance_Identifier1")
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1")

        DBCluster cluster1 = new DBCluster()
                .withStorageEncrypted(false)
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier1")
        DBCluster cluster2 = new DBCluster()
                .withStorageEncrypted(true)
                .withDBClusterIdentifier("Cluster_Identifier2")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier2")

        GlobalCluster globalCluster = new GlobalCluster()
                .withStorageEncrypted(false)
                .withGlobalClusterIdentifier("Global_Cluster_Identifier")
                .withStatus("available")
                .withGlobalClusterArn("arn:aws:rds::123456789012:global-cluster:Global_Cluster_Identifier")
                .withGlobalClusterMembers(new GlobalClusterMember()
                        .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier1"))

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([globalCluster])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([instance])
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([cluster1, cluster2])

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.RdsUnencryptedDatabase.name()
        risks.get(0).amazonResourceName == "arn:aws:rds::123456789012:global-cluster:Global_Cluster_Identifier"
        risks.get(0).properties.size() == 2
        risks.get(0).properties.getProperty("DBGlobalClusterIdentifier") == "Global_Cluster_Identifier"
        risks.get(0).properties.getProperty("DBGlobalClusterStatus") == "available"
    }

    def "risks when instances are not encrypted with pagination"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance1 = new DBInstance()
                .withStorageEncrypted(true)
                .withDBInstanceIdentifier("Instance_Identifier1")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1")
        DBInstance instance2 = new DBInstance()
                .withStorageEncrypted(false)
                .withDBInstanceIdentifier("Instance_Identifier2")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier2")
        def instanceResult1 = new DescribeDBInstancesResult()
                .withDBInstances([instance1])
                .withMarker("page1of2")
        def instanceResult2 = new DescribeDBInstancesResult()
                .withDBInstances([instance2])

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        2 * client.describeDBInstances(_) >>> [instanceResult1, instanceResult2]
        1 * client.describeDBClusters(_) >> new DescribeDBClustersResult().withDBClusters([])

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.RdsUnencryptedDatabase.name()
        risks.get(0).amazonResourceName == "arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier2"
        risks.get(0).properties.size() == 2
        risks.get(0).properties.getProperty("DBInstanceIdentifier") == "Instance_Identifier2"
        risks.get(0).properties.getProperty("DBInstanceStatus") == "available"
    }

    def "risks when clusters are not encrypted with pagination"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DBInstance instance = new DBInstance()
                .withStorageEncrypted(false)
                .withDBInstanceIdentifier("Instance_Identifier1")
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withDBInstanceStatus("available")
                .withDBInstanceArn("arn:aws:rds:us-east-1:123456789012:db:Instance_Identifier1")

        DBCluster cluster1 = new DBCluster()
                .withStorageEncrypted(true)
                .withDBClusterIdentifier("Cluster_Identifier1")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier1")
        DBCluster cluster2 = new DBCluster()
                .withStorageEncrypted(false)
                .withDBClusterIdentifier("Cluster_Identifier2")
                .withStatus("available")
                .withDBClusterArn("arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier2")
        def clusterResult1 = new DescribeDBClustersResult()
                .withDBClusters([cluster1])
                .withMarker("page1of2")
        def clusterResult2 = new DescribeDBClustersResult()
                .withDBClusters([cluster2])

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([instance])
        2 * client.describeDBClusters(_) >>> [clusterResult1, clusterResult2]

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.RdsUnencryptedDatabase.name()
        risks.get(0).amazonResourceName == "arn:aws:rds:us-east-1:123456789012:cluster:Cluster_Identifier2"
        risks.get(0).properties.size() == 2
        risks.get(0).properties.getProperty("DBClusterIdentifier") == "Cluster_Identifier2"
        risks.get(0).properties.getProperty("DBClusterStatus") == "available"
    }

    def "defensive against exceptions on instances"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> { throw new AmazonRDSException("exception") }
        0 * client.describeDBClusters(_)

        then:
        risks.size() == 0  // no risks but failed detection
        detectionResult.type == DetectionType.RdsUnencryptedDatabase.name()
        detectionResult.status == DetectionStatus.DETECTION_FAILED.status
        detectionResult.getError(0).startsWith("Error describing DB instances")
    }

    def "defensive against exceptions on clusters"() {
        setup:
        AmazonRDSClient client = Mock()

        RdsUnencryptedDatabaseDetector detector = init(new RdsUnencryptedDatabaseDetector())
                .withClientBuilderOverride(client)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        1 * client.describeGlobalClusters(_) >> new DescribeGlobalClustersResult().withGlobalClusters([])
        1 * client.describeDBInstances(_) >> new DescribeDBInstancesResult().withDBInstances([])
        1 * client.describeDBClusters(_) >> { throw new AmazonRDSException("exception") }

        then:
        risks.size() == 0  // no risks but failed detection
        detectionResult.type == DetectionType.RdsUnencryptedDatabase.name()
        detectionResult.status == DetectionStatus.DETECTION_FAILED.status
        detectionResult.getError(0).startsWith("Error describing DB clusters")
    }
}
