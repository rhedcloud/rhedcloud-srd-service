package edu.emory.it.services.srd

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.regions.Region
import com.amazonaws.services.ec2.model.DescribeSubnetsResult
import com.amazonaws.services.ec2.model.Subnet
import com.amazonaws.services.ec2.model.Tag
import edu.emory.it.services.srd.detector.AbstractSecurityRiskDetector
import edu.emory.it.services.srd.remediator.AbstractSecurityRiskRemediator
import org.jdom.Element
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFields
import spock.lang.Specification

class UTSpecification extends Specification {
    AWSCredentialsProvider credentialsProvider = Mock()
    String securityRole = "rhedcloud/RHEDcloudSecurityRiskDetectionServiceRole"
    List<Region> JUST_US_EAST_1 = [new Region(new InMemoryRegionImpl("us-east-1", null))]
    AppConfig appConfig

    AppConfig setupAppConfig(Hashtable additionalObjects) {
        EnterpriseFields enterpriseFields = Mock()
        enterpriseFields.getEODocRoot() >> {
            new Element("EnterpriseFieldsMock")
        }

        DetectedSecurityRisk detectedSecurityRisk = Mock()
        detectedSecurityRisk.clone() >> {
            def risk = new DetectedSecurityRisk()
            risk.ENTERPRISE_FIELDS = enterpriseFields
            return risk
        }

        Hashtable m_objects = new Hashtable()
        m_objects.put(DetectedSecurityRisk.class.getName(), detectedSecurityRisk)
        if (additionalObjects != null)
            m_objects.putAll(additionalObjects)

        AppConfig aConfig = Mock()
        aConfig.getObjects() >> m_objects

        return aConfig
    }


    public <T extends AbstractSecurityRiskDetector> T init(T detector) {
        return init(detector, null)
    }
    public <T extends AbstractSecurityRiskDetector> T init(T detector, Hashtable additionalObjects) {
        appConfig = setupAppConfig(additionalObjects)
        detector.init(appConfig, credentialsProvider, securityRole)
        return detector
    }
    public <T extends AbstractSecurityRiskRemediator> T init(T remediator) {
        return init(remediator, null)
    }
    public <T extends AbstractSecurityRiskRemediator> T init(T remediator, Hashtable additionalObjects) {
        appConfig = setupAppConfig(additionalObjects)
        remediator.init(appConfig, credentialsProvider, securityRole)
        return remediator
    }

    public DescribeSubnetsResult getDisallowedSubnetsInteractionEc2DescribeSubnetsResult() {
        return new DescribeSubnetsResult().withSubnets(
                 new Subnet().withTags(new Tag("Name", "MGMT1 - IT ONLY")).withSubnetId("subnet-04a05af0740561e10").withVpcId("vpc-04b5f86e39dbb4c38")
                ,new Subnet().withTags(new Tag("Name", "MGMT2 - LITS ONLY")).withSubnetId("subnet-0775750a2841d0cf0").withVpcId("vpc-04b5f86e39dbb4c38")
                ,new Subnet().withTags(new Tag("Name", "Private Subnet 1")).withSubnetId("subnet-0d5099277adc20d26").withVpcId("vpc-04b5f86e39dbb4c38")
                ,new Subnet().withTags(new Tag("Name", "Attachment Tier AZ-a")).withSubnetId("subnet-0a1c8f1c7a4c09e2d").withVpcId("vpc-06688ec4f41fcd168")
                ,new Subnet().withTags(new Tag("Name", "Attachment Tier AZ-b")).withSubnetId("subnet-088565f6bdffeee75").withVpcId("vpc-06688ec4f41fcd168")
                ,new Subnet().withTags(new Tag("Name", "Application Tier AZ-a")).withSubnetId("subnet-0b39465ece002a56d").withVpcId("vpc-06688ec4f41fcd168")
        )
    }
}
