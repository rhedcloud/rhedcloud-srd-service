package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPIClient
import com.amazonaws.services.resourcegroupstaggingapi.model.ErrorCode
import com.amazonaws.services.resourcegroupstaggingapi.model.FailureInfo
import com.amazonaws.services.resourcegroupstaggingapi.model.TagResourcesResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class TagPolicyViolationRemediatorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext context = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    // ################################# remediate ###################################

    def "test remediate - no tags on account"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationNoTagsOnAccount, "arn:aws:")

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.status
    }

    def "test remediate - no profile tag detection"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationNoProfileTag, "arn:aws:")

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.status
    }

    def "test remediate - no RTPS entry"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationNoRTPSEntry, "arn:aws:")

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.status
    }

    def "test remediate - bad ARN value"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "blargo")

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("Unexpected ARN at risk")
    }

    def "test remediate - incomplete ARN value"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:123456789101")

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("remediator has Malformed ARN - no service specified")
    }

    // ################################# handleDetectedRisk ###################################

    def "test handleDetectedRisk - missing violation properties for TagPolicyViolationBadValue"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:123456789101")

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        0 * client.tagResources(_)
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("missing violation properties")
    }

    def "test handleDetectedRisk - missing violation properties for TagPolicyViolationNoTag"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationNoTag, "arn:aws:123456789101")

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        0 * client.tagResources(_)
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("missing violation properties")
    }

    def "test handleDetectedRisk - empty tag name"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("", "value")
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:123456789101")
        detected.setProperties(properties)

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        0 * client.tagResources(_)
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("has empty tag name")
    }

    def "test handleDetectedRisk - empty tag value"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "")
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:123456789101")
        detected.setProperties(properties)

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        0 * client.tagResources(_)
        detected.remediationResult.error.size() == 1
        ((String) detected.remediationResult.error[0]).contains("has empty tag value")
    }

    def "test handleDetectedRisk - with tag name/value"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "value")
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:ec2:us-east-1:123456789101:customer-gateway/cg-1334444")
        detected.setProperties(properties)
        detected.setRemediationResult(new RemediationResult())

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        1 * client.tagResources(_) >> new TagResourcesResult().withFailedResourcesMap(Collections.emptyMap())
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
    }

    def "test handleDetectedRisk - with multiple tag name/value"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "value")
        properties.put("name2", "value2")
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationNoTag, "arn:aws:ec2:us-east-1:123456789101:customer-gateway/cg-1334444")
        detected.setProperties(properties)
        detected.setRemediationResult(new RemediationResult())

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        1 * client.tagResources(_) >> new TagResourcesResult().withFailedResourcesMap(Collections.emptyMap())
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
    }

    def "test handleDetectedRisk - with error during tagging"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "value")
        String arn = "arn:aws:elasticbeanstalk:us-east-1:123456789012:environment/App/env"
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, arn)
        detected.setProperties(properties)
        detected.setRemediationResult(new RemediationResult())

        // fail because resource isn't in the correct state
        def tagResourcesResult = new TagResourcesResult()
                .withFailedResourcesMap([arn: new FailureInfo()
                        .withErrorMessage("Environment named env is in an invalid state for this operation. Must be Ready.")
                        .withStatusCode(400)
                        .withErrorCode(ErrorCode.InvalidParameterException)])

        when:
        remediator.handleDetectedRisk(detected, context, client)

        then:
        1 * client.tagResources(_) >> tagResourcesResult
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.description == "Remediation Error"
        detected.remediationResult.getError(0).startsWith("Failed to add required tags to resource")
        detected.remediationResult.getError(0).contains("Environment named env is in an invalid state")
    }

    def "test remediate - with success"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationRemediator remediator = init(new TagPolicyViolationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "value")
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:ec2:us-east-1:123456789101:customer-gateway/cg-1334444")
        detected.setProperties(properties)

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        1 * client.tagResources(_) >> new TagResourcesResult().withFailedResourcesMap(Collections.emptyMap())
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
    }

    def "test remediate - as simulation"() {
        given:
        AWSResourceGroupsTaggingAPIClient client = Mock()
        TagPolicyViolationSimulationRemediator remediator = init(new TagPolicyViolationSimulationRemediator())
                .withClientBuilderOverride(client)
        Properties properties = new Properties()
        properties.put("name", "value")
        // specifically use S3 ARN since it doesn't have an account or region in the ARN
        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.TagPolicyViolationBadValue, "arn:aws:s3:::mybucket")
        detected.setProperties(properties)

        when:
        remediator.remediate("123456789101", detected, context)

        then:
        0 * client.tagResources(_)   // ensure tagResources doesn't get called in simulation mode
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        detected.remediationResult.description.contains("Simulation Only")
    }
}
