package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.Subnet
import com.amazonaws.services.ec2.model.Tag
import com.amazonaws.services.lambda.AWSLambdaClient
import com.amazonaws.services.lambda.model.FunctionConfiguration
import com.amazonaws.services.lambda.model.ListFunctionsResult
import com.amazonaws.services.lambda.model.VpcConfigResponse
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class LambdaResidesWithinManagementSubnetsDetectorTest extends UTSpecification {
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when no functions"() {
        setup:
        AWSLambdaClient lambdaClient = Mock()
        AmazonEC2Client ec2 = Mock()

        LambdaResidesWithinManagementSubnetsDetector detector = init(new LambdaResidesWithinManagementSubnetsDetector())
                .withClientBuilderOverride(lambdaClient)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        ListFunctionsResult listFunctionsResult_noFunctions = new ListFunctionsResult()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * ec2.describeSubnets() >> getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        1 * lambdaClient.listFunctions(_) >> listFunctionsResult_noFunctions

        then:
        risks.size() == 0
    }

    def "no detection when function has no VPC"() {
        setup:
        AWSLambdaClient lambdaClient = Mock()
        AmazonEC2Client ec2 = Mock()

        LambdaResidesWithinManagementSubnetsDetector detector = init(new LambdaResidesWithinManagementSubnetsDetector())
                .withClientBuilderOverride(lambdaClient)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        ListFunctionsResult listFunctionsResult_noVpc = new ListFunctionsResult().withFunctions(
                new FunctionConfiguration().withFunctionArn("arn:aws:lambda:us-east-1:123456789012:function:f1")
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * ec2.describeSubnets() >> getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        1 * lambdaClient.listFunctions(_) >> listFunctionsResult_noVpc

        then:
        risks.size() == 0
    }

    def "no detection when function in allowed subnet"() {
        setup:
        AWSLambdaClient lambdaClient = Mock()
        AmazonEC2Client ec2 = Mock()

        LambdaResidesWithinManagementSubnetsDetector detector = init(new LambdaResidesWithinManagementSubnetsDetector())
                .withClientBuilderOverride(lambdaClient)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def disallowedSubnetsInteractionEc2DescribeSubnetsResult = getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        Subnet allowedSubnet = disallowedSubnetsInteractionEc2DescribeSubnetsResult.subnets.find({
            ((Subnet) it).tags.any({((Tag) it).key == "Name" && ((Tag) it).value == "Private Subnet 1" })
        })

        ListFunctionsResult listFunctionsResult_allowedSubnet = new ListFunctionsResult().withFunctions(
                new FunctionConfiguration().withFunctionArn("arn:aws:lambda:us-east-1:123456789012:function:f1")
                        .withVpcConfig(new VpcConfigResponse().withSubnetIds(allowedSubnet.subnetId))
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * ec2.describeSubnets() >> disallowedSubnetsInteractionEc2DescribeSubnetsResult
        1 * lambdaClient.listFunctions(_) >> listFunctionsResult_allowedSubnet

        then:
        risks.size() == 0
    }

    def "risk detection when function in disallowed subnet"() {
        setup:
        AWSLambdaClient lambdaClient = Mock()
        AmazonEC2Client ec2 = Mock()

        LambdaResidesWithinManagementSubnetsDetector detector = init(new LambdaResidesWithinManagementSubnetsDetector())
                .withClientBuilderOverride(lambdaClient)
                .withClientBuilderOverride(ec2)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        def disallowedSubnetsInteractionEc2DescribeSubnetsResult = getDisallowedSubnetsInteractionEc2DescribeSubnetsResult()
        Subnet disallowedSubnet = disallowedSubnetsInteractionEc2DescribeSubnetsResult.subnets.find({
            ((Subnet) it).tags.any({((Tag) it).key == "Name" && ((Tag) it).value == "Attachment Tier AZ-a" })
        })

        ListFunctionsResult listFunctionsResult_disallowedSubnet = new ListFunctionsResult().withFunctions(
                new FunctionConfiguration().withFunctionArn("arn:aws:lambda:us-east-1:123456789012:function:f1")
                        .withVpcConfig(new VpcConfigResponse().withSubnetIds(disallowedSubnet.subnetId))
        )

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        1 * ec2.describeSubnets() >> disallowedSubnetsInteractionEc2DescribeSubnetsResult
        1 * lambdaClient.listFunctions(_) >> listFunctionsResult_disallowedSubnet

        then:
        risks.size() == 1
    }
}
