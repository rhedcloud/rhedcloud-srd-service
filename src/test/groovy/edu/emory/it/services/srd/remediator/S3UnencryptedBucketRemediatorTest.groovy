package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CopyPartResult
import com.amazonaws.services.s3.model.GetBucketEncryptionResult
import com.amazonaws.services.s3.model.GetObjectMetadataRequest
import com.amazonaws.services.s3.model.InitiateMultipartUploadResult
import com.amazonaws.services.s3.model.ListObjectsV2Request
import com.amazonaws.services.s3.model.ListObjectsV2Result
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.amazonaws.services.s3.model.ServerSideEncryptionConfiguration
import com.amazonaws.services.s3.model.ServerSideEncryptionRule
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class S3UnencryptedBucketRemediatorTest extends UTSpecification {
    static bucketName = "bucket"
    static bucketKey = "bucketKey"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "remediate should set bucket default encryption even with no files"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketRemediator remediator = init(new S3UnencryptedBucketRemediator())
                .withClientBuilderOverride(s3)

        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3UnencryptedBucket, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        1 * s3.getBucketEncryption(bucketName) >> getBucketEncryptionResult
        1 * s3.setBucketEncryption(_)
        1 * s3.listObjectsV2(_ as ListObjectsV2Request) >> new ListObjectsV2Result()  // no files
        0 * s3.getObjectMetadata(_)
        0 * s3.copyObject(_)
        0 * s3.deleteObject(_)
        0 * s3.copyPart(_)
        0 * s3.completeMultipartUpload(_)
    }

    def "remediate should set bucket default encryption even with encrypted files"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketRemediator remediator = init(new S3UnencryptedBucketRemediator())
                .withClientBuilderOverride(s3)

        // bucket is not encrypted
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()

        // one small object
        S3ObjectSummary s3ObjectSummary = new S3ObjectSummary()
        s3ObjectSummary.bucketName = bucketName
        s3ObjectSummary.key = bucketKey
        ListObjectsV2Result listObjectsV2Result = new ListObjectsV2Result()
        listObjectsV2Result.objectSummaries.add(s3ObjectSummary)

        // the object is encrypted
        ObjectMetadata objectMetadata = new ObjectMetadata()
        objectMetadata.setSSEAlgorithm("AES256")

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3UnencryptedBucket, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        1 * s3.getBucketEncryption(bucketName) >> getBucketEncryptionResult
        1 * s3.setBucketEncryption(_)
        1 * s3.listObjectsV2(_ as ListObjectsV2Request) >> listObjectsV2Result
        1 * s3.getObjectMetadata(_ as GetObjectMetadataRequest) >> objectMetadata
        0 * s3.copyObject(_)
        0 * s3.deleteObject(_)
        0 * s3.copyPart(_)
        0 * s3.completeMultipartUpload(_)
    }

    def "remediate should set bucket default encryption and encrypt files"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketRemediator remediator = init(new S3UnencryptedBucketRemediator())
                .withClientBuilderOverride(s3)

        // bucket is not encrypted
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()

        // one small object
        S3ObjectSummary s3ObjectSummary = new S3ObjectSummary()
        s3ObjectSummary.bucketName = bucketName
        s3ObjectSummary.key = bucketKey
        ListObjectsV2Result listObjectsV2Result = new ListObjectsV2Result()
        listObjectsV2Result.objectSummaries.add(s3ObjectSummary)

        // the object is not encrypted
        ObjectMetadata objectMetadata = new ObjectMetadata()

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3UnencryptedBucket, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        1 * s3.getBucketEncryption(bucketName) >> getBucketEncryptionResult
        1 * s3.setBucketEncryption(_)
        1 * s3.listObjectsV2(_ as ListObjectsV2Request) >> listObjectsV2Result
        1 * s3.getObjectMetadata(_ as GetObjectMetadataRequest) >> objectMetadata
        2 * s3.copyObject(_)
        2 * s3.deleteObject(_)
        0 * s3.copyPart(_)
        0 * s3.completeMultipartUpload(_)
    }

    def "remediate should set bucket default encryption and encrypt large files"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketRemediator remediator = init(new S3UnencryptedBucketRemediator())
                .withClientBuilderOverride(s3)

        // bucket is not encrypted
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()

        // one large object
        S3ObjectSummary s3ObjectSummary = new S3ObjectSummary()
        s3ObjectSummary.bucketName = bucketName
        s3ObjectSummary.key = bucketKey
        ListObjectsV2Result listObjectsV2Result = new ListObjectsV2Result()
        listObjectsV2Result.objectSummaries.add(s3ObjectSummary)

        // the object is not encrypted
        def greaterThan5gb = 5L * 1024 * 1024 * 1024 + 10
        ObjectMetadata objectMetadata = new ObjectMetadata()
        objectMetadata.setContentLength(greaterThan5gb)

        // multipart copies
        InitiateMultipartUploadResult initiateMultipartUploadResult = new InitiateMultipartUploadResult()
        initiateMultipartUploadResult.uploadId = "an-upload-id"

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3UnencryptedBucket, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        1 * s3.getBucketEncryption(bucketName) >> getBucketEncryptionResult
        1 * s3.setBucketEncryption(_)
        1 * s3.listObjectsV2(_ as ListObjectsV2Request) >> listObjectsV2Result
        1 * s3.getObjectMetadata(_ as GetObjectMetadataRequest) >> objectMetadata
        2 * s3.initiateMultipartUpload(_) >> initiateMultipartUploadResult
        2050 * s3.copyPart(_) >> new CopyPartResult()  // 5gb / 5mb * 2 + 2
        2 * s3.completeMultipartUpload(_)
        2 * s3.deleteObject(_)
        0 * s3.copyObject(_)
    }

    def "remediate no longer required when bucket default encryption is already set"() {
        setup:
        AmazonS3Client s3 = Mock()
        S3UnencryptedBucketRemediator remediator = init(new S3UnencryptedBucketRemediator())
                .withClientBuilderOverride(s3)

        ServerSideEncryptionConfiguration serverSideEncryptionConfiguration = new ServerSideEncryptionConfiguration()
                .withRules(new ServerSideEncryptionRule())
        GetBucketEncryptionResult getBucketEncryptionResult = new GetBucketEncryptionResult()
                .withServerSideEncryptionConfiguration(serverSideEncryptionConfiguration)

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.S3UnencryptedBucket, "arn:aws:s3:::${bucketName}")

        when:
        remediator.remediate("123456789101", detected, srContext)

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description != null
        1 * s3.getBucketEncryption(bucketName) >> getBucketEncryptionResult
        0 * s3.setBucketEncryption(_)
        0 * s3.listObjectsV2(_ as ListObjectsV2Request) >> new ListObjectsV2Result()  // no files
        0 * s3.getObjectMetadata(_)
        0 * s3.copyObject(_)
        0 * s3.deleteObject(_)
        0 * s3.copyPart(_)
        0 * s3.completeMultipartUpload(_)
    }
}
