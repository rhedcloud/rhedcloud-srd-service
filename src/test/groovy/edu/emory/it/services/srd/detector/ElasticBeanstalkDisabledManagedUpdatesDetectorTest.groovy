package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentManagedActionsResult
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentsResult
import com.amazonaws.services.elasticbeanstalk.model.EnvironmentDescription
import com.amazonaws.services.elasticbeanstalk.model.ManagedAction
import edu.emory.it.services.srd.DetectionStatus
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class ElasticBeanstalkDisabledManagedUpdatesDetectorTest extends UTSpecification {
    static Hashtable additionalObjects = new Hashtable()
    static {
        Properties properties = new Properties()
        properties.setProperty("whitelistedAccounts", "123456789012\n  \t  \n  \t , \n \t 210987654321 ")  // white space gets stripped
        PropertyConfig propertyConfig = new PropertyConfig()
        propertyConfig.setProperties(properties)
        additionalObjects.put("ElasticBeanstalkDisabledManagedUpdates".toLowerCase(), propertyConfig)
    }

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "no detection when account whitelisted"() {
        setup:
        AWSElasticBeanstalkClient elasticBeanstalkClient = Mock()

        ElasticBeanstalkDisabledManagedUpdatesDetector detector = init(new ElasticBeanstalkDisabledManagedUpdatesDetector(), additionalObjects)
                .withClientBuilderOverride(elasticBeanstalkClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        0 * elasticBeanstalkClient.describeEnvironments()

        then:
        risks.size() == 0
        detectionResult.type == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        detectionResult.status == DetectionStatus.DETECTION_SUCCESS.status
    }

    def "whitelistedAccounts property is optional"() {
        setup:
        AWSElasticBeanstalkClient elasticBeanstalkClient = Mock()

        ElasticBeanstalkDisabledManagedUpdatesDetector detector = init(new ElasticBeanstalkDisabledManagedUpdatesDetector())
                .withClientBuilderOverride(elasticBeanstalkClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DescribeEnvironmentsResult describeEnvironmentsResult = new DescribeEnvironmentsResult()
                .withEnvironments(new EnvironmentDescription()
                        .withStatus("Ready")
                        .withHealthStatus("Ok")
                        .withEnvironmentName("rhedcloud-srd-service-dev")
                        .withEnvironmentId("e-1a2b3c4d5e")
                        .withEnvironmentArn("arn:aws:elasticbeanstalk:us-east-1:123456789012:environment/RHEDcloud Security Risk Detector Service/rhedcloud-srd-service-dev"))

        DescribeEnvironmentManagedActionsResult noManagedActions = new DescribeEnvironmentManagedActionsResult()

        when:
        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        1 * elasticBeanstalkClient.describeEnvironments() >> describeEnvironmentsResult
        1 * elasticBeanstalkClient.describeEnvironmentManagedActions(_) >> noManagedActions

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        risks.get(0).amazonResourceName == describeEnvironmentsResult.environments[0].environmentArn
        detectionResult.type == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        detectionResult.status == DetectionStatus.DETECTION_SUCCESS.status
        // ensure properties are set that are used to pass information to the remediator
        risks.get(0).getProperties().size() == 1
        risks.get(0).getProperties().containsKey("environmentId")
        risks.get(0).getProperties().getProperty("environmentId") == describeEnvironmentsResult.environments[0].environmentId
    }

    def "risk because of no managed action"() {
        setup:
        AWSElasticBeanstalkClient elasticBeanstalkClient = Mock()

        ElasticBeanstalkDisabledManagedUpdatesDetector detector = init(new ElasticBeanstalkDisabledManagedUpdatesDetector(), additionalObjects)
                .withClientBuilderOverride(elasticBeanstalkClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DescribeEnvironmentsResult describeEnvironmentsResult = new DescribeEnvironmentsResult()
                .withEnvironments(new EnvironmentDescription()
                        .withStatus("Ready")
                        .withHealthStatus("Ok")
                        .withEnvironmentName("rhedcloud-srd-service-dev")
                        .withEnvironmentId("e-1a2b3c4d5e")
                        .withEnvironmentArn("arn:aws:elasticbeanstalk:us-east-1:234567890123:environment/RHEDcloud Security Risk Detector Service/rhedcloud-srd-service-dev"))

        DescribeEnvironmentManagedActionsResult noManagedActions = new DescribeEnvironmentManagedActionsResult()

        when:
        detector.detect(detection, "234567890123", srContext)  // account is not whitelisted
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        1 * elasticBeanstalkClient.describeEnvironments() >> describeEnvironmentsResult
        1 * elasticBeanstalkClient.describeEnvironmentManagedActions(_) >> noManagedActions

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        risks.get(0).amazonResourceName == describeEnvironmentsResult.environments[0].environmentArn
        detectionResult.type == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        detectionResult.status == DetectionStatus.DETECTION_SUCCESS.status
        // ensure properties are set that are used to pass information to the remediator
        risks.get(0).getProperties().size() == 1
        risks.get(0).getProperties().containsKey("environmentId")
        risks.get(0).getProperties().getProperty("environmentId") == describeEnvironmentsResult.environments[0].environmentId
    }

    def "no risk because of managed action"() {
        setup:
        AWSElasticBeanstalkClient elasticBeanstalkClient = Mock()

        ElasticBeanstalkDisabledManagedUpdatesDetector detector = init(new ElasticBeanstalkDisabledManagedUpdatesDetector(), additionalObjects)
                .withClientBuilderOverride(elasticBeanstalkClient)
                .withRegionOverride(JUST_US_EAST_1)

        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        DescribeEnvironmentsResult describeEnvironmentsResult = new DescribeEnvironmentsResult()
                .withEnvironments(new EnvironmentDescription()
                        .withStatus("Ready")
                        .withHealthStatus("Ok")
                        .withEnvironmentName("rhedcloud-srd-service-dev")
                        .withEnvironmentId("e-1a2b3c4d5e")
                        .withEnvironmentArn("arn:aws:elasticbeanstalk:us-east-1:234567890123:environment/RHEDcloud Security Risk Detector Service/rhedcloud-srd-service-dev"))

        DescribeEnvironmentManagedActionsResult withManagedActions = new DescribeEnvironmentManagedActionsResult()
                .withManagedActions(new ManagedAction())

        when:
        detector.detect(detection, "234567890123", srContext)  // account is not whitelisted
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk

        then:
        1 * elasticBeanstalkClient.describeEnvironments() >> describeEnvironmentsResult
        1 * elasticBeanstalkClient.describeEnvironmentManagedActions(_) >> withManagedActions

        then:
        risks.size() == 0
    }
}
