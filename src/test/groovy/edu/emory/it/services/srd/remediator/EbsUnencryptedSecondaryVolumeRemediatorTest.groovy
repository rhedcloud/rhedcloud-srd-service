package edu.emory.it.services.srd.remediator

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import com.amazonaws.services.ec2.model.DescribeVolumesResult
import com.amazonaws.services.ec2.model.DeviceType
import com.amazonaws.services.ec2.model.Instance
import com.amazonaws.services.ec2.model.InstanceStateChange
import com.amazonaws.services.ec2.model.Reservation
import com.amazonaws.services.ec2.model.StopInstancesResult
import com.amazonaws.services.ec2.model.Volume
import com.amazonaws.services.ec2.model.VolumeAttachment
import com.amazonaws.services.ec2.model.VolumeAttachmentState
import com.amazonaws.services.ec2.model.VolumeState
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTDetectedSecurityRisk
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.openeai.utils.lock.Lock

@Category(UTCategoryFast.class)
class EbsUnencryptedSecondaryVolumeRemediatorTest extends UTSpecification {
    static String INSTANCE_ID = "i-016d590b0bd35b091"
    static String ANOTHER_INSTANCE_ID = "i-07065dc8f212f92ce"
    static String VOLUME_ID = "vol-0b07321771c04af00"
    static String ROOT_DEVICE_NAME = "/dev/xvda"
    static String ATTACHED_DEVICE_NAME = "/dev/sda"

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "remediation error when no volume instances are available"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but no volumes"
        1 * ec2.describeVolumes(_) >> new DescribeVolumesResult()

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_FAILED.getStatus()
        detected.remediationResult.error[0] == "Volume '${VOLUME_ID}' cannot be found."
    }

    def "remediation no longer required when only volume is primary"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE)
                        .withAttachments(new VolumeAttachment()
                                .withInstanceId(INSTANCE_ID)  // <--- attached to the instance
                                .withDevice(ROOT_DEVICE_NAME) // <--- same device === not secondary
                                .withState(VolumeAttachmentState.Attached)))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but only primary volume"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Specified volume '${VOLUME_ID}' is a primary volume and does not need to be remediated."
    }

    def "remediation no longer required when volume is encrypted"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.TRUE))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but volume is encrypted"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Volume '${VOLUME_ID}' already fixed, it is already encrypted."
    }

    def "remediation no longer required when volume is not in use"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.Deleted)
                        .withEncrypted(Boolean.FALSE))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but volume is not in use"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Volume '${VOLUME_ID}' already stopped."
    }

    def "remediation no longer required when no volume attachment"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but volume is not attached"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Volume '${VOLUME_ID}' is detached from any instances. No change necessary."
    }

    def "remediation no longer required when volume is not attached"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE)  // <--- unencrypted
                        .withAttachments(new VolumeAttachment()
                                .withInstanceId(ANOTHER_INSTANCE_ID)  // <--- attached to another instance
                                .withDevice(ATTACHED_DEVICE_NAME) // <--- secondary
                                .withState(VolumeAttachmentState.Detached)))  // <--- not attached

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "but volume is not attached"
        1 * ec2.describeVolumes(_) >> describeVolumesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_NO_LONGER_REQUIRED.getStatus()
        detected.remediationResult.description == "Volume '${VOLUME_ID}' is detached from any instances. No change necessary."
    }

    def "remediation when secondary volume is unencrypted"() {
        setup:
        AmazonEC2Client ec2 = Mock()

        DescribeInstancesResult describeInstancesResult = new DescribeInstancesResult()
                .withReservations(new Reservation()
                        .withInstances(new Instance()
                                .withInstanceId(INSTANCE_ID)
                                .withRootDeviceType(DeviceType.Ebs)
                                .withRootDeviceName(ROOT_DEVICE_NAME)))

        DescribeVolumesResult describeVolumesResult = new DescribeVolumesResult()
                .withVolumes(new Volume()
                        .withVolumeId(VOLUME_ID)
                        .withState(VolumeState.InUse)
                        .withEncrypted(Boolean.FALSE)  // <--- unencrypted
                        .withAttachments(new VolumeAttachment()
                                .withInstanceId(ANOTHER_INSTANCE_ID)  // <--- attached to another instance
                                .withDevice(ATTACHED_DEVICE_NAME) // <--- secondary
                                .withState(VolumeAttachmentState.Attached)))

        StopInstancesResult stopInstancesResult = new StopInstancesResult()
                .withStoppingInstances(new InstanceStateChange()
                        .withInstanceId(ANOTHER_INSTANCE_ID))

        DetectedSecurityRisk detected = UTDetectedSecurityRisk.create(DetectionType.EbsUnencryptedSecondaryVolume, "arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}")

        EbsUnencryptedSecondaryVolumeRemediator remediator = init(new EbsUnencryptedSecondaryVolumeRemediator())
                .withClientBuilderOverride(ec2)

        when:
        remediator.remediate("123456789012", detected, srContext)

        then: "with an instance"
        1 * ec2.describeInstances(_) >> describeInstancesResult
        then: "with unencrypted secondary volume"
        1 * ec2.describeVolumes(_) >> describeVolumesResult
        then: "the instance is stopped"
        1 * ec2.stopInstances(_) >> stopInstancesResult

        then:
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description == "The following instances '${ANOTHER_INSTANCE_ID}' were stopped because volume 'arn:aws:ec2:us-east-1:123456789012:volume/${VOLUME_ID}' is unencrypted."
    }
}
