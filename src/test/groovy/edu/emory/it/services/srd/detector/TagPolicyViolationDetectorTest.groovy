package edu.emory.it.services.srd.detector

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.Property
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskResourceTaggingProfile
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPI
import com.amazonaws.services.resourcegroupstaggingapi.AWSResourceGroupsTaggingAPIClient
import com.amazonaws.services.resourcegroupstaggingapi.model.GetResourcesResult
import com.amazonaws.services.resourcegroupstaggingapi.model.ResourceTagMapping
import com.amazonaws.services.resourcegroupstaggingapi.model.Tag
import edu.emory.it.services.srd.DetectionStatus
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.UTCategoryFast
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.UTSpecification
import edu.emory.it.services.srd.util.AwsAccountServiceUtil
import edu.emory.it.services.srd.util.ResourceTaggingProfileServiceUtil
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.experimental.categories.Category
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.openeai.utils.lock.Lock
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.rhedcloud.moa.jmsobjects.tagging.v1_0.ResourceTaggingProfile
import org.rhedcloud.moa.objects.resources.v1_0.AccountMetadataFilter
import org.rhedcloud.moa.objects.resources.v1_0.ManagedTag
import org.rhedcloud.moa.objects.resources.v1_0.ServiceFilter
import org.spockframework.runtime.Sputnik

@Category(UTCategoryFast.class)
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(Sputnik.class)
@PrepareForTest([AwsAccountServiceUtil.class, ResourceTaggingProfileServiceUtil])
class TagPolicyViolationDetectorTest extends UTSpecification {
    String LOGTAG= "[TagPolicyViolationDetectorTest] "
    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)

    def "Test checkResourceTags - malformed resource ARN"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("gobbletygook")
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue")

        when:
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.detectedSecurityRisk
        DetectionResult detectionResult = detection.detectionResult

        then:
        risks.size() == 0  // no risks but failed detection
        detectionResult.type == DetectionType.TagPolicyViolationBadValue.name()
        detectionResult.status == DetectionStatus.DETECTION_FAILED.status
        detectionResult.getError(0).startsWith("Malformed ARN")
    }

    def "Test checkResourceTags - matched tags with no filters"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue")

        when:
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test checkResourceTags - matched tags with service filter and no account metadata filter"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")

        ServiceFilter serviceFilter = new ServiceFilter()
        serviceFilter.setServiceName("ec2")
        serviceFilter.setResourceName(["customer-gateway"])
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", serviceFilter)

        when:
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test checkResourceTags - matched tags with service filter without resource name and no account metadata filter"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")

        ServiceFilter serviceFilter = new ServiceFilter()
        serviceFilter.setServiceName("ec2")
        serviceFilter.setResourceName([])
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", serviceFilter)

        when:
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test checkResourceTags - matched tags with no service filter and account metadata filter"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")

        AccountMetadataFilter accountMetadataFilter = new AccountMetadataFilter()
        accountMetadataFilter.setPropertyName("Environment")
        accountMetadataFilter.setPropertyValue(["prod"])
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", null, accountMetadataFilter)

        Property accountProperty = new Property()
        accountProperty.setKey("Environment")
        accountProperty.setValue("prod")
        List<Property> accountProperties = [accountProperty]

        AwsAccountServiceUtil mockUtil = Mock(AwsAccountServiceUtil)
        PowerMockito.mockStatic(AwsAccountServiceUtil.class)

        when:
        Mockito.when(AwsAccountServiceUtil.getInstance())
                .thenReturn(mockUtil)
        1 * mockUtil.getAccountProperties(_,_) >> accountProperties
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test checkResourceTags - mismatched tag values with no filters"() {
        setup:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValueWrong")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue")

        when:
        detector.checkResourceTags(detection, resourceTagMapping, managedTag, "123456789012", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.TagPolicyViolationBadValue.name()
        risks.get(0).amazonResourceName == resourceTagMapping.resourceARN
        // ensure properties are set that are used to pass information to the remediator
        risks.get(0).getProperties().size() == 1
        risks.get(0).getProperties().containsKey(managedTag.tagName)
        risks.get(0).getProperties().getProperty(managedTag.tagName) == managedTag.tagValue
    }

    def "Test findResourcesWithTagHistory - resources found" () {
        given:
        AWSResourceGroupsTaggingAPI client = Mock(AWSResourceGroupsTaggingAPIClient)
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("srd:profile").withValue("abcdefghi")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:elastic-load-balancer/124")
        List<ResourceTagMapping> resourceTagMappings = [resourceTagMapping]

        when:
        1 * client.getResources(_) >> new GetResourcesResult().withResourceTagMappingList(resourceTagMappings).withPaginationToken("")
        List<ResourceTagMapping> mappings = detector.findResourcesWithTagHistory(client)

        then:
        mappings.size() == resourceTagMappings.size()
        mappings == resourceTagMappings
    }

    def "Test scanResourcesForTagPolicyViolations - no violations"() {
        given:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue"),
                        new Tag().withKey("srd:profile").withValue("profileId")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")

        AccountMetadataFilter accountMetadataFilter = new AccountMetadataFilter()
        accountMetadataFilter.setPropertyName("Environment")
        accountMetadataFilter.setPropertyValue(["prod"])
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", null, accountMetadataFilter)

        Property accountProperty = new Property()
        accountProperty.setKey("Environment")
        accountProperty.setValue("prod")
        List<Property> accountProperties = [accountProperty]

        List<ResourceTagMapping> mappings = [resourceTagMapping]
        ResourceTaggingProfile taggingProfile = new ResourceTaggingProfile()
        taggingProfile.setManagedTag([managedTag])

        AwsAccountServiceUtil mockUtil = Mock(AwsAccountServiceUtil)
        ResourceTaggingProfileServiceUtil mockRTPSUtil = Mock(ResourceTaggingProfileServiceUtil)
        PowerMockito.mockStatic(AwsAccountServiceUtil.class)
        PowerMockito.mockStatic(ResourceTaggingProfileServiceUtil)

        when:
        Mockito.when(AwsAccountServiceUtil.getInstance())
                .thenReturn(mockUtil)
        Mockito.when(ResourceTaggingProfileServiceUtil.getInstance())
                .thenReturn(mockRTPSUtil)
        1 * mockUtil.getAccountProperties(_,_) >> accountProperties
        1 * mockRTPSUtil.query(_,_) >> taggingProfile
        detector.scanResourcesForTagPolicyViolations(detection, mappings, "123456789012", "us-east-1", null, LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test detect - no violations"() {
        given:
        AWSResourceGroupsTaggingAPIClient resourceGroupsTaggingAPI = Mock()
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
                .withClientBuilderOverride(resourceGroupsTaggingAPI)
                .withRegionOverride(JUST_US_EAST_1)
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue"),
                        new Tag().withKey("srd:profile").withValue("profileId")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")
        List<ResourceTagMapping> resourceTagMappings = [resourceTagMapping]

        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", null, null)

        ResourceTaggingProfile taggingProfile = new ResourceTaggingProfile()
        taggingProfile.setManagedTag([managedTag])

        ResourceTaggingProfileServiceUtil mockRTPSUtil = Mock(ResourceTaggingProfileServiceUtil)
        PowerMockito.mockStatic(ResourceTaggingProfileServiceUtil)

        when:
        Mockito.when(ResourceTaggingProfileServiceUtil.getInstance())
                .thenReturn(mockRTPSUtil)
        1 * mockRTPSUtil.isServiceReady() >> true
        1 * mockRTPSUtil.query(_,_) >> taggingProfile
        1 * resourceGroupsTaggingAPI.getResources(_) >> new GetResourcesResult().withResourceTagMappingList(resourceTagMappings).withPaginationToken("")

        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()
        DetectionResult detectionResult = detection.detectionResult

        then:
        risks.size() == 0
        detectionResult.type == DetectionType.TagPolicyViolationNoTag.name()
        detectionResult.status == DetectionStatus.DETECTION_SUCCESS.status
    }

    def "Test detect - service not ready"() {
        given:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()

        ResourceTaggingProfileServiceUtil mockRTPSUtil = Mock(ResourceTaggingProfileServiceUtil)
        PowerMockito.mockStatic(ResourceTaggingProfileServiceUtil)

        when:
        Mockito.when(ResourceTaggingProfileServiceUtil.getInstance())
                .thenReturn(mockRTPSUtil)
        1 * mockRTPSUtil.isServiceReady() >> false

        detector.detect(detection, "123456789012", srContext)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()
        DetectionResult detectionResult = detection.detectionResult

        then:
        risks.size() == 0
        detectionResult.type == DetectionType.TagPolicyViolationNoTag.name()
        detectionResult.status == DetectionStatus.DETECTION_FAILED.status
        detectionResult.getError(0).startsWith("ResourceTaggingProfileServiceUtil not ready")
    }

    def "Test detect - simulation"() {
        given:
        AWSResourceGroupsTaggingAPIClient resourceGroupsTaggingAPI = Mock()
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
                .withClientBuilderOverride(resourceGroupsTaggingAPI)
                .withRegionOverride(JUST_US_EAST_1)
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue"),
                           new Tag().withKey("srd:profile").withValue("cimp:simulation")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")
        List<ResourceTagMapping> resourceTagMappings = [resourceTagMapping]

        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", null, null)

        ResourceTaggingProfile taggingProfile = new ResourceTaggingProfile()
        taggingProfile.setManagedTag([managedTag])

        ResourceTaggingProfileServiceUtil mockRTPSUtil = Mock(ResourceTaggingProfileServiceUtil)
        PowerMockito.mockStatic(ResourceTaggingProfileServiceUtil)

        when:
        Mockito.when(ResourceTaggingProfileServiceUtil.getInstance())
                .thenReturn(mockRTPSUtil)
        1 * mockRTPSUtil.isServiceReady() >> true
        1 * mockRTPSUtil.query(_,_) >> taggingProfile
        1 * resourceGroupsTaggingAPI.getResources(_) >> new GetResourcesResult().withResourceTagMappingList(resourceTagMappings).withPaginationToken("")

        SecurityRiskDetectionRequisition srdRequisition = new SecurityRiskDetectionRequisition() {
            SecurityRiskResourceTaggingProfile getSecurityRiskResourceTaggingProfile() {
                return new SecurityRiskResourceTaggingProfile() {
                    String getNamespace() { return "cimp" }
                    String getProfileName() { return "simulation" }
                    String getRevision() { return "1" }
                }
            }
        }
        SecurityRiskContext src = new SecurityRiskContext(this.class.simpleName + " ", lock, srdRequisition)

        detector.detect(detection, "123456789012", src)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()
        DetectionResult detectionResult = detection.detectionResult

        then:
        risks.size() == 0
        detectionResult.type == DetectionType.TagPolicyViolationNoTag.name()
        detectionResult.status == DetectionStatus.DETECTION_SUCCESS.status
    }

    def "Test scanResourcesForTagPolicyViolations - no violations with simulation"() {
        given:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue"),
                        new Tag().withKey("srd:profile").withValue("profileId")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")

        AccountMetadataFilter accountMetadataFilter = new AccountMetadataFilter()
        accountMetadataFilter.setPropertyName("Environment")
        accountMetadataFilter.setPropertyValue(["prod"])
        ManagedTag managedTag = buildManagedTag("TestTagName", "TestTagValue", null, accountMetadataFilter)

        Property accountProperty = new Property()
        accountProperty.setKey("Environment")
        accountProperty.setValue("prod")
        List<Property> accountProperties = [accountProperty]

        List<ResourceTagMapping> mappings = [resourceTagMapping]
        ResourceTaggingProfile taggingProfile = new ResourceTaggingProfile()
        taggingProfile.setManagedTag([managedTag])

        AwsAccountServiceUtil mockUtil = Mock(AwsAccountServiceUtil)
        ResourceTaggingProfileServiceUtil mockRTPSUtil = Mock(ResourceTaggingProfileServiceUtil)
        PowerMockito.mockStatic(AwsAccountServiceUtil.class)
        PowerMockito.mockStatic(ResourceTaggingProfileServiceUtil)

        when:
        Mockito.when(AwsAccountServiceUtil.getInstance())
                .thenReturn(mockUtil)
        Mockito.when(ResourceTaggingProfileServiceUtil.getInstance())
                .thenReturn(mockRTPSUtil)
        1 * mockUtil.getAccountProperties(_,_) >> accountProperties
        1 * mockRTPSUtil.query(_,_) >> taggingProfile
        detector.scanResourcesForTagPolicyViolations(detection, mappings, "123456789012", "us-east-1", "cimp:testing:987", LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 0
    }

    def "Test scanResourcesForTagPolicyViolations - no profile tag"() {
        given:
        TagPolicyViolationDetector detector = init(new TagPolicyViolationDetector())
        SecurityRiskDetection detection = UTSecurityRiskDetection.create()
        ResourceTagMapping resourceTagMapping = new ResourceTagMapping()
                .withTags([new Tag().withKey("TestTagName").withValue("TestTagValue")])
                .withResourceARN("arn:aws:ec2:us-east-1:123456789012:customer-gateway/cg-1334444")
        List<ResourceTagMapping> mappings = [resourceTagMapping]

        when:
        detector.scanResourcesForTagPolicyViolations(detection, mappings, "123456789012", "us-east-1", null, LOGTAG)
        List<DetectedSecurityRisk> risks = detection.getDetectedSecurityRisk()

        then:
        risks.size() == 1
        risks.get(0).type == DetectionType.TagPolicyViolationNoProfileTag.name()
        risks.get(0).amazonResourceName == resourceTagMapping.resourceARN
    }

    ManagedTag buildManagedTag(String tagName, String tagValue,
                               ServiceFilter serviceFilter = null, AccountMetadataFilter accountMetadataFilter = null) {
        ManagedTag managedTag = new ManagedTag() {
            ServiceFilter mtServiceFilter;
            AccountMetadataFilter mtAccountMetadataFilter

            ServiceFilter getServiceFilter() {
                return this.mtServiceFilter;
            }
            void setServiceFilter(ServiceFilter aServiceFilter) {
                super.setServiceFilter(aServiceFilter)
                this.mtServiceFilter = aServiceFilter;
            }
            ServiceFilter newServiceFilter() {
                return new ServiceFilter();
            }
            AccountMetadataFilter getAccountMetadataFilter() {
                return this.mtAccountMetadataFilter;
            }
            void setAccountMetadataFilter(AccountMetadataFilter aAccountMetadataFilter) {
                super.setAccountMetadataFilter(aAccountMetadataFilter)
                this.mtAccountMetadataFilter = aAccountMetadataFilter;
            }
            AccountMetadataFilter newAccountMetadataFilter() {
                return new AccountMetadataFilter();
            }
        }

        if (tagName != null)
            managedTag.setTagName(tagName)
        if (tagValue != null)
            managedTag.setTagValue(tagValue)
        if (serviceFilter != null)
            managedTag.setServiceFilter(serviceFilter)
        if (accountMetadataFilter != null)
            managedTag.setAccountMetadataFilter(accountMetadataFilter)

        return managedTag
    }
}
