package edu.emory.it.services

/**
 * RDS related integration tests.
 * Broken out because they're generally quite slow.
 *
 * Category #2 is for the non Postgres related tests (oracle and sqlserver).
 */
interface ITCategoryRds2 {}
