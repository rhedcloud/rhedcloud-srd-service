package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClient
import com.amazonaws.services.sqs.model.AddPermissionRequest
import com.amazonaws.services.sqs.model.CreateQueueResult
import com.amazonaws.services.sqs.model.GetQueueAttributesResult
import com.amazonaws.services.sqs.model.QueueDeletedRecentlyException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.SQSExternallySharedQueueDetector
import edu.emory.it.services.srd.remediator.SQSExternallySharedQueueRemediator
import edu.emory.it.services.srd.util.AWSPolicyUtil
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class SQSExternallySharedQueueTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonSQS sqs = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonSQSClient.class)

        SQSExternallySharedQueueDetector detector = setupDetector(credentialsProvider)
        SQSExternallySharedQueueRemediator remediator = setupRemediator(credentialsProvider)

        String queueUrl = createQueue(sqs)
        String queueName = queueUrl.substring(queueUrl.lastIndexOf("/") + 1)
        addExternalPermission(sqs, queueUrl)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a queue with an external id"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.SQSExternallySharedQueue.name() == it.getType() &&
                    ("arn:aws:sqs:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:${queueName}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a queue with an external id"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNewv)
        waitTillPolicyHasChanged(sqs, ITAccount.getAccountId(), queueUrl)
        then: "remediate remove the permission"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isExternallyShared(sqs, ITAccount.getAccountId(), queueUrl) == false


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.SQSExternallySharedQueue.name() == it.getType() &&
                    ("arn:aws:sqs:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:${queueName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteQueue(sqs, queueUrl)
    }

    private SQSExternallySharedQueueDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        SQSExternallySharedQueueDetector underTest = new SQSExternallySharedQueueDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private SQSExternallySharedQueueRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        SQSExternallySharedQueueRemediator underTest = new SQSExternallySharedQueueRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createQueue(AmazonSQS sqs) {
        CreateQueueResult result
        try {
            result = sqs.createQueue("srd-test-queue")
        } catch (QueueDeletedRecentlyException e) {
            sleep(60100)
            result = sqs.createQueue("srd-test-queue")
        }
        return result.getQueueUrl()

    }

    private void addExternalPermission(AmazonSQS sqs, String queueUrl) {
        AddPermissionRequest addPermissionRequest = new AddPermissionRequest()
                .withQueueUrl(queueUrl)
                .withAWSAccountIds(ITAccount.getMasterAccount())
                .withActions("*")
                .withLabel("test-label")
        sqs.addPermission(addPermissionRequest)
    }

    private boolean isExternallyShared(AmazonSQS sqs, String accountId, String queueUrl) {
        GetQueueAttributesResult queueAttributesResult = sqs.getQueueAttributes(queueUrl, Arrays.asList("Policy"))
        if (queueAttributesResult.getAttributes() != null) {
            def whitelistedAccounts = Collections.singleton(accountId)
            String policy = queueAttributesResult.getAttributes().get("Policy")
            return AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy);
        }

        return false
    }

    private void deleteQueue(AmazonSQS sqs, String queueUrl) {
        if (queueUrl) {
            sqs.deleteQueue(queueUrl)
        }
    }

    private void waitTillPolicyHasChanged(AmazonSQS sqs, String accountId, String queueUrl) {
        boolean done = false
        for (int i=0; i<10 && !done; i++) {
            if (i != 0) {
                sleep(5000)
            }
            done = !isExternallyShared(sqs, accountId, queueUrl)
        }
    }
}

