package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ecs.AmazonECS
import com.amazonaws.services.ecs.AmazonECSClient
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.ECSLaunchedWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.ECSLaunchedWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class ECSLaunchedWithinManagementSubnetsTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        ECSLaunchedWithinManagementSubnetsDetector detector = new ECSLaunchedWithinManagementSubnetsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        ECSLaunchedWithinManagementSubnetsRemediator remediator = new ECSLaunchedWithinManagementSubnetsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        def expectedARN = "arn:aws:ecs:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:service/${RdsDatabaseHelper.ecsServiceName}"

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.ECSLaunchedWithinManagementSubnets.name()
        detected.get(0).getAmazonResourceName() == expectedARN

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "ECS service ${expectedARN} deleted"
    }

    def setupSpec() {
        AmazonEC2 ec2Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonEC2Client.class)
        AmazonECS ecsClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonECSClient.class)

        RdsDatabaseHelper.createElasticContainerServiceInDisallowedSubnet(ec2Client, ecsClient)
    }
}
