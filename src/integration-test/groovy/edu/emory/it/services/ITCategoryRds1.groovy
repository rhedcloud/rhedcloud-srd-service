package edu.emory.it.services

/**
 * RDS related integration tests.
 * Broken out because they're generally quite slow.
 *
 * Category #1 is for the first Postgres related test.
 *
 * Other Postgres tests will be put in category #3 so that the instance
 *  can get closer to transitioning to a 'stopped' state before the tests run.
 */
interface ITCategoryRds1 {}
