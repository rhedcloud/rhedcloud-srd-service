package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.directory.AWSDirectoryService
import com.amazonaws.services.directory.AWSDirectoryServiceClient
import com.amazonaws.services.directory.model.CreateDirectoryRequest
import com.amazonaws.services.directory.model.CreateDirectoryResult
import com.amazonaws.services.directory.model.CreateMicrosoftADRequest
import com.amazonaws.services.directory.model.CreateMicrosoftADResult
import com.amazonaws.services.directory.model.DeleteDirectoryRequest
import com.amazonaws.services.directory.model.DeleteDirectoryResult
import com.amazonaws.services.directory.model.DescribeDirectoriesRequest
import com.amazonaws.services.directory.model.DescribeDirectoriesResult
import com.amazonaws.services.directory.model.DirectoryVpcSettings
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.CreateSubnetRequest
import com.amazonaws.services.ec2.model.CreateSubnetResult
import com.amazonaws.services.ec2.model.CreateVpcRequest
import com.amazonaws.services.ec2.model.CreateVpcResult
import com.amazonaws.services.ec2.model.DeleteSubnetRequest
import com.amazonaws.services.ec2.model.DeleteVpcRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.detector.DSIneligibleDirectoryDetector
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO disable ignore when account is given permission to describe directories and create a directory
class DSIneligibleDirectoryTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSDirectoryService directoryService = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSDirectoryServiceClient.class)

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        DSIneligibleDirectoryDetector detector = setupDetector(credentialsProvider)

        String vpcId = createVPC(ec2, "10.0.0.0/20")
        String subnet1 = createSubnet(ec2, vpcId, "10.0.0.0/25", "${ITAccount.getRegion()}a")
        String subnet2 = createSubnet(ec2, vpcId, "10.0.0.128/25", "${ITAccount.getRegion()}b")

        DirectoryVpcSettings directoryVpcSettings = new DirectoryVpcSettings(){{
            setVpcId(vpcId)
            setSubnetIds([subnet1, subnet2])
        }}

        String nonMicrosoftDirectoryId =  createDirectory(directoryService, "test3.example.com", directoryVpcSettings)
        String standardDirectoryId = createMicrosoftDirectory(directoryService, true, "test.example.com", directoryVpcSettings)
        String enterpriseDirectoryId =  createMicrosoftDirectory(directoryService, false, "test2.example.com", directoryVpcSettings)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a distribution where cache is not enabled"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedStandard = detected.find({
            DetectionType.DSIneligibleDirectory.name() == it.getType() &&
                    ("arn:aws:ds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:directory/${standardDirectoryId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detectedEnterprise = detected.find({
            DetectionType.DSIneligibleDirectory.name() == it.getType() &&
                    ("arn:aws:ds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:directory/${enterpriseDirectoryId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detectedNonMicrosoft = detected.find({
            DetectionType.DSIneligibleDirectory.name() == it.getType() &&
                    ("arn:aws:ds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:directory/${nonMicrosoftDirectoryId}") == it.getAmazonResourceName()
        })

        then: "only standard directory should be detected"
        detected.isEmpty() == false
        detectedStandard != null
        detectedEnterprise == null
        detectedNonMicrosoft != null


        cleanup:
        deleteDirectory(directoryService, nonMicrosoftDirectoryId)
        deleteDirectory(directoryService, standardDirectoryId)
        deleteDirectory(directoryService, enterpriseDirectoryId)
        waitForDeleteToFinish(directoryService, nonMicrosoftDirectoryId)
        waitForDeleteToFinish(directoryService, standardDirectoryId)
        waitForDeleteToFinish(directoryService, enterpriseDirectoryId)
        deleteSubnet(ec2, subnet1)
        deleteSubnet(ec2, subnet2)
        deleteVPC(ec2, vpcId)

    }


    private DSIneligibleDirectoryDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        DSIneligibleDirectoryDetector underTest = new DSIneligibleDirectoryDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

// class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createMicrosoftDirectory(AWSDirectoryService directoryService, boolean isStandard, String dnsName, DirectoryVpcSettings directoryVpcSettings) {
        CreateMicrosoftADRequest request = new CreateMicrosoftADRequest().withName(dnsName).withPassword("Testing!23").withVpcSettings(
                directoryVpcSettings
        )
        if (isStandard) {
            request.setEdition("Standard")
        } else {
            request.setEdition("Enterprise")
        }

        CreateMicrosoftADResult result = directoryService.createMicrosoftAD(request)
        return result.getDirectoryId()
    }

    private String createDirectory(AWSDirectoryService directoryService, String dnsName, DirectoryVpcSettings directoryVpcSettings) {
        CreateDirectoryRequest request = new CreateDirectoryRequest().withName(dnsName).withPassword("Testing!23").withSize("Small").withVpcSettings(
                directoryVpcSettings
        )
        CreateDirectoryResult result = directoryService.createDirectory(request)
        return result.getDirectoryId()
    }

    private String createVPC(AmazonEC2 ec2, String cidrBlock) {
        CreateVpcRequest request = new CreateVpcRequest().withCidrBlock(cidrBlock).withInstanceTenancy("default")
        CreateVpcResult result = ec2.createVpc(request)
        return result.getVpc().getVpcId()
    }

    private String createSubnet(AmazonEC2 ec2, String vpcId, String cidrBlock, String availabilityZone) {
        CreateSubnetRequest request = new CreateSubnetRequest().withVpcId(vpcId).withCidrBlock(cidrBlock).withAvailabilityZone(availabilityZone)
        CreateSubnetResult result = ec2.createSubnet(request)
        return result.getSubnet().getSubnetId()
    }

    private void deleteSubnet(AmazonEC2 ec2, String subnetId) {
        if (subnetId) {
            DeleteSubnetRequest request = new DeleteSubnetRequest().withSubnetId(subnetId)
            ec2.deleteSubnet(request)
        }
    }

    private void deleteVPC(AmazonEC2 ec2, String vpcId) {
        if (vpcId) {
            DeleteVpcRequest request = new DeleteVpcRequest().withVpcId(vpcId)
            ec2.deleteVpc(request)
        }
    }

    private void deleteDirectory(AWSDirectoryService directoryService, String directoryId) {
        if (directoryId) {
            DeleteDirectoryRequest request = new DeleteDirectoryRequest().withDirectoryId(directoryId)
            DeleteDirectoryResult result = directoryService.deleteDirectory(request)
            waitForDeleteToFinish(directoryService, directoryId)
        }
    }

    private void waitForDeleteToFinish(AWSDirectoryService directoryService, String directoryId) {
        if (directoryId) {
            boolean done = false
            for (int i = 0; i < 10 && !done; i++) {
                DescribeDirectoriesRequest describeDirectoriesRequest = new DescribeDirectoriesRequest().withDirectoryIds(directoryId)
                DescribeDirectoriesResult describeDirectoriesResult = directoryService.describeDirectories(describeDirectoriesRequest)
                done = describeDirectoriesResult.getDirectoryDescriptions().isEmpty()
                if (i != 0) {
                    sleep(60000)
                }
            }
        }
    }
}

