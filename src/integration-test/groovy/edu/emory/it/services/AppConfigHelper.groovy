package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult

class AppConfigHelper {
    static def createEOMock(def additionalObjects) {
        DetectedSecurityRisk detectedSecurityRisk = new DetectedSecurityRisk() {
            Object clone() {
                return new DetectedSecurityRisk() {
                    private RemediationResult m_remediationResult
                    private String m_type
                    private String m_amazonResourceName

                    RemediationResult getRemediationResult() { return this.m_remediationResult }
                    void setRemediationResult(RemediationResult v) { this.m_remediationResult = v }
                    String getType() { return this.m_type }
                    void setType(String v) { this.m_type = v }
                    String getAmazonResourceName() { return this.m_amazonResourceName }
                    void setAmazonResourceName(String v) { this.m_amazonResourceName = v }
                }
            }
        }

        RemediationResult remediationResult = new RemediationResult() {
            Object clone() {
                return new RemediationResult() {
                    private String m_status = null
                    private String m_description = null

                    String getStatus() { return this.m_status }
                    void setStatus(String v) { this.m_status = v }
                    String getDescription() { return this.m_description }
                    void setDescription(String v) { this.m_description = v }
                }
            }
        }

        Hashtable m_objects = new Hashtable()
        m_objects.put(DetectedSecurityRisk.class.getName(), detectedSecurityRisk)
        m_objects.put(RemediationResult.class.getName(), remediationResult)

        if (additionalObjects) {
            additionalObjects.each {
                k, v ->
                    m_objects.put(k, v)
            }
        }

        m_objects
    }
}
