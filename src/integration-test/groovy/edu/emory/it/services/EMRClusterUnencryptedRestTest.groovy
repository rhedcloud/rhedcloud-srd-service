package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduceClient
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.EMRClusterUnencryptedRestDetector
import edu.emory.it.services.srd.remediator.EMRClusterUnencryptedRestRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class EMRClusterUnencryptedRestTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        EMRClusterUnencryptedRestDetector detector = new EMRClusterUnencryptedRestDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        EMRClusterUnencryptedRestRemediator remediator = new EMRClusterUnencryptedRestRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            it.getProperties().get("clusterName") == RdsDatabaseHelper.emrClusterName &&
                    ((String) it.getProperties().get("clusterState")) in ["STARTING", "WAITING"]
        })

        then:
        detectedRisk != null
        detectedRisk.getType() == DetectionType.EMRClusterUnencryptedRest.name()
        detectedRisk.getAmazonResourceName().contains("elasticmapreduce")

        when:
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)

        then:
        detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        // the description contains the cluster id which is variable so just check for pieces of the description
        detectedRisk.getRemediationResult().getDescription().contains("EMR cluster ")
        detectedRisk.getRemediationResult().getDescription().contains(RdsDatabaseHelper.emrClusterName)
        detectedRisk.getRemediationResult().getDescription().contains(" terminated")
    }

    def setupSpec() {
        AmazonEC2 ec2Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonEC2Client.class)
        AmazonElasticMapReduce emrClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonElasticMapReduceClient.class)

        RdsDatabaseHelper.createElasticMapReduceClusterInDisallowedSubnet(ec2Client, emrClient)
    }
}
