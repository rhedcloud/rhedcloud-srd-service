package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.DBInstanceAlreadyExistsException
import com.amazonaws.services.rds.model.DBInstanceNotFoundException
import com.amazonaws.services.rds.model.InvalidDBInstanceStateException
import com.amazonaws.services.rds.model.OptionGroupAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RdsOracleUnencryptedTransportDetector
import edu.emory.it.services.srd.remediator.RdsOracleUnencryptedTransportRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryRds2.class)
class RdsOracleUnencryptedTransportTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RdsOracleUnencryptedTransportDetector detector = new RdsOracleUnencryptedTransportDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RdsOracleUnencryptedTransportRemediator remediator = new RdsOracleUnencryptedTransportRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.RdsOracleUnencryptedTransport.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:rds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:db:${RdsDatabaseHelper.oracleDBInstanceIdentifier}"
        // ensure properties are set that are used to pass information to the remediator
        detected.get(0).getProperties().size() == 2
        detected.get(0).getProperties().containsKey("DBInstanceIdentifier")
        detected.get(0).getProperties().containsKey("DBInstanceStatus")

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription().contains("stopped with snapshot")
    }

    def setupSpec() {
        AmazonRDS client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonRDSClient.class)

        RdsDatabaseHelper.deleteOldRdsSnapshots(client)

        // create the parameter group
        try {
            RdsDatabaseHelper.createOracleOptionGroup(client)
        } catch (OptionGroupAlreadyExistsException e) {
            // ignore
        }

        // make sure the parameter group has the correct NATIVE_NETWORK_ENCRYPTION setting
        RdsDatabaseHelper.modifyOracleOptionGroup(client)

        // modifying an option group may change the status of the instance to 'modifying'.
        // wait for it to become available again
        try {
            RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.oracleDBInstanceIdentifier, ['available', 'stopped'], client)
        } catch (DBInstanceNotFoundException e) {
            // ignore because we're about to create it
        }

        // create the DB instance
        try {
            RdsDatabaseHelper.createOracleWithEncryptedStorage(client)
        } catch (DBInstanceAlreadyExistsException e) {
            // ignore
        }

        // if the instance is still stopping from a previous run then wait for it to finish
        // and become stopped.
        try {
            RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.oracleDBInstanceIdentifier, ['available', 'stopped'], client)
        } catch (DBInstanceNotFoundException e) {
            // ignore because we're about to create it
        }

        // start up the instance
        try {
            RdsDatabaseHelper.startDBInstance(RdsDatabaseHelper.oracleDBInstanceIdentifier, client)
        } catch (InvalidDBInstanceStateException e) {
            // ignore - may already be running
        }

        // now wait for it to finish starting and become available
        RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.oracleDBInstanceIdentifier, ['available'], client)
    }
}