package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.AccessKeyMetadata
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyRequest
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyResult
import com.amazonaws.services.identitymanagement.model.CreateLoginProfileRequest
import com.amazonaws.services.identitymanagement.model.CreateLoginProfileResult
import com.amazonaws.services.identitymanagement.model.CreateServiceSpecificCredentialRequest
import com.amazonaws.services.identitymanagement.model.CreateServiceSpecificCredentialResult
import com.amazonaws.services.identitymanagement.model.DeleteAccessKeyRequest
import com.amazonaws.services.identitymanagement.model.DeleteSSHPublicKeyRequest
import com.amazonaws.services.identitymanagement.model.DeleteServiceSpecificCredentialRequest
import com.amazonaws.services.identitymanagement.model.DuplicateSSHPublicKeyException
import com.amazonaws.services.identitymanagement.model.EntityAlreadyExistsException
import com.amazonaws.services.identitymanagement.model.GetLoginProfileRequest
import com.amazonaws.services.identitymanagement.model.GetLoginProfileResult
import com.amazonaws.services.identitymanagement.model.GetSSHPublicKeyRequest
import com.amazonaws.services.identitymanagement.model.GetSSHPublicKeyResult
import com.amazonaws.services.identitymanagement.model.ListAccessKeysRequest
import com.amazonaws.services.identitymanagement.model.ListAccessKeysResult
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysRequest
import com.amazonaws.services.identitymanagement.model.ListSSHPublicKeysResult
import com.amazonaws.services.identitymanagement.model.ListServiceSpecificCredentialsRequest
import com.amazonaws.services.identitymanagement.model.ListServiceSpecificCredentialsResult
import com.amazonaws.services.identitymanagement.model.LoginProfile
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException
import com.amazonaws.services.identitymanagement.model.ServiceSpecificCredentialMetadata
import com.amazonaws.services.identitymanagement.model.StatusType
import com.amazonaws.services.identitymanagement.model.UploadSSHPublicKeyRequest
import com.amazonaws.services.identitymanagement.model.UploadSSHPublicKeyResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.IamUnusedCredentialDetector
import edu.emory.it.services.srd.remediator.IamUnusedCredentialRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class IamUnusedCredentialTest extends Specification {

    static String EXISTING_TEST_USER = "srduser"

    static String TEST_PUB_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCtGnc7hF0XsCsTPh8jZEQJmJsQp2toZOKqZEiEh6uhKTYoBvHi3d76a0PotKeXleHKdYSN5nmvUADLKa8maSN12P3daS5jOTIj2h/3h03GSmyD8W1mue2YiNtWdWNrQnhXBAKMCsLSw+7kHkvvrKBk2LyTMM2ds5JQNJjDWKdVZv9rKF2uunMEs+Cz4CYeOsX3D3w8qlte52nSVJqJ6fdZppuky1isLvU+pPMzazcdnNP12CzdY39p2bFNgOeKJYtidgJOQbQCuDsYnuKcik5GGZwF4Hw+RD4NwuVJ655BuO1qO42h20fIIbcLD9FSo6RVUnn3nDQ492eFJ7A0bS0t"

    def "integration test"() throws EnterpriseFieldException {
        setup:
        String userName = EXISTING_TEST_USER
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonIdentityManagement iam = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonIdentityManagementClient.class)

        IamUnusedCredentialDetector detector = setupDetector(credentialsProvider)
        IamUnusedCredentialRemediator remediator = setupRemediator(credentialsProvider)

        createLogin(iam, userName)
        String sshPublicKeyId = createSshPublicKey(iam, userName)
        String httpCredentialId = createHttpCredentialForGit(iam, userName)
        String accessKeyId = createTempAccessKey(iam, userName)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "detecting an account with a unencrypted bucket"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRiskLogin = detected.find({
            DetectionType.IamUnusedCredentialAwsConsole.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:user/${userName}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detectedRiskAccessKey = detected.find({
            DetectionType.IamUnusedCredentialApiKey.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:user/${userName}/access-key/${accessKeyId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRiskLogin != null
        detectedRiskAccessKey != null

        when: "remediate an expired login"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRiskLogin)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        then: "remediate should disable the login"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isLoginEnabled(iam, userName) == false
        isHttpCredentialEnabled(iam, userName, httpCredentialId) == false
        isSshPublicKeyEnabled(iam, userName, sshPublicKeyId) == false

        when: "remediate an expired accessKey"
        DetectedSecurityRiskSubclass detectedRiskNew1 =  new DetectedSecurityRiskSubclass(detectedRiskAccessKey)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew1, srContext)
        then: "remediate should delete the accessKey"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isAccessKeyExist(iam, userName, accessKeyId) == false

        when: "detecting an account that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRiskLogin2 = detected2.find({
            DetectionType.IamUnusedCredentialAwsConsole.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:user/${userName}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detectedRiskAccessKey2 = detected2.find({
            DetectionType.IamUnusedCredentialApiKey.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:user/${userName}/access-key/${accessKeyId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRiskLogin2 == null
        detectedRiskAccessKey2 == null

        cleanup:
        deleteHttpCredential(iam, userName, httpCredentialId)
        deleteSshKey(iam, userName, sshPublicKeyId)
        deleteTempAccessKey(iam, userName, accessKeyId)

    }


    private IamUnusedCredentialDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig("0") //set to zero so that newly created user is expired

        IamUnusedCredentialDetector underTest = new IamUnusedCredentialDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private IamUnusedCredentialRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig("0") //set to zero so that newly created user is expired

        IamUnusedCredentialRemediator underTest = new IamUnusedCredentialRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig(String expiryInDays) {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("IamUnusedCredentialExpiryInDays", expiryInDays)
        }}
        String iamCredentialString = "IamUnusedCredential".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (iamCredentialString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private void createLogin(AmazonIdentityManagement iam, String userName) {
        try {
            CreateLoginProfileRequest request = new CreateLoginProfileRequest().withUserName(userName).withPassword("CredentialRedactedAndRotated")
            CreateLoginProfileResult result = iam.createLoginProfile(request)
        } catch (EntityAlreadyExistsException e) {
            //ignore
        }
    }

    private String createSshPublicKey(AmazonIdentityManagement iam, String userName) {

        try {
            UploadSSHPublicKeyRequest request = new UploadSSHPublicKeyRequest().withUserName(userName).withSSHPublicKeyBody(TEST_PUB_KEY)
            UploadSSHPublicKeyResult result = iam.uploadSSHPublicKey(request)
            return result.getSSHPublicKey().getSSHPublicKeyId()
        } catch (DuplicateSSHPublicKeyException e) {
            //just grab the first one there.  Test account should not have any ssh public keys
            ListSSHPublicKeysRequest listSSHPublicKeysRequest = new ListSSHPublicKeysRequest().withUserName(userName).withMaxItems(1)
            ListSSHPublicKeysResult result = iam.listSSHPublicKeys(listSSHPublicKeysRequest)
            return result.getSSHPublicKeys().get(0).getSSHPublicKeyId()
        }
    }

    private String createHttpCredentialForGit(AmazonIdentityManagement iam, String userName) {
        CreateServiceSpecificCredentialRequest request = new CreateServiceSpecificCredentialRequest().withUserName(userName).withServiceName("codecommit.amazonaws.com")
        CreateServiceSpecificCredentialResult result = iam.createServiceSpecificCredential(request)
        return result.getServiceSpecificCredential().getServiceSpecificCredentialId()
    }

    private String createTempAccessKey(AmazonIdentityManagement iam, String userName) {
        CreateAccessKeyRequest request = new CreateAccessKeyRequest().withUserName(userName)
        CreateAccessKeyResult result = iam.createAccessKey(request)
        return result.getAccessKey().getAccessKeyId()
    }

    private boolean isLoginEnabled(AmazonIdentityManagement iam, String userName) {
        GetLoginProfileRequest getLoginProfileRequest = new GetLoginProfileRequest().withUserName(userName)
        LoginProfile loginProfile = null
        try {
            GetLoginProfileResult getLoginProfileResult = iam.getLoginProfile(getLoginProfileRequest)
            loginProfile = getLoginProfileResult.getLoginProfile()
        } catch (NoSuchEntityException e) {
            return false
        }
        return true
    }

    private boolean isSshPublicKeyEnabled(AmazonIdentityManagement iam, String userName, String keyId) {
        GetSSHPublicKeyRequest getSSHPublicKeyRequest = new GetSSHPublicKeyRequest().withUserName(userName).withSSHPublicKeyId(keyId).withEncoding("SSH")
        GetSSHPublicKeyResult result = iam.getSSHPublicKey(getSSHPublicKeyRequest)
        if (result?.getSSHPublicKey()?.getStatus() == StatusType.Active.name() ) {
            return true
        }

        return false
    }

    private boolean isHttpCredentialEnabled(AmazonIdentityManagement iam, String userName, String credentialId) {

        ListServiceSpecificCredentialsRequest listServiceSpecificCredentialsRequest = new ListServiceSpecificCredentialsRequest().withUserName(userName)
        ListServiceSpecificCredentialsResult listServiceSpecificCredentialsResult = iam.listServiceSpecificCredentials(listServiceSpecificCredentialsRequest)

        for (ServiceSpecificCredentialMetadata metadata : listServiceSpecificCredentialsResult.getServiceSpecificCredentials()) {
            if (metadata.getServiceSpecificCredentialId().equals(credentialId)) {
               return metadata.getStatus().equals(StatusType.Active.name())
            }
        }

        return false
    }

    private boolean isAccessKeyExist(AmazonIdentityManagement iam, String userName, String accessKeyId) {
        boolean done = false
        ListAccessKeysRequest accessKeysRequest = new ListAccessKeysRequest()
                .withUserName(userName)

        while (!done) {

            ListAccessKeysResult accessKeys = iam.listAccessKeys(accessKeysRequest)

            for (AccessKeyMetadata metadata : accessKeys.getAccessKeyMetadata()) {

                if (metadata.getAccessKeyId().equals(accessKeyId)) {
                    return true
                }
            }

            accessKeysRequest.setMarker(accessKeys.getMarker())

            if (!accessKeys.getIsTruncated()) {
                done = true
            }
        }

        return false
    }

    private void deleteTempAccessKey(AmazonIdentityManagement iam, String userName, String accessKeyId) {
        if (userName != null && accessKeyId != null && isAccessKeyExist(iam, userName, accessKeyId)) {
            DeleteAccessKeyRequest deleteAccessKeyRequest = new DeleteAccessKeyRequest().withUserName(userName).withAccessKeyId(accessKeyId)
            iam.deleteAccessKey(deleteAccessKeyRequest)
        }
    }

    private void deleteHttpCredential(AmazonIdentityManagement iam, String userName, String credentialId) {
        if (userName != null && credentialId != null) {
            DeleteServiceSpecificCredentialRequest request = new DeleteServiceSpecificCredentialRequest().withUserName(userName).withServiceSpecificCredentialId(credentialId)
            iam.deleteServiceSpecificCredential(request)
        }
    }

    private void deleteSshKey(AmazonIdentityManagement iam, String userName, String keyId) {
        if (userName != null && keyId != null) {
            DeleteSSHPublicKeyRequest request = new DeleteSSHPublicKeyRequest().withUserName(userName).withSSHPublicKeyId(keyId)
            iam.deleteSSHPublicKey(request)
        }
    }
}
