package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.AttachUserPolicyRequest
import com.amazonaws.services.identitymanagement.model.CreateUserRequest
import com.amazonaws.services.identitymanagement.model.ListPoliciesRequest
import com.amazonaws.services.identitymanagement.model.ListPoliciesResult
import com.amazonaws.services.identitymanagement.model.Policy
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.IamRestrictionPoliciesDetachedDetector
import edu.emory.it.services.srd.remediator.IamRestrictionPoliciesDetachedRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - account the tests are run under is not authorized to perform iam:CreateUser
class IamRestrictionPoliciesDetachedTest extends Specification {

    private static final String USER_NAME = "integration-test-user"

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = setupAppConfig()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        IamRestrictionPoliciesDetachedDetector detector = new IamRestrictionPoliciesDetachedDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        IamRestrictionPoliciesDetachedRemediator remediator = new IamRestrictionPoliciesDetachedRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        detected != null
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.IamRestrictionPoliciesDetached.name() == it.getType() &&
                    it.getAmazonResourceName().contains("iam::${ITAccount.getAccountId()}:user/")
        })
        detectedRisk != null

        when:
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)

        then:
        // usually the risk is remediated but sometimes the SRD is concurrently running in the same
        // environment and it deletes the user before we can in the test.
        // the situation is handled as part of the remediator (postponed) so we just look for either condition
        // here in the test.
        (detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus() &&
                detectedRisk.getRemediationResult().getDescription() == "Could not attach required policy so deleted user ${USER_NAME}.") ||
        (detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_POSTPONED.getStatus() &&
                detectedRisk.getRemediationResult().getDescription() == "User ${USER_NAME} may have been deleted.")
    }

    def setupSpec() {
        AmazonIdentityManagement iam = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRole(ITAccount.getRole())
                .build(AmazonIdentityManagementClient.class)

        CreateUserRequest createUserRequest = new CreateUserRequest()
                .withUserName(USER_NAME)
        iam.createUser(createUserRequest)

        /*
         * add just enough policies so that one missing policy can be attached during remediation
         * but subsequent policies give a LimitExceededException causing the user to be deleted
         */
        ListPoliciesRequest listPoliciesRequest = new ListPoliciesRequest()
        ListPoliciesResult listPoliciesResult = iam.listPolicies(listPoliciesRequest)
        AttachUserPolicyRequest attachUserPolicyRequest = new AttachUserPolicyRequest()
                .withUserName(createUserRequest.getUserName())
        int i = 0
        for (Policy policy : listPoliciesResult.policies) {
            if (policy.getPolicyName().contains("ReadOnly")) {
                iam.attachUserPolicy(attachUserPolicyRequest.withPolicyArn(policy.getArn()))
                if (++i == 9)
                    break
            }
        }
    }

    AppConfig setupAppConfig() {

        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("ignoreArns", "")
        }}
        String propertiesString = "IamRestrictionPoliciesDetached".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }
}
