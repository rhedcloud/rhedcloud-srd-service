package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.detector.AccountInWrongOrganizationalUnitBaseDetector
import edu.emory.it.services.srd.detector.AccountInWrongOrganizationalUnitHipaaDetector
import edu.emory.it.services.srd.detector.AccountInWrongOrganizationalUnitStandardDetector
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class AccountInWrongOrganizationalUnitTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AccountInWrongOrganizationalUnitStandardDetector standardDetector = setupStandardDetector(credentialsProvider)
        AccountInWrongOrganizationalUnitHipaaDetector hipaaDetector = setupHipaaDetector(credentialsProvider)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "standard account in standard detector"
        List<DetectedSecurityRisk> detected = standardDetector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.AccountInWrongOrganizationalUnit.name() == it.getType() &&
                    ("arn:aws:organizations::${ITAccount.getMasterAccount()}:account/*/${ITAccount.getAccountId()}") == it.getAmazonResourceName()
        })

        then: "should result in no detection "
        detectedRisk == null

        when: "standard account in hipaa detector"
        detected = hipaaDetector.detect(ITAccount.getAccountId(), srContext)
        detectedRisk = detected.find({
            DetectionType.AccountInWrongOrganizationalUnit.name() == it.getType() &&
                    ("arn:aws:organizations::${ITAccount.getMasterAccount()}:account/*/${ITAccount.getAccountId()}") == it.getAmazonResourceName()
        })

        then: "should result in detection"
        detectedRisk != null

        when: "hipaa account in hipaa detector"
        detected = hipaaDetector.detect(ITAccount.getHipaaAccount(), srContext)
        detectedRisk = detected.find({
            DetectionType.AccountInWrongOrganizationalUnit.name() == it.getType() &&
                    ("arn:aws:organizations::${ITAccount.getMasterAccount()}:account/*/${ITAccount.getHipaaAccount()}") == it.getAmazonResourceName()
        })

        then: "should result in no detection"
        detectedRisk == null

        when: "hipaa account in standard detector"
        detected = standardDetector.detect(ITAccount.getHipaaAccount(), srContext)
        detectedRisk = detected.find({
            DetectionType.AccountInWrongOrganizationalUnit.name() == it.getType() &&
                    ("arn:aws:organizations::${ITAccount.getMasterAccount()}:account/*/${ITAccount.getHipaaAccount()}") == it.getAmazonResourceName()
        })

        then: "should result in detection"
        detectedRisk != null

    }

    private AccountInWrongOrganizationalUnitStandardDetector setupStandardDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        AccountInWrongOrganizationalUnitBaseDetector underTest = new AccountInWrongOrganizationalUnitStandardDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AccountInWrongOrganizationalUnitHipaaDetector setupHipaaDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        AccountInWrongOrganizationalUnitHipaaDetector underTest = new AccountInWrongOrganizationalUnitHipaaDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }


    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("HipaaOrgName", "EmoryHipaaResearchAccounts")
            setProperty("StandardOrgName", "EmoryResearchAccounts")
            setProperty("OrgsToIgnore", "EmoryAccountQuarantineOrg,EmoryAccountPendingDeleteOrg")
        }}

        String propertiesString = "AccountInWrongOrganizationalUnit".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }
}

