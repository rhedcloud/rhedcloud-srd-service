package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.cloudfront.AmazonCloudFront
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient
import com.amazonaws.services.cloudfront.model.CookiePreference
import com.amazonaws.services.cloudfront.model.CreateDistributionRequest
import com.amazonaws.services.cloudfront.model.CreateDistributionResult
import com.amazonaws.services.cloudfront.model.CustomHeaders
import com.amazonaws.services.cloudfront.model.CustomOriginConfig
import com.amazonaws.services.cloudfront.model.DefaultCacheBehavior
import com.amazonaws.services.cloudfront.model.DistributionAlreadyExistsException
import com.amazonaws.services.cloudfront.model.DistributionConfig
import com.amazonaws.services.cloudfront.model.ForwardedValues
import com.amazonaws.services.cloudfront.model.GetDistributionConfigRequest
import com.amazonaws.services.cloudfront.model.GetDistributionConfigResult
import com.amazonaws.services.cloudfront.model.Headers
import com.amazonaws.services.cloudfront.model.Origin
import com.amazonaws.services.cloudfront.model.OriginSslProtocols
import com.amazonaws.services.cloudfront.model.Origins
import com.amazonaws.services.cloudfront.model.TrustedSigners
import com.amazonaws.services.cloudfront.model.UpdateDistributionRequest
import com.amazonaws.services.cloudfront.model.UpdateDistributionResult
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerRequest
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerResult
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest
import com.amazonaws.services.elasticloadbalancing.model.Listener
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.CloudFrontNonHTTPSOriginEnabledDetector
import edu.emory.it.services.srd.remediator.CloudFrontNonHTTPSOriginEnabledRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when permission has been given to the detector to list distributions.
class CloudFrontNonHTTPSOriginEnabledTest extends Specification {

    static String AVAILABILITY_ZONE = "${ITAccount.getRegion()}a"

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonCloudFront cloudFront = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonCloudFrontClient.class)

        AmazonElasticLoadBalancingClient elb = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonElasticLoadBalancingClient.class)

        CloudFrontNonHTTPSOriginEnabledDetector detector = setupDetector(credentialsProvider)
        CloudFrontNonHTTPSOriginEnabledRemediator remediator = setupRemediator(credentialsProvider)

        String originDomainName = createElasticLoadBalancer(elb)
        String distributionId = createCloudFrontDistribution(cloudFront, originDomainName)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "detecting a CloudFront with non https origin"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.CloudFrontNonHTTPSOriginEnabled.name() == it.getType() &&
                    ("arn:aws:cloudfront::${ITAccount.getAccountId()}:distribution/${distributionId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a CloudFront with non https origin"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)

        then: "remediate should set origin to https only"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isOriginHttpsOnly(cloudFront, distributionId) == true


        when: "detecting a CloudFront that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk riskdetected2 = detected2.find({
            DetectionType.CloudFrontNonHTTPSOriginEnabled.name() == it.getType() &&
                    ("arn:aws:cloudfront::${ITAccount.getAccountId()}:distribution/${distributionId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        riskdetected2 == null

        cleanup:
        disableCloudFront(cloudFront, distributionId)
        deleteLoadBalancer(elb)
    }


    private CloudFrontNonHTTPSOriginEnabledDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudFrontNonHTTPSOriginEnabledDetector underTest = new CloudFrontNonHTTPSOriginEnabledDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private CloudFrontNonHTTPSOriginEnabledRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudFrontNonHTTPSOriginEnabledRemediator underTest = new CloudFrontNonHTTPSOriginEnabledRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createElasticLoadBalancer(AmazonElasticLoadBalancingClient elb) {
        CreateLoadBalancerRequest createLoadBalancerRequest = new CreateLoadBalancerRequest()
                .withLoadBalancerName("testBalancer")
                .withListeners(new Listener(){{
                    setProtocol("HTTP")
            setLoadBalancerPort(80)
            setInstancePort(80)
            setInstanceProtocol("HTTP")
        }})
                .withAvailabilityZones(AVAILABILITY_ZONE)

        CreateLoadBalancerResult result =  elb.createLoadBalancer(createLoadBalancerRequest)
        return result.getDNSName()
    }

    private void deleteLoadBalancer(AmazonElasticLoadBalancingClient elb) {
        DeleteLoadBalancerRequest request = new DeleteLoadBalancerRequest().withLoadBalancerName("testBalancer")
        elb.deleteLoadBalancer(request)
    }

    private String createCloudFrontDistribution(AmazonCloudFront cloudFront, String originDomainName) {

        DistributionConfig config = new DistributionConfig() {{
            setOrigins(new Origins() {{
                setQuantity(1)
                setItems(new ArrayList<Origin>() {{
                    add(new Origin() {{
                        setId("test-origin")
                        setDomainName(originDomainName)
                        setCustomHeaders(new CustomHeaders() {{
                            setQuantity(0)
                        }})
                        setOriginPath("")
                        setCustomOriginConfig(new CustomOriginConfig(){{
                            setHTTPPort(80)
                            setHTTPSPort(443)
                            setOriginProtocolPolicy("http-only")
                            setOriginSslProtocols(new OriginSslProtocols() {{
                                setQuantity(3)
                                setItems(new ArrayList<String>() {{
                                    add("TLSv1")
                                    add("TLSv1.1")
                                    add("TLSv1.2")
                                }})
                            }})
                            setOriginReadTimeout(30)
                            setOriginKeepaliveTimeout(5)
                        }})
                    }})
                }})
            }})
            setDefaultCacheBehavior(new DefaultCacheBehavior(){{
                setTrustedSigners(new TrustedSigners(){{
                    setEnabled(true)
                    setQuantity(0)
                }})
                setMinTTL(3600L)
                setDefaultTTL(86400L)
                setMaxTTL(31536000L)
                setViewerProtocolPolicy("allow-all")
                setForwardedValues(new ForwardedValues(){{
                    setHeaders(new Headers() {{
                        setQuantity(0)
                    }})
                    setCookies(new CookiePreference(){{
                        setForward("none")
                    }})
                    setQueryString(true)
                }})
                setTargetOriginId("test-origin")
            }})
            setCallerReference("CloudFrontNonHTTPSOriginEnabledTest")
            setComment("")
            setEnabled(true)
        }}

        try {
            CreateDistributionRequest request = new CreateDistributionRequest().withDistributionConfig(config)
            CreateDistributionResult result = cloudFront.createDistribution(request)
            return result.getDistribution().getId()

        } catch (DistributionAlreadyExistsException e) {
            String errorMessage = e.getErrorMessage()
            String distributionId =  errorMessage.substring(errorMessage.lastIndexOf(" ") + 1)

            GetDistributionConfigRequest getDistributionConfigRequest = new GetDistributionConfigRequest().withId(distributionId)
            GetDistributionConfigResult getDistributionConfigResult = cloudFront.getDistributionConfig(getDistributionConfigRequest)
            DistributionConfig distributionConfig = getDistributionConfigResult.getDistributionConfig()
            distributionConfig.setEnabled(true)
            distributionConfig.getDefaultCacheBehavior().setTargetOriginId("test-origin")
            distributionConfig.setOrigins(config.getOrigins())

            UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest().withId(distributionId).withDistributionConfig(distributionConfig).withIfMatch(getDistributionConfigResult.getETag())
            UpdateDistributionResult updateDistributionResult = cloudFront.updateDistribution(updateDistributionRequest)

            return distributionId
        }
    }

    private void disableCloudFront(AmazonCloudFront cloudFront, String distributionId) {
        if (distributionId) {
            GetDistributionConfigResult getDistributionConfigResult = cloudFront.getDistributionConfig(new GetDistributionConfigRequest().withId(distributionId))
            DistributionConfig config = getDistributionConfigResult.getDistributionConfig()
            config.setEnabled(false)
            UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest().withId(distributionId).withDistributionConfig(config).withIfMatch(getDistributionConfigResult.getETag())
            UpdateDistributionResult updateDistributionResult = cloudFront.updateDistribution(updateDistributionRequest)
        }
    }

    private boolean isOriginHttpsOnly(AmazonCloudFront cloudFront, String distributionId) {
        GetDistributionConfigRequest request = new GetDistributionConfigRequest().withId(distributionId)
        GetDistributionConfigResult result = cloudFront.getDistributionConfig(request)

        return result?.getDistributionConfig()?.getOrigins()?.getItems()?.get(0)?.getCustomOriginConfig().getOriginProtocolPolicy() == 'https-only'
    }
}
