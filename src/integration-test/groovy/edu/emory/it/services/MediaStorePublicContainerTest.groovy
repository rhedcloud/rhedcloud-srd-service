package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.mediastore.AWSMediaStore
import com.amazonaws.services.mediastore.AWSMediaStoreClient
import com.amazonaws.services.mediastore.model.Container
import com.amazonaws.services.mediastore.model.CreateContainerRequest
import com.amazonaws.services.mediastore.model.ListContainersRequest
import com.amazonaws.services.mediastore.model.ListContainersResult
import com.amazonaws.services.mediastore.model.PutContainerPolicyRequest
import com.amazonaws.services.mediastoredata.AWSMediaStoreData
import com.amazonaws.services.mediastoredata.AWSMediaStoreDataClient
import com.amazonaws.services.mediastoredata.model.PutObjectRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.MediaStorePublicContainerHipaaDetector
import edu.emory.it.services.srd.remediator.MediaStorePublicContainerHipaaRemediator
import edu.emory.it.services.srd.remediator.MediaStorePublicContainerStandardRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

import java.nio.file.Files

@org.junit.experimental.categories.Category(ITCategorySlow1.class)
class MediaStorePublicContainerTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        // the Standard and HIPAA detectors are identical so doesn't matter which is used here
        MediaStorePublicContainerHipaaDetector detector = new MediaStorePublicContainerHipaaDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        MediaStorePublicContainerStandardRemediator standardRemediator = new MediaStorePublicContainerStandardRemediator()
        standardRemediator.init(appConfig, credentialsProvider, ITAccount.getRole())
        MediaStorePublicContainerHipaaRemediator hipaaRemediator = new MediaStorePublicContainerHipaaRemediator()
        hipaaRemediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.MediaStorePublicContainer.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:mediastore:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:container/${RdsDatabaseHelper.mediaStoreContainer}"

        when: "Remediation for Standard compliance class accounts is passive"
        standardRemediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)
        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Passive Remediation"

        when: "Remediation for HIPAA compliance class accounts is active"
        hipaaRemediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)
        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Elemental MediaStore container ${detected.get(0).getAmazonResourceName()} deleted"
    }

    def setupSpec() {
        AWSMediaStore msClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AWSMediaStoreClient.class)

        CreateContainerRequest createContainerRequest = new CreateContainerRequest()
                .withContainerName(RdsDatabaseHelper.mediaStoreContainer)

        msClient.createContainer(createContainerRequest)

        // wait for the container to become active
        Container itContainer = null

        for (i in 1..20) {
            ListContainersRequest listContainersRequest = new ListContainersRequest()
                    .withMaxResults(100)  // get as many as is allowed
            ListContainersResult listContainersResult = msClient.listContainers(listContainersRequest)

            String containerStatus = null

            for (Container container : listContainersResult.getContainers()) {
                if (container.getName() == RdsDatabaseHelper.mediaStoreContainer) {
                    containerStatus = container.getStatus()
                    if (container.getStatus() == "ACTIVE") {
                        itContainer = container
                    }
                    break
                }
            }
            if (containerStatus == null)
                throw new RuntimeException("Elemental MediaStore container ${RdsDatabaseHelper.mediaStoreContainer} is missing")
            if (itContainer != null)
                break

            println("${RdsDatabaseHelper.mediaStoreContainer} - waiting for ACTIVE status but got ${containerStatus}")
            sleep(1000 * 60)
        }

        if (itContainer == null)
            throw new RuntimeException("Elemental MediaStore container is not available for tests")

        // allow public access to the objects in the container
        PutContainerPolicyRequest putContainerPolicyRequest = new PutContainerPolicyRequest()
                .withContainerName(RdsDatabaseHelper.mediaStoreContainer)
                .withPolicy("""{
    "Version": "2012-10-17",
    "Statement": [{
        "Sid": "IntegrationTestPublicAccess",
        "Action": [ "mediastore:*" ],
        "Principal": {"AWS" : "*"},
        "Effect": "Allow",
        "Resource": "arn:aws:mediastore:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:container/${RdsDatabaseHelper.mediaStoreContainer}/*",
        "Condition": { "Bool": { "aws:SecureTransport": "true" } }
    }]
}""")
        msClient.putContainerPolicy(putContainerPolicyRequest)

        // policy changes need time to take effect
        println("${RdsDatabaseHelper.mediaStoreContainer} - waiting for policy change to take effect")
        sleep(1000 * 60 * 15)

        println("${RdsDatabaseHelper.mediaStoreContainer} - upload files to container")

        // now upload some files into the container
        AWSMediaStoreData msdClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withEndpoint(itContainer.getEndpoint())
                .build(AWSMediaStoreDataClient.class)

        File file = new File(getClass().getClassLoader().getResource("ebs-application/application.go").getFile())
        byte[] fileBytes = Files.readAllBytes(file.toPath())

        PutObjectRequest por = new PutObjectRequest()
                .withContentType("text/plain")

        msdClient.putObject(por.withPath("application.go").withBody(new ByteArrayInputStream(fileBytes)))
        msdClient.putObject(por.withPath("dir1/application1.go").withBody(new ByteArrayInputStream(fileBytes)))
        msdClient.putObject(por.withPath("dir2/application2.go").withBody(new ByteArrayInputStream(fileBytes)))
        msdClient.putObject(por.withPath("dir2/dir2a/application2a.go").withBody(new ByteArrayInputStream(fileBytes)))
    }
}
