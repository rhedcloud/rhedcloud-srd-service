package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.DBInstanceAlreadyExistsException
import com.amazonaws.services.rds.model.DBInstanceNotFoundException
import com.amazonaws.services.rds.model.DBParameterGroupAlreadyExistsException
import com.amazonaws.services.rds.model.InvalidDBInstanceStateException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RdsPostgreSQLUnencryptedTransportDetector
import edu.emory.it.services.srd.remediator.RdsPostgreSQLUnencryptedTransportRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryRds3.class)
class RdsPostgreSQLUnencryptedTransportTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RdsPostgreSQLUnencryptedTransportDetector detector = new RdsPostgreSQLUnencryptedTransportDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RdsPostgreSQLUnencryptedTransportRemediator remediator = new RdsPostgreSQLUnencryptedTransportRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        def expectedArn = "arn:aws:rds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:db:${RdsDatabaseHelper.postgresDBInstanceIdentifier}"

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            it.getAmazonResourceName() == expectedArn
        })

        then:
        detectedRisk != null
        detectedRisk.getType() == DetectionType.RdsPostgreSQLUnencryptedTransport.name()
        // ARN match done above in the find()
        // ensure properties are set that are used to pass information to the remediator
        detectedRisk.getProperties().size() == 2
        detectedRisk.getProperties().containsKey("DBInstanceIdentifier")
        detectedRisk.getProperties().containsKey("DBInstanceStatus")

        when:
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)

        then:
        detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detectedRisk.getRemediationResult().getDescription().contains("stopped with snapshot")
    }

    def setupSpec() {
        AmazonRDS client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonRDSClient.class)

        RdsDatabaseHelper.deleteOldRdsSnapshots(client)

        // create the parameter group
        try {
            RdsDatabaseHelper.createPostgresDBParameterGroup(client)
        } catch (DBParameterGroupAlreadyExistsException e) {
            // ignore
        }

        // make sure the parameter group has the correct SSL setting
        RdsDatabaseHelper.modifyPostgresDBParameterGroup(client)

        // create the DB instance
        try {
            RdsDatabaseHelper.createPostgresWithUnencryptedStorage(client)
        } catch (DBInstanceAlreadyExistsException e) {
            // ignore
        }

        // if the instance is still stopping from a previous run then wait for it to finish
        // and become stopped.
        try {
            RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.postgresDBInstanceIdentifier, ['available', 'stopped'], client)
        } catch (DBInstanceNotFoundException e) {
            // ignore because we're about to create it
        }

        // start up the instance
        try {
            RdsDatabaseHelper.startDBInstance(RdsDatabaseHelper.postgresDBInstanceIdentifier, client)
        } catch (InvalidDBInstanceStateException e) {
            // ignore - may already be running
        }

        // now wait for it to finish starting and become available
        RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.postgresDBInstanceIdentifier, ['available'], client)
    }
}