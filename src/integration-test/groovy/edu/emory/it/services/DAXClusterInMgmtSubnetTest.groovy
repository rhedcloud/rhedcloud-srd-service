package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.dax.AmazonDax
import com.amazonaws.services.dax.AmazonDaxClient
import com.amazonaws.services.dax.model.Cluster
import com.amazonaws.services.dax.model.ClusterAlreadyExistsException
import com.amazonaws.services.dax.model.CreateClusterRequest
import com.amazonaws.services.dax.model.CreateSubnetGroupRequest
import com.amazonaws.services.dax.model.DescribeClustersRequest
import com.amazonaws.services.dax.model.DescribeClustersResult
import com.amazonaws.services.dax.model.SubnetGroupAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.DAXClusterInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.DAXClusterInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - ignored because we don't have permission to describe subnet groups in the detector
class DAXClusterInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonDax dax = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonDaxClient.class)

        DAXClusterInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        DAXClusterInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        String subnetGroup = createSubnetGroup(dax)
        String clusterName = createCluster(dax, subnetGroup)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with spot requests in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.DAXClusterInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:dax:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:cache/${clusterName}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null

        when: "remediate an account with spot requests in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        sleep(60000)

        then: "remediate should terminate the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()


        when: "detecting an account with stopped instances that have unencrypted instances"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected3 = detecteds2.find({
            DetectionType.DAXClusterInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:dax:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:cache/${clusterName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected3 == null

    }


    private DAXClusterInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        DAXClusterInMgmtSubnetDetector underTest = new DAXClusterInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private DAXClusterInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        DAXClusterInMgmtSubnetRemediator underTest = new DAXClusterInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }


    private String createSubnetGroup(AmazonDax dax) {
        String subnetGroupName = "srd-test-dax-in-disallowed-subnet"

        CreateSubnetGroupRequest createSubnetGroupRequest = new CreateSubnetGroupRequest().withSubnetGroupName(subnetGroupName).withSubnetIds(ITAccount.getExistingDisallowedSubnet())
        try {
            dax.createSubnetGroup(createSubnetGroupRequest);
        } catch (SubnetGroupAlreadyExistsException e) {
            //ignore
        }

        return subnetGroupName
    }

    private String createCluster(AmazonDax dax, String subnetGroupName) {
        String clusterName = "srd-test-dax"

        CreateClusterRequest createClusterRequest = new CreateClusterRequest()
                .withSubnetGroupName(subnetGroupName)
                .withClusterName(clusterName)
                .withNodeType("dax.r3.large")
                .withReplicationFactor(1)
                .withIamRoleArn(ITAccount.getDAXServiceRole())

        try {
            dax.createCluster(createClusterRequest)
        } catch (ClusterAlreadyExistsException e) {
            Cluster cluster = getCluster(dax, clusterName)
            if (cluster.getStatus() == "deleting") {
                waitUntilStatusIsNot("deleting", dax, clusterName)
                dax.createCluster(createClusterRequest)
            }
        }

        waitUntilStatusIsNot("creating", dax, clusterName);

        return clusterName;
    }

    private Cluster getCluster(AmazonDax dax, String clusterName) {
        DescribeClustersRequest request = new DescribeClustersRequest().withClusterNames(clusterName);
        DescribeClustersResult result = dax.describeClusters(request)
        Cluster cluster = result.getClusters().get(0)

        return cluster
    }

    private void waitUntilStatusIsNot(String originalStatus, AmazonDax dax, String clusterName) {
        String status = originalStatus;
        for (int i=0; i < 20 && status == originalStatus; i++) {
            sleep(20000);
            Cluster cluster = getCluster(dax, clusterName)
            status = cluster.getStatus()
        }
    }
}
