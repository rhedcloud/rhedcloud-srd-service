package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.DBParameterGroupAlreadyExistsException
import com.amazonaws.services.rds.model.DBSubnetGroupAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RdsLaunchedWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.RdsLaunchedWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryRds5.class)
class RdsLaunchedWithinManagementSubnetsTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RdsLaunchedWithinManagementSubnetsDetector detector = new RdsLaunchedWithinManagementSubnetsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RdsLaunchedWithinManagementSubnetsRemediator remediator = new RdsLaunchedWithinManagementSubnetsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.RdsLaunchedWithinManagementSubnets.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:rds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:db:${RdsDatabaseHelper.rdsInManagementSubnetDBInstanceIdentifier}"
        // ensure properties are set that are used to pass information to the remediator
        detected.get(0).getProperties().size() == 2
        detected.get(0).getProperties().containsKey("DBInstanceIdentifier")
        detected.get(0).getProperties().containsKey("DBInstanceStatus")

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription().contains("deleted with snapshot")
    }

    def setupSpec() {
        AmazonEC2 ec2Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonEC2Client.class)
        AmazonRDS rdsClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonRDSClient.class)

        RdsDatabaseHelper.deleteOldRdsSnapshots(rdsClient)

        // create the parameter group
        try {
            RdsDatabaseHelper.createPostgresDBParameterGroup(rdsClient)
        } catch (DBParameterGroupAlreadyExistsException e) {
            // ignore
        }

        // make sure the parameter group has the correct SSL setting
        RdsDatabaseHelper.modifyPostgresDBParameterGroup(rdsClient)

        try {
            RdsDatabaseHelper.createRdsDisallowedSubnetGroup(ec2Client, rdsClient)
        } catch (DBSubnetGroupAlreadyExistsException e) {
            // ignore
        }

        // create the DB instance.
        // this may throw DBInstanceAlreadyExistsException which means
        //  that the DB instance from another test run may still be deleting.
        // to keep things simple, just let the test fail, requiring it to be manually re-run.
        RdsDatabaseHelper.createDBInstanceInDisallowedSubnet(rdsClient)

        // now wait for it to finish starting and become available
        RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.rdsInManagementSubnetDBInstanceIdentifier, ['available'], rdsClient)
    }
}
