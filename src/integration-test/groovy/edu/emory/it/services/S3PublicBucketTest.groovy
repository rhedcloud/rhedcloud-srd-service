package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AccessControlList
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.Grant
import com.amazonaws.services.s3.model.Grantee
import com.amazonaws.services.s3.model.GroupGrantee
import com.amazonaws.services.s3.model.Permission
import com.amazonaws.services.s3.model.PutObjectResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.S3PublicBucketDetector
import edu.emory.it.services.srd.remediator.S3PublicBucketRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class S3PublicBucketTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonS3Client.class)

        S3PublicBucketDetector detector = setupDetector(credentialsProvider)
        S3PublicBucketRemediator remediator = setupRemediator(credentialsProvider)

        String bucketName = createPublicBucketOnAws(s3)
        String bucketKey = createPublicObjectOnAws(s3, bucketName)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "detecting an account with a public bucket and a public object"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk aclRiskDetected = detected.find({
            DetectionType.S3PublicBucketAcl.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk objectRiskDetected = detected.find({
            DetectionType.S3PublicBucketObject.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}/${bucketKey}") == it.getAmazonResourceName()
        })


        then: "error should be detected"
        detected.isEmpty() == false
        aclRiskDetected != null

        when: "remediate an account with a public bucket"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(aclRiskDetected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should stop the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        isBucketPublic(s3, bucketName) == false

//        when: "remediate an account with a public object"
//        DetectedSecurityRiskSubclass detectedRisk1 =  new DetectedSecurityRiskSubclass(objectRiskDetected);
//        remediator.remediate(ITAccount.getAccountId(), detectedRisk1);
//        then: "remediate should stop the instance"
//        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk1.getRemediationResultNew().getStatus();
//        null != detectedRisk1.getRemediationResultNew().getDescription();
//        isObjectPublic(s3, bucketName, bucketKey) == false;

        when: "detecting an account bucket remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk aclRiskDetected2 = detected2.find({
            DetectionType.S3PublicBucketAcl.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk objectRiskDetected2 = detected2.find({
            DetectionType.S3PublicBucketObject.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}/${bucketKey}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        aclRiskDetected2 == null
        objectRiskDetected2 == null

        cleanup:
        deleteObject(s3, bucketName, bucketKey)
        deleteBucket(s3, bucketName)

    }


    private S3PublicBucketDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        S3PublicBucketDetector underTest = new S3PublicBucketDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private S3PublicBucketRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        S3PublicBucketRemediator underTest = new S3PublicBucketRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createPublicBucketOnAws(AmazonS3 s3) {
        String bucketName = "srd-test-public-bucket-${System.currentTimeMillis()}"
        Bucket bucket = s3.createBucket(bucketName)

        AccessControlList acl = s3.getBucketAcl(bucketName)
        acl.grantPermission(GroupGrantee.AllUsers, Permission.Read)
        acl.grantPermission(GroupGrantee.AuthenticatedUsers, Permission.Write)
        s3.setBucketAcl(bucketName, acl)

        return bucketName
    }

    private String createPublicObjectOnAws(AmazonS3 s3, String bucketName) {
        String bucketKey = "tmp/srd-test-public-bucket.txt"
        PutObjectResult putObjectResult = s3.putObject(bucketName, bucketKey, "test-content")

        AccessControlList acl = s3.getObjectAcl(bucketName, bucketKey)
        acl.grantPermission(GroupGrantee.AllUsers, Permission.Read)
        acl.grantPermission(GroupGrantee.AuthenticatedUsers, Permission.Write)
        s3.setObjectAcl(bucketName, bucketKey, acl)

        return bucketKey
    }

    private void deleteObject(AmazonS3 s3, String bucketName, String bucketKey) {
        if (bucketName != null && bucketKey != null) {
            s3.deleteObject(bucketName, bucketKey)
        }
    }

    private void deleteBucket(AmazonS3 s3, String bucketName) {
        if (bucketName != null) {
           s3.deleteBucket(bucketName)
        }
    }

    private boolean isBucketPublic(AmazonS3 s3, String bucketName) {
        AccessControlList acl = s3.getBucketAcl(bucketName)
        List<Grant> grants = acl.getGrantsAsList()

        for (Grant grant : grants) {
            Grantee grantee = grant.getGrantee()
            if (grantee instanceof GroupGrantee) {
                GroupGrantee groupGrantee =  (GroupGrantee) grantee
                if (groupGrantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                    return true
                }
            }
        }

        return false
    }

    private boolean isObjectPublic(AmazonS3 s3, String bucketName, String bucketKey) {
        AccessControlList acl = s3.getObjectAcl(bucketName, bucketKey)
        List<Grant> grants = acl.getGrantsAsList()

        for (Grant grant : grants) {
            Grantee grantee = grant.getGrantee()
            if (grantee instanceof GroupGrantee) {
                GroupGrantee groupGrantee =  (GroupGrantee) grantee
                if (groupGrantee == GroupGrantee.AllUsers || grantee == GroupGrantee.AuthenticatedUsers) {
                    return true
                }
            }
        }

        return false
    }
}
