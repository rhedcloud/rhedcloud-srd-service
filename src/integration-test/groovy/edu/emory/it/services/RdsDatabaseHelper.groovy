package edu.emory.it.services

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement
import com.amazonaws.services.dynamodbv2.model.KeyType
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput
import com.amazonaws.services.dynamodbv2.model.SSESpecification
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ecs.AmazonECS
import com.amazonaws.services.ecs.model.AmazonECSException
import com.amazonaws.services.ecs.model.AwsVpcConfiguration
import com.amazonaws.services.ecs.model.CreateServiceRequest
import com.amazonaws.services.ecs.model.NetworkConfiguration
import com.amazonaws.services.elasticache.AmazonElastiCache
import com.amazonaws.services.elasticache.model.CreateCacheClusterRequest
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk
import com.amazonaws.services.elasticbeanstalk.model.AWSElasticBeanstalkException
import com.amazonaws.services.elasticbeanstalk.model.ConfigurationOptionSetting
import com.amazonaws.services.elasticbeanstalk.model.CreateApplicationRequest
import com.amazonaws.services.elasticbeanstalk.model.CreateApplicationVersionRequest
import com.amazonaws.services.elasticbeanstalk.model.CreateEnvironmentRequest
import com.amazonaws.services.elasticbeanstalk.model.CreateStorageLocationResult
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentsRequest
import com.amazonaws.services.elasticbeanstalk.model.DescribeEnvironmentsResult
import com.amazonaws.services.elasticbeanstalk.model.S3Location
import com.amazonaws.services.elasticbeanstalk.model.UpdateEnvironmentRequest
import com.amazonaws.services.elasticmapreduce.AmazonElasticMapReduce
import com.amazonaws.services.elasticmapreduce.model.ActionOnFailure
import com.amazonaws.services.elasticmapreduce.model.HadoopJarStepConfig
import com.amazonaws.services.elasticmapreduce.model.JobFlowInstancesConfig
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowRequest
import com.amazonaws.services.elasticmapreduce.model.RunJobFlowResult
import com.amazonaws.services.elasticmapreduce.model.StepConfig
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.model.ApplyMethod
import com.amazonaws.services.rds.model.CreateDBInstanceRequest
import com.amazonaws.services.rds.model.CreateDBParameterGroupRequest
import com.amazonaws.services.rds.model.CreateDBSubnetGroupRequest
import com.amazonaws.services.rds.model.CreateOptionGroupRequest
import com.amazonaws.services.rds.model.DBInstance
import com.amazonaws.services.rds.model.DeleteDBSnapshotRequest
import com.amazonaws.services.rds.model.DescribeDBInstancesRequest
import com.amazonaws.services.rds.model.DescribeDBInstancesResult
import com.amazonaws.services.rds.model.DescribeDBSnapshotsRequest
import com.amazonaws.services.rds.model.DescribeDBSnapshotsResult
import com.amazonaws.services.rds.model.ModifyDBParameterGroupRequest
import com.amazonaws.services.rds.model.ModifyOptionGroupRequest
import com.amazonaws.services.rds.model.OptionConfiguration
import com.amazonaws.services.rds.model.OptionSetting
import com.amazonaws.services.rds.model.Parameter
import com.amazonaws.services.rds.model.StartDBInstanceRequest
import com.amazonaws.services.redshift.AmazonRedshift
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.PutObjectRequest
import edu.emory.it.services.srd.util.VpcUtil

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class RdsDatabaseHelper {
    static def postgresDBInstanceIdentifier = "integration-test-postgres-unencrypted-storage"
    static def postgresDBParameterGroupName = "integration-test-postgres-unencrypted-transport"
    static def rdsInDisallowedSubnetDBInstanceIdentifier = "integration-test-rds-disallowed-subnet"
    static def rdsDisallowedSubnetGroupName = "rdsDisallowedSubnetGroup"
    static def oracleDBInstanceIdentifier = "integration-test-oracle-encrypted-storage"
    static def oracleOptionGroupName = "integration-test-oracle-unencrypted-transport"
    static def sqlServerDBInstanceIdentifier = "integration-test-sqlserver-unencrypted-storage"
    static def sqlServerDBParameterGroupName = "integration-test-sqlserver-unencrypted-transport"
    static def memcachedCacheClusterId = "it-memcached"  // max length of 20
    static def redisCacheClusterId = "it-redis"  // max length of 20
    static def dynamoDbTableName = "integration-test-dynamodb-unencrypted-table"
    static def redshiftClusterIdentifier = "integration-test-redshift-unencrypted"
    static def ebsApplicationName = "integration-test-ebs-application"
    static def ebsEnvironmentName = "integration-test-ebs-environment"
    static def ecsClusterName = "integration-test-ecs-cluster"
    static def ecsServiceName = "integration-test-ecs-service"
    static def emrClusterName = "integration-test-emr-cluster"
    static def lambdaFunctionName = "integration-test-lambda-function"
    static def lambdaRoleName = "integration-test-lambda-role"
    static def mediaStoreContainer = "integration_test_mediastore_container"

    static def startDBInstance(String dbInstanceIdentifier, AmazonRDS client) {
        StartDBInstanceRequest startDBInstanceRequest = new StartDBInstanceRequest()
                .withDBInstanceIdentifier(dbInstanceIdentifier)

        client.startDBInstance(startDBInstanceRequest)
    }

    static def waitForDBInstanceStatus(String dbInstanceIdentifier, List<String> statuses, AmazonRDS client) {
        DescribeDBInstancesRequest describeDBInstancesRequest = new DescribeDBInstancesRequest()
                .withDBInstanceIdentifier(dbInstanceIdentifier)

        for (i in 1..20) {
            DescribeDBInstancesResult describeDBInstancesResult = client.describeDBInstances(describeDBInstancesRequest)
            List<DBInstance> instances = describeDBInstancesResult.getDBInstances()
            if (statuses.contains(instances[0].getDBInstanceStatus()))
                return
            println("${dbInstanceIdentifier} - waiting for status ${statuses} got ${instances[0].getDBInstanceStatus()}")
            sleep(1000 * 60)
        }

        throw new RuntimeException("Database is not available for tests")
    }


    static def createPostgresDBParameterGroup(AmazonRDS client) {
        CreateDBParameterGroupRequest createDBParameterGroupRequest = new CreateDBParameterGroupRequest()
                .withDBParameterGroupName(postgresDBParameterGroupName)
                .withDBParameterGroupFamily("postgres9.6")
                .withDescription("Parameter group for integration tests")

        client.createDBParameterGroup(createDBParameterGroupRequest)
    }

    static def modifyPostgresDBParameterGroup(AmazonRDS client) {
        ModifyDBParameterGroupRequest modifyDBParameterGroupRequest = new ModifyDBParameterGroupRequest()
                .withDBParameterGroupName(postgresDBParameterGroupName)
                .withParameters(new Parameter()
                .withParameterName("rds.force_ssl")
                .withParameterValue("0")  // <---- so the detector finds it
                .withApplyMethod(ApplyMethod.PendingReboot))

        client.modifyDBParameterGroup(modifyDBParameterGroupRequest)
    }

    static def createPostgresWithUnencryptedStorage(AmazonRDS client) {
        CreateDBInstanceRequest dbInstanceRequest = new CreateDBInstanceRequest()
                .withDBInstanceIdentifier(postgresDBInstanceIdentifier)
                .withDBName("integrationTestPostgresUnencryptedStorage")
                .withDBInstanceClass("db.t2.micro")
                .withEngine("postgres")
                .withEngineVersion("9.6.6")
                .withStorageType("standard")  // <---- so we can keep the allocated storage small
                .withAllocatedStorage(5)
                .withMultiAZ(false)  // <---- so it can be stopped without error
                .withPubliclyAccessible(false)
                .withMasterUsername("integrationtest")
                .withMasterUserPassword("integrationtest")
                .withStorageEncrypted(false)  // <---- so the detector finds it
                .withDBParameterGroupName(postgresDBParameterGroupName)
                .withBackupRetentionPeriod(0)

        client.createDBInstance(dbInstanceRequest)
    }

    static def createSqlServerDBParameterGroup(AmazonRDS client) {
        CreateDBParameterGroupRequest createDBParameterGroupRequest = new CreateDBParameterGroupRequest()
                .withDBParameterGroupName(sqlServerDBParameterGroupName)
                .withDBParameterGroupFamily("sqlserver-ex-14.0")
                .withDescription("Parameter group for integration tests")

        client.createDBParameterGroup(createDBParameterGroupRequest)
    }

    static def modifySqlServerDBParameterGroup(AmazonRDS client) {
        ModifyDBParameterGroupRequest modifyDBParameterGroupRequest = new ModifyDBParameterGroupRequest()
                .withDBParameterGroupName(sqlServerDBParameterGroupName)
                .withParameters(new Parameter()
                .withParameterName("rds.force_ssl")
                .withParameterValue("0")  // <---- so the detector finds it
                .withApplyMethod(ApplyMethod.PendingReboot))

        client.modifyDBParameterGroup(modifyDBParameterGroupRequest)
    }

    static def createSqlServerWithEncryptedStorage(AmazonRDS client) {
        // for both RdsHipaaIneligibleSqlServerDatabaseDetector and RdsSqlServerUnencryptedTransportDetector
        CreateDBInstanceRequest dbInstanceRequest = new CreateDBInstanceRequest()
                .withDBInstanceIdentifier(sqlServerDBInstanceIdentifier)
                .withDBInstanceClass("db.t2.micro")
                .withEngine("sqlserver-ex")
                .withStorageType("gp2")  // <---- so we can keep the allocated storage small
                .withAllocatedStorage(20)
                .withMultiAZ(false)  // <---- so it can be stopped without error
                .withPubliclyAccessible(false)
                .withMasterUsername("integrationtest")
                .withMasterUserPassword("integrationtest")
                .withStorageEncrypted(false)  // <---- so the detector finds it
                .withDBParameterGroupName(sqlServerDBParameterGroupName)
                .withBackupRetentionPeriod(0)

        client.createDBInstance(dbInstanceRequest)
    }

    static def createOracleOptionGroup(AmazonRDS client) {
        CreateOptionGroupRequest createOptionGroupRequest = new CreateOptionGroupRequest()
                .withOptionGroupName(oracleOptionGroupName)
                .withOptionGroupDescription("Option group for integration tests")
                .withEngineName("oracle-se2")
                .withMajorEngineVersion("12.1")

        client.createOptionGroup(createOptionGroupRequest)
    }

    static def modifyOracleOptionGroup(AmazonRDS client) {
        ModifyOptionGroupRequest modifyOptionGroupRequest = new ModifyOptionGroupRequest()
                .withOptionGroupName(oracleOptionGroupName)
                .withApplyImmediately(true)
                .withOptionsToInclude(
                    new OptionConfiguration()
                        .withOptionName("NATIVE_NETWORK_ENCRYPTION")
                        .withOptionSettings(
                            new OptionSetting()
                                    .withName("SQLNET.ENCRYPTION_TYPES_SERVER")
                                    .withValue("RC4_256,AES256,AES192,3DES168,RC4_128,AES128,3DES112,RC4_56,DES,RC4_40,DES40")
                            ,new OptionSetting()
                                    .withName("SQLNET.CRYPTO_CHECKSUM_TYPES_SERVER")
                                    .withValue("SHA1,MD5")
                            ,new OptionSetting()
                                    .withName("SQLNET.CRYPTO_CHECKSUM_SERVER")
                                    .withValue("REQUESTED")
                            ,new OptionSetting()
                                    .withName("SQLNET.ENCRYPTION_SERVER")
                                    .withValue("REQUESTED")  // <---- so the detector finds it
                )
        )

        client.modifyOptionGroup(modifyOptionGroupRequest)
    }

    static def createOracleWithEncryptedStorage(AmazonRDS client) {
        CreateDBInstanceRequest dbInstanceRequest = new CreateDBInstanceRequest()
                .withDBInstanceIdentifier(oracleDBInstanceIdentifier)
                .withDBName("itOracle")
                .withDBInstanceClass("db.t2.small")
                .withEngine("oracle-se2")
                .withLicenseModel("license-included")
                .withStorageType("gp2")  // <---- so we can keep the allocated storage small
                .withAllocatedStorage(20)
                .withMultiAZ(false)  // <---- so it can be stopped without error
                .withPubliclyAccessible(false)
                .withMasterUsername("integrationtest")
                .withMasterUserPassword("integrationtest")
                .withStorageEncrypted(true)  // <---- so the detector does not find it
                .withOptionGroupName(oracleOptionGroupName)
                .withBackupRetentionPeriod(0)

        client.createDBInstance(dbInstanceRequest)
    }

    static def createRdsDisallowedSubnetGroup(AmazonEC2 ec2Client, AmazonRDS rdsClient) {
        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client)
        if (disallowedSubnets.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")

        CreateDBSubnetGroupRequest createDBSubnetGroupRequest = new CreateDBSubnetGroupRequest()
                .withDBSubnetGroupName(rdsDisallowedSubnetGroupName)
                .withDBSubnetGroupDescription(rdsDisallowedSubnetGroupName)
                .withSubnetIds(disallowedSubnets)  // <---- so the detector finds it

        rdsClient.createDBSubnetGroup(createDBSubnetGroupRequest)
    }

    static def createDBInstanceInDisallowedSubnet(AmazonRDS client) {
        CreateDBInstanceRequest dbInstanceRequest = new CreateDBInstanceRequest()
                .withDBInstanceIdentifier(rdsInDisallowedSubnetDBInstanceIdentifier)
                .withDBName("rdsInDisallowedSubnetDBInstanceIdentifier")
                .withDBInstanceClass("db.t2.small")
                .withEngine("postgres")
                .withEngineVersion("9.6.6")
                .withStorageType("standard")  // <---- so we can keep the allocated storage small
                .withAllocatedStorage(5)
                .withMultiAZ(false)  // <---- so it can be stopped without error
                .withPubliclyAccessible(false)
                .withMasterUsername("integrationtest")
                .withMasterUserPassword("integrationtest")
                .withStorageEncrypted(true)
                .withDBParameterGroupName(postgresDBParameterGroupName)
                .withDBSubnetGroupName(rdsDisallowedSubnetGroupName)
                .withBackupRetentionPeriod(0)

        client.createDBInstance(dbInstanceRequest)
    }

    static def deleteOldRdsSnapshots(AmazonRDS client) {
        // don't need to bother with paginated results since we'll delete many more snapshots
        // than we create on each test run
        DescribeDBSnapshotsRequest describeDBSnapshotsRequest = new DescribeDBSnapshotsRequest()
                .withMaxRecords(100)
                .withSnapshotType("manual")
        DescribeDBSnapshotsResult describeDBSnapshotsResult = client.describeDBSnapshots(describeDBSnapshotsRequest)

        describeDBSnapshotsResult.getDBSnapshots().findAll {
            it.status == 'available'  // only 'available' snapshots can be deleted
        }.each {
            DeleteDBSnapshotRequest deleteDBSnapshotRequest = new DeleteDBSnapshotRequest()
                    .withDBSnapshotIdentifier(it.getDBSnapshotIdentifier())
            client.deleteDBSnapshot(deleteDBSnapshotRequest)
        }
    }

    static def createElastiCacheMemcached(AmazonElastiCache client) {
        CreateCacheClusterRequest createCacheClusterRequest = new CreateCacheClusterRequest()
                .withCacheClusterId(memcachedCacheClusterId)
                .withEngine("memcached")
                .withCacheNodeType("cache.t2.micro")
                .withNumCacheNodes(1)

        client.createCacheCluster(createCacheClusterRequest)
    }

    static def createElastiCacheRedis(AmazonElastiCache client) {
        CreateCacheClusterRequest createCacheClusterRequest = new CreateCacheClusterRequest()
                .withCacheClusterId(redisCacheClusterId)
                .withEngine("redis")
                .withCacheNodeType("cache.t2.micro")
                .withNumCacheNodes(1)

        client.createCacheCluster(createCacheClusterRequest)
    }

    static def createDynamoDbTable(AmazonDynamoDB client) {
        CreateTableRequest createTableRequest = new CreateTableRequest()
                .withTableName(dynamoDbTableName)
                .withSSESpecification(new SSESpecification()
                                        .withEnabled(false))  // <---- server-side encryption off so the detector finds it
                .withProvisionedThroughput(new ProvisionedThroughput(1, 1))  // small to reduce cost
                .withAttributeDefinitions(new AttributeDefinition("pk", ScalarAttributeType.S))
                .withKeySchema(new KeySchemaElement("pk", KeyType.HASH))

        client.createTable(createTableRequest)
    }

    static def createRedshiftCluster(AmazonRedshift client) {
        com.amazonaws.services.redshift.model.CreateClusterRequest createClusterRequest =
            new com.amazonaws.services.redshift.model.CreateClusterRequest()
                .withClusterIdentifier(redshiftClusterIdentifier)
                .withNodeType("dc2.large")
                .withClusterType("single-node")
                .withPubliclyAccessible(false)
                .withEnhancedVpcRouting(false)
                .withMasterUsername("integrationtest")
                .withMasterUserPassword("Integrationtest1")
                .withEncrypted(false)  // <---- so the detector finds it

        client.createCluster(createClusterRequest)
    }

    static def createElasticBeanstalkApplication(AWSElasticBeanstalk ebsClient, AmazonS3 s3Client) {
        CreateApplicationRequest createApplicationRequest = new CreateApplicationRequest()
                .withApplicationName(ebsApplicationName)
                .withDescription("EBS application for integration tests")

        try {
            ebsClient.createApplication(createApplicationRequest)
        } catch (AWSElasticBeanstalkException e) {
            // ignore if the application already exists
            if (!e.getMessage().startsWith("Application ${ebsApplicationName} already exists"))
                throw e
        }

        // create the ZIP file for deployment
        File deploymentZip = File.createTempFile("ebs", ".zip")
        deploymentZip.deleteOnExit()

        new ZipOutputStream(new FileOutputStream(deploymentZip)).withCloseable {zipFile ->
            zipFile.putNextEntry(new ZipEntry("application.go"))

            File file = new File(getClass().getClassLoader().getResource("ebs-application/application.go").getFile())
            def buffer = new byte[1024]
            file.withInputStream { i ->
                def l = i.read(buffer)
                zipFile.write(buffer, 0, l)
            }

            zipFile.closeEntry()
        }

        // Deploy code
        CreateStorageLocationResult storageLocation = ebsClient.createStorageLocation()
        String bucket = storageLocation.getS3Bucket()
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucket, "ebs-1.0.zip", deploymentZip)
        s3Client.putObject(putObjectRequest)

        CreateApplicationVersionRequest createApplicationVersionRequest = new CreateApplicationVersionRequest()
                .withApplicationName(ebsApplicationName)
                .withVersionLabel("1.0")
                .withSourceBundle(new S3Location(bucket, "ebs-1.0.zip"))
        try {
            ebsClient.createApplicationVersion(createApplicationVersionRequest)
        } catch (AWSElasticBeanstalkException e) {
            // ignore if the version already exists
            if (!e.getMessage().startsWith("Application Version 1.0 already exists"))
                throw e
        }


        // https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
        // don't include aws:elasticbeanstalk:managedactions <---- so the detector finds it
        CreateEnvironmentRequest createEnvironmentRequest = new CreateEnvironmentRequest()
                .withEnvironmentName(ebsEnvironmentName)
                .withApplicationName(ebsApplicationName)
                .withSolutionStackName("64bit Amazon Linux 2017.09 v2.7.7 running Go 1.10")
                .withVersionLabel("1.0")
                .withOptionSettings(
                        new ConfigurationOptionSetting()
                                .withNamespace("aws:elasticbeanstalk:environment")
                                .withOptionName("ServiceRole")
                                .withValue("aws-elasticbeanstalk-service-role")
                        ,new ConfigurationOptionSetting()
                                .withNamespace("aws:autoscaling:launchconfiguration")
                                .withOptionName("IamInstanceProfile")
                                .withValue("aws-elasticbeanstalk-ec2-role"))

        try {
            ebsClient.createEnvironment(createEnvironmentRequest)
        } catch (AWSElasticBeanstalkException e) {
            // ignore if the environment already exists
            if (!e.getMessage().startsWith("Environment ${ebsEnvironmentName} already exists"))
                throw e
        }

        // wait for the environment to become 'Ready'
        DescribeEnvironmentsRequest describeEnvironmentsRequest = new DescribeEnvironmentsRequest()
                .withApplicationName(ebsApplicationName)
                .withEnvironmentNames(ebsEnvironmentName)

        for (i in 1..20) {
            DescribeEnvironmentsResult describeEnvironmentsResult = ebsClient.describeEnvironments(describeEnvironmentsRequest)
            def environments = describeEnvironmentsResult.getEnvironments().findAll {
                it.status != "Terminated" && it.status != "Terminating"
            }
            if (environments.size() != 1)
                throw new RuntimeException("Unexpected EB environments (after create)")
            if (environments[0].getStatus() == "Ready")
                break
            println("${ebsEnvironmentName} - waiting for Ready status (after create) but got ${environments[0].getStatus()}")
            sleep(1000 * 60)
        }

        // disable managed actions so the detector finds it
        UpdateEnvironmentRequest updateEnvironmentRequest = new UpdateEnvironmentRequest()
                .withEnvironmentName(ebsEnvironmentName)
                .withOptionSettings(
                    new ConfigurationOptionSetting()
                        .withNamespace("aws:elasticbeanstalk:managedactions")
                        .withOptionName("ManagedActionsEnabled")
                        .withValue("false"))

        ebsClient.updateEnvironment(updateEnvironmentRequest)

        // wait for the environment to become 'Ready'
        for (i in 1..20) {
            DescribeEnvironmentsResult describeEnvironmentsResult = ebsClient.describeEnvironments(describeEnvironmentsRequest)
            def environments = describeEnvironmentsResult.getEnvironments().findAll {
                it.status != "Terminated" && it.status != "Terminating"
            }
            if (environments.size() != 1)
                throw new RuntimeException("Unexpected EB environments")
            if (environments[0].getStatus() == "Ready") {
                // wait some more as it seems that the managed actions configuration updates
                // don't take effect immediately once the environment is marked 'Ready'
                sleep(1000 * 60 * 3)
                return
            }
            println("${ebsEnvironmentName} - waiting for Ready status (after update) but got ${environments[0].getStatus()}")
            sleep(1000 * 60)
        }

        throw new RuntimeException("EB environment is not available for tests")
    }

    static def createElasticContainerServiceInDisallowedSubnet(AmazonEC2 ec2Client, AmazonECS ecsClient) {
        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client)
        if (disallowedSubnets.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")


        com.amazonaws.services.ecs.model.CreateClusterRequest createClusterRequest =
            new com.amazonaws.services.ecs.model.CreateClusterRequest()
                .withClusterName(ecsClusterName)

        ecsClient.createCluster(createClusterRequest)

        CreateServiceRequest createServiceRequest = new CreateServiceRequest()
            .withCluster(ecsClusterName)
            .withServiceName(ecsServiceName)
            .withLaunchType("EC2")
            .withTaskDefinition("first-run-task-definition:1")
            .withDesiredCount(1)
            .withNetworkConfiguration(  // <---- launch in disallowed subnet so the detector finds it
                new NetworkConfiguration().withAwsvpcConfiguration(
                        new AwsVpcConfiguration().withSubnets(disallowedSubnets)))

        try {
            ecsClient.createService(createServiceRequest)
        } catch (AmazonECSException e) {
            // ignore if the service already exists
            if (!e.getMessage().startsWith("Creation of service was not idempotent"))
                throw e
        }
    }

    static def createElasticMapReduceClusterInDisallowedSubnet(AmazonEC2 ec2Client, AmazonElasticMapReduce emrClient) {
        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client)
        if (disallowedSubnets.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")

        StepConfig enableDebugging = new StepConfig()
                .withName("Setup Hadoop Debugging")
                .withActionOnFailure(ActionOnFailure.CONTINUE)
                .withHadoopJarStep(new HadoopJarStepConfig()
                        .withJar("command-runner.jar")
                        .withArgs("state-pusher-script"))

        RunJobFlowRequest runJobFlowRequest = new RunJobFlowRequest()
                .withName(emrClusterName)
                .withReleaseLabel("emr-5.13.0")
                .withSteps(enableDebugging)
                .withServiceRole("EMR_DefaultRole")
                .withJobFlowRole("EMR_EC2_DefaultRole")
                .withVisibleToAllUsers(true)
                .withInstances(new JobFlowInstancesConfig()
                        .withInstanceCount(2)
                        .withMasterInstanceType("m4.large")
                        .withSlaveInstanceType("m4.large")
                        .withTerminationProtected(true)
                        .withKeepJobFlowAliveWhenNoSteps(true)
                        .withEc2SubnetId(disallowedSubnets[0]))  // <---- so the detector finds it

        RunJobFlowResult runJobFlowResult = emrClient.runJobFlow(runJobFlowRequest)
        //println("created ${runJobFlowRequest.getName()} ::: ${runJobFlowResult}")
    }
}