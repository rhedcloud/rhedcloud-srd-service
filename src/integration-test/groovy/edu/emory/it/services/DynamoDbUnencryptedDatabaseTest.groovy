package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import com.amazonaws.services.dynamodbv2.model.ResourceInUseException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.DynamoDbUnencryptedDatabaseDetector
import edu.emory.it.services.srd.remediator.DynamoDbUnencryptedDatabaseRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class DynamoDbUnencryptedDatabaseTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        DynamoDbUnencryptedDatabaseDetector detector = new DynamoDbUnencryptedDatabaseDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        DynamoDbUnencryptedDatabaseRemediator remediator = new DynamoDbUnencryptedDatabaseRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.DynamoDbUnencryptedDatabase.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:dynamodb:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:table/${RdsDatabaseHelper.dynamoDbTableName}"

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Passive Remediation"
    }

    def setupSpec() {
        AmazonDynamoDB client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonDynamoDBClient.class)

        try {
            RdsDatabaseHelper.createDynamoDbTable(client)
        } catch (ResourceInUseException e) {
            // ignore
        }
    }
}
