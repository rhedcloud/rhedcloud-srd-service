package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.guardduty.AmazonGuardDuty
import com.amazonaws.services.guardduty.AmazonGuardDutyClient
import com.amazonaws.services.guardduty.model.CreateDetectorRequest
import com.amazonaws.services.guardduty.model.DeleteDetectorRequest
import com.amazonaws.services.guardduty.model.ListDetectorsRequest
import com.amazonaws.services.guardduty.model.ListDetectorsResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.GuardDutyMisconfigurationDetector
import edu.emory.it.services.srd.remediator.GuardDutyMisconfigurationRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - ignored because test requires DeleteDetector & CreateDetector permissions
class GuardDutyMisconfigurationTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        PropertyConfig guardDutyMisconfigurationProperties = Mock()
        guardDutyMisconfigurationProperties.getProperties() >> new Properties() {{
            setProperty("GuardDutyMasterAccountID", ITAccount.getGuardDutyMasterAccount())
            setProperty("GuardDutySplunkCfnTemplate", "https://s3.amazonaws.com/emory-aws-dev-cfn-templates/rhedcloud-aws-guardduty-splunk-cfn.compact.json")
        }}
        appConfig.getObjects() >> AppConfigHelper.createEOMock([
            ("GuardDutyMisconfiguration".toLowerCase()) : guardDutyMisconfigurationProperties
        ])

        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        GuardDutyMisconfigurationDetector detector = new GuardDutyMisconfigurationDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        GuardDutyMisconfigurationRemediator remediator = new GuardDutyMisconfigurationRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "one risk detected for each region (us-east-1 and us-east-2)"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        then:
        detected.size() == 2


        when: "us-east-1 - the GuardDuty detector was created in this region but isn't enabled"
        def east1 = detected.find { it.getAmazonResourceName().startsWith("arn:aws:guardduty:us-east-1:${ITAccount.getAccountId()}:detector/") }
        then:
        east1 != null
        east1.getType() == DetectionType.GuardDutyMisconfiguration.name()
        east1.getProperties().size() == 2
        east1.getProperties().containsKey("detectorId")
        east1.getProperties().containsKey("status")

        when: "us-east-1 - during remediation the GuardDuty detector is enabled"
        remediator.remediate(ITAccount.getAccountId(), east1, srContext)
        then:
        east1.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        east1.getRemediationResult().getDescription() == "Detector ${east1.getAmazonResourceName()} enabled. Member associated to GuardDuty master. "


        when: "us-east-2 - the GuardDuty detector wasn't created in this region"
        def east2 = detected.find { it.getAmazonResourceName().startsWith("arn:aws:guardduty:us-east-2:${ITAccount.getAccountId()}:detector/") }
        then:
        east2 != null
        east2.getType() == DetectionType.GuardDutyMisconfiguration.name()

        when: "us-east-2 - during remediation the GuardDuty detector is created"
        remediator.remediate(ITAccount.getAccountId(), east2, srContext)
        then:
        east2.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        east2.getRemediationResult().getDescription().startsWith("Created detector with ID ")
        east2.getRemediationResult().getDescription().contains("Member associated to GuardDuty master.")
    }

    def setupSpecTODO() {
        setupEast1()
        setupEast2()
    }

    def setupEast1() {
        AmazonGuardDuty client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion("us-east-1")  // hard-coded because test relies on the GuardDuty detector being in this region
                .withRole(ITAccount.getRole())
                .build(AmazonGuardDutyClient.class)

        // delete the detector if it's there
        ListDetectorsResult listDetectorsResult = client.listDetectors(new ListDetectorsRequest())

        if (listDetectorsResult.detectorIds.size() > 0) {
            DeleteDetectorRequest deleteDetectorRequest = new DeleteDetectorRequest()
                    .withDetectorId(listDetectorsResult.detectorIds[0])
            client.deleteDetector(deleteDetectorRequest)
        }

        // then, create a detector in a disabled state
        CreateDetectorRequest createDetectorRequest = new CreateDetectorRequest()
                .withEnable(false)  // <---- so the detector finds it
        client.createDetector(createDetectorRequest)
    }

    def setupEast2() {
        AmazonGuardDuty client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion("us-east-2")  // hard-coded because test relies on there being no GuardDuty detector in this region
                .withRole(ITAccount.getRole())
                .build(AmazonGuardDutyClient.class)

        // delete the detector if it's there
        ListDetectorsResult listDetectorsResult = client.listDetectors(new ListDetectorsRequest())

        if (listDetectorsResult.detectorIds.size() > 0) {
            DeleteDetectorRequest deleteDetectorRequest = new DeleteDetectorRequest()
                    .withDetectorId(listDetectorsResult.detectorIds[0])
            client.deleteDetector(deleteDetectorRequest)
        }
    }
}
