package edu.emory.it.services

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.codebuild.model.VpcConfig
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.sagemaker.AmazonSageMaker
import com.amazonaws.services.sagemaker.AmazonSageMakerClient
import com.amazonaws.services.sagemaker.model.CreateNotebookInstanceRequest
import com.amazonaws.services.sagemaker.model.InstanceType
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.detector.SageMakerWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.SageMakerWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class SageMakerWithinManagementSubnetsTest extends Specification {

    public static final String NOTEBOOK_INSTANCE_NAME = "integration-test-sagemaker-notebook"

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        SageMakerWithinManagementSubnetsDetector detector = new SageMakerWithinManagementSubnetsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        SageMakerWithinManagementSubnetsRemediator remediator = new SageMakerWithinManagementSubnetsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)
        SecurityRiskDetection securityRiskDetection = UTSecurityRiskDetection.create()

        when:
        detector.detect(securityRiskDetection, ITAccount.getAccountId(), srContext)

        then: "risks should be from SageMaker"
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            risk.getType() == DetectionType.SageMakerWithinManagementSubnets.name()
            risk.getAmazonResourceName().startsWith("arn:aws:sagemaker:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:")
        }

        when:
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            remediator.remediate(ITAccount.getAccountId(), risk, srContext)
        }

        then: "the risks should be remediated"
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            risk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        }
    }

    def setupSpec() {
        AmazonEC2 ec2Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonEC2Client.class)
        AmazonSageMaker sageMakerClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonSageMakerClient.class)

        List<VpcConfig> disallowedSubnets = ITAccount.getDisallowedVpcConfigs(ec2Client)
        if (disallowedSubnets.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")
        VpcConfig disallowedSubnet = disallowedSubnets[0]

        CreateNotebookInstanceRequest createNotebookInstanceRequest = new CreateNotebookInstanceRequest()
                .withNotebookInstanceName(NOTEBOOK_INSTANCE_NAME)
                .withInstanceType(InstanceType.MlT2Medium)
                .withRoleArn("arn:aws:iam::${ITAccount.getAccountId()}:role/service-role/AmazonSageMaker-ExecutionRole-20180920T082331")
                .withSecurityGroupIds(disallowedSubnet.securityGroupIds[0])
                .withSubnetId(disallowedSubnet.subnets[0])
        sageMakerClient.createNotebookInstance(createNotebookInstanceRequest)
    }
}
