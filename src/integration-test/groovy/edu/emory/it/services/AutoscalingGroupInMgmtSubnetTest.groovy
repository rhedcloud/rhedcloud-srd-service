package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.autoscaling.AmazonAutoScaling
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient
import com.amazonaws.services.autoscaling.model.AmazonAutoScalingException
import com.amazonaws.services.autoscaling.model.CreateAutoScalingGroupRequest
import com.amazonaws.services.autoscaling.model.CreateAutoScalingGroupResult
import com.amazonaws.services.autoscaling.model.CreateLaunchConfigurationRequest
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsRequest
import com.amazonaws.services.autoscaling.model.DescribeAutoScalingGroupsResult
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult
import com.amazonaws.services.ec2.model.Filter
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.AutoscalingGroupInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.AutoscalingGroupInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class AutoscalingGroupInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonAutoScaling asClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonAutoScalingClient.class)

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        AutoscalingGroupInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        AutoscalingGroupInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        String arn = createAutoscalingGroup(ec2, asClient)

        when: "detecting an account with an Auto Scaling group in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.AutoscalingGroupInMgmtSubnet.name() == it.getType() && arn == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null


        when: "remediate an account with an Auto Scaling group in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        sleep(2000)
        then: "remediate should delete the group"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()

        when: "detecting an account with deleted Auto Scaling group"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected2 = detecteds2.find({
            DetectionType.AutoscalingGroupInMgmtSubnet.name() == it.getType() && arn == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected2 == null


    }


    private AutoscalingGroupInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        AutoscalingGroupInMgmtSubnetDetector underTest = new AutoscalingGroupInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private AutoscalingGroupInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        AutoscalingGroupInMgmtSubnetRemediator underTest = new AutoscalingGroupInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createAutoscalingGroup(AmazonEC2 ec2, AmazonAutoScaling asClient) {

        CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest()
                .withGroupName("srd-test-autoscaling-group")
                .withVpcId(ITAccount.getVPCWithDisallowedSubnet())
                .withDescription("Security group for testing")

        try {
            CreateSecurityGroupResult csgrResult = ec2.createSecurityGroup(csgr)
        } catch (AmazonEC2Exception e) {
            if (e.getErrorCode().equals("InvalidGroup.Duplicate")) {
                // if already exist, don't worry about it
            } else {
                throw e
            }
        }

        DescribeSecurityGroupsRequest describeSecurityGroupsRequest = new DescribeSecurityGroupsRequest().withFilters(new Filter("vpc-id", [ITAccount.getVPCWithDisallowedSubnet()]))
        DescribeSecurityGroupsResult describeSecurityGroupsResult = ec2.describeSecurityGroups(describeSecurityGroupsRequest);

        String securityGroupId = describeSecurityGroupsResult.getSecurityGroups().find { it.groupName == "srd-test-autoscaling-group" }.getGroupId();

        CreateLaunchConfigurationRequest createLaunchConfigurationRequest = new CreateLaunchConfigurationRequest()
                .withLaunchConfigurationName("emory-integration-test-launch-config")
                .withImageId(ITAccount.getImageIdInRegion())
                .withInstanceType("t2.micro")
                .withSecurityGroups(securityGroupId)

        try {
            asClient.createLaunchConfiguration(createLaunchConfigurationRequest)
        } catch (AmazonAutoScalingException e) {
            if (e.getErrorCode().equals("AlreadyExists")) {
                // if already exist, don't worry about it
            } else {
                throw e
            }
        }

        String autoScalingGroupName = "emory-integration-test-group";
        CreateAutoScalingGroupRequest autoScalingGroupRequest = new CreateAutoScalingGroupRequest()
                .withAutoScalingGroupName(autoScalingGroupName)
                .withLaunchConfigurationName("emory-integration-test-launch-config")
                .withVPCZoneIdentifier(ITAccount.getExistingDisallowedSubnet())
                .withMaxSize(1)
                .withMinSize(1)


        try {
            CreateAutoScalingGroupResult result = asClient.createAutoScalingGroup(autoScalingGroupRequest)
        } catch (Exception e) {
            throw e
        }

        DescribeAutoScalingGroupsRequest describeAutoScalingGroupsRequest = new DescribeAutoScalingGroupsRequest().withAutoScalingGroupNames(autoScalingGroupName)
        DescribeAutoScalingGroupsResult describeAutoScalingGroupsResult = asClient.describeAutoScalingGroups(describeAutoScalingGroupsRequest);
        return describeAutoScalingGroupsResult.getAutoScalingGroups().get(0).getAutoScalingGroupARN()
    }
}
