package edu.emory.it.services

/**
 * RDS related integration tests.
 * Broken out because they're generally quite slow.
 *
 * Category #4 is for the next sqlserver related test.
 *
 * Other sqlserver tests were be put in category #2 so that the instance
 *  can get closer to transitioning to a 'stopped' state before these tests run.
 */
interface ITCategoryRds4 {}
