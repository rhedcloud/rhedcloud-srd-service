package edu.emory.it.services

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.codebuild.AWSCodeBuild
import com.amazonaws.services.codebuild.AWSCodeBuildClient
import com.amazonaws.services.codebuild.model.ArtifactsType
import com.amazonaws.services.codebuild.model.BatchGetProjectsRequest
import com.amazonaws.services.codebuild.model.BatchGetProjectsResult
import com.amazonaws.services.codebuild.model.CreateProjectRequest
import com.amazonaws.services.codebuild.model.EnvironmentType
import com.amazonaws.services.codebuild.model.ProjectArtifacts
import com.amazonaws.services.codebuild.model.ProjectEnvironment
import com.amazonaws.services.codebuild.model.ProjectSource
import com.amazonaws.services.codebuild.model.SourceType
import com.amazonaws.services.codebuild.model.VpcConfig
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.detector.CodeBuildWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.CodeBuildWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // Creating a CodeBuild project with VPC info gives: Not authorized to perform DescribeSecurityGroups
class CodeBuildWithinManagementSubnetsTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSCodeBuild codeBuild = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSCodeBuildClient.class)

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        List<VpcConfig> disallowedVpcConfigs = ITAccount.getDisallowedVpcConfigs(ec2)
        if (disallowedVpcConfigs.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")
        VpcConfig disallowedVpcConfig = disallowedVpcConfigs[0]

        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()

        CodeBuildWithinManagementSubnetsDetector detector = new CodeBuildWithinManagementSubnetsDetector()
        CodeBuildWithinManagementSubnetsRemediator remediator = new CodeBuildWithinManagementSubnetsRemediator()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        String projectName = createProject(codeBuild, disallowedVpcConfig)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)
        SecurityRiskDetection securityRiskDetection = UTSecurityRiskDetection.create()

        when: "detect an at risk CodeBuild project"
        detector.detect(securityRiskDetection, ITAccount.getAccountId(), srContext)

        def filteredRisks = securityRiskDetection.detectedSecurityRisk.findAll({
            DetectionType.CodeBuildVPCMisconfigurationInMgmtSubnet.name() == ((DetectedSecurityRisk) it).type &&
                    ((DetectedSecurityRisk) it).amazonResourceName.contains(projectName)
        })

        then: "error should be detected"
        filteredRisks != null
        filteredRisks.size() == 1

        when: "remediate the CodeBuild project"
        DetectedSecurityRisk detected = (DetectedSecurityRisk) filteredRisks[0]
        remediator.remediate(ITAccount.getAccountId(), detected, srContext)
        sleep(60000)

        then: "remediate should delete the project"
        detected.remediationResult.status == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.remediationResult.description != null
        projectWasDeleted(codeBuild, projectName)
    }

    private static String createProject(AWSCodeBuild codeBuild, VpcConfig vpcConfig) {
        String projectName = "integration-test-disallowed-subnet-${System.currentTimeMillis()}"
        // the S3 bucket for the source needs to exist
        // the service role is something like: "arn:aws:iam::${ITAccount.getAccountId()}:role/service-role/codebuild-khb-codebuild-project-service-role"
        CreateProjectRequest createProjectRequest = new CreateProjectRequest()
                .withName(projectName)
                .withSource(new ProjectSource().withType(SourceType.S3).withLocation("integration-test-codebuild-bucket/"))
                .withEnvironment(new ProjectEnvironment().withType(EnvironmentType.LINUX_CONTAINER).withImage("aws/codebuild/java:openjdk-8"))
                .withServiceRole("arn:aws:iam::${ITAccount.getAccountId()}:role/service-role/blah")
                .withArtifacts(new ProjectArtifacts().withType(ArtifactsType.NO_ARTIFACTS))
                .withVpcConfig(vpcConfig)

        codeBuild.createProject(createProjectRequest)

        return projectName
    }

    private static boolean projectWasDeleted(AWSCodeBuild codeBuild, String projectName) {
        BatchGetProjectsRequest batchGetProjectsRequest = new BatchGetProjectsRequest().withNames(projectName)
        BatchGetProjectsResult batchGetProjectsResult = codeBuild.batchGetProjects(batchGetProjectsRequest)
        return batchGetProjectsResult.getProjectsNotFound().contains(projectName)
    }
}
