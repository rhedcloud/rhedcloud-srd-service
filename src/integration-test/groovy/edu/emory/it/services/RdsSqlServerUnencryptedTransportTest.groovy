package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.rds.AmazonRDS
import com.amazonaws.services.rds.AmazonRDSClient
import com.amazonaws.services.rds.model.DBInstanceAlreadyExistsException
import com.amazonaws.services.rds.model.DBInstanceNotFoundException
import com.amazonaws.services.rds.model.DBParameterGroupAlreadyExistsException
import com.amazonaws.services.rds.model.InvalidDBInstanceStateException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RdsSqlServerUnencryptedTransportDetector
import edu.emory.it.services.srd.remediator.RdsSqlServerUnencryptedTransportRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryRds2.class)
class RdsSqlServerUnencryptedTransportTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RdsSqlServerUnencryptedTransportDetector detector = new RdsSqlServerUnencryptedTransportDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RdsSqlServerUnencryptedTransportRemediator remediator = new RdsSqlServerUnencryptedTransportRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.RdsSqlServerUnencryptedTransport.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:rds:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:db:${RdsDatabaseHelper.sqlServerDBInstanceIdentifier}"
        // ensure properties are set that are used to pass information to the remediator
        detected.get(0).getProperties().size() == 2
        detected.get(0).getProperties().containsKey("DBInstanceIdentifier")
        detected.get(0).getProperties().containsKey("DBInstanceStatus")

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription().contains("stopped with snapshot")
    }

    def setupSpec() {
        AmazonRDS client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonRDSClient.class)

        RdsDatabaseHelper.deleteOldRdsSnapshots(client)

        // create the parameter group
        try {
            RdsDatabaseHelper.createSqlServerDBParameterGroup(client)
        } catch (DBParameterGroupAlreadyExistsException e) {
            // ignore
        }

        // make sure the parameter group has the correct SSL setting
        RdsDatabaseHelper.modifySqlServerDBParameterGroup(client)

        // create the DB instance
        try {
            RdsDatabaseHelper.createSqlServerWithEncryptedStorage(client)
        } catch (DBInstanceAlreadyExistsException e) {
            // ignore
        }

        // if the instance is still stopping from a previous run then wait for it to finish
        // and become stopped.
        try {
            RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.sqlServerDBInstanceIdentifier, ['available', 'stopped'], client)
        } catch (DBInstanceNotFoundException e) {
            // ignore because we're about to create it
        }

        // start up the instance
        try {
            RdsDatabaseHelper.startDBInstance(RdsDatabaseHelper.sqlServerDBInstanceIdentifier, client)
        } catch (InvalidDBInstanceStateException e) {
            // ignore - may already be running
        }

        // now wait for it to finish starting and become available
        RdsDatabaseHelper.waitForDBInstanceStatus(RdsDatabaseHelper.sqlServerDBInstanceIdentifier, ['available'], client)
    }
}