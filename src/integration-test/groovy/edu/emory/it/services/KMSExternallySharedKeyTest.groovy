package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.kms.AWSKMS
import com.amazonaws.services.kms.AWSKMSClient
import com.amazonaws.services.kms.model.CreateKeyResult
import com.amazonaws.services.kms.model.DisableKeyRequest
import com.amazonaws.services.kms.model.GetKeyPolicyRequest
import com.amazonaws.services.kms.model.GetKeyPolicyResult
import com.amazonaws.services.kms.model.PutKeyPolicyRequest
import com.amazonaws.services.kms.model.ScheduleKeyDeletionRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.KMSExternallySharedKeyDetector
import edu.emory.it.services.srd.remediator.KMSExternallySharedKeyRemediator
import edu.emory.it.services.srd.util.AWSPolicyUtil
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class KMSExternallySharedKeyTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSKMS kms = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSKMSClient.class)

        KMSExternallySharedKeyDetector detector = setupDetector(credentialsProvider)
        KMSExternallySharedKeyRemediator remediator = setupRemediator(credentialsProvider)

        String keyId = createKey(kms)
        addExternalPolicy(kms, keyId)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a queue with an external id"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.KMSExternallySharedKey.name() == it.getType() &&
                    ("arn:aws:kms:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:key/${keyId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a queue with an external id"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        waitTillPolicyHasChanged(kms, ITAccount.getAccountId(), keyId)

        then: "remediate remove the permission"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isExternallyShared(kms, ITAccount.getAccountId(), keyId) == false

        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.KMSExternallySharedKey.name() == it.getType() &&
                    ("arn:aws:kms:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:key/${keyId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteKey(kms, keyId)

    }

    void addExternalPolicy(AWSKMS kms, String keyId) {
        PutKeyPolicyRequest request = new PutKeyPolicyRequest().withKeyId(keyId).withPolicyName("default")
            .withPolicy("""
{
  "Version": "2012-10-17",
  "Id": "key-consolepolicy-3",
  "Statement": [
    {
      "Sid": "Enable IAM User Permissions",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${ITAccount.getAccountId()}:root"
      },
      "Action": "kms:*",
      "Resource": "*"
    },
    {
      "Sid": "Allow access for Key Administrators",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${ITAccount.getAccountId()}:role/${ITAccount.getRole()}"
        ]
      },
      "Action": [
        "kms:Create*",
        "kms:Describe*",
        "kms:Enable*",
        "kms:List*",
        "kms:Put*",
        "kms:Update*",
        "kms:Revoke*",
        "kms:Disable*",
        "kms:Get*",
        "kms:Delete*",
        "kms:TagResource",
        "kms:UntagResource",
        "kms:ScheduleKeyDeletion",
        "kms:CancelKeyDeletion"
      ],
      "Resource": "*"
    },
    {
      "Sid": "Allow use of the key",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${ITAccount.getMasterAccount()}:root"
        ]
      },
      "Action": [
        "kms:Encrypt",
        "kms:Decrypt",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:DescribeKey"
      ],
      "Resource": "*"
    },
    {
      "Sid": "Allow attachment of persistent resources",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "arn:aws:iam::${ITAccount.getMasterAccount()}:root"
        ]
      },
      "Action": [
        "kms:CreateGrant",
        "kms:ListGrants",
        "kms:RevokeGrant"
      ],
      "Resource": "*",
      "Condition": {
        "Bool": {
          "kms:GrantIsForAWSResource": true
        }
      }
    }
  ]
}
            """)
        kms.putKeyPolicy(request)
    }

    private KMSExternallySharedKeyDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        KMSExternallySharedKeyDetector underTest = new KMSExternallySharedKeyDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private KMSExternallySharedKeyRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        KMSExternallySharedKeyRemediator underTest = new KMSExternallySharedKeyRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createKey(AWSKMS kms) {
        CreateKeyResult result = kms.createKey()
        return result.getKeyMetadata().getKeyId()
    }

    private void deleteKey(AWSKMS kms, String keyId) {
        if (keyId) {
            DisableKeyRequest request = new DisableKeyRequest().withKeyId(keyId)
            kms.disableKey(request)
            ScheduleKeyDeletionRequest scheduleKeyDeletionRequest = new ScheduleKeyDeletionRequest().withKeyId(keyId)
            kms.scheduleKeyDeletion(scheduleKeyDeletionRequest)
        }
    }

    private boolean isExternallyShared(AWSKMS kms, String accountId, String keyId) {
        GetKeyPolicyRequest getKeyPolicyRequest = new GetKeyPolicyRequest().withKeyId(keyId).withPolicyName("default")
        GetKeyPolicyResult getKeyPolicyResult = kms.getKeyPolicy(getKeyPolicyRequest)
        def whitelistedAccounts = Collections.singleton(accountId)
        String policy = getKeyPolicyResult.getPolicy()
        return AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy);
    }

    private void waitTillPolicyHasChanged(AWSKMS kms, String accountId, String keyId) {
        boolean done = false
        for (int i=0; i<20 && !done; i++) {
            done = !isExternallyShared(kms, accountId, keyId)
            sleep(6000)
        }
    }
}

