package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.redshift.AmazonRedshift
import com.amazonaws.services.redshift.AmazonRedshiftClient
import com.amazonaws.services.redshift.model.ClusterAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RedshiftUnencryptedRestDetector
import edu.emory.it.services.srd.remediator.RedshiftUnencryptedRestRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class RedshiftUnencryptedRestTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RedshiftUnencryptedRestDetector detector = new RedshiftUnencryptedRestDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RedshiftUnencryptedRestRemediator remediator = new RedshiftUnencryptedRestRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.RedshiftUnencryptedRest.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:redshift:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:cluster:${RdsDatabaseHelper.redshiftClusterIdentifier}"

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Passive Remediation"
    }

    def setupSpec() {
        AmazonRedshift client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonRedshiftClient.class)

        try {
            RdsDatabaseHelper.createRedshiftCluster(client)
        } catch (ClusterAlreadyExistsException e) {
            // ignore
        }
    }
}
