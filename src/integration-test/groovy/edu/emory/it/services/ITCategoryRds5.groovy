package edu.emory.it.services

/**
 * RDS related integration tests.
 * Broken out because they're generally quite slow.
 *
 * Category #5 is the next set of RDS tests.
 */
interface ITCategoryRds5 {}
