package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.elasticsearch.AWSElasticsearch
import com.amazonaws.services.elasticsearch.AWSElasticsearchClient
import com.amazonaws.services.elasticsearch.model.CreateElasticsearchDomainRequest
import com.amazonaws.services.elasticsearch.model.EBSOptions
import com.amazonaws.services.elasticsearch.model.ESPartitionInstanceType
import com.amazonaws.services.elasticsearch.model.ElasticsearchClusterConfig
import com.amazonaws.services.elasticsearch.model.VPCOptions
import com.amazonaws.services.elasticsearch.model.VolumeType
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.ElasticSearchInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.ElasticSearchInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - ignored because we don't have permission to list Elasticsearch Domains in test account
class ElasticSearchInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSElasticsearch elasticSearch = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSElasticsearchClient.class)

        ElasticSearchInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        ElasticSearchInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        String domainName = createDomain(elasticSearch)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with spot requests in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.ElasticSearchInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:es:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:domain/${domainName}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null

        when: "remediate an account with spot requests in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        sleep(2000)

        then: "remediate should terminate the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()


        when: "detecting an account with stopped instances that have unencrypted instances"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected3 = detecteds2.find({
            DetectionType.ElasticSearchInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:es:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:domain/${domainName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected3 == null

    }


    private ElasticSearchInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        ElasticSearchInMgmtSubnetDetector underTest = new ElasticSearchInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private ElasticSearchInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        ElasticSearchInMgmtSubnetRemediator underTest = new ElasticSearchInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createDomain(AWSElasticsearch elasticsearch) {
        String domainName = "srd-test-es-in-disallowed-subnet";
        CreateElasticsearchDomainRequest createElasticsearchDomainRequest = new CreateElasticsearchDomainRequest()
            .withDomainName(domainName)
            .withVPCOptions(new VPCOptions()
                .withSubnetIds(ITAccount.getExistingDisallowedSubnet())
            )
            .withElasticsearchClusterConfig(new ElasticsearchClusterConfig()
                .withInstanceType(ESPartitionInstanceType.T2SmallElasticsearch)
                .withInstanceCount(1)
                .withDedicatedMasterEnabled(false)
                .withZoneAwarenessEnabled(false)
            )
            .withElasticsearchVersion("6.2")
            .withEBSOptions(new EBSOptions()
                .withVolumeType(VolumeType.Gp2)
                .withVolumeSize(10)
                .withEBSEnabled(true)
            )


        elasticsearch.createElasticsearchDomain(createElasticsearchDomainRequest)


        return domainName
    }
}
