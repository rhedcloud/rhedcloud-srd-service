package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.ElasticBeanstalkDisabledManagedUpdatesDetector
import edu.emory.it.services.srd.remediator.ElasticBeanstalkDisabledManagedUpdatesRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast2.class)
class ElasticBeanstalkDisabledManagedUpdatesTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        ElasticBeanstalkDisabledManagedUpdatesDetector detector = new ElasticBeanstalkDisabledManagedUpdatesDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        ElasticBeanstalkDisabledManagedUpdatesRemediator remediator = new ElasticBeanstalkDisabledManagedUpdatesRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        def expectedArn = "arn:aws:elasticbeanstalk:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:environment/${RdsDatabaseHelper.ebsApplicationName}/${RdsDatabaseHelper.ebsEnvironmentName}"

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            it.getAmazonResourceName() == expectedArn
        })

        then:
        detectedRisk != null
        detectedRisk.getType() == DetectionType.ElasticBeanstalkDisabledManagedUpdates.name()
        // ARN match done above in the find()

        when:
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)

        then:
        detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detectedRisk.getRemediationResult().getDescription() == "Managed actions enabled for environment ${RdsDatabaseHelper.ebsEnvironmentName}"
    }

    def setupSpec() {
        AWSElasticBeanstalk ebsClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AWSElasticBeanstalkClient.class)

        AmazonS3 s3Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonS3Client.class)

        RdsDatabaseHelper.createElasticBeanstalkApplication(ebsClient, s3Client)
    }
}
