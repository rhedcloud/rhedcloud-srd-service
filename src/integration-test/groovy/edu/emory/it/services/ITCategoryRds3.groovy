package edu.emory.it.services

/**
 * RDS related integration tests.
 * Broken out because they're generally quite slow.
 *
 * Category #3 is for the next Postgres related test.
 *
 * Other Postgres tests were be put in category #1 so that the instance
 *  can get closer to transitioning to a 'stopped' state before these tests run.
 */
interface ITCategoryRds3 {}
