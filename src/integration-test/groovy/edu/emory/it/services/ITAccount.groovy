package edu.emory.it.services

import com.amazonaws.services.codebuild.model.VpcConfig
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult
import com.amazonaws.services.ec2.model.DescribeSubnetsResult
import com.amazonaws.services.ec2.model.DescribeVpcsResult
import com.amazonaws.services.ec2.model.Filter
import com.amazonaws.services.ec2.model.SecurityGroup
import com.amazonaws.services.ec2.model.Subnet
import com.amazonaws.services.ec2.model.Tag
import edu.emory.it.services.srd.util.VpcUtil

class ITAccount {
    static def serviceForge = [
            standardAccountId    : "085920342244",
            standardAccountName  : "AWS Admin 128",
            standardAccountEmail : "aws-admin-128@serviceforge.net",
            hipaaAccountId       : "958348860784",
            hipaaAccountName     : "Serviceforge 127",
            hipaaAccountEmail    : "aws-admin-127@serviceforge.net",
            masterAccountId      : "415104484521",
            masterAccountName    : "Serviceforge 100",
            masterAccountEmail   : "aws-admin-100@serviceforge.net",
            VPCWithDisallowedSubnet    : "vpc-8822eff2"
    ]
    static def awsDev = [
            standardAccountId    : "533807676754",
            standardAccountName  : "Emory Dev 2",
            standardAccountEmail : "aws-dev-2@emory.edu",
            hipaaAccountId       : "753445921657",
            hipaaAccountName     : "AWS Dev 3",
            hipaaAccountEmail    : "aws-dev-3@emory.edu",
            masterAccountId      : "236994348204",
            masterAccountName    : "AWS Dev Master",
            masterAccountEmail   : "aws-dev-master@emory.edu",
            VPCWithDisallowedSubnet    : "vpc-0cba9c802785016c6"
    ]
    static def testDev = [
            standardAccountId    : "104468895143",
            standardAccountName  : "AWS Test 2",
            standardAccountEmail : "aws-test-2@emory.edu",
            hipaaAccountId       : "843017634583",
            hipaaAccountName     : "AWS Test 3",
            hipaaAccountEmail    : "aws-test-3@emory.edu",
            masterAccountId      : "120396672714",
            masterAccountName    : "AWS Test Master",
            masterAccountEmail   : "aws-test-master@emory.edu",
            VPCWithDisallowedSubnet    : "vpc-02e5868e4628679d0"
    ]
    static def stageDev = [
            standardAccountId    : "853936001930",
            standardAccountName  : "Emory Stage 2",
            standardAccountEmail : "aws-stage-2@emory.edu",
            hipaaAccountId       : "649517950242",
            hipaaAccountName     : "Emory Stage 3",
            hipaaAccountEmail    : "aws-stage-3@emory.edu",
            masterAccountId      : "473457766643",
            masterAccountName    : "AWS Stage Master",
            masterAccountEmail   : "aws-stage-master@emory.edu",
            VPCWithDisallowedSubnet    : "vpc-0191f08dd6c763997"
    ]
    static def prodDev = [
            standardAccountId    : "941824850922",
            standardAccountName  : "Emory Account 1",
            standardAccountEmail : "aws-account-1@emory.edu",
            hipaaAccountId       : "802088123585",
            hipaaAccountName     : "Emory Account 3",
            hipaaAccountEmail    : "aws-account-3@emory.edu",
            masterAccountId      : "522074860894",
            masterAccountName    : "AWS Account Master",
            masterAccountEmail   : "aws-account-master@emory.edu",
            VPCWithDisallowedSubnet    : "vpc-0ca60131d6987b52d"
    ]
    static def accountSeries = serviceForge

    static def getAccountId() {
        return accountSeries["standardAccountId"]
    }
    static String getHipaaAccount() {
        return accountSeries["hipaaAccountId"]
    }
    static String getMasterAccount() {
        return accountSeries["masterAccountId"]
    }
    static String getGuardDutyMasterAccount() {
        // DEV/TEST/STAGE - 683810778794 - aws-admin-129@serviceforge.net
        // PROD           - 926373515601
        return "683810778794"
    }
    static def getRegion() {
        return "us-east-1"
    }
    static def getRole() {
        return "rhedcloud/RHEDcloudSecurityRiskDetectionServiceRole"
    }
    static def getExistingDisallowedSubnet() {
        return "subnet-35aa131b"
    }
    static def getVPCWithDisallowedSubnet() {
        return accountSeries["VPCWithDisallowedSubnet"]
    }
    static def getSecurityGroupIdInVpc() {
        return "sg-3e034074"
    }
    static def getImageIdInRegion() {
        return "ami-97785bed"
    }
    static def getDAXServiceRole() {
        return "arn:aws:iam::${getAccountId()}:role/aws-service-role/dax.amazonaws.com/AWSServiceRoleForDAX"
    }
    static List<VpcConfig> getDisallowedVpcConfigs(AmazonEC2 ec2) {
        List<VpcConfig> disallowedVpcConfigs = new ArrayList<>()

        DescribeVpcsResult describeVpcsResult = ec2.describeVpcs()
        if (describeVpcsResult.vpcs.size() != 1) {
            throw new RuntimeException("Strange VPC configuration: " + describeVpcsResult)
        }
        def vpcId = describeVpcsResult.vpcs[0].vpcId

        DescribeSecurityGroupsRequest describeSecurityGroupsRequest = new DescribeSecurityGroupsRequest()
                .withFilters(new Filter("vpc-id").withValues(vpcId))
        DescribeSecurityGroupsResult describeSecurityGroupsResult = ec2.describeSecurityGroups(describeSecurityGroupsRequest)
        SecurityGroup sg = describeSecurityGroupsResult.securityGroups.find({ it.groupName == "default" })
        if (sg == null) {
            throw new RuntimeException("Strange SecurityGroup configuration: " + describeSecurityGroupsResult)
        }

        DescribeSubnetsResult describeSubnetsResult = ec2.describeSubnets()
        for (Subnet subnet : describeSubnetsResult.subnets) {
            for (Tag tag : subnet.tags) {
                if (VpcUtil.DISALLOWED_SUBNETS_TAG_PREDICATE.test(tag)) {
                    VpcConfig vpcConfig = new VpcConfig()
                            .withVpcId(vpcId)
                            .withSubnets(subnet.subnetId)
                            .withSecurityGroupIds(sg.groupId)
                    disallowedVpcConfigs.add(vpcConfig)
                }
            }
        }

        return disallowedVpcConfigs
    }
}
