package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.cloudfront.AmazonCloudFront
import com.amazonaws.services.cloudfront.AmazonCloudFrontClient
import com.amazonaws.services.cloudfront.model.CookiePreference
import com.amazonaws.services.cloudfront.model.CreateDistributionRequest
import com.amazonaws.services.cloudfront.model.CreateDistributionResult
import com.amazonaws.services.cloudfront.model.CustomHeaders
import com.amazonaws.services.cloudfront.model.DefaultCacheBehavior
import com.amazonaws.services.cloudfront.model.DistributionAlreadyExistsException
import com.amazonaws.services.cloudfront.model.DistributionConfig
import com.amazonaws.services.cloudfront.model.ForwardedValues
import com.amazonaws.services.cloudfront.model.GetDistributionConfigRequest
import com.amazonaws.services.cloudfront.model.GetDistributionConfigResult
import com.amazonaws.services.cloudfront.model.Headers
import com.amazonaws.services.cloudfront.model.Origin
import com.amazonaws.services.cloudfront.model.Origins
import com.amazonaws.services.cloudfront.model.S3OriginConfig
import com.amazonaws.services.cloudfront.model.TrustedSigners
import com.amazonaws.services.cloudfront.model.UpdateDistributionRequest
import com.amazonaws.services.cloudfront.model.UpdateDistributionResult
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.Bucket
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.CloudFrontStaticSiteACLDetector
import edu.emory.it.services.srd.remediator.CloudFrontStaticSiteACLRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when permission has been given to the detector to list distributions and create distributions
class CloudFrontStaticSiteACLTest extends Specification {

    static final String WEB_ACL_ID = "876aaf89-5dd0-4bca-8237-6f2db304ed80"  //TODO set up AWS WAF on test environment

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonCloudFront cloudFront = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonCloudFrontClient.class)

        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonS3Client.class)

        CloudFrontStaticSiteACLDetector detector = setupDetector(credentialsProvider)
        CloudFrontStaticSiteACLRemediator remediator = setupRemediator(credentialsProvider)

        String s3Bucket = createS3Bucket(s3)
        String distributionId = createCloudFrontDistribution(cloudFront, s3Bucket)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a CloudFront with no web acl on static site"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.CloudFrontStaticSiteACL.name() == it.getType() &&
                    ("arn:aws:cloudfront::${ITAccount.getAccountId()}:distribution/${distributionId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a CloudFront with non https origin"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)

        then: "remediate should set origin to https only"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isWebAclSet(cloudFront, distributionId) == true


        when: "detecting a CloudFront that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk riskdetected2 = detected2.find({
            DetectionType.CloudFrontStaticSiteACL.name() == it.getType() &&
                    ("arn:aws:cloudfront::${ITAccount.getAccountId()}:distribution/${distributionId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        riskdetected2 == null

        cleanup:
        disableCloudFront(cloudFront, distributionId)
        deleteS3Bucket(s3, s3Bucket)

    }

    private CloudFrontStaticSiteACLDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudFrontStaticSiteACLDetector underTest = new CloudFrontStaticSiteACLDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private CloudFrontStaticSiteACLRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudFrontStaticSiteACLRemediator underTest = new CloudFrontStaticSiteACLRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("webAclId", WEB_ACL_ID)
        }}
        String cloudFrontPropertiesString = "CloudFrontStaticSiteACL".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (cloudFrontPropertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createS3Bucket(AmazonS3 s3) {
        String bucketName = "srd-test-cloud-static-site-${System.currentTimeMillis()}"
        Bucket bucket = s3.createBucket(bucketName)

        return bucketName
    }

    private void deleteS3Bucket(AmazonS3 s3, String bucketName) {
        if (bucketName) {
            s3.deleteBucket(bucketName)
        }
    }

    private String createCloudFrontDistribution(AmazonCloudFront cloudFront, String s3bucketName) {

        DistributionConfig config = new DistributionConfig() {{
            setOrigins(new Origins() {{
                setQuantity(1)
                setItems(new ArrayList<Origin>() {{
                    add(new Origin() {{
                        setId("S3-${s3bucketName}")
                        setDomainName("${s3bucketName}.s3.amazonaws.com")
                        setCustomHeaders(new CustomHeaders() {{
                            setQuantity(0)
                        }})
                        setS3OriginConfig(new S3OriginConfig(){{
                            setOriginAccessIdentity("")
                        }})
                        setOriginPath("")
                    }})
                }})
            }})
            setDefaultCacheBehavior(new DefaultCacheBehavior(){{
                setTrustedSigners(new TrustedSigners(){{
                    setEnabled(true)
                    setQuantity(0)
                }})
                setMinTTL(3600L)
                setDefaultTTL(86400L)
                setMaxTTL(31536000L)
                setViewerProtocolPolicy("allow-all")
                setForwardedValues(new ForwardedValues(){{
                    setHeaders(new Headers() {{
                        setQuantity(0)
                    }})
                    setCookies(new CookiePreference(){{
                        setForward("none")
                    }})
                    setQueryString(true)
                }})
                setTargetOriginId("S3-${s3bucketName}")
            }})
            setCallerReference("cloudFrontStaticSiteACLTest")
            setComment("")
            setEnabled(true)
        }}

        try {
            CreateDistributionRequest request = new CreateDistributionRequest().withDistributionConfig(config)
            CreateDistributionResult result = cloudFront.createDistribution(request)
            return result.getDistribution().getId()

        } catch (DistributionAlreadyExistsException e) {
            String errorMessage = e.getErrorMessage()
            String distributionId =  errorMessage.substring(errorMessage.lastIndexOf(" ") + 1)

            GetDistributionConfigRequest getDistributionConfigRequest = new GetDistributionConfigRequest().withId(distributionId)
            GetDistributionConfigResult getDistributionConfigResult = cloudFront.getDistributionConfig(getDistributionConfigRequest)
            DistributionConfig distributionConfig = getDistributionConfigResult.getDistributionConfig()
            distributionConfig.setEnabled(true)
            distributionConfig.getDefaultCacheBehavior().setTargetOriginId("S3-${s3bucketName}")
            distributionConfig.setOrigins(config.getOrigins())

            UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest().withId(distributionId).withDistributionConfig(distributionConfig).withIfMatch(getDistributionConfigResult.getETag())
            UpdateDistributionResult updateDistributionResult = cloudFront.updateDistribution(updateDistributionRequest)

            return distributionId
        }
    }

    private void disableCloudFront(AmazonCloudFront cloudFront, String distributionId) {
        if (distributionId) {
            GetDistributionConfigResult getDistributionConfigResult = cloudFront.getDistributionConfig(new GetDistributionConfigRequest().withId(distributionId))
            DistributionConfig config = getDistributionConfigResult.getDistributionConfig()
            config.setEnabled(false)
            UpdateDistributionRequest updateDistributionRequest = new UpdateDistributionRequest().withId(distributionId).withDistributionConfig(config).withIfMatch(getDistributionConfigResult.getETag())
            UpdateDistributionResult updateDistributionResult = cloudFront.updateDistribution(updateDistributionRequest)
        }
    }
    
    private boolean isWebAclSet(AmazonCloudFront cloudFront, String distributionId) {

        GetDistributionConfigRequest request = new GetDistributionConfigRequest().withId(distributionId)
        GetDistributionConfigResult result = cloudFront.getDistributionConfig(request)

        DistributionConfig config = result.getDistributionConfig()
        if (config.getWebACLId() == null || config.getWebACLId().isEmpty()) {
            for (Origin origin : config.getOrigins().getItems()) {
                if (origin.getS3OriginConfig() != null) {
                    return false
                }
            }
        }

        return true
    }



}
