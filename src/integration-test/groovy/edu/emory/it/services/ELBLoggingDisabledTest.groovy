package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClient as AmazonElasticLoadBalancingClientV1
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerRequest as CreateLoadBalancerRequestV1
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerResult as CreateLoadBalancerResultV1
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest as DeleteLoadBalancerRequestV1
import com.amazonaws.services.elasticloadbalancing.model.Listener as ListenerV1
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClient
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerRequest
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerResult
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteLoadBalancerRequest
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersRequest
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeLoadBalancersResult
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectsRequest
import com.amazonaws.services.s3.model.ListObjectsV2Result
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.ELBLoggingDisabledDetector
import edu.emory.it.services.srd.remediator.ELBLoggingDisabledRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

import java.util.stream.Collectors

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class ELBLoggingDisabledTest extends Specification {

    static String AVAILABILITY_ZONE = "${ITAccount.getRegion()}a"

    def "integration test for v2 Elastic Load Balancing"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonElasticLoadBalancingClient elb = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonElasticLoadBalancingClient.class)

        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonS3Client.class)

        String configBucketName =  "srd-test-elb-logging-disabled${System.currentTimeMillis()}"

        ELBLoggingDisabledDetector detector = setupDetector(credentialsProvider, configBucketName)
        ELBLoggingDisabledRemediator remediator = setupRemediator(credentialsProvider, configBucketName)


        String loadBalancerArn = createElbOnAwsV2(elb)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with a load balancer that has logging disabled"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.ELBLoggingDisabled.name() == it.getType() &&
                    loadBalancerArn == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null


        when: "remediate an account where s3 bucket is not set up"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return an error"
        RemediationStatus.REMEDIATION_FAILED.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        null != detectedRisk.getRemediationResultNew().getError(0)


        when: "remediate an account where s3 permission is incorrect"
        String bucketName = createBucketOnAws(s3, configBucketName)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return an error"
        RemediationStatus.REMEDIATION_FAILED.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        null != detectedRisk.getRemediationResultNew().getError(0)

        when: "remediate an account where s3 is set up correctly"
        addCredentialsForLogging(s3, bucketName)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return success"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()


        when: "detecting an account where ELB logging is enabled"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk risk2 = detected2.find({
            DetectionType.ELBLoggingDisabled.name() == it.getType() &&
                    loadBalancerArn == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        risk2 == null

        cleanup:
        deleteELBV2(elb, loadBalancerArn)
        deleteBucket(s3, bucketName)

    }


    def "integration test for v1 Elastic Load Balancing"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonElasticLoadBalancingClientV1 elb = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonElasticLoadBalancingClientV1.class)

        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonS3Client.class)

        String configBucketName =  "srd-test-elb-logging-disabled${System.currentTimeMillis()}"

        ELBLoggingDisabledDetector detector = setupDetector(credentialsProvider, configBucketName)
        ELBLoggingDisabledRemediator remediator = setupRemediator(credentialsProvider, configBucketName)

        String loadBalancerName = createElbOnAwsV1(elb)

        when: "detecting an account with a Elastic Load Balancing that has logging disabled"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.ELBLoggingDisabled.name() == it.getType() &&
                    ("arn:aws:elasticloadbalancing:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:loadbalancer/${loadBalancerName}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null


        when: "remediate an account where s3 bucket is not set up"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return an error"
        RemediationStatus.REMEDIATION_FAILED.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        null != detectedRisk.getRemediationResultNew().getError(0)


        when: "remediate an account where s3 permission is incorrect"
        String bucketName = createBucketOnAws(s3, configBucketName)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return an error"
        RemediationStatus.REMEDIATION_FAILED.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        null != detectedRisk.getRemediationResultNew().getError(0)

        when: "remediate an account where s3 is set up correctly"
        addCredentialsForLogging(s3, bucketName)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        then: "remediate should return success"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()


        when: "detecting an account where ELB logging is enabled"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk risk2 = detected2.find({
            DetectionType.ELBLoggingDisabled.name() == it.getType() &&
                    ("arn:aws:elasticloadbalancing:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:loadbalancer/${loadBalancerName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        risk2 == null

        cleanup:
        deleteELBV1(elb, loadBalancerName)
        deleteBucket(s3, bucketName)

    }

    private ELBLoggingDisabledDetector setupDetector(AWSCredentialsProvider credentialsProvider, String configBucketName) {

        AppConfig appConfig = setupAppConfig(configBucketName)

        ELBLoggingDisabledDetector underTest = new ELBLoggingDisabledDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private ELBLoggingDisabledRemediator setupRemediator(AWSCredentialsProvider credentialsProvider, String configBucketName) {

        AppConfig appConfig = setupAppConfig(configBucketName)

        ELBLoggingDisabledRemediator underTest = new ELBLoggingDisabledRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private AppConfig setupAppConfig(String configBucketName) {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("S3LoggingBucketPath-${ITAccount.getRegion()}", configBucketName)
        }}
        String elbLoggingString = "ELBLoggingDisabled".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (elbLoggingString) : propertyConfig
        ])

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private void addCredentialsForLogging(AmazonS3 s3, String bucketName) {

        String policy =
        """{
            "Version": "2012-10-17",
            "Id": "Policy1523476567042",
            "Statement": [
                {
                    "Sid": "Stmt1523476562591",
                    "Effect": "Allow",
                    "Principal": {
                        "AWS": "arn:aws:iam::127311923021:root"
                    },
                    "Action": "s3:PutObject",
                    "Resource": "arn:aws:s3:::${bucketName}/*"
                }
            ]
        }"""  //127311923021 is the account id of the us-east-1 Elastic Load Balancing as described in https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html#attach-bucket-policy

        s3.setBucketPolicy(bucketName, policy)
    }

    private String createBucketOnAws(AmazonS3 s3, String configBucketName) {
        String bucketName = configBucketName
        s3.createBucket(bucketName)
        return bucketName
    }

    private String createElbOnAwsV2(AmazonElasticLoadBalancingClient elb) {
        String loadBalancerName = "srd-test-elb-logging"

        CreateLoadBalancerRequest request = new CreateLoadBalancerRequest()
            .withName("srd-test-elb-logging")
            .withSubnets("subnet-b8767cdc", "subnet-bf6a4090")

        CreateLoadBalancerResult result = elb.createLoadBalancer(request)
        DescribeLoadBalancersRequest describeLoadBalancersRequest = new DescribeLoadBalancersRequest().withNames(loadBalancerName)
        DescribeLoadBalancersResult describeLoadBalancersResult = elb.describeLoadBalancers(describeLoadBalancersRequest)
        return describeLoadBalancersResult.getLoadBalancers().get(0).getLoadBalancerArn()
    }

    private String createElbOnAwsV1(AmazonElasticLoadBalancingClientV1 elb) {
        String balancerName = "srd-test-elb-logging-v1"
        CreateLoadBalancerRequestV1 createLoadBalancerRequest = new CreateLoadBalancerRequestV1()
                .withLoadBalancerName(balancerName)
                .withListeners(new ListenerV1(){{
                    setProtocol("HTTP")
            setLoadBalancerPort(80)
            setInstancePort(80)
            setInstanceProtocol("HTTP")
        }})
                .withAvailabilityZones(AVAILABILITY_ZONE)

        CreateLoadBalancerResultV1 result =  elb.createLoadBalancer(createLoadBalancerRequest)
        return balancerName
    }

    private void deleteELBV2(AmazonElasticLoadBalancingClient elb, String loadBalancerArn) {
        if (loadBalancerArn) {
            DeleteLoadBalancerRequest request = new DeleteLoadBalancerRequest().withLoadBalancerArn(loadBalancerArn)
            elb.deleteLoadBalancer(request)
        }
    }

    private void deleteELBV1(AmazonElasticLoadBalancingClientV1 elb, String loadBalancerName) {
        if (loadBalancerName) {
            DeleteLoadBalancerRequestV1 request = new DeleteLoadBalancerRequestV1().withLoadBalancerName(loadBalancerName)
            elb.deleteLoadBalancer(request)
        }
    }

    private void deleteBucket(AmazonS3 s3, String bucketName) {
        if (bucketName != null) {
            ListObjectsV2Result result = s3.listObjectsV2(bucketName)  //not worrying about pagination, there is probably only 1 file in the bucket created by the elb
            DeleteObjectsRequest deleteObjectsRequest = new DeleteObjectsRequest().withBucketName(bucketName).withKeys(result.getObjectSummaries().stream().map({new DeleteObjectsRequest.KeyVersion(it.getKey())}).collect(Collectors.toList()))
            s3.deleteObjects(deleteObjectsRequest)
            s3.deleteBucket(bucketName)
        }
    }
}
