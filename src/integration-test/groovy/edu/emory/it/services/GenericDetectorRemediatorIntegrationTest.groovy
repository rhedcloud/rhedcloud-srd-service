package edu.emory.it.services

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.DetectionResult
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazon.aws.moa.objects.resources.v1_0.SecurityRiskDetectionRequisition
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.organizations.AWSOrganizations
import com.amazonaws.services.organizations.AWSOrganizationsClientBuilder
import edu.emory.it.services.srd.util.AWSOrgUtil
import edu.emory.it.services.srd.util.SecurityRiskAwsRegionsAndClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.openeai.config.AppConfig
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import org.openeai.utils.lock.UuidKey
import spock.lang.Specification

/**
 * A simple integration test that allows a SRD/SRR pair to be run.
 * Mostly used during development and run from the IDE.
 */
class GenericDetectorRemediatorIntegrationTest extends Specification {
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger(GenericDetectorRemediatorIntegrationTest.class)

//    static def accountId = "533807676754"  // aws-dev-2    Standard
//    static def accountId = "753445921657"  // aws-dev-3    HIPAA
//    static def accountId = "731363757793"  // aws-dev-300  Standard
    static def accountId = "250049456440"  // aws-dev-476  Standard

//    static def accountId = "783744221069"  // AWS Dev CIMP 1  Standard
//    static def accountId = "536485806252"  // AWS Dev CIMP 2  Enhanced Security
//    static def accountId = "927811698555"  // AWS Dev CIMP 3  HIPAA


//    static def accountId = "843017634583"  // aws-test-3   HIPAA
//    static def accountId = "675300927411"  // aws-test-50  Standard
//    static def accountId = "465681791096"  // aws-test-103  Standard
//    static def accountId = "094256604584"  // aws-test-110  HIPAA

//    static def accountId = "391697446721"  // aws-stage-1  Standard
//    static def accountId = "649517950242"  // aws-stage-3  HIPAA

//    static def accountId = "967177476534"  // aws-account-2  Standard
//    static def accountId = "802088123585"  // aws-account-3  HIPAA

    Lock lock = Mock()
    SecurityRiskDetectionRequisition requisition = Mock()
    SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock, requisition)
    AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

    def "t1"() {
        setup:
        LogManager.getRootLogger().setLevel(Level.INFO)  // DEBUG is very noisy

        // AppConfig with properties
        PropertyConfig VPCFlowLogsDisabledPropertyConfig = Mock()
        VPCFlowLogsDisabledPropertyConfig.getProperties() >> new Properties() {{
            setProperty("flowLogRole", "RHEDcloudVpcFlowLogRole")
        }}
        PropertyConfig IamExternalTrustRelationshipPolicyPropertyConfig = Mock()
        IamExternalTrustRelationshipPolicyPropertyConfig.getProperties() >> new Properties() {{
            setProperty("ignoreArns", "arn:aws:iam::*:role/rhedcloud/*,arn:aws:iam::*:role/customroles/*")
            //setProperty("ignoreArns", "arn:aws:iam::*:role/rhedcloud/RHEDcloudAdministratorRole,arn:aws:iam::*:role/rhedcloud/RHEDcloudAuditorRole")
        }}
        PropertyConfig IamRestrictionPoliciesDetachedPropertyConfig = Mock()
        IamRestrictionPoliciesDetachedPropertyConfig.getProperties() >> new Properties() {{
            setProperty("ignoreArns", "arn:aws:iam::*:role/aws-service-role/*")
        }}
        PropertyConfig GuardDutyMisconfigurationPropertyConfig = Mock()
        GuardDutyMisconfigurationPropertyConfig.getProperties() >> new Properties() {{
            setProperty("GuardDutyMasterAccountID", "926373515601") // aws-security-1 in production
            setProperty("GuardDutySplunkCfnTemplate", "https://s3.amazonaws.com/emory-aws-dev-cfn-templates/rhedcloud-aws-guardduty-splunk-cfn.compact.json")
            setProperty("HECToken", "hec-token")
            setProperty("HECEndpoint", "https://xxx.splunkcloud.com")
        }}

        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock([
            ("VPCFlowLogsDisabled".toLowerCase()) : VPCFlowLogsDisabledPropertyConfig,
            ("IamExternalTrustRelationshipPolicy".toLowerCase()) : IamExternalTrustRelationshipPolicyPropertyConfig,
            ("IamRestrictionPoliciesDetached".toLowerCase()) : IamRestrictionPoliciesDetachedPropertyConfig,
            ("GuardDutyMisconfiguration".toLowerCase()) : GuardDutyMisconfigurationPropertyConfig
        ])

        // let all lock acquisitions succeed
        lock.set(_) >> new UuidKey()

        // some SRD/SRR need AWSOrgUtil so initialize it here
        AWSOrganizations orgClient = AWSOrganizationsClientBuilder.standard()
                .withCredentials(credentialsProvider)
                .withRegion(SecurityRiskAwsRegionsAndClientBuilder.DEFAULT_REGION)
                .build()
        AWSOrgUtil.getInstance().initialize(orgClient, "EmoryHipaaAccountOrg", "EmoryStandardAccountOrg", null,
                "EmoryAccountQuarantineOrg", "EmoryAccountPendingDeleteOrg", "EmoryAccountAdministrationOrg")

        // SRD is required and SRR is optional
        def detector = new edu.emory.it.services.srd.detector.ExampleDetector()
        def remediator = new edu.emory.it.services.srd.remediator.ExampleRemediator()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        if (remediator) {
            remediator.init(appConfig, credentialsProvider, ITAccount.getRole())
        }

        SecurityRiskDetection securityRiskDetection = new SecurityRiskDetection() {
            @Override DetectionResult newDetectionResult() { return new DetectionResult() }
            @Override DetectedSecurityRisk newDetectedSecurityRisk() {
                return new DetectedSecurityRisk() {
                    @Override RemediationResult newRemediationResult() { return new RemediationResult() }
                }
            }
        }

        when:
        long srdTook = System.currentTimeMillis()
        long srrTook = 0

        detector.detect(securityRiskDetection, accountId, srContext)
        srdTook = (System.currentTimeMillis() - srdTook)
        // log and filter risks to determine which will be remediated, if any
        // for example, to filter by region: ((DetectedSecurityRisk) it).amazonResourceName.startsWith("arn:aws:ec2:ca-central-1:${accountId}")
        def filteredRisks = securityRiskDetection.detectedSecurityRisk.findAll({
            logger.info("risk detected for ARN " + ((DetectedSecurityRisk) it).amazonResourceName)
            true  // all risks
        })

        if (remediator) {
            srrTook = System.currentTimeMillis()
            filteredRisks.each {
                logger.info("calling remediate for ARN " + ((DetectedSecurityRisk) it).amazonResourceName)
                remediator.remediate(accountId, (DetectedSecurityRisk) it, srContext)
            }
            srrTook = (System.currentTimeMillis() - srrTook)
        }

        then:
        logger.info("Took ${srdTook + srrTook} ms with SRD at " + srdTook + " ms and SRR at " + srrTook + " ms")
        srContext.metricCollector.logMetricDatum(accountId, detector.class.simpleName)
    }
}
