package edu.emory.it.services

import com.amazon.aws.moa.jmsobjects.security.v1_0.SecurityRiskDetection
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.sagemaker.AmazonSageMaker
import com.amazonaws.services.sagemaker.AmazonSageMakerClient
import com.amazonaws.services.sagemaker.model.CreateNotebookInstanceRequest
import com.amazonaws.services.sagemaker.model.InstanceType
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.UTSecurityRiskDetection
import edu.emory.it.services.srd.detector.SageMakerVPCMisconfigurationDetector
import edu.emory.it.services.srd.remediator.SageMakerVPCMisconfigurationRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class SageMakerVPCMisconfigurationTest extends Specification {

    public static final String NOTEBOOK_INSTANCE_NAME = "integration-test-sagemaker-notebook"

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        SageMakerVPCMisconfigurationDetector detector = new SageMakerVPCMisconfigurationDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        SageMakerVPCMisconfigurationRemediator remediator = new SageMakerVPCMisconfigurationRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)
        SecurityRiskDetection securityRiskDetection = UTSecurityRiskDetection.create()

        when:
        detector.detect(securityRiskDetection, ITAccount.getAccountId(), srContext)

        then: "risks should be from SageMaker"
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            risk.getType() == DetectionType.SageMakerVPCMisconfiguration.name()
            risk.getAmazonResourceName().startsWith("arn:aws:sagemaker:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:")
        }

        when:
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            remediator.remediate(ITAccount.getAccountId(), risk, srContext)
        }

        then: "the risks should be remediated"
        securityRiskDetection.detectedSecurityRisk.each { risk ->
            risk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        }
    }

    def setupSpec() {
        AmazonSageMaker sageMakerClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonSageMakerClient.class)

        CreateNotebookInstanceRequest createNotebookInstanceRequest = new CreateNotebookInstanceRequest()
                .withNotebookInstanceName(NOTEBOOK_INSTANCE_NAME)
                .withInstanceType(InstanceType.MlT2Medium)
                .withRoleArn("arn:aws:iam::${ITAccount.getAccountId()}:role/service-role/AmazonSageMaker-ExecutionRole-20180920T082331")
        sageMakerClient.createNotebookInstance(createNotebookInstanceRequest)
    }
}
