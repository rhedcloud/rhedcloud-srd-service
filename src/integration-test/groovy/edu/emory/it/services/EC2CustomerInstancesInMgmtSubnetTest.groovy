package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.CreateKeyPairRequest
import com.amazonaws.services.ec2.model.CreateKeyPairResult
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult
import com.amazonaws.services.ec2.model.DescribeInstancesRequest
import com.amazonaws.services.ec2.model.DescribeInstancesResult
import com.amazonaws.services.ec2.model.Instance
import com.amazonaws.services.ec2.model.Reservation
import com.amazonaws.services.ec2.model.RunInstancesRequest
import com.amazonaws.services.ec2.model.RunInstancesResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.EC2CustomerInstancesInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.EC2CustomerInstancesInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class EC2CustomerInstancesInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        EC2CustomerInstancesInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        EC2CustomerInstancesInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        String instanceId = createInstancesOnAws(ec2)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with an EC2 instance in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.EC2CustomerInstancesInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:instance/${instanceId}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null


        when: "remediate an account with an EC2 instance in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        sleep(2000)
        then: "remediate should terminate the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        isInstanceTerminating(ec2, instanceId) == true

        when: "detecting an account with stopped instances that have unencrypted instances"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected2 = detecteds2.find({
            DetectionType.EC2CustomerInstancesInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:instance/${instanceId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected2 == null


    }


    private EC2CustomerInstancesInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2CustomerInstancesInMgmtSubnetDetector underTest = new EC2CustomerInstancesInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private EC2CustomerInstancesInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2CustomerInstancesInMgmtSubnetRemediator underTest = new EC2CustomerInstancesInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createInstancesOnAws(AmazonEC2 ec2) {

        CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest()
                .withGroupName("srd-test-ebs-unencrypted-secondary-volume")
                .withDescription("Security group for testing")

        try {
            CreateSecurityGroupResult csgrResult = ec2.createSecurityGroup(csgr)
        } catch (AmazonEC2Exception e) {
            if (e.getErrorCode().equals("InvalidGroup.Duplicate")) {
                // if already exist, don't worry about it
            } else {
                throw e
            }
        }



        CreateKeyPairRequest createKeyPairRequest = new CreateKeyPairRequest()
        createKeyPairRequest.withKeyName("srd-test-ebs-unencrypted-secondary-volume")

        try {
            CreateKeyPairResult createKeyPairResult = ec2.createKeyPair(createKeyPairRequest)
        } catch (AmazonEC2Exception e) {
            if (e.getErrorCode().equals("InvalidKeyPair.Duplicate")) {
                // if already exist, don't worry about it
            } else {
                throw e
            }
        }

        RunInstancesRequest request = new RunInstancesRequest()
                .withImageId(ITAccount.getImageIdInRegion())
                .withInstanceType("t2.micro")
                .withMinCount(1)
                .withMaxCount(1)
                .withKeyName("srd-test-ebs-unencrypted-secondary-volume")
                .withSubnetId(ITAccount.getExistingDisallowedSubnet())

        RunInstancesResult runInstancesResult = ec2.runInstances(request)
        Reservation reservation = runInstancesResult.getReservation()

        List<Instance> instances = reservation.getInstances()
        String instanceId = instances.get(0).getInstanceId()



        String state = instances.get(0).getState().getName()
        String availabilityZone = instances.get(0).getPlacement().getAvailabilityZone()
        while (state != null && (state.equals("pending") || availabilityZone == null)) {  //wait until it finishes starting
            sleep(5000)
            DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest().withInstanceIds(instanceId)
            DescribeInstancesResult instancesResult = ec2.describeInstances(describeInstanceRequest)
            state = instancesResult?.getReservations()?.get(0)?.getInstances()?.get(0).getState()?.getName()
            availabilityZone = instancesResult?.getReservations()?.get(0)?.getInstances()?.get(0).getPlacement()?.getAvailabilityZone()
        }

        return instanceId
    }

    private boolean isInstanceTerminating(AmazonEC2 ec2, String instanceId) {
        DescribeInstancesRequest describeInstanceRequest = new DescribeInstancesRequest().withInstanceIds(instanceId)
        DescribeInstancesResult instancesResult = ec2.describeInstances(describeInstanceRequest)
        String state = instancesResult?.getReservations()?.get(0)?.getInstances()?.get(0).getState()?.getName()
        return state ==  "shutting-down" || state == "terminated"
    }
}
