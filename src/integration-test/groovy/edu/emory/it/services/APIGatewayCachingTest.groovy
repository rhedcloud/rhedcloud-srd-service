package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.apigateway.AmazonApiGatewayClient
import com.amazonaws.services.apigateway.model.CreateDeploymentRequest
import com.amazonaws.services.apigateway.model.CreateRestApiRequest
import com.amazonaws.services.apigateway.model.CreateRestApiResult
import com.amazonaws.services.apigateway.model.DeleteRestApiRequest
import com.amazonaws.services.apigateway.model.GetResourcesRequest
import com.amazonaws.services.apigateway.model.GetResourcesResult
import com.amazonaws.services.apigateway.model.GetStageRequest
import com.amazonaws.services.apigateway.model.GetStageResult
import com.amazonaws.services.apigateway.model.PutIntegrationRequest
import com.amazonaws.services.apigateway.model.PutIntegrationResponseRequest
import com.amazonaws.services.apigateway.model.PutMethodRequest
import com.amazonaws.services.apigateway.model.PutMethodResponseRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.APIGatewayCachingDetector
import edu.emory.it.services.srd.remediator.APIGatewayCachingRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class APIGatewayCachingTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonApiGatewayClient gateway = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonApiGatewayClient.class)

        APIGatewayCachingDetector detector = setupDetector(credentialsProvider)
        APIGatewayCachingRemediator remediator = setupRemediator(credentialsProvider)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        String apiId = createApi(gateway)
        String stageName = createDeploymentAndStage(gateway, apiId)

        when: "detecting a distribution where cache is not enabled"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.ApiGatewayCaching.name() == it.getType() &&
                    ("arn:aws:apigateway:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:${apiId}/${stageName}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a distribution where cache is not enabled"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        sleep(7000)
        then: "remediate should enable cache on the stage"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isStageCached(gateway, apiId, stageName) == true


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.ApiGatewayCaching.name() == it.getType() &&
                    ("arn:aws:apigateway:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:${apiId}/${stageName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteApiGateway(gateway, apiId)
    }


    private APIGatewayCachingDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        APIGatewayCachingDetector underTest = new APIGatewayCachingDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private APIGatewayCachingRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        APIGatewayCachingRemediator underTest = new APIGatewayCachingRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("defaultCacheSizeGB", "0.5")
        }}
        String apiGatewayPropertiesString = "APIGatewayCaching".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (apiGatewayPropertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createApi(AmazonApiGatewayClient gateway) {


        CreateRestApiRequest request = new CreateRestApiRequest().withName("test")
        CreateRestApiResult result = gateway.createRestApi(request)
        String apiId = result.getId()

        GetResourcesRequest getResourcesRequest = new GetResourcesRequest().withRestApiId(apiId)
        GetResourcesResult getResourcesResult = gateway.getResources(getResourcesRequest)
        String resourceId = getResourcesResult.getItems().get(0).getId()

        PutMethodRequest putMethodRequest = new PutMethodRequest().withRestApiId(apiId).withResourceId(resourceId).withHttpMethod("GET").withAuthorizationType("NONE")
        gateway.putMethod(putMethodRequest)

        PutMethodResponseRequest putMethodResponseRequest = new PutMethodResponseRequest().withRestApiId(apiId).withResourceId(resourceId).withHttpMethod("GET").withStatusCode("200")
        gateway.putMethodResponse(putMethodResponseRequest)

        PutIntegrationRequest putIntegrationRequest = new PutIntegrationRequest().withRestApiId(apiId).withResourceId(resourceId).withHttpMethod("GET").withType("HTTP").withIntegrationHttpMethod("GET").withUri("http://example.com/")
        gateway.putIntegration(putIntegrationRequest)

        PutIntegrationResponseRequest putIntegrationResponseRequest = new PutIntegrationResponseRequest().withRestApiId(apiId).withResourceId(resourceId).withHttpMethod("GET").withStatusCode("200")
        gateway.putIntegrationResponse(putIntegrationResponseRequest)

        return apiId
    }

    private String createDeploymentAndStage(AmazonApiGatewayClient gateway, String restApi) {
        String stageName = "test"
        CreateDeploymentRequest request = new CreateDeploymentRequest().withRestApiId(restApi).withStageName(stageName)

        gateway.createDeployment(request)

        return stageName
    }

    private boolean isStageCached(AmazonApiGatewayClient gateway, String apiId, String stageName) {
        GetStageRequest request = new GetStageRequest().withRestApiId(apiId).withStageName(stageName)
        GetStageResult result = gateway.getStage(request)
        return result.getCacheClusterEnabled()
    }

    private void deleteApiGateway(AmazonApiGatewayClient gateway, String apiId) {
        if (apiId != null) {
            DeleteRestApiRequest request = new DeleteRestApiRequest().withRestApiId(apiId)
            try {
                gateway.deleteRestApi(request)
            } catch (Exception e) {
                System.out.println(e.getMessage())
            }
        }
    }
}

