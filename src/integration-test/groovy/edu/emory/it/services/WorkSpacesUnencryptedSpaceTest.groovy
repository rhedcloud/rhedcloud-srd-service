package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.regions.Region
import com.amazonaws.services.workspaces.AmazonWorkspaces
import com.amazonaws.services.workspaces.AmazonWorkspacesClient
import com.amazonaws.services.workspaces.model.CreateWorkspacesRequest
import com.amazonaws.services.workspaces.model.CreateWorkspacesResult
import com.amazonaws.services.workspaces.model.DescribeWorkspacesRequest
import com.amazonaws.services.workspaces.model.DescribeWorkspacesResult
import com.amazonaws.services.workspaces.model.WorkspaceRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.WorkSpacesUnencryptedSpaceDetector
import edu.emory.it.services.srd.remediator.WorkSpacesUnencryptedSpaceRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when directory id and directory user is setup on test account + security settings allow for integratin test iam user to create workspace
class WorkSpacesUnencryptedSpaceTest extends Specification {

    static final String DIRECTORY_ID = "d-90672c0ece"  //directory must be created beforehand and id put here https://docs.aws.amazon.com/workspaces/latest/adminguide/register-deregister-directory.html

    static final String DIRECTORY_USER = "testing" //user must be added beforehand https://aws.amazon.com/blogs/security/how-to-manage-identities-in-simple-ad-directories/



    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonWorkspaces workspaces = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonWorkspacesClient.class)

        WorkSpacesUnencryptedSpaceDetector detector = setupDetector(credentialsProvider)
        WorkSpacesUnencryptedSpaceRemediator remediator = setupRemediator(credentialsProvider)

        String workspaceId = createWorkspace(workspaces)
        waitTillStatusChangesFromPending(workspaces, workspaceId)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a workspace that is not encrypted"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.WorkSpacesUnencryptedSpace.name() == it.getType() &&
                    ("arn:aws:workspaces:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:workspace/${workspaceId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a workspace that is not encrypted"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        waitTillStatusChangesToTerminating(workspaces, workspaceId)

        then: "remediate should terminate the workspace"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isWorkspaceTerminated(workspaces, workspaceId) == true


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.WorkSpacesUnencryptedSpace.name() == it.getType() &&
                    ("arn:aws:workspaces:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:workspace/${workspaceId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null


    }

    private WorkSpacesUnencryptedSpaceDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        WorkSpacesUnencryptedSpaceDetector underTest = new WorkSpacesUnencryptedSpaceDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())
        underTest.setOverrideRegions([new Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private WorkSpacesUnencryptedSpaceRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        WorkSpacesUnencryptedSpaceRemediator underTest = new WorkSpacesUnencryptedSpaceRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())
        underTest.setOverrideRegions([new Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createWorkspace(AmazonWorkspaces workspaces) {
        String bundleId = "wsb-bh8rsxt14"  //this is the value bundle from amazon, id can be found using  workspaces.desribeWorkspaceBundles(DescribeWorkspaceBundlesRequest().withOwner("Amazon"))

        DescribeWorkspacesRequest describeWorkspacesRequest = new DescribeWorkspacesRequest().withUserName(DIRECTORY_USER).withDirectoryId(DIRECTORY_ID)
        DescribeWorkspacesResult describeWorkspacesResult = workspaces.describeWorkspaces(describeWorkspacesRequest)
        if (!describeWorkspacesResult.getWorkspaces().isEmpty()) {
            //sometimes this is wrong, a terminated instance sometimes does not show up in here
            return describeWorkspacesResult.getWorkspaces().get(0).getWorkspaceId()
        }

        CreateWorkspacesRequest createWorkspacesRequest = new CreateWorkspacesRequest().withWorkspaces([
                new WorkspaceRequest() {{
                    setBundleId(bundleId)
                    setDirectoryId(DIRECTORY_ID)
                    setUserName(DIRECTORY_USER)
                }}
        ])
        CreateWorkspacesResult result = workspaces.createWorkspaces(createWorkspacesRequest)
        return result.getPendingRequests().get(0).getWorkspaceId()
    }

    private boolean isWorkspaceTerminated(AmazonWorkspaces workspaces, String workspaceId) {
        DescribeWorkspacesRequest workspacesRequest = new DescribeWorkspacesRequest().withWorkspaceIds(workspaceId)
        DescribeWorkspacesResult workspacesResult = workspaces.describeWorkspaces(workspacesRequest)

        if (workspacesResult.getWorkspaces().isEmpty()) {
            return true
        }

        String state = workspacesResult.getWorkspaces().get(0).getState()

        return state.equals("TERMINATING") || state.equals("TERMINATED")
    }

    private boolean isWorkspacePending(AmazonWorkspaces workspaces, String workspaceId) {
        DescribeWorkspacesRequest workspacesRequest = new DescribeWorkspacesRequest().withWorkspaceIds(workspaceId)
        DescribeWorkspacesResult workspacesResult = workspaces.describeWorkspaces(workspacesRequest)

        if (workspacesResult.getWorkspaces().isEmpty()) {
            return true
        }

        String state = workspacesResult.getWorkspaces().get(0).getState()

        return state.equals("PENDING")
    }

    private void waitTillStatusChangesToTerminating(AmazonWorkspaces workspaces, String workspaceId) {
        boolean done = false
        for (int i=0; i<10 && !done; i++) {
            if (i != 0) {
                sleep(30000)
            }
            done = isWorkspaceTerminated(workspaces, workspaceId)
        }
    }

    private void waitTillStatusChangesFromPending(AmazonWorkspaces workspaces, String workspaceId) {
        boolean done = false
        for (int i=0; i<30 && !done; i++) {
            if (i != 0) {
                sleep(60000)
            }
            done = !isWorkspacePending(workspaces, workspaceId)
        }
    }
}

