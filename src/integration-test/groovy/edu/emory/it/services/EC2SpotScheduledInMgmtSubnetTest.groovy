package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.CreateLaunchTemplateRequest
import com.amazonaws.services.ec2.model.CreateLaunchTemplateResult
import com.amazonaws.services.ec2.model.DescribeLaunchTemplatesRequest
import com.amazonaws.services.ec2.model.DescribeLaunchTemplatesResult
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsRequest
import com.amazonaws.services.ec2.model.DescribeSpotFleetRequestsResult
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsRequest
import com.amazonaws.services.ec2.model.DescribeSpotInstanceRequestsResult
import com.amazonaws.services.ec2.model.FleetLaunchTemplateSpecification
import com.amazonaws.services.ec2.model.InstanceType
import com.amazonaws.services.ec2.model.LaunchSpecification
import com.amazonaws.services.ec2.model.LaunchTemplateConfig
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecificationRequest
import com.amazonaws.services.ec2.model.RequestLaunchTemplateData
import com.amazonaws.services.ec2.model.RequestSpotFleetRequest
import com.amazonaws.services.ec2.model.RequestSpotFleetResult
import com.amazonaws.services.ec2.model.RequestSpotInstancesRequest
import com.amazonaws.services.ec2.model.RequestSpotInstancesResult
import com.amazonaws.services.ec2.model.SpotFleetLaunchSpecification
import com.amazonaws.services.ec2.model.SpotFleetRequestConfig
import com.amazonaws.services.ec2.model.SpotFleetRequestConfigData
import com.amazonaws.services.ec2.model.SpotInstanceRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.EC2SpotScheduledInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.EC2SpotScheduledInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class EC2SpotScheduledInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        EC2SpotScheduledInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        EC2SpotScheduledInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        String spotInstanceRequestId = createSpotInstanceRequest(ec2)
        String spotBlockRequestId = createSpotBlockRequest(ec2)
        String spotFleetRequestId = createSpotFleetRequest(ec2)
        String spotFleetRequestId2 = createSpotFleetRequestWithLaunchTemplate(ec2)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "detecting an account with spot requests in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.EC2SpotScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotInstanceRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected1 = detecteds.find({
            DetectionType.EC2SpotScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotBlockRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected2 = detecteds.find({
            DetectionType.EC2SpotFleetScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotFleetRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected2a = detecteds.find({
            DetectionType.EC2SpotFleetScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotFleetRequestId2}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null
        detected1 != null
        detected2 != null
        detected2a != null


        when: "remediate an account with spot requests in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        DetectedSecurityRiskSubclass detectedRisk1 =  new DetectedSecurityRiskSubclass(detected1)
        DetectedSecurityRiskSubclass detectedRisk2 =  new DetectedSecurityRiskSubclass(detected2)
        DetectedSecurityRiskSubclass detectedRisk2a =  new DetectedSecurityRiskSubclass(detected2a)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk1, srContext)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk2, srContext)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk2a, srContext)
        sleep(60000)

        then: "remediate should terminate the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()
        spotInstanceRequestIsCancelling(ec2, spotInstanceRequestId) == true
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk1.getRemediationResultNew().getStatus()
        null != detectedRisk1.getRemediationResultNew().getDescription()
        spotInstanceRequestIsCancelling(ec2, spotBlockRequestId) == true
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk2.getRemediationResultNew().getStatus()
        null != detectedRisk2.getRemediationResultNew().getDescription()
        spotFleetRequestIsCancelling(ec2, spotFleetRequestId) == true
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk2a.getRemediationResultNew().getStatus()
        null != detectedRisk2a.getRemediationResultNew().getDescription()
        spotFleetRequestIsCancelling(ec2, spotFleetRequestId2) == true

        when: "detecting an account with stopped instances that have unencrypted instances"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected3 = detecteds2.find({
            DetectionType.EC2SpotScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotInstanceRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected4 = detecteds2.find({
            DetectionType.EC2SpotScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotBlockRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected5 = detecteds2.find({
            DetectionType.EC2SpotFleetScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotFleetRequestId}") == it.getAmazonResourceName()
        })
        DetectedSecurityRisk detected6 = detecteds2.find({
            DetectionType.EC2SpotFleetScheduledInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:spot-request/${spotFleetRequestId2}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected3 == null
        detected4 == null
        detected5 == null
        detected6 == null

    }


    private EC2SpotScheduledInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2SpotScheduledInMgmtSubnetDetector underTest = new EC2SpotScheduledInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private EC2SpotScheduledInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2SpotScheduledInMgmtSubnetRemediator underTest = new EC2SpotScheduledInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createSpotInstanceRequest(AmazonEC2 ec2) {

        def startDate = new Date() + 7
        def endDate = new Date() + 14

        // Initializes a Spot Instance Request
        RequestSpotInstancesRequest requestRequest = new RequestSpotInstancesRequest();

        // Request 1 x t1.micro instance with a bid price of $0.03.
        requestRequest.setSpotPrice("0.03");
        requestRequest.setInstanceCount(Integer.valueOf(1));

        requestRequest.setValidFrom(startDate)
        requestRequest.setValidUntil(endDate)

        LaunchSpecification launchSpecification = new LaunchSpecification();
        launchSpecification.setImageId(ITAccount.getImageIdInRegion());
        launchSpecification.setInstanceType(InstanceType.T2Micro);

        launchSpecification.setSubnetId(ITAccount.getExistingDisallowedSubnet())

        // Add the launch specification.
        requestRequest.setLaunchSpecification(launchSpecification);

        // Call the RequestSpotInstance API.
        RequestSpotInstancesResult requestResult =
                ec2.requestSpotInstances(requestRequest);

        return requestResult.getSpotInstanceRequests().get(0).getSpotInstanceRequestId()
    }

    private String createSpotBlockRequest(AmazonEC2 ec2) {

        def startDate = new Date() + 7
        def endDate = new Date() + 14

        // Initializes a Spot Instance Request
        RequestSpotInstancesRequest requestRequest = new RequestSpotInstancesRequest();

        // Request 1 x t1.micro instance with a bid price of $0.03.
        requestRequest.setSpotPrice("0.03");
        requestRequest.setInstanceCount(Integer.valueOf(1));
        requestRequest.setBlockDurationMinutes(360)

        requestRequest.setValidFrom(startDate)
        requestRequest.setValidUntil(endDate)

        LaunchSpecification launchSpecification = new LaunchSpecification();
        launchSpecification.setImageId(ITAccount.getImageIdInRegion())
        launchSpecification.setInstanceType(InstanceType.T2Micro);

        launchSpecification.setSubnetId(ITAccount.getExistingDisallowedSubnet());

        // Add the launch specification.
        requestRequest.setLaunchSpecification(launchSpecification);

        // Call the RequestSpotInstance API.
        RequestSpotInstancesResult requestResult =
                ec2.requestSpotInstances(requestRequest);

        return requestResult.getSpotInstanceRequests().get(0).getSpotInstanceRequestId()
    }

    private String createSpotFleetRequest(AmazonEC2 ec2) {

        def startDate = new Date() + 7
        def endDate = new Date() + 14

        // Initializes a Spot Instance Request
        SpotFleetRequestConfigData config = new SpotFleetRequestConfigData();
        RequestSpotFleetRequest fleetRequestRequest = new RequestSpotFleetRequest();
        fleetRequestRequest.setSpotFleetRequestConfig(config)

        // Request 1 x t1.micro instance with a bid price of $0.03.
        config.setSpotPrice("0.03");

        config.setValidFrom(startDate)
        config.setValidUntil(endDate)
        config.setTerminateInstancesWithExpiration(true)
        config.setIamFleetRole("arn:aws:iam::${ITAccount.getAccountId()}:role/aws-service-role/spotfleet.amazonaws.com/AWSServiceRoleForEC2SpotFleet")

        SpotFleetLaunchSpecification launchSpecification = new SpotFleetLaunchSpecification();
        launchSpecification.setImageId(ITAccount.getImageIdInRegion());
        launchSpecification.setInstanceType(InstanceType.T2Micro);

        launchSpecification.setSubnetId(ITAccount.getExistingDisallowedSubnet())

        // Add the launch specification.
        config.setLaunchSpecifications([launchSpecification]);

        // Call the RequestSpotInstance API.
        RequestSpotFleetResult requestResult =
                ec2.requestSpotFleet(fleetRequestRequest);

        return requestResult.getSpotFleetRequestId()
    }

    private String createSpotFleetRequestWithLaunchTemplate(AmazonEC2 ec2) {

        def startDate = new Date() + 7
        def endDate = new Date() + 14

        String templateName = "srd-test-spot-fleet-request-in-disallowed-subnet"
        CreateLaunchTemplateRequest createLaunchTemplateRequest = new CreateLaunchTemplateRequest()
                .withLaunchTemplateName(templateName)
                .withLaunchTemplateData(new RequestLaunchTemplateData()
                .withImageId(ITAccount.getImageIdInRegion())
                .withInstanceType(InstanceType.T2Micro)
                .withNetworkInterfaces(new LaunchTemplateInstanceNetworkInterfaceSpecificationRequest()
                .withAssociatePublicIpAddress(false)
                .withDeleteOnTermination(true)
                .withDeviceIndex(0)
                .withIpv6AddressCount(1)
                .withSubnetId(ITAccount.getExistingDisallowedSubnet())
        )
        )

        CreateLaunchTemplateResult createLaunchTemplateResult = null;
        try {
            createLaunchTemplateResult = ec2.createLaunchTemplate(createLaunchTemplateRequest)

        } catch (AmazonEC2Exception e) {
            if (!e.getErrorCode().equals("InvalidLaunchTemplateName.AlreadyExistsException")) {
                throw e;
            }
        }

        String templateId, templateVersion;
        if (createLaunchTemplateResult != null) {
            templateId = createLaunchTemplateResult.getLaunchTemplate().getLaunchTemplateId()
            templateVersion = createLaunchTemplateResult.getLaunchTemplate().getLatestVersionNumber().toString()
        } else {
            DescribeLaunchTemplatesRequest describeLaunchTemplatesRequest = new DescribeLaunchTemplatesRequest().withLaunchTemplateNames(templateName);
            DescribeLaunchTemplatesResult describeLaunchTemplatesResult = ec2.describeLaunchTemplates(describeLaunchTemplatesRequest);
            templateId = describeLaunchTemplatesResult.getLaunchTemplates().get(0).getLaunchTemplateId()
            templateVersion = describeLaunchTemplatesResult.getLaunchTemplates().get(0).getLatestVersionNumber().toString()
        }


        // Initializes a Spot Instance Request
        SpotFleetRequestConfigData config = new SpotFleetRequestConfigData();
        RequestSpotFleetRequest fleetRequestRequest = new RequestSpotFleetRequest();
        fleetRequestRequest.setSpotFleetRequestConfig(config)

        // Request 1 x t1.micro instance with a bid price of $0.03.
        config.setSpotPrice("0.03");

        config.setValidFrom(startDate)
        config.setValidUntil(endDate)
        config.setTerminateInstancesWithExpiration(true)
        config.setIamFleetRole("arn:aws:iam::${ITAccount.getAccountId()}:role/aws-service-role/spotfleet.amazonaws.com/AWSServiceRoleForEC2SpotFleet")

        // Add the launch config.
        LaunchTemplateConfig launchTemplateConfig = new LaunchTemplateConfig()
                .withLaunchTemplateSpecification(new FleetLaunchTemplateSpecification()
                    .withLaunchTemplateId(templateId)
                    .withVersion(templateVersion)
                )
        config.withLaunchTemplateConfigs(launchTemplateConfig)

        // Call the RequestSpotInstance API.
        RequestSpotFleetResult requestResult =
                ec2.requestSpotFleet(fleetRequestRequest);

        return requestResult.getSpotFleetRequestId()
    }

    private boolean spotInstanceRequestIsCancelling(AmazonEC2 ec2, String spotRequestId) {
        DescribeSpotInstanceRequestsRequest spotInstanceRequestsRequest = new DescribeSpotInstanceRequestsRequest().withSpotInstanceRequestIds(spotRequestId);
        DescribeSpotInstanceRequestsResult describeSpotInstanceRequestsResult = ec2.describeSpotInstanceRequests(spotInstanceRequestsRequest);

        List<SpotInstanceRequest> spotInstanceRequests = describeSpotInstanceRequestsResult.getSpotInstanceRequests();
        return spotInstanceRequests?.get(0)?.getState() == "cancelled"
    }

    private boolean spotFleetRequestIsCancelling(AmazonEC2 ec2, String spotFleetRequestId) {
        DescribeSpotFleetRequestsRequest describeSpotFleetRequestsRequest = new DescribeSpotFleetRequestsRequest().withSpotFleetRequestIds(spotFleetRequestId);
        DescribeSpotFleetRequestsResult describeSpotFleetRequestsResult = ec2.describeSpotFleetRequests(describeSpotFleetRequestsRequest);

        List<SpotFleetRequestConfig> spotFleetRequests = describeSpotFleetRequestsResult.getSpotFleetRequestConfigs();
        return spotFleetRequests?.get(0)?.getSpotFleetRequestState() == "cancelled" || spotFleetRequests?.get(0)?.getSpotFleetRequestState() == "cancelled_terminating";
    }
}
