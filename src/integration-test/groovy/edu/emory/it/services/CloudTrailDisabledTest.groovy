package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.cloudtrail.AWSCloudTrail
import com.amazonaws.services.cloudtrail.AWSCloudTrailClient
import com.amazonaws.services.cloudtrail.model.DeleteTrailRequest
import com.amazonaws.services.cloudtrail.model.DescribeTrailsRequest
import com.amazonaws.services.cloudtrail.model.DescribeTrailsResult
import com.amazonaws.services.cloudtrail.model.GetTrailStatusRequest
import com.amazonaws.services.cloudtrail.model.GetTrailStatusResult
import com.amazonaws.services.cloudtrail.model.StartLoggingRequest
import com.amazonaws.services.cloudtrail.model.StopLoggingRequest
import com.amazonaws.services.cloudtrail.model.Trail
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.CloudTrailDisabledDetector
import edu.emory.it.services.srd.remediator.CloudTrailDisabledRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class CloudTrailDisabledTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSCloudTrail cloudTrail = getClient(ITAccount.getRegion())

        CloudTrailDisabledDetector detector = setupDetector(credentialsProvider)
        CloudTrailDisabledRemediator remediator = setupRemediator(credentialsProvider)

        List<String> existingLogging = turnOffExistingCloudTrailLogging(cloudTrail)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account where CloudTrail not enabled"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.CloudTrailDisabled.name() == it.getType() &&
                    ("arn:aws:cloudtrail:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:trail") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate an account where CloudTrail not enabled"
        DetectedSecurityRiskSubclass detectedRiskNew = new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        then: "remediate should enable CloudTrail"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        boolean cloudTrailCreated = detectedRiskNew.getRemediationResultNew().getDescription().contains("created")
        isCloudTrailEnabled(cloudTrail) == true


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.CloudTrailDisabled.name() == it.getType() &&
                    ("arn:aws:cloudtrail:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:trail") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        if (cloudTrailCreated) {
            deleteCloudTrailFromTest(cloudTrail)
        }
        turnOnExistingLogging(cloudTrail, existingLogging)

    }



    private CloudTrailDisabledDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudTrailDisabledDetector underTest = new CloudTrailDisabledDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private CloudTrailDisabledRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CloudTrailDisabledRemediator underTest = new CloudTrailDisabledRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("s3BucketToUse", "emory-aws-cloudtrail-{accountId}")
        }}
        String propertiesString = "CloudTrailDisabled".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private List<String> turnOffExistingCloudTrailLogging(AWSCloudTrail cloudTrail) {
        DescribeTrailsRequest request = new DescribeTrailsRequest().withIncludeShadowTrails(true)
        DescribeTrailsResult result = cloudTrail.describeTrails(request)

        List<String> activeTrails = []

        for (Trail trail : result.getTrailList()) {
            AWSCloudTrail regionClient = getClient(trail.getHomeRegion())
            GetTrailStatusRequest getTrailStatusRequest = new GetTrailStatusRequest().withName(trail.getName())
            GetTrailStatusResult getTrailStatusResult = regionClient.getTrailStatus(getTrailStatusRequest)
            if (getTrailStatusResult.isLogging()) {
                activeTrails.add(trail.getName())
                StopLoggingRequest stopLoggingRequest = new StopLoggingRequest().withName(trail.getName())
                regionClient.stopLogging(stopLoggingRequest)
            }
        }

        return activeTrails
    }

    private void turnOnExistingLogging(AWSCloudTrail cloudTrail, List<String> existingCloudTrails) {
        DescribeTrailsRequest request = new DescribeTrailsRequest().withIncludeShadowTrails(true).withTrailNameList(existingCloudTrails)
        DescribeTrailsResult result = cloudTrail.describeTrails(request)

        for (Trail trail : result.getTrailList()) {
            AWSCloudTrail regionClient = getClient(trail.getHomeRegion())
            GetTrailStatusRequest getTrailStatusRequest = new GetTrailStatusRequest().withName(trail.getName())
            GetTrailStatusResult getTrailStatusResult = regionClient.getTrailStatus(getTrailStatusRequest)
            if (!getTrailStatusResult.isLogging()) {
                StartLoggingRequest startLoggingRequest = new StartLoggingRequest().withName(trail.getName())
                regionClient.startLogging(startLoggingRequest)
            }
        }
    }

    private void deleteCloudTrailFromTest(AWSCloudTrail cloudTrail) {
        DeleteTrailRequest request = new DeleteTrailRequest().withName("emory-aws-cloudtrail")
        cloudTrail.deleteTrail(request)
    }

    private boolean isCloudTrailEnabled(AWSCloudTrail cloudTrail) {
        GetTrailStatusRequest request = new GetTrailStatusRequest().withName("emory-aws-cloudtrail")
        GetTrailStatusResult result = cloudTrail.getTrailStatus(request)
        return result.getIsLogging()
    }

    protected AWSCloudTrail getClient(String region) {
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(region)
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSCloudTrailClient.class)
    }
}

