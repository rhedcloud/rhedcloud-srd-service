package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult
import com.amazonaws.services.ec2.model.Filter
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.AttachRolePolicyRequest
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest
import com.amazonaws.services.identitymanagement.model.EntityAlreadyExistsException
import com.amazonaws.services.lambda.AWSLambda
import com.amazonaws.services.lambda.AWSLambdaClient
import com.amazonaws.services.lambda.model.CreateFunctionRequest
import com.amazonaws.services.lambda.model.FunctionCode
import com.amazonaws.services.lambda.model.ResourceConflictException
import com.amazonaws.services.lambda.model.Runtime
import com.amazonaws.services.lambda.model.VpcConfig
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.LambdaResidesWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.LambdaResidesWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import edu.emory.it.services.srd.util.VpcUtil
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class LambdaResidesWithinManagementSubnetsTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        LambdaResidesWithinManagementSubnetsDetector detector = new LambdaResidesWithinManagementSubnetsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        LambdaResidesWithinManagementSubnetsRemediator remediator = new LambdaResidesWithinManagementSubnetsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.LambdaResidesWithinManagementSubnets.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:lambda:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:function:${RdsDatabaseHelper.lambdaFunctionName}"

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Lambda function ${detected.get(0).getAmazonResourceName()} deleted"
    }

    def setupSpec() {
        AmazonEC2 ec2Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonEC2Client.class)
        AmazonIdentityManagement iamClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonIdentityManagementClient.class)
        AWSLambda lambdaClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AWSLambdaClient.class)

        def assumeRolePolicyDocument = """{
                "Version": "2012-10-17",
                "Statement": [
                    {
                      "Effect": "Allow",
                      "Principal": {
                        "Service": "lambda.amazonaws.com"
                      },
                      "Action": "sts:AssumeRole"
                    }
                ]
            }"""

        def roleArn = "arn:aws:iam::${ITAccount.getAccountId()}:role/service-role/${RdsDatabaseHelper.lambdaRoleName}"

        try {
            CreateRoleRequest createRoleRequest = new CreateRoleRequest()
                    .withRoleName(RdsDatabaseHelper.lambdaRoleName)
                    .withPath("/service-role/")
                    .withAssumeRolePolicyDocument(assumeRolePolicyDocument)
            iamClient.createRole(createRoleRequest)

            // see https://docs.aws.amazon.com/lambda/latest/dg/vpc.html
            AttachRolePolicyRequest attachRolePolicyRequest = new AttachRolePolicyRequest()
                    .withRoleName(RdsDatabaseHelper.lambdaRoleName)
                    .withPolicyArn("arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole")
            iamClient.attachRolePolicy(attachRolePolicyRequest)
        } catch (EntityAlreadyExistsException e) {
            // ignore
        }

        Set<String> disallowedSubnets = VpcUtil.getDisallowedSubnets(ec2Client)
        if (disallowedSubnets.isEmpty())
            throw new RuntimeException("Tests rely on disallowed subnets")

        // create the ZIP file for deployment
        File deploymentZip = File.createTempFile(RdsDatabaseHelper.lambdaFunctionName, ".zip")
        deploymentZip.deleteOnExit()

        new ZipOutputStream(new FileOutputStream(deploymentZip)).withCloseable { zipFile ->
            zipFile.putNextEntry(new ZipEntry("index.js"))

            File file = new File(getClass().getClassLoader().getResource("lambda-function/index.js").getFile())
            def buffer = new byte[1024]
            file.withInputStream { i ->
                def l = i.read(buffer)
                zipFile.write(buffer, 0, l)
            }

            zipFile.closeEntry()
        }

        ByteBuffer buffer = ByteBuffer.allocate((int) deploymentZip.size())
        new FileInputStream(deploymentZip).withCloseable { fileInputStream ->
            FileChannel fileChannel = fileInputStream.getChannel()
            fileChannel.read(buffer)
        }
        buffer.flip()  // prepare for reading from the buffer

        String groupId
        try {
            CreateSecurityGroupRequest createSecurityGroupRequest = new CreateSecurityGroupRequest()
                    .withGroupName("srd-test-lambda-in-disallowed-subnet")
                    .withVpcId(ITAccount.getVPCWithDisallowedSubnet())
                    .withDescription("Security group for testing")
            CreateSecurityGroupResult createSecurityGroupResult = ec2Client.createSecurityGroup(createSecurityGroupRequest)
            groupId = createSecurityGroupResult.groupId
        } catch (AmazonEC2Exception e) {
            if (e.getErrorCode().equals("InvalidGroup.Duplicate")) {
                DescribeSecurityGroupsResult describeSecurityGroupsResult = ec2Client.describeSecurityGroups(
                        new DescribeSecurityGroupsRequest().withFilters(
                                new Filter("group-name", ["srd-test-lambda-in-disallowed-subnet"])))
                groupId = describeSecurityGroupsResult.securityGroups[0].groupId
            } else {
                throw e
            }
        }

        CreateFunctionRequest createFunctionRequest = new CreateFunctionRequest()
                .withFunctionName(RdsDatabaseHelper.lambdaFunctionName)
                .withRuntime(Runtime.Nodejs610)
                .withCode(new FunctionCode()
                        .withZipFile(buffer))
                .withHandler("index.handler")
                .withRole(roleArn)
                .withVpcConfig(new VpcConfig()
                        .withSubnetIds(disallowedSubnets)
                        .withSecurityGroupIds(groupId))

        try {
            lambdaClient.createFunction(createFunctionRequest)
        } catch (ResourceConflictException e) {
            // thrown if the function already exists but that shouldn't happen
            // as it's should have been deleted at the end of the test as part of the remediation
            // but since it does exist, just use the existing function in the remainder of the test
        }
    }
}
