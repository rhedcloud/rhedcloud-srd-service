package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.CreateVpcRequest
import com.amazonaws.services.ec2.model.CreateVpcResult
import com.amazonaws.services.ec2.model.DeleteVpcRequest
import com.amazonaws.services.ec2.model.DescribeFlowLogsRequest
import com.amazonaws.services.ec2.model.DescribeFlowLogsResult
import com.amazonaws.services.ec2.model.Filter
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.VPCFlowLogsDisabledDetector
import edu.emory.it.services.srd.remediator.VPCFlowLogsDisabledRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when remediator has been given permission to create Flow Logs.
class VPCFlowLogsDisabledTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        VPCFlowLogsDisabledDetector detector = setupDetector(credentialsProvider)
        VPCFlowLogsDisabledRemediator remediator = setupRemediator(credentialsProvider)

        String vpcId = createVPC(ec2)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account where VPC Flow Log is not enabled"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.VPCFlowLogsDisabled.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:vpc/${vpcId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate an account where VPC Flow Log is not enabled"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)


        then: "remediate should enable cache on the stage"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isVPCFlowLogEnabled(ec2, vpcId) == true


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.VPCFlowLogsDisabled.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:vpc/${vpcId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteVPC(ec2, vpcId)
    }


    private VPCFlowLogsDisabledDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        VPCFlowLogsDisabledDetector underTest = new VPCFlowLogsDisabledDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private VPCFlowLogsDisabledRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        VPCFlowLogsDisabledRemediator underTest = new VPCFlowLogsDisabledRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("flowLogRole", "RHEDcloudVpcFlowLogRole")
        }}
        String propertiesString = "VPCFlowLogsDisabled".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createVPC(AmazonEC2 ec2) {
        CreateVpcRequest request = new CreateVpcRequest().withCidrBlock("10.0.0.0/16").withInstanceTenancy("default")
        CreateVpcResult result = ec2.createVpc(request)
        return result.getVpc().getVpcId()
    }

    private void deleteVPC(AmazonEC2 ec2, String vpcId) {
        if (vpcId) {
            DeleteVpcRequest request = new DeleteVpcRequest().withVpcId(vpcId)
            ec2.deleteVpc(request)
        }
    }

    private boolean isVPCFlowLogEnabled(AmazonEC2 ec2, String vpcId) {
        DescribeFlowLogsRequest flowLogsRequest = new DescribeFlowLogsRequest().withFilter(new Filter() {{
            setName("resource-id")
            setValues(Arrays.asList(vpcId))
        }})
        DescribeFlowLogsResult flowLogsResult = ec2.describeFlowLogs(flowLogsRequest)
        return !flowLogsResult.getFlowLogs().isEmpty()
    }
}

