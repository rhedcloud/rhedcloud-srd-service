package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.CreateBucketRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.S3BucketsResideOutsideOfUSRegionsDetector
import edu.emory.it.services.srd.remediator.S3BucketsResideOutsideOfUSRegionsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class S3BucketsResideOutsideOfUSRegionsTest extends Specification {

    private static final String NON_US_REGION = "ca-central-1"
    private static final String BUCKET_NAME = "integration-test-bucket-in-the-great-white-north"
    private static final String FILE_NAME = "hoser.txt"
    private static final String FILE_CONTENT = "Coo roo coo coo, coo coo coo cooo"

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        S3BucketsResideOutsideOfUSRegionsDetector detector = new S3BucketsResideOutsideOfUSRegionsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        S3BucketsResideOutsideOfUSRegionsRemediator remediator = new S3BucketsResideOutsideOfUSRegionsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        detected != null
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.S3BucketsResideOutsideOfUSRegions.name() == it.getType() &&
                    it.getAmazonResourceName().contains("arn:aws:s3:::${BUCKET_NAME}")
        })
        detectedRisk != null

        when:
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)

        then:
        detectedRisk.getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detectedRisk.getRemediationResult().getDescription() == "Remediated S3 bucket '${BUCKET_NAME}' in location '${NON_US_REGION}'"
    }

    def setupSpec() {
        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRole(ITAccount.getRole())
                .build(AmazonS3Client.class)

        CreateBucketRequest createBucketRequest = new CreateBucketRequest(BUCKET_NAME, NON_US_REGION)
        Bucket bucket = s3.createBucket(createBucketRequest)
        println bucket

        s3.putObject(BUCKET_NAME, FILE_NAME, FILE_CONTENT)
    }
}
