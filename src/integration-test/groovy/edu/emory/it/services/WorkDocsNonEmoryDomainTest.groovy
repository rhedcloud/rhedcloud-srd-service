package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.workdocs.AmazonWorkDocs
import com.amazonaws.services.workdocs.AmazonWorkDocsClient
import com.amazonaws.services.workdocs.model.CreateUserRequest
import com.amazonaws.services.workdocs.model.CreateUserResult
import com.amazonaws.services.workdocs.model.DeleteUserRequest
import com.amazonaws.services.workdocs.model.DescribeUsersRequest
import com.amazonaws.services.workdocs.model.DescribeUsersResult
import com.amazonaws.services.workdocs.model.EntityAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.WorkDocsNonEmoryDomainDetector
import edu.emory.it.services.srd.remediator.WorkDocsNonEmoryDomainRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when workdocs is set up on testing account and ORG_ID is correctly added
class WorkDocsNonEmoryDomainTest extends Specification {

    // ORG ID taken from Directory Service.
    // See https://docs.aws.amazon.com/workdocs/latest/developerguide/connect-workdocs-iam.html
    static final String ORG_ID = "TODO"

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonWorkDocs workDocs = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonWorkDocsClient.class)

        WorkDocsNonEmoryDomainDetector detector = setupDetector(credentialsProvider)
        WorkDocsNonEmoryDomainRemediator remediator = setupRemediator(credentialsProvider)

        String userId = createWorkDocsUser(workDocs)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a distribution where cache is not enabled"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.WorkDocsNonEmoryDomain.name() == it.getType() &&
                    ("arn:aws:workdocs:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:user/${userId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a distribution where cache is not enabled"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        then: "remediate should enable cache on the stage"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        doesUserExist(workDocs, userId) == false


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.WorkDocsNonEmoryDomain.name() == it.getType() &&
                    ("arn:aws:workdocs:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:user/${userId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteUser(workDocs, userId)

    }

    private WorkDocsNonEmoryDomainDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        WorkDocsNonEmoryDomainDetector underTest = new WorkDocsNonEmoryDomainDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private WorkDocsNonEmoryDomainRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        WorkDocsNonEmoryDomainRemediator underTest = new WorkDocsNonEmoryDomainRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("organizationId",  ORG_ID)
        }}
        String propertiesString = "WorkDocsNonEmoryDomain".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }


    private String createWorkDocsUser(AmazonWorkDocs workDocs) {
        CreateUserRequest createUserRequest = new CreateUserRequest()
                .withOrganizationId(ORG_ID)
                .withEmailAddress("testing@gmail.com")
                .withGivenName("First")
                .withSurname("Last")
                .withUsername("testingaccount")
                .withPassword("Testing!23")

        try {
            CreateUserResult result = workDocs.createUser(createUserRequest)
            return result.getUser().getId()
        } catch (EntityAlreadyExistsException e) {
            DescribeUsersRequest describeUsersRequest = new DescribeUsersRequest().withOrganizationId(ORG_ID).withQuery("testingaccount")
            DescribeUsersResult result = workDocs.describeUsers(describeUsersRequest)
            return result.getUsers().get(0).getId()
        }

    }

    private boolean doesUserExist(AmazonWorkDocs workDocs, String userId) {
        DescribeUsersRequest request = new DescribeUsersRequest().withUserIds(userId)
        DescribeUsersResult result = workDocs.describeUsers(request)
        return !result.getUsers().isEmpty()
    }

    private void deleteUser(AmazonWorkDocs workDocs, String userId) {
        if (userId && doesUserExist(workDocs, userId)) {
            DeleteUserRequest request = new DeleteUserRequest().withUserId(userId)
            workDocs.deleteUser(request)
        }
    }
}

