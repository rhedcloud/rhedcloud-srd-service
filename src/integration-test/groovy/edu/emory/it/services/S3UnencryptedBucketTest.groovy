package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.Bucket
import com.amazonaws.services.s3.model.GetBucketEncryptionResult
import com.amazonaws.services.s3.model.GetObjectMetadataRequest
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectResult
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.S3UnencryptedBucketDetector
import edu.emory.it.services.srd.remediator.S3UnencryptedBucketRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class S3UnencryptedBucketTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonS3 s3 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonS3Client.class)

        S3UnencryptedBucketDetector detector = setupDetector(credentialsProvider)
        S3UnencryptedBucketRemediator remediator = setupRemediator(credentialsProvider)

        String bucketName = createUnencryptedBucketOnAws(s3)
        String bucketKey = createUnencryptedObjectOnAws(s3, bucketName)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)


        when: "detecting an account with a unencrypted bucket"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.S3UnencryptedBucket.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate an account with an unencrypted bucket"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        waitForBucketEncryptionToTakeEffect(s3, bucketName)

        then: "remediate should encrypt the bucket and objects inside"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isBucketEncryptionOn(s3, bucketName) == true
        isObjectEncrypted(s3, bucketName, bucketKey) == true


        when: "detecting an account that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk riskdetected2 = detected2.find({
            DetectionType.S3UnencryptedBucket.name() == it.getType() &&
                    ("arn:aws:s3:::${bucketName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        riskdetected2 == null

        cleanup:
        deleteObject(s3, bucketName, bucketKey)
        deleteBucket(s3, bucketName)

    }


    private S3UnencryptedBucketDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        S3UnencryptedBucketDetector underTest = new S3UnencryptedBucketDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private S3UnencryptedBucketRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        S3UnencryptedBucketRemediator underTest = new S3UnencryptedBucketRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createUnencryptedBucketOnAws(AmazonS3 s3) {
        String bucketName = "srd-test-unencrypted-bucket-${System.currentTimeMillis()}"
        Bucket bucket = s3.createBucket(bucketName)

        return bucket.getName()
    }

    private String createUnencryptedObjectOnAws(AmazonS3 s3, String bucketName) {
        String bucketKey = "tmp/srd-test-unencrypted-bucket.txt"
        PutObjectResult putObjectResult = s3.putObject(bucketName, bucketKey, "test-content")

        return bucketKey
    }

    private void deleteObject(AmazonS3 s3, String bucketName, String bucketKey) {
        if (bucketName != null && bucketKey != null) {
            s3.deleteObject(bucketName, bucketKey)
        }
    }

    private void deleteBucket(AmazonS3 s3, String bucketName) {
        if (bucketName != null) {
           s3.deleteBucket(bucketName)
        }
    }

    private boolean isBucketEncryptionOn(AmazonS3 s3, String bucketName) {
        GetBucketEncryptionResult result = null
        try {
            result = s3.getBucketEncryption(bucketName)

        } catch ( AmazonS3Exception e) {
            if (e.getStatusCode() != 404) {  //ignore not found exception
                throw e
            }
        }

        String algorithm = result?.getServerSideEncryptionConfiguration()?.getRules()?.get(0)?.getApplyServerSideEncryptionByDefault()?.getSSEAlgorithm()
        return algorithm != null
    }

    private boolean isObjectEncrypted(AmazonS3 s3, String bucketName, String bucketKey) {

        GetObjectMetadataRequest request2 =
                new GetObjectMetadataRequest(bucketName, bucketKey)

        ObjectMetadata metadata = s3.getObjectMetadata(request2)

        if (metadata.getSSEAlgorithm() == null || metadata.getSSEAlgorithm().isEmpty()) {
            return false
        } else {
            return true
        }
    }

    private void waitForBucketEncryptionToTakeEffect(AmazonS3 s3, String bucketName) {
        for (int i=0; !isBucketEncryptionOn(s3, bucketName) && i < 60; i++) {
            sleep(5000)
        }
        sleep(5000)
    }
}
