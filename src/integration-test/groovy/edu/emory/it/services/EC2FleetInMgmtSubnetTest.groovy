package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.services.ec2.AmazonEC2
import com.amazonaws.services.ec2.AmazonEC2Client
import com.amazonaws.services.ec2.model.AmazonEC2Exception
import com.amazonaws.services.ec2.model.CreateFleetRequest
import com.amazonaws.services.ec2.model.CreateFleetResult
import com.amazonaws.services.ec2.model.CreateLaunchTemplateRequest
import com.amazonaws.services.ec2.model.CreateLaunchTemplateResult
import com.amazonaws.services.ec2.model.DescribeLaunchTemplatesRequest
import com.amazonaws.services.ec2.model.DescribeLaunchTemplatesResult
import com.amazonaws.services.ec2.model.FleetLaunchTemplateConfigRequest
import com.amazonaws.services.ec2.model.FleetLaunchTemplateSpecificationRequest
import com.amazonaws.services.ec2.model.InstanceType
import com.amazonaws.services.ec2.model.LaunchTemplateInstanceNetworkInterfaceSpecificationRequest
import com.amazonaws.services.ec2.model.RequestLaunchTemplateData
import com.amazonaws.services.ec2.model.TargetCapacitySpecificationRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.EC2FleetInMgmtSubnetDetector
import edu.emory.it.services.srd.remediator.EC2FleetInMgmtSubnetRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - ignored because we don't have permission describe fleets on test account
class EC2FleetInMgmtSubnetTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonEC2 ec2 = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AmazonEC2Client.class)

        EC2FleetInMgmtSubnetDetector detector = setupDetector(credentialsProvider)
        EC2FleetInMgmtSubnetRemediator remediator = setupRemediator(credentialsProvider)

        String fleetId = createFleet(ec2)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with spot requests in a disallowed subnet"
        List<DetectedSecurityRisk> detecteds = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected = detecteds.find({
            DetectionType.EC2FleetInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:fleet/${fleetId}") == it.getAmazonResourceName()
        })
        then: "error should be detected"
        detecteds.isEmpty() == false
        detected != null

        when: "remediate an account with spot requests in a disallowed subnet"
        DetectedSecurityRiskSubclass detectedRisk =  new DetectedSecurityRiskSubclass(detected)
        remediator.remediate(ITAccount.getAccountId(), detectedRisk, srContext)
        sleep(60000)

        then: "remediate should terminate the instance"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRisk.getRemediationResultNew().getStatus()
        null != detectedRisk.getRemediationResultNew().getDescription()


        when: "detecting an account with stopped instances that have unencrypted instances"
        List<DetectedSecurityRisk> detecteds2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detected3 = detecteds2.find({
            DetectionType.EC2FleetInMgmtSubnet.name() == it.getType() &&
                    ("arn:aws:ec2:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:fleet/${fleetId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detected3 == null

    }


    private EC2FleetInMgmtSubnetDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2FleetInMgmtSubnetDetector underTest = new EC2FleetInMgmtSubnetDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new com.amazonaws.regions.Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private EC2FleetInMgmtSubnetRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        EC2FleetInMgmtSubnetRemediator underTest = new EC2FleetInMgmtSubnetRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    //Private class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createFleet(AmazonEC2 ec2) {
        def startDate = new Date() + 7
        def endDate = new Date() + 14
        String templateName = "srd-test-ec2-fleet-in-disallowed-subnet"
        CreateLaunchTemplateRequest createLaunchTemplateRequest = new CreateLaunchTemplateRequest()
                .withLaunchTemplateName(templateName)
                .withLaunchTemplateData(new RequestLaunchTemplateData()
                    .withImageId(ITAccount.getImageIdInRegion())
                    .withInstanceType(InstanceType.T2Micro)
                    .withNetworkInterfaces(new LaunchTemplateInstanceNetworkInterfaceSpecificationRequest()
                        .withAssociatePublicIpAddress(false)
                        .withDeleteOnTermination(true)
                        .withDeviceIndex(0)
                        .withIpv6AddressCount(1)
                        .withSubnetId(ITAccount.getExistingDisallowedSubnet())
                    )
                )

        CreateLaunchTemplateResult createLaunchTemplateResult = null;
        try {
            createLaunchTemplateResult = ec2.createLaunchTemplate(createLaunchTemplateRequest)

        } catch (AmazonEC2Exception e) {
            if (!e.getErrorCode().equals("InvalidLaunchTemplateName.AlreadyExistsException")) {
                throw e;
            }
        }

        String templateId, templateVersion;
        if (createLaunchTemplateResult != null) {
            templateId = createLaunchTemplateResult.getLaunchTemplate().getLaunchTemplateId()
            templateVersion = createLaunchTemplateResult.getLaunchTemplate().getLatestVersionNumber().toString()
        } else {
            DescribeLaunchTemplatesRequest describeLaunchTemplatesRequest = new DescribeLaunchTemplatesRequest().withLaunchTemplateNames(templateName);
            DescribeLaunchTemplatesResult describeLaunchTemplatesResult = ec2.describeLaunchTemplates(describeLaunchTemplatesRequest);
            templateId = describeLaunchTemplatesResult.getLaunchTemplates().get(0).getLaunchTemplateId()
            templateVersion = describeLaunchTemplatesResult.getLaunchTemplates().get(0).getLatestVersionNumber().toString()
        }


        CreateFleetRequest createFleetRequest = new CreateFleetRequest()
            .withTargetCapacitySpecification(
                new TargetCapacitySpecificationRequest()
                    .withDefaultTargetCapacityType("spot")
                    .withSpotTargetCapacity(1)
                    .withOnDemandTargetCapacity(0)
                    .withTotalTargetCapacity(1)
            )
            .withTerminateInstancesWithExpiration(true)
            .withLaunchTemplateConfigs(
                new FleetLaunchTemplateConfigRequest()
                    .withLaunchTemplateSpecification(new FleetLaunchTemplateSpecificationRequest()
                        .withLaunchTemplateId(templateId)
                        .withVersion(templateVersion)
                    )
                    //TODO add overrides and check
            )
            .withValidFrom(startDate)
            .withValidUntil(endDate)

        CreateFleetResult createFleetResult = ec2.createFleet(createFleetRequest)

        return createFleetResult.getFleetId()
    }
}
