package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.elasticache.AmazonElastiCache
import com.amazonaws.services.elasticache.AmazonElastiCacheClient
import com.amazonaws.services.elasticache.model.CacheClusterAlreadyExistsException
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.RdsHipaaIneligibleMemcachedDetector
import edu.emory.it.services.srd.remediator.RdsHipaaIneligibleMemcachedRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class RdsHipaaIneligibleMemcachedTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        RdsHipaaIneligibleMemcachedDetector detector = new RdsHipaaIneligibleMemcachedDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        RdsHipaaIneligibleMemcachedRemediator remediator = new RdsHipaaIneligibleMemcachedRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.RdsHipaaIneligibleMemcached.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:elasticache:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:cluster:${RdsDatabaseHelper.memcachedCacheClusterId}"

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_NOTIFICATION_ONLY.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Passive Remediation"
    }

    def setupSpec() {
        AmazonElastiCache client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonElastiCacheClient.class)

        // create the ElastiCache memcached cluster
        try {
            RdsDatabaseHelper.createElastiCacheMemcached(client)
        } catch (CacheClusterAlreadyExistsException e) {
            // ignore
        }
    }
}
