package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClient
import com.amazonaws.services.simplesystemsmanagement.model.DeleteParameterRequest
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersRequest
import com.amazonaws.services.simplesystemsmanagement.model.DescribeParametersResult
import com.amazonaws.services.simplesystemsmanagement.model.ParametersFilter
import com.amazonaws.services.simplesystemsmanagement.model.PutParameterRequest
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.SSMUnencryptedParametersDetector
import edu.emory.it.services.srd.remediator.SSMUnencryptedParametersRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class SSMUnencryptedParametersTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSSimpleSystemsManagement ssm = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSSimpleSystemsManagementClient.class)

        SSMUnencryptedParametersDetector detector = setupDetector(credentialsProvider)
        SSMUnencryptedParametersRemediator remediator = setupRemediator(credentialsProvider)

        String parameterName = createParameter(ssm)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account where a parameter is not encrypted."
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.SSMUnencryptedParameters.name() == it.getType() &&
                    ("arn:aws:ssm:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:parameter/${parameterName}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate an account where a parameter is not encrypted."
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        then: "remediate should enable encryptiong"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isParameterEncrypted(ssm, parameterName) == true


        when: "detecting a distribution that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.SSMUnencryptedParameters.name() == it.getType() &&
                    ("arn:aws:ssm:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:parameter/${parameterName}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteParameter(ssm, parameterName)

    }


    private SSMUnencryptedParametersDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        SSMUnencryptedParametersDetector underTest = new SSMUnencryptedParametersDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private SSMUnencryptedParametersRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        SSMUnencryptedParametersRemediator underTest = new SSMUnencryptedParametersRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createParameter(AWSSimpleSystemsManagement ssm) {
        String parameterName = "/srd-test/temp"
        PutParameterRequest request = new PutParameterRequest().withName(parameterName).withType("String").withValue("bla bla")
        ssm.putParameter(request)
        return parameterName
    }

    private boolean isParameterEncrypted(AWSSimpleSystemsManagement ssm, String parameterName) {
        DescribeParametersRequest describeParametersRequest = new DescribeParametersRequest().withFilters(new ParametersFilter(){{
            setKey("Name")
            setValues(new ArrayList<String>(){{
                add(parameterName)
            }})
        }})
        DescribeParametersResult result = ssm.describeParameters(describeParametersRequest)
        return result?.getParameters()?.get(0)?.getType()?.equals("SecureString")
    }

    private void deleteParameter(AWSSimpleSystemsManagement ssm, String parameterName) {
        if (parameterName) {
            DeleteParameterRequest request = new DeleteParameterRequest().withName(parameterName)
            ssm.deleteParameter(request)
        }
    }
}

