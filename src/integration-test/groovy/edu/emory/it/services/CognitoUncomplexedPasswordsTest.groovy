package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.regions.InMemoryRegionImpl
import com.amazonaws.regions.Region
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient
import com.amazonaws.services.cognitoidp.model.CreateUserPoolRequest
import com.amazonaws.services.cognitoidp.model.CreateUserPoolResult
import com.amazonaws.services.cognitoidp.model.DeleteUserPoolRequest
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolRequest
import com.amazonaws.services.cognitoidp.model.DescribeUserPoolResult
import com.amazonaws.services.cognitoidp.model.PasswordPolicyType
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.CognitoUncomplexedPasswordsDetector
import edu.emory.it.services.srd.remediator.CognitoUncomplexedPasswordsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO remove ignore when permission has been given to create user pools
class CognitoUncomplexedPasswordsTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AWSCognitoIdentityProvider cognito = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .build(AWSCognitoIdentityProviderClient.class)


        CognitoUncomplexedPasswordsDetector detector = setupDetector(credentialsProvider)
        CognitoUncomplexedPasswordsRemediator remediator = setupRemediator(credentialsProvider)

        String userPoolId = createUserPool(cognito)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting a CloudFront with no web acl on static site"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.CognitoUncomplexedPasswords.name() == it.getType() &&
                    ("arn:aws:cognito-idp:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:userpool/${userPoolId}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate a CloudFront with non https origin"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)

        then: "remediate should set origin to https only"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isPasswordComplex(cognito, userPoolId) == true


        when: "detecting a CloudFront that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk riskdetected2 = detected2.find({
            DetectionType.CognitoUncomplexedPasswords.name() == it.getType() &&
                    ("arn:aws:cognito-idp:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:userpool/${userPoolId}") == it.getAmazonResourceName()
        })
        then: "there should be no detected issues"
        riskdetected2 == null

        cleanup:
        deleteUserPool(cognito, userPoolId)

    }

    private boolean isPasswordComplex(AWSCognitoIdentityProvider cognito, String userPoolId) {
        DescribeUserPoolRequest describeUserPoolRequest = new DescribeUserPoolRequest().withUserPoolId(userPoolId)
        DescribeUserPoolResult describeUserPoolResult = cognito.describeUserPool(describeUserPoolRequest)
        PasswordPolicyType passwordPolicy = describeUserPoolResult.getUserPool().getPolicies().getPasswordPolicy()

        if ( passwordPolicy.getMinimumLength() == null
                || passwordPolicy.getMinimumLength() < 9
                || passwordPolicy.getRequireLowercase() != Boolean.TRUE
                || passwordPolicy.getRequireUppercase() != Boolean.TRUE
                || passwordPolicy.getRequireNumbers() != Boolean.TRUE
                || passwordPolicy.getRequireSymbols() != Boolean.TRUE) {
            return false
        }

        return true
    }

    private CognitoUncomplexedPasswordsDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CognitoUncomplexedPasswordsDetector underTest = new CognitoUncomplexedPasswordsDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private CognitoUncomplexedPasswordsRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig()

        CognitoUncomplexedPasswordsRemediator underTest = new CognitoUncomplexedPasswordsRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        underTest.setOverrideRegions([new Region(new InMemoryRegionImpl(ITAccount.getRegion(), null))])

        return underTest
    }

    private AppConfig setupAppConfig() {
        AppConfig aConfig = Mock()
        aConfig.getObjects() >> AppConfigHelper.createEOMock()

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }


    private String createUserPool(AWSCognitoIdentityProvider cognito) {
        CreateUserPoolRequest request = new CreateUserPoolRequest().withPoolName("srd-test-${System.currentTimeMillis()}")
        CreateUserPoolResult result = cognito.createUserPool(request)
        result.getUserPool().getId()
    }

    private void deleteUserPool(AWSCognitoIdentityProvider cognito, String userPoolId) {
        if (userPoolId) {
            DeleteUserPoolRequest request = new DeleteUserPoolRequest().withUserPoolId(userPoolId)
            cognito.deleteUserPool(request)
        }
    }


}
