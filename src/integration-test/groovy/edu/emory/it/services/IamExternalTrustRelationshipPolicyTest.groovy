package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazon.aws.moa.objects.resources.v1_0.RemediationResult
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient
import com.amazonaws.services.identitymanagement.model.CreateRoleRequest
import com.amazonaws.services.identitymanagement.model.DeleteRoleRequest
import com.amazonaws.services.identitymanagement.model.EntityAlreadyExistsException
import com.amazonaws.services.identitymanagement.model.GetRoleRequest
import com.amazonaws.services.identitymanagement.model.Role
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.IamExternalTrustRelationshipPolicyDetector
import edu.emory.it.services.srd.remediator.IamExternalTrustRelationshipPolicyRemediator
import edu.emory.it.services.srd.util.AWSPolicyUtil
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.openeai.config.AppConfig
import org.openeai.config.EnterpriseFieldException
import org.openeai.config.PropertyConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
class IamExternalTrustRelationshipPolicyTest extends Specification {

    def "integration test"() throws EnterpriseFieldException {
        setup:
        AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider()

        AmazonIdentityManagement iam = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(credentialsProvider)
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonIdentityManagementClient.class)

        IamExternalTrustRelationshipPolicyDetector detector = setupDetector(credentialsProvider)
        IamExternalTrustRelationshipPolicyRemediator remediator = setupRemediator(credentialsProvider)

        String roleName = createRole(iam)

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when: "detecting an account with an external trust role"
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk = detected.find({
            DetectionType.IamExternalTrustRelationshipPolicy.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:role/${roleName}") == it.getAmazonResourceName()
        })

        then: "error should be detected"
        detected.isEmpty() == false
        detectedRisk != null

        when: "remediate an account with an external trust role"
        DetectedSecurityRiskSubclass detectedRiskNew =  new DetectedSecurityRiskSubclass(detectedRisk)
        remediator.remediate(ITAccount.getAccountId(), detectedRiskNew, srContext)
        then: "remediate should disable the login"
        RemediationStatus.REMEDIATION_SUCCESS.getStatus() == detectedRiskNew.getRemediationResultNew().getStatus()
        null != detectedRiskNew.getRemediationResultNew().getDescription()
        isExternallyShared(iam, roleName) == false


        when: "detecting an account that has been remediated"
        List<DetectedSecurityRisk> detected2 = detector.detect(ITAccount.getAccountId(), srContext)
        DetectedSecurityRisk detectedRisk2 = detected2.find({
            DetectionType.IamExternalTrustRelationshipPolicy.name() == it.getType() &&
                    ("arn:aws:iam::${ITAccount.getAccountId()}:role/${roleName}") == it.getAmazonResourceName()
        })


        then: "there should be no detected issues"
        detectedRisk2 == null

        cleanup:
        deleteRole(iam, roleName)
    }


    private IamExternalTrustRelationshipPolicyDetector setupDetector(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig() //set to zero so that newly created user is expired

        IamExternalTrustRelationshipPolicyDetector underTest = new IamExternalTrustRelationshipPolicyDetector()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private IamExternalTrustRelationshipPolicyRemediator setupRemediator(AWSCredentialsProvider credentialsProvider) {

        AppConfig appConfig = setupAppConfig() //set to zero so that newly created user is expired

        IamExternalTrustRelationshipPolicyRemediator underTest = new IamExternalTrustRelationshipPolicyRemediator()
        underTest.init(appConfig, credentialsProvider, ITAccount.getRole())

        return underTest
    }

    private AppConfig setupAppConfig() {

        AppConfig aConfig = Mock()
        PropertyConfig propertyConfig = Mock()
        propertyConfig.getProperties() >> new Properties() {{
            setProperty("ignoreArns", "")
        }}
        String propertiesString = "IamExternalTrustRelationshipPolicy".toLowerCase()
        aConfig.getObjects() >> AppConfigHelper.createEOMock([
                (propertiesString) : propertyConfig
        ])

        return aConfig
    }

    // class so that we can read what was stored in Remediation Result because regular detected risk will error out while trying to get xml objects
    class DetectedSecurityRiskSubclass extends DetectedSecurityRisk {
        private RemediationResult result

        DetectedSecurityRiskSubclass(DetectedSecurityRisk detectedSecurityRisk) throws EnterpriseFieldException  {
            this.setType(detectedSecurityRisk.getType())
            this.setAmazonResourceName(detectedSecurityRisk.getAmazonResourceName())
        }

        @Override
        void setRemediationResult(RemediationResult result) {
            this.result = result
        }

        RemediationResult getRemediationResultNew() {
            return result
        }
    }

    private String createRole(AmazonIdentityManagement iam) {
        String roleName = "srd-test-external-trust-role"
        CreateRoleRequest request = new CreateRoleRequest().withRoleName(roleName).withAssumeRolePolicyDocument(
                """{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": { "AWS": "arn:aws:iam::${ITAccount.getHipaaAccount()}:root" },
            "Action": "sts:AssumeRole",
            "Condition": {}
        }
    ]
}""")
         try {
             iam.createRole(request)

         } catch (EntityAlreadyExistsException e) {
            //ignore
        }

        return roleName
    }

    private void deleteRole(AmazonIdentityManagement iam, String roleName) {
        DeleteRoleRequest deleteRoleRequest = new DeleteRoleRequest().withRoleName(roleName)
        if (roleName) {
            iam.deleteRole(deleteRoleRequest)
        }
    }

    private boolean isExternallyShared(AmazonIdentityManagement iam, String roleName) {
        GetRoleRequest request = new GetRoleRequest().withRoleName(roleName)
        Role role = iam.getRole(request).getRole()
        def whitelistedAccounts = Collections.singleton(ITAccount.getAccountId())
        // policy documents have to be decoded
        // see https://docs.aws.amazon.com/IAM/latest/APIReference/API_PolicyVersion.html
        String policy = URLDecoder.decode(role.getAssumeRolePolicyDocument(), StandardCharsets.UTF_8.name())
        return AWSPolicyUtil.policyIsAllowAndHasPrincipalNotEqualToAccount(whitelistedAccounts, policy)
    }
}
