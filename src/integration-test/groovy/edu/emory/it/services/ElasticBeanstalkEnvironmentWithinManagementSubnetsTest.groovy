package edu.emory.it.services

import com.amazon.aws.moa.objects.resources.v1_0.DetectedSecurityRisk
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalk
import com.amazonaws.services.elasticbeanstalk.AWSElasticBeanstalkClient
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import edu.emory.it.services.srd.DetectionType
import edu.emory.it.services.srd.RemediationStatus
import edu.emory.it.services.srd.detector.ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector
import edu.emory.it.services.srd.remediator.ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator
import edu.emory.it.services.srd.util.EmoryAwsClientBuilder
import edu.emory.it.services.srd.util.SecurityRiskContext
import org.junit.Ignore
import org.openeai.config.AppConfig
import org.openeai.utils.lock.Lock
import spock.lang.Specification

@org.junit.experimental.categories.Category(ITCategoryFast.class)
@Ignore // TODO - ignore test until we figure out how to create an Elastic Beanstalk environment that is deployed in a disallowed subnet
class ElasticBeanstalkEnvironmentWithinManagementSubnetsTest extends Specification {

    def "Successful Remediation"() {
        setup:
        AppConfig appConfig = Mock()
        appConfig.getObjects() >> AppConfigHelper.createEOMock()
        def credentialsProvider = new EnvironmentVariableCredentialsProvider()

        ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector detector = new ElasticBeanstalkEnvironmentWithinManagementSubnetsDetector()
        detector.init(appConfig, credentialsProvider, ITAccount.getRole())
        ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator remediator = new ElasticBeanstalkEnvironmentWithinManagementSubnetsRemediator()
        remediator.init(appConfig, credentialsProvider, ITAccount.getRole())

        Lock lock = Mock()
        SecurityRiskContext srContext = new SecurityRiskContext(this.class.simpleName + " ", lock)

        when:
        List<DetectedSecurityRisk> detected = detector.detect(ITAccount.getAccountId(), srContext)

        then:
        // one risk should be detected
        detected != null
        detected.size() == 1
        detected.get(0).getType() == DetectionType.ElasticBeanstalkEnvironmentWithinManagementSubnets.name()
        detected.get(0).getAmazonResourceName() == "arn:aws:elasticbeanstalk:${ITAccount.getRegion()}:${ITAccount.getAccountId()}:environment/${RdsDatabaseHelper.ebsApplicationName}/${RdsDatabaseHelper.ebsEnvironmentName}"

        when:
        remediator.remediate(ITAccount.getAccountId(), detected.get(0), srContext)

        then:
        detected.get(0).getRemediationResult().getStatus() == RemediationStatus.REMEDIATION_SUCCESS.getStatus()
        detected.get(0).getRemediationResult().getDescription() == "Environment ${RdsDatabaseHelper.ebsEnvironmentName} terminated"
    }

    def setupSpecTODO() {
        AWSElasticBeanstalk ebsClient = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AWSElasticBeanstalkClient.class)

        AmazonS3 s3Client = new EmoryAwsClientBuilder()
                .withAccountId(ITAccount.getAccountId())
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .withSessionName(this.getClass().getSimpleName())
                .withRegion(ITAccount.getRegion())
                .withRole(ITAccount.getRole())
                .build(AmazonS3Client.class)

        RdsDatabaseHelper.createElasticBeanstalkApplication(ebsClient, s3Client)
    }
}
