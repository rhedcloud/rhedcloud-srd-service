# The RHEDcloud Security Risk Detection Service

## Copyright
This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications.
This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

## License
This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt

## Description
The RHEDcloud Security Risk Detection Service implements security vulnerability detection and remediation when triggered
 by events or on a schedule. Security checker commands are used to detect and correct security vulnerabilities that
 cannot be managed by policy alone.

## Javadoc
The javadoc for the Security Risk Detection Service can be found [here](http://javadoc.rhedcloud.org/rhedcloud-srd-service/index.html).

### Program Flow
```
SecurityRiskDetection(Scheduled|Batch)Command
-> SecurityRiskDetectionGenerateCommand
-> SecurityRiskDetectionSyncCommand -> DynamoDB + SIEM Integration (Splunk)
 + SrdAccountNotificationCommand -> AWS Account Service Account Notifications
```

### Components
* SecurityRiskDetectionBatchCommand:
 On schedule, the command sends a batch oriented Generate command that contains detector information from the configuration.
 
* SecurityRiskDetectionGenerateCommand:
 When processing a "single" request, runs the detector (SRD) and possibly the remediator (SRR).
 In batch mode, each detector listed in the request is run against each (non-srdExempt) account.
 The resulting `SecurityRiskDetection` is published as a Sync message.

* SecurityRiskDetectionSyncCommand:
 Handles Generate Sync messages and persists identified risks to a DynamoDB.
 Also, handles integration with Security Information and Event Management (SIEM) systems.

* SrdAccountNotificationCommand:
 Handles Generate Sync messages and converts them to AccountNotification messages which it sends to AWS Account Service.

* SecurityRiskDetectionQueryCommand:
 Retrieves detection results from DynamoDB for other services to consume.

* AccountSyncCommand:
 Handles Account Create and Update Sync messages which it uses to keep local account information caches up to date.

* SecurityRiskDetectionScheduleCommand:
 Is deprecated and all but replaced by SecurityRiskDetectionBatchCommand. 
 On schedule (minute or hourly), the command queries AWS Account Service for all accounts.
 For each account and each configured detector, it will send a Generate message which is handled by SecurityRiskDetectionGenerationCommand.

##### Detectors / Remediators
See the [Security Risk Detector Technical Design Document](https://serviceforge.atlassian.net/wiki/spaces/ESSRDS/pages/137953292/Security+Risk+Detector+Technical+Design+Document)

### Setup instructions
AWS is the cloud provider and target of the service.
 
##### AWS Credentials
The Security Risk Detection Service runs using a master account that has permission to assume a defined role in the member accounts.
All member accounts must be set up with a role that has permission to perform all operations required by the detectors.
That setup is handled by the [RS CloudFormation templates](https://bitbucket.org/rhedcloud/rhedcloud-aws-rs-account-cfn/src/master/).

Please fill in the following section in the config descriptor with the AWS credentials from the master account
and the name of the role that will be assumed:

    <PropertyConfig name="AwsCredentials" refresh="false">
        <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
        <Property>
            <PropertyName>accessKeyId</PropertyName>
            <PropertyValue>ACCESS_KEY</PropertyValue>
        </Property>
        <Property>
            <PropertyName>secretKey</PropertyName>
            <PropertyValue>SECRET_KEY</PropertyValue>
        </Property>
        <Property>
            <PropertyName>securityRole</PropertyName>
            <PropertyValue>rhedcloud/RHEDcloudSecurityRiskDetectionServiceRole</PropertyValue>
        </Property>
    </PropertyConfig>

In addition to the credentials needed for the master account, credentials to access the DynamoDB tables must be configured.

    <PropertyConfig name="SrdRuntimeStorage" refresh="false">
      <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
      <Property>
        <PropertyName>accessKeyId</PropertyName>
        <PropertyValue>ACCESS_KEY</PropertyValue>
      </Property>
      <Property>
        <PropertyName>secretKey</PropertyName>
        <PropertyValue>SECRET_KEY</PropertyValue>
      </Property>
      <Property>
        <PropertyName>tableRegion</PropertyName>
        <PropertyValue>us-east-1</PropertyValue>
      </Property>
      <Property>
        <PropertyName>storageTableName</PropertyName>
        <PropertyValue>SecurityRiskDetectorRuntimeEnv</PropertyValue>
      </Property>
    </PropertyConfig>

    <PropertyConfig name="SrdSyncStorage" refresh="false">
      <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
      <Property>
        <PropertyName>accessKeyId</PropertyName>
        <PropertyValue>ACCESS_KEY</PropertyValue>
      </Property>
      <Property>
        <PropertyName>secretKey</PropertyName>
        <PropertyValue>SECRET_KEY</PropertyValue>
      </Property>
      <Property>
        <PropertyName>tableRegion</PropertyName>
        <PropertyValue>us-east-1</PropertyValue>
      </Property>
      <Property>
        <PropertyName>storageTableName</PropertyName>
        <PropertyValue>SecurityRiskDetectorSyncEnv</PropertyValue>
      </Property>
    </PropertyConfig>

##### DynamoDB tables
The tables mentioned in the `SrdRuntimeStorage` and `SrdSyncStorage` sections will be created at runtime,
if needed, in the AWS Account associated with the credentials.  By default, the provisioned read and write
capacity will set to a specific amount, but for installations with a lot of accounts, the capacity should
manually be set to auto scale.

The table names are typically named `SecurityRiskDetectorRuntime` and `SecurityRiskDetectorSync` with the environment
name (like Dev, Test, Prod, etc.) as a suffix. 

The account credentials supplied must have permission to create, read, and write to the DynamoDB table.
The following (somewhat broad) policy can be used to give permission to the user associated with the credentials:

Name: SecurityRiskDetectionDynamoDBAccess
Description: Full Access to SecurityRiskDetector tables

    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": "dynamodb:*",
                "Resource": "arn:aws:dynamodb:us-east-1:134285882893:table/SecurityRiskDetector*"
            }
        ]
    }

##### Organizational Unit Names
The Security Risk Detection Service relies on a pre-configured Organizational Unit structure in the master account.
Some detectors/remediators are responsible for checking that a member account is in the correct OU or will move
an account into a specific OU to mitigate detected risks.

The names of the Organizational Units is configurable in an installation though they will typically have well
known names.  See the [environment setup script](https://bitbucket.org/rhedcloud/rhedcloud-aws-admin-master-cfn/src/master/README-env-00.sh)
for more information.

Please fill in the following section in the config descriptor with the correct names.

    <PropertyConfig name="OrgNames" refresh="false">
      <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
      <Property>
        <PropertyName>HipaaOrgName</PropertyName>
        <PropertyValue>RHEDcloudHipaaAccountOrg</PropertyValue>
      </Property>
      <Property>
        <PropertyName>StandardOrgName</PropertyName>
        <PropertyValue>RHEDcloudStandardAccountOrg</PropertyValue>
      </Property>
      <Property>
        <PropertyName>QuarantineOrgName</PropertyName>
        <PropertyValue>RHEDcloudAccountQuarantineOrg</PropertyValue>
      </Property>
      <Property>
        <PropertyName>PendingDeleteOrgName</PropertyName>
        <PropertyValue>RHEDcloudAccountPendingDeleteOrg</PropertyValue>
      </Property>
      <Property>
        <PropertyName>AdministrationOrgName</PropertyName>
        <PropertyValue>RHEDcloudAccountAdministrationOrg</PropertyValue>
      </Property>
    </PropertyConfig>

##### SRD/SRR Configuration
Many of the SRDs and SRRs have specific configurations that are also specified in the configuration file.

In particular, every SRD supports two optional configuration properties:
1. AdditionalNotificationText, and
1. FrequencyBackoff

###### AdditionalNotificationText
When a risk is detected, an Account/User Notification may be sent to the owner of the account.
If this property is set, the text from the property value will be included in the text of the notification.
Typically, it will contain general information regarding the risk and URL links to knowledge base articles
to further assist the owner of the AWS member account.

###### FrequencyBackoff
Each detector will run as frequently as possible in each account.  From a security perspective, this provides a very
small window of time that a risk is active in an account before it is detected and, possibly, remediated.  This can
be important for certain risks, like unencrypted databases, because the risk is remediated before the account user
has had time to load much data into the database.

One of the side effects of continuous detector execution is an increased cost due to CloudTrail tracking all the
AWS API calls made by the detectors.  These costs can add up!!

The `FrequencyBackoff` setting is used to insert a delay (in seconds) between when a detector finishes to when it will run again.

###### Configuration Example

    <PropertyConfig name="AutoscalingGroupInMgmtSubnet" refresh="false">
      <ConfigClass>org.openeai.config.PropertyConfig</ConfigClass>
      <Property>
        <PropertyName>AdditionalNotificationText</PropertyName>
        <PropertyValue>
          Your Auto Scaling group has been terminated because Auto Scaling groups must be
          launched in either public or private subnets.
          For more information, visit https://site.service-now.com/sp?id=kb_article&amp;sys_id=123abc456def789ghi
          and https://d0.awsstatic.com/whitepapers/compliance/AWS_HIPAA_Compliance_Whitepaper.pdf,
          or contact the support team for help at aws.help@site.org
        </PropertyValue>
      </Property>
      <Property>
        <PropertyName>FrequencyBackoff</PropertyName>
        <PropertyValue>900</PropertyValue>
      </Property>
    </PropertyConfig>
